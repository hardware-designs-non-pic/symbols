Element(0x00 "Trimpot - RM063 side adj" "" "pot-RM063" 260 0 3 100 0x00)
(
	# top
	Pin(50 50 60 38 "1" 0x101)
	# bottom
	Pin(50 250 60 38 "3" 0x01)
	# middle
	Pin(150 150 60 38 "5" 0x01)
	# front
	ElementLine(0 0 0 300 10)
	# bottom
	ElementLine(0 300 200 300 10)
	# back
	ElementLine(200 0 200 300 10)
	#  top
	ElementLine(200 0 0 0 10)

	Mark(50 50)
)
