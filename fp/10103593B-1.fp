
Element["" "micro-USB jack SM" "" "`FCI 10103593'" 26261 13000 2478 -10000 2 100 "auto"]
(
	Pad[5118 12500 5118 17000 984 1000 4500 "1" "1" "onsolder,square,edge2"]
	Pad[2559 12500 2559 17000 984 1000 4500 "2" "2" "onsolder,square,edge2"]
	Pad[2559 5000 2559 20000 984 1000 0 "2" "2" "onsolder,square,edge2"]
	Pad[0 12500 0 17000 984 1000 4500 "3" "3" "onsolder,square,edge2"]
	Pad[-2559 12500 -2559 17000 984 1000 4500 "4" "4" "onsolder,square,edge2"]
	Pad[-5118 12500 -5118 17000 984 1000 4500 "5" "5" "onsolder,square,edge2"]
	Pad[-5118 5000 -5118 20000 984 1000 0 "5" "5" "onsolder,square,edge2"]
	Pad[-9154 12500 -9154 17000 3540 1000 4500 "6" "6" "onsolder,square,edge2"]
	Pad[9154 12500 9154 17000 3540 1000 4500 "6" "6" "onsolder,square,edge2"]
	Pad[15500 11000 22750 11000 6000 1000 6000 "6" "6" "onsolder,square,edge2"]
	Pad[-22750 11000 -15500 11000 6000 1000 6000 "6" "6" "onsolder,square"]
	Pad[15500 -2000 22750 -2000 6000 1000 6000 "6" "6" "onsolder,square,edge2"]
	Pad[-22750 -2000 -15500 -2000 6000 1000 6000 "6" "6" "onsolder,square"]
	ElementLine [-15000 -12500 -15000 12500 1000]
	ElementLine [15000 -12500 15000 12500 1000]
	ElementLine [-15000 12500 15000 12500 1000]
	ElementLine [-15000 -12500 15000 -12500 1000]
)
