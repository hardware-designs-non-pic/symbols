
Element["" "" "" "" 2.0000mm 4.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-1.5000mm -4.0000mm 2.0000mm -4.0000mm 40.00mil 20.00mil 60.00mil "" "2" "square"]
	Pad[-1.5000mm 2.5000mm 1.5000mm 2.5000mm 40.00mil 20.00mil 60.00mil "" "6" "square"]
	Pad[-1.5000mm -3.0000mm -1.5000mm -3.0000mm 40.00mil 20.00mil 60.00mil "" "10" "square"]
	Pad[-1.5000mm -3.0000mm -1.5000mm -3.0000mm 40.00mil 0.00mil 60.00mil "" "12" "onsolder,square"]
	Pad[-1.5000mm 4.5000mm 2.0000mm 4.5000mm 40.00mil 20.00mil 60.00mil "" "3" "square"]
	Pad[-1.5000mm -2.0000mm 1.5000mm -2.0000mm 40.00mil 20.00mil 60.00mil "" "7" "square"]
	Pad[-1.5000mm 0.0000 2.0000mm 0.0000 40.00mil 20.00mil 60.00mil "" "1" "square"]
	Pad[-1.5000mm -4.0000mm 2.0000mm -4.0000mm 40.00mil 0.00mil 60.00mil "" "4" "onsolder,square"]
	Pad[-1.5000mm 2.5000mm 1.5000mm 2.5000mm 40.00mil 0.00mil 60.00mil "" "8" "onsolder,square"]
	Pad[-1.5000mm 4.5000mm 2.0000mm 4.5000mm 40.00mil 0.00mil 60.00mil "" "5" "onsolder,square"]
	Pad[-1.5000mm -2.0000mm 1.5000mm -2.0000mm 40.00mil 0.00mil 60.00mil "" "9" "onsolder,square"]
	Pad[-1.5000mm 3.500mm -1.5000mm 3.500mm  40.00mil 0.00mil 60.00mil "" "13" "onsolder,square"]
	Pad[-1.5000mm 3.500mm -1.5000mm 3.500mm  40.00mil 20.00mil 60.00mil "" "11" "square"]
	ElementLine [-2.0000mm 5.0000mm 2.5000mm 5.0000mm 10.00mil]
	ElementLine [2.5000mm -4.5000mm -2.0000mm -4.5000mm 10.00mil]
	ElementLine [2.6800mm 5.0000mm 2.6800mm -4.5000mm 10.00mil]

	)
