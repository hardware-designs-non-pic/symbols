Element(0x00 "Pushbutton - tact switch" "S1" "tact-switch" 260 0 3 100 0x00)
(
	Pin(-150 -100 80 40 "" 0x101)
	Pin(-150  100 80 40 "" 0x01)
	Pin( 150 -100 80 40 "" 0x01)
	Pin( 150  100 80 40 "" 0x01)
	ElementLine(-190 -140  190 -140 10)
	ElementLine( 190 -140  190  140 10)
	ElementLine( 190  140 -190  140 10)
	ElementLine(-190  140 -190 -140 10)

#	ElementLine(-120 -115  120 -115 10)
#	ElementLine( 120 -115  120  125 10)
#	ElementLine( 120  125 -120  125 10)
#	ElementLine(-120  125 -120 -115 10)

	ElementLine(-120  110 120  110 10)
	ElementLine(-120 -100 120 -100 10)

)
