
Element["" "" "" "" 485.00mil 480.00mil 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 5.00mil 560.00mil 20.00mil 566.00mil 550.00mil "" "1" "edge2"]
	Pin[-350.00mil -345.00mil 125.00mil 20.00mil 131.00mil 115.00mil "" "1" "edge2"]
	Pin[350.00mil -345.00mil 125.00mil 20.00mil 131.00mil 115.00mil "" "3" "edge2"]
	Pin[-350.00mil 355.00mil 125.00mil 20.00mil 131.00mil 115.00mil "" "2" "edge2"]
	Pin[350.00mil 355.00mil 125.00mil 20.00mil 131.00mil 115.00mil "" "4" "edge2"]
	Pad[-350.00mil 355.00mil 350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "2" ""]
	Pad[350.00mil -345.00mil 350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "3" "edge2"]
	Pad[-350.00mil -345.00mil 350.00mil -345.00mil 40.00mil 20.00mil 60.00mil "" "4" ""]
	Pad[-350.00mil -345.00mil -350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "5" "edge2"]
	Pad[-350.00mil 355.00mil 350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "6" "onsolder"]
	Pad[350.00mil -345.00mil 350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "7" "onsolder,edge2"]
	Pad[-350.00mil -345.00mil 350.00mil -345.00mil 40.00mil 20.00mil 60.00mil "" "8" "onsolder"]
	Pad[-350.00mil -345.00mil -350.00mil 355.00mil 40.00mil 20.00mil 60.00mil "" "9" "onsolder,edge2"]
	ElementLine [-480.00mil -475.00mil -480.00mil 480.00mil 10.00mil]
	ElementLine [-480.00mil 480.00mil 480.00mil 480.00mil 10.00mil]
	ElementLine [480.00mil 480.00mil 480.00mil -475.00mil 10.00mil]
	ElementLine [480.00mil -475.00mil -480.00mil -475.00mil 10.00mil]

	)
