
Element["" "" "" "" 7.0000mm 7.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-5.2500mm -0.5000mm -5.2500mm 0.5000mm 2.0000mm 20.00mil 2.2540mm "1" "1" "square"]
	Pad[5.2500mm -0.5000mm 5.2500mm 0.5000mm 2.0000mm 20.00mil 2.2540mm "2" "2" "square"]
	ElementLine [-5.7500mm -6.2500mm 5.7500mm -6.2500mm 10.00mil]
	ElementLine [6.2500mm -5.7500mm 6.2500mm 5.7500mm 10.00mil]
	ElementLine [5.7500mm 6.2500mm -5.7500mm 6.2500mm 10.00mil]
	ElementLine [-6.2500mm 5.7500mm -6.2500mm -5.7500mm 10.00mil]
	ElementArc [-5.7500mm -5.7500mm 0.5000mm 0.5000mm 270 90 10.00mil]
	ElementArc [5.7500mm -5.7500mm 0.5000mm 0.5000mm 180 90 10.00mil]
	ElementArc [5.7500mm 5.7500mm 0.5000mm 0.5000mm 90 90 10.00mil]
	ElementArc [-5.7500mm 5.7500mm 0.5000mm 0.5000mm 0 90 10.00mil]

	Attribute("author" "Tobias Nadler")
	Attribute("copyright" "Kai-Martin Knaak / 2013 Tobias Nadler")
	Attribute("use-license" "Unlimited")
	Attribute("dist-license" "GPL2 or GPL3")
	Attribute("description" "PISR inductance")
	)
