
Element["" "" "" "" 3.7500mm 5.5000mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 -3.5000mm 100.00mil 30.00mil 106.00mil 33.00mil "" "1" ""]
	Pin[0.0000 0.0000 100.00mil 30.00mil 106.00mil 33.00mil "" "2" ""]
	Pin[0.0000 3.5000mm 100.00mil 30.00mil 106.00mil 33.00mil "" "3" ""]
	ElementLine [-3.5000mm -5.2500mm -3.5000mm 5.2500mm 0.5000mm]
	ElementLine [-3.5000mm 5.2500mm 3.5000mm 5.2500mm 0.5000mm]
	ElementLine [-3.5000mm -5.2500mm 3.5000mm -5.2500mm 0.5000mm]
	ElementLine [3.5000mm -5.2500mm 3.5000mm 5.2500mm 0.5000mm]
	ElementLine [2.5000mm -5.2500mm 2.5000mm 5.2500mm 0.5000mm]
	ElementLine [-2.0000mm -5.2500mm -2.0000mm 5.2500mm 6.00mil]
	ElementLine [2.0000mm -5.2500mm 2.0000mm 5.2500mm 6.00mil]
	ElementLine [-3.5000mm -1.7500mm 2.5000mm -1.7500mm 0.5000mm]
	ElementLine [-3.5000mm 1.7500mm 2.5000mm 1.7500mm 0.5000mm]

	Attribute("author" "Tobias Nadler")
	Attribute("copyright" "2015 Tobias Nadler")
	Attribute("use-license" "Unlimited")
	Attribute("dist-license" "GPLv2 or GPLv3 or CC BY-SA 3.0")
	Attribute("description" "Terminal block, 3.5mm centers, 3 pin")
	)
