# Author: Erich Heinzle
# No warranties express or implied
# Footprint designed to suit ebay listed 807 FU7 socket
# dist-license: GPL
# use-license: unlimited
Element["" "" "" "" 2060.00mil 2330.00mil 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 -19.5000mm 3.5000mm 20.00mil 3.6524mm 3.1000mm "1" "1" "hole"]
	Pin[0.0000 19.5000mm 3.5000mm 20.00mil 3.6524mm 3.1000mm "2" "2" "hole"]
	Pin[0.0000 0.0000 32.0000mm 20.00mil 32.4000mm 32.0000mm "3" "3" "hole"]
	ElementLine [-5.2500mm 23.3380mm -14.0540mm 11.2180mm 0.4000mm]
	ElementLine [-5.2500mm -23.2500mm -14.0540mm -11.2160mm 0.4000mm]
	ElementLine [14.0540mm -11.2160mm 5.2500mm -23.2500mm 0.4000mm]
	ElementLine [14.0540mm 11.2180mm 5.2500mm 23.3380mm 0.4000mm]
	ElementArc [0.0000 -19.5000mm 6.5000mm 6.5000mm 216 54 0.4000mm]
	ElementArc [0.0000 -19.5000mm 6.5000mm 6.5000mm 270 54 0.4000mm]
	ElementArc [0.0000 19.5000mm 6.5000mm 6.5000mm 90 54 0.4000mm]
	ElementArc [0.0000 19.5000mm 6.5000mm 6.5000mm 36 54 0.4000mm]
	ElementArc [0.0000 0.0000 18.0000mm 18.0000mm 180 40 0.4000mm]
	ElementArc [0.0000 0.0000 18.0000mm 18.0000mm 320 40 0.4000mm]
	ElementArc [0.0000 0.0000 18.0000mm 18.0000mm 0 40 0.4000mm]
	ElementArc [0.0000 0.0000 18.0000mm 18.0000mm 140 40 0.4000mm]
)
