# author: Erich S. Heinzle
# email: a1039181@gmail.com
# dist-license: GPL 2
# use-license: unlimited

Element["" "ferrite_bead" "ferrite_bead" "ferrite_bead" 75.00mil 50.00mil 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 88.43mil 39.37mil 92.37mil 39.37mil "1" "1" "square"]
	Pin[0.0000 400.00mil 88.43mil 39.37mil 92.37mil 39.37mil "2" "2" ""]
	ElementLine [0.0000 300.00mil 0.0000 400.00mil 10.00mil]
	ElementLine [-70.00mil 100.00mil -70.00mil 300.00mil 10.00mil]
	ElementLine [-70.00mil 300.00mil 70.00mil 300.00mil 10.00mil]
	ElementLine [70.00mil 100.00mil 70.00mil 300.00mil 10.00mil]
	ElementLine [-70.00mil 100.00mil 70.00mil 100.00mil 10.00mil]
	ElementLine [0.0000 0.0000 0.0000 98.42mil 10.00mil]

	)
