# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n32
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n32
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 36
#
Element["" " U    " "" "" 0 0 0 -13390 0 100 ""]
(
ElementLine[-10240 7480 -10240 10240 500]
ElementLine[-10240 10240 -7480 10240 500]
ElementLine[-7480 10240 -10240 7480 500]
ElementLine[9840 9840 -9840 9840 500]
ElementLine[-9840 9840 -9840 -9840 500]
ElementLine[-9840 -9840 9840 -9840 500]
ElementLine[9840 -9840 9840 9840 500]
ElementLine[-8280 9850 -9850 8280 500]
ElementLine[-9850 8660 -8670 9840 500]
ElementLine[-9050 9840 -9840 9050 500]
ElementArc[-7480 7480 877 877 0 360 790]
Pad[-4920 8620 -4920 10860 1100 2000 1900 "0" "_2_" "square"]
Pad[-2950 8620 -2950 10860 1100 2000 1900 "0" "_3_" "square"]
Pad[-980 8620 -980 10860 1100 2000 1900 "0" "_4_" "square"]
Pad[980 8620 980 10860 1100 2000 1900 "0" "_5_" "square"]
Pad[6880 8620 6880 10860 1100 2000 1900 "0" "_8_" "square"]
Pad[8620 6880 10860 6880 1100 2000 1900 "0" "_9_" "square"]
Pad[8620 4920 10860 4920 1100 2000 1900 "0" "_10_" "square"]
Pad[8620 2950 10860 2950 1100 2000 1900 "0" "_11_" "square"]
Pad[8620 980 10860 980 1100 2000 1900 "0" "_12_" "square"]
Pad[8620 -980 10860 -980 1100 2000 1900 "0" "_13_" "square"]
Pad[8620 -2950 10860 -2950 1100 2000 1900 "0" "_14_" "square"]
Pad[-6880 8620 -6880 10860 1100 2000 1900 "0" "_1_" "square"]
Pad[3390 3390 3390 3390 6790 2000 7590 "0" "_33_" "square"]
Pad[-3390 3390 -3390 3390 6790 2000 7590 "0" "_33_" "square"]
Pad[-3390 -3390 -3390 -3390 6790 2000 7590 "0" "_33_" "square"]
Pad[3390 -3390 3390 -3390 6790 2000 7590 "0" "_33_" "square"]
Pad[8620 -4920 10860 -4920 1100 2000 1900 "0" "_15_" "square"]
Pad[8620 -6880 10860 -6880 1100 2000 1900 "0" "_16_" "square"]
Pad[6880 -10860 6880 -8620 1100 2000 1900 "0" "_17_" "square"]
Pad[4920 -10860 4920 -8620 1100 2000 1900 "0" "_18_" "square"]
Pad[2950 -10860 2950 -8620 1100 2000 1900 "0" "_19_" "square"]
Pad[980 -10860 980 -8620 1100 2000 1900 "0" "_20_" "square"]
Pad[-980 -10860 -980 -8620 1100 2000 1900 "0" "_21_" "square"]
Pad[-2950 -10860 -2950 -8620 1100 2000 1900 "0" "_22_" "square"]
Pad[-4920 -10860 -4920 -8620 1100 2000 1900 "0" "_23_" "square"]
Pad[-6880 -10860 -6880 -8620 1100 2000 1900 "0" "_24_" "square"]
Pad[-10860 -6880 -8620 -6880 1100 2000 1900 "0" "_25_" "square"]
Pad[-10860 -4920 -8620 -4920 1100 2000 1900 "0" "_26_" "square"]
Pad[-10860 -2950 -8620 -2950 1100 2000 1900 "0" "_27_" "square"]
Pad[-10860 -980 -8620 -980 1100 2000 1900 "0" "_28_" "square"]
Pad[2950 8620 2950 10860 1100 2000 1900 "0" "_6_" "square"]
Pad[4920 8620 4920 10860 1100 2000 1900 "0" "_7_" "square"]
Pad[-10860 980 -8620 980 1100 2000 1900 "0" "_29_" "square"]
Pad[-10860 2950 -8620 2950 1100 2000 1900 "0" "_30_" "square"]
Pad[-10860 4920 -8620 4920 1100 2000 1900 "0" "_31_" "square"]
Pad[-10860 6880 -8620 6880 1100 2000 1900 "0" "_32_" "square"]
)
