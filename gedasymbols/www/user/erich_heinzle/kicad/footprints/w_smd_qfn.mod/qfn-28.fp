# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module qfn-28
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: qfn-28
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 32
#
Element["" " U    " "" "" 0 0 0 -15750 0 100 ""]
(
ElementLine[-12200 8660 -8660 12200 500]
ElementLine[-8660 12200 -12200 12200 500]
ElementLine[-12200 12200 -12200 8660 500]
ElementLine[-9450 11810 -11810 9450 500]
ElementLine[-11810 9450 -11810 9840 500]
ElementLine[-11810 9840 -9840 11810 500]
ElementLine[-9840 11810 -10240 11810 500]
ElementLine[-10240 11810 -11810 10240 500]
ElementLine[-11810 10240 -11810 10630 500]
ElementLine[-11810 10630 -10630 11810 500]
ElementLine[-10630 11810 -11020 11810 500]
ElementLine[-11020 11810 -11810 11020 500]
ElementLine[-11810 -11810 11810 -11810 500]
ElementLine[11810 -11810 11810 11810 500]
ElementLine[11810 11810 -11810 11810 500]
ElementLine[-11810 11810 -11810 -11810 500]
ElementArc[-9060 9050 877 877 0 360 790]
Pad[-5120 10335 -5120 12105 1380 2000 2180 "0" "_2_" "square"]
Pad[-2560 10335 -2560 12105 1380 2000 2180 "0" "_3_" "square"]
Pad[0 10335 0 12105 1380 2000 2180 "0" "_4_" "square"]
Pad[2560 10335 2560 12105 1380 2000 2180 "0" "_5_" "square"]
Pad[10345 7680 12115 7680 1380 2000 2180 "0" "_8_" "square"]
Pad[10345 5120 12115 5120 1380 2000 2180 "0" "_9_" "square"]
Pad[10345 2560 12115 2560 1380 2000 2180 "0" "_10_" "square"]
Pad[10345 0 12115 0 1380 2000 2180 "0" "_11_" "square"]
Pad[10345 -2560 12115 -2560 1380 2000 2180 "0" "_12_" "square"]
Pad[10345 -5120 12115 -5120 1380 2000 2180 "0" "_13_" "square"]
Pad[10345 -7680 12115 -7680 1380 2000 2180 "0" "_14_" "square"]
Pad[-7680 10335 -7680 12105 1380 2000 2180 "0" "_1_" "square"]
Pad[3740 3740 3740 3740 7480 2000 8280 "0" "_29_" "square"]
Pad[-3740 3740 -3740 3740 7480 2000 8280 "0" "_29_" "square"]
Pad[-3740 -3740 -3740 -3740 7480 2000 8280 "0" "_29_" "square"]
Pad[3740 -3740 3740 -3740 7480 2000 8280 "0" "_29_" "square"]
Pad[7879 -12115 7879 -10345 1380 2000 2180 "0" "_15_" "square"]
Pad[5320 -12115 5320 -10345 1380 2000 2180 "0" "_16_" "square"]
Pad[2760 -12115 2760 -10345 1380 2000 2180 "0" "_17_" "square"]
Pad[0 -12115 0 -10345 1380 2000 2180 "0" "_18_" "square"]
Pad[-2360 -12115 -2360 -10345 1380 2000 2180 "0" "_19_" "square"]
Pad[-4920 -12115 -4920 -10345 1380 2000 2180 "0" "_20_" "square"]
Pad[-7480 -12115 -7480 -10345 1380 2000 2180 "0" "_21_" "square"]
Pad[-12105 -7680 -10335 -7680 1380 2000 2180 "0" "_22_" "square"]
Pad[-12105 -5120 -10335 -5120 1380 2000 2180 "0" "_23_" "square"]
Pad[-12105 -2560 -10335 -2560 1380 2000 2180 "0" "_24_" "square"]
Pad[-12105 0 -10335 0 1380 2000 2180 "0" "_25_" "square"]
Pad[-12105 2560 -10335 2560 1380 2000 2180 "0" "_26_" "square"]
Pad[-12105 5120 -10335 5120 1380 2000 2180 "0" "_27_" "square"]
Pad[-12105 7680 -10335 7680 1380 2000 2180 "0" "_28_" "square"]
Pad[5120 10335 5120 12105 1380 2000 2180 "0" "_6_" "square"]
Pad[7680 10335 7680 12105 1380 2000 2180 "0" "_7_" "square"]
)
