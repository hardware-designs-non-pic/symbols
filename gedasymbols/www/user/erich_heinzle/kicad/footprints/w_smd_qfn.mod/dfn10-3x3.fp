# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn10-3x3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn10-3x3
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 12
#
Element["" " dfn10-3x3 " "" "" 0 0 0 -7480 0 100 ""]
(
ElementLine[-6300 -6300 -6300 -4330 500]
ElementLine[-6300 -4330 -4330 -6300 500]
ElementLine[-4330 -6300 -6300 -6300 500]
ElementLine[-5910 -5120 -5120 -5910 500]
ElementLine[-5910 -5910 -5910 5910 500]
ElementLine[-5910 5910 5910 5910 500]
ElementLine[5910 5910 5910 -5910 500]
ElementLine[5910 -5910 -5910 -5910 500]
ElementArc[-4330 -4330 559 559 0 360 500]
Pad[-6496 -3937 -4724 -3937 984 2000 1784 "0" "_1_" "square"]
Pad[-6496 -1968 -4724 -1968 984 2000 1784 "0" "_2_" "square"]
Pad[-6496 0 -4724 0 984 2000 1784 "0" "_3_" "square"]
Pad[-6496 1968 -4724 1968 984 2000 1784 "0" "_4_" "square"]
Pad[-6496 3937 -4724 3937 984 2000 1784 "0" "_5_" "square"]
Pad[4724 3937 6496 3937 984 2000 1784 "0" "_6_" "square"]
Pad[4724 1968 6496 1968 984 2000 1784 "0" "_7_" "square"]
Pad[4724 0 6496 0 984 2000 1784 "0" "_8_" "square"]
Pad[4724 -1968 6496 -1968 984 2000 1784 "0" "_9_" "square"]
Pad[4724 -3937 6496 -3937 984 2000 1784 "0" "_10_" "square"]
Pad[-905 -2342 905 -2342 4685 2000 5485 "0" "_11_" "square"]
Pad[-905 2342 905 2342 4685 2000 5485 "0" "_11_" "square"]
)
