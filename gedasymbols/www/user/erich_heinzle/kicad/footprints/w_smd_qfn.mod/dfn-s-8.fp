# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn-s-8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn-s-8
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 5
# Draw arc object count: 0
# Pad count: 9
#
Element["" " dfn-s-8 " "" "" 0 0 0 15750 0 100 ""]
(
ElementLine[-10240 9840 -10240 12200 500]
ElementLine[-10240 12200 -7870 12200 500]
ElementLine[-7870 12200 -10240 9840 500]
ElementArc[-7480 6690 390 390 0 360 500]
ElementArc[-7480 6690 790 790 0 360 500]
ElementArc[-7480 6690 1180 1180 0 360 500]
ElementArc[-7480 6690 1180 1180 0 360 500]
ElementLine[-8660 11810 -9840 10630 500]
ElementLine[-9840 11020 -9060 11810 500]
ElementLine[-9450 11810 -9840 11420 500]
ElementArc[-7480 6690 1420 1420 0 360 500]
ElementLine[-9840 -11810 9840 -11810 500]
ElementLine[9840 -11810 9840 11810 500]
ElementLine[9840 11810 -9840 11810 500]
ElementLine[-9840 11810 -9840 -11810 500]
Pad[-7500 9745 -7500 12295 1770 2000 2570 "0" "_1_" "blah"]
Pad[-2500 9745 -2500 12295 1770 2000 2570 "0" "_2_" "blah"]
Pad[2500 9745 2500 12295 1770 2000 2570 "0" "_3_" "blah"]
Pad[7500 9745 7500 12295 1770 2000 2570 "0" "_4_" "blah"]
Pad[7500 -12295 7500 -9745 1770 2000 2570 "0" "_5_" "blah"]
Pad[2500 -12295 2500 -9745 1770 2000 2570 "0" "_6_" "blah"]
Pad[-2500 -12295 -2500 -9745 1770 2000 2570 "0" "_7_" "blah"]
Pad[-7500 -12295 -7500 -9745 1770 2000 2570 "0" "_8_" "blah"]
Pad[-3349 0 3349 0 9440 2000 10240 "0" "_9_" "square"]
)
