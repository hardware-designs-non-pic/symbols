# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module r-px2son-n8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: r-px2son-n8
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 8
#
Element["" " r-px2son-n8 " "" "" 0 0 0 -4724 0 100 ""]
(
ElementArc[-1574 984 440 440 0 360 500]
ElementLine[-2755 -2165 2755 -2165 500]
ElementLine[2755 -2165 2755 2165 500]
ElementLine[2755 2165 -2755 2165 500]
ElementLine[-2755 2165 -2755 -2165 500]
ElementLine[-3150 1379 -3150 2559 500]
ElementLine[-3150 2559 -1970 2559 500]
ElementLine[-1970 2559 -3150 1379 500]
Pad[-2066 663 -2066 1962 787 2000 1587 "0" "_1_" "square"]
Pad[-688 663 -688 1962 787 2000 1587 "0" "_2_" "square"]
Pad[688 663 688 1962 787 2000 1587 "0" "_3_" "square"]
Pad[2066 663 2066 1962 787 2000 1587 "0" "_4_" "square"]
Pad[2066 -1962 2066 -663 787 2000 1587 "0" "_5_" "square"]
Pad[688 -1962 688 -663 787 2000 1587 "0" "_6_" "square"]
Pad[-688 -1962 -688 -663 787 2000 1587 "0" "_7_" "square"]
Pad[-2066 -1962 -2066 -663 787 2000 1587 "0" "_8_" "square"]
)
