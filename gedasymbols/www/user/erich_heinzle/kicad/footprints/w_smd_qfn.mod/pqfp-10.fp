# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pqfp-10
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pqfp-10
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" " pqfp-10 " "" "" 0 0 0 -5510 0 100 ""]
(
ElementLine[-4720 2360 -4720 3540 500]
ElementLine[-4720 3540 -3540 3540 500]
ElementLine[-3540 3540 -4720 2360 500]
ElementLine[-3150 3150 -2760 3150 500]
ElementLine[4330 -3150 -4330 -3150 500]
ElementLine[-4320 -3150 -4320 3150 500]
ElementLine[-4330 3150 4330 3150 500]
ElementLine[4330 3150 4330 -3150 500]
Pad[-2950 1950 -2950 3050 1170 2000 1970 "0" "_1_" "square"]
Pad[-970 1850 -970 3150 970 2000 1770 "0" "_2_" "square"]
Pad[970 1850 970 3150 970 2000 1770 "0" "_3_" "square"]
Pad[2950 1950 2950 3050 1170 2000 1970 "0" "_4_" "square"]
Pad[2840 0 3940 0 1370 2000 2170 "0" "_5_" "square"]
Pad[2950 -3050 2950 -1950 1170 2000 1970 "0" "_6_" "square"]
Pad[970 -3150 970 -1850 970 2000 1770 "0" "_7_" "square"]
Pad[-970 -3150 -970 -1850 970 2000 1770 "0" "_8_" "square"]
Pad[-2950 -3050 -2950 -1950 1170 2000 1970 "0" "_9_" "square"]
Pad[-3940 0 -2840 0 1370 2000 2170 "0" "_10_" "square"]
)
