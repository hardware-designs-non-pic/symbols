# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module qfn-16
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: qfn-16
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 20
#
Element["" " U    " "" "" 0 0 0 -11420 0 100 ""]
(
ElementLine[-7088 10238 -10238 7088 500]
ElementLine[-10238 7088 -10238 10238 500]
ElementLine[-10238 10238 -7088 10238 500]
ElementLine[-9838 7878 -7878 9838 500]
ElementLine[-9838 8268 -8268 9838 500]
ElementLine[-8268 9838 -8658 9838 500]
ElementLine[-8658 9838 -9838 8658 500]
ElementLine[-9838 8658 -9838 9058 500]
ElementLine[-9838 9058 -9058 9838 500]
ElementLine[-9842 9842 -9842 -9842 500]
ElementLine[-9842 -9842 9842 -9842 500]
ElementLine[9842 -9842 9842 9842 500]
ElementLine[9842 9842 -9842 9842 500]
ElementArc[-7094 7074 877 877 0 360 780]
Pad[-4724 7992 -4724 9724 1417 2000 2217 "0" "_1_" "square"]
Pad[-1574 7992 -1574 9724 1417 2000 2217 "0" "_2_" "square"]
Pad[1574 7992 1574 9724 1417 2000 2217 "0" "_3_" "square"]
Pad[4724 7992 4724 9724 1417 2000 2217 "0" "_4_" "square"]
Pad[7992 1574 9724 1574 1417 2000 2217 "0" "_6_" "square"]
Pad[7992 -1574 9724 -1574 1417 2000 2217 "0" "_7_" "square"]
Pad[7992 -4724 9724 -4724 1417 2000 2217 "0" "_8_" "square"]
Pad[4724 -9724 4724 -7992 1417 2000 2217 "0" "_9_" "square"]
Pad[1574 -9724 1574 -7992 1417 2000 2217 "0" "_10_" "square"]
Pad[3198 3198 3198 3198 6397 2000 7197 "0" "_21_" "square"]
Pad[-3198 3198 -3198 3198 6397 2000 7197 "0" "_21_" "square"]
Pad[-3198 -3198 -3198 -3198 6397 2000 7197 "0" "_21_" "square"]
Pad[3198 -3198 3198 -3198 6397 2000 7197 "0" "_21_" "square"]
Pad[-1574 -9724 -1574 -7992 1417 2000 2217 "0" "_11_" "square"]
Pad[-4724 -9724 -4724 -7992 1417 2000 2217 "0" "_12_" "square"]
Pad[-9724 -4724 -7992 -4724 1417 2000 2217 "0" "_13_" "square"]
Pad[-9724 -1574 -7992 -1574 1417 2000 2217 "0" "_14_" "square"]
Pad[-9724 1574 -7992 1574 1417 2000 2217 "0" "_15_" "square"]
Pad[-9724 4724 -7992 4724 1417 2000 2217 "0" "_16_" "square"]
Pad[7992 4724 9724 4724 1417 2000 2217 "0" "_5_" "square"]
)
