# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lfcsp-48
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lfcsp-48
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 52
#
Element["" " U    " "" "" 0 0 0 -17720 0 100 ""]
(
ElementLine[-14170 11420 -14170 14170 500]
ElementLine[-14170 14170 -11420 14170 500]
ElementLine[-11420 14170 -14170 11420 500]
ElementLine[-12200 13780 -13780 12200 500]
ElementLine[-13780 12200 -13780 12600 500]
ElementLine[-13780 12600 -12600 13780 500]
ElementLine[-12600 13780 -12990 13780 500]
ElementLine[-12990 13780 -13780 12990 500]
ElementLine[-13780 11810 -13780 13780 500]
ElementLine[-13780 13780 -11810 13780 500]
ElementLine[-11810 13780 11810 13780 500]
ElementLine[11810 13780 13780 11810 500]
ElementLine[13780 11810 13780 -11810 500]
ElementLine[13780 -11810 11810 -13780 500]
ElementLine[11810 -13780 -11810 -13780 500]
ElementLine[-11810 -13780 -13780 -11810 500]
ElementLine[-13780 -11810 -13780 11810 500]
ElementArc[-10620 10630 877 877 0 360 790]
Pad[-10830 12645 -10830 14215 1260 2000 2060 "0" "_1_" "square"]
Pad[-8860 12645 -8860 14215 1260 2000 2060 "0" "_2_" "square"]
Pad[-4920 12645 -4920 14215 1260 2000 2060 "0" "_4_" "square"]
Pad[-2950 12645 -2950 14215 1260 2000 2060 "0" "_5_" "square"]
Pad[-980 12645 -980 14215 1260 2000 2060 "0" "_6_" "square"]
Pad[980 12645 980 14215 1260 2000 2060 "0" "_7_" "square"]
Pad[12645 8860 14215 8860 1260 2000 2060 "0" "_14_" "square"]
Pad[12645 6890 14215 6890 1260 2000 2060 "0" "_15_" "square"]
Pad[12645 4920 14215 4920 1260 2000 2060 "0" "_16_" "square"]
Pad[12645 2950 14215 2950 1260 2000 2060 "0" "_17_" "square"]
Pad[12645 980 14215 980 1260 2000 2060 "0" "_18_" "square"]
Pad[12645 -980 14215 -980 1260 2000 2060 "0" "_19_" "square"]
Pad[12645 -2950 14215 -2950 1260 2000 2060 "0" "_20_" "square"]
Pad[-6890 12645 -6890 14215 1260 2000 2060 "0" "_3_" "square"]
Pad[4620 4620 4620 4620 9250 2000 10050 "0" "_49_" "square"]
Pad[-4620 4620 -4620 4620 9250 2000 10050 "0" "_49_" "square"]
Pad[-4620 -4620 -4620 -4620 9250 2000 10050 "0" "_49_" "square"]
Pad[4620 -4620 4620 -4620 9250 2000 10050 "0" "_49_" "square"]
Pad[10830 -14215 10830 -12645 1260 2000 2060 "0" "_25_" "square"]
Pad[8860 -14215 8860 -12645 1260 2000 2060 "0" "_26_" "square"]
Pad[6890 -14215 6890 -12645 1260 2000 2060 "0" "_27_" "square"]
Pad[4920 -14215 4920 -12645 1260 2000 2060 "0" "_28_" "square"]
Pad[2950 -14215 2950 -12645 1260 2000 2060 "0" "_29_" "square"]
Pad[980 -14215 980 -12645 1260 2000 2060 "0" "_30_" "square"]
Pad[-980 -14215 -980 -12645 1260 2000 2060 "0" "_31_" "square"]
Pad[-14215 -10830 -12645 -10830 1260 2000 2060 "0" "_37_" "square"]
Pad[-14215 -8860 -12645 -8860 1260 2000 2060 "0" "_38_" "square"]
Pad[-14215 -6890 -12645 -6890 1260 2000 2060 "0" "_39_" "square"]
Pad[-14215 -4920 -12645 -4920 1260 2000 2060 "0" "_40_" "square"]
Pad[-14215 -2950 -12645 -2950 1260 2000 2060 "0" "_41_" "square"]
Pad[-14215 -980 -12645 -980 1260 2000 2060 "0" "_42_" "square"]
Pad[2950 12645 2950 14215 1260 2000 2060 "0" "_8_" "square"]
Pad[4920 12645 4920 14215 1260 2000 2060 "0" "_9_" "square"]
Pad[6890 12645 6890 14215 1260 2000 2060 "0" "_10_" "square"]
Pad[8860 12645 8860 14215 1260 2000 2060 "0" "_11_" "square"]
Pad[12645 10830 14215 10830 1260 2000 2060 "0" "_13_" "square"]
Pad[12645 -4920 14215 -4920 1260 2000 2060 "0" "_21_" "square"]
Pad[12645 -6890 14215 -6890 1260 2000 2060 "0" "_22_" "square"]
Pad[-2950 -14215 -2950 -12645 1260 2000 2060 "0" "_32_" "square"]
Pad[-4920 -14215 -4920 -12645 1260 2000 2060 "0" "_33_" "square"]
Pad[-14215 980 -12645 980 1260 2000 2060 "0" "_43_" "square"]
Pad[-14205 2950 -12635 2950 1250 2000 2050 "0" "_44_" "square"]
Pad[10820 12635 10820 14205 1250 2000 2050 "0" "_12_" "square"]
Pad[12635 -8850 14205 -8850 1250 2000 2050 "0" "_23_" "square"]
Pad[12635 -10820 14205 -10820 1250 2000 2050 "0" "_24_" "square"]
Pad[-6890 -14205 -6890 -12635 1250 2000 2050 "0" "_34_" "square"]
Pad[-8850 -14205 -8850 -12635 1250 2000 2050 "0" "_35_" "square"]
Pad[-10820 -14205 -10820 -12635 1250 2000 2050 "0" "_36_" "square"]
Pad[-14205 4920 -12635 4920 1250 2000 2050 "0" "_45_" "square"]
Pad[-14205 6890 -12635 6890 1250 2000 2050 "0" "_46_" "square"]
Pad[-14205 8850 -12635 8850 1250 2000 2050 "0" "_47_" "square"]
Pad[-14205 10820 -12635 10820 1250 2000 2050 "0" "_48_" "square"]
)
