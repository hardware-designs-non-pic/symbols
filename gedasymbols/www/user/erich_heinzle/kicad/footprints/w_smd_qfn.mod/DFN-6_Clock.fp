# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module DFN-6_Clock
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: DFN-6_Clock
# Text descriptor count: 1
# Draw segment object count: 45
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 6
#
Element["" " U    " "" "" 0 0 0 -5510 0 100 ""]
(
ElementLine[21000 -11000 21000 10000 2000]
ElementLine[21000 10000 21000 11000 2000]
ElementLine[21000 11000 -21000 11000 2000]
ElementLine[-21000 11000 -21000 -11000 2000]
ElementLine[-21000 -11000 21000 -11000 2000]
ElementArc[16000 -6000 1414 1414 0 360 2000]
ElementLine[-12000 17500 -12000 15000 2000]
ElementLine[-12000 15000 -8000 15000 2000]
ElementLine[-8000 15000 -8000 17500 2000]
ElementLine[-8000 17500 -2000 17500 2000]
ElementLine[-2000 17500 -2000 15000 2000]
ElementLine[-2000 15000 2000 15000 2000]
ElementLine[2000 15000 2000 17500 2000]
ElementLine[2000 17500 8000 17500 2000]
ElementLine[8000 17500 8000 15000 2000]
ElementLine[8000 15000 12000 15000 2000]
ElementLine[12000 15000 12000 17500 2000]
ElementLine[12000 17500 18000 17500 2000]
ElementLine[18000 17500 18000 15000 2000]
ElementLine[18000 15000 22000 15000 2000]
ElementLine[22000 15000 22000 17500 2000]
ElementLine[22000 17500 25000 17500 2000]
ElementLine[25000 17500 27500 15000 2000]
ElementLine[27500 15000 27500 -15000 2000]
ElementLine[27500 -15000 25000 -17500 2000]
ElementLine[25000 -17500 22000 -17500 2000]
ElementLine[22000 -17500 22000 -15000 2000]
ElementLine[22000 -15000 18000 -15000 2000]
ElementLine[18000 -15000 18000 -17500 2000]
ElementLine[18000 -17500 12000 -17500 2000]
ElementLine[12000 -17500 12000 -15000 2000]
ElementLine[12000 -15000 8000 -15000 2000]
ElementLine[8000 -15000 8000 -17500 2000]
ElementLine[8000 -17500 2000 -17500 2000]
ElementLine[2000 -17500 2000 -15000 2000]
ElementLine[2000 -15000 -2000 -15000 2000]
ElementLine[-2000 -15000 -2000 -17500 2000]
ElementLine[-2000 -17500 -8000 -17500 2000]
ElementLine[-8000 -17500 -8000 -15000 2000]
ElementLine[-8000 -15000 -12000 -15000 2000]
ElementLine[-12000 -15000 -12000 -17500 2000]
ElementLine[-12000 -17500 -25000 -17500 2000]
ElementLine[-25000 -17500 -27500 -15000 2000]
ElementLine[-27500 -15000 -27500 15000 2000]
ElementLine[-27500 15000 -25000 17500 2000]
ElementLine[-25000 17500 -12000 17500 2000]
Pad[10000 -15750 10000 -13250 6000 2000 6800 "0" "_1_" "square"]
Pad[0 -15750 0 -13250 6000 2000 6800 "0" "_2_" "square"]
Pad[-10000 -15750 -10000 -13250 6000 2000 6800 "0" "_3_" "square"]
Pad[-10000 13250 -10000 15750 6000 2000 6800 "0" "_4_" "square"]
Pad[0 13250 0 15750 6000 2000 6800 "0" "_5_" "square"]
Pad[10000 13250 10000 15750 6000 2000 6800 "0" "_6_" "square"]
)
