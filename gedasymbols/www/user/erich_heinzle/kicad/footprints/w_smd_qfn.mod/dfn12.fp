# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn12
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn12
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 13
#
Element["" " dfn12 " "" "" 0 0 0 -9050 0 100 ""]
(
ElementLine[-5120 6300 -6300 5120 500]
ElementLine[-5120 6300 -6300 6300 500]
ElementLine[-6300 6300 -6300 5120 500]
ElementArc[-3939 3540 877 877 0 360 780]
ElementLine[-5910 -5910 5910 -5910 500]
ElementLine[5910 -5910 5910 5910 500]
ElementLine[5910 5910 -5910 5910 500]
ElementLine[-5910 5910 -5910 -5910 500]
Pad[-4420 4620 -4420 6400 970 2000 1770 "0" "_1_" "square"]
Pad[-2650 4620 -2650 6400 970 2000 1770 "0" "_2_" "square"]
Pad[-870 4620 -870 6400 970 2000 1770 "0" "_3_" "square"]
Pad[870 4620 870 6400 970 2000 1770 "0" "_4_" "square"]
Pad[870 -6400 870 -4620 970 2000 1770 "0" "_9_" "square"]
Pad[-870 -6400 -870 -4620 970 2000 1770 "0" "_10_" "square"]
Pad[-2650 -6400 -2650 -4620 970 2000 1770 "0" "_11_" "square"]
Pad[-4420 -6400 -4420 -4620 970 2000 1770 "0" "_12_" "square"]
Pad[-1440 0 1440 0 6490 2000 7290 "0" "_13_" "square"]
Pad[2650 4620 2650 6400 970 2000 1770 "0" "_5_" "square"]
Pad[4420 4620 4420 6400 970 2000 1770 "0" "_6_" "square"]
Pad[2650 -6400 2650 -4620 970 2000 1770 "0" "_8_" "square"]
Pad[4420 -6400 4420 -4620 970 2000 1770 "0" "_7_" "square"]
)
