# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n40
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n40
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 49
#
Element["" " U    " "" "" 0 0 0 -15350 0 100 ""]
(
ElementLine[-12200 9450 -12200 12200 500]
ElementLine[-12200 12200 -9450 12200 500]
ElementLine[-9450 12200 -12200 9450 500]
ElementLine[-11810 -11810 11810 -11810 500]
ElementLine[11810 -11810 11810 11810 500]
ElementLine[11810 11810 -11810 11810 500]
ElementLine[-11810 11810 -11810 -11810 500]
ElementLine[-10240 11820 -11810 10250 500]
ElementLine[-11810 10630 -10630 11810 500]
ElementLine[-11010 11810 -11800 11020 500]
ElementArc[-9440 9460 877 877 0 360 790]
Pad[-6880 10580 -6880 12820 1100 2000 1900 "0" "_2_" "square"]
Pad[-4920 10580 -4920 12820 1100 2000 1900 "0" "_3_" "square"]
Pad[-2950 10580 -2950 12820 1100 2000 1900 "0" "_4_" "square"]
Pad[-980 10580 -980 12820 1100 2000 1900 "0" "_5_" "square"]
Pad[4920 10580 4920 12820 1100 2000 1900 "0" "_8_" "square"]
Pad[6880 10580 6880 12820 1100 2000 1900 "0" "_9_" "square"]
Pad[8850 10580 8850 12820 1100 2000 1900 "0" "_10_" "square"]
Pad[10580 8850 12820 8850 1100 2000 1900 "0" "_11_" "square"]
Pad[10580 6880 12820 6880 1100 2000 1900 "0" "_12_" "square"]
Pad[10580 4920 12820 4920 1100 2000 1900 "0" "_13_" "square"]
Pad[10580 2950 12820 2950 1100 2000 1900 "0" "_14_" "square"]
Pad[-8850 10580 -8850 12820 1100 2000 1900 "0" "_1_" "square"]
Pad[0 0 0 0 5900 2000 6700 "0" "_41_" "square"]
Pad[10580 980 12820 980 1100 2000 1900 "0" "_15_" "square"]
Pad[10580 -980 12820 -980 1100 2000 1900 "0" "_16_" "square"]
Pad[10580 -6880 12820 -6880 1100 2000 1900 "0" "_19_" "square"]
Pad[10580 -8850 12820 -8850 1100 2000 1900 "0" "_20_" "square"]
Pad[8850 -12820 8850 -10580 1100 2000 1900 "0" "_21_" "square"]
Pad[6880 -12820 6880 -10580 1100 2000 1900 "0" "_22_" "square"]
Pad[4920 -12820 4920 -10580 1100 2000 1900 "0" "_23_" "square"]
Pad[2950 -12820 2950 -10580 1100 2000 1900 "0" "_24_" "square"]
Pad[980 -12820 980 -10580 1100 2000 1900 "0" "_25_" "square"]
Pad[-980 -12820 -980 -10580 1100 2000 1900 "0" "_26_" "square"]
Pad[-2950 -12820 -2950 -10580 1100 2000 1900 "0" "_27_" "square"]
Pad[-4920 -12820 -4920 -10580 1100 2000 1900 "0" "_28_" "square"]
Pad[980 10580 980 12820 1100 2000 1900 "0" "_6_" "square"]
Pad[2950 10580 2950 12820 1100 2000 1900 "0" "_7_" "square"]
Pad[-6880 -12820 -6880 -10580 1100 2000 1900 "0" "_29_" "square"]
Pad[-8850 -12820 -8850 -10580 1100 2000 1900 "0" "_30_" "square"]
Pad[-12820 -8850 -10580 -8850 1100 2000 1900 "0" "_31_" "square"]
Pad[-12820 -6880 -10580 -6880 1100 2000 1900 "0" "_32_" "square"]
Pad[10580 -2950 12820 -2950 1100 2000 1900 "0" "_17_" "square"]
Pad[10580 -4920 12820 -4920 1100 2000 1900 "0" "_18_" "square"]
Pad[-12820 -4920 -10580 -4920 1100 2000 1900 "0" "_33_" "square"]
Pad[-12820 -2950 -10580 -2950 1100 2000 1900 "0" "_34_" "square"]
Pad[-12820 -970 -10580 -970 1100 2000 1900 "0" "_35_" "square"]
Pad[-12820 970 -10580 970 1100 2000 1900 "0" "_36_" "square"]
Pad[-12820 2950 -10580 2950 1100 2000 1900 "0" "_37_" "square"]
Pad[-12820 4920 -10580 4920 1100 2000 1900 "0" "_38_" "square"]
Pad[-12820 6870 -10580 6870 1100 2000 1900 "0" "_39_" "square"]
Pad[-12820 8850 -10580 8850 1100 2000 1900 "0" "_40_" "square"]
Pad[-5900 0 -5900 0 5900 2000 6700 "0" "_41_" "square"]
Pad[5900 0 5900 0 5900 2000 6700 "0" "_41_" "square"]
Pad[-5900 -5900 -5900 -5900 5900 2000 6700 "0" "_41_" "square"]
Pad[0 -5900 0 -5900 5900 2000 6700 "0" "_41_" "square"]
Pad[5900 -5900 5900 -5900 5900 2000 6700 "0" "_41_" "square"]
Pad[-5900 5900 -5900 5900 5900 2000 6700 "0" "_41_" "square"]
Pad[0 5900 0 5900 5900 2000 6700 "0" "_41_" "square"]
Pad[5900 5900 5900 5900 5900 2000 6700 "0" "_41_" "square"]
)
