# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lga-24
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lga-24
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 24
#
Element["" " U    " "" "" 0 0 0 -10236 0 100 ""]
(
ElementLine[-8661 4724 -8661 8661 590]
ElementLine[-8661 8661 -4724 8661 590]
ElementLine[-9055 4330 -4330 9055 590]
ElementLine[-4330 9055 -9055 9055 590]
ElementLine[-9055 9055 -9055 4330 590]
ElementLine[-8270 5120 -8270 8270 500]
ElementLine[-8270 8270 -5120 8270 500]
ElementLine[-7870 5910 -5910 7870 500]
ElementLine[-7870 6300 -6300 7870 500]
ElementLine[-6300 7870 -6690 7870 500]
ElementLine[-6690 7870 -7870 6690 500]
ElementLine[-7870 6690 -7870 7090 500]
ElementLine[-7870 7090 -7090 7870 500]
ElementLine[-7870 5910 -7870 6690 500]
ElementLine[-7870 5510 -7870 6300 500]
ElementLine[-7870 7870 -7870 -7870 500]
ElementLine[-7870 -7870 7870 -7870 500]
ElementLine[7870 -7870 7870 7870 500]
ElementLine[7870 7870 -7870 7870 500]
ElementArc[-5520 5500 877 877 0 360 780]
Pad[-4921 6496 -4921 7086 1181 2000 1981 "0" "_1_" "square"]
Pad[-2952 6496 -2952 7086 1181 2000 1981 "0" "_2_" "square"]
Pad[-984 6496 -984 7086 1181 2000 1981 "0" "_3_" "square"]
Pad[984 6496 984 7086 1181 2000 1981 "0" "_4_" "square"]
Pad[4921 6496 4921 7086 1181 2000 1981 "0" "_6_" "square"]
Pad[6496 4921 7086 4921 1181 2000 1981 "0" "_7_" "square"]
Pad[6496 2952 7086 2952 1181 2000 1981 "0" "_8_" "square"]
Pad[6496 984 7086 984 1181 2000 1981 "0" "_9_" "square"]
Pad[6496 -984 7086 -984 1181 2000 1981 "0" "_10_" "square"]
Pad[6496 -2952 7086 -2952 1181 2000 1981 "0" "_11_" "square"]
Pad[6496 -4921 7086 -4921 1181 2000 1981 "0" "_12_" "square"]
Pad[4921 -7086 4921 -6496 1181 2000 1981 "0" "_13_" "square"]
Pad[2952 -7086 2952 -6496 1181 2000 1981 "0" "_14_" "square"]
Pad[984 -7086 984 -6496 1181 2000 1981 "0" "_15_" "square"]
Pad[-984 -7086 -984 -6496 1181 2000 1981 "0" "_16_" "square"]
Pad[-2952 -7086 -2952 -6496 1181 2000 1981 "0" "_17_" "square"]
Pad[-4921 -7086 -4921 -6496 1181 2000 1981 "0" "_18_" "square"]
Pad[-7086 -4921 -6496 -4921 1181 2000 1981 "0" "_19_" "square"]
Pad[-7086 -2952 -6496 -2952 1181 2000 1981 "0" "_20_" "square"]
Pad[2952 6496 2952 7086 1181 2000 1981 "0" "_5_" "square"]
Pad[-7086 -984 -6496 -984 1181 2000 1981 "0" "_21_" "square"]
Pad[-7086 984 -6496 984 1181 2000 1981 "0" "_22_" "square"]
Pad[-7086 2952 -6496 2952 1181 2000 1981 "0" "_23_" "square"]
Pad[-7086 4921 -6496 4921 1181 2000 1981 "0" "_24_" "square"]
)
