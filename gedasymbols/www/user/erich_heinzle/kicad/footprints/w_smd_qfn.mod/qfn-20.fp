# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module qfn-20
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: qfn-20
# Text descriptor count: 1
# Draw segment object count: 15
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 24
#
Element["" " U    " "" "" 0 0 0 -11420 0 100 ""]
(
ElementLine[-5120 8270 -8270 5120 500]
ElementLine[-8270 5120 -8270 8270 500]
ElementLine[-8270 8270 -5120 8270 500]
ElementLine[-7870 5910 -5910 7870 500]
ElementLine[-7870 6300 -6300 7870 500]
ElementLine[-6300 7870 -6690 7870 500]
ElementLine[-6690 7870 -7870 6690 500]
ElementLine[-7870 6690 -7870 7090 500]
ElementLine[-7870 7090 -7090 7870 500]
ElementLine[-7870 5910 -7870 6690 500]
ElementLine[-7870 5510 -7870 6300 500]
ElementLine[-7870 7870 -7870 -7870 500]
ElementLine[-7870 -7870 7870 -7870 500]
ElementLine[7870 -7870 7870 7870 500]
ElementLine[7870 7870 -7870 7870 500]
ElementArc[-5520 5500 877 877 0 360 780]
Pad[-3939 6895 -3939 8585 1180 2000 1980 "0" "_1_" "square"]
Pad[-1969 6895 -1969 8585 1180 2000 1980 "0" "_2_" "square"]
Pad[0 6895 0 8585 1180 2000 1980 "0" "_3_" "square"]
Pad[1969 6895 1969 8585 1180 2000 1980 "0" "_4_" "square"]
Pad[6895 3939 8585 3939 1180 2000 1980 "0" "_6_" "square"]
Pad[6895 1969 8585 1969 1180 2000 1980 "0" "_7_" "square"]
Pad[6895 0 8585 0 1180 2000 1980 "0" "_8_" "square"]
Pad[6895 -1969 8585 -1969 1180 2000 1980 "0" "_9_" "square"]
Pad[6895 -3939 8585 -3939 1180 2000 1980 "0" "_10_" "square"]
Pad[2460 2460 2460 2460 4920 2000 5720 "0" "_21_" "square"]
Pad[-2460 2460 -2460 2460 4920 2000 5720 "0" "_21_" "square"]
Pad[-2460 -2460 -2460 -2460 4920 2000 5720 "0" "_21_" "square"]
Pad[2460 -2460 2460 -2460 4920 2000 5720 "0" "_21_" "square"]
Pad[3939 -8585 3939 -6895 1180 2000 1980 "0" "_11_" "square"]
Pad[1969 -8585 1969 -6895 1180 2000 1980 "0" "_12_" "square"]
Pad[0 -8585 0 -6895 1180 2000 1980 "0" "_13_" "square"]
Pad[-1969 -8585 -1969 -6895 1180 2000 1980 "0" "_14_" "square"]
Pad[-3939 -8585 -3939 -6895 1180 2000 1980 "0" "_15_" "square"]
Pad[-8585 -3939 -6895 -3939 1180 2000 1980 "0" "_16_" "square"]
Pad[-8585 -1969 -6895 -1969 1180 2000 1980 "0" "_17_" "square"]
Pad[-8585 0 -6895 0 1180 2000 1980 "0" "_18_" "square"]
Pad[-8585 1969 -6895 1969 1180 2000 1980 "0" "_19_" "square"]
Pad[-8585 3939 -6895 3939 1180 2000 1980 "0" "_20_" "square"]
Pad[3939 6895 3939 8585 1180 2000 1980 "0" "_5_" "square"]
)
