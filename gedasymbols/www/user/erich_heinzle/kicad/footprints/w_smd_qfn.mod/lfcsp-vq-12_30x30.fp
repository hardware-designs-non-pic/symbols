# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lfcsp-vq-12_30x30
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lfcsp-vq-12_30x30
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 13
#
Element["" " lfcsp-vq-12 30x30 " "" "" 0 0 0 -7480 0 100 ""]
(
ElementLine[-6300 4330 -6300 6300 500]
ElementLine[-6300 6300 -4330 6300 500]
ElementLine[-4330 6300 -6300 4330 500]
ElementLine[-5120 5910 -5910 5120 500]
ElementLine[-5910 4720 -5910 5910 500]
ElementLine[-5910 5910 -4720 5910 500]
ElementLine[-5910 4720 -5910 -4720 500]
ElementLine[-5910 -4720 -4720 -5910 500]
ElementLine[-4720 -5910 4720 -5910 500]
ElementLine[4720 -5910 5910 -4720 500]
ElementLine[5910 -4720 5910 4720 500]
ElementLine[5910 4720 4720 5910 500]
ElementLine[4720 5910 -4720 5910 500]
ElementArc[-4330 4330 559 559 0 360 500]
Pad[-1969 4900 -1969 5900 1170 2000 1970 "0" "_1_" "square"]
Pad[0 0 0 0 6500 2000 7300 "0" "_13_" "square"]
Pad[0 4900 0 5900 1170 2000 1970 "0" "_2_" "square"]
Pad[1969 4900 1969 5900 1170 2000 1970 "0" "_3_" "square"]
Pad[4900 1969 5900 1969 1170 2000 1970 "0" "_4_" "square"]
Pad[4900 0 5900 0 1170 2000 1970 "0" "_5_" "square"]
Pad[4900 -1969 5900 -1969 1170 2000 1970 "0" "_6_" "square"]
Pad[1969 -5900 1969 -4900 1170 2000 1970 "0" "_7_" "square"]
Pad[0 -5900 0 -4900 1170 2000 1970 "0" "_8_" "square"]
Pad[-1969 -5900 -1969 -4900 1170 2000 1970 "0" "_9_" "square"]
Pad[-5900 -1969 -4900 -1969 1170 2000 1970 "0" "_10_" "square"]
Pad[-5900 0 -4900 0 1170 2000 1970 "0" "_11_" "square"]
Pad[-5900 1969 -4900 1969 1170 2000 1970 "0" "_12_" "square"]
)
