# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n16
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n16
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 22
#
Element["" " pvqfn-n16 " "" "" 0 0 0 -10630 0 100 ""]
(
ElementLine[-8270 5910 -8270 7480 500]
ElementLine[-8270 7480 -6690 7480 500]
ElementLine[-6690 7480 -8270 5910 500]
ElementLine[-7870 -7090 7870 -7090 500]
ElementLine[7870 7090 -7870 7090 500]
ElementArc[-6700 5910 400 400 0 360 500]
ElementLine[7870 -7090 7870 7090 500]
ElementLine[-7870 7090 -7870 -7090 500]
Pad[-4920 5390 -4920 7990 940 2000 1740 "0" "_2_" "square"]
Pad[-2950 5390 -2950 7990 940 2000 1740 "0" "_3_" "square"]
Pad[-980 5390 -980 7990 940 2000 1740 "0" "_4_" "square"]
Pad[980 5390 980 7990 940 2000 1740 "0" "_5_" "square"]
Pad[2950 5390 2950 7990 940 2000 1740 "0" "_6_" "square"]
Pad[6380 2950 8980 2950 940 2000 1740 "0" "_8_" "square"]
Pad[6380 -2950 8980 -2950 940 2000 1740 "0" "_9_" "square"]
Pad[4920 -7990 4920 -5390 940 2000 1740 "0" "_10_" "square"]
Pad[2950 -7990 2950 -5390 940 2000 1740 "0" "_11_" "square"]
Pad[980 -7990 980 -5390 940 2000 1740 "0" "_12_" "square"]
Pad[-980 -7990 -980 -5390 940 2000 1740 "0" "_13_" "square"]
Pad[-2950 -7990 -2950 -5390 940 2000 1740 "0" "_14_" "square"]
Pad[-8980 -2950 -6380 -2950 940 2000 1740 "0" "_16_" "square"]
Pad[-8980 2950 -6380 2950 940 2000 1740 "0" "_1_" "square"]
Pad[2950 1969 2950 1969 3939 2000 4739 "0" "_17_" "square"]
Pad[-2950 1969 -2950 1969 3939 2000 4739 "0" "_17_" "square"]
Pad[-2950 -1969 -2950 -1969 3939 2000 4739 "0" "_17_" "square"]
Pad[2950 -1969 2950 -1969 3939 2000 4739 "0" "_17_" "square"]
Pad[4920 5390 4920 7990 940 2000 1740 "0" "_7_" "square"]
Pad[-4920 -7990 -4920 -5390 940 2000 1740 "0" "_15_" "square"]
Pad[0 -2954 0 -984 1969 2000 2769 "0" "_17_" "square"]
Pad[0 984 0 2954 1969 2000 2769 "0" "_17_" "square"]
)
