# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n14
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n14
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 18
#
Element["" " pvqfn-n14 " "" "" 0 0 0 -10630 0 100 ""]
(
ElementLine[-7480 5910 -7480 7480 500]
ElementLine[-7480 7480 -5910 7480 500]
ElementLine[-5910 7480 -7480 5910 500]
ElementArc[-5900 5910 400 400 0 360 500]
ElementLine[-7090 -7090 7090 -7090 500]
ElementLine[7090 -7090 7090 7090 500]
ElementLine[7090 7090 -7090 7090 500]
ElementLine[-7090 7090 -7090 -7090 500]
Pad[-3939 5390 -3939 7990 940 2000 1740 "0" "_2_" "square"]
Pad[-1969 5390 -1969 7990 940 2000 1740 "0" "_3_" "square"]
Pad[0 5390 0 7990 940 2000 1740 "0" "_4_" "square"]
Pad[1969 5390 1969 7990 940 2000 1740 "0" "_5_" "square"]
Pad[3939 5390 3939 7990 940 2000 1740 "0" "_6_" "square"]
Pad[5390 2950 7990 2950 940 2000 1740 "0" "_7_" "square"]
Pad[5390 -2950 7990 -2950 940 2000 1740 "0" "_8_" "square"]
Pad[3939 -7990 3939 -5390 940 2000 1740 "0" "_9_" "square"]
Pad[1969 -7990 1969 -5390 940 2000 1740 "0" "_10_" "square"]
Pad[0 -7990 0 -5390 940 2000 1740 "0" "_11_" "square"]
Pad[-1969 -7990 -1969 -5390 940 2000 1740 "0" "_12_" "square"]
Pad[-3939 -7990 -3939 -5390 940 2000 1740 "0" "_13_" "square"]
Pad[-7990 -2950 -5390 -2950 940 2000 1740 "0" "_14_" "square"]
Pad[-7990 2950 -5390 2950 940 2000 1740 "0" "_1_" "square"]
Pad[1969 1969 1969 1969 3939 2000 4739 "0" "_15_" "square"]
Pad[-1969 1969 -1969 1969 3939 2000 4739 "0" "_15_" "square"]
Pad[-1969 -1969 -1969 -1969 3939 2000 4739 "0" "_15_" "square"]
Pad[1969 -1969 1969 -1969 3939 2000 4739 "0" "_15_" "square"]
)
