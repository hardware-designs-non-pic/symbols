# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module qfn-44
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: qfn-44
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 48
#
Element["" " U    " "" "" 0 0 0 -19290 0 100 ""]
(
ElementLine[-15750 14170 -16139 14170 500]
ElementLine[-16139 14170 -16139 16139 500]
ElementLine[-16139 16139 -14170 16139 500]
ElementLine[-14170 16139 -14170 15750 500]
ElementLine[-15750 14170 -14170 15750 500]
ElementLine[-14170 15750 -14570 15750 500]
ElementLine[-14570 15750 -15750 14570 500]
ElementLine[-15750 14570 -15750 14960 500]
ElementLine[-15750 14960 -14960 15750 500]
ElementArc[-12200 12200 877 877 0 360 790]
ElementLine[-15750 15750 15750 15750 790]
ElementLine[15750 15750 15750 -15750 790]
ElementLine[15750 -15750 -15750 -15750 790]
ElementLine[-15750 -15750 -15750 15750 790]
Pad[-12800 14865 -12800 16635 1380 2000 2180 "0" "_1_" "square"]
Pad[-10240 14865 -10240 16635 1380 2000 2180 "0" "_2_" "square"]
Pad[-5120 14865 -5120 16635 1380 2000 2180 "0" "_4_" "square"]
Pad[-2560 14865 -2560 16635 1380 2000 2180 "0" "_5_" "square"]
Pad[0 14865 0 16635 1380 2000 2180 "0" "_6_" "square"]
Pad[2560 14865 2560 16635 1380 2000 2180 "0" "_7_" "square"]
Pad[14865 7680 16635 7680 1380 2000 2180 "0" "_14_" "square"]
Pad[14865 5120 16635 5120 1380 2000 2180 "0" "_15_" "square"]
Pad[14865 2560 16635 2560 1380 2000 2180 "0" "_16_" "square"]
Pad[14865 0 16635 0 1380 2000 2180 "0" "_17_" "square"]
Pad[14865 -2560 16635 -2560 1380 2000 2180 "0" "_18_" "square"]
Pad[14865 -5120 16635 -5120 1380 2000 2180 "0" "_19_" "square"]
Pad[14865 -7680 16635 -7680 1380 2000 2180 "0" "_20_" "square"]
Pad[-7680 14865 -7680 16635 1380 2000 2180 "0" "_3_" "square"]
Pad[6400 6400 6400 6400 12800 2000 13600 "0" "_45_" "square"]
Pad[-6400 6400 -6400 6400 12800 2000 13600 "0" "_45_" "square"]
Pad[-6400 -6400 -6400 -6400 12800 2000 13600 "0" "_45_" "square"]
Pad[6400 -6400 6400 -6400 12800 2000 13600 "0" "_45_" "square"]
Pad[7680 -16635 7680 -14865 1380 2000 2180 "0" "_25_" "square"]
Pad[5120 -16635 5120 -14865 1380 2000 2180 "0" "_26_" "square"]
Pad[2560 -16635 2560 -14865 1380 2000 2180 "0" "_27_" "square"]
Pad[0 -16635 0 -14865 1380 2000 2180 "0" "_28_" "square"]
Pad[-2560 -16635 -2560 -14865 1380 2000 2180 "0" "_29_" "square"]
Pad[-5120 -16635 -5120 -14865 1380 2000 2180 "0" "_30_" "square"]
Pad[-7680 -16635 -7680 -14865 1380 2000 2180 "0" "_31_" "square"]
Pad[-16635 -7680 -14865 -7680 1380 2000 2180 "0" "_36_" "square"]
Pad[-16635 -5120 -14865 -5120 1380 2000 2180 "0" "_37_" "square"]
Pad[-16635 -2560 -14865 -2560 1380 2000 2180 "0" "_38_" "square"]
Pad[-16635 0 -14865 0 1380 2000 2180 "0" "_39_" "square"]
Pad[-16635 2560 -14865 2560 1380 2000 2180 "0" "_40_" "square"]
Pad[-16635 5120 -14865 5120 1380 2000 2180 "0" "_41_" "square"]
Pad[-16635 7680 -14865 7680 1380 2000 2180 "0" "_42_" "square"]
Pad[5120 14865 5120 16635 1380 2000 2180 "0" "_8_" "square"]
Pad[7680 14865 7680 16635 1380 2000 2180 "0" "_9_" "square"]
Pad[10240 14865 10240 16635 1380 2000 2180 "0" "_10_" "square"]
Pad[12800 14865 12800 16635 1380 2000 2180 "0" "_11_" "square"]
Pad[14865 12800 16635 12800 1380 2000 2180 "0" "_12_" "square"]
Pad[14865 10240 16635 10240 1380 2000 2180 "0" "_13_" "square"]
Pad[14865 -10240 16635 -10240 1380 2000 2180 "0" "_21_" "square"]
Pad[14865 -12800 16635 -12800 1380 2000 2180 "0" "_22_" "square"]
Pad[12800 -16635 12800 -14865 1380 2000 2180 "0" "_23_" "square"]
Pad[10240 -16635 10240 -14865 1380 2000 2180 "0" "_24_" "square"]
Pad[-10240 -16635 -10240 -14865 1380 2000 2180 "0" "_32_" "square"]
Pad[-12800 -16635 -12800 -14865 1380 2000 2180 "0" "_33_" "square"]
Pad[-16635 -12800 -14865 -12800 1380 2000 2180 "0" "_34_" "square"]
Pad[-16635 -10240 -14865 -10240 1380 2000 2180 "0" "_35_" "square"]
Pad[-16635 10240 -14865 10240 1380 2000 2180 "0" "_43_" "square"]
Pad[-16635 12800 -14865 12800 1380 2000 2180 "0" "_44_" "square"]
)
