# Kicad module converted to gEDA PCB with java utility written by Erich Heinzle
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module QFN-20-1EP_3x4mm_Pitch0.5mm
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: QFN-20-1EP_3x4mm_Pitch0.5mm
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 24
#
Element["" "REF  " "" "" 0 0 0 -12795 0 100 ""]
(
ElementLine[-7874 -9842 -7874 9842 196]
ElementLine[7874 -9842 7874 9842 196]
ElementLine[-7874 -9842 7874 -9842 196]
ElementLine[-7874 9842 7874 9842 196]
ElementLine[6397 -8366 6397 -6299 590]
ElementLine[-6397 8366 -6397 6299 590]
ElementLine[6397 8366 6397 6299 590]
ElementLine[-6397 -8366 -4330 -8366 590]
ElementLine[-6397 8366 -4330 8366 590]
ElementLine[6397 8366 4330 8366 590]
ElementLine[6397 -8366 4330 -8366 590]
Pad[-6397 -4921 -4625 -4921 984 2000 1784 "GND" "1" "square"]
Pad[-6397 -2952 -4625 -2952 984 2000 1784 "GND" "2" "square"]
Pad[-6397 -984 -4625 -984 984 2000 1784 "GND" "3" "square"]
Pad[-6397 984 -4625 984 984 2000 1784 "GND" "4" "square"]
Pad[-6397 2952 -4625 2952 984 2000 1784 "GND" "5" "square"]
Pad[-6397 4921 -4625 4921 984 2000 1784 "GND" "6" "square"]
Pad[-2952 6594 -2952 8366 984 2000 1784 "GND" "7" "square"]
Pad[-984 6594 -984 8366 984 2000 1784 "GND" "8" "square"]
Pad[984 6594 984 8366 984 2000 1784 "GND" "9" "square"]
Pad[2952 6594 2952 8366 984 2000 1784 "GND" "10" "square"]
Pad[4625 4921 6397 4921 984 2000 1784 "GND" "11" "square"]
Pad[4625 2952 6397 2952 984 2000 1784 "GND" "12" "square"]
Pad[4625 984 6397 984 984 2000 1784 "GND" "13" "square"]
Pad[4625 -984 6397 -984 984 2000 1784 "GND" "14" "square"]
Pad[4625 -2952 6397 -2952 984 2000 1784 "GND" "15" "square"]
Pad[4625 -4921 6397 -4921 984 2000 1784 "GND" "16" "square"]
Pad[2952 -8366 2952 -6594 984 2000 1784 "GND" "17" "square"]
Pad[984 -8366 984 -6594 984 2000 1784 "GND" "18" "square"]
Pad[-984 -8366 -984 -6594 984 2000 1784 "GND" "19" "square"]
Pad[-2952 -8366 -2952 -6594 984 2000 1784 "GND" "20" "square"]
Pad[1624 1624 1624 3592 3248 2000 4048 "GND" "21" "square"]
Pad[1624 -3592 1624 -1624 3248 2000 4048 "GND" "21" "square"]
Pad[-1624 1624 -1624 3592 3248 2000 4048 "GND" "21" "square"]
Pad[-1624 -3592 -1624 -1624 3248 2000 4048 "GND" "21" "square"]
)
