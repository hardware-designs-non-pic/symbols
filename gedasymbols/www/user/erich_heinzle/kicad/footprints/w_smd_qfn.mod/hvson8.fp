# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module hvson8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: hvson8
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 9
#
Element["" " hvson8 " "" "" 0 0 0 9842 0 100 ""]
(
ElementLine[-6302 3934 -6302 6294 500]
ElementLine[-6302 6294 -3932 6294 500]
ElementLine[-3932 6294 -6302 3934 500]
ElementArc[-3542 3540 390 390 0 360 500]
ElementArc[-3542 3540 790 790 0 360 500]
ElementLine[-4722 5904 -5902 4724 500]
ElementLine[-5902 5114 -5122 5904 500]
ElementLine[-5512 5904 -5902 5514 500]
ElementLine[-5905 -5905 5905 -5905 500]
ElementLine[5905 -5905 5905 5905 500]
ElementLine[5905 5905 -5905 5905 500]
ElementLine[-5905 5905 -5905 -5905 500]
Pad[-3838 4862 -3838 6240 1377 2000 2177 "0" "_1_" "square"]
Pad[-1279 4862 -1279 6240 1377 2000 2177 "0" "_2_" "square"]
Pad[1279 4862 1279 6240 1377 2000 2177 "0" "_3_" "square"]
Pad[3838 4862 3838 6240 1377 2000 2177 "0" "_4_" "square"]
Pad[3838 -6240 3838 -4862 1377 2000 2177 "0" "_5_" "square"]
Pad[1279 -6240 1279 -4862 1377 2000 2177 "0" "_6_" "square"]
Pad[-1279 -6240 -1279 -4862 1377 2000 2177 "0" "_7_" "square"]
Pad[-3838 -6240 -3838 -4862 1377 2000 2177 "0" "_8_" "square"]
Pad[-1574 0 1574 0 6692 2000 7492 "0" "_9_" "square"]
)
