# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn6-3x3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn6-3x3
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 7
#
Element["" " dfn6-3x3 " "" "" 0 0 0 -9842 0 100 ""]
(
ElementLine[-5905 2362 -2362 5905 590]
ElementLine[-2755 5905 -5905 2755 590]
ElementLine[-5905 3149 -3149 5905 590]
ElementLine[-5905 3543 -3543 5905 590]
ElementLine[-3937 5905 -5905 3937 590]
ElementLine[-5905 4724 -4724 5905 590]
ElementLine[-4330 5905 -5905 4330 590]
ElementLine[-5905 5118 -5118 5905 590]
ElementArc[-7872 5120 790 790 0 360 590]
ElementArc[-7882 5120 390 390 0 360 590]
ElementLine[-5905 -5905 5905 -5905 590]
ElementLine[-5905 5905 5905 5905 590]
ElementLine[5905 -5905 5905 5905 590]
ElementLine[-5905 5905 -5905 -5905 590]
Pad[-3937 4921 -3937 6496 1771 2000 2571 "0" "_1_" "square"]
Pad[0 4921 0 6496 1771 2000 2571 "0" "_2_" "square"]
Pad[3937 4921 3937 6496 1771 2000 2571 "0" "_3_" "square"]
Pad[3937 -6496 3937 -4921 1771 2000 2571 "0" "_4_" "square"]
Pad[0 -6496 0 -4921 1771 2000 2571 "0" "_5_" "square"]
Pad[-3937 -6496 -3937 -4921 1771 2000 2571 "0" "_6_" "square"]
Pad[-1771 0 1771 0 6299 2000 7099 "0" "_7_" "square"]
)
