# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module wson
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: wson
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 5
# Draw arc object count: 0
# Pad count: 9
#
Element["" " wson " "" "" 0 0 0 9842 0 100 ""]
(
ElementLine[-8271 5902 -8271 8262 500]
ElementLine[-8271 8262 -5901 8262 500]
ElementLine[-5901 8262 -8271 5902 500]
ElementArc[-5117 5115 390 390 0 360 500]
ElementArc[-5117 5115 790 790 0 360 500]
ElementArc[-5117 5115 1180 1180 0 360 500]
ElementArc[-5117 5115 1180 1180 0 360 500]
ElementLine[-6691 7872 -7871 6692 500]
ElementLine[-7871 7082 -7091 7872 500]
ElementLine[-7481 7872 -7871 7482 500]
ElementArc[-5117 5115 1420 1420 0 360 500]
ElementLine[-7874 -7874 7874 -7874 500]
ElementLine[7874 -7874 7874 7874 500]
ElementLine[7874 7874 -7874 7874 500]
ElementLine[-7874 7874 -7874 -7874 500]
Pad[-4724 6102 -4724 6889 1181 2000 1981 "0" "_1_" "square"]
Pad[-1574 6102 -1574 6889 1181 2000 1981 "0" "_2_" "square"]
Pad[1574 6102 1574 6889 1181 2000 1981 "0" "_3_" "square"]
Pad[4724 6102 4724 6889 1181 2000 1981 "0" "_4_" "square"]
Pad[4724 -6889 4724 -6102 1181 2000 1981 "0" "_5_" "square"]
Pad[1574 -6889 1574 -6102 1181 2000 1981 "0" "_6_" "square"]
Pad[-1574 -6889 -1574 -6102 1181 2000 1981 "0" "_7_" "square"]
Pad[-4724 -6889 -4724 -6102 1181 2000 1981 "0" "_8_" "square"]
Pad[-1574 0 1574 0 8661 2000 9461 "0" "_9_" "square"]
)
