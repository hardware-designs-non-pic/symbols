# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn8-2x3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn8-2x3
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 9
#
Element["" " dfn8-2x3 " "" "" 0 0 0 -9050 0 100 ""]
(
ElementArc[-5510 5120 790 790 0 360 500]
ElementArc[-5520 5120 390 390 0 360 500]
ElementLine[-3939 -5910 3939 -5910 500]
ElementLine[-3939 5910 3939 5910 500]
ElementLine[3939 -5910 3939 5910 500]
ElementLine[-3939 5910 -3939 -5910 500]
Pad[-2950 4810 -2950 6590 1170 2000 1970 "0" "_1_" "square"]
Pad[-970 4810 -970 6590 1170 2000 1970 "0" "_2_" "square"]
Pad[970 4810 970 6590 1170 2000 1970 "0" "_3_" "square"]
Pad[2950 4810 2950 6590 1170 2000 1970 "0" "_4_" "square"]
Pad[2950 -6590 2950 -4810 1170 2000 1970 "0" "_5_" "square"]
Pad[970 -6590 970 -4810 1170 2000 1970 "0" "_6_" "square"]
Pad[-970 -6590 -970 -4810 1170 2000 1970 "0" "_7_" "square"]
Pad[-2950 -6590 -2950 -4810 1170 2000 1970 "0" "_8_" "square"]
Pad[0 -590 0 590 5700 2000 6500 "0" "_9_" "square"]
)
