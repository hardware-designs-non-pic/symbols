# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module r-qfn044-d
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: r-qfn044-d
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 48
#
Element["" " U    " "" "" 0 0 0 -16732 0 100 ""]
(
ElementLine[-13779 12204 -14173 12204 500]
ElementLine[-14173 12204 -14173 14173 500]
ElementLine[-14173 14173 -12204 14173 500]
ElementLine[-12204 14173 -12204 13779 500]
ElementLine[-13779 12204 -12204 13779 500]
ElementLine[-12204 13779 -12598 13779 500]
ElementLine[-12598 13779 -13779 12598 500]
ElementLine[-13779 12598 -13779 12992 500]
ElementLine[-13779 12992 -12992 13779 500]
ElementArc[-10826 10826 880 880 0 360 790]
ElementLine[-13779 13779 13779 13779 500]
ElementLine[13779 13779 13779 -13779 500]
ElementLine[13779 -13779 -13779 -13779 500]
ElementLine[-13779 -13779 -13779 13779 500]
Pad[-9842 12401 -9842 13582 1181 2000 1981 "0" "_1_" "square"]
Pad[-7874 12401 -7874 13582 1181 2000 1981 "0" "_2_" "square"]
Pad[-3937 12401 -3937 13582 1181 2000 1981 "0" "_4_" "square"]
Pad[-1968 12401 -1968 13582 1181 2000 1981 "0" "_5_" "square"]
Pad[0 12401 0 13582 1181 2000 1981 "0" "_6_" "square"]
Pad[1968 12401 1968 13582 1181 2000 1981 "0" "_7_" "square"]
Pad[12401 5905 13582 5905 1181 2000 1981 "0" "_14_" "square"]
Pad[12401 3937 13582 3937 1181 2000 1981 "0" "_15_" "square"]
Pad[12401 1968 13582 1968 1181 2000 1981 "0" "_16_" "square"]
Pad[12401 0 13582 0 1181 2000 1981 "0" "_17_" "square"]
Pad[12401 -1968 13582 -1968 1181 2000 1981 "0" "_18_" "square"]
Pad[12401 -3937 13582 -3937 1181 2000 1981 "0" "_19_" "square"]
Pad[12401 -5905 13582 -5905 1181 2000 1981 "0" "_20_" "square"]
Pad[-5905 12401 -5905 13582 1181 2000 1981 "0" "_3_" "square"]
Pad[5413 5413 5413 5413 10826 2000 11626 "0" "_45_" "square"]
Pad[-5413 5413 -5413 5413 10826 2000 11626 "0" "_45_" "square"]
Pad[-5413 -5413 -5413 -5413 10826 2000 11626 "0" "_45_" "square"]
Pad[5413 -5413 5413 -5413 10826 2000 11626 "0" "_45_" "square"]
Pad[5905 -13582 5905 -12401 1181 2000 1981 "0" "_25_" "square"]
Pad[3937 -13582 3937 -12401 1181 2000 1981 "0" "_26_" "square"]
Pad[1968 -13582 1968 -12401 1181 2000 1981 "0" "_27_" "square"]
Pad[0 -13582 0 -12401 1181 2000 1981 "0" "_28_" "square"]
Pad[-1968 -13582 -1968 -12401 1181 2000 1981 "0" "_29_" "square"]
Pad[-3937 -13582 -3937 -12401 1181 2000 1981 "0" "_30_" "square"]
Pad[-5905 -13582 -5905 -12401 1181 2000 1981 "0" "_31_" "square"]
Pad[-13582 -5905 -12401 -5905 1181 2000 1981 "0" "_36_" "square"]
Pad[-13582 -3937 -12401 -3937 1181 2000 1981 "0" "_37_" "square"]
Pad[-13582 -1968 -12401 -1968 1181 2000 1981 "0" "_38_" "square"]
Pad[-13582 0 -12401 0 1181 2000 1981 "0" "_39_" "square"]
Pad[-13582 1968 -12401 1968 1181 2000 1981 "0" "_40_" "square"]
Pad[-13582 3937 -12401 3937 1181 2000 1981 "0" "_41_" "square"]
Pad[-13582 5905 -12401 5905 1181 2000 1981 "0" "_42_" "square"]
Pad[3937 12401 3937 13582 1181 2000 1981 "0" "_8_" "square"]
Pad[5905 12401 5905 13582 1181 2000 1981 "0" "_9_" "square"]
Pad[7874 12401 7874 13582 1181 2000 1981 "0" "_10_" "square"]
Pad[9842 12401 9842 13582 1181 2000 1981 "0" "_11_" "square"]
Pad[12401 9842 13582 9842 1181 2000 1981 "0" "_12_" "square"]
Pad[12401 7874 13582 7874 1181 2000 1981 "0" "_13_" "square"]
Pad[12401 -7874 13582 -7874 1181 2000 1981 "0" "_21_" "square"]
Pad[12401 -9842 13582 -9842 1181 2000 1981 "0" "_22_" "square"]
Pad[9842 -13582 9842 -12401 1181 2000 1981 "0" "_23_" "square"]
Pad[7874 -13582 7874 -12401 1181 2000 1981 "0" "_24_" "square"]
Pad[-7874 -13582 -7874 -12401 1181 2000 1981 "0" "_32_" "square"]
Pad[-9842 -13582 -9842 -12401 1181 2000 1981 "0" "_33_" "square"]
Pad[-13582 -9842 -12401 -9842 1181 2000 1981 "0" "_34_" "square"]
Pad[-13582 -7874 -12401 -7874 1181 2000 1981 "0" "_35_" "square"]
Pad[-13582 7874 -12401 7874 1181 2000 1981 "0" "_43_" "square"]
Pad[-13582 9842 -12401 9842 1181 2000 1981 "0" "_44_" "square"]
)
