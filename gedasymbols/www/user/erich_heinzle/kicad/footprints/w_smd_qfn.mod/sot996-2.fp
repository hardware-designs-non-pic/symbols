# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sot996-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sot996-2
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 8
#
Element["" " sot996-2 " "" "" 0 0 0 -8661 0 100 ""]
(
ElementArc[-5510 5120 790 790 0 360 500]
ElementArc[-5520 5120 390 390 0 360 500]
ElementLine[-3939 -5910 3939 -5910 500]
ElementLine[-3939 5910 3939 5910 500]
ElementLine[3939 -5910 3939 5910 500]
ElementLine[-3939 5910 -3939 -5910 500]
Pad[-2952 2795 -2952 5275 1456 2000 2256 "0" "_1_" "square"]
Pad[-984 3582 -984 5275 1456 2000 2256 "0" "_2_" "square"]
Pad[984 3582 984 5275 1456 2000 2256 "0" "_3_" "square"]
Pad[2952 3582 2952 5275 1456 2000 2256 "0" "_4_" "square"]
Pad[2952 -5275 2952 -3582 1456 2000 2256 "0" "_5_" "square"]
Pad[984 -5275 984 -3582 1456 2000 2256 "0" "_6_" "square"]
Pad[-984 -5275 -984 -3582 1456 2000 2256 "0" "_7_" "square"]
Pad[-2952 -5275 -2952 -3582 1456 2000 2256 "0" "_8_" "square"]
)
