# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn8
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 9
#
Element["" " dfn8 " "" "" 0 0 0 -9060 0 100 ""]
(
ElementLine[-6300 5120 -6300 6300 500]
ElementLine[-6300 6300 -5120 6300 500]
ElementLine[-5120 6300 -6300 5120 500]
ElementArc[-3930 3540 877 877 0 360 780]
ElementLine[-5910 -5910 5910 -5910 500]
ElementLine[5910 -5910 5910 5910 500]
ElementLine[5910 5910 -5910 5910 500]
ElementLine[-5910 5910 -5910 -5910 500]
Pad[-2950 4700 -2950 5800 1370 2000 2170 "0" "_1_" "square"]
Pad[-970 4700 -970 5800 1370 2000 2170 "0" "_2_" "square"]
Pad[970 4700 970 5800 1370 2000 2170 "0" "_3_" "square"]
Pad[2950 4700 2950 5800 1370 2000 2170 "0" "_4_" "square"]
Pad[2950 -5800 2950 -4700 1370 2000 2170 "0" "_5_" "square"]
Pad[970 -5800 970 -4700 1370 2000 2170 "0" "_6_" "square"]
Pad[-970 -5800 -970 -4700 1370 2000 2170 "0" "_7_" "square"]
Pad[-2950 -5800 -2950 -4700 1370 2000 2170 "0" "_8_" "square"]
Pad[-585 0 585 0 6100 2000 6900 "0" "_9_" "square"]
)
