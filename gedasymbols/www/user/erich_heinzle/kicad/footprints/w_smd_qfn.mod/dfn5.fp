# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dfn5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dfn5
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 12
#
Element["" " dfn5 " "" "" 0 0 0 -15350 0 100 ""]
(
ElementArc[6290 -7870 1414 1414 0 360 1000]
ElementLine[-9840 -11420 9840 -11420 1000]
ElementLine[9840 -11420 9840 11420 1000]
ElementLine[9840 11420 -9840 11420 1000]
ElementLine[-9840 11420 -9840 -11420 1000]
Pad[7500 -11494 7500 -10505 2950 2000 3750 "0" "_1_" "square"]
Pad[2500 -11494 2500 -10505 2950 2000 3750 "0" "_2_" "square"]
Pad[-2500 -11494 -2500 -10505 2950 2000 3750 "0" "_3_" "square"]
Pad[-7500 -11494 -7500 -10505 2950 2000 3750 "0" "_4_" "square"]
Pad[6960 11650 8040 11650 1870 2000 2670 "0" "_5_" "square"]
Pad[-8040 11650 -6960 11650 1870 2000 2670 "0" "_5_" "square"]
Pad[9950 -2570 9950 -970 1950 2000 2750 "0" "_5_" "square"]
Pad[-9950 -2570 -9950 -970 1950 2000 2750 "0" "_5_" "square"]
Pad[3975 6770 5005 6770 7990 2000 8790 "0" "_5_" "square"]
Pad[3975 -1220 5005 -1220 7990 2000 8790 "0" "_5_" "square"]
Pad[-5005 6770 -3975 6770 7990 2000 8790 "0" "_5_" "square"]
Pad[-5005 -1220 -3975 -1220 7990 2000 8790 "0" "_5_" "square"]
)
