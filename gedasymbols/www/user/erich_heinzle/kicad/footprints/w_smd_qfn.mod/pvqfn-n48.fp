# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n48
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n48
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 57
#
Element["" " U    " "" "" 0 0 0 -17320 0 100 ""]
(
ElementLine[-14170 11420 -14170 14170 500]
ElementLine[-14170 14170 -11420 14170 500]
ElementLine[-11420 14170 -14170 11420 500]
ElementLine[-13780 13780 13780 13780 500]
ElementLine[13780 13780 13780 -13780 500]
ElementLine[13780 -13780 -13780 -13780 500]
ElementLine[-13780 -13780 -13780 13780 500]
ElementLine[-12210 13790 -13780 12220 500]
ElementLine[-13780 12600 -12600 13780 500]
ElementLine[-12980 13780 -13770 12990 500]
ElementArc[-11410 11420 877 877 0 360 790]
Pad[-8850 12560 -8850 14800 1100 2000 1900 "0" "_2_" "square"]
Pad[-6880 12560 -6880 14800 1100 2000 1900 "0" "_3_" "square"]
Pad[-4920 12560 -4920 14800 1100 2000 1900 "0" "_4_" "square"]
Pad[-2950 12560 -2950 14800 1100 2000 1900 "0" "_5_" "square"]
Pad[2950 12560 2950 14800 1100 2000 1900 "0" "_8_" "square"]
Pad[4920 12560 4920 14800 1100 2000 1900 "0" "_9_" "square"]
Pad[6880 12560 6880 14800 1100 2000 1900 "0" "_10_" "square"]
Pad[8850 12560 8850 14800 1100 2000 1900 "0" "_11_" "square"]
Pad[10820 12560 10820 14800 1100 2000 1900 "0" "_12_" "square"]
Pad[12560 10820 14800 10820 1100 2000 1900 "0" "_13_" "square"]
Pad[12560 8850 14800 8850 1100 2000 1900 "0" "_14_" "square"]
Pad[-10820 12560 -10820 14800 1100 2000 1900 "0" "_1_" "square"]
Pad[0 0 0 0 7070 2000 7870 "0" "_49_" "square"]
Pad[12560 6880 14800 6880 1100 2000 1900 "0" "_15_" "square"]
Pad[12560 4920 14800 4920 1100 2000 1900 "0" "_16_" "square"]
Pad[12560 -980 14800 -980 1100 2000 1900 "0" "_19_" "square"]
Pad[12560 -2950 14800 -2950 1100 2000 1900 "0" "_20_" "square"]
Pad[12560 -4920 14800 -4920 1100 2000 1900 "0" "_21_" "square"]
Pad[12560 -6880 14800 -6880 1100 2000 1900 "0" "_22_" "square"]
Pad[12560 -8850 14800 -8850 1100 2000 1900 "0" "_23_" "square"]
Pad[12560 -10820 14800 -10820 1100 2000 1900 "0" "_24_" "square"]
Pad[10820 -14800 10820 -12560 1100 2000 1900 "0" "_25_" "square"]
Pad[8850 -14800 8850 -12560 1100 2000 1900 "0" "_26_" "square"]
Pad[6880 -14800 6880 -12560 1100 2000 1900 "0" "_27_" "square"]
Pad[4920 -14800 4920 -12560 1100 2000 1900 "0" "_28_" "square"]
Pad[-980 12560 -980 14800 1100 2000 1900 "0" "_6_" "square"]
Pad[980 12560 980 14800 1100 2000 1900 "0" "_7_" "square"]
Pad[2950 -14800 2950 -12560 1100 2000 1900 "0" "_29_" "square"]
Pad[980 -14800 980 -12560 1100 2000 1900 "0" "_30_" "square"]
Pad[-980 -14800 -980 -12560 1100 2000 1900 "0" "_31_" "square"]
Pad[-2950 -14800 -2950 -12560 1100 2000 1900 "0" "_32_" "square"]
Pad[12560 2950 14800 2950 1100 2000 1900 "0" "_17_" "square"]
Pad[12560 980 14800 980 1100 2000 1900 "0" "_18_" "square"]
Pad[-4920 -14800 -4920 -12560 1100 2000 1900 "0" "_33_" "square"]
Pad[-6880 -14800 -6880 -12560 1100 2000 1900 "0" "_34_" "square"]
Pad[-8850 -14800 -8850 -12560 1100 2000 1900 "0" "_35_" "square"]
Pad[-10820 -14800 -10820 -12560 1100 2000 1900 "0" "_36_" "square"]
Pad[-14800 -10820 -12560 -10820 1100 2000 1900 "0" "_37_" "square"]
Pad[-14800 -8850 -12560 -8850 1100 2000 1900 "0" "_38_" "square"]
Pad[-14800 -6880 -12560 -6880 1100 2000 1900 "0" "_39_" "square"]
Pad[-14800 -4920 -12560 -4920 1100 2000 1900 "0" "_40_" "square"]
Pad[-7470 0 -7070 0 7070 2000 7870 "0" "_49_" "square"]
Pad[7070 0 7470 0 7070 2000 7870 "0" "_49_" "square"]
Pad[-7270 -7270 -7270 -7270 7470 2000 8270 "0" "_49_" "square"]
Pad[0 -7470 0 -7070 7070 2000 7870 "0" "_49_" "square"]
Pad[7270 -7270 7270 -7270 7470 2000 8270 "0" "_49_" "square"]
Pad[-7270 7270 -7270 7270 7470 2000 8270 "0" "_49_" "square"]
Pad[0 7070 0 7470 7070 2000 7870 "0" "_49_" "square"]
Pad[7270 7270 7270 7270 7470 2000 8270 "0" "_49_" "square"]
Pad[-14790 -2950 -12550 -2950 1100 2000 1900 "0" "_41_" "square"]
Pad[-14790 -970 -12550 -970 1100 2000 1900 "0" "_42_" "square"]
Pad[-14790 970 -12550 970 1100 2000 1900 "0" "_43_" "square"]
Pad[-14790 2950 -12550 2950 1100 2000 1900 "0" "_44_" "square"]
Pad[-14790 4920 -12550 4920 1100 2000 1900 "0" "_45_" "square"]
Pad[-14790 6870 -12550 6870 1100 2000 1900 "0" "_46_" "square"]
Pad[-14790 8850 -12550 8850 1100 2000 1900 "0" "_47_" "square"]
Pad[-14790 10820 -12550 10820 1100 2000 1900 "0" "_48_" "square"]
)
