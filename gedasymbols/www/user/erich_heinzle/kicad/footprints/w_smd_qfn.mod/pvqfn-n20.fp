# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module pvqfn-n20
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: pvqfn-n20
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 26
#
Element["" " pvqfn-n20 " "" "" 0 0 0 -10630 0 100 ""]
(
ElementLine[-9450 5910 -9450 7480 500]
ElementLine[-9450 7480 -7870 7480 500]
ElementLine[-7870 7480 -9450 5910 500]
ElementLine[9060 7090 -9060 7090 500]
ElementLine[-9060 -7090 9060 -7090 500]
ElementArc[-7879 5910 400 400 0 360 500]
ElementLine[9060 -7090 9060 7090 500]
ElementLine[-9060 7090 -9060 -7090 500]
Pad[-6890 5390 -6890 7990 940 2000 1740 "0" "_2_" "square"]
Pad[-4920 5390 -4920 7990 940 2000 1740 "0" "_3_" "square"]
Pad[-2950 5390 -2950 7990 940 2000 1740 "0" "_4_" "square"]
Pad[-980 5390 -980 7990 940 2000 1740 "0" "_5_" "square"]
Pad[980 5390 980 7990 940 2000 1740 "0" "_6_" "square"]
Pad[7360 2950 9960 2950 940 2000 1740 "0" "_10_" "square"]
Pad[7360 -2950 9960 -2950 940 2000 1740 "0" "_11_" "square"]
Pad[6890 -7990 6890 -5390 940 2000 1740 "0" "_12_" "square"]
Pad[4920 -7990 4920 -5390 940 2000 1740 "0" "_13_" "square"]
Pad[2950 -7990 2950 -5390 940 2000 1740 "0" "_14_" "square"]
Pad[980 -7990 980 -5390 940 2000 1740 "0" "_15_" "square"]
Pad[-980 -7990 -980 -5390 940 2000 1740 "0" "_16_" "square"]
Pad[-9960 -2950 -7360 -2950 940 2000 1740 "0" "_20_" "square"]
Pad[-9960 2950 -7360 2950 940 2000 1740 "0" "_1_" "square"]
Pad[3939 1969 3939 1969 3939 2000 4739 "0" "_21_" "square"]
Pad[-3939 1969 -3939 1969 3939 2000 4739 "0" "_21_" "square"]
Pad[-3939 -1969 -3939 -1969 3939 2000 4739 "0" "_21_" "square"]
Pad[3939 -1969 3939 -1969 3939 2000 4739 "0" "_21_" "square"]
Pad[2950 5390 2950 7990 940 2000 1740 "0" "_7_" "square"]
Pad[-2950 -7990 -2950 -5390 940 2000 1740 "0" "_17_" "square"]
Pad[0 -1969 0 -1969 3939 2000 4739 "0" "_21_" "square"]
Pad[0 1969 0 1969 3939 2000 4739 "0" "_21_" "square"]
Pad[4960 5390 4960 7990 940 2000 1740 "0" "_8_" "square"]
Pad[6890 5390 6890 7990 940 2000 1740 "0" "_9_" "square"]
Pad[-4920 -7990 -4920 -5390 940 2000 1740 "0" "_18_" "square"]
Pad[-6890 -7990 -6890 -5390 940 2000 1740 "0" "_19_" "square"]
)
