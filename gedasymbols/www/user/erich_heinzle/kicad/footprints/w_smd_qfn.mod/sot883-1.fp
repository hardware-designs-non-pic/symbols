# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sot883-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sot883-1
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 8
#
Element["" " sot883-1 " "" "" 0 0 0 -4724 0 100 ""]
(
ElementLine[-4133 2952 -4724 2362 590]
ElementLine[-3543 2952 -4724 1771 590]
ElementLine[-4724 1181 -4724 2952 590]
ElementLine[-4724 2952 -2952 2952 590]
ElementLine[-2952 2952 -4724 1181 590]
ElementArc[-2755 984 440 440 0 360 500]
ElementLine[-3937 -2165 3937 -2165 500]
ElementLine[3937 -2165 3937 2165 500]
ElementLine[3937 2165 -3937 2165 500]
ElementLine[-3937 2165 -3937 -2165 500]
Pad[-2952 787 -2952 1377 1181 2000 1981 "0" "_1_" "square"]
Pad[-984 984 -984 1377 1181 2000 1981 "0" "_2_" "square"]
Pad[984 984 984 1377 1181 2000 1981 "0" "_3_" "square"]
Pad[2952 984 2952 1377 1181 2000 1981 "0" "_4_" "square"]
Pad[2952 -1377 2952 -984 1181 2000 1981 "0" "_5_" "square"]
Pad[984 -1377 984 -984 1181 2000 1981 "0" "_6_" "square"]
Pad[-984 -1377 -984 -984 1181 2000 1981 "0" "_7_" "square"]
Pad[-2952 -1377 -2952 -984 1181 2000 1981 "0" "_8_" "square"]
)
