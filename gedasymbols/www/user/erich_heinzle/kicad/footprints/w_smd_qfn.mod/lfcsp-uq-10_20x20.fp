# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lfcsp-uq-10_20x20
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lfcsp-uq-10_20x20
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" " lfcsp-uq-10 20x20 " "" "" 0 0 0 -7480 0 100 ""]
(
ElementLine[-2360 4330 -4330 4330 500]
ElementLine[-4330 4330 -4330 2360 500]
ElementLine[-4330 2360 -2360 4330 500]
ElementLine[-3939 2760 -3939 3150 500]
ElementLine[-3939 3150 -3150 3939 500]
ElementLine[-3939 -3939 3939 -3939 500]
ElementLine[3939 -3939 3939 3939 500]
ElementLine[3939 3939 -3939 3939 500]
ElementLine[-3939 3939 -3939 -3939 500]
ElementArc[-2360 2360 559 559 0 360 500]
Pad[-1969 3439 -1969 4439 1170 2000 1970 "0" "_1_" "square"]
Pad[0 3439 0 4439 1170 2000 1970 "0" "_2_" "square"]
Pad[1969 3439 1969 4439 1170 2000 1970 "0" "_3_" "square"]
Pad[3439 970 4439 970 1170 2000 1970 "0" "_4_" "square"]
Pad[3439 -970 4439 -970 1170 2000 1970 "0" "_5_" "square"]
Pad[1969 -4439 1969 -3439 1170 2000 1970 "0" "_6_" "square"]
Pad[0 -4439 0 -3439 1170 2000 1970 "0" "_7_" "square"]
Pad[-1969 -4439 -1969 -3439 1170 2000 1970 "0" "_8_" "square"]
Pad[-4439 -970 -3439 -970 1170 2000 1970 "0" "_9_" "square"]
Pad[-4439 970 -3439 970 1170 2000 1970 "0" "_10_" "square"]
)
