# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lfcsp-uq-10_13x16
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lfcsp-uq-10_13x16
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" " lfcsp-uq-10 13x16 " "" "" 0 0 0 -6300 0 100 ""]
(
ElementLine[3150 1969 1570 3540 500]
ElementLine[3150 1969 3150 3540 500]
ElementLine[3150 3540 1570 3540 500]
ElementArc[1180 1570 559 559 0 360 500]
ElementLine[-2760 3150 2760 3150 500]
ElementLine[2760 3150 2760 -3150 500]
ElementLine[2760 -3150 -2760 -3150 500]
ElementLine[-2760 -3150 -2760 3150 500]
Pad[1570 2760 1570 3540 790 2000 1590 "0" "_1_" "square"]
Pad[1770 790 3330 790 790 2000 1590 "0" "_2_" "square"]
Pad[1770 -790 3330 -790 790 2000 1590 "0" "_3_" "square"]
Pad[1570 -3540 1570 -2760 790 2000 1590 "0" "_4_" "square"]
Pad[0 -3540 0 -2760 790 2000 1590 "0" "_5_" "square"]
Pad[-1570 -3540 -1570 -2760 790 2000 1590 "0" "_6_" "square"]
Pad[-3330 -790 -1770 -790 790 2000 1590 "0" "_7_" "square"]
Pad[-3330 790 -1770 790 790 2000 1590 "0" "_8_" "square"]
Pad[-1570 2760 -1570 3540 790 2000 1590 "0" "_9_" "square"]
Pad[0 2760 0 3540 790 2000 1590 "0" "_10_" "square"]
)
