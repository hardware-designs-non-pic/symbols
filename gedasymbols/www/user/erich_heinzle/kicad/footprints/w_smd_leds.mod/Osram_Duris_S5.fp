# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Osram_Duris_S5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Osram_Duris_S5
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 8
#
Element["" "LD?" "" "" 0 0 0 -9060 0 100 ""]
(
ElementLine[-6496 6496 6496 6496 590]
ElementLine[6496 6496 6496 -6496 590]
ElementLine[6496 -6496 -6496 -6496 590]
ElementLine[-6496 -6496 -6496 6496 590]
ElementArc[-4724 -4724 787 787 0 360 590]
ElementArc[0 0 5118 5118 0 360 590]
Pad[3937 629 3937 3897 1259 2000 2059 "" "1" "square"]
Pad[-728 1279 -728 3248 2559 2000 3359 "" "2" "square"]
Pad[-5531 -5531 -5531 5531 1929 2000 2729 "" "2" "square"]
Pad[5531 -5531 5531 5531 1929 2000 2729 "" "1" "square"]
Pad[3937 -3897 3937 -629 1259 2000 2059 "" "1" "square"]
Pad[-3287 -3248 -3287 -1279 2559 2000 3359 "" "2" "square"]
Pad[-728 -3248 -728 -1279 2559 2000 3359 "" "2" "square"]
Pad[-3287 1279 -3287 3248 2559 2000 3359 "" "2" "square"]
)
