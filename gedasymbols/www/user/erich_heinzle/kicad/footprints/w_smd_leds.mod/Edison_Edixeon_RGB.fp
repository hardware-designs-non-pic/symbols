# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Edison_Edixeon_RGB
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Edison_Edixeon_RGB
# Text descriptor count: 1
# Draw segment object count: 44
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 7
#
Element["" "LD?" "" "" 0 0 0 -18110 0 100 ""]
(
ElementLine[14570 5310 28540 5310 1500]
ElementLine[28540 5310 28540 9250 1500]
ElementLine[28540 9250 12600 9250 1500]
ElementLine[14570 -1969 28540 -1969 1500]
ElementLine[28540 -1969 28540 1969 1500]
ElementLine[28540 1969 14570 1969 1500]
ElementLine[12600 -9250 28540 -9250 1500]
ElementLine[28540 -9250 28540 -5310 1500]
ElementLine[28540 -5310 14570 -5310 1500]
ElementLine[-14570 5310 -28540 5310 1500]
ElementLine[-28540 5310 -28540 9250 1500]
ElementLine[-28540 9250 -12600 9250 1500]
ElementLine[-14570 -5310 -28540 -5310 1500]
ElementLine[-28540 -5310 -28540 -9250 1500]
ElementLine[-28540 -9250 -12600 -9250 1500]
ElementLine[-14570 1969 -28540 1969 1500]
ElementLine[-28540 1969 -28540 -1969 1500]
ElementLine[-28540 -1969 -14570 -1969 1500]
ElementLine[2560 15550 5910 14570 1500]
ElementLine[-5910 14570 -2560 15550 1500]
ElementLine[-2560 -15550 -5910 -14570 1500]
ElementLine[2560 -15550 5910 -14570 1500]
ElementLine[0 -15750 2560 -15550 1500]
ElementLine[-2560 -15550 0 -15750 1500]
ElementLine[2560 15550 0 15750 1500]
ElementLine[-2560 15550 0 15750 1500]
ElementLine[-8270 13390 -11020 11020 1500]
ElementLine[-11020 11020 -13390 8270 1500]
ElementLine[13390 8270 11020 11020 1500]
ElementLine[11020 11020 8270 13390 1500]
ElementLine[8270 -13390 11020 -11020 1500]
ElementLine[11020 -11020 13390 -8270 1500]
ElementLine[5910 -14570 8270 -13390 1500]
ElementLine[14570 -5910 13390 -8270 1500]
ElementLine[14570 5910 13390 8270 1500]
ElementLine[5910 14570 8270 13390 1500]
ElementLine[-5910 14570 -8270 13390 1500]
ElementLine[-14570 5910 -13390 8270 1500]
ElementLine[-14570 -5910 -13390 -8270 1500]
ElementLine[-13390 -8270 -11020 -11020 1500]
ElementLine[-11020 -11020 -8270 -13390 1500]
ElementLine[-8270 -13390 -5910 -14570 1500]
ElementLine[14570 -5910 14570 5910 1500]
ElementLine[-14570 5910 -14570 -5910 1500]
ElementArc[-16340 -7280 790 790 0 360 390]
ElementArc[-16340 7280 790 790 0 360 390]
ElementArc[0 0 11020 11020 0 360 1500]
Pad[-28540 7280 -22640 7280 5120 2000 5920 "" "2" "square"]
Pad[0 0 0 0 25200 2000 26000 "" "7" "blah"]
Pad[-28540 0 -22640 0 5120 2000 5920 "" "3" "square"]
Pad[-28540 -7280 -22640 -7280 5120 2000 5920 "" "1" "square"]
Pad[22640 -7280 28540 -7280 5120 2000 5920 "" "6" "square"]
Pad[22640 0 28540 0 5120 2000 5920 "" "4" "square"]
Pad[22640 7280 28540 7280 5120 2000 5920 "" "5" "square"]
)
