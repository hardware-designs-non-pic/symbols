# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Led_0805
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Led_0805
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -6500 0 100 ""]
(
ElementLine[790 0 -390 -1180 500]
ElementLine[-390 -1180 -390 1180 500]
ElementLine[-390 1180 790 0 500]
ElementLine[390 390 390 -390 500]
ElementLine[0 -790 0 790 500]
ElementLine[1180 3150 1180 -3150 500]
ElementLine[-1180 -3150 -1180 3150 500]
ElementLine[-3939 -3150 3939 -3150 500]
ElementLine[3939 -3150 3939 3150 500]
ElementLine[3939 3150 -3939 3150 500]
ElementLine[-3939 3150 -3939 -3150 500]
Pad[-4130 0 -4130 0 4720 2000 5520 "" "1" "square"]
Pad[4130 0 4130 0 4720 2000 5520 "" "2" "square"]
)
