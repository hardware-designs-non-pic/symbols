# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Led_0402
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Led_0402
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -3540 0 100 ""]
(
ElementLine[-790 1570 -790 -1570 390]
ElementLine[790 -1570 790 1570 390]
ElementLine[-1969 -1570 1969 -1570 390]
ElementLine[1969 -1570 1969 1570 390]
ElementLine[1969 1570 -1969 1570 390]
ElementLine[-1969 1570 -1969 -1570 390]
ElementLine[0 390 0 -390 390]
ElementLine[-390 -790 -390 790 390]
ElementLine[-390 790 390 0 390]
ElementLine[390 0 -390 -790 390]
Pad[-2160 -200 -2160 200 2350 2000 3150 "" "1" "square"]
Pad[2160 -200 2160 200 2350 2000 3150 "" "2" "square"]
)
