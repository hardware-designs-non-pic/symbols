# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Edison_Edixeon_A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Edison_Edixeon_A
# Text descriptor count: 1
# Draw segment object count: 46
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -18110 0 100 ""]
(
ElementLine[23430 -2950 24210 -1370 1500]
ElementLine[24210 -1380 28540 -1380 1500]
ElementLine[28540 -1370 28540 -7670 1500]
ElementLine[28540 -7680 24210 -7680 1500]
ElementLine[24210 -7680 23430 -6110 1500]
ElementLine[14570 -6110 23430 -6110 1500]
ElementLine[23430 -2960 14570 -2960 1500]
ElementLine[-17320 -3939 -17320 -1570 390]
ElementLine[-17320 -1570 -16930 -1180 390]
ElementLine[-16930 -1180 -16340 -1180 390]
ElementLine[-16340 -1180 -15940 -1570 390]
ElementLine[-15940 -1570 -15940 -3939 390]
ElementLine[-15940 -3939 -16340 -4330 390]
ElementLine[-16340 -4330 -16930 -4330 390]
ElementLine[-16930 -4330 -17320 -3939 390]
ElementLine[-18900 -5510 -18900 -200 1500]
ElementLine[-18900 -200 -18110 -200 1500]
ElementLine[-18110 -200 -18110 2760 1500]
ElementLine[-23430 2960 -24210 1380 1500]
ElementLine[-23430 6110 -24210 7680 1500]
ElementLine[-28540 7680 -24210 7680 1500]
ElementLine[-28540 1380 -24210 1380 1500]
ElementLine[-23430 6110 -14570 6110 1500]
ElementLine[-23430 2960 -14570 2960 1500]
ElementLine[-14570 -5510 -18900 -5510 1500]
ElementLine[-28540 7670 -28540 1370 1500]
ElementLine[-8270 13390 -11020 11020 1500]
ElementLine[-11020 11020 -13390 8270 1500]
ElementLine[13390 8270 11020 11020 1500]
ElementLine[11020 11020 8270 13390 1500]
ElementLine[8270 -13390 11020 -11020 1500]
ElementLine[11020 -11020 13390 -8270 1500]
ElementLine[5910 -14570 8270 -13390 1500]
ElementLine[14570 -5910 13390 -8270 1500]
ElementLine[14570 5910 13390 8270 1500]
ElementLine[5910 14570 8270 13390 1500]
ElementLine[-5910 14570 -8270 13390 1500]
ElementLine[-14570 5910 -13390 8270 1500]
ElementLine[-14570 -5910 -13390 -8270 1500]
ElementLine[-13390 -8270 -11020 -11020 1500]
ElementLine[-11020 -11020 -8270 -13390 1500]
ElementLine[-8270 -13390 -5910 -14570 1500]
ElementLine[5910 14570 -5910 14570 1500]
ElementLine[14570 -5910 14570 5910 1500]
ElementLine[-5910 -14570 5910 -14570 1500]
ElementLine[-14570 5910 -14570 -5910 1500]
ElementArc[0 0 11020 11020 0 360 1500]
Pad[26775 -4720 34645 -4720 6300 2000 7100 "" "2" "square"]
Pad[-34645 4720 -26775 4720 6300 2000 7100 "" "1" "square"]
Pad[0 0 0 0 25200 2000 26000 "" "3" "blah"]
)
