# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Avago_Aveon
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Avago_Aveon
# Text descriptor count: 1
# Draw segment object count: 34
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -18110 0 100 ""]
(
ElementLine[26770 -7480 17720 -7480 1500]
ElementLine[17720 -7480 17720 -5910 1500]
ElementLine[17720 -5910 14570 -5910 1500]
ElementLine[26770 -7480 26770 -1180 1500]
ElementLine[26770 -1180 17720 -1180 1500]
ElementLine[17720 -1180 17720 -2760 1500]
ElementLine[17720 -2760 14570 -2760 1500]
ElementLine[-26770 7480 -17720 7480 1500]
ElementLine[-17720 7480 -17720 5910 1500]
ElementLine[-17720 5910 -14570 5910 1500]
ElementLine[-26770 7480 -26770 1180 1500]
ElementLine[-26770 1180 -17720 1180 1500]
ElementLine[-17720 1180 -17720 2760 1500]
ElementLine[-17720 2760 -14570 2760 1500]
ElementLine[-8270 13390 -11020 11020 1500]
ElementLine[-11020 11020 -13390 8270 1500]
ElementLine[13390 8270 11020 11020 1500]
ElementLine[11020 11020 8270 13390 1500]
ElementLine[8270 -13390 11020 -11020 1500]
ElementLine[11020 -11020 13390 -8270 1500]
ElementLine[5910 -14570 8270 -13390 1500]
ElementLine[14570 -5910 13390 -8270 1500]
ElementLine[14570 5910 13390 8270 1500]
ElementLine[5910 14570 8270 13390 1500]
ElementLine[-5910 14570 -8270 13390 1500]
ElementLine[-14570 5910 -13390 8270 1500]
ElementLine[-14570 -5910 -13390 -8270 1500]
ElementLine[-13390 -8270 -11020 -11020 1500]
ElementLine[-11020 -11020 -8270 -13390 1500]
ElementLine[-8270 -13390 -5910 -14570 1500]
ElementLine[5910 14570 -5910 14570 1500]
ElementLine[14570 -5910 14570 5910 1500]
ElementLine[-5910 -14570 5910 -14570 1500]
ElementLine[-14570 5910 -14570 -5910 1500]
ElementArc[22050 -4330 1243 1243 0 360 1500]
ElementArc[0 0 11810 11810 0 360 1500]
Pad[20475 -4330 24605 -4330 6300 2000 7100 "" "2" "square"]
Pad[-24605 4330 -20475 4330 6300 2000 7100 "" "1" "square"]
Pad[0 0 0 0 25200 2000 26000 "" "3" "blah"]
)
