# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_MT
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_MT
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 9
#
Element["" "LD?" "" "" 0 0 0 -20870 0 100 ""]
(
ElementLine[16320 12010 13960 12010 1000]
ElementLine[12600 -17720 12600 17720 1500]
ElementLine[-12600 17720 -12600 -17720 1500]
ElementLine[-17720 -17720 17720 -17720 1500]
ElementLine[17720 -17720 17720 17720 1500]
ElementLine[17720 17720 -17720 17720 1500]
ElementLine[-17720 17720 -17720 -17720 1500]
ElementLine[-15150 -12990 -15150 -10630 1000]
ElementLine[-14000 -11820 -16359 -11820 1000]
ElementArc[0 0 16140 16140 0 360 1500]
Pad[15640 -15640 15640 15640 3740 2000 4540 "" "2" "square"]
Pad[-15640 -15640 -15640 15640 3740 2000 4540 "" "1" "square"]
Pad[0 -5700 0 5700 23620 2000 24420 "" "3" "square"]
Pad[-5860 7120 -5860 12120 7500 2000 8300 "" "3" "square"]
Pad[-5860 -12120 -5860 -7120 7500 2000 8300 "" "3" "square"]
Pad[5860 -12120 5860 -7120 7500 2000 8300 "" "3" "square"]
Pad[5860 7120 5860 12120 7500 2000 8300 "" "3" "square"]
Pad[15640 -10450 15640 10450 3500 2000 4300 "" "2" "square"]
Pad[-15640 -10450 -15640 10450 3500 2000 4300 "" "1" "square"]
)
