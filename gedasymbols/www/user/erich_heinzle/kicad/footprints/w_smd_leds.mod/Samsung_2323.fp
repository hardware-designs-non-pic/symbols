# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Samsung_2323
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Samsung_2323
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 9
#
Element["" "LD**" "" "" 0 0 0 -6500 0 100 ""]
(
ElementLine[-2760 -3350 1770 -3350 500]
ElementLine[-3350 2760 -3350 -2760 500]
ElementLine[2760 3350 -2760 3350 500]
ElementLine[3350 -1380 3350 2760 500]
ElementLine[3939 -4530 4530 -3939 500]
ElementLine[3350 -4530 4530 -3350 500]
ElementLine[4530 -2760 2760 -4530 500]
ElementLine[4530 -4530 4530 4530 500]
ElementLine[4530 4530 -4530 4530 500]
ElementLine[-4530 4530 -4530 -4530 500]
ElementLine[-4530 -4530 4530 -4530 500]
ElementArc[1570 -1570 1791 1791 263 -90 500]
ElementArc[2760 2760 590 590 180 -90 500]
ElementArc[-2760 2760 590 590 90 -90 500]
ElementArc[-2760 -2760 590 590 0 -90 500]
Pad[-2880 -3325 -2880 3325 1320 2000 2120 "" "1" "square"]
Pad[1520 -1880 1520 1880 4210 2000 5010 "" "2" "square"]
Pad[-2100 -3664 -2100 -1975 280 2000 1080 "" "1" "blah"]
Pad[-2100 1975 -2100 3664 280 2000 1080 "" "1" "blah"]
Pad[-870 -1644 -870 1644 650 2000 1450 "" "2" "blah"]
Pad[-4725 0 -4335 0 790 2000 1590 "" "1" "square"]
Pad[4335 0 4725 0 790 2000 1590 "" "2" "square"]
Pad[-3740 -1644 -3740 1644 650 2000 1450 "" "1" "blah"]
Pad[3740 -1644 3740 1644 650 2000 1450 "" "2" "blah"]
)
