# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Led_3528
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Led_3528
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -6500 0 100 ""]
(
ElementLine[-6100 -2170 -4130 -4130 1500]
ElementLine[-6100 0 -6100 -4130 1500]
ElementLine[-6100 -4130 6100 -4130 1500]
ElementLine[6100 -4130 6100 4130 1500]
ElementLine[6100 4130 -6100 4130 1500]
ElementLine[-6100 4130 -6100 0 1500]
Pad[-4430 -1185 -4430 1185 4720 2000 5520 "" "1" "square"]
Pad[4430 -1185 4430 1185 4720 2000 5520 "" "2" "square"]
)
