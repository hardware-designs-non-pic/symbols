# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Osram_Duris_E3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Osram_Duris_E3
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD?" "" "" 0 0 0 -9060 0 100 ""]
(
ElementLine[-3543 -1968 3543 -1968 590]
ElementLine[3543 -1968 4724 -787 590]
ElementLine[4724 -787 4724 787 590]
ElementLine[4724 787 3543 1968 590]
ElementLine[3543 1968 -3543 1968 590]
ElementLine[-3543 1968 -4724 787 590]
ElementLine[-4724 787 -4724 -787 590]
ElementLine[-4724 -787 -3543 -1968 590]
ElementLine[-6102 -984 -4133 -2952 590]
ElementLine[-6102 2952 6102 2952 590]
ElementLine[6102 2952 6102 -2952 590]
ElementLine[6102 -2952 -6102 -2952 590]
ElementLine[-6102 -2952 -6102 2952 590]
Pad[5314 -196 5314 196 4330 2000 5130 "" "1" "square"]
Pad[-5314 -196 -5314 196 4330 2000 5130 "" "2" "square"]
)
