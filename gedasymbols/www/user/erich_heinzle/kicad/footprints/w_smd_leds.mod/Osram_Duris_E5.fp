# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Osram_Duris_E5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Osram_Duris_E5
# Text descriptor count: 1
# Draw segment object count: 40
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 7
#
Element["" "LD**" "" "" 0 0 0 -9840 0 100 ""]
(
ElementLine[-10236 1181 -11023 1181 500]
ElementLine[-11023 1181 -11023 4724 500]
ElementLine[-11023 4724 -10236 4724 500]
ElementLine[-10236 -4724 -11023 -4724 500]
ElementLine[-11023 -4724 -11023 -1181 500]
ElementLine[-11023 -1181 -10236 -1181 500]
ElementLine[10236 -1181 11023 -1181 500]
ElementLine[11023 -1181 11023 -4724 500]
ElementLine[11023 -4724 10236 -4724 500]
ElementLine[10236 4724 11023 4724 500]
ElementLine[11023 4724 11023 1181 500]
ElementLine[11023 1181 10236 1181 500]
ElementLine[7677 -5118 -7677 -5118 500]
ElementLine[9055 3740 9055 -3740 500]
ElementLine[-7677 5118 7677 5118 500]
ElementLine[-9055 -3740 -9055 3740 500]
ElementLine[-7870 -5910 -8270 -5910 500]
ElementLine[-8267 5905 -10236 3937 500]
ElementLine[-10240 -3939 -10240 -4330 500]
ElementLine[-10236 4330 -8661 5905 500]
ElementLine[-8660 -5910 -9060 -5910 500]
ElementLine[-9055 5905 -10236 4724 500]
ElementLine[-10240 -4720 -10240 -5120 500]
ElementLine[-10236 5118 -9448 5905 500]
ElementLine[-10236 -5905 -10236 5905 500]
ElementLine[-10236 5905 10236 5905 500]
ElementLine[10236 5905 10236 -5905 500]
ElementLine[10236 -5905 -10236 -5905 500]
ElementLine[9055 3740 8858 4330 500]
ElementLine[8858 4330 8267 4921 500]
ElementLine[-9055 3740 -8858 4330 500]
ElementLine[-8858 4330 -8267 4921 500]
ElementLine[9055 -3740 8858 -4330 500]
ElementLine[8858 -4330 8267 -4921 500]
ElementLine[7677 5118 8267 4921 500]
ElementLine[-7677 5118 -8267 4921 500]
ElementLine[8267 -4921 7677 -5118 500]
ElementLine[-7677 -5118 -8267 -4921 500]
ElementLine[-8267 -4921 -8858 -4330 500]
ElementLine[-8858 -4330 -9055 -3740 500]
Pad[9055 2814 9842 2814 3937 2000 4737 "" "4" "square"]
Pad[9055 -2814 9842 -2814 3937 2000 4737 "" "2" "square"]
Pad[-9842 2814 -9055 2814 3937 2000 4737 "" "3" "square"]
Pad[-9842 -2814 -9055 -2814 3937 2000 4737 "" "1" "square"]
Pad[-984 0 984 0 5118 2000 5918 "" "5" "square"]
Pad[-1870 -4232 1870 -4232 3346 2000 4146 "" "5" "square"]
Pad[-1870 4232 1870 4232 3346 2000 4146 "" "5" "square"]
)
