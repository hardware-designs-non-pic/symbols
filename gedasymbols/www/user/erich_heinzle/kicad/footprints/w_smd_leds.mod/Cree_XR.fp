# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_XR
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_XR
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 9
#
Element["" "LD?" "" "" 0 0 0 -17320 0 100 ""]
(
ElementLine[-17720 -13780 -17720 13780 1500]
ElementLine[-17720 13780 17720 13780 1500]
ElementLine[17720 13780 17720 -13780 1500]
ElementLine[17720 -13780 -17720 -13780 1500]
ElementLine[-15350 790 -15350 -790 590]
ElementLine[-14560 0 -16139 0 590]
ElementArc[0 0 11020 11020 0 360 1500]
ElementArc[0 0 13390 13390 0 360 1500]
Pad[15200 -9250 15200 9250 1170 2000 1970 "" "2" "square"]
Pad[-15200 -9250 -15200 9250 1170 2000 1970 "" "1" "square"]
Pad[-2700 2705 -2700 4575 5400 2000 6200 "" "3" "square"]
Pad[-2700 -4575 -2700 -2705 5400 2000 6200 "" "3" "square"]
Pad[2700 -4575 2700 -2705 5400 2000 6200 "" "3" "square"]
Pad[2700 2705 2700 4575 5400 2000 6200 "" "3" "square"]
Pad[0 -2559 0 2559 22440 2000 23240 "" "3" "square"]
Pad[-15354 -12204 -15354 12204 3149 2000 3949 "" "1" "square"]
Pad[15354 -12204 15354 12204 3149 2000 3949 "" "2" "square"]
)
