# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Edison_Federal_5050
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Edison_Federal_5050
# Text descriptor count: 1
# Draw segment object count: 15
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 17
#
Element["" "LD?" "" "" 0 0 0 -12200 0 100 ""]
(
ElementLine[5910 5910 8270 5910 500]
ElementLine[8270 5910 8270 8270 500]
ElementLine[8270 8270 8270 8660 500]
ElementLine[8270 8660 -8660 8660 500]
ElementLine[-8660 8660 -8660 7480 500]
ElementLine[-8660 7480 -5510 7480 500]
ElementLine[-5510 -7480 -8270 -7480 500]
ElementLine[-8270 -7480 -8270 -8660 500]
ElementLine[-8270 -8660 8270 -8660 500]
ElementLine[8270 -8660 8270 -5910 500]
ElementLine[8270 -5910 5510 -5910 500]
ElementLine[-9840 -9840 -9840 9840 1000]
ElementLine[-9840 9840 9840 9840 1000]
ElementLine[9840 9840 9840 -9840 1000]
ElementLine[9840 -9840 -9840 -9840 1000]
ElementArc[0 0 8270 8270 0 360 1000]
Pad[8550 2010 8550 3190 1570 2000 2370 "" "3" "square"]
Pad[0 -5900 0 5900 7870 2000 8670 "" "9" "square"]
Pad[9335 2600 10905 2600 1570 2000 2370 "" "3" "blah"]
Pad[9335 -7850 10905 -7850 1570 2000 2370 "" "1" "blah"]
Pad[9335 -2610 10905 -2610 1570 2000 2370 "" "2" "blah"]
Pad[9335 7850 10905 7850 1570 2000 2370 "" "4" "blah"]
Pad[-10905 7850 -9335 7850 1570 2000 2370 "" "8" "blah"]
Pad[-10905 2600 -9335 2600 1570 2000 2370 "" "7" "blah"]
Pad[-10905 -2600 -9335 -2600 1570 2000 2370 "" "6" "blah"]
Pad[-10905 -7850 -9335 -7850 1570 2000 2370 "" "5" "blah"]
Pad[8550 -8440 8550 -7260 1570 2000 2370 "" "1" "square"]
Pad[8550 -3200 8550 -2020 1570 2000 2370 "" "2" "square"]
Pad[8550 7260 8550 8440 1570 2000 2370 "" "4" "square"]
Pad[-8550 7260 -8550 8440 1570 2000 2370 "" "8" "square"]
Pad[-8550 2010 -8550 3190 1570 2000 2370 "" "7" "square"]
Pad[-8550 -3190 -8550 -2010 1570 2000 2370 "" "6" "square"]
Pad[-8550 -8440 -8550 -7260 1570 2000 2370 "" "5" "square"]
)
