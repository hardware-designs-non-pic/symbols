# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Led_PLCC2_3528
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Led_PLCC2_3528
# Text descriptor count: 1
# Draw segment object count: 15
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -8270 0 100 ""]
(
ElementLine[390 790 390 -790 780]
ElementLine[-390 1180 -390 -1180 780]
ElementLine[5910 5510 5910 -5510 780]
ElementLine[-5910 -5510 -5910 5510 780]
ElementLine[-780 -1969 -780 1969 780]
ElementLine[-780 1969 1180 0 780]
ElementLine[1180 0 -780 -1969 780]
ElementLine[3150 5510 5900 2760 780]
ElementLine[4720 5510 5900 4330 780]
ElementLine[5900 4720 5120 5510 780]
ElementLine[3930 5510 5900 3540 780]
ElementLine[-6690 -5510 -6690 5510 780]
ElementLine[-6690 5510 6690 5510 780]
ElementLine[6690 5510 6690 -5510 780]
ElementLine[6690 -5500 -6690 -5500 780]
ElementArc[0 0 4720 4720 0 360 780]
Pad[-5900 -1775 -5900 1775 7070 2000 7870 "" "1" "square"]
Pad[5900 -1775 5900 1775 7070 2000 7870 "" "2" "square"]
)
