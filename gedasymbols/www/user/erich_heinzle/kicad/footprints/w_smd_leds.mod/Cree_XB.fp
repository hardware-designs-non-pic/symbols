# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_XB
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_XB
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 5
#
Element["" "LD?" "" "" 0 0 0 -7090 0 100 ""]
(
ElementLine[-5120 -5120 5120 -5120 590]
ElementLine[5120 -5120 5120 5120 590]
ElementLine[5120 5120 -5120 5120 590]
ElementLine[-5120 5120 -5120 -5120 590]
ElementLine[-3939 790 -3939 -790 590]
ElementLine[-3140 0 -4720 0 590]
ElementArc[0 0 4330 4330 0 360 590]
Pad[3910 -3915 3910 3915 1370 2000 2170 "" "2" "square"]
Pad[-3910 -3915 -3910 3915 1370 2000 2170 "" "1" "square"]
Pad[0 -2790 0 2790 3620 2000 4420 "" "3" "square"]
Pad[-5110 -155 -5110 155 1090 2000 1890 "" "1" "square"]
Pad[5110 -155 5110 155 1090 2000 1890 "" "2" "square"]
)
