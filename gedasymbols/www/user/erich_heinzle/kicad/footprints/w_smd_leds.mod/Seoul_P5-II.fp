# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Seoul_P5-II
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Seoul_P5-II
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 7
#
Element["" "LD?" "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-11810 -9840 -11810 9840 590]
ElementLine[-11810 -9840 11810 -9840 590]
ElementLine[11810 -9840 11810 9840 590]
ElementLine[11810 9840 -11810 9840 590]
ElementArc[-9450 7480 707 707 0 360 590]
ElementArc[0 0 8270 8270 0 360 590]
Pad[10040 -8075 10040 -6305 2360 2000 3160 "" "1" "square"]
Pad[10040 -1180 10040 1180 2360 2000 3160 "" "2" "square"]
Pad[10040 6305 10040 8075 2360 2000 3160 "" "3" "square"]
Pad[-10040 6305 -10040 8075 2360 2000 3160 "" "4" "square"]
Pad[-10040 -1180 -10040 1180 2360 2000 3160 "" "5" "square"]
Pad[-10040 -8075 -10040 -6305 2360 2000 3160 "" "6" "square"]
Pad[0 -2360 0 2360 13780 2000 14580 "" "7" "square"]
)
