# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Luxeon_M
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Luxeon_M
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" "LD?" "" "" 0 0 0 -16930 0 100 ""]
(
ElementLine[-12200 12200 -10630 12200 1000]
ElementLine[-12200 12200 -12200 10630 1000]
ElementLine[12200 -12200 12200 -10630 1000]
ElementLine[12200 -12200 10630 -12200 1000]
ElementLine[-12200 -10630 -12200 -12200 1000]
ElementLine[-12200 -12200 -10630 -12200 1000]
ElementLine[-13770 -13770 13770 -13770 1000]
ElementLine[13770 -13770 13770 13770 1000]
ElementLine[13770 13770 -13770 13770 1000]
ElementLine[-13770 13770 -13770 -13770 1000]
ElementArc[0 0 12600 12600 0 360 1000]
Pad[6310 -5870 6310 5870 2150 2000 2950 "" "1" "square"]
Pad[-6310 -5870 -6310 5870 2150 2000 2950 "" "2" "square"]
Pad[0 -2500 0 2500 8890 2000 9690 "" "3" "square"]
Pad[-9500 -5565 -9500 5565 8540 2000 9340 "" "2" "square"]
Pad[9500 -5565 9500 5565 8540 2000 9340 "" "1" "square"]
Pad[0 -9330 0 9330 8890 2000 9690 "" "3" "square"]
Pad[-11190 -11590 11190 -11590 1540 2000 2340 "" "3" "square"]
Pad[-11190 11590 11190 11590 1540 2000 2340 "" "3" "square"]
Pad[-9875 -13070 9875 -13070 1420 2000 2220 "" "3" "square"]
Pad[-9875 13070 9875 13070 1420 2000 2220 "" "3" "square"]
)
