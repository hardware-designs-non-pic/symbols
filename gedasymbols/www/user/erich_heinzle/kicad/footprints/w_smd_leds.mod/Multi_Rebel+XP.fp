# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Multi_Rebel+XP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Multi_Rebel+XP
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 11
#
Element["" "LD?" "" "" 0 0 2760 -8270 0 100 ""]
(
ElementLine[4920 6890 4920 4920 500]
ElementLine[4920 4920 6890 4920 500]
ElementLine[-6890 4920 -4920 4920 500]
ElementLine[-4920 4920 -4920 6890 500]
ElementLine[-6890 -6890 -6890 6890 500]
ElementLine[-6890 6890 6890 6890 500]
ElementLine[6890 6890 6890 -6890 500]
ElementLine[6890 -6890 -6890 -6890 500]
ElementLine[-6300 -6100 -6300 6100 590]
ElementLine[-6300 6100 11420 6100 590]
ElementLine[11420 6100 11420 -6100 590]
ElementLine[11420 -6100 -6300 -6100 590]
ElementArc[0 0 5120 5120 0 360 590]
Pad[9740 -3940 9740 -2360 3540 2000 4340 "" "1" "square"]
Pad[9740 2360 9740 3940 3540 2000 4340 "" "2" "square"]
Pad[-3540 -4725 -3540 4725 2360 2000 3160 "" "2" "square"]
Pad[10435 -5710 12405 -5710 1969 2000 2769 "" "1" "blah"]
Pad[10435 5710 12405 5710 1969 2000 2769 "" "2" "blah"]
Pad[-6895 5910 -4925 5910 1969 2000 2769 "" "2" "blah"]
Pad[-6895 -5910 -4925 -5910 1969 2000 2769 "" "2" "blah"]
Pad[0 -3935 0 3935 5120 2000 5920 "" "2" "square"]
Pad[5510 -5510 5510 5510 1969 2000 2769 "" "1" "square"]
Pad[-5510 -5510 -5510 5510 1969 2000 2769 "" "2" "square"]
Pad[7280 -4925 7280 -1375 1570 2000 2370 "" "1" "square"]
)
