# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Seoul_Z7
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Seoul_Z7
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 13
#
Element["" "LD?" "" "" 0 0 0 -16540 0 100 ""]
(
ElementLine[-17720 -13780 17720 -13780 1000]
ElementLine[17720 -13780 17720 13780 1000]
ElementLine[17720 13780 -17720 13780 1000]
ElementLine[-17720 13780 -17720 -13780 1000]
ElementArc[14960 -12200 790 790 0 360 1000]
ElementArc[0 0 11020 11020 0 360 1000]
Pad[14180 3050 14960 3050 4170 2000 4970 "" "3" "square"]
Pad[14180 -9250 14960 -9250 4170 2000 4970 "" "1" "square"]
Pad[14180 -3050 14960 -3050 4170 2000 4970 "" "2" "square"]
Pad[14180 9250 14960 9250 4170 2000 4970 "" "4" "square"]
Pad[-14960 9250 -14180 9250 4170 2000 4970 "" "5" "square"]
Pad[-14960 3050 -14180 3050 4170 2000 4970 "" "6" "square"]
Pad[-14960 -3050 -14180 -3050 4170 2000 4970 "" "7" "square"]
Pad[-14960 -9250 -14180 -9250 4170 2000 4970 "" "8" "square"]
Pad[4527 -8661 4527 -4527 9055 2000 9855 "" "9" "square"]
Pad[-4527 -8661 -4527 -4527 9055 2000 9855 "" "9" "square"]
Pad[-4527 4527 -4527 8661 9055 2000 9855 "" "9" "square"]
Pad[4527 4527 4527 8661 9055 2000 9855 "" "9" "square"]
Pad[0 -4133 0 4133 18110 2000 18910 "" "9" "square"]
)
