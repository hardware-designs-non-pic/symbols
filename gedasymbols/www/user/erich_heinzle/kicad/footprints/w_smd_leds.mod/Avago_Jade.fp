# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Avago_Jade
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Avago_Jade
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[7870 -3939 3939 -7870 1000]
ElementLine[5120 -7870 7870 -5120 1000]
ElementLine[6300 -7870 7870 -6300 1000]
ElementLine[9840 0 7870 0 1000]
ElementLine[9840 -5120 7870 -5120 1000]
ElementLine[9840 5120 7870 5120 1000]
ElementLine[-7870 5120 -9840 5120 1000]
ElementLine[-7870 -5120 -9840 -5120 1000]
ElementLine[-7870 0 -9840 0 1000]
ElementLine[-7870 -7870 7870 -7870 1000]
ElementLine[7870 -7870 7870 7870 1000]
ElementLine[7870 7870 -7870 7870 1000]
ElementLine[-7870 7870 -7870 -7870 1000]
ElementArc[0 0 6690 6690 0 360 1000]
Pad[8560 -5095 8560 5095 3350 2000 4150 "" "2" "square"]
Pad[-8570 -5095 -8570 5095 3350 2000 4150 "" "1" "square"]
Pad[0 0 0 0 7480 2000 8280 "" "3" "blah"]
)
