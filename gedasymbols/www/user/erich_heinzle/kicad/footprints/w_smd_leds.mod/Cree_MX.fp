# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_MX
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_MX
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -12990 0 100 ""]
(
ElementLine[9840 6690 13390 6690 1000]
ElementLine[13390 6690 13390 -6690 1000]
ElementLine[13390 -6690 9840 -6690 1000]
ElementLine[-9840 -6690 -13390 -6690 1000]
ElementLine[-13390 -6690 -13390 6690 1000]
ElementLine[-13390 6690 -9840 6690 1000]
ElementLine[9840 6690 6690 9840 1000]
ElementLine[7870 9840 9840 7870 1000]
ElementLine[-7870 -5910 -7870 5910 1000]
ElementLine[5910 -7870 -5910 -7870 1000]
ElementLine[7870 5910 7870 -5910 1000]
ElementLine[-5910 7870 5910 7870 1000]
ElementLine[8660 -9840 -8660 -9840 1000]
ElementLine[9840 8660 9840 -8660 1000]
ElementLine[-8660 9840 8660 9840 1000]
ElementLine[-9840 -8660 -9840 8660 1000]
ElementArc[-5910 -5910 1960 1960 0 -90 1000]
ElementArc[5910 -5910 1960 1960 270 -90 1000]
ElementArc[5910 5910 1960 1960 180 -90 1000]
ElementArc[-5910 5910 1960 1960 90 -90 1000]
ElementArc[8660 8660 1180 1180 180 -90 1000]
ElementArc[-8660 8660 1180 1180 90 -90 1000]
ElementArc[8660 -8660 1180 1180 270 -90 1000]
ElementArc[-8660 -8660 1180 1180 0 -90 1000]
Pad[12300 -4230 12300 4230 6890 2000 7690 "" "2" "square"]
Pad[-12300 -4230 -12300 4230 6890 2000 7690 "" "1" "square"]
Pad[0 -5510 0 5510 4330 2000 5130 "" "3" "square"]
)
