# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Led_1206
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Led_1206
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -6500 0 100 ""]
(
ElementLine[0 -790 0 390 1000]
ElementLine[-390 1180 -390 -1180 1000]
ElementLine[-1180 -2360 -1180 2360 1000]
ElementLine[-1180 2360 1180 0 1000]
ElementLine[1180 0 -1180 -2360 1000]
ElementLine[3150 3540 3150 -3540 1000]
ElementLine[-3150 -3540 -3150 3540 1000]
ElementLine[-6300 -3540 6300 -3540 1000]
ElementLine[6300 -3540 6300 3540 1000]
ElementLine[6300 3540 -6300 3540 1000]
ElementLine[-6300 3540 -6300 -3540 1000]
Pad[-6880 0 -6880 0 5900 2000 6700 "" "1" "square"]
Pad[6880 0 6880 0 5900 2000 6700 "" "2" "square"]
)
