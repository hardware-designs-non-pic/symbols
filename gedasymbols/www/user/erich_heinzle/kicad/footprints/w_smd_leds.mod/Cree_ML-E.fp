# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_ML-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_ML-E
# Text descriptor count: 1
# Draw segment object count: 23
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "LD**" "" "" 0 0 0 -9840 0 100 ""]
(
ElementLine[5120 -7090 6300 -5910 1000]
ElementLine[4330 -7090 6300 -5120 1000]
ElementLine[4720 4130 4530 4720 500]
ElementLine[4530 4720 3939 5310 500]
ElementLine[-4720 4130 -4530 4720 500]
ElementLine[-4530 4720 -3939 5310 500]
ElementLine[4720 -4130 4530 -4720 500]
ElementLine[4530 -4720 3939 -5310 500]
ElementLine[3350 5510 3939 5310 500]
ElementLine[-3350 5510 -3939 5310 500]
ElementLine[3939 -5310 3350 -5510 500]
ElementLine[3350 -5510 -3350 -5510 500]
ElementLine[-3350 -5510 -3150 -5510 500]
ElementLine[4720 4130 4720 -4130 500]
ElementLine[-3350 5510 3350 5510 500]
ElementLine[-4720 -4130 -4720 4130 500]
ElementLine[-3350 -5510 -3939 -5310 500]
ElementLine[-3939 -5310 -4530 -4720 500]
ElementLine[-4530 -4720 -4720 -4130 500]
ElementLine[-6300 -7090 6300 -7090 1000]
ElementLine[6300 -7090 6300 7090 1000]
ElementLine[6300 7090 -6300 7090 1000]
ElementLine[-6300 7090 -6300 -7090 1000]
Pad[5010 4040 5970 4040 4530 2000 5330 "" "3" "square"]
Pad[5010 -4040 5970 -4040 4530 2000 5330 "" "4" "square"]
Pad[-5970 4040 -5010 4040 4530 2000 5330 "" "2" "square"]
Pad[-5970 -4040 -5010 -4040 4530 2000 5330 "" "1" "square"]
Pad[0 -4920 0 4920 2760 2000 3560 "" "5" "square"]
)
