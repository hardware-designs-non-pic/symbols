# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_XP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_XP
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 5
#
Element["" "LD?" "" "" 0 0 0 -9060 0 100 ""]
(
ElementLine[-6890 -4530 -4530 -4530 590]
ElementLine[-4530 -6890 -4530 -4530 590]
ElementLine[6890 -4530 4530 -4530 590]
ElementLine[4530 -6890 4530 -4530 590]
ElementLine[-5510 790 -5510 -790 590]
ElementLine[-4720 0 -6300 0 590]
ElementLine[-6890 6890 6890 6890 590]
ElementLine[6890 6890 6890 -6890 590]
ElementLine[6890 -6890 -6890 -6890 590]
ElementLine[-6890 -6890 -6890 6890 590]
ElementArc[0 0 5120 5120 0 360 590]
Pad[5500 -5515 5500 5515 1960 2000 2760 "" "2" "square"]
Pad[-5510 -5515 -5510 5515 1960 2000 2760 "" "1" "square"]
Pad[0 -3940 0 3940 5110 2000 5910 "" "3" "square"]
Pad[-7080 -199 -7080 199 1570 2000 2370 "" "1" "square"]
Pad[7080 -199 7080 199 1570 2000 2370 "" "2" "square"]
)
