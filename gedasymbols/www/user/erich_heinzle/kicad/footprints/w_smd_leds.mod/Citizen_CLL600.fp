# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Citizen_CLL600
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Citizen_CLL600
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -3939 0 100 ""]
(
ElementLine[-3939 1570 -3939 -1570 780]
ElementLine[3939 -1570 3939 1570 780]
ElementLine[-3939 -1570 3939 -1570 780]
ElementLine[3939 1570 -3939 1570 780]
Pad[-3910 -185 -3910 185 3170 2000 3970 "" "1" "square"]
Pad[825 0 3735 0 3540 2000 4340 "" "2" "square"]
)
