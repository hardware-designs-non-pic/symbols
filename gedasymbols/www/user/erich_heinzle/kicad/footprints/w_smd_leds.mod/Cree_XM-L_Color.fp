# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_XM-L_Color
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_XM-L_Color
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" "LD?" "" "" 0 0 0 -12200 0 100 ""]
(
ElementLine[5118 7874 7480 7874 590]
ElementLine[6299 9055 6299 6692 590]
ElementLine[-9840 -9840 -9840 9840 1000]
ElementLine[-9840 9840 9840 9840 1000]
ElementLine[9840 9840 9840 -9840 1000]
ElementLine[9840 -9840 -9840 -9840 1000]
ElementArc[0 0 8270 8270 0 360 1000]
Pad[8464 1877 8464 3059 2755 2000 3555 "" "3" "square"]
Pad[0 -4133 0 4133 11417 2000 12217 "" "9" "square"]
Pad[8464 -8464 8464 -7283 2755 2000 3555 "" "1" "square"]
Pad[8464 -3059 8464 -1877 2755 2000 3555 "" "2" "square"]
Pad[8464 7283 8464 8464 2755 2000 3555 "" "4" "square"]
Pad[-8464 7283 -8464 8464 2755 2000 3555 "" "5" "square"]
Pad[-8464 1877 -8464 3059 2755 2000 3555 "" "6" "square"]
Pad[-8464 -3059 -8464 -1877 2755 2000 3555 "" "7" "square"]
Pad[-8464 -8464 -8464 -7283 2755 2000 3555 "" "8" "square"]
Pad[0 -3996 0 3996 7598 2000 8398 "" "9" "square"]
)
