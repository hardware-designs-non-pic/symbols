# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Osram_GoldenDragon
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Osram_GoldenDragon
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 5
# Draw arc object count: 4
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -15350 0 100 ""]
(
ElementLine[-10630 -11810 10630 -11810 1500]
ElementLine[-13780 8270 -13780 -8660 1500]
ElementLine[10240 11810 -11020 11810 1500]
ElementLine[13780 -8660 13780 9060 1500]
ElementLine[-13780 -3540 -22050 -3540 1500]
ElementLine[-22050 -3540 -22050 3540 1500]
ElementLine[-22050 3540 -13780 3540 1500]
ElementLine[22050 -3540 13780 -3540 1500]
ElementLine[22050 -3540 22050 3540 1500]
ElementLine[22050 3540 13780 3540 1500]
ElementLine[6300 0 9060 0 1500]
ElementLine[9060 0 9060 790 1500]
ElementLine[9060 790 8660 1969 1500]
ElementLine[8660 1969 7870 3939 1500]
ElementLine[7870 3939 6690 5910 1500]
ElementLine[6690 5910 4720 7480 1500]
ElementLine[4720 7480 3150 5510 1500]
ElementArc[0 0 6312 6312 0 360 1500]
ElementArc[10240 8270 1570 1570 0 360 1500]
ElementArc[-10240 8270 1570 1570 0 360 1500]
ElementArc[10240 -8270 1570 1570 0 360 1500]
ElementArc[-10250 -8270 1570 1570 0 360 1500]
ElementArc[-10630 8660 3174 3174 83 -90 1500]
ElementArc[-10630 -8660 3150 3150 0 -90 1500]
ElementArc[10630 -8660 3150 3150 270 -90 1500]
ElementArc[10630 8660 3175 3175 180 -90 1500]
Pad[19690 -1380 19690 1380 6300 2000 7100 "" "2" "square"]
Pad[-19690 -1380 -19690 1380 6300 2000 7100 "" "1" "square"]
Pad[0 0 0 0 15750 2000 16550 "" "3" "blah"]
)
