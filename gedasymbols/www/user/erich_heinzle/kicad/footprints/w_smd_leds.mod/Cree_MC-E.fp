# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_MC-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_MC-E
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 9
#
Element["" "LD**" "" "" 0 0 0 -16540 0 100 ""]
(
ElementLine[-14960 10240 -14960 -13780 1000]
ElementLine[-11420 13780 -14960 10240 1000]
ElementLine[14960 13780 14960 -13780 1000]
ElementLine[14960 -13780 -14960 -13780 1000]
ElementLine[-11420 13780 14960 13780 1000]
ElementArc[0 0 12600 12600 0 360 1000]
Pad[-17735 -2950 -17145 -2950 4130 2000 4930 "" "3" "square"]
Pad[17145 8860 17735 8860 4130 2000 4930 "" "8" "square"]
Pad[-17735 -8850 -17145 -8850 4130 2000 4930 "" "4" "square"]
Pad[17145 2950 17735 2950 4130 2000 4930 "" "7" "square"]
Pad[-17735 2950 -17145 2950 4130 2000 4930 "" "2" "square"]
Pad[-17735 8860 -17145 8860 4130 2000 4930 "" "1" "square"]
Pad[17145 -2950 17735 -2950 4130 2000 4930 "" "6" "square"]
Pad[17145 -8860 17735 -8860 4130 2000 4930 "" "5" "square"]
Pad[0 -4330 0 4330 7080 2000 7880 "" "9" "square"]
)
