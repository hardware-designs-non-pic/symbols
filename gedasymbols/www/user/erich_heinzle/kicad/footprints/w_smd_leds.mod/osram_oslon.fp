# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module osram_oslon
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: osram_oslon
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 1
# Draw arc object count: 2
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -7870 0 100 ""]
(
ElementLine[1969 -790 1969 790 500]
ElementLine[-5910 -5910 -5910 5910 500]
ElementLine[-5910 5910 5910 5910 500]
ElementLine[5910 5910 5910 -5910 500]
ElementLine[5910 -5910 -5910 -5910 500]
ElementArc[0 0 4330 4330 0 360 500]
ElementArc[1969 0 790 790 0 -90 500]
ElementArc[1969 0 790 790 90 -90 500]
Pad[4430 -4425 4430 4425 2170 2000 2970 "" "2" "square"]
Pad[-4430 -4425 -4430 4425 2170 2000 2970 "" "1" "square"]
Pad[0 -3540 0 3540 3939 2000 4739 "" "3" "square"]
)
