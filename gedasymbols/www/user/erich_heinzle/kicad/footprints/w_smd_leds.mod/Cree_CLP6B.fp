# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_CLP6B
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_CLP6B
# Text descriptor count: 1
# Draw segment object count: 28
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 6
#
Element["" "LD**" "" "" 0 0 0 -14760 0 100 ""]
(
ElementLine[9840 -5510 11420 -5510 590]
ElementLine[11420 -5510 11420 -11020 590]
ElementLine[11420 -11020 9840 -11020 590]
ElementLine[9840 2760 11420 2760 590]
ElementLine[11420 2760 11420 -2760 590]
ElementLine[11420 -2760 9840 -2760 590]
ElementLine[11420 11020 11420 5510 590]
ElementLine[11420 5510 9840 5510 590]
ElementLine[9840 11020 11420 11020 590]
ElementLine[-9840 5510 -11420 5510 590]
ElementLine[-11420 5510 -11420 11020 590]
ElementLine[-11420 11020 -9840 11020 590]
ElementLine[-9840 -2760 -11420 -2760 590]
ElementLine[-11420 -2760 -11420 2760 590]
ElementLine[-11420 2760 -9840 2760 590]
ElementLine[-9840 -11020 -11420 -11020 590]
ElementLine[-11420 -11020 -11420 -5510 590]
ElementLine[-11420 -5510 -9840 -5510 590]
ElementLine[-9840 -11020 -9060 -11810 590]
ElementLine[-9840 -10630 -8660 -11810 590]
ElementLine[-9840 -10240 -8270 -11810 590]
ElementLine[-9840 -9840 -7870 -11810 590]
ElementLine[-9840 -9450 -7480 -11810 590]
ElementLine[-9840 -9060 -7090 -11810 590]
ElementLine[-9840 -11810 -9840 11810 590]
ElementLine[-9840 11810 9840 11810 590]
ElementLine[9840 11810 9840 -11810 590]
ElementLine[9840 -11810 -9840 -11810 590]
ElementArc[0 0 7880 7880 0 360 590]
Pad[-7580 0 -6600 0 6890 2000 7690 "" "2" "square"]
Pad[6600 0 7580 0 6890 2000 7690 "" "5" "square"]
Pad[-7580 -8270 -6600 -8270 6890 2000 7690 "" "1" "square"]
Pad[6600 -8270 7580 -8270 6890 2000 7690 "" "6" "square"]
Pad[-7580 8270 -6600 8270 6890 2000 7690 "" "3" "square"]
Pad[6600 8270 7580 8270 6890 2000 7690 "" "4" "square"]
)
