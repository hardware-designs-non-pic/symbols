# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sharp_doubledome
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sharp_doubledome
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "LD**" "" "" 0 0 0 -7870 0 100 ""]
(
ElementLine[-5510 -5510 -5510 5510 500]
ElementLine[-5510 5510 5510 5510 500]
ElementLine[5510 5510 5510 -5510 500]
ElementLine[5510 -5510 -5510 -5510 500]
ElementArc[4330 -4330 390 390 0 360 500]
ElementArc[0 0 4720 4720 0 360 500]
Pad[-3540 -2755 -3540 2755 3939 2000 4739 "" "1" "square"]
Pad[3540 -2755 3540 2755 3939 2000 4739 "" "2" "square"]
)
