# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Cree_XM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Cree_XM
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 5
#
Element["" "LD?" "" "" 0 0 0 -12200 0 100 ""]
(
ElementLine[-7480 -1180 -7480 1180 1000]
ElementLine[-8660 0 -6300 0 1000]
ElementLine[6690 -6690 9840 -6690 1000]
ElementLine[6690 -6690 6690 -9840 1000]
ElementLine[-9840 -6690 -6690 -6690 1000]
ElementLine[-6690 -6690 -6690 -9840 1000]
ElementLine[-9840 -9840 -9840 9840 1000]
ElementLine[-9840 9840 9840 9840 1000]
ElementLine[9840 9840 9840 -9840 1000]
ElementLine[9840 -9840 -9840 -9840 1000]
ElementArc[0 0 8270 8270 0 360 1000]
Pad[8460 -8465 8460 8465 1969 2000 2769 "" "2" "square"]
Pad[-8460 -8465 -8460 8465 1969 2000 2769 "" "1" "square"]
Pad[0 -3975 0 3975 10950 2000 11750 "" "3" "square"]
Pad[-10160 -199 -10160 199 1570 2000 2370 "" "1" "square"]
Pad[10160 -199 10160 199 1570 2000 2370 "" "2" "square"]
)
