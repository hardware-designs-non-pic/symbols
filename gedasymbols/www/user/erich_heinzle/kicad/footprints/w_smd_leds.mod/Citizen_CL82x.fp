# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Citizen_CL82x
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Citizen_CL82x
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 3
#
Element["" "LD**" "" "" 0 0 0 -3939 0 100 ""]
(
ElementLine[-3150 -1570 3150 -1570 590]
ElementLine[3150 -1570 3150 1570 590]
ElementLine[3150 1570 -3150 1570 590]
ElementLine[-3150 1570 -3150 -1570 590]
ElementArc[1570 0 559 559 180 -90 590]
ElementArc[-1570 0 559 559 0 -90 590]
ElementArc[0 -1580 2786 2786 135 -90 590]
ElementArc[0 1580 2786 2786 -45 -90 590]
Pad[-2850 -95 -2850 95 3350 2000 4150 "" "1" "square"]
Pad[1175 0 3345 0 2360 2000 3160 "" "2" "square"]
Pad[3080 -325 3080 325 2890 2000 3690 "" "2" "square"]
)
