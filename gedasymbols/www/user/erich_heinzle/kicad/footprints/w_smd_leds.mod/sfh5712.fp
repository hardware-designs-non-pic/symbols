# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sfh5712
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sfh5712
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 4
#
Element["" "sfh5712" "" "" 0 0 0 -7874 0 100 ""]
(
ElementLine[-1968 5905 -393 4330 590]
ElementLine[-5905 2362 -4330 787 590]
ElementLine[-1968 5905 -5905 5905 590]
ElementLine[-5905 2362 -5905 5905 590]
ElementLine[-5511 1968 -5511 5511 590]
ElementLine[-5511 5511 -1574 5511 590]
ElementLine[-5118 1574 -5118 5118 590]
ElementLine[-5118 5118 -1181 5118 590]
ElementLine[-4724 1181 -4724 4724 590]
ElementLine[-4724 4724 -787 4724 590]
ElementLine[-4330 -4330 4330 -4330 590]
ElementLine[-4330 4330 4330 4330 590]
ElementLine[4330 -4330 4330 4330 590]
ElementLine[-4330 4330 -4330 -4330 590]
ElementArc[-7478 3545 790 790 0 360 590]
ElementArc[-7488 3545 390 390 0 360 590]
Pad[-3248 3248 -2854 3248 3346 2000 4146 "" "1" "square"]
Pad[2854 3248 3248 3248 3346 2000 4146 "" "2" "square"]
Pad[2854 -3248 3248 -3248 3346 2000 4146 "" "3" "square"]
Pad[-3248 -3248 -2854 -3248 3346 2000 4146 "" "4" "square"]
)
