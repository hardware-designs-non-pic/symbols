# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Seoul_P4
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Seoul_P4
# Text descriptor count: 1
# Draw segment object count: 47
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "LD?" "" "" 0 0 0 -18110 0 100 ""]
(
ElementLine[-14570 -2360 -18900 -2360 1500]
ElementLine[-18900 -2360 -18900 -5510 1500]
ElementLine[-14570 -5510 -18900 -5510 1500]
ElementLine[18900 2360 18900 2760 1500]
ElementLine[18900 2760 17720 3939 1500]
ElementLine[17720 3939 18900 5120 1500]
ElementLine[18900 5120 18900 5510 1500]
ElementLine[14960 2360 18900 2360 1500]
ElementLine[14570 5510 18900 5510 1500]
ElementLine[19690 -5910 17720 -5910 1500]
ElementLine[19690 -2760 17720 -2760 1500]
ElementLine[28350 -1180 19690 -1180 1500]
ElementLine[19690 -7480 28350 -7480 1500]
ElementLine[-17720 2760 -19690 2760 1500]
ElementLine[-17720 5910 -19690 5910 1500]
ElementLine[-28350 7480 -19690 7480 1500]
ElementLine[-28350 1180 -19690 1180 1500]
ElementLine[19700 -7480 19700 -5910 1500]
ElementLine[17720 -5910 14570 -5910 1500]
ElementLine[28350 -7480 28350 -1180 1500]
ElementLine[19680 -1180 19680 -2760 1500]
ElementLine[17720 -2760 14570 -2760 1500]
ElementLine[-19680 7480 -19680 5910 1500]
ElementLine[-17720 5910 -14570 5910 1500]
ElementLine[-28350 7470 -28350 1170 1500]
ElementLine[-19680 1180 -19680 2760 1500]
ElementLine[-17720 2760 -14570 2760 1500]
ElementLine[-8270 13390 -11020 11020 1500]
ElementLine[-11020 11020 -13390 8270 1500]
ElementLine[13390 8270 11020 11020 1500]
ElementLine[11020 11020 8270 13390 1500]
ElementLine[8270 -13390 11020 -11020 1500]
ElementLine[11020 -11020 13390 -8270 1500]
ElementLine[5910 -14570 8270 -13390 1500]
ElementLine[14570 -5910 13390 -8270 1500]
ElementLine[14570 5910 13390 8270 1500]
ElementLine[5910 14570 8270 13390 1500]
ElementLine[-5910 14570 -8270 13390 1500]
ElementLine[-14570 5910 -13390 8270 1500]
ElementLine[-14570 -5910 -13390 -8270 1500]
ElementLine[-13390 -8270 -11020 -11020 1500]
ElementLine[-11020 -11020 -8270 -13390 1500]
ElementLine[-8270 -13390 -5910 -14570 1500]
ElementLine[5910 14570 -5910 14570 1500]
ElementLine[14570 -5910 14570 5910 1500]
ElementLine[-5910 -14570 5910 -14570 1500]
ElementLine[-14570 5910 -14570 -5910 1500]
ElementArc[0 0 11020 11020 0 360 1500]
Pad[23820 -4410 28540 -4410 8270 2000 9070 "" "2" "square"]
Pad[-28540 4410 -23820 4410 8270 2000 9070 "" "1" "square"]
Pad[0 0 0 0 23620 2000 24420 "" "3" "blah"]
)
