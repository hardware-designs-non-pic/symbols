# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Luxeon_C
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Luxeon_C
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "LD?" "" "" 0 0 0 -5510 0 100 ""]
(
ElementLine[1570 -1180 2760 -1180 500]
ElementLine[2760 -1180 2760 1180 500]
ElementLine[2760 1180 1570 1180 500]
ElementLine[1570 1180 1570 -1180 500]
ElementLine[-2760 0 -2760 -1570 500]
ElementLine[-2760 -1570 390 -1570 500]
ElementLine[390 -1570 390 1570 500]
ElementLine[390 1570 -2760 1570 500]
ElementLine[-2760 1570 -2760 0 500]
ElementLine[-3939 -3150 3939 -3150 500]
ElementLine[3939 -3150 3939 3150 500]
ElementLine[3939 3150 -3939 3150 500]
ElementLine[-3939 3150 -3939 -3150 500]
Pad[1550 -765 1550 765 4170 2000 4970 "" "1" "square"]
Pad[-2730 -1950 -2730 1950 1800 2000 2600 "" "2" "square"]
Pad[2555 -2750 4525 -2750 1969 2000 2769 "" "1" "blah"]
Pad[2555 2750 4525 2750 1969 2000 2769 "" "1" "blah"]
Pad[-4525 2750 -2555 2750 1969 2000 2769 "" "2" "blah"]
Pad[-4525 -2750 -2555 -2750 1969 2000 2769 "" "2" "blah"]
)
