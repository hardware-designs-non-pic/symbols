# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module Seoul_5630
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: Seoul_5630
# Text descriptor count: 1
# Draw segment object count: 41
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "LD**" "" "" 0 0 0 -9840 0 100 ""]
(
ElementLine[-10240 1180 -11020 1180 500]
ElementLine[-11020 1180 -11020 4720 500]
ElementLine[-11020 4720 -10240 4720 500]
ElementLine[-10240 -4720 -11020 -4720 500]
ElementLine[-11020 -4720 -11020 -1180 500]
ElementLine[-11020 -1180 -10240 -1180 500]
ElementLine[10240 -1180 11020 -1180 500]
ElementLine[11020 -1180 11020 -4720 500]
ElementLine[11020 -4720 10240 -4720 500]
ElementLine[10240 4720 11020 4720 500]
ElementLine[11020 4720 11020 1180 500]
ElementLine[11020 1180 10240 1180 500]
ElementLine[7280 -5120 -7280 -5120 500]
ElementLine[8660 3740 8660 -3740 500]
ElementLine[-7280 5120 7280 5120 500]
ElementLine[-8660 -3740 -8660 3740 500]
ElementLine[-7870 -5910 -8270 -5910 500]
ElementLine[-8270 -5910 -10240 -3939 500]
ElementLine[-10240 -3939 -10240 -4330 500]
ElementLine[-10240 -4330 -8660 -5910 500]
ElementLine[-8660 -5910 -9060 -5910 500]
ElementLine[-9060 -5910 -10240 -4720 500]
ElementLine[-10240 -4720 -10240 -5120 500]
ElementLine[-10240 -5120 -9450 -5910 500]
ElementLine[-10240 -3540 -7870 -5910 500]
ElementLine[-10240 -5910 -10240 5910 500]
ElementLine[-10240 5910 10240 5910 500]
ElementLine[10240 5910 10240 -5910 500]
ElementLine[10240 -5910 -10240 -5910 500]
ElementLine[8660 3730 8470 4320 500]
ElementLine[8470 4330 7879 4920 500]
ElementLine[-8660 3740 -8470 4330 500]
ElementLine[-8470 4330 -7879 4920 500]
ElementLine[8660 -3730 8470 -4320 500]
ElementLine[8470 -4330 7879 -4920 500]
ElementLine[7290 5110 7879 4910 500]
ElementLine[-7280 5110 -7870 4910 500]
ElementLine[7879 -4910 7290 -5110 500]
ElementLine[-7280 -5120 -7870 -4920 500]
ElementLine[-7879 -4910 -8470 -4320 500]
ElementLine[-8470 -4330 -8660 -3740 500]
Pad[9940 2655 9940 3245 3740 2000 4540 "" "5" "square"]
Pad[9940 -3245 9940 -2655 3740 2000 4540 "" "1" "square"]
Pad[-9940 2655 -9940 3245 3740 2000 4540 "" "2" "square"]
Pad[-9940 -3245 -9940 -2655 3740 2000 4540 "" "4" "square"]
Pad[-1965 0 1965 0 7090 2000 7890 "" "3" "square"]
)
