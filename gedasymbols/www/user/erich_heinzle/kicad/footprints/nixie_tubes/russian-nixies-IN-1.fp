# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-1
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 11
#
Element["" ">NAME" "" "" 0 0 81250 -3120 1 100 ""]
(
ElementArc[0 0 48889 48889 0 360 590]
Pin[0 49140 14500 2000 15300 10500 "" "0" ""]
Pin[-44700 20410 14500 2000 15300 10500 "" "1" ""]
Pin[-48640 -6990 14500 2000 15300 10500 "" "2" ""]
Pin[-37140 -32180 14500 2000 15300 10500 "" "3" ""]
Pin[-13840 -47150 14500 2000 15300 10500 "" "4" ""]
Pin[13840 -47150 14500 2000 15300 10500 "" "5" ""]
Pin[37140 -32180 14500 2000 15300 10500 "" "6" ""]
Pin[48640 -6990 14500 2000 15300 10500 "" "7" ""]
Pin[44700 20410 14500 2000 15300 10500 "" "8" ""]
Pin[26560 41340 14500 2000 15300 10500 "" "9" ""]
Pin[-26560 41340 14500 2000 15300 10500 "" "A" ""]
)
