# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-9
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-9
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 1
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 0 14370 0 100 ""]
(
ElementArc[0 0 7113 7113 0 360 500]
ElementArc[0 310 12504 12504 0 360 1000]
Pin[-6250 0 4360 2000 5160 2360 "" "A" ""]
Pad[-6250 -2180 -6250 2180 4360 2000 5160 "" "A" ""]
Pad[-6250 -2180 -6250 2180 4360 2000 5160 "" "A" ",onsolder"]
Pin[6240 0 4360 2000 5160 2360 "" "K" ""]
Pad[6240 -2180 6240 2180 4360 2000 5160 "" "K" ""]
Pad[6240 -2180 6240 2180 4360 2000 5160 "" "K" ",onsolder"]
)
