# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-IN-18-AMP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-IN-18-AMP
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 2
# Draw arc object count: 1
# Pad count: 14
#
Element["" "1" "" "" 0 0 5450 -25130 0 100 ""]
(
ElementLine[0 71310 0 62260 0]
ElementLine[0 62260 -2360 68160 0]
ElementLine[-2360 68160 2360 68160 0]
ElementLine[2360 68160 0 62260 0]
ElementLine[-9840 0 9840 0 500]
ElementLine[0 9840 0 -9840 500]
ElementArc[0 0 43148 43148 0 360 250]
ElementArc[0 0 6958 6958 0 360 500]
ElementArc[0 -660 50766 50766 0 360 0]
Pin[-33850 9840 12590 2000 13390 9050 "" "1" ""]
Pin[-26770 23220 12590 2000 13390 9050 "" "2" ""]
Pin[-14560 32280 12590 2000 13390 9050 "" "3" ""]
Pin[0 35430 12590 2000 13390 9050 "" "4" ""]
Pin[14560 32280 12590 2000 13390 9050 "" "5" ""]
Pin[26770 23220 12590 2000 13390 9050 "" "6" ""]
Pin[33850 9840 12590 2000 13390 9050 "" "7" ""]
Pin[35030 -5110 12590 2000 13390 9050 "" "8" ""]
Pin[29920 -18890 12590 2000 13390 9050 "" "9" ""]
Pin[19290 -29920 12590 2000 13390 9050 "" "10" ""]
Pin[5110 -35030 12590 2000 13390 9050 "" "11" ""]
Pin[-9840 -33850 12590 2000 13390 9050 "" "12" ""]
Pin[-22830 -26770 12590 2000 13390 9050 "" "13" ""]
Pin[-32280 -14960 12590 2000 13390 9050 "" "14" ""]
)
