# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-6
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-6
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -1250 15000 2 100 ""]
(
ElementArc[0 0 7113 7113 0 360 500]
Pin[-6250 0 5140 2000 5940 3140 "" "A" ""]
Pad[-6250 -2575 -6250 2575 5140 2000 5940 "" "A" ""]
Pad[-6250 -2575 -6250 2575 5140 2000 5940 "" "A" ",onsolder"]
Pin[6240 0 5140 2000 5940 3140 "" "K" ""]
Pad[6240 -2575 6240 2575 5140 2000 5940 "" "K" ""]
Pad[6240 -2575 6240 2575 5140 2000 5940 "" "K" ",onsolder"]
)
