# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-19
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-19
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 2
# Pad count: 13
#
Element["" ">NAME" "" "" 0 0 32500 -1250 1 100 ""]
(
ElementArc[-20 -160 18017 18017 0 360 310]
ElementArc[0 0 17932 17932 0 360 500]
ElementArc[0 620 22500 22500 0 360 620]
ElementArc[0 620 22500 22500 0 360 1000]
Pin[6820 14900 5300 2000 6100 3300 "" "2" ""]
Pin[12680 10440 5300 2000 6100 3300 "" "3" ""]
Pin[16030 3870 5300 2000 6100 3300 "" "4" ""]
Pin[16190 -3480 5300 2000 6100 3300 "" "5" ""]
Pin[13140 -10190 5300 2000 6100 3300 "" "6" ""]
Pin[7490 -14920 5300 2000 6100 3300 "" "7" ""]
Pin[340 -16720 5300 2000 6100 3300 "" "8" ""]
Pin[-6870 -15240 5300 2000 6100 3300 "" "9" ""]
Pin[-12730 -10770 5300 2000 6100 3300 "" "10" ""]
Pin[-16070 -4210 5300 2000 6100 3300 "" "11" ""]
Pin[-16240 3150 5300 2000 6100 3300 "" "12" ""]
Pin[-13190 9860 5300 2000 6100 3300 "" "13" ""]
Pin[-7530 14580 5300 2000 6100 3300 "" "14" ""]
)
