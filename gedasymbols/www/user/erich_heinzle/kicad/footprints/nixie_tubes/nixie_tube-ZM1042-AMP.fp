# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-ZM1042-AMP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-ZM1042-AMP
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 15
# Draw arc object count: 0
# Pad count: 13
#
Element["" "1" "" "" 0 0 -18630 -14580 0 100 ""]
(
ElementLine[0 56290 0 64170 500]
ElementLine[0 56290 -1960 60230 500]
ElementLine[-1960 60230 1960 60230 500]
ElementLine[1960 60230 0 56290 500]
ElementArc[0 0 39527 39527 0 360 500]
ElementArc[0 -33070 3338 3338 0 360 500]
ElementArc[-14330 -29760 3338 3338 0 360 500]
ElementArc[-25860 -20660 3338 3338 0 360 500]
ElementArc[-32240 -7360 3338 3338 0 360 500]
ElementArc[-32280 7400 3338 3338 0 360 500]
ElementArc[-25820 20700 3338 3338 0 360 500]
ElementArc[-14330 29800 3338 3338 0 360 500]
ElementArc[0 33140 3338 3338 0 360 500]
ElementArc[14290 29880 3338 3338 0 360 500]
ElementArc[25820 20660 3338 3338 0 360 500]
ElementArc[32160 7360 3338 3338 0 360 500]
ElementArc[32240 -7280 3338 3338 0 360 500]
ElementArc[25860 -20550 3338 3338 0 360 500]
ElementArc[0 0 28977 28977 0 360 500]
Pin[-32240 -7350 12590 2000 13390 9050 "" "0" ""]
Pin[-25850 -20610 12590 2000 13390 9050 "" "1" ""]
Pin[25850 -20610 12590 2000 13390 9050 "" "2" ""]
Pin[32240 -7350 12590 2000 13390 9050 "" "3" ""]
Pin[32240 7350 12590 2000 13390 9050 "" "4" ""]
Pin[25850 20610 12590 2000 13390 9050 "" "5" ""]
Pin[14350 29790 12590 2000 13390 9050 "" "6" ""]
Pin[-14350 29790 12590 2000 13390 9050 "" "7" ""]
Pin[-25850 20610 12590 2000 13390 9050 "" "8" ""]
Pin[-32240 7350 12590 2000 13390 9050 "" "9" ""]
Pin[0 -33070 12590 2000 13390 9050 "" "A" ""]
Pin[-14350 -29790 12590 2000 13390 9050 "" "A1" ""]
Pin[0 33070 12590 2000 13390 9050 "" "IV" ""]
)
