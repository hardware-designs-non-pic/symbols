# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-NE2-NEON-BULB-DRILL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-NE2-NEON-BULB-DRILL
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 1
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 500 -1300 0 100 ""]
(
ElementArc[0 0 4992 4992 0 360 500]
ElementArc[0 0 3946 3946 0 360 500]
ElementArc[0 0 3536 3536 0 360 500]
Pin[0 -5000 5600 2000 6400 2750 "" "1" ""]
Pin[0 5000 5600 2000 6400 2750 "" "2" ""]
)
