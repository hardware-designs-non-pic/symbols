# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-B13B
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-B13B
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 17
# Draw arc object count: 1
# Pad count: 13
#
Element["" "1" "" "" 0 0 -21120 -17260 0 100 ""]
(
ElementLine[0 57080 0 64960 1000]
ElementLine[0 57080 -1960 61020 1000]
ElementLine[-1960 61020 1960 61020 1000]
ElementLine[1960 61020 0 57080 1000]
ElementArc[0 0 40362 40362 0 360 1000]
ElementArc[0 0 36416 36416 0 360 1000]
ElementArc[0 0 33856 33856 0 360 1000]
ElementArc[0 -37790 4342 4342 0 360 1000]
ElementArc[-17320 -33850 4342 4342 0 360 1000]
ElementArc[-30310 -22830 4342 4342 0 360 1000]
ElementArc[-37400 -6690 4342 4342 0 360 1000]
ElementArc[-36720 10410 4342 4342 0 360 1000]
ElementArc[-28740 25190 4342 4342 0 360 1000]
ElementArc[31490 -21650 4342 4342 0 360 1000]
ElementArc[37790 -5900 4342 4342 0 360 1000]
ElementArc[36610 11410 4342 4342 0 360 1000]
ElementArc[28740 25190 4342 4342 0 360 1000]
ElementArc[15740 34640 4342 4342 0 360 1000]
ElementArc[0 38180 4342 4342 0 360 1000]
ElementArc[-15740 34640 4342 4342 0 360 1000]
ElementArc[0 0 13916 13916 0 360 1000]
ElementArc[0 57080 3930 3930 0 360 1000]
Pin[-37400 -6890 10000 2000 10800 5600 "" "0" ""]
Pin[-30310 -22830 10000 2000 10800 5600 "" "1" ""]
Pin[31490 -21650 10000 2000 10800 5600 "" "2" ""]
Pin[37790 -5900 10000 2000 10800 5600 "" "3" ""]
Pin[36610 11410 10000 2000 10800 5600 "" "4" ""]
Pin[28740 25190 10000 2000 10800 5600 "" "5" ""]
Pin[15740 34640 10000 2000 10800 5600 "" "6" ""]
Pin[-15740 34640 10000 2000 10800 5600 "" "7" ""]
Pin[-28740 25190 10000 2000 10800 5600 "" "8" ""]
Pin[-36720 10410 10000 2000 10800 5600 "" "9" ""]
Pin[0 -37790 10000 2000 10800 5600 "" "A" ""]
Pin[-17120 -33850 10000 2000 10800 5600 "" "A1" ""]
Pin[0 38180 10000 2000 10800 5600 "" "IV" ""]
)
