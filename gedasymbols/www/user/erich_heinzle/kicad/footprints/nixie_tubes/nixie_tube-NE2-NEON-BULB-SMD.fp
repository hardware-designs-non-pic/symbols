# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-NE2-NEON-BULB-SMD
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-NE2-NEON-BULB-SMD
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -1000 -3300 0 100 ""]
(
ElementLine[-9840 -5900 9840 -5900 400]
ElementLine[9840 -5900 9840 5900 400]
ElementLine[9840 5900 -9840 5900 400]
ElementLine[-9840 5900 -9840 -5900 400]
ElementLine[-5500 -2000 -5500 3000 400]
ElementLine[-5500 3000 -3000 7000 400]
ElementLine[5500 -2000 5500 3000 400]
ElementLine[5500 3000 3000 7000 400]
Pad[-8095 0 -3705 0 4200 2000 5000 "" "1" "square"]
Pad[3705 0 8095 0 4200 2000 5000 "" "2" "square"]
)
