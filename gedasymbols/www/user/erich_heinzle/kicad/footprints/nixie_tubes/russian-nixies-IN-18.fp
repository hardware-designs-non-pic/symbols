# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-18
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-18
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 7
# Draw arc object count: 1
# Pad count: 14
#
Element["" ">NAME" "" "" 0 0 69370 -3120 1 100 ""]
(
ElementLine[-9840 0 9840 0 500]
ElementLine[0 9840 0 -9840 500]
ElementArc[0 0 7071 7071 0 360 50]
ElementArc[0 0 3536 3536 0 360 5000]
ElementArc[0 0 3536 3536 0 360 5000]
ElementArc[0 0 7071 7071 0 360 50]
ElementArc[0 0 44024 44024 0 360 500]
ElementArc[0 0 43855 43855 0 360 500]
ElementArc[0 0 6958 6958 0 360 500]
ElementArc[0 310 50001 50001 0 360 1000]
Pin[22830 26770 12590 2000 13390 9050 "" "0" ""]
Pin[-5110 35030 12590 2000 13390 9050 "" "1" ""]
Pin[-19290 29920 12590 2000 13390 9050 "" "2" ""]
Pin[-33850 -9840 12590 2000 13390 9050 "" "3" ""]
Pin[26770 -23220 12590 2000 13390 9050 "" "4" ""]
Pin[14560 -32280 12590 2000 13390 9050 "" "5" ""]
Pin[-14560 -32280 12590 2000 13390 9050 "" "6" ""]
Pin[-26770 -23220 12590 2000 13390 9050 "" "7" ""]
Pin[-29920 18890 12590 2000 13390 9050 "" "8" ""]
Pin[32280 14960 12590 2000 13390 9050 "" "9" ""]
Pin[0 -35430 12590 2000 13390 9050 "" "A" ""]
Pin[9840 33850 12590 2000 13390 9050 "" "A2" ""]
Pin[33850 -9840 12590 2000 13390 9050 "" "N/C" ""]
Pin[-35030 5110 12590 2000 13390 9050 "" "N/C2" ""]
)
