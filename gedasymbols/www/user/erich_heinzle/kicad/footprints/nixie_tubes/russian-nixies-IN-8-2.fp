# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-8-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-8-2
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 2
# Pad count: 12
#
Element["" ">NAME" "" "" 0 0 35620 -3120 1 100 ""]
(
ElementArc[0 0 21213 21213 0 360 250]
ElementArc[0 0 21001 21001 0 360 500]
ElementArc[0 0 25000 25000 0 360 500]
ElementArc[0 0 25000 25000 0 360 1000]
Pin[-11810 -13770 5140 2000 5940 3140 "" "." ""]
Pin[-14760 10820 5140 2000 5940 3140 "" "0" ""]
Pin[7870 15740 5140 2000 5940 3140 "" "1" ""]
Pin[14760 10820 5140 2000 5940 3140 "" "2" ""]
Pin[17710 2950 5140 2000 5940 3140 "" "3" ""]
Pin[16730 -5900 5140 2000 5940 3140 "" "4" ""]
Pin[11810 -13770 5140 2000 5940 3140 "" "5" ""]
Pin[3930 -17710 5140 2000 5940 3140 "" "6" ""]
Pin[-3930 -17710 5140 2000 5940 3140 "" "7" ""]
Pin[-16730 -5900 5140 2000 5940 3140 "" "8" ""]
Pin[-17710 2950 5140 2000 5940 3140 "" "9" ""]
Pin[-7870 15740 5140 2000 5940 3140 "" "A" ""]
)
