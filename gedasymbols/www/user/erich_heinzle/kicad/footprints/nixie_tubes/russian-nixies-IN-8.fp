# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-8
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 2
# Pad count: 11
#
Element["" ">NAME" "" "" 0 0 36250 -3120 1 100 ""]
(
ElementArc[0 0 21213 21213 0 360 250]
ElementArc[0 0 21171 21171 0 360 500]
ElementArc[-420 -270 24610 24610 0 360 500]
ElementArc[-310 0 24690 24690 0 360 1000]
Pin[-16700 6510 5930 2000 6730 3930 "" "0" ""]
Pin[10990 13710 5930 2000 6730 3930 "" "1" ""]
Pin[16620 7500 5930 2000 6730 3930 "" "2" ""]
Pin[17930 -740 5930 2000 6730 3930 "" "3" ""]
Pin[15100 -9240 5930 2000 6730 3930 "" "4" ""]
Pin[8670 -15950 5930 2000 6730 3930 "" "5" ""]
Pin[130 -18120 5930 2000 6730 3930 "" "6" ""]
Pin[-7590 -16480 5930 2000 6730 3930 "" "7" ""]
Pin[-14430 -11140 5930 2000 6730 3930 "" "8" ""]
Pin[-17560 -2290 5930 2000 6730 3930 "" "9" ""]
Pin[-12200 13730 5930 2000 6730 3930 "" "A" ""]
)
