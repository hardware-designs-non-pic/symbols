# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module nixie_tube-ZM1000
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: nixie_tube-ZM1000
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 18
# Draw arc object count: 1
# Pad count: 16
#
Element["" ">NAME" "" "" 0 0 1070 -29740 0 100 ""]
(
ElementLine[0 36410 0 42320 500]
ElementLine[0 36410 980 39370 500]
ElementLine[980 39370 -980 39370 500]
ElementLine[-980 39370 0 36410 500]
ElementArc[0 0 25046 25046 0 360 500]
ElementArc[-5000 20000 2489 2489 0 360 500]
ElementArc[-5000 10000 2489 2489 0 360 500]
ElementArc[5000 10000 2489 2489 0 360 500]
ElementArc[15000 10000 2489 2489 0 360 500]
ElementArc[5000 20000 2489 2489 0 360 500]
ElementArc[-15000 10000 2489 2489 0 360 500]
ElementArc[-15000 0 2489 2489 0 360 500]
ElementArc[-5000 -10000 2489 2489 0 360 500]
ElementArc[-5000 0 2489 2489 0 360 500]
ElementArc[15000 0 2489 2489 0 360 500]
ElementArc[5000 0 2489 2489 0 360 500]
ElementArc[15000 -10000 2489 2489 0 360 500]
ElementArc[-5000 -20000 2489 2489 0 360 500]
ElementArc[-15000 -10000 2489 2489 0 360 500]
ElementArc[5000 -20000 2489 2489 0 360 500]
ElementArc[5000 -10000 2489 2489 0 360 500]
ElementArc[0 0 23504 23504 0 360 500]
ElementArc[0 -270 31491 31491 0 360 500]
Pin[-5000 0 5500 2000 6300 3200 "" "0" ""]
Pin[5000 20000 5500 2000 6300 3200 "" "1" ""]
Pin[5000 10000 5500 2000 6300 3200 "" "2" ""]
Pin[5000 0 5500 2000 6300 3200 "" "3" ""]
Pin[5000 -10000 5500 2000 6300 3200 "" "4" ""]
Pin[5000 -20000 5500 2000 6300 3200 "" "5" ""]
Pin[-5000 -20000 5500 2000 6300 3200 "" "6" ""]
Pin[-5000 -10000 5500 2000 6300 3200 "" "7" ""]
Pin[-5000 20000 5500 2000 6300 3200 "" "8" ""]
Pin[-5000 10000 5500 2000 6300 3200 "" "9" ""]
Pin[-15000 -10000 5500 2000 6300 3200 "" "A" ""]
Pin[15000 -10000 5500 2000 6300 3200 "" "A1" ""]
Pin[-15000 0 5500 2000 6300 3200 "" "DP" ""]
Pin[15000 10000 5500 2000 6300 3200 "" "NC1" ""]
Pin[-15000 10000 5500 2000 6300 3200 "" "NC2" ""]
Pin[15000 0 5500 2000 6300 3200 "" "V" ""]
)
