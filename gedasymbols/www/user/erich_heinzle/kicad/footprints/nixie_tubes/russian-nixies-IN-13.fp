# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module russian-nixies-IN-13
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: russian-nixies-IN-13
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 6
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 13750 10620 0 100 ""]
(
ElementLine[25000 -7500 22500 -7500 500]
ElementLine[1250 -7500 0 -7500 500]
ElementLine[25000 -7500 22500 -7500 1000]
ElementLine[22500 -7500 1250 -7500 1000]
ElementLine[1250 -7500 0 -7500 1000]
ElementLine[0 6250 25000 6250 1000]
ElementLine[0 -5000 25000 -5000 1000]
ElementArc[0 -620 6870 6870 0 360 500]
ElementArc[25000 -620 6880 6880 0 360 500]
ElementArc[25000 -620 6870 6870 0 360 1000]
ElementArc[25000 -620 6880 6880 0 360 1000]
ElementArc[0 -620 6870 6870 0 360 1000]
ElementArc[0 -620 6870 6870 0 360 1000]
Pin[0 0 4360 2000 5160 2360 "" "A" ""]
Pad[0 -2180 0 2180 4360 2000 5160 "" "A" ""]
Pad[0 -2180 0 2180 4360 2000 5160 "" "A" ",onsolder"]
Pin[12500 0 4360 2000 5160 2360 "" "K" ""]
Pad[12500 -2180 12500 2180 4360 2000 5160 "" "K" ""]
Pad[12500 -2180 12500 2180 4360 2000 5160 "" "K" ",onsolder"]
Pin[25000 0 4360 2000 5160 2360 "" "K2" ""]
Pad[25000 -2180 25000 2180 4360 2000 5160 "" "K2" ""]
Pad[25000 -2180 25000 2180 4360 2000 5160 "" "K2" ",onsolder"]
)
