# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder5x20_vert_closed_Bulgin_FX0456_RevB_Date05Jul2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder5x20_vert_closed_Bulgin_FX0456_RevB_Date05Jul2010
# Text descriptor count: 1
# Draw segment object count: 39
# Draw circle object count: 4
# Draw arc object count: 0
# Pad count: 6
#
Element["" "F" "" "" 0 0 1100 -32300 0 100 ""]
(
ElementLine[11000 -18500 -8000 20000 1500]
ElementLine[7000 -20500 -11500 18000 1500]
ElementLine[20000 -8000 19000 -10500 1500]
ElementLine[19000 -10500 17000 -13500 1500]
ElementLine[17000 -13500 14000 -16500 1500]
ElementLine[14000 -16500 10500 -19000 1500]
ElementLine[10500 -19000 6000 -21000 1500]
ElementLine[6000 -21000 3000 -21500 1500]
ElementLine[3000 -21500 -4000 -21500 1500]
ElementLine[-4000 -21500 -7000 -20500 1500]
ElementLine[-7000 -20500 -9500 -19500 1500]
ElementLine[-9500 -19500 -13000 -17500 1500]
ElementLine[-13000 -17500 -16000 -15000 1500]
ElementLine[-16000 -15000 -18000 -12000 1500]
ElementLine[-18000 -12000 -20500 -8000 1500]
ElementLine[-20000 8500 -19000 10500 1500]
ElementLine[-19000 10500 -18000 12000 1500]
ElementLine[-18000 12000 -16500 14000 1500]
ElementLine[-16500 14000 -14500 16000 1500]
ElementLine[-14500 16000 -11000 18500 1500]
ElementLine[-11000 18500 -8500 20000 1500]
ElementLine[-8500 20000 -5500 21000 1500]
ElementLine[-5500 21000 -3000 21500 1500]
ElementLine[-3000 21500 -500 21500 1500]
ElementLine[-500 21500 1500 21500 1500]
ElementLine[1500 21500 3500 21500 1500]
ElementLine[3500 21500 5000 21000 1500]
ElementLine[5000 21000 9000 19500 1500]
ElementLine[9000 19500 11500 18000 1500]
ElementLine[11500 18000 15000 15500 1500]
ElementLine[15000 15500 18000 12000 1500]
ElementLine[18000 12000 20000 8000 1500]
ElementLine[-25000 -25000 -25000 -6500 1500]
ElementLine[-25000 25000 -25000 6500 1500]
ElementLine[25000 25000 25000 6000 1500]
ElementLine[25000 -25000 25000 -6000 1500]
ElementLine[25000 -6000 25000 -6500 1500]
ElementLine[25000 -25000 -25000 -25000 1500]
ElementLine[-25000 25000 25000 25000 1500]
ElementArc[-19700 19700 4472 4472 0 360 1500]
ElementArc[19700 19700 4478 4478 0 360 1500]
ElementArc[-19700 -19700 4494 4494 0 360 1500]
ElementArc[19700 -19700 4460 4460 0 360 1500]
Pin[20000 0 11810 2000 12610 5310 "" "2" ""]
Pin[-20000 0 11810 2000 12610 5310 "" "1" ""]
Pin[19700 -19700 8660 2000 9460 8660 "" "3" ""]
Pin[-19700 -19700 8660 2000 9460 8660 "" "3" ""]
Pin[-19700 19700 8660 2000 9460 8660 "" "3" ""]
Pin[19700 19700 8660 2000 9460 8660 "" "3" ""]
)
