# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder5x20_horiz_SemiClosed_Casing10x25mm_RevA_Date25Jul2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder5x20_horiz_SemiClosed_Casing10x25mm_RevA_Date25Jul2010
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "F" "" "" 0 0 0 -30000 0 100 ""]
(
ElementLine[-23620 -9840 -23620 -19690 1500]
ElementLine[-23620 19690 -23620 9840 1500]
ElementLine[23620 19690 23620 9840 1500]
ElementLine[23620 -19690 23620 -9840 1500]
ElementLine[-17720 0 17720 0 1500]
ElementLine[-17720 -9840 -17720 9840 1500]
ElementLine[17720 -9840 17720 9840 1500]
ElementLine[39370 -7480 39370 -9840 1500]
ElementLine[-39370 7480 -39370 9840 1500]
ElementLine[-39370 9840 39370 9840 1500]
ElementLine[39370 9840 39370 7480 1500]
ElementLine[39370 -9840 -39370 -9840 1500]
ElementLine[-39370 -9840 -39370 -7480 1500]
ElementLine[47240 -7480 47240 -19690 1500]
ElementLine[-47240 7480 -47240 19690 1500]
ElementLine[-47240 19690 47240 19690 1500]
ElementLine[47240 19690 47240 7480 1500]
ElementLine[47240 -19690 -47240 -19690 1500]
ElementLine[-47240 -19690 -47240 -7480 1500]
Pin[43310 0 9840 2000 10640 5910 "" "2" ""]
Pin[-43310 0 9840 2000 10640 5910 "" "1" ""]
)
