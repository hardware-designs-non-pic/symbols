# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder_Fuse_TR5_Littlefuse-No560_No460_RevA_28Aug2012
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder_Fuse_TR5_Littlefuse-No560_No460_RevA_28Aug2012
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "F" "" "" 0 0 -590 -27130 0 100 ""]
(
ElementLine[15160 10980 15630 11540 1500]
ElementLine[15630 11540 15980 12170 1500]
ElementLine[15980 12170 16180 13150 1500]
ElementLine[16180 13150 16060 14170 1500]
ElementLine[16060 14170 15470 15200 1500]
ElementLine[15470 15200 14610 15870 1500]
ElementLine[14610 15870 13460 16180 1500]
ElementLine[13460 16180 12320 16020 1500]
ElementLine[12320 16020 11610 15710 1500]
ElementLine[11610 15710 11100 15120 1500]
ElementArc[0 40 18700 18700 0 360 1500]
Pin[-10000 0 5910 2000 6710 3940 "" "1" ""]
Pin[10000 40 5910 2000 6710 3940 "" "2" ""]
)
