# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder6x32_Horiz_Open_Schurter0031-8002_RevB_Date05Jul2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder6x32_Horiz_Open_Schurter0031-8002_RevB_Date05Jul2010
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "F" "" "" 0 0 0 -40000 0 100 ""]
(
ElementLine[-86000 10000 -86000 30000 1500]
ElementLine[-86000 -30000 -86000 -10000 1500]
ElementLine[-86000 -10000 -80000 -10000 1500]
ElementLine[-80000 10000 -86000 10000 1500]
ElementLine[86000 10000 86000 30000 1500]
ElementLine[86000 -30000 86000 -10000 1500]
ElementLine[86000 -10000 80000 -10000 1500]
ElementLine[80000 10000 86000 10000 1500]
ElementLine[-86000 -21500 86000 -21500 1500]
ElementLine[86000 21500 -86000 21500 1500]
ElementLine[86000 30000 -86000 30000 1500]
ElementLine[-86000 -30000 86000 -30000 1500]
Pin[0 0 12800 2000 13600 12800 "" "" "hole"]
Pin[74000 0 12800 2000 13600 5120 "" "2" ""]
Pin[-74000 0 12800 2000 13600 5120 "" "1" ""]
)
