# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder5x20_horiz_open_lateral_Type-II_RevC_Date24Feb2011
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder5x20_horiz_open_lateral_Type-II_RevC_Date24Feb2011
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "F" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[12800 0 -12800 0 1200]
ElementLine[50660 13500 35660 13500 1500]
ElementLine[12660 13500 24660 13500 1500]
ElementLine[22500 10000 23660 10000 1500]
ElementLine[47160 10000 36660 10000 1500]
ElementLine[12660 -10000 23660 -10000 1500]
ElementLine[47150 -10000 36650 -10000 1500]
ElementLine[50660 -13500 36160 -13500 1500]
ElementLine[12660 -13500 24160 -13500 1500]
ElementLine[-50660 13500 -36160 13500 1500]
ElementLine[-13160 13500 -24160 13500 1500]
ElementLine[-23160 10000 -23660 10000 1500]
ElementLine[-23160 10000 -13160 10000 1500]
ElementLine[-47660 10000 -36660 10000 1500]
ElementLine[-50660 -13500 -35660 -13500 1500]
ElementLine[-13160 -13500 -24160 -13500 1500]
ElementLine[-13160 -10000 -23660 -10000 1500]
ElementLine[-47660 -10000 -36660 -10000 1500]
ElementLine[50660 -13500 50660 13500 1500]
ElementLine[12660 13500 12660 10000 1500]
ElementLine[12660 -13500 12660 -10000 1500]
ElementLine[-50660 -13500 -50660 13500 1500]
ElementLine[-13160 13500 -13160 10000 1500]
ElementLine[-13160 -13500 -13160 -9500 1500]
ElementLine[22500 10000 -23500 10000 1500]
ElementLine[-23000 -10000 22500 -10000 1500]
ElementLine[-13160 0 -13160 -10000 1500]
ElementLine[-47650 -10000 -47650 10000 1500]
ElementLine[-13160 10000 -13160 0 1500]
ElementLine[12660 0 12660 -10000 1500]
ElementLine[47150 -10000 47150 10000 1500]
ElementLine[12660 10000 12660 0 1500]
Pin[30160 -10000 9250 2000 10050 5310 "" "2" ""]
Pin[30160 10000 9250 2000 10050 5310 "" "2" ""]
Pin[-30160 -10000 9250 2000 10050 5310 "" "1" ""]
Pin[-30160 10000 9250 2000 10050 5310 "" "1" ""]
)
