# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuse_TE5_Littlefuse-395Series_RevA_28Aug2012
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuse_TE5_Littlefuse-395Series_RevA_28Aug2012
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "F" "" "" 0 0 390 -13390 0 100 ""]
(
ElementLine[-16730 7870 16730 7870 1500]
ElementLine[16730 7870 16730 -7870 1500]
ElementLine[16730 -7870 -16730 -7870 1500]
ElementLine[-16730 -7870 -16730 7870 1500]
Pin[-10000 0 5910 2000 6710 3940 "" "1" ""]
Pin[10000 40 5910 2000 6710 3940 "" "2" ""]
)
