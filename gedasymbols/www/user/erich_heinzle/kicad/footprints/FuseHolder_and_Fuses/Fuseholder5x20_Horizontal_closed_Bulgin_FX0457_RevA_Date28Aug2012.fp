# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder5x20_Horizontal_closed_Bulgin_FX0457_RevA_Date28Aug2012
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder5x20_Horizontal_closed_Bulgin_FX0457_RevA_Date28Aug2012
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "F" "" "" 0 0 1100 -32300 0 100 ""]
(
ElementLine[109060 -3150 105910 -3150 1500]
ElementLine[105910 -3150 105910 3150 1500]
ElementLine[105910 3150 109060 3150 1500]
ElementLine[77560 24800 77560 -24800 1500]
ElementLine[109060 -24800 -29130 -24800 1500]
ElementLine[109060 24800 109060 -24800 1500]
ElementLine[-29130 24800 109060 24800 1500]
ElementLine[-29130 24800 -29130 -24800 1500]
Pin[20000 0 11810 2000 12610 5310 "" "2" ""]
Pin[-20000 0 11810 2000 12610 5310 "" "1" ""]
)
