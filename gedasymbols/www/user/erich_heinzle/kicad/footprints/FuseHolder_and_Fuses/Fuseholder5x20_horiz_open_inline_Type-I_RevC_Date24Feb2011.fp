# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Fuseholder5x20_horiz_open_inline_Type-I_RevC_Date24Feb2011
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Fuseholder5x20_horiz_open_inline_Type-I_RevC_Date24Feb2011
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "F" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[12800 0 -12800 0 1200]
ElementLine[12800 -13780 12800 13780 1200]
ElementLine[47240 13780 12800 13780 1200]
ElementLine[47240 -13780 12800 -13780 1200]
ElementLine[-42320 9840 -47240 9840 1200]
ElementLine[-41340 -9840 -47240 -9840 1200]
ElementLine[5910 9840 -42320 9840 1200]
ElementLine[4920 -9840 -41340 -9840 1200]
ElementLine[47240 9840 5910 9840 1200]
ElementLine[47240 -9840 4920 -9840 1200]
ElementLine[47240 -9840 47240 9840 1200]
ElementLine[51180 -13780 47240 -13780 1200]
ElementLine[51180 -13780 51180 13780 1200]
ElementLine[51180 13780 47240 13780 1200]
ElementLine[-47240 -9840 -47240 9840 1200]
ElementLine[-12800 -13780 -51180 -13780 1200]
ElementLine[-51180 -13780 -51180 13780 1200]
ElementLine[-12800 13780 -51180 13780 1200]
ElementLine[-12800 -13780 -12800 13780 1200]
Pin[19690 0 9250 2000 10050 5310 "" "2" ""]
Pin[39370 0 9250 2000 10050 5310 "" "2" ""]
Pin[-19690 0 9250 2000 10050 5310 "" "1" ""]
Pin[-39370 0 9250 2000 10050 5310 "" "1" ""]
)
