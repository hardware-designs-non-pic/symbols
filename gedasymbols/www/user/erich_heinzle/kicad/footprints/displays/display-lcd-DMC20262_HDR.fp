# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module display-lcd-DMC20262_HDR
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: display-lcd-DMC20262_HDR
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 14
#
Element["" ">VALUE" "" "" 0 0 22500 -22500 1 100 ""]
(
ElementLine[-5000 -35000 15000 -35000 1000]
ElementLine[15000 -35000 15000 35000 1000]
ElementLine[15000 35000 -5000 35000 1000]
ElementLine[-5000 35000 -5000 -35000 1000]
Pin[10000 30000 6000 2000 6800 4000 "" "1" "square"]
Pin[0 30000 6000 2000 6800 4000 "" "2" ""]
Pin[10000 20000 6000 2000 6800 4000 "" "3" ""]
Pin[0 20000 6000 2000 6800 4000 "" "4" ""]
Pin[10000 10000 6000 2000 6800 4000 "" "5" ""]
Pin[0 10000 6000 2000 6800 4000 "" "6" ""]
Pin[10000 0 6000 2000 6800 4000 "" "7" ""]
Pin[0 0 6000 2000 6800 4000 "" "8" ""]
Pin[10000 -10000 6000 2000 6800 4000 "" "9" ""]
Pin[0 -10000 6000 2000 6800 4000 "" "10" ""]
Pin[10000 -20000 6000 2000 6800 4000 "" "11" ""]
Pin[0 -20000 6000 2000 6800 4000 "" "12" ""]
Pin[10000 -30000 6000 2000 6800 4000 "" "13" ""]
Pin[0 -30000 6000 2000 6800 4000 "" "14" ""]
)
