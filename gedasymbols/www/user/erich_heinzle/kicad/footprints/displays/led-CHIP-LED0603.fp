# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-CHIP-LED0603
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-CHIP-LED0603
# Text descriptor count: 1
# Draw segment object count: 22
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -7500 1 100 ""]
(
ElementLine[-1770 -1770 1770 -1770 260]
ElementLine[1770 -1770 1770 -3340 260]
ElementLine[-1770 -3340 1770 -3340 260]
ElementLine[-1770 -1770 -1770 -3340 260]
ElementLine[-1770 3340 1770 3340 260]
ElementLine[1770 3340 1770 1770 260]
ElementLine[-1770 1770 1770 1770 260]
ElementLine[-1770 3340 -1770 1770 260]
ElementLine[-1770 0 -1180 0 260]
ElementLine[-1180 0 -1180 -1180 260]
ElementLine[-1770 -1180 -1180 -1180 260]
ElementLine[-1770 0 -1770 -1180 260]
ElementLine[1180 0 1770 0 260]
ElementLine[1770 0 1770 -1180 260]
ElementLine[1180 -1180 1770 -1180 260]
ElementLine[1180 0 1180 -1180 260]
ElementLine[-590 0 590 0 260]
ElementLine[590 0 590 -1180 260]
ElementLine[-590 -1180 590 -1180 260]
ElementLine[-590 0 -590 -1180 260]
ElementLine[-1570 -1770 -1570 1770 400]
ElementLine[1570 -1770 1570 1770 400]
Pad[0 2950 0 2950 3140 2000 3940 "" "A" "square"]
Pad[0 -2950 0 -2950 3140 2000 3940 "" "C" "square"]
)
