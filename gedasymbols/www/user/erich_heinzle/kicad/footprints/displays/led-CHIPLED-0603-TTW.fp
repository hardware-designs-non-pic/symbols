# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-CHIPLED-0603-TTW
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-CHIPLED-0603-TTW
# Text descriptor count: 1
# Draw segment object count: 62
# Draw circle object count: 1
# Draw arc object count: 2
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 -7500 -7500 1 100 ""]
(
ElementLine[-1770 -2750 -980 -2750 260]
ElementLine[-980 -2750 -980 -3340 260]
ElementLine[-1770 -3340 -980 -3340 260]
ElementLine[-1770 -2750 -1770 -3340 260]
ElementLine[-1080 -2160 -880 -2160 260]
ElementLine[-880 -2160 -880 -2360 260]
ElementLine[-1080 -2360 -880 -2360 260]
ElementLine[-1080 -2160 -1080 -2360 260]
ElementLine[-1770 -1370 -1570 -1370 260]
ElementLine[-1570 -1370 -1570 -2850 260]
ElementLine[-1770 -2850 -1570 -2850 260]
ElementLine[-1770 -1370 -1770 -2850 260]
ElementLine[980 -2160 1770 -2160 260]
ElementLine[1770 -2160 1770 -3340 260]
ElementLine[980 -3340 1770 -3340 260]
ElementLine[980 -2160 980 -3340 260]
ElementLine[-1770 -1370 1770 -1370 260]
ElementLine[1770 -1370 1770 -2260 260]
ElementLine[-1770 -2260 1770 -2260 260]
ElementLine[-1770 -1370 -1770 -2260 260]
ElementLine[-1770 3340 -980 3340 260]
ElementLine[-980 3340 -980 1370 260]
ElementLine[-1770 1370 -980 1370 260]
ElementLine[-1770 3340 -1770 1370 260]
ElementLine[980 3340 1770 3340 260]
ElementLine[1770 3340 1770 1370 260]
ElementLine[980 1370 1770 1370 260]
ElementLine[980 3340 980 1370 260]
ElementLine[-1080 2260 1080 2260 260]
ElementLine[1080 2260 1080 1370 260]
ElementLine[-1080 1370 1080 1370 260]
ElementLine[-1080 2260 -1080 1370 260]
ElementLine[-1080 2550 -680 2550 260]
ElementLine[-680 2550 -680 2160 260]
ElementLine[-1080 2160 -680 2160 260]
ElementLine[-1080 2550 -1080 2160 260]
ElementLine[680 2550 1080 2550 260]
ElementLine[1080 2550 1080 2160 260]
ElementLine[680 2160 1080 2160 260]
ElementLine[680 2550 680 2160 260]
ElementLine[-490 0 490 0 260]
ElementLine[490 0 490 -980 260]
ElementLine[-490 -980 490 -980 260]
ElementLine[-490 0 -490 -980 260]
ElementLine[-680 -1270 680 -1270 260]
ElementLine[680 -1270 680 -2750 260]
ElementLine[-680 -2750 680 -2750 260]
ElementLine[-680 -1270 -680 -2750 260]
ElementLine[-1570 -2460 1570 -2460 260]
ElementLine[1570 -2460 1570 -4420 260]
ElementLine[-1570 -4420 1570 -4420 260]
ElementLine[-1570 -2460 -1570 -4420 260]
ElementLine[-1570 4420 1570 4420 260]
ElementLine[1570 4420 1570 2460 260]
ElementLine[-1570 2460 1570 2460 260]
ElementLine[-1570 4420 -1570 2460 260]
ElementLine[-680 2650 680 2650 260]
ElementLine[680 2650 680 1270 260]
ElementLine[-680 1270 680 1270 260]
ElementLine[-680 2650 -680 1270 260]
ElementLine[-1570 -1470 -1570 1370 400]
ElementLine[1570 -1370 1570 1370 400]
ElementArc[-1370 -2460 198 198 0 360 100]
ElementArc[0 -3240 1180 1180 180 -180 400]
ElementArc[0 3240 1080 1080 0 -180 200]
Pad[-590 3440 590 3440 1960 2000 2760 "" "A" "square"]
Pad[0 1960 0 1960 1370 2000 2170 "" "A@1" "square"]
Pad[-590 -3440 590 -3440 1960 2000 2760 "" "C" "square"]
Pad[0 -1960 0 -1960 1370 2000 2170 "" "C@1" "square"]
)
