# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_nokia_6100-DF23C-10DS-0.5V
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_nokia_6100-DF23C-10DS-0.5V
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" ">NAME" "" "" 0 0 2500 -15000 0 100 ""]
(
ElementLine[-9840 -10030 -9840 -5900 800]
ElementLine[-9840 -10030 9840 -10030 800]
ElementLine[9840 -10030 9840 -5900 800]
ElementLine[-9840 10030 -9840 5900 800]
ElementLine[-9840 10030 9840 10030 800]
ElementLine[9840 10030 9840 5900 800]
Pad[-12985 -3930 -7875 -3930 1180 2000 1980 "" "1" "square"]
Pad[-12985 -1960 -7875 -1960 1180 2000 1980 "" "2" "square"]
Pad[-12985 0 -7875 0 1180 2000 1980 "" "3" "square"]
Pad[-12985 1960 -7875 1960 1180 2000 1980 "" "4" "square"]
Pad[-12985 3930 -7875 3930 1180 2000 1980 "" "5" "square"]
Pad[7875 3930 12985 3930 1180 2000 1980 "" "6" "square"]
Pad[7875 1960 12985 1960 1180 2000 1980 "" "7" "square"]
Pad[7875 0 12985 0 1180 2000 1980 "" "8" "square"]
Pad[7875 -1960 12985 -1960 1180 2000 1980 "" "9" "square"]
Pad[7875 -3930 12985 -3930 1180 2000 1980 "" "10" "square"]
)
