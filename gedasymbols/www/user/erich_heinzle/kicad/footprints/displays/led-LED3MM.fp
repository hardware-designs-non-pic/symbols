# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LED3MM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LED3MM
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 0
# Draw arc object count: 18
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 22500 -7500 0 100 ""]
(
ElementLine[6200 5000 6200 -5000 1000]
ElementArc[0 0 6000 6000 0 -40 600]
ElementArc[0 0 5993 5993 0 -42 600]
ElementArc[0 0 5993 5993 180 -41 600]
ElementArc[0 0 6000 6000 180 -40 600]
ElementArc[0 0 6000 6000 270 -54 600]
ElementArc[0 0 6000 6000 0 -53 600]
ElementArc[0 0 5993 5993 180 -52 600]
ElementArc[0 0 6000 6000 90 -52 600]
ElementArc[0 0 2500 2500 0 -90 600]
ElementArc[0 0 4000 4000 0 -90 600]
ElementArc[0 0 2500 2500 180 -90 600]
ElementArc[0 0 4000 4000 180 -90 600]
ElementArc[0 0 8000 8000 270 -50 1000]
ElementArc[0 0 7990 7990 0 -62 1000]
ElementArc[0 0 7990 7990 180 -50 1000]
ElementArc[0 0 8000 8000 90 -60 1000]
ElementArc[0 0 8000 8000 0 -28 1000]
ElementArc[0 0 7996 7996 0 -32 1000]
Pin[-5000 0 6600 2000 7400 3200 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ",onsolder"]
Pin[5000 0 6600 2000 7400 3200 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ",onsolder"]
)
