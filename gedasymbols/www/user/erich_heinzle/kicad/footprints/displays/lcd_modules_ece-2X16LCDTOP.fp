# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_modules_ece-2X16LCDTOP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_modules_ece-2X16LCDTOP
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 4
# Draw arc object count: 0
# Pad count: 16
#
Element["" ">NAME" "" "" 0 0 -8990 -13520 0 100 ""]
(
ElementLine[0 0 315000 0 500]
ElementLine[315000 0 315000 -140000 500]
ElementLine[315000 -140000 0 -140000 500]
ElementLine[0 -140000 0 0 500]
ElementLine[5000 -130000 15000 -130000 500]
ElementLine[10000 -125000 10000 -135000 500]
ElementLine[300000 -130000 310000 -130000 500]
ElementLine[305000 -125000 305000 -135000 500]
ElementLine[300000 -10000 310000 -10000 500]
ElementLine[305000 -5000 305000 -15000 500]
ElementLine[5000 -10000 15000 -10000 500]
ElementLine[10000 -5000 10000 -15000 500]
ElementArc[10000 -130000 3536 3536 0 360 500]
ElementArc[305000 -130000 3536 3536 0 360 500]
ElementArc[305000 -10000 3536 3536 0 360 500]
ElementArc[10000 -10000 3536 3536 0 360 500]
Pin[30980 -130000 7000 2000 7800 3340 "" "1" "square"]
Pin[40980 -130000 7000 2000 7800 3340 "" "2" "square"]
Pin[50980 -130000 7000 2000 7800 3340 "" "3" "square"]
Pin[60980 -130000 7000 2000 7800 3340 "" "4" "square"]
Pin[70980 -130000 7000 2000 7800 3340 "" "5" "square"]
Pin[80980 -130000 7000 2000 7800 3340 "" "6" "square"]
Pin[90980 -130000 7000 2000 7800 3340 "" "7" "square"]
Pin[100980 -130000 7000 2000 7800 3340 "" "8" "square"]
Pin[110980 -130000 7000 2000 7800 3340 "" "9" "square"]
Pin[120980 -130000 7000 2000 7800 3340 "" "10" "square"]
Pin[130980 -130000 7000 2000 7800 3340 "" "11" "square"]
Pin[140980 -130000 7000 2000 7800 3340 "" "12" "square"]
Pin[150980 -130000 7000 2000 7800 3340 "" "13" "square"]
Pin[160980 -130000 7000 2000 7800 3340 "" "14" "square"]
Pin[170980 -130000 7000 2000 7800 3340 "" "15" "square"]
Pin[180980 -130000 7000 2000 7800 3340 "" "16" "square"]
)
