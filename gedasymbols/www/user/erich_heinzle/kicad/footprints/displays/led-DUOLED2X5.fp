# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-DUOLED2X5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-DUOLED2X5
# Text descriptor count: 1
# Draw segment object count: 37
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 0 -12000 0 100 ""]
(
ElementLine[-10000 5000 10000 5000 600]
ElementLine[10000 -5000 -10000 -5000 600]
ElementLine[-8500 0 -3000 0 600]
ElementLine[-7000 -1500 -7000 1500 600]
ElementLine[-7000 -1500 -3000 0 600]
ElementLine[-3000 0 -7000 1500 600]
ElementLine[-3000 -1500 -3000 0 600]
ElementLine[-3000 0 -3000 1500 600]
ElementLine[-6000 2000 -5000 4000 600]
ElementLine[-5000 4000 -5000 3000 600]
ElementLine[-5000 4000 -5800 3400 600]
ElementLine[-5800 3400 -5000 3000 600]
ElementLine[-4000 2000 -3000 4000 600]
ElementLine[-3000 4000 -3000 3000 600]
ElementLine[-3000 4000 -3800 3400 600]
ElementLine[-3800 3400 -3000 3000 600]
ElementLine[7000 1500 7000 -1500 600]
ElementLine[7000 1500 3000 0 600]
ElementLine[3000 0 7000 -1500 600]
ElementLine[3000 1500 3000 0 600]
ElementLine[3000 0 3000 -1500 600]
ElementLine[6000 -2000 5000 -4000 600]
ElementLine[5000 -4000 5000 -3000 600]
ElementLine[5000 -4000 5800 -3400 600]
ElementLine[5800 -3400 5000 -3000 600]
ElementLine[4000 -2000 3000 -4000 600]
ElementLine[3000 -4000 3000 -3000 600]
ElementLine[3000 -4000 3800 -3400 600]
ElementLine[3800 -3400 3000 -3000 600]
ElementLine[3000 0 -3000 0 600]
ElementLine[10000 -5000 10000 -3500 600]
ElementLine[10000 5000 10000 3500 600]
ElementLine[10000 3500 10000 -3500 600]
ElementLine[-10000 -5000 -10000 -3500 600]
ElementLine[-10000 5000 -10000 3500 600]
ElementLine[-10000 3500 -10000 -3500 600]
ElementLine[8500 0 3000 0 600]
Pin[-10000 0 5200 2000 6000 3200 "" "AG" ""]
Pad[-10000 -2600 -10000 2600 5200 2000 6000 "" "AG" ""]
Pad[-10000 -2600 -10000 2600 5200 2000 6000 "" "AG" ",onsolder"]
Pin[10000 0 5200 2000 6000 3200 "" "AR" ""]
Pad[10000 -2600 10000 2600 5200 2000 6000 "" "AR" ""]
Pad[10000 -2600 10000 2600 5200 2000 6000 "" "AR" ",onsolder"]
Pin[0 0 5200 2000 6000 3200 "" "K" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "K" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "K" ",onsolder"]
)
