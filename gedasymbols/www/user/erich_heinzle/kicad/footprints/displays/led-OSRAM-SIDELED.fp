# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-OSRAM-SIDELED
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-OSRAM-SIDELED
# Text descriptor count: 1
# Draw segment object count: 36
# Draw circle object count: 0
# Draw arc object count: 1
# Pad count: 2
#
Element["" "C" "" "" 0 0 0 10000 1 100 ""]
(
ElementLine[-8260 8660 8260 8660 260]
ElementLine[8260 8660 8260 1570 260]
ElementLine[-8260 1570 8260 1570 260]
ElementLine[-8260 8660 -8260 1570 260]
ElementLine[-8260 -1570 8260 -1570 260]
ElementLine[8260 -1570 8260 -8660 260]
ElementLine[-8260 -8660 8260 -8660 260]
ElementLine[-8260 -1570 -8260 -8660 260]
ElementLine[-7480 8260 7480 8260 260]
ElementLine[7480 8260 7480 2360 260]
ElementLine[-7480 2360 7480 2360 260]
ElementLine[-7480 8260 -7480 2360 260]
ElementLine[-7480 -2360 7480 -2360 260]
ElementLine[7480 -2360 7480 -8260 260]
ElementLine[-7480 -8260 7480 -8260 260]
ElementLine[-7480 -2360 -7480 -8260 260]
ElementLine[-7280 8070 -2750 8070 260]
ElementLine[-2750 8070 -2750 3930 260]
ElementLine[-7280 3930 -2750 3930 260]
ElementLine[-7280 8070 -7280 3930 260]
ElementLine[-7280 8070 -7280 2950 400]
ElementLine[-7280 2950 -6690 2950 400]
ElementLine[-6690 2950 -6690 -2950 400]
ElementLine[-6690 -2950 -7280 -2950 400]
ElementLine[-7280 -2950 -7280 -8070 400]
ElementLine[-7280 -8070 3540 -8070 400]
ElementLine[3540 -8070 3540 8070 400]
ElementLine[3540 8070 -7280 8070 400]
ElementLine[3540 8070 4130 8070 400]
ElementLine[4130 8070 7280 7280 400]
ElementLine[7280 7280 7280 -7280 400]
ElementLine[7280 -7280 4130 -8070 400]
ElementLine[4130 -8070 3540 -8070 400]
ElementLine[4130 -8070 4130 8070 400]
ElementLine[-2160 3540 3340 4720 400]
ElementLine[-2160 -3540 3340 -4720 400]
ElementArc[-1770 0 3561 3561 84 -167 400]
Pad[0 -9840 0 -9840 15740 2000 16540 "" "A" "square"]
Pad[0 9840 0 9840 15740 2000 16540 "" "C" "square"]
)
