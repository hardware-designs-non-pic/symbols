# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 7segdisp_HDSP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 7segdisp_HDSP
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" "DISP***" "" "" 0 0 0 -25590 0 100 ""]
(
ElementLine[-14000 -21700 14000 -21700 1000]
ElementLine[-14000 21700 -14000 -21700 1000]
ElementLine[14000 21700 -14000 21700 1000]
ElementLine[14000 -21700 14000 21700 1000]
ElementLine[-9500 13000 -7000 2000 1200]
ElementLine[3500 13000 6000 2000 1200]
ElementLine[6500 -1500 9000 -13000 1200]
ElementLine[-6500 -1500 -4000 -13000 1200]
ElementLine[-8500 15000 2000 15000 1200]
ElementLine[-2500 -15000 8000 -15000 1200]
ElementLine[-5500 0 5000 0 1200]
ElementArc[10500 15500 1414 1414 0 360 1200]
Pin[-10400 -15799 5910 2000 9460 3150 "" "1" "blah"]
Pad[-11775 -15799 -9025 -15799 5910 2000 6710 "" "1" "square"]
Pad[-11775 -15799 -9025 -15799 5910 2000 6710 "" "1" "square,onsolder"]
Pin[-10400 -7899 5910 2000 9460 3150 "" "2" ""]
Pad[-11775 -7899 -9025 -7899 5910 2000 6710 "" "2" ""]
Pad[-11775 -7899 -9025 -7899 5910 2000 6710 "" "2" ",onsolder"]
Pin[-10400 0 5910 2000 9460 3150 "" "3" ""]
Pad[-11775 0 -9025 0 5910 2000 6710 "" "3" ""]
Pad[-11775 0 -9025 0 5910 2000 6710 "" "3" ",onsolder"]
Pin[-10400 7899 5910 2000 9460 3150 "" "4" ""]
Pad[-11775 7899 -9025 7899 5910 2000 6710 "" "4" ""]
Pad[-11775 7899 -9025 7899 5910 2000 6710 "" "4" ",onsolder"]
Pin[-10400 15799 5910 2000 9460 3150 "" "5" ""]
Pad[-11775 15799 -9025 15799 5910 2000 6710 "" "5" ""]
Pad[-11775 15799 -9025 15799 5910 2000 6710 "" "5" ",onsolder"]
Pin[10400 15799 5910 2000 9460 3150 "" "6" ""]
Pad[9025 15799 11775 15799 5910 2000 6710 "" "6" ""]
Pad[9025 15799 11775 15799 5910 2000 6710 "" "6" ",onsolder"]
Pin[10400 7899 5910 2000 9460 3150 "" "7" ""]
Pad[9025 7899 11775 7899 5910 2000 6710 "" "7" ""]
Pad[9025 7899 11775 7899 5910 2000 6710 "" "7" ",onsolder"]
Pin[10400 0 5910 2000 9460 3150 "" "8" ""]
Pad[9025 0 11775 0 5910 2000 6710 "" "8" ""]
Pad[9025 0 11775 0 5910 2000 6710 "" "8" ",onsolder"]
Pin[10400 -7899 5910 2000 9460 3150 "" "9" ""]
Pad[9025 -7899 11775 -7899 5910 2000 6710 "" "9" ""]
Pad[9025 -7899 11775 -7899 5910 2000 6710 "" "9" ",onsolder"]
Pin[10400 -15799 5910 2000 9460 3150 "" "10" ""]
Pad[9025 -15799 11775 -15799 5910 2000 6710 "" "10" ""]
Pad[9025 -15799 11775 -15799 5910 2000 6710 "" "10" ",onsolder"]
)
