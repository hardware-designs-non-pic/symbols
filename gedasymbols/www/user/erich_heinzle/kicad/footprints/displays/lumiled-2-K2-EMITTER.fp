# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lumiled-2-K2-EMITTER
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lumiled-2-K2-EMITTER
# Text descriptor count: 0
# Draw segment object count: 19
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 5
#
Element["" "lumiled-2-K2-EMITTER" "" "" 0 0 0 25000 0 100 ""]
(
ElementLine[14760 14760 14760 -10030 500]
ElementLine[14760 -10030 10030 -14760 500]
ElementLine[10030 -14760 -10030 -14760 500]
ElementLine[-10030 -14760 -14760 -10030 500]
ElementLine[-14760 -10030 -14760 10030 500]
ElementLine[-14760 10030 -10030 14760 500]
ElementLine[-10030 14760 14760 14760 500]
ElementLine[-27550 19680 27550 19680 500]
ElementLine[27550 19680 27550 9050 500]
ElementLine[27550 9050 13580 9050 500]
ElementLine[13580 9050 13580 -9050 500]
ElementLine[13580 -9050 27550 -9050 500]
ElementLine[27550 -9050 27550 -19680 500]
ElementLine[27550 -19680 -27550 -19680 500]
ElementLine[-27550 -19680 -27550 -9050 500]
ElementLine[-27550 -9050 -13580 -9050 500]
ElementLine[-13580 -9050 -13580 9050 500]
ElementLine[-13580 9050 -27550 9050 500]
ElementLine[-27550 9050 -27550 19680 500]
ElementArc[0 0 7439 7439 0 360 250]
Pad[-22830 -4520 -18110 -4520 6690 2000 7490 "" "P_1" "square"]
Pad[-22830 4520 -18110 4520 6690 2000 7490 "" "P_2" "square"]
Pad[18110 4520 22830 4520 6690 2000 7490 "" "P_3" "square"]
Pad[18110 -4520 22830 -4520 6690 2000 7490 "" "P_4" "square"]
Pad[-2360 0 2360 0 6690 2000 7490 "" "P_5" "square"]
)
