# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-SMART-LED
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-SMART-LED
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7000 -5000 1 100 ""]
(
ElementLine[-590 1370 590 1370 260]
ElementLine[590 1370 590 190 260]
ElementLine[-590 190 590 190 260]
ElementLine[-590 1370 -590 190 260]
ElementLine[-590 -2360 590 -2360 260]
ElementLine[590 -2360 590 -3340 260]
ElementLine[-590 -3340 590 -3340 260]
ElementLine[-590 -2360 -590 -3340 260]
ElementLine[-590 3340 590 3340 260]
ElementLine[590 3340 590 2360 260]
ElementLine[-590 2360 590 2360 260]
ElementLine[-590 3340 -590 2360 260]
ElementLine[-1370 -2360 1370 -2360 400]
ElementLine[1370 -2360 1370 2360 400]
ElementLine[1370 2360 -1370 2360 400]
ElementLine[-1370 2360 -1370 -2360 400]
ElementLine[1370 1570 590 2360 400]
Pad[0 -2850 0 -2850 1370 2000 2170 "" "A" "square"]
Pad[0 2850 0 2850 1370 2000 2170 "" "B" "square"]
)
