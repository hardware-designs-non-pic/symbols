# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_n3200_lph7677-LPH7677
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_n3200_lph7677-LPH7677
# Text descriptor count: 1
# Draw segment object count: 38
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 15
#
Element["" "LED" "" "" 0 0 -51510 71240 0 100 ""]
(
ElementLine[-72440 75190 -56690 75190 260]
ElementLine[-56690 75190 -56690 66140 260]
ElementLine[-72440 66140 -56690 66140 260]
ElementLine[-72440 75190 -72440 66140 260]
ElementLine[63770 75190 79520 75190 260]
ElementLine[79520 75190 79520 65740 260]
ElementLine[63770 65740 79520 65740 260]
ElementLine[63770 75190 63770 65740 260]
ElementLine[40150 41730 49600 41730 260]
ElementLine[49600 41730 49600 12590 260]
ElementLine[40150 12590 49600 12590 260]
ElementLine[40150 41730 40150 12590 260]
ElementLine[56290 -13770 65350 -13770 260]
ElementLine[65350 -13770 65350 -30310 260]
ElementLine[56290 -30310 65350 -30310 260]
ElementLine[56290 -13770 56290 -30310 260]
ElementLine[-77950 -56690 85030 -56690 260]
ElementLine[85030 -56690 85030 -77950 260]
ElementLine[-77950 -77950 85030 -77950 260]
ElementLine[-77950 -56690 -77950 -77950 260]
ElementLine[84250 -77160 62990 -77160 300]
ElementLine[62990 -77160 61020 -77160 300]
ElementLine[62990 -77160 -77160 -77160 300]
ElementLine[-77160 -77160 -77160 33460 300]
ElementLine[-77160 33460 -77160 33850 300]
ElementLine[-77160 33460 -77160 76370 300]
ElementLine[-77160 76370 84250 76370 300]
ElementLine[84250 76370 84250 -77160 300]
ElementLine[-57480 78740 -57480 74800 300]
ElementLine[-43700 78740 -43700 74800 300]
ElementLine[-20070 78740 -20070 74800 300]
ElementLine[-6290 78740 -6290 74800 300]
ElementLine[15350 79130 15350 75190 300]
ElementLine[29130 79130 29130 75190 300]
ElementLine[50780 79130 50780 75190 300]
ElementLine[64560 79130 64560 75190 300]
ElementLine[29130 -63770 37000 -63770 500]
ElementLine[33070 -59840 33070 -67710 500]
ElementArc[33070 -63770 2772 2772 0 360 500]
Pad[13555 -70000 24185 -70000 1180 2000 1980 "" "1VDD" "square"]
Pad[9115 -70000 19745 -70000 1180 2000 1980 "" "2SCLK" "square"]
Pad[4235 -70000 14865 -70000 1180 2000 1980 "" "3SDIN" "square"]
Pad[-315 -70000 10315 -70000 1180 2000 1980 "" "4D/C" "square"]
Pad[-4755 -70000 5875 -70000 1180 2000 1980 "" "5SCE" "square"]
Pad[-9075 -70110 1555 -70110 1180 2000 1980 "" "6GND" "square"]
Pad[-13625 -70000 -2995 -70000 1180 2000 1980 "" "7VOUT" "square"]
Pad[-18175 -70000 -7545 -70000 1180 2000 1980 "" "8RES" "square"]
Pad[71650 -67240 71650 -61340 7870 2000 8670 "" "P$6" "square"]
Pad[-64560 -67240 -64560 -61340 7870 2000 8670 "" "P$7" "square"]
Pad[-63770 -67240 -63770 -61340 7870 2000 8670 "" "P$11" "square"]
Pad[-64560 67520 -64560 73420 7870 2000 8670 "" "P$12" "square"]
Pad[71650 67520 71650 73420 7870 2000 8670 "" "P$13" "square"]
Pad[32540 27160 52220 27160 7870 2000 8670 "" "P$14" "square"]
Pad[56685 -22040 64555 -22040 7870 2000 8670 "" "P$15" "square"]
)
