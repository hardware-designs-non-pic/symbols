# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module kingbright-SA15-11
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: kingbright-SA15-11
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" ">NAME" "" "" 0 0 -47500 -87500 0 100 ""]
(
ElementLine[-65000 92500 -65000 -92500 500]
ElementLine[-65000 -92500 65000 -92500 500]
ElementLine[65000 -92500 65000 92500 500]
ElementLine[65000 92500 -65000 92500 500]
ElementLine[45000 -75000 -35000 -75000 5000]
ElementLine[-45000 75000 35000 75000 5000]
ElementLine[-40000 0 40000 0 5000]
ElementLine[45000 -65000 40000 -10000 5000]
ElementLine[40000 10000 35000 65000 5000]
ElementLine[-35000 -65000 -40000 -10000 5000]
ElementLine[-40000 10000 -45000 65000 5000]
ElementArc[45000 75000 1768 1768 0 360 0]
Pin[-20000 80000 0 2000 6800 4000 "" "1" "blah"]
Pad[-23000 80000 -17000 80000 0 2000 800 "" "1" "square"]
Pad[-23000 80000 -17000 80000 0 2000 800 "" "1" "square,onsolder"]
Pin[-10000 80000 6000 2000 6800 4000 "" "2" ""]
Pin[0 80000 6000 2000 6800 4000 "" "3" ""]
Pin[10000 80000 6000 2000 6800 4000 "" "4" ""]
Pin[20000 80000 6000 2000 6800 4000 "" "5" ""]
Pin[20000 -80000 6000 2000 6800 4000 "" "6" ""]
Pin[10000 -80000 6000 2000 6800 4000 "" "7" ""]
Pin[0 -80000 6000 2000 6800 4000 "" "8" ""]
Pin[-10000 -80000 6000 2000 6800 4000 "" "9" ""]
Pin[-20000 -80000 6000 2000 6800 4000 "" "10" ""]
)
