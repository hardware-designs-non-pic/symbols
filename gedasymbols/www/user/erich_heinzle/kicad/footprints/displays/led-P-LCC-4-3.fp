# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-P-LCC-4-3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-P-LCC-4-3
# Text descriptor count: 1
# Draw segment object count: 67
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 -17500 -2500 1 100 ""]
(
ElementLine[-4520 -2950 -1370 -2950 260]
ElementLine[-1370 -2950 -1370 -7280 260]
ElementLine[-4520 -7280 -1370 -7280 260]
ElementLine[-4520 -2950 -4520 -7280 260]
ElementLine[1370 -2950 4520 -2950 260]
ElementLine[4520 -2950 4520 -7280 260]
ElementLine[1370 -7280 4520 -7280 260]
ElementLine[1370 -2950 1370 -7280 260]
ElementLine[1370 7280 4520 7280 260]
ElementLine[4520 7280 4520 2950 260]
ElementLine[1370 2950 4520 2950 260]
ElementLine[1370 7280 1370 2950 260]
ElementLine[-4520 7280 -1370 7280 260]
ElementLine[-1370 7280 -1370 2950 260]
ElementLine[-4520 2950 -1370 2950 260]
ElementLine[-4520 7280 -4520 2950 260]
ElementLine[-4330 7080 -1570 7080 260]
ElementLine[-1570 7080 -1570 3140 260]
ElementLine[-4330 3140 -1570 3140 260]
ElementLine[-4330 7080 -4330 3140 260]
ElementLine[1570 7080 4330 7080 260]
ElementLine[4330 7080 4330 3140 260]
ElementLine[1570 3140 4330 3140 260]
ElementLine[1570 7080 1570 3140 260]
ElementLine[1570 -3140 4330 -3140 260]
ElementLine[4330 -3140 4330 -7080 260]
ElementLine[1570 -7080 4330 -7080 260]
ElementLine[1570 -3140 1570 -7080 260]
ElementLine[-4330 -3140 -1570 -3140 260]
ElementLine[-1570 -3140 -1570 -7080 260]
ElementLine[-4330 -7080 -1570 -7080 260]
ElementLine[-4330 -3140 -4330 -7080 260]
ElementLine[390 -390 1960 -390 260]
ElementLine[1960 -390 1960 -1960 260]
ElementLine[390 -1960 1960 -1960 260]
ElementLine[390 -390 390 -1960 260]
ElementLine[390 1960 1960 1960 260]
ElementLine[1960 1960 1960 390 260]
ElementLine[390 390 1960 390 260]
ElementLine[390 1960 390 390 260]
ElementLine[-1960 1960 -390 1960 260]
ElementLine[-390 1960 -390 390 260]
ElementLine[-1960 390 -390 390 260]
ElementLine[-1960 1960 -1960 390 260]
ElementLine[-5510 -6290 -5510 2550 800]
ElementLine[-5510 4130 -5510 6290 800]
ElementLine[-5510 6290 -3930 6290 800]
ElementLine[-3930 6290 -3340 6290 800]
ElementLine[-3340 6290 3930 6290 800]
ElementLine[3930 6290 5510 6290 800]
ElementLine[5510 6290 5510 -6290 800]
ElementLine[5510 -6290 -3930 -6290 800]
ElementLine[-3930 -6290 -5510 -6290 800]
ElementLine[-3930 -6290 -3930 -7080 400]
ElementLine[-3930 -7080 -1960 -7080 400]
ElementLine[-1960 -7080 -1960 -6490 400]
ElementLine[1960 -6490 1960 -7080 400]
ElementLine[1960 -7080 4330 -7080 400]
ElementLine[4330 -7080 4330 -6290 400]
ElementLine[-3930 6290 -3930 7080 400]
ElementLine[-3930 7080 -1960 7080 400]
ElementLine[-1960 7080 -1960 6490 400]
ElementLine[1960 6490 1960 7080 400]
ElementLine[1960 7080 3930 7080 400]
ElementLine[3930 7080 3930 6290 400]
ElementLine[-3340 6290 -5510 4130 800]
ElementLine[-5510 4130 -5510 2550 800]
ElementArc[0 0 3055 3055 0 360 400]
Pad[-7870 9450 -7870 15350 12990 2000 13790 "" "1" "square"]
Pad[-7870 -15350 -7870 -9450 12990 2000 13790 "" "2" "square"]
Pad[7870 -15350 7870 -9450 12990 2000 13790 "" "3" "square"]
Pad[7870 9450 7870 15350 12990 2000 13790 "" "4" "square"]
)
