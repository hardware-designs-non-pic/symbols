# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_modules_ece-2X16LCDSIDE
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_modules_ece-2X16LCDSIDE
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 4
# Draw arc object count: 0
# Pad count: 14
#
Element["" ">NAME" "" "" 0 0 -8990 -13520 0 100 ""]
(
ElementLine[0 -140000 0 0 500]
ElementLine[0 -140000 332500 -140000 500]
ElementLine[332500 -140000 332500 0 500]
ElementLine[332500 0 0 0 500]
ElementLine[5000 -10000 15000 -10000 500]
ElementLine[10000 -5000 10000 -15000 500]
ElementLine[5000 -130000 15000 -130000 500]
ElementLine[10000 -125000 10000 -135000 500]
ElementLine[317500 -130000 327500 -130000 500]
ElementLine[322500 -125000 322500 -135000 500]
ElementLine[317500 -10000 327500 -10000 500]
ElementLine[322500 -5000 322500 -15000 500]
ElementArc[10000 -10000 3536 3536 0 360 500]
ElementArc[10000 -130000 3536 3536 0 360 500]
ElementArc[322500 -130000 3536 3536 0 360 500]
ElementArc[322500 -10000 3536 3536 0 360 500]
Pin[20000 -40000 7000 2000 7800 3340 "" "1" "square"]
Pin[10000 -40000 7000 2000 7800 3340 "" "2" "square"]
Pin[20000 -50000 7000 2000 7800 3340 "" "3" "square"]
Pin[10000 -50000 7000 2000 7800 3340 "" "4" "square"]
Pin[20000 -60000 7000 2000 7800 3340 "" "5" "square"]
Pin[10000 -60000 7000 2000 7800 3340 "" "6" "square"]
Pin[20000 -70000 7000 2000 7800 3340 "" "7" "square"]
Pin[10000 -70000 7000 2000 7800 3340 "" "8" "square"]
Pin[20000 -80000 7000 2000 7800 3340 "" "9" "square"]
Pin[10000 -80000 7000 2000 7800 3340 "" "10" "square"]
Pin[20000 -90000 7000 2000 7800 3340 "" "11" "square"]
Pin[10000 -90000 7000 2000 7800 3340 "" "12" "square"]
Pin[20000 -100000 7000 2000 7800 3340 "" "13" "square"]
Pin[10000 -100000 7000 2000 7800 3340 "" "14" "square"]
)
