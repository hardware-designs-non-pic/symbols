# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_2x16_led_backlight-HY-1602F
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_2x16_led_backlight-HY-1602F
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 8
# Draw arc object count: 0
# Pad count: 16
#
Element["" ">NAME" "" "" 0 0 -8990 -13520 0 100 ""]
(
ElementLine[-21490 -9840 293460 -9840 500]
ElementLine[293460 -9840 293460 131880 500]
ElementLine[293460 131880 -21490 131880 500]
ElementLine[-21490 131880 -21490 -9840 500]
ElementLine[-1810 11810 273770 11810 500]
ElementLine[273770 11810 273770 110230 500]
ElementLine[-1810 11810 -1810 110230 500]
ElementLine[-1810 110230 273770 110230 500]
ElementLine[-16570 0 -6730 0 500]
ElementLine[-11650 4920 -11650 -4920 500]
ElementLine[278700 0 288540 0 500]
ElementLine[283620 4920 283620 -4920 500]
ElementLine[-16570 122040 -6730 122040 500]
ElementLine[-11650 126960 -11650 117120 500]
ElementLine[278700 122040 288540 122040 500]
ElementLine[283620 126960 283620 117120 500]
ElementArc[-11650 0 9928 9928 0 360 250]
ElementArc[283620 0 9942 9942 0 360 250]
ElementArc[283620 122040 9942 9942 0 360 250]
ElementArc[-11650 122040 9928 9928 0 360 250]
ElementArc[-11650 0 3479 3479 0 360 500]
ElementArc[283620 0 3479 3479 0 360 500]
ElementArc[-11650 122040 3479 3479 0 360 500]
ElementArc[283620 122040 3479 3479 0 360 500]
Pin[10000 0 7600 2000 8400 3930 "" "1" ""]
Pad[10000 -3800 10000 3800 7600 2000 8400 "" "1" ""]
Pad[10000 -3800 10000 3800 7600 2000 8400 "" "1" ",onsolder"]
Pin[20000 0 7600 2000 8400 3930 "" "2" ""]
Pad[20000 -3800 20000 3800 7600 2000 8400 "" "2" ""]
Pad[20000 -3800 20000 3800 7600 2000 8400 "" "2" ",onsolder"]
Pin[30000 0 7600 2000 8400 3930 "" "3" ""]
Pad[30000 -3800 30000 3800 7600 2000 8400 "" "3" ""]
Pad[30000 -3800 30000 3800 7600 2000 8400 "" "3" ",onsolder"]
Pin[40000 0 7600 2000 8400 3930 "" "4" ""]
Pad[40000 -3800 40000 3800 7600 2000 8400 "" "4" ""]
Pad[40000 -3800 40000 3800 7600 2000 8400 "" "4" ",onsolder"]
Pin[50000 0 7600 2000 8400 3930 "" "5" ""]
Pad[50000 -3800 50000 3800 7600 2000 8400 "" "5" ""]
Pad[50000 -3800 50000 3800 7600 2000 8400 "" "5" ",onsolder"]
Pin[60000 0 7600 2000 8400 3930 "" "6" ""]
Pad[60000 -3800 60000 3800 7600 2000 8400 "" "6" ""]
Pad[60000 -3800 60000 3800 7600 2000 8400 "" "6" ",onsolder"]
Pin[70000 0 7600 2000 8400 3930 "" "7" ""]
Pad[70000 -3800 70000 3800 7600 2000 8400 "" "7" ""]
Pad[70000 -3800 70000 3800 7600 2000 8400 "" "7" ",onsolder"]
Pin[80000 0 7600 2000 8400 3930 "" "8" ""]
Pad[80000 -3800 80000 3800 7600 2000 8400 "" "8" ""]
Pad[80000 -3800 80000 3800 7600 2000 8400 "" "8" ",onsolder"]
Pin[90000 0 7600 2000 8400 3930 "" "9" ""]
Pad[90000 -3800 90000 3800 7600 2000 8400 "" "9" ""]
Pad[90000 -3800 90000 3800 7600 2000 8400 "" "9" ",onsolder"]
Pin[100000 0 7600 2000 8400 3930 "" "10" ""]
Pad[100000 -3800 100000 3800 7600 2000 8400 "" "10" ""]
Pad[100000 -3800 100000 3800 7600 2000 8400 "" "10" ",onsolder"]
Pin[110000 0 7600 2000 8400 3930 "" "11" ""]
Pad[110000 -3800 110000 3800 7600 2000 8400 "" "11" ""]
Pad[110000 -3800 110000 3800 7600 2000 8400 "" "11" ",onsolder"]
Pin[120000 0 7600 2000 8400 3930 "" "12" ""]
Pad[120000 -3800 120000 3800 7600 2000 8400 "" "12" ""]
Pad[120000 -3800 120000 3800 7600 2000 8400 "" "12" ",onsolder"]
Pin[130000 0 7600 2000 8400 3930 "" "13" ""]
Pad[130000 -3800 130000 3800 7600 2000 8400 "" "13" ""]
Pad[130000 -3800 130000 3800 7600 2000 8400 "" "13" ",onsolder"]
Pin[140000 0 7600 2000 8400 3930 "" "14" ""]
Pad[140000 -3800 140000 3800 7600 2000 8400 "" "14" ""]
Pad[140000 -3800 140000 3800 7600 2000 8400 "" "14" ",onsolder"]
Pin[150000 0 7600 2000 8400 3930 "" "15" ""]
Pad[150000 -3800 150000 3800 7600 2000 8400 "" "15" ""]
Pad[150000 -3800 150000 3800 7600 2000 8400 "" "15" ",onsolder"]
Pin[160000 0 7600 2000 8400 3930 "" "16" ""]
Pad[160000 -3800 160000 3800 7600 2000 8400 "" "16" ""]
Pad[160000 -3800 160000 3800 7600 2000 8400 "" "16" ",onsolder"]
)
