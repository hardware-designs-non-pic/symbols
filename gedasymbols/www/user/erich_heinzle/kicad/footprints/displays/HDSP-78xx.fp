# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Vincent Vocanson
# No warranties express or implied
# Footprint converted from Kicad Module HDSP-78xx
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: HDSP-78xx
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 11
#
Element["" "AFF***" "" "" 0 0 0 -42500 0 100 ""]
(
ElementLine[-15000 -20000 -10000 -25000 1200]
ElementLine[-9500 13000 -7000 2000 1200]
ElementLine[3500 13000 6000 2000 1200]
ElementLine[6500 -1500 9000 -13000 1200]
ElementLine[-6500 -1500 -4000 -13000 1200]
ElementLine[-8500 15000 2000 15000 1200]
ElementLine[-2500 -15000 8000 -15000 1200]
ElementLine[-5500 0 5000 0 1200]
ElementLine[-15000 -23000 -13000 -25000 1200]
ElementLine[-15000 -21500 -11500 -25000 1200]
ElementLine[15000 -25000 -15000 -25000 1200]
ElementLine[-15000 -25000 -15000 25000 1200]
ElementLine[-15000 25000 15000 25000 1200]
ElementLine[15000 25000 15000 -25000 1200]
ElementArc[-9500 -14500 1414 1414 0 360 1200]
ElementArc[9000 14500 1414 1414 0 360 1200]
Pin[-10000 -20000 7480 2000 8280 2760 "" "1" "square"]
Pin[-10000 -10000 7480 2000 8280 2760 "" "2" ""]
Pin[-10000 0 7480 2000 8280 2760 "" "3" ""]
Pin[-10000 10000 7480 2000 8280 2760 "" "4" ""]
Pin[-10000 20000 7480 2000 8280 2760 "" "5" ""]
Pin[10000 20000 7480 2000 8280 2760 "" "6" ""]
Pin[10000 10000 7480 2000 8280 2760 "" "7" ""]
Pin[10000 0 8000 2000 8800 2760 "" "8" ""]
Pin[10000 -10000 7480 2000 8280 2760 "" "9" ""]
Pin[10000 -20000 7480 2000 8280 2760 "" "10" ""]
Pin[-10000 -10000 5500 2000 6300 3200 "" "" ""]
)
