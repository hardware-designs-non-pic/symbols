# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-1206
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-1206
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 7500 -7500 0 100 ""]
(
ElementLine[-390 390 390 390 260]
ElementLine[390 390 390 -390 260]
ElementLine[-390 -390 390 -390 260]
ElementLine[-390 390 -390 -390 260]
ElementLine[1770 2750 3140 2750 260]
ElementLine[3140 2750 3140 1770 260]
ElementLine[1770 1770 3140 1770 260]
ElementLine[1770 2750 1770 1770 260]
ElementLine[3140 2750 3540 2750 260]
ElementLine[3540 2750 3540 -1960 260]
ElementLine[3140 -1960 3540 -1960 260]
ElementLine[3140 2750 3140 -1960 260]
ElementLine[3140 -2160 3540 -2160 260]
ElementLine[3540 -2160 3540 -2750 260]
ElementLine[3140 -2750 3540 -2750 260]
ElementLine[3140 -2160 3140 -2750 260]
ElementLine[-3540 2750 -3140 2750 260]
ElementLine[-3140 2750 -3140 -1960 260]
ElementLine[-3540 -1960 -3140 -1960 260]
ElementLine[-3540 2750 -3540 -1960 260]
ElementLine[-3540 -2160 -3140 -2160 260]
ElementLine[-3140 -2160 -3140 -2750 260]
ElementLine[-3540 -2750 -3140 -2750 260]
ElementLine[-3540 -2160 -3540 -2750 260]
ElementLine[1770 2750 2360 2750 260]
ElementLine[2360 2750 2360 1770 260]
ElementLine[1770 1770 2360 1770 260]
ElementLine[1770 2750 1770 1770 260]
ElementLine[6100 2950 -6100 2950 400]
ElementLine[-6100 2950 -6100 -2950 400]
ElementLine[-6100 -2950 6100 -2950 400]
ElementLine[6100 -2950 6100 2950 400]
ElementArc[0 0 2917 2917 180 -95 400]
ElementArc[0 0 2917 2917 0 -84 400]
ElementArc[0 0 2917 2917 0 -95 400]
ElementArc[0 0 2917 2917 180 -84 400]
Pad[-5590 -400 -5590 400 6290 2000 7090 "" "A" "square"]
Pad[5590 -400 5590 400 6290 2000 7090 "" "C" "square"]
)
