# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-Q62902-B152
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-Q62902-B152
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 21
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -12500 0 100 ""]
(
ElementLine[11700 7300 -8300 7300 600]
ElementLine[-8300 -7300 11700 -7300 600]
ElementLine[-11700 7300 -11700 -7300 600]
ElementLine[-8300 7300 -10000 7300 600]
ElementLine[-8300 -7300 -10000 -7300 600]
ElementLine[-10000 7300 -10000 -7300 600]
ElementLine[-10000 7300 -11700 7300 600]
ElementLine[-10000 -7300 -11700 -7300 600]
ElementLine[-8300 -7300 -8300 7300 600]
ElementLine[11700 -7300 11700 7300 600]
ElementArc[-11700 0 1000 1000 90 -180 600]
ElementArc[0 0 5698 5698 0 -52 600]
ElementArc[0 0 5700 5700 270 -52 600]
ElementArc[0 0 5700 5700 90 -52 600]
ElementArc[0 0 5699 5699 180 -51 600]
ElementArc[0 0 5700 5700 180 -39 600]
ElementArc[0 0 5691 5691 180 -38 600]
ElementArc[0 0 5699 5699 0 -39 600]
ElementArc[0 0 5700 5700 0 -38 600]
ElementArc[0 0 6700 6700 270 -59 600]
ElementArc[0 0 6695 6695 0 -59 600]
ElementArc[0 0 6694 6694 180 -59 600]
ElementArc[0 0 6700 6700 90 -59 600]
ElementArc[0 0 6700 6700 0 -32 600]
ElementArc[0 0 6689 6689 0 -31 600]
ElementArc[0 0 6692 6692 180 -31 600]
ElementArc[0 0 6700 6700 180 -32 600]
ElementArc[0 0 2500 2500 0 -90 600]
ElementArc[0 0 4000 4000 0 -90 600]
ElementArc[0 0 2500 2500 180 -90 600]
ElementArc[0 0 4000 4000 180 -90 600]
Pin[5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "A" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[-5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "K" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
