# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-PLCC4-TOPLED-RG
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-PLCC4-TOPLED-RG
# Text descriptor count: 1
# Draw segment object count: 68
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 -22500 -2500 1 100 ""]
(
ElementLine[-1960 -390 -390 -390 260]
ElementLine[-390 -390 -390 -1960 260]
ElementLine[-1960 -1960 -390 -1960 260]
ElementLine[-1960 -390 -1960 -1960 260]
ElementLine[390 1960 1960 1960 260]
ElementLine[1960 1960 1960 390 260]
ElementLine[390 390 1960 390 260]
ElementLine[390 1960 390 390 260]
ElementLine[-1960 1960 -390 1960 260]
ElementLine[-390 1960 -390 390 260]
ElementLine[-1960 390 -390 390 260]
ElementLine[-1960 1960 -1960 390 260]
ElementLine[-4920 -6100 -980 -6100 260]
ElementLine[-980 -6100 -980 -11610 260]
ElementLine[-4920 -11610 -980 -11610 260]
ElementLine[-4920 -6100 -4920 -11610 260]
ElementLine[780 -6100 4720 -6100 260]
ElementLine[4720 -6100 4720 -11610 260]
ElementLine[780 -11610 4720 -11610 260]
ElementLine[780 -6100 780 -11610 260]
ElementLine[980 11610 4920 11610 260]
ElementLine[4920 11610 4920 6100 260]
ElementLine[980 6100 4920 6100 260]
ElementLine[980 11610 980 6100 260]
ElementLine[-4920 11610 -980 11610 260]
ElementLine[-980 11610 -980 6100 260]
ElementLine[-4920 6100 -980 6100 260]
ElementLine[-4920 11610 -4920 6100 260]
ElementLine[-5310 -5700 -590 -5700 260]
ElementLine[-590 -5700 -590 -12000 260]
ElementLine[-5310 -12000 -590 -12000 260]
ElementLine[-5310 -5700 -5310 -12000 260]
ElementLine[390 -5700 5110 -5700 260]
ElementLine[5110 -5700 5110 -12000 260]
ElementLine[390 -12000 5110 -12000 260]
ElementLine[390 -5700 390 -12000 260]
ElementLine[590 12000 5310 12000 260]
ElementLine[5310 12000 5310 5700 260]
ElementLine[590 5700 5310 5700 260]
ElementLine[590 12000 590 5700 260]
ElementLine[-5310 12000 -590 12000 260]
ElementLine[-590 12000 -590 5700 260]
ElementLine[-5310 5700 -590 5700 260]
ElementLine[-5310 12000 -5310 5700 260]
ElementLine[-5510 6290 -3930 6290 800]
ElementLine[-3930 6290 3340 6290 800]
ElementLine[3340 6290 3930 6290 800]
ElementLine[3930 6290 5510 6290 800]
ElementLine[5510 6290 5510 4130 800]
ElementLine[5510 4130 5510 -6290 800]
ElementLine[5510 -6290 -3930 -6290 800]
ElementLine[-3930 -6290 -5510 -6290 800]
ElementLine[-3930 -6290 -3930 -10430 400]
ElementLine[-3930 -10430 -1960 -10430 400]
ElementLine[-1960 -10430 -1960 -6490 400]
ElementLine[1960 -6490 1960 -10430 400]
ElementLine[1960 -10430 4330 -10430 400]
ElementLine[4330 -10430 4330 -6290 400]
ElementLine[-3930 6290 -3930 10430 400]
ElementLine[-3930 10430 -1960 10430 400]
ElementLine[-1960 10430 -1960 6490 400]
ElementLine[1960 6490 1960 10430 400]
ElementLine[1960 10430 3930 10430 400]
ElementLine[3930 10430 3930 6290 400]
ElementLine[-5510 -6290 -5510 6290 800]
ElementLine[5510 4130 3340 6290 800]
ElementLine[-5510 0 5510 0 500]
ElementLine[0 5510 0 -5510 500]
ElementArc[0 0 3889 3889 0 360 500]
Pad[8850 13970 8850 13970 15740 2000 16540 "" "1" "square"]
Pad[8660 -13970 8660 -13970 15740 2000 16540 "" "2" "square"]
Pad[-8850 -13970 -8850 -13970 15740 2000 16540 "" "3" "square"]
Pad[-8850 13970 -8850 13970 15740 2000 16540 "" "4" "square"]
)
