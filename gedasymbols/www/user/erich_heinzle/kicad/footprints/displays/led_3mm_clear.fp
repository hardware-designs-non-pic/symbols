# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module led_3mm_clear
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: led_3mm_clear
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 1
# Draw arc object count: 4
# Pad count: 2
#
Element["" "led_3mm_clear" "" "" 0 0 0 13779 0 100 ""]
(
ElementLine[-5910 5120 5910 5120 1180]
ElementLine[-5120 5910 5120 5910 1180]
ElementArc[0 0 5910 5910 0 360 1180]
ElementArc[0 0 7898 7898 45 -270 1180]
Pin[0 -5000 7870 2000 8670 3150 "" "1" ""]
Pin[0 5000 7870 2000 8670 3150 "" "2" ""]
)
