# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Vincent Vocanson
# No warranties express or implied
# Footprint converted from Kicad Module HDSP-48xx
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: HDSP-48xx
# Text descriptor count: 1
# Draw segment object count: 64
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 20
#
Element["" "BAR***" "" "" 0 0 0 -30000 0 100 ""]
(
ElementLine[42000 -10000 48000 -10000 1200]
ElementLine[48000 -10000 48000 8000 1200]
ElementLine[48000 8000 42000 8000 1200]
ElementLine[42000 8000 42000 -10000 1200]
ElementLine[32000 -10000 38000 -10000 1200]
ElementLine[38000 -10000 38000 8000 1200]
ElementLine[38000 8000 32000 8000 1200]
ElementLine[32000 8000 32000 -10000 1200]
ElementLine[22000 -10000 28000 -10000 1200]
ElementLine[28000 -10000 28000 8000 1200]
ElementLine[28000 8000 22000 8000 1200]
ElementLine[22000 8000 22000 -10000 1200]
ElementLine[12000 -10000 18000 -10000 1200]
ElementLine[18000 -10000 18000 8000 1200]
ElementLine[18000 8000 12000 8000 1200]
ElementLine[12000 8000 12000 -10000 1200]
ElementLine[2000 -10000 8000 -10000 1200]
ElementLine[8000 -10000 8000 8000 1200]
ElementLine[8000 8000 2000 8000 1200]
ElementLine[2000 8000 2000 -10000 1200]
ElementLine[-8000 -10000 -2000 -10000 1200]
ElementLine[-2000 -10000 -2000 8000 1200]
ElementLine[-2000 8000 -8000 8000 1200]
ElementLine[-8000 8000 -8000 -10000 1200]
ElementLine[-18000 8000 -12000 8000 1200]
ElementLine[-12000 8000 -12000 -10000 1200]
ElementLine[-12000 -10000 -18000 -10000 1200]
ElementLine[-18000 -10000 -18000 8000 1200]
ElementLine[-28000 -10000 -22000 -10000 1200]
ElementLine[-22000 -10000 -22000 8000 1200]
ElementLine[-22000 8000 -28000 8000 1200]
ElementLine[-28000 8000 -28000 -10000 1200]
ElementLine[-38000 8000 -38000 -10000 1200]
ElementLine[-38000 -10000 -32000 -10000 1200]
ElementLine[-32000 -10000 -32000 8000 1200]
ElementLine[-32000 8000 -38000 8000 1200]
ElementLine[-48000 8000 -48000 -10000 1200]
ElementLine[-48000 -10000 -42000 -10000 1200]
ElementLine[-42000 -10000 -42000 8000 1200]
ElementLine[-42000 8000 -48000 8000 1200]
ElementLine[44000 8000 46000 8000 1200]
ElementLine[46000 -10000 44000 -10000 1200]
ElementLine[34000 8000 36000 8000 1200]
ElementLine[36000 -10000 34000 -10000 1200]
ElementLine[24000 8000 26000 8000 1200]
ElementLine[26000 -10000 24000 -10000 1200]
ElementLine[14000 8000 16000 8000 1200]
ElementLine[16000 -10000 14000 -10000 1200]
ElementLine[4000 8000 6000 8000 1200]
ElementLine[6000 -10000 4000 -10000 1200]
ElementLine[-6000 8000 -4000 8000 1200]
ElementLine[-4000 -10000 -6000 -10000 1200]
ElementLine[-14000 -10000 -16000 -10000 1200]
ElementLine[-16000 8000 -14000 8000 1200]
ElementLine[-26000 8000 -24000 8000 1200]
ElementLine[-24000 -10000 -26000 -10000 1200]
ElementLine[-36000 8000 -34000 8000 1200]
ElementLine[-34000 -10000 -36000 -10000 1200]
ElementLine[-46000 -10000 -44000 -10000 1200]
ElementLine[-44000 8000 -46000 8000 1200]
ElementLine[50000 -20000 50000 20000 1200]
ElementLine[50000 20000 -50000 20000 1200]
ElementLine[-50000 20000 -50000 -20000 1200]
ElementLine[-50000 -20000 50000 -20000 1200]
Pin[-45000 15000 8000 2000 8800 3600 "" "1" "square"]
Pin[-35000 15000 8000 2000 8800 3600 "" "2" ""]
Pin[-25000 15000 8000 2000 8800 3600 "" "3" ""]
Pin[-15000 15000 8000 2000 8800 3600 "" "4" ""]
Pin[25000 -15000 8000 2000 8800 3600 "" "13" ""]
Pin[15000 -15000 8000 2000 8800 3600 "" "14" ""]
Pin[5000 -15000 8000 2000 8800 3600 "" "15" ""]
Pin[-5000 -15000 8000 2000 8800 3600 "" "16" ""]
Pin[-5000 15000 8000 2000 8800 3600 "" "5" ""]
Pin[5000 15000 8000 2000 8800 3600 "" "6" ""]
Pin[15000 15000 8000 2000 8800 3600 "" "7" ""]
Pin[25000 15000 8000 2000 8800 3600 "" "8" ""]
Pin[35000 -15000 8000 2000 8800 3600 "" "12" ""]
Pin[45000 -15000 8000 2000 8800 3600 "" "11" ""]
Pin[45000 15000 8000 2000 8800 3600 "" "10" ""]
Pin[35000 15000 8000 2000 8800 3600 "" "9" ""]
Pin[-15000 -15000 8000 2000 8800 3600 "" "17" ""]
Pin[-25000 -15000 8000 2000 8800 3600 "" "18" ""]
Pin[-35000 -15000 8000 2000 8800 3600 "" "19" ""]
Pin[-45000 -15000 8000 2000 8800 3600 "" "20" ""]
)
