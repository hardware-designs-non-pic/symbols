# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-U57X32
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-U57X32
# Text descriptor count: 1
# Draw segment object count: 23
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 27500 -7500 0 100 ""]
(
ElementLine[-12500 -7500 12500 -7500 600]
ElementLine[12500 7500 12500 -7500 600]
ElementLine[12500 7500 -12500 7500 600]
ElementLine[-12500 -7500 -12500 7500 600]
ElementLine[-10500 -5500 10500 -5500 600]
ElementLine[10500 5500 10500 -5500 600]
ElementLine[10500 5500 -10500 5500 600]
ElementLine[-10500 -5500 -10500 5500 600]
ElementLine[-10000 -4000 10000 -4000 600]
ElementLine[9000 -5000 9000 5000 600]
ElementLine[-10000 -2000 10000 -2000 600]
ElementLine[-10000 0 10000 0 600]
ElementLine[-10000 2000 10000 2000 600]
ElementLine[-10000 4000 10000 4000 600]
ElementLine[-9000 -5000 -9000 5000 600]
ElementLine[-7000 -5000 -7000 5000 600]
ElementLine[-5000 -5000 -5000 5000 600]
ElementLine[-3000 -5000 -3000 5000 600]
ElementLine[-1000 -5000 -1000 5000 600]
ElementLine[1000 -5000 1000 5000 600]
ElementLine[3000 -5000 3000 5000 600]
ElementLine[5000 -5000 5000 5000 600]
ElementLine[7000 -5000 7000 5000 600]
Pin[-5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
