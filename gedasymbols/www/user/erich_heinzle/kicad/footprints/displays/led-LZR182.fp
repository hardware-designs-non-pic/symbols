# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LZR182
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LZR182
# Text descriptor count: 1
# Draw segment object count: 28
# Draw circle object count: 0
# Draw arc object count: 24
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 5000 -17000 0 100 ""]
(
ElementLine[5000 -1500 6000 -1500 260]
ElementLine[6000 -1500 6000 -6000 260]
ElementLine[5000 -6000 6000 -6000 260]
ElementLine[5000 -1500 5000 -6000 260]
ElementLine[-6000 -4000 -5000 -4000 260]
ElementLine[-5000 -4000 -5000 -6000 260]
ElementLine[-6000 -6000 -5000 -6000 260]
ElementLine[-6000 -4000 -6000 -6000 260]
ElementLine[5000 8500 6000 8500 260]
ElementLine[6000 8500 6000 4000 260]
ElementLine[5000 4000 6000 4000 260]
ElementLine[5000 8500 5000 4000 260]
ElementLine[-6000 6000 -5000 6000 260]
ElementLine[-5000 6000 -5000 4000 260]
ElementLine[-6000 4000 -5000 4000 260]
ElementLine[-6000 6000 -6000 4000 260]
ElementLine[5000 -10000 5000 -8500 600]
ElementLine[5000 -8500 5000 -1500 600]
ElementLine[-5000 -10000 -5000 -8500 600]
ElementLine[-5000 -1500 -5000 -8500 600]
ElementLine[5000 10000 5000 8500 600]
ElementLine[5000 -1500 5000 1500 600]
ElementLine[5000 1500 5000 8500 600]
ElementLine[-5000 -1500 -5000 1500 600]
ElementLine[-5000 10000 -5000 8500 600]
ElementLine[-5000 8500 -5000 1500 600]
ElementLine[-5000 -10000 5000 -10000 600]
ElementLine[-5000 10000 5000 10000 600]
ElementArc[0 -5000 3500 3500 0 -90 600]
ElementArc[0 -5000 2000 2000 0 -90 600]
ElementArc[0 -5000 2000 2000 180 -90 600]
ElementArc[0 -5000 3500 3500 180 -90 600]
ElementArc[0 -5000 4489 4489 0 -49 600]
ElementArc[0 -5000 4500 4500 270 -49 600]
ElementArc[0 -5000 4500 4500 90 -49 600]
ElementArc[0 -5000 4496 4496 180 -49 600]
ElementArc[0 -4990 4496 4496 180 -41 600]
ElementArc[0 -4990 4490 4490 180 -41 600]
ElementArc[0 -4990 4500 4500 0 -41 600]
ElementArc[0 -5000 4496 4496 0 -41 600]
ElementArc[0 5000 3500 3500 0 -90 600]
ElementArc[0 5000 2000 2000 0 -90 600]
ElementArc[0 5000 2000 2000 180 -90 600]
ElementArc[0 5000 3500 3500 180 -90 600]
ElementArc[0 5000 4496 4496 0 -49 600]
ElementArc[0 5000 4500 4500 270 -49 600]
ElementArc[0 5000 4500 4500 90 -49 600]
ElementArc[0 5000 4489 4489 180 -49 600]
ElementArc[0 4990 4489 4489 180 -41 600]
ElementArc[0 4990 4500 4500 180 -41 600]
ElementArc[0 5000 4500 4500 0 -41 600]
ElementArc[0 4990 4496 4496 0 -41 600]
Pin[-5000 -5000 5200 2000 6000 3200 "" "A1" ""]
Pad[-5000 -6600 -5000 -3400 5200 2000 6000 "" "A1" ""]
Pad[-5000 -6600 -5000 -3400 5200 2000 6000 "" "A1" ",onsolder"]
Pin[-5000 5000 5200 2000 6000 3200 "" "A2" ""]
Pad[-5000 3400 -5000 6600 5200 2000 6000 "" "A2" ""]
Pad[-5000 3400 -5000 6600 5200 2000 6000 "" "A2" ",onsolder"]
Pin[5000 -5000 5200 2000 6000 3200 "" "K1" ""]
Pad[5000 -6600 5000 -3400 5200 2000 6000 "" "K1" ""]
Pad[5000 -6600 5000 -3400 5200 2000 6000 "" "K1" ",onsolder"]
Pin[5000 5000 5200 2000 6000 3200 "" "K2" ""]
Pad[5000 3400 5000 6600 5200 2000 6000 "" "K2" ""]
Pad[5000 3400 5000 6600 5200 2000 6000 "" "K2" ",onsolder"]
)
