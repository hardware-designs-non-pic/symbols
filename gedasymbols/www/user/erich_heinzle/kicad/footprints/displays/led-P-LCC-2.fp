# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-P-LCC-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-P-LCC-2
# Text descriptor count: 1
# Draw segment object count: 35
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -12500 -2500 1 100 ""]
(
ElementLine[-5110 8850 5110 8850 260]
ElementLine[5110 8850 5110 2950 260]
ElementLine[-5110 2950 5110 2950 260]
ElementLine[-5110 8850 -5110 2950 260]
ElementLine[-5110 -2950 5110 -2950 260]
ElementLine[5110 -2950 5110 -8850 260]
ElementLine[-5110 -8850 5110 -8850 260]
ElementLine[-5110 -2950 -5110 -8850 260]
ElementLine[-980 980 980 980 260]
ElementLine[980 980 980 -980 260]
ElementLine[-980 -980 980 -980 260]
ElementLine[-980 980 -980 -980 260]
ElementLine[-5510 -2550 5510 -2550 260]
ElementLine[5510 -2550 5510 -9250 260]
ElementLine[-5510 -9250 5510 -9250 260]
ElementLine[-5510 -2550 -5510 -9250 260]
ElementLine[-5510 9250 5510 9250 260]
ElementLine[5510 9250 5510 2550 260]
ElementLine[-5510 2550 5510 2550 260]
ElementLine[-5510 9250 -5510 2550 260]
ElementLine[-5510 4130 -5510 6290 800]
ElementLine[-5510 6290 -4330 6290 800]
ElementLine[-4330 6290 -3340 6290 800]
ElementLine[-3340 6290 4330 6290 800]
ElementLine[4330 6290 5510 6290 800]
ElementLine[5510 6290 5510 -6290 800]
ElementLine[5510 -6290 -5510 -6290 800]
ElementLine[-4330 -6290 -4330 -7080 400]
ElementLine[-4330 -7080 4330 -7080 400]
ElementLine[4330 -7080 4330 -6290 400]
ElementLine[-4330 6290 -4330 7080 400]
ElementLine[-4330 7080 4330 7080 400]
ElementLine[4330 7080 4330 6290 400]
ElementLine[-3340 6290 -5510 4130 800]
ElementLine[-5510 -6290 -5510 4130 800]
ElementArc[0 0 3055 3055 0 360 400]
Pad[0 -10820 0 -10820 15740 2000 16540 "" "A" "square"]
Pad[0 10820 0 10820 15740 2000 16540 "" "C" "square"]
)
