# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module kingbright-SA18-11
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: kingbright-SA18-11
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" ">NAME" "" "" 0 0 -47500 -87500 0 100 ""]
(
ElementLine[-75000 -110000 75000 -110000 500]
ElementLine[75000 -110000 75000 110000 500]
ElementLine[75000 110000 -75000 110000 500]
ElementLine[-75000 110000 -75000 -110000 500]
ElementLine[-50000 0 50000 0 5600]
ElementLine[60000 -95000 -40000 -95000 5600]
ElementLine[-60000 100000 40000 100000 5600]
ElementLine[40000 90000 50000 10000 5600]
ElementLine[50000 -10000 60000 -85000 5600]
ElementLine[-40000 -85000 -50000 -10000 5600]
ElementLine[-50000 10000 -60000 90000 5600]
ElementArc[55000 100000 354 354 0 360 2800]
Pin[-20000 83500 0 2000 6000 3200 "" "1" "blah"]
Pad[-22600 83500 -17400 83500 0 2000 800 "" "1" "square"]
Pad[-22600 83500 -17400 83500 0 2000 800 "" "1" "square,onsolder"]
Pin[-10000 83500 5200 2000 6000 3200 "" "2" ""]
Pin[0 83500 5200 2000 6000 3200 "" "3" ""]
Pin[10000 83500 5200 2000 6000 3200 "" "4" ""]
Pin[20000 83500 5200 2000 6000 3200 "" "5" ""]
Pin[20000 -83500 5200 2000 6000 3200 "" "6" ""]
Pin[10000 -83500 5200 2000 6000 3200 "" "7" ""]
Pin[0 -83500 5200 2000 6000 3200 "" "8" ""]
Pin[-10000 -83500 5200 2000 6000 3200 "" "9" ""]
Pin[-20000 -83500 5200 2000 6000 3200 "" "10" ""]
)
