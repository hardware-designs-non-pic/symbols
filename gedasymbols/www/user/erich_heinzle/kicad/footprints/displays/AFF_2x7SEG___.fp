# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Vincent Vocanson
# No warranties express or implied
# Footprint converted from Kicad Module AFF_2x7SEG___
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: AFF_2x7SEG___
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 8
#
Element["" "AFF***" "" "" 0 0 0 -42500 0 100 ""]
(
ElementLine[15000 -18000 11000 -3000 2000]
ElementLine[35000 -18000 31000 -3000 2000]
ElementLine[6000 17000 10000 2000 2000]
ElementLine[30000 3000 26000 18000 2000]
ElementLine[13000 0 29000 0 2000]
ElementLine[17000 -21000 33000 -21000 2000]
ElementLine[7000 20000 23000 20000 2000]
ElementLine[-28000 -3000 -24000 -18000 2000]
ElementLine[-8000 -3000 -4000 -18000 2000]
ElementLine[-33000 18000 -29000 3000 2000]
ElementLine[-13000 18000 -9000 3000 2000]
ElementLine[-21000 -21000 -5000 -21000 2000]
ElementLine[-27000 0 -11000 0 2000]
ElementLine[-31000 20000 -15000 20000 2000]
ElementLine[-40000 35000 40000 35000 1200]
ElementLine[40000 35000 40000 -35000 1200]
ElementLine[40000 -35000 -40000 -35000 1200]
ElementLine[-40000 -35000 -40000 35000 1200]
ElementArc[34000 20000 2000 2000 0 360 2000]
ElementArc[-5000 20000 2000 2000 0 360 2000]
Pin[-15000 30000 8000 2000 8800 3600 "" "1" "square"]
Pin[-5000 30000 8000 2000 8800 3600 "" "2" ""]
Pin[5000 30000 8000 2000 8800 3600 "" "3" ""]
Pin[15000 30000 8000 2000 8800 3600 "" "4" ""]
Pin[15000 -30000 8000 2000 8800 3600 "" "5" ""]
Pin[5000 -30000 8000 2000 8800 3600 "" "6" ""]
Pin[-5000 -30000 8000 2000 8800 3600 "" "7" ""]
Pin[-15000 -30000 8000 2000 8800 3600 "" "8" ""]
)
