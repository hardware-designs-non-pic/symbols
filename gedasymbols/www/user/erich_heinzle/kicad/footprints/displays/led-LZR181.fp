# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LZR181
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LZR181
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 12
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 5000 -12000 0 100 ""]
(
ElementLine[5000 3500 6000 3500 260]
ElementLine[6000 3500 6000 -1000 260]
ElementLine[5000 -1000 6000 -1000 260]
ElementLine[5000 3500 5000 -1000 260]
ElementLine[-6000 1000 -5000 1000 260]
ElementLine[-5000 1000 -5000 -1000 260]
ElementLine[-6000 -1000 -5000 -1000 260]
ElementLine[-6000 1000 -6000 -1000 260]
ElementLine[-5000 5000 5000 5000 600]
ElementLine[5000 5000 5000 3500 600]
ElementLine[5000 -5000 5000 -3500 600]
ElementLine[5000 -3500 5000 3500 600]
ElementLine[-5000 -5000 -5000 -3500 600]
ElementLine[-5000 5000 -5000 3500 600]
ElementLine[-5000 3500 -5000 -3500 600]
ElementLine[-5000 -5000 5000 -5000 600]
ElementArc[0 0 3500 3500 0 -90 600]
ElementArc[0 0 2000 2000 0 -90 600]
ElementArc[0 0 2000 2000 180 -90 600]
ElementArc[0 0 3500 3500 180 -90 600]
ElementArc[0 0 4489 4489 0 -49 600]
ElementArc[0 0 4500 4500 270 -49 600]
ElementArc[0 0 4500 4500 90 -49 600]
ElementArc[0 0 4489 4489 180 -49 600]
ElementArc[0 0 4489 4489 180 -41 600]
ElementArc[0 0 4500 4500 180 -41 600]
ElementArc[0 0 4500 4500 0 -41 600]
ElementArc[0 0 4489 4489 0 -41 600]
Pin[-5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
