# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-SMARTLED-TTW
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-SMARTLED-TTW
# Text descriptor count: 1
# Draw segment object count: 33
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 -7500 -7500 1 100 ""]
(
ElementLine[-680 -1270 680 -1270 260]
ElementLine[680 -1270 680 -2750 260]
ElementLine[-680 -2750 680 -2750 260]
ElementLine[-680 -1270 -680 -2750 260]
ElementLine[-590 1370 590 1370 260]
ElementLine[590 1370 590 190 260]
ElementLine[-590 190 590 190 260]
ElementLine[-590 1370 -590 190 260]
ElementLine[-590 -2360 590 -2360 260]
ElementLine[590 -2360 590 -3340 260]
ElementLine[-590 -3340 590 -3340 260]
ElementLine[-590 -2360 -590 -3340 260]
ElementLine[-590 3340 590 3340 260]
ElementLine[590 3340 590 2360 260]
ElementLine[-590 2360 590 2360 260]
ElementLine[-590 3340 -590 2360 260]
ElementLine[-880 -1180 880 -1180 260]
ElementLine[880 -1180 880 -3830 260]
ElementLine[-880 -3830 880 -3830 260]
ElementLine[-880 -1180 -880 -3830 260]
ElementLine[-680 2750 680 2750 260]
ElementLine[680 2750 680 1270 260]
ElementLine[-680 1270 680 1270 260]
ElementLine[-680 2750 -680 1270 260]
ElementLine[-880 3830 880 3830 260]
ElementLine[880 3830 880 1180 260]
ElementLine[-880 1180 880 1180 260]
ElementLine[-880 3830 -880 1180 260]
ElementLine[-1370 -2360 1370 -2360 400]
ElementLine[1370 -2360 1370 2360 400]
ElementLine[1370 2360 -1370 2360 400]
ElementLine[-1370 2360 -1370 -2360 400]
ElementLine[1370 1570 590 2360 400]
Pad[-590 3440 590 3440 1960 2000 2760 "" "A" "square"]
Pad[0 1960 0 1960 1370 2000 2170 "" "A@1" "square"]
Pad[-590 -3440 590 -3440 1960 2000 2760 "" "C" "square"]
Pad[0 -1960 0 -1960 1370 2000 2170 "" "C@1" "square"]
)
