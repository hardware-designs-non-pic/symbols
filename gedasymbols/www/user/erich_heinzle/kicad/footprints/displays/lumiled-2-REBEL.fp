# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lumiled-2-REBEL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lumiled-2-REBEL
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 2500 -12500 0 100 ""]
(
ElementLine[-8000 6600 12300 6600 260]
ElementLine[12300 -6600 12300 6600 260]
ElementLine[-8000 -6600 12300 -6600 260]
ElementLine[-8000 6600 -8000 -6600 260]
ElementArc[0 0 4031 4031 0 360 250]
Pad[9450 3160 9450 3360 4720 2000 5520 "" "P_1" "square"]
Pad[9450 -3360 9450 -3160 4720 2000 5520 "" "P_2" "square"]
Pad[-3550 -1965 -3550 1965 7090 2000 7890 "" "P_3" "square"]
)
