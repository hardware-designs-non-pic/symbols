# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-SFH482
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-SFH482
# Text descriptor count: 1
# Draw segment object count: 3
# Draw circle object count: 2
# Draw arc object count: 12
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -17500 0 100 ""]
(
ElementLine[-8500 -6000 -11000 -8500 600]
ElementLine[-11000 -8500 -8500 -11000 600]
ElementLine[-6000 -8500 -8500 -11000 600]
ElementArc[0 0 7425 7425 0 360 300]
ElementArc[0 0 6718 6718 0 360 500]
ElementArc[0 0 7000 7000 270 -60 600]
ElementArc[0 0 6991 6991 0 -60 600]
ElementArc[0 0 7000 7000 90 -60 600]
ElementArc[0 0 6991 6991 180 -60 600]
ElementArc[0 0 6994 6994 180 -32 600]
ElementArc[0 0 7000 7000 180 -32 600]
ElementArc[0 0 6994 6994 0 -32 600]
ElementArc[0 0 7000 7000 0 -32 600]
ElementArc[0 0 2500 2500 0 -90 600]
ElementArc[0 0 4000 4000 0 -90 600]
ElementArc[0 0 2500 2500 180 -90 600]
ElementArc[0 0 4000 4000 180 -90 600]
Pin[-5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
