# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Vincent Vocanson
# No warranties express or implied
# Footprint converted from Kicad Module ELD-426x
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ELD-426x
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 10
#
Element["" "AFF***" "" "" 0 0 0 -38000 0 100 ""]
(
ElementLine[-40000 32000 -40000 -32000 1200]
ElementLine[40000 -32000 40000 32000 1200]
ElementLine[-40000 -32000 40000 -32000 1200]
ElementLine[-40000 32000 40000 32000 1200]
ElementLine[-25000 -22500 -7500 -22500 1500]
ElementLine[-10000 -2500 -5000 -22500 1500]
ElementLine[-32500 -2500 -27500 -22500 1500]
ElementLine[-35000 22500 -17500 22500 1500]
ElementLine[-32500 2500 -37500 22500 1500]
ElementLine[-10000 2500 -15000 22500 1500]
ElementLine[15000 -22500 32500 -22500 1500]
ElementLine[5000 22500 22500 22500 1500]
ElementLine[30000 2500 25000 22500 1500]
ElementLine[2500 22500 7500 2500 1500]
ElementLine[30000 -2500 35000 -22500 1500]
ElementLine[7500 -2500 12500 -22500 1500]
ElementLine[10000 0 27500 0 1500]
ElementLine[-29000 0 -13000 0 1500]
ElementLine[-29500 0 -12500 0 1500]
ElementArc[35000 20000 2500 2500 0 360 1500]
ElementArc[-5000 20000 2500 2500 0 360 1500]
Pin[-20000 25000 8000 2000 8800 3600 "" "1" "square"]
Pin[-10000 25000 8000 2000 8800 3600 "" "2" ""]
Pin[0 25000 8000 2000 8800 3600 "" "3" ""]
Pin[10000 25000 8000 2000 8800 3600 "" "4" ""]
Pin[-10000 -25000 8000 2000 8800 3600 "" "9" ""]
Pin[-20000 -25000 8000 2000 8800 3600 "" "10" ""]
Pin[20000 25000 8000 2000 8800 3600 "" "5" ""]
Pin[10000 -25000 8000 2000 8800 3600 "" "7" ""]
Pin[20000 -25000 8000 2000 8800 3600 "" "6" ""]
Pin[0 -25000 8000 2000 8800 3600 "" "8" ""]
)
