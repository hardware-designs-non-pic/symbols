# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module display-lcd-lxd-353-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: display-lcd-lxd-353-E
# Text descriptor count: 1
# Draw segment object count: 73
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 15
#
Element["" ">NAME" "" "" 0 0 1250 -39750 0 100 ""]
(
ElementLine[-48500 25000 -43500 25000 260]
ElementLine[-43500 25000 -43500 20000 260]
ElementLine[-48500 20000 -43500 20000 260]
ElementLine[-48500 25000 -48500 20000 260]
ElementLine[18000 25000 23000 25000 260]
ElementLine[23000 25000 23000 20000 260]
ElementLine[18000 20000 23000 20000 260]
ElementLine[18000 25000 18000 20000 260]
ElementLine[50740 25000 55750 25000 260]
ElementLine[55750 25000 55750 20000 260]
ElementLine[50740 20000 55750 20000 260]
ElementLine[50740 25000 50740 20000 260]
ElementLine[-15250 25000 -10250 25000 260]
ElementLine[-10250 25000 -10250 20000 260]
ElementLine[-15250 20000 -10250 20000 260]
ElementLine[-15250 25000 -15250 20000 260]
ElementLine[-77500 10000 -62500 10000 260]
ElementLine[-62500 10000 -62500 5000 260]
ElementLine[-77500 5000 -62500 5000 260]
ElementLine[-77500 10000 -77500 5000 260]
ElementLine[-100000 60000 100000 60000 500]
ElementLine[100000 -60000 -100000 -60000 500]
ElementLine[100000 60000 100000 45000 500]
ElementLine[-100000 45000 -100000 60000 500]
ElementLine[100000 45000 -100000 45000 500]
ElementLine[-100000 -60000 -100000 -45000 500]
ElementLine[100000 45000 100000 -45000 500]
ElementLine[-100000 -45000 100000 -45000 500]
ElementLine[-100000 -45000 -100000 -20000 500]
ElementLine[-100000 -20000 -100000 20000 500]
ElementLine[-100000 20000 -100000 45000 500]
ElementLine[100000 -45000 100000 -60000 500]
ElementLine[-87500 -35000 -90000 -32500 500]
ElementLine[-90000 -32500 -90000 32500 500]
ElementLine[-90000 32500 -87500 35000 500]
ElementLine[-87500 35000 87500 35000 500]
ElementLine[87500 35000 90000 32500 500]
ElementLine[90000 32500 90000 -32500 500]
ElementLine[90000 -32500 87500 -35000 500]
ElementLine[87500 -35000 -87500 -35000 500]
ElementLine[-54500 22500 -53500 10500 5000]
ElementLine[-53500 4500 -52500 -7500 5000]
ElementLine[63240 -7500 62250 4500 5000]
ElementLine[62250 10500 61250 22500 5000]
ElementLine[66750 25000 72000 25000 5000]
ElementLine[77500 22500 78500 10500 5000]
ElementLine[78500 4500 79500 -7500 5000]
ElementLine[68750 -10000 74000 -10000 5000]
ElementLine[67750 7500 73000 7500 5000]
ElementLine[-2250 -7500 -3250 4500 5000]
ElementLine[-3250 10500 -4250 22500 5000]
ElementLine[1250 25000 6500 25000 5000]
ElementLine[12000 22500 13000 10500 5000]
ElementLine[13000 4500 14000 -7500 5000]
ElementLine[3250 -10000 8500 -10000 5000]
ElementLine[2250 7500 7500 7500 5000]
ElementLine[31000 -7500 30000 4500 5000]
ElementLine[30000 10500 29000 22500 5000]
ElementLine[34490 25000 39750 25000 5000]
ElementLine[45250 22500 46250 10500 5000]
ElementLine[46250 4500 47250 -7500 5000]
ElementLine[36500 -10000 41750 -10000 5000]
ElementLine[35500 7500 40740 7500 5000]
ElementLine[-35500 -7500 -36500 4500 5000]
ElementLine[-36500 10500 -37500 22500 5000]
ElementLine[-32000 25000 -26750 25000 5000]
ElementLine[-21250 22500 -20250 10500 5000]
ElementLine[-20250 4500 -19250 -7500 5000]
ElementLine[-30000 -10000 -24750 -10000 5000]
ElementLine[-31000 7500 -25750 7500 5000]
ElementLine[-100000 -20000 -104000 -20000 500]
ElementLine[-104000 -20000 -104000 20000 500]
ElementLine[-104000 20000 -100000 20000 500]
Pad[-70000 47500 -70000 57500 5000 2000 5800 "" "1" "square"]
Pad[-60000 47500 -60000 57500 5000 2000 5800 "" "2" "square"]
Pad[-50000 47500 -50000 57500 5000 2000 5800 "" "3" "square"]
Pad[-40000 47500 -40000 57500 5000 2000 5800 "" "4" "square"]
Pad[-30000 47500 -30000 57500 5000 2000 5800 "" "5" "square"]
Pad[-20000 47500 -20000 57500 5000 2000 5800 "" "6" "square"]
Pad[-10000 47500 -10000 57500 5000 2000 5800 "" "7" "square"]
Pad[0 47500 0 57500 5000 2000 5800 "" "8" "square"]
Pad[10000 47500 10000 57500 5000 2000 5800 "" "9" "square"]
Pad[20000 47500 20000 57500 5000 2000 5800 "" "10" "square"]
Pad[30000 47500 30000 57500 5000 2000 5800 "" "11" "square"]
Pad[40000 47500 40000 57500 5000 2000 5800 "" "12" "square"]
Pad[50000 47500 50000 57500 5000 2000 5800 "" "13" "square"]
Pad[60000 47500 60000 57500 5000 2000 5800 "" "14" "square"]
Pad[70000 47500 70000 57500 5000 2000 5800 "" "15" "square"]
)
