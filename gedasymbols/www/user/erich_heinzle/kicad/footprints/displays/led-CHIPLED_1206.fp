# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-CHIPLED_1206
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-CHIPLED_1206
# Text descriptor count: 1
# Draw segment object count: 46
# Draw circle object count: 1
# Draw arc object count: 1
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -7500 1 100 ""]
(
ElementLine[-3340 -6000 -1370 -6000 260]
ElementLine[-1370 -6000 -1370 -6490 260]
ElementLine[-3340 -6490 -1370 -6490 260]
ElementLine[-3340 -6000 -3340 -6490 260]
ElementLine[-3340 -4820 -2460 -4820 260]
ElementLine[-2460 -4820 -2460 -6100 260]
ElementLine[-3340 -6100 -2460 -6100 260]
ElementLine[-3340 -4820 -3340 -6100 260]
ElementLine[-1770 -4820 -1270 -4820 260]
ElementLine[-1270 -4820 -1270 -5700 260]
ElementLine[-1770 -5700 -1270 -5700 260]
ElementLine[-1770 -4820 -1770 -5700 260]
ElementLine[-2550 -4820 -880 -4820 260]
ElementLine[-880 -4820 -880 -5310 260]
ElementLine[-2550 -5310 -880 -5310 260]
ElementLine[-2550 -4820 -2550 -5310 260]
ElementLine[1370 -5110 3340 -5110 260]
ElementLine[3340 -5110 3340 -6490 260]
ElementLine[1370 -6490 3340 -6490 260]
ElementLine[1370 -5110 1370 -6490 260]
ElementLine[980 -4820 3340 -4820 260]
ElementLine[3340 -4820 3340 -5310 260]
ElementLine[980 -5310 3340 -5310 260]
ElementLine[980 -4820 980 -5310 260]
ElementLine[-3340 -3740 3340 -3740 260]
ElementLine[3340 -3740 3340 -4920 260]
ElementLine[-3340 -4920 3340 -4920 260]
ElementLine[-3340 -3740 -3340 -4920 260]
ElementLine[-3340 6490 3340 6490 260]
ElementLine[3340 6490 3340 3740 260]
ElementLine[-3340 3740 3340 3740 260]
ElementLine[-3340 6490 -3340 3740 260]
ElementLine[-3340 -1370 -2060 -1370 260]
ElementLine[-2060 -1370 -2060 -3050 260]
ElementLine[-3340 -3050 -2060 -3050 260]
ElementLine[-3340 -1370 -3340 -3050 260]
ElementLine[2060 -1370 3340 -1370 260]
ElementLine[3340 -1370 3340 -3050 260]
ElementLine[2060 -3050 3340 -3050 260]
ElementLine[2060 -1370 2060 -3050 260]
ElementLine[-680 0 680 0 260]
ElementLine[680 0 680 -1370 260]
ElementLine[-680 -1370 680 -1370 260]
ElementLine[-680 0 -680 -1370 260]
ElementLine[-3140 3740 -3140 -3740 400]
ElementLine[3140 -3740 3140 3740 400]
ElementArc[-2160 -5610 269 269 0 360 200]
ElementArc[0 -6390 1570 1570 180 -180 400]
Pad[0 6880 0 6880 5900 2000 6700 "" "A" "square"]
Pad[0 -6880 0 -6880 5900 2000 6700 "" "C" "square"]
)
