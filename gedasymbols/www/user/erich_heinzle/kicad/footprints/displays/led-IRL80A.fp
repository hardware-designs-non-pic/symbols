# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-IRL80A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-IRL80A
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 1
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 17500 -7500 0 100 ""]
(
ElementLine[3500 -9000 3500 -7000 600]
ElementLine[3500 -7000 3500 -3000 600]
ElementLine[3500 -3000 3500 2500 600]
ElementLine[3500 2500 3500 7000 600]
ElementLine[3500 7000 3500 9000 600]
ElementLine[3500 9000 -3500 9000 600]
ElementLine[-3500 -9000 -3500 -7000 600]
ElementLine[-3500 -7000 -3500 -3000 600]
ElementLine[-3500 -3000 -3500 3000 600]
ElementLine[-3500 3000 -3500 7000 600]
ElementLine[-3500 7000 -3500 9000 600]
ElementLine[-3500 -9000 3500 -9000 600]
ElementLine[-5500 -1000 -5500 1000 200]
ElementLine[-4500 -2000 -4500 2000 200]
ElementArc[-3500 0 3000 3000 90 -180 600]
Pin[0 5000 5200 2000 11200 3200 "" "A" ""]
Pad[-2600 5000 2600 5000 5200 2000 6000 "" "A" ""]
Pad[-2600 5000 2600 5000 5200 2000 6000 "" "A" ",onsolder"]
Pin[0 -5000 5200 2000 11200 3200 "" "K" ""]
Pad[-2600 -5000 2600 -5000 5200 2000 6000 "" "K" ""]
Pad[-2600 -5000 2600 -5000 5200 2000 6000 "" "K" ",onsolder"]
)
