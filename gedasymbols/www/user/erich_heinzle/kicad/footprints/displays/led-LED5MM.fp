# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LED5MM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LED5MM
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 1
# Draw arc object count: 7
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 27500 -7500 0 100 ""]
(
ElementLine[10000 7500 10000 -7500 800]
ElementArc[0 0 7071 7071 0 360 300]
ElementArc[0 0 12500 12500 180 -286 1000]
ElementArc[0 0 4500 4500 0 -90 600]
ElementArc[0 0 4500 4500 180 -90 600]
ElementArc[0 0 6500 6500 0 -90 600]
ElementArc[0 0 6500 6500 180 -90 600]
ElementArc[0 0 8500 8500 0 -90 600]
ElementArc[0 0 8500 8500 180 -90 600]
Pin[-5000 0 6600 2000 7400 3200 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ",onsolder"]
Pin[5000 0 6600 2000 7400 3200 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ",onsolder"]
)
