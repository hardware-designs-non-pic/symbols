# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-P-LCC-2-TOPLED-RG
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-P-LCC-2-TOPLED-RG
# Text descriptor count: 1
# Draw segment object count: 43
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -12500 -2500 1 100 ""]
(
ElementLine[-5110 11810 5110 11810 260]
ElementLine[5110 11810 5110 5900 260]
ElementLine[-5110 5900 5110 5900 260]
ElementLine[-5110 11810 -5110 5900 260]
ElementLine[-5110 -5900 5110 -5900 260]
ElementLine[5110 -5900 5110 -11810 260]
ElementLine[-5110 -11810 5110 -11810 260]
ElementLine[-5110 -5900 -5110 -11810 260]
ElementLine[-980 980 980 980 260]
ElementLine[980 980 980 -980 260]
ElementLine[-980 -980 980 -980 260]
ElementLine[-980 980 -980 -980 260]
ElementLine[-4520 -9440 4520 -9440 260]
ElementLine[4520 -9440 4520 -10620 260]
ElementLine[-4520 -10620 4520 -10620 260]
ElementLine[-4520 -9440 -4520 -10620 260]
ElementLine[-4520 10620 4520 10620 260]
ElementLine[4520 10620 4520 9440 260]
ElementLine[-4520 9440 4520 9440 260]
ElementLine[-4520 10620 -4520 9440 260]
ElementLine[-5900 -5900 5900 -5900 260]
ElementLine[5900 -5900 5900 -12590 260]
ElementLine[-5900 -12590 5900 -12590 260]
ElementLine[-5900 -5900 -5900 -12590 260]
ElementLine[-5900 12590 5900 12590 260]
ElementLine[5900 12590 5900 5900 260]
ElementLine[-5900 5900 5900 5900 260]
ElementLine[-5900 12590 -5900 5900 260]
ElementLine[-5510 4130 -5510 6290 800]
ElementLine[-5510 6290 -4330 6290 800]
ElementLine[-4330 6290 -3340 6290 800]
ElementLine[-3340 6290 4330 6290 800]
ElementLine[4330 6290 5510 6290 800]
ElementLine[5510 6290 5510 -6290 800]
ElementLine[5510 -6290 -5510 -6290 800]
ElementLine[-4330 -6290 -4330 -9640 400]
ElementLine[4330 -9640 4330 -6290 400]
ElementLine[-4330 6290 -4330 9640 400]
ElementLine[4330 9640 4330 6290 400]
ElementLine[-3340 6290 -5510 4130 800]
ElementLine[-5510 -6290 -5510 4130 800]
ElementLine[-5510 0 5510 0 500]
ElementLine[0 5510 0 -5510 500]
ElementArc[0 0 3055 3055 0 360 400]
ElementArc[0 0 3889 3889 0 360 500]
Pad[0 -13770 0 -13770 15740 2000 16540 "" "A" "square"]
Pad[0 13770 0 13770 15740 2000 16540 "" "C" "square"]
)
