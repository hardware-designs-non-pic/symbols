# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 7segdisp_acda02
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 7segdisp_acda02
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 20
#
Element["" "DISP***" "" "" 0 0 0 -29133 0 100 ""]
(
ElementLine[8270 -1180 9840 -8660 1180]
ElementLine[20080 -1180 21650 -8660 1180]
ElementLine[18500 8660 20080 1180 1180]
ElementLine[6690 8660 8270 1180 1180]
ElementLine[-9840 8660 -8270 1180 1180]
ElementLine[-20080 1180 -21650 8660 1180]
ElementLine[-18500 -8660 -20080 -1180 1180]
ElementLine[-8270 -1180 -6690 -8660 1180]
ElementLine[17330 9840 7879 9840 1180]
ElementLine[20480 -9840 11030 -9840 1180]
ElementLine[-7870 -9840 -17320 -9840 1180]
ElementLine[-11030 9840 -20480 9840 1180]
ElementLine[18900 0 9450 0 1180]
ElementLine[-9450 0 -18900 0 1180]
ElementLine[790 -19690 790 19690 1180]
ElementLine[-790 19690 -790 -19690 1180]
ElementLine[-27950 19690 -27950 -19690 1180]
ElementLine[-27950 -19690 27950 -19690 1180]
ElementLine[27950 -19690 27950 19690 1180]
ElementLine[27950 19690 -27950 19690 1180]
ElementArc[23620 8270 790 790 0 360 1180]
ElementArc[-4720 8270 790 790 0 360 1180]
Pad[-25500 15950 -25500 23430 3540 2000 4340 "" "1" "square"]
Pad[-19840 15950 -19840 23430 3540 2000 4340 "" "2" "square"]
Pad[-14170 15950 -14170 23430 3540 2000 4340 "" "3" "square"]
Pad[-8500 15950 -8500 23430 3540 2000 4340 "" "4" "square"]
Pad[-2830 15950 -2830 23430 3540 2000 4340 "" "5" "square"]
Pad[2830 15950 2830 23430 3540 2000 4340 "" "6" "square"]
Pad[8500 15950 8500 23430 3540 2000 4340 "" "7" "square"]
Pad[14170 15950 14170 23430 3540 2000 4340 "" "8" "square"]
Pad[19840 15950 19840 23430 3540 2000 4340 "" "9" "square"]
Pad[25510 15950 25510 23430 3540 2000 4340 "" "10" "square"]
Pad[25510 -23430 25510 -15950 3540 2000 4340 "" "11" "square"]
Pad[19840 -23430 19840 -15950 3540 2000 4340 "" "12" "square"]
Pad[14170 -23430 14170 -15950 3540 2000 4340 "" "13" "square"]
Pad[8500 -23430 8500 -15950 3540 2000 4340 "" "14" "square"]
Pad[2830 -23430 2830 -15950 3540 2000 4340 "" "15" "square"]
Pad[-2830 -23430 -2830 -15950 3540 2000 4340 "" "16" "square"]
Pad[-8500 -23430 -8500 -15950 3540 2000 4340 "" "17" "square"]
Pad[-14170 -23430 -14170 -15950 3540 2000 4340 "" "18" "square"]
Pad[-19840 -23430 -19840 -15950 3540 2000 4340 "" "19" "square"]
Pad[-25510 -23430 -25510 -15950 3540 2000 4340 "" "20" "square"]
)
