# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-MICRO-SIDELED
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-MICRO-SIDELED
# Text descriptor count: 1
# Draw segment object count: 33
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -2500 1 100 ""]
(
ElementLine[-1570 -4330 1570 -4330 260]
ElementLine[1570 -4330 1570 -7080 260]
ElementLine[-1570 -7080 1570 -7080 260]
ElementLine[-1570 -4330 -1570 -7080 260]
ElementLine[-1570 7080 1570 7080 260]
ElementLine[1570 7080 1570 4330 260]
ElementLine[-1570 4330 1570 4330 260]
ElementLine[-1570 7080 -1570 4330 260]
ElementLine[-1370 6880 1370 6880 260]
ElementLine[1370 6880 1370 4520 260]
ElementLine[-1370 4520 1370 4520 260]
ElementLine[-1370 6880 -1370 4520 260]
ElementLine[-1370 -4520 1370 -4520 260]
ElementLine[1370 -4520 1370 -6880 260]
ElementLine[-1370 -6880 1370 -6880 260]
ElementLine[-1370 -4520 -1370 -6880 260]
ElementLine[-490 -4420 490 -4420 260]
ElementLine[490 -4420 490 -6880 260]
ElementLine[-490 -6880 490 -6880 260]
ElementLine[-490 -4420 -490 -6880 260]
ElementLine[-490 6880 490 6880 260]
ElementLine[490 6880 490 4420 260]
ElementLine[-490 4420 490 4420 260]
ElementLine[-490 6880 -490 4420 260]
ElementLine[2550 -4330 -390 -4330 400]
ElementLine[-390 -4330 -1370 -3930 400]
ElementLine[-1370 -3930 -1370 3540 400]
ElementLine[-1370 3540 -390 4330 400]
ElementLine[-390 4330 2550 4330 400]
ElementLine[2550 4330 2550 -4330 400]
ElementLine[2360 -3540 980 -2750 200]
ElementLine[980 -2750 980 2750 200]
ElementLine[980 2750 2360 3540 200]
Pad[0 -7670 0 -7670 6290 2000 7090 "" "A" "square"]
Pad[0 7670 0 7670 6290 2000 7090 "" "C" "square"]
)
