# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module display-lcd-lxd-30-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: display-lcd-lxd-30-E
# Text descriptor count: 1
# Draw segment object count: 52
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 24
#
Element["" ">NAME" "" "" 0 0 2500 -29500 0 100 ""]
(
ElementLine[-19500 17500 -15500 17500 260]
ElementLine[-15500 17500 -15500 13500 260]
ElementLine[-19500 13500 -15500 13500 260]
ElementLine[-19500 17500 -19500 13500 260]
ElementLine[13000 17500 17000 17500 260]
ElementLine[17000 17500 17000 13500 260]
ElementLine[13000 13500 17000 13500 260]
ElementLine[13000 17500 13000 13500 260]
ElementLine[-60000 45000 60000 45000 500]
ElementLine[60000 -45000 -60000 -45000 500]
ElementLine[60000 45000 60000 35000 500]
ElementLine[-60000 35000 -60000 45000 500]
ElementLine[60000 35000 -60000 35000 500]
ElementLine[-60000 -45000 -60000 -35000 500]
ElementLine[60000 35000 60000 -35000 500]
ElementLine[-60000 -35000 60000 -35000 500]
ElementLine[-60000 -35000 -60000 -20000 500]
ElementLine[-60000 -20000 -60000 20000 500]
ElementLine[-60000 20000 -60000 35000 500]
ElementLine[60000 -35000 60000 -45000 500]
ElementLine[-50000 -22500 -50000 22500 500]
ElementLine[-50000 22500 -47500 25000 500]
ElementLine[-47500 25000 47500 25000 500]
ElementLine[47500 25000 50000 22500 500]
ElementLine[50000 22500 50000 -22500 500]
ElementLine[50000 -22500 47500 -25000 500]
ElementLine[47500 -25000 -47500 -25000 500]
ElementLine[-47500 -25000 -50000 -22500 500]
ElementLine[27000 -12500 26000 -3000 3600]
ElementLine[26000 3000 25000 12500 3600]
ElementLine[28490 15500 34490 15500 3600]
ElementLine[38000 12500 39000 3000 3600]
ElementLine[39000 -3000 40000 -12500 3600]
ElementLine[30500 -15500 36500 -15500 3600]
ElementLine[29500 0 35500 0 3600]
ElementLine[-38000 -12500 -39000 -3000 3600]
ElementLine[-39000 3000 -40000 12500 3600]
ElementLine[-36500 15500 -30500 15500 3600]
ElementLine[-27000 12500 -26000 3000 3600]
ElementLine[-26000 -3000 -25000 -12500 3600]
ElementLine[-34490 -15500 -28490 -15500 3600]
ElementLine[-35500 0 -29500 0 3600]
ElementLine[-5500 -12500 -6500 -3000 3600]
ElementLine[-6500 3000 -7500 12500 3600]
ElementLine[-4000 15500 2000 15500 3600]
ElementLine[5500 12500 6500 3000 3600]
ElementLine[6500 -3000 7500 -12500 3600]
ElementLine[-2000 -15500 4000 -15500 3600]
ElementLine[-3000 0 3000 0 3600]
ElementLine[-60000 -20000 -64000 -20000 500]
ElementLine[-64000 -20000 -64000 20000 500]
ElementLine[-64000 20000 -60000 20000 500]
Pad[-55000 37500 -55000 42500 5000 2000 5800 "" "1" "square"]
Pad[-45000 37500 -45000 42500 5000 2000 5800 "" "2" "square"]
Pad[-35000 37500 -35000 42500 5000 2000 5800 "" "3" "square"]
Pad[-25000 37500 -25000 42500 5000 2000 5800 "" "4" "square"]
Pad[-15000 37500 -15000 42500 5000 2000 5800 "" "5" "square"]
Pad[-5000 37500 -5000 42500 5000 2000 5800 "" "6" "square"]
Pad[5000 37500 5000 42500 5000 2000 5800 "" "7" "square"]
Pad[15000 37500 15000 42500 5000 2000 5800 "" "8" "square"]
Pad[25000 37500 25000 42500 5000 2000 5800 "" "9" "square"]
Pad[35000 37500 35000 42500 5000 2000 5800 "" "10" "square"]
Pad[45000 37500 45000 42500 5000 2000 5800 "" "11" "square"]
Pad[55000 37500 55000 42500 5000 2000 5800 "" "12" "square"]
Pad[55000 -42500 55000 -37500 5000 2000 5800 "" "13" "square"]
Pad[45000 -42500 45000 -37500 5000 2000 5800 "" "14" "square"]
Pad[35000 -42500 35000 -37500 5000 2000 5800 "" "15" "square"]
Pad[25000 -42500 25000 -37500 5000 2000 5800 "" "16" "square"]
Pad[15000 -42500 15000 -37500 5000 2000 5800 "" "17" "square"]
Pad[5000 -42500 5000 -37500 5000 2000 5800 "" "18" "square"]
Pad[-5000 -42500 -5000 -37500 5000 2000 5800 "" "19" "square"]
Pad[-15000 -42500 -15000 -37500 5000 2000 5800 "" "20" "square"]
Pad[-25000 -42500 -25000 -37500 5000 2000 5800 "" "21" "square"]
Pad[-35000 -42500 -35000 -37500 5000 2000 5800 "" "22" "square"]
Pad[-45000 -42500 -45000 -37500 5000 2000 5800 "" "23" "square"]
Pad[-55000 -42500 -55000 -37500 5000 2000 5800 "" "24" "square"]
)
