# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-CHIPLED_0805
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-CHIPLED_0805
# Text descriptor count: 1
# Draw segment object count: 50
# Draw circle object count: 1
# Draw arc object count: 2
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -7500 1 100 ""]
(
ElementLine[1180 -1960 2460 -1960 260]
ElementLine[2460 -1960 2460 -3930 260]
ElementLine[1180 -3930 2460 -3930 260]
ElementLine[1180 -1960 1180 -3930 260]
ElementLine[-1270 -1960 -680 -1960 260]
ElementLine[-680 -1960 -680 -2950 260]
ElementLine[-1270 -2950 -680 -2950 260]
ElementLine[-1270 -1960 -1270 -2950 260]
ElementLine[680 -1960 1270 -1960 260]
ElementLine[1270 -1960 1270 -2950 260]
ElementLine[680 -2950 1270 -2950 260]
ElementLine[680 -1960 680 -2950 260]
ElementLine[-780 -1960 780 -1960 260]
ElementLine[780 -1960 780 -2650 260]
ElementLine[-780 -2650 780 -2650 260]
ElementLine[-780 -1960 -780 -2650 260]
ElementLine[1180 3930 2460 3930 260]
ElementLine[2460 3930 2460 1960 260]
ElementLine[1180 1960 2460 1960 260]
ElementLine[1180 3930 1180 1960 260]
ElementLine[-2460 3930 -1180 3930 260]
ElementLine[-1180 3930 -1180 1960 260]
ElementLine[-2460 1960 -1180 1960 260]
ElementLine[-2460 3930 -2460 1960 260]
ElementLine[680 2950 1270 2950 260]
ElementLine[1270 2950 1270 1960 260]
ElementLine[680 1960 1270 1960 260]
ElementLine[680 2950 680 1960 260]
ElementLine[-1270 2950 -680 2950 260]
ElementLine[-680 2950 -680 1960 260]
ElementLine[-1270 1960 -680 1960 260]
ElementLine[-1270 2950 -1270 1960 260]
ElementLine[-780 2650 780 2650 260]
ElementLine[780 2650 780 1960 260]
ElementLine[-780 1960 780 1960 260]
ElementLine[-780 2650 -780 1960 260]
ElementLine[-390 0 390 0 260]
ElementLine[390 0 390 -780 260]
ElementLine[-390 -780 390 -780 260]
ElementLine[-390 0 -390 -780 260]
ElementLine[-2360 -1960 -1180 -1960 260]
ElementLine[-1180 -1960 -1180 -3140 260]
ElementLine[-2360 -3140 -1180 -3140 260]
ElementLine[-2360 -1960 -2360 -3140 260]
ElementLine[-2460 -3640 -1570 -3640 260]
ElementLine[-1570 -3640 -1570 -3930 260]
ElementLine[-2460 -3930 -1570 -3930 260]
ElementLine[-2460 -3640 -2460 -3930 260]
ElementLine[2260 -2060 2260 2060 400]
ElementLine[-2260 1960 -2260 -3640 400]
ElementArc[-1770 -3340 283 283 0 360 200]
ElementArc[0 -3930 1370 1370 180 -180 400]
ElementArc[0 3930 1370 1370 0 -180 400]
Pad[0 4130 0 4130 4720 2000 5520 "" "A" "square"]
Pad[0 -4130 0 -4130 4720 2000 5520 "" "C" "square"]
)
