# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 7segdisp_sa05
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 7segdisp_sa05
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" "DISP***" "" "" 0 0 0 -43307 0 100 ""]
(
ElementLine[25000 -37500 25000 37500 1270]
ElementLine[-25000 37500 -25000 -37500 1270]
ElementLine[15000 0 -15000 0 5510]
ElementLine[-12000 -23000 -18000 23000 5510]
ElementLine[-18000 23000 12000 23000 5510]
ElementLine[12000 23000 18000 -23000 5510]
ElementLine[18000 -23000 -12000 -23000 5510]
ElementLine[-20000 37500 -25000 32500 1200]
ElementLine[-23000 37500 -25000 35500 1200]
ElementLine[-21500 37500 -25000 34000 1200]
ElementLine[-25000 37500 25000 37500 1270]
ElementLine[25000 -37500 -25000 -37500 1270]
ElementArc[19500 22500 1414 1414 0 360 3939]
Pin[-20000 30000 5910 2000 6710 3150 "" "1" "blah"]
Pad[-20000 28625 -20000 31375 5910 2000 6710 "" "1" "square"]
Pad[-20000 28625 -20000 31375 5910 2000 6710 "" "1" "square,onsolder"]
Pin[-10000 30000 5910 2000 6710 3150 "" "2" ""]
Pad[-10000 28625 -10000 31375 5910 2000 6710 "" "2" ""]
Pad[-10000 28625 -10000 31375 5910 2000 6710 "" "2" ",onsolder"]
Pin[0 30000 5910 2000 6710 3150 "" "3" ""]
Pad[0 28625 0 31375 5910 2000 6710 "" "3" ""]
Pad[0 28625 0 31375 5910 2000 6710 "" "3" ",onsolder"]
Pin[10000 30000 5910 2000 6710 3150 "" "4" ""]
Pad[10000 28625 10000 31375 5910 2000 6710 "" "4" ""]
Pad[10000 28625 10000 31375 5910 2000 6710 "" "4" ",onsolder"]
Pin[20000 30000 5910 2000 6710 3150 "" "5" ""]
Pad[20000 28625 20000 31375 5910 2000 6710 "" "5" ""]
Pad[20000 28625 20000 31375 5910 2000 6710 "" "5" ",onsolder"]
Pin[20000 -30000 5910 2000 6710 3150 "" "6" ""]
Pad[20000 -31375 20000 -28625 5910 2000 6710 "" "6" ""]
Pad[20000 -31375 20000 -28625 5910 2000 6710 "" "6" ",onsolder"]
Pin[10000 -30000 5910 2000 6710 3150 "" "7" ""]
Pad[10000 -31375 10000 -28625 5910 2000 6710 "" "7" ""]
Pad[10000 -31375 10000 -28625 5910 2000 6710 "" "7" ",onsolder"]
Pin[0 -30000 5910 2000 6710 3150 "" "8" ""]
Pad[0 -31375 0 -28625 5910 2000 6710 "" "8" ""]
Pad[0 -31375 0 -28625 5910 2000 6710 "" "8" ",onsolder"]
Pin[-10000 -30000 5910 2000 6710 3150 "" "9" ""]
Pad[-10000 -31375 -10000 -28625 5910 2000 6710 "" "9" ""]
Pad[-10000 -31375 -10000 -28625 5910 2000 6710 "" "9" ",onsolder"]
Pin[-20000 -30000 5910 2000 6710 3150 "" "10" ""]
Pad[-20000 -31375 -20000 -28625 5910 2000 6710 "" "10" ""]
Pad[-20000 -31375 -20000 -28625 5910 2000 6710 "" "10" ",onsolder"]
)
