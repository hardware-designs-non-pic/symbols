# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-DUOLED-A-5MM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-DUOLED-A-5MM
# Text descriptor count: 1
# Draw segment object count: 3
# Draw circle object count: 0
# Draw arc object count: 13
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 27500 -12500 0 100 ""]
(
ElementLine[10000 -5700 10000 -3500 1000]
ElementLine[10000 -3700 10000 3500 1000]
ElementLine[10000 3500 10000 5700 1000]
ElementArc[0 0 11513 11513 0 -34 1000]
ElementArc[0 0 9996 9996 180 -40 600]
ElementArc[0 0 9996 9996 180 -140 600]
ElementArc[0 0 11501 11501 0 -133 1000]
ElementArc[0 0 9996 9996 0 -40 600]
ElementArc[0 0 11510 11510 180 -133 1000]
ElementArc[0 0 9996 9996 0 -140 600]
ElementArc[0 0 4500 4500 0 -90 600]
ElementArc[0 0 4500 4500 180 -90 600]
ElementArc[0 0 6500 6500 0 -90 600]
ElementArc[0 0 6500 6500 180 -90 600]
ElementArc[0 0 8500 8500 0 -90 600]
ElementArc[0 0 8500 8500 180 -90 600]
Pin[0 0 5200 2000 6000 3200 "" "A" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "A" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[10000 0 5200 2000 6000 3200 "" "CR" ""]
Pad[10000 -2600 10000 2600 5200 2000 6000 "" "CR" ""]
Pad[10000 -2600 10000 2600 5200 2000 6000 "" "CR" ",onsolder"]
Pin[-10000 0 5200 2000 6000 3200 "" "CX" ""]
Pad[-10000 -2600 -10000 2600 5200 2000 6000 "" "CX" ""]
Pad[-10000 -2600 -10000 2600 5200 2000 6000 "" "CX" ",onsolder"]
)
