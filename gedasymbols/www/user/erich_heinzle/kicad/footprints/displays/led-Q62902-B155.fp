# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-Q62902-B155
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-Q62902-B155
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 1
# Draw arc object count: 1
# Pad count: 2
#
Element["" "A+" "" "" 0 0 17000 9000 0 100 ""]
(
ElementLine[11500 -4000 26500 -4000 260]
ElementLine[26500 -4000 26500 -6000 260]
ElementLine[11500 -6000 26500 -6000 260]
ElementLine[11500 -4000 11500 -6000 260]
ElementLine[11500 6000 26500 6000 260]
ElementLine[26500 6000 26500 4000 260]
ElementLine[11500 4000 26500 4000 260]
ElementLine[11500 6000 11500 4000 260]
ElementLine[-5000 12000 -5000 10000 600]
ElementLine[39500 -12000 11500 -12000 600]
ElementLine[39500 -12000 39500 12000 600]
ElementLine[-5000 12000 11500 12000 600]
ElementLine[11500 12000 11500 -12000 600]
ElementLine[11500 12000 39500 12000 600]
ElementLine[11500 -12000 -5000 -12000 600]
ElementLine[-5000 -10000 -20500 -10000 600]
ElementLine[-5000 -10000 -5000 -12000 600]
ElementLine[-20500 10000 -5000 10000 600]
ElementLine[-5000 10000 -5000 -10000 600]
ElementLine[-27500 -2500 -27500 2500 600]
ElementLine[-24000 -5500 -24000 5500 600]
ElementLine[-20500 -7500 -20500 7500 600]
ElementLine[-1600 0 1600 0 500]
ElementLine[0 1600 0 -1600 500]
ElementArc[0 0 1131 1131 0 360 500]
ElementArc[-20500 0 10000 10000 90 -180 600]
Pin[30000 5000 5200 2000 11200 3200 "" "A" ""]
Pad[27400 5000 32600 5000 5200 2000 6000 "" "A" ""]
Pad[27400 5000 32600 5000 5200 2000 6000 "" "A" ",onsolder"]
Pin[30000 -5000 5200 2000 11200 3200 "" "K" ""]
Pad[27400 -5000 32600 -5000 5200 2000 6000 "" "K" ""]
Pad[27400 -5000 32600 -5000 5200 2000 6000 "" "K" ",onsolder"]
)
