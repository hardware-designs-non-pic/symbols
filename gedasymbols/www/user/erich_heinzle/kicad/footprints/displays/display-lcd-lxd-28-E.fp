# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module display-lcd-lxd-28-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: display-lcd-lxd-28-E
# Text descriptor count: 1
# Draw segment object count: 45
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 18
#
Element["" ">NAME" "" "" 0 0 4500 -56000 0 100 ""]
(
ElementLine[2500 35000 7500 35000 260]
ElementLine[7500 35000 7500 30000 260]
ElementLine[2500 30000 7500 30000 260]
ElementLine[2500 35000 2500 30000 260]
ElementLine[-52500 35000 -47500 35000 260]
ElementLine[-47500 35000 -47500 30000 260]
ElementLine[-52500 30000 -47500 30000 260]
ElementLine[-52500 35000 -52500 30000 260]
ElementLine[-75000 75000 75000 75000 500]
ElementLine[75000 75000 75000 60000 500]
ElementLine[-75000 60000 -75000 75000 500]
ElementLine[75000 60000 -75000 60000 500]
ElementLine[-75000 -75000 -75000 -60000 500]
ElementLine[75000 60000 75000 -60000 500]
ElementLine[-75000 -60000 -75000 -20000 500]
ElementLine[-75000 -20000 -75000 20000 500]
ElementLine[-75000 20000 -75000 60000 500]
ElementLine[75000 -60000 75000 -75000 500]
ElementLine[-62500 -50000 -65000 -47500 500]
ElementLine[-65000 -47500 -65000 47500 500]
ElementLine[-65000 47500 -62500 50000 500]
ElementLine[-62500 50000 62500 50000 500]
ElementLine[62500 50000 65000 47500 500]
ElementLine[65000 47500 65000 -47500 500]
ElementLine[65000 -47500 62500 -50000 500]
ElementLine[62500 -50000 -62500 -50000 500]
ElementLine[22500 -28000 20000 -5000 6600]
ElementLine[20000 5000 17500 28000 6600]
ElementLine[23000 32500 42000 32500 6600]
ElementLine[47500 28000 50000 5000 6600]
ElementLine[50000 -5000 52500 -28000 6600]
ElementLine[28000 -32500 47000 -32500 6600]
ElementLine[25500 0 44500 0 6600]
ElementLine[-32500 -28000 -35000 -5000 6600]
ElementLine[-35000 5000 -37500 28000 6600]
ElementLine[-32000 32500 -13000 32500 6600]
ElementLine[-7500 28000 -5000 5000 6600]
ElementLine[-5000 -5000 -2500 -28000 6600]
ElementLine[-27000 -32500 -8000 -32500 6600]
ElementLine[-29500 0 -10500 0 6600]
ElementLine[-75000 -60000 75000 -60000 500]
ElementLine[75000 -75000 -75000 -75000 500]
ElementLine[-75000 -20000 -79000 -20000 500]
ElementLine[-79000 -20000 -79000 20000 500]
ElementLine[-79000 20000 -75000 20000 500]
Pad[-40000 62500 -40000 72500 5000 2000 5800 "" "1" "square"]
Pad[-30000 62500 -30000 72500 5000 2000 5800 "" "2" "square"]
Pad[-20000 62500 -20000 72500 5000 2000 5800 "" "3" "square"]
Pad[-10000 62500 -10000 72500 5000 2000 5800 "" "4" "square"]
Pad[0 62500 0 72500 5000 2000 5800 "" "5" "square"]
Pad[10000 62500 10000 72500 5000 2000 5800 "" "6" "square"]
Pad[20000 62500 20000 72500 5000 2000 5800 "" "7" "square"]
Pad[30000 62500 30000 72500 5000 2000 5800 "" "8" "square"]
Pad[40000 62500 40000 72500 5000 2000 5800 "" "9" "square"]
Pad[40000 -72500 40000 -62500 5000 2000 5800 "" "10" "square"]
Pad[30000 -72500 30000 -62500 5000 2000 5800 "" "11" "square"]
Pad[20000 -72500 20000 -62500 5000 2000 5800 "" "12" "square"]
Pad[10000 -72500 10000 -62500 5000 2000 5800 "" "13" "square"]
Pad[0 -72500 0 -62500 5000 2000 5800 "" "14" "square"]
Pad[-10000 -72500 -10000 -62500 5000 2000 5800 "" "15" "square"]
Pad[-20000 -72500 -20000 -62500 5000 2000 5800 "" "16" "square"]
Pad[-30000 -72500 -30000 -62500 5000 2000 5800 "" "17" "square"]
Pad[-40000 -72500 -40000 -62500 5000 2000 5800 "" "18" "square"]
)
