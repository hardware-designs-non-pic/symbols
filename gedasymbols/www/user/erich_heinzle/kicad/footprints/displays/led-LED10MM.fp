# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LED10MM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LED10MM
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 1
# Draw arc object count: 9
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 37500 -7500 0 100 ""]
(
ElementLine[20000 -10000 20000 10000 1000]
ElementArc[0 0 14142 14142 0 360 250]
ElementArc[0 0 22361 22361 180 -307 1000]
ElementArc[0 0 17500 17500 180 -90 500]
ElementArc[0 0 15000 15000 180 -90 500]
ElementArc[0 0 12500 12500 180 -90 500]
ElementArc[0 0 10000 10000 180 -90 500]
ElementArc[0 0 17500 17500 0 -90 500]
ElementArc[0 0 15000 15000 0 -90 500]
ElementArc[0 0 12500 12500 0 -90 500]
ElementArc[0 0 10000 10000 0 -90 500]
Pin[-5000 0 6600 2000 7400 3200 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ""]
Pad[-5000 -3300 -5000 3300 6600 2000 7400 "" "A" ",onsolder"]
Pin[5000 0 6600 2000 7400 3200 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ""]
Pad[5000 -3300 5000 3300 6600 2000 7400 "" "K" ",onsolder"]
)
