# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LSU260
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LSU260
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 1
# Draw arc object count: 3
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 5000 -7000 0 100 ""]
(
ElementLine[-5500 1000 -4500 1000 260]
ElementLine[-4500 1000 -4500 -1000 260]
ElementLine[-5500 -1000 -4500 -1000 260]
ElementLine[-5500 1000 -5500 -1000 260]
ElementLine[2000 1000 5500 1000 260]
ElementLine[5500 1000 5500 -1000 260]
ElementLine[2000 -1000 5500 -1000 260]
ElementLine[2000 1000 2000 -1000 260]
ElementLine[0 2000 -4500 2000 600]
ElementLine[-4500 2000 -4500 1000 600]
ElementLine[-4500 -2000 0 -2000 600]
ElementLine[-4500 1000 -5500 1000 600]
ElementLine[-4500 1000 -4500 -1000 600]
ElementLine[-5500 1000 -5500 -1000 600]
ElementLine[-5500 -1000 -4500 -1000 600]
ElementLine[-4500 -1000 -4500 -2000 600]
ElementLine[2000 1000 5500 1000 600]
ElementLine[5500 1000 5500 -1000 600]
ElementLine[5500 -1000 2000 -1000 600]
ElementLine[1500 1500 1000 2000 600]
ElementLine[1000 2000 -1000 2000 600]
ElementLine[-1500 1500 -1000 2000 600]
ElementLine[1500 -1500 1000 -2000 600]
ElementLine[1000 -2000 -1000 -2000 600]
ElementLine[-1500 -1500 -1000 -2000 600]
ElementArc[0 0 1414 1414 0 360 300]
ElementArc[0 0 1000 1000 180 -90 600]
ElementArc[0 0 1000 1000 0 -90 600]
ElementArc[0 0 2121 2121 225 -90 600]
Pin[-5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
