# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 7segdisp
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 7segdisp
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 10
#
Element["" "DISP***" "" "" 0 0 0 -29527 0 100 ""]
(
ElementLine[-15000 -20000 -10000 -25000 1200]
ElementLine[-9500 13000 -7000 2000 1200]
ElementLine[3500 13000 6000 2000 1200]
ElementLine[6500 -1500 9000 -13000 1200]
ElementLine[-6500 -1500 -4000 -13000 1200]
ElementLine[-8500 15000 2000 15000 1200]
ElementLine[-2500 -15000 8000 -15000 1200]
ElementLine[-5500 0 5000 0 1200]
ElementLine[-15000 -23000 -13000 -25000 1200]
ElementLine[-15000 -21500 -11500 -25000 1200]
ElementLine[15000 -25000 -15000 -25000 1200]
ElementLine[-15000 -25000 -15000 25000 1200]
ElementLine[-15000 25000 15000 25000 1200]
ElementLine[15000 25000 15000 -25000 1200]
ElementArc[-9500 -14500 1414 1414 0 360 1200]
Pin[-10000 -20000 5910 2000 9460 3150 "" "1" "blah"]
Pad[-11375 -20000 -8625 -20000 5910 2000 6710 "" "1" "square"]
Pad[-11375 -20000 -8625 -20000 5910 2000 6710 "" "1" "square,onsolder"]
Pin[-10000 -10000 5910 2000 9460 3150 "" "2" ""]
Pad[-11375 -10000 -8625 -10000 5910 2000 6710 "" "2" ""]
Pad[-11375 -10000 -8625 -10000 5910 2000 6710 "" "2" ",onsolder"]
Pin[-10000 0 5910 2000 9460 3150 "" "3" ""]
Pad[-11375 0 -8625 0 5910 2000 6710 "" "3" ""]
Pad[-11375 0 -8625 0 5910 2000 6710 "" "3" ",onsolder"]
Pin[-10000 10000 5910 2000 9460 3150 "" "4" ""]
Pad[-11375 10000 -8625 10000 5910 2000 6710 "" "4" ""]
Pad[-11375 10000 -8625 10000 5910 2000 6710 "" "4" ",onsolder"]
Pin[-10000 20000 5910 2000 9460 3150 "" "5" ""]
Pad[-11375 20000 -8625 20000 5910 2000 6710 "" "5" ""]
Pad[-11375 20000 -8625 20000 5910 2000 6710 "" "5" ",onsolder"]
Pin[10000 20000 5910 2000 9460 3150 "" "6" ""]
Pad[8625 20000 11375 20000 5910 2000 6710 "" "6" ""]
Pad[8625 20000 11375 20000 5910 2000 6710 "" "6" ",onsolder"]
Pin[10000 10000 5910 2000 9460 3150 "" "7" ""]
Pad[8625 10000 11375 10000 5910 2000 6710 "" "7" ""]
Pad[8625 10000 11375 10000 5910 2000 6710 "" "7" ",onsolder"]
Pin[10000 0 5910 2000 9460 3150 "" "8" ""]
Pad[8625 0 11375 0 5910 2000 6710 "" "8" ""]
Pad[8625 0 11375 0 5910 2000 6710 "" "8" ",onsolder"]
Pin[10000 -10000 5910 2000 9460 3150 "" "9" ""]
Pad[8625 -10000 11375 -10000 5910 2000 6710 "" "9" ""]
Pad[8625 -10000 11375 -10000 5910 2000 6710 "" "9" ",onsolder"]
Pin[10000 -20000 5910 2000 9460 3150 "" "10" ""]
Pad[8625 -20000 11375 -20000 5910 2000 6710 "" "10" ""]
Pad[8625 -20000 11375 -20000 5910 2000 6710 "" "10" ",onsolder"]
)
