# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-MINI-TOPLED-SANTANA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-MINI-TOPLED-SANTANA
# Text descriptor count: 1
# Draw segment object count: 49
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -2500 1 100 ""]
(
ElementLine[-2160 -5900 2160 -5900 260]
ElementLine[2160 -5900 2160 -8260 260]
ElementLine[-2160 -8260 2160 -8260 260]
ElementLine[-2160 -5900 -2160 -8260 260]
ElementLine[-2160 8260 2160 8260 260]
ElementLine[2160 8260 2160 5900 260]
ElementLine[-2160 5900 2160 5900 260]
ElementLine[-2160 8260 -2160 5900 260]
ElementLine[-1960 8070 1960 8070 260]
ElementLine[1960 8070 1960 6100 260]
ElementLine[-1960 6100 1960 6100 260]
ElementLine[-1960 8070 -1960 6100 260]
ElementLine[-1960 -6100 1960 -6100 260]
ElementLine[1960 -6100 1960 -8070 260]
ElementLine[-1960 -8070 1960 -8070 260]
ElementLine[-1960 -6100 -1960 -8070 260]
ElementLine[-780 1570 590 1570 260]
ElementLine[590 1570 590 190 260]
ElementLine[-780 190 590 190 260]
ElementLine[-780 1570 -780 190 260]
ElementLine[-1960 8260 1960 8260 260]
ElementLine[1960 8260 1960 5510 260]
ElementLine[-1960 5510 1960 5510 260]
ElementLine[-1960 8260 -1960 5510 260]
ElementLine[-1960 -5510 1960 -5510 260]
ElementLine[1960 -5510 1960 -8070 260]
ElementLine[-1960 -8070 1960 -8070 260]
ElementLine[-1960 -5510 -1960 -8070 260]
ElementLine[-1960 -3930 1960 -3930 260]
ElementLine[1960 -3930 1960 -5510 260]
ElementLine[-1960 -5510 1960 -5510 260]
ElementLine[-1960 -3930 -1960 -5510 260]
ElementLine[-1960 5510 1960 5510 260]
ElementLine[1960 5510 1960 4130 260]
ElementLine[-1960 4130 1960 4130 260]
ElementLine[-1960 5510 -1960 4130 260]
ElementLine[2750 3930 1370 3930 400]
ElementLine[1370 3930 -2750 3930 400]
ElementLine[-2750 3930 -2750 -3930 400]
ElementLine[-2750 -3930 2750 -3930 400]
ElementLine[2750 -3930 2750 2550 400]
ElementLine[2750 2550 2750 3930 400]
ElementLine[1770 2750 -1770 2750 400]
ElementLine[-1770 2750 -1770 -2750 400]
ElementLine[-1770 -2750 1770 -2750 400]
ElementLine[1770 -2750 1770 2750 400]
ElementLine[2750 2550 1370 3930 400]
ElementLine[-5310 0 5310 0 500]
ElementLine[0 5310 0 -5310 500]
ElementArc[0 0 3748 3748 0 360 500]
Pad[0 -8660 0 -8660 6290 2000 7090 "" "A" "square"]
Pad[0 8660 0 8660 6290 2000 7090 "" "C" "square"]
)
