# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Vincent Vocanson
# No warranties express or implied
# Footprint converted from Kicad Module LTS-69xx
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: LTS-69xx
# Text descriptor count: 1
# Draw segment object count: 21
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 8
#
Element["" "AFF***" "" "" 0 0 0 -42500 0 100 ""]
(
ElementLine[12000 3000 7000 22000 2000]
ElementLine[-15000 22000 2000 22000 2000]
ElementLine[-15000 2000 -20000 22000 2000]
ElementLine[-8000 -22000 -14000 -2000 2000]
ElementLine[-3000 -22000 12000 -22000 2000]
ElementLine[-9000 0 7000 0 2000]
ElementLine[17000 -21000 13000 -1000 2000]
ElementLine[-15000 22500 2500 22500 1200]
ElementLine[-20000 -37500 -25000 -37500 1200]
ElementLine[-25000 -37500 -25000 37500 1200]
ElementLine[-25000 37500 -20000 37500 1200]
ElementLine[20000 37500 25000 37500 1200]
ElementLine[25000 37500 25000 -37500 1200]
ElementLine[25000 -37500 20000 -37500 1200]
ElementLine[-20000 37500 20000 37500 1200]
ElementLine[20000 -37500 -20000 -37500 1200]
ElementLine[-9500 0 7500 0 1200]
ElementLine[13500 -2000 17000 -21500 1200]
ElementLine[-3500 -22000 12000 -22000 1200]
ElementLine[-14000 -2500 -8500 -22000 1200]
ElementLine[-15000 2000 -20000 22500 1200]
ElementArc[17500 22500 2500 2500 0 360 2000]
Pin[-15000 30000 8000 2000 8800 3600 "" "1" "square"]
Pin[-5000 30000 8000 2000 8800 3600 "" "2" ""]
Pin[5000 30000 8000 2000 8800 3600 "" "3" ""]
Pin[15000 30000 8000 2000 8800 3600 "" "4" ""]
Pin[15000 -30000 8000 2000 8800 3600 "" "5" ""]
Pin[5000 -30000 8000 2000 8800 3600 "" "6" ""]
Pin[-5000 -30000 8000 2000 8800 3600 "" "7" ""]
Pin[-15000 -30000 8000 2000 8800 3600 "" "8" ""]
)
