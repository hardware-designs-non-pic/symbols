# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-OSRAM-MINI-TOP-LED
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-OSRAM-MINI-TOP-LED
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "A" "" "" 0 0 0 -10000 0 100 ""]
(
ElementLine[-1960 -2360 1960 -2360 260]
ElementLine[1960 -2360 1960 -5510 260]
ElementLine[-1960 -5510 1960 -5510 260]
ElementLine[-1960 -2360 -1960 -5510 260]
ElementLine[-1960 5510 1960 5510 260]
ElementLine[1960 5510 1960 2360 260]
ElementLine[-1960 2360 1960 2360 260]
ElementLine[-1960 5510 -1960 2360 260]
ElementLine[-590 2360 590 2360 260]
ElementLine[590 2360 590 1180 260]
ElementLine[-590 1180 590 1180 260]
ElementLine[-590 2360 -590 1180 260]
ElementLine[-1770 -2550 1770 -2550 260]
ElementLine[1770 -2550 1770 -5310 260]
ElementLine[-1770 -5310 1770 -5310 260]
ElementLine[-1770 -2550 -1770 -5310 260]
ElementLine[-1770 5310 1770 5310 260]
ElementLine[1770 5310 1770 2550 260]
ElementLine[-1770 2550 1770 2550 260]
ElementLine[-1770 5310 -1770 2550 260]
ElementLine[-2360 -3540 -2360 2750 400]
ElementLine[-1770 3540 -1570 3540 400]
ElementLine[-1570 3540 2360 3540 400]
ElementLine[2360 3540 2360 -3540 400]
ElementLine[2360 -3540 -2360 -3540 400]
ElementLine[-1770 -3740 -1770 -4330 400]
ElementLine[-1770 -4330 1770 -4330 400]
ElementLine[1770 -4330 1770 -3740 400]
ElementLine[-2360 2750 -1570 3540 400]
ElementLine[-1770 3540 -1770 4330 400]
ElementLine[-1770 4330 1770 4330 400]
ElementLine[1770 4330 1770 3740 400]
Pad[0 -10230 0 -10230 15740 2000 16540 "" "A" "square"]
Pad[0 10230 0 10230 15740 2000 16540 "" "C" "square"]
)
