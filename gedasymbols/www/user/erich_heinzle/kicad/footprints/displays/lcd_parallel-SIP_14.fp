# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module lcd_parallel-SIP_14
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: lcd_parallel-SIP_14
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 14
#
Element["" "1" "" "" 0 0 -63400 3400 0 100 ""]
(
Pin[-60000 0 5200 2000 6000 3200 "" "1" ""]
Pin[-50000 0 5200 2000 6000 3200 "" "2" ""]
Pin[-40000 0 5200 2000 6000 3200 "" "3" ""]
Pin[-30000 0 5200 2000 6000 3200 "" "4" ""]
Pin[-20000 0 5200 2000 6000 3200 "" "5" ""]
Pin[-10000 0 5200 2000 6000 3200 "" "6" ""]
Pin[0 0 5200 2000 6000 3200 "" "7" ""]
Pin[10000 0 5200 2000 6000 3200 "" "8" ""]
Pin[20000 0 5200 2000 6000 3200 "" "9" ""]
Pin[30000 0 5200 2000 6000 3200 "" "10" ""]
Pin[40000 0 5200 2000 6000 3200 "" "11" ""]
Pin[50000 0 5200 2000 6000 3200 "" "12" ""]
Pin[60000 0 5200 2000 6000 3200 "" "13" ""]
Pin[70000 0 5200 2000 6000 3200 "" "14" ""]
)
