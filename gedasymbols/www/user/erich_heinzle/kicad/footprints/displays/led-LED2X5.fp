# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module led-LED2X5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: led-LED2X5
# Text descriptor count: 1
# Draw segment object count: 23
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 0 -12000 0 100 ""]
(
ElementLine[8500 5000 9500 5000 260]
ElementLine[9500 5000 9500 -5000 260]
ElementLine[8500 -5000 9500 -5000 260]
ElementLine[8500 5000 8500 -5000 260]
ElementLine[-10000 5000 10000 5000 600]
ElementLine[10000 -5000 10000 5000 600]
ElementLine[10000 -5000 -10000 -5000 600]
ElementLine[-10000 5000 -10000 -5000 600]
ElementLine[-7500 0 2000 0 600]
ElementLine[-2000 -1500 -2000 1500 600]
ElementLine[-2000 -1500 2000 0 600]
ElementLine[2000 0 7000 0 600]
ElementLine[2000 0 -2000 1500 600]
ElementLine[2000 -1500 2000 0 600]
ElementLine[2000 0 2000 1500 600]
ElementLine[3500 1000 4500 3000 600]
ElementLine[4500 3000 4500 2000 600]
ElementLine[4500 3000 3700 2400 600]
ElementLine[3700 2400 4500 2000 600]
ElementLine[5500 1000 6500 3000 600]
ElementLine[6500 3000 6500 2000 600]
ElementLine[6500 3000 5700 2400 600]
ElementLine[5700 2400 6500 2000 600]
Pin[-5000 0 5200 2000 6000 3200 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ""]
Pad[-5000 -2600 -5000 2600 5200 2000 6000 "" "A" ",onsolder"]
Pin[5000 0 5200 2000 6000 3200 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ""]
Pad[5000 -2600 5000 2600 5200 2000 6000 "" "K" ",onsolder"]
)
