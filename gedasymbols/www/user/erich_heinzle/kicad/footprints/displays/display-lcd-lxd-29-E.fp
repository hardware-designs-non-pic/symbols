# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module display-lcd-lxd-29-E
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: display-lcd-lxd-29-E
# Text descriptor count: 1
# Draw segment object count: 45
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 18
#
Element["" ">NAME" "" "" 0 0 3500 -40500 0 100 ""]
(
ElementLine[-40000 25000 -35000 25000 260]
ElementLine[-35000 25000 -35000 20000 260]
ElementLine[-40000 20000 -35000 20000 260]
ElementLine[-40000 25000 -40000 20000 260]
ElementLine[0 25000 5000 25000 260]
ElementLine[5000 25000 5000 20000 260]
ElementLine[0 20000 5000 20000 260]
ElementLine[0 25000 0 20000 260]
ElementLine[-55000 60000 55000 60000 500]
ElementLine[55000 -60000 -55000 -60000 500]
ElementLine[55000 60000 55000 45000 500]
ElementLine[-55000 45000 -55000 60000 500]
ElementLine[55000 45000 -55000 45000 500]
ElementLine[-55000 -60000 -55000 -45000 500]
ElementLine[55000 45000 55000 -45000 500]
ElementLine[-55000 -45000 55000 -45000 500]
ElementLine[-55000 -45000 -55000 -20000 500]
ElementLine[-55000 -20000 -55000 20000 500]
ElementLine[-55000 20000 -55000 45000 500]
ElementLine[55000 -45000 55000 -60000 500]
ElementLine[-42500 -35000 -45000 -32500 500]
ElementLine[-45000 -32500 -45000 32500 500]
ElementLine[-45000 32500 -42500 35000 500]
ElementLine[-42500 35000 42500 35000 500]
ElementLine[42500 35000 45000 32500 500]
ElementLine[45000 32500 45000 -32500 500]
ElementLine[45000 -32500 42500 -35000 500]
ElementLine[42500 -35000 -42500 -35000 500]
ElementLine[17500 -20000 15000 -3000 5000]
ElementLine[15000 3000 12500 20000 5000]
ElementLine[18000 22500 27000 22500 5000]
ElementLine[32500 20000 35000 3000 5000]
ElementLine[35000 -3000 37500 -20000 5000]
ElementLine[23000 -22500 32000 -22500 5000]
ElementLine[20500 0 29500 0 5000]
ElementLine[-22500 -20000 -25000 -3000 5000]
ElementLine[-25000 3000 -27500 20000 5000]
ElementLine[-22000 22500 -13000 22500 5000]
ElementLine[-7500 20000 -5000 3000 5000]
ElementLine[-5000 -3000 -2500 -20000 5000]
ElementLine[-17000 -22500 -8000 -22500 5000]
ElementLine[-19500 0 -10500 0 5000]
ElementLine[-55000 -20000 -59000 -20000 500]
ElementLine[-59000 -20000 -59000 20000 500]
ElementLine[-59000 20000 -55000 20000 500]
Pad[-40000 47500 -40000 57500 5000 2000 5800 "" "1" "square"]
Pad[-30000 47500 -30000 57500 5000 2000 5800 "" "2" "square"]
Pad[-20000 47500 -20000 57500 5000 2000 5800 "" "3" "square"]
Pad[-10000 47500 -10000 57500 5000 2000 5800 "" "4" "square"]
Pad[0 47500 0 57500 5000 2000 5800 "" "5" "square"]
Pad[10000 47500 10000 57500 5000 2000 5800 "" "6" "square"]
Pad[20000 47500 20000 57500 5000 2000 5800 "" "7" "square"]
Pad[30000 47500 30000 57500 5000 2000 5800 "" "8" "square"]
Pad[40000 47500 40000 57500 5000 2000 5800 "" "9" "square"]
Pad[40000 -57500 40000 -47500 5000 2000 5800 "" "10" "square"]
Pad[30000 -57500 30000 -47500 5000 2000 5800 "" "11" "square"]
Pad[20000 -57500 20000 -47500 5000 2000 5800 "" "12" "square"]
Pad[10000 -57500 10000 -47500 5000 2000 5800 "" "13" "square"]
Pad[0 -57500 0 -47500 5000 2000 5800 "" "14" "square"]
Pad[-10000 -57500 -10000 -47500 5000 2000 5800 "" "15" "square"]
Pad[-20000 -57500 -20000 -47500 5000 2000 5800 "" "16" "square"]
Pad[-30000 -57500 -30000 -47500 5000 2000 5800 "" "17" "square"]
Pad[-40000 -57500 -40000 -47500 5000 2000 5800 "" "18" "square"]
)
