# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-GLIMM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-GLIMM
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 0
# Draw arc object count: 2
# Pad count: 2
#
Element["" ">name" "" "" 0 0 5000 -18000 0 100 ""]
(
ElementLine[-5000 -10000 15000 -10000 1000]
ElementLine[-5000 10000 15000 10000 1000]
ElementArc[-5000 0 10000 10000 0 360 1000]
ElementArc[15000 0 10000 10000 0 360 1000]
Pin[-5000 0 7000 2000 7800 3140 "" "1" "blah"]
Pad[-5000 -3500 -5000 3500 7000 2000 7800 "" "1" "square"]
Pad[-5000 -3500 -5000 3500 7000 2000 7800 "" "1" "square,onsolder"]
Pin[15000 0 7000 2000 7800 3140 "" "2" ""]
Pad[15000 -3500 15000 3500 7000 2000 7800 "" "2" ""]
Pad[15000 -3500 15000 3500 7000 2000 7800 "" "2" ",onsolder"]
)
