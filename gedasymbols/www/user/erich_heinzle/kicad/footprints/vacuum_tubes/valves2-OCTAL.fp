# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-OCTAL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-OCTAL
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">value" "" "" 0 0 43000 67000 0 100 ""]
(
ElementArc[0 0 11172 11172 0 360 400]
ElementArc[7500 12500 2475 2475 0 360 400]
ElementArc[0 0 44194 44194 0 360 500]
Pin[30000 30000 15000 2000 15800 7080 "" "1" "blah"]
Pad[30000 22500 30000 37500 15000 2000 15800 "" "1" "square"]
Pad[30000 22500 30000 37500 15000 2000 15800 "" "1" "square,onsolder"]
Pin[40000 0 15000 2000 30800 7080 "" "2" ""]
Pad[32500 0 47500 0 15000 2000 15800 "" "2" ""]
Pad[32500 0 47500 0 15000 2000 15800 "" "2" ",onsolder"]
Pin[30000 -30000 15000 2000 15800 7080 "" "3" ""]
Pad[30000 -37500 30000 -22500 15000 2000 15800 "" "3" ""]
Pad[30000 -37500 30000 -22500 15000 2000 15800 "" "3" ",onsolder"]
Pin[0 -40000 15000 2000 15800 7080 "" "4" ""]
Pad[0 -47500 0 -32500 15000 2000 15800 "" "4" ""]
Pad[0 -47500 0 -32500 15000 2000 15800 "" "4" ",onsolder"]
Pin[-30000 -30000 15000 2000 15800 7080 "" "5" ""]
Pad[-30000 -37500 -30000 -22500 15000 2000 15800 "" "5" ""]
Pad[-30000 -37500 -30000 -22500 15000 2000 15800 "" "5" ",onsolder"]
Pin[-40000 0 15000 2000 30800 7080 "" "6" ""]
Pad[-47500 0 -32500 0 15000 2000 15800 "" "6" ""]
Pad[-47500 0 -32500 0 15000 2000 15800 "" "6" ",onsolder"]
Pin[-30000 30000 15000 2000 15800 7080 "" "7" ""]
Pad[-30000 22500 -30000 37500 15000 2000 15800 "" "7" ""]
Pad[-30000 22500 -30000 37500 15000 2000 15800 "" "7" ",onsolder"]
Pin[0 40000 15000 2000 15800 7080 "" "8" ""]
Pad[0 32500 0 47500 15000 2000 15800 "" "8" ""]
Pad[0 32500 0 47500 15000 2000 15800 "" "8" ",onsolder"]
)
