# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-NOVAL1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-NOVAL1
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 22
# Draw arc object count: 0
# Pad count: 10
#
Element["" "1" "" "" 0 0 10000 23500 0 100 ""]
(
ElementLine[0 0 -390 0 0]
ElementArc[0 0 30816 30816 0 360 800]
ElementArc[0 0 6958 6958 0 360 800]
ElementArc[-20190 -15000 4172 4172 0 360 500]
ElementArc[-24900 -480 4172 4172 0 360 500]
ElementArc[-21090 14510 4172 4172 0 360 500]
ElementArc[8250 23720 4172 4172 0 360 500]
ElementArc[20390 15000 4172 4172 0 360 500]
ElementArc[25290 1000 4172 4172 0 360 500]
ElementArc[21090 -14000 4172 4172 0 360 500]
ElementArc[8720 -23130 4172 4172 0 360 500]
ElementArc[-7350 -24220 4172 4172 0 360 500]
ElementArc[-24900 -480 3069 3069 0 360 250]
ElementArc[-20190 -15000 3069 3069 0 360 250]
ElementArc[-7350 -24220 3069 3069 0 360 250]
ElementArc[8720 -23130 3069 3069 0 360 250]
ElementArc[21090 -14000 3069 3069 0 360 250]
ElementArc[25290 1000 3069 3069 0 360 250]
ElementArc[20390 15000 3069 3069 0 360 250]
ElementArc[8250 23720 3069 3069 0 360 250]
ElementArc[-21090 14510 3069 3069 0 360 250]
ElementArc[0 0 29232 29232 0 360 250]
ElementArc[0 0 5728 5728 0 360 250]
Pin[10820 34840 10000 2000 10800 4000 "" "1" ""]
Pin[29520 21650 10000 2000 10800 4000 "" "2" ""]
Pin[36410 0 10000 2000 10800 4000 "" "3" ""]
Pin[29520 -21650 10000 2000 10800 4000 "" "4" ""]
Pin[12790 -34050 10000 2000 10800 4000 "" "5" ""]
Pin[-10820 -34840 10000 2000 10800 4000 "" "6" ""]
Pin[-29520 -21650 10000 2000 10800 4000 "" "7" ""]
Pin[-36410 -980 10000 2000 10800 4000 "" "8" ""]
Pin[-30110 20660 10000 2000 10800 4000 "" "9" ""]
Pin[10820 6880 10000 2000 10800 4000 "" "M" ""]
Pad[10820 1880 10820 11880 10000 2000 10800 "" "M" ""]
Pad[10820 1880 10820 11880 10000 2000 10800 "" "M" ",onsolder"]
)
