# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-MSTBA8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-MSTBA8
# Text descriptor count: 1
# Draw segment object count: 63
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 -66500 -33500 0 100 ""]
(
ElementLine[-84000 -18000 -84000 17500 500]
ElementLine[-84000 -18000 -72500 -18000 500]
ElementLine[-72500 -18000 -67500 -18000 500]
ElementLine[-67500 -18000 -52500 -18000 500]
ElementLine[-52500 -18000 -47500 -18000 500]
ElementLine[72500 -18000 84000 -18000 500]
ElementLine[-84000 17500 84000 17500 500]
ElementLine[84000 -18000 84000 17500 500]
ElementLine[-84000 29500 -73500 29500 500]
ElementLine[-84000 17500 -84000 29500 500]
ElementLine[84000 17500 84000 29500 500]
ElementLine[-66500 29500 -68000 24500 500]
ElementLine[-66500 29500 -53500 29500 500]
ElementLine[-68000 24500 -72000 24500 500]
ElementLine[-73500 29500 -72000 24500 500]
ElementLine[-73500 29500 -66500 29500 500]
ElementLine[-46500 29500 -48000 24500 500]
ElementLine[-46500 29500 -33500 29500 500]
ElementLine[-33500 29500 -26500 29500 500]
ElementLine[-26500 29500 -13500 29500 500]
ElementLine[-13500 29500 -6500 29500 500]
ElementLine[-6500 29500 6500 29500 500]
ElementLine[6500 29500 13500 29500 500]
ElementLine[13500 29500 26500 29500 500]
ElementLine[26500 29500 33500 29500 500]
ElementLine[33500 29500 46500 29500 500]
ElementLine[46500 29500 53500 29500 500]
ElementLine[53500 29500 66500 29500 500]
ElementLine[66500 29500 73500 29500 500]
ElementLine[73500 29500 84000 29500 500]
ElementLine[-53500 29500 -52000 24500 500]
ElementLine[-53500 29500 -46500 29500 500]
ElementLine[-52000 24500 -48000 24500 500]
ElementLine[-32000 24500 -28000 24500 500]
ElementLine[-26500 29500 -28000 24500 500]
ElementLine[-33500 29500 -32000 24500 500]
ElementLine[-32500 -18000 -27500 -18000 500]
ElementLine[-47500 -18000 -32500 -18000 500]
ElementLine[-12500 -18000 -7500 -18000 500]
ElementLine[-27500 -18000 -12500 -18000 500]
ElementLine[-12000 24500 -8000 24500 500]
ElementLine[-6500 29500 -8000 24500 500]
ElementLine[-13500 29500 -12000 24500 500]
ElementLine[7500 -18000 12500 -18000 500]
ElementLine[-7500 -18000 7500 -18000 500]
ElementLine[8000 24500 12000 24500 500]
ElementLine[13500 29500 12000 24500 500]
ElementLine[6500 29500 8000 24500 500]
ElementLine[28000 24500 32000 24500 500]
ElementLine[26500 29500 28000 24500 500]
ElementLine[33500 29500 32000 24500 500]
ElementLine[27500 -18000 32500 -18000 500]
ElementLine[12500 -18000 27500 -18000 500]
ElementLine[48000 24500 52000 24500 500]
ElementLine[53500 29500 52000 24500 500]
ElementLine[46500 29500 48000 24500 500]
ElementLine[32500 -18000 47500 -18000 500]
ElementLine[47500 -18000 52500 -18000 500]
ElementLine[68000 24500 72000 24500 500]
ElementLine[73500 29500 72000 24500 500]
ElementLine[66500 29500 68000 24500 500]
ElementLine[52500 -18000 67500 -18000 500]
ElementLine[67500 -18000 72500 -18000 500]
Pin[-70000 -10000 7500 2000 8300 5500 "" "1" ""]
Pad[-70000 -13750 -70000 -6250 7500 2000 8300 "" "1" ""]
Pad[-70000 -13750 -70000 -6250 7500 2000 8300 "" "1" ",onsolder"]
Pin[-50000 -10000 7500 2000 8300 5500 "" "2" ""]
Pad[-50000 -13750 -50000 -6250 7500 2000 8300 "" "2" ""]
Pad[-50000 -13750 -50000 -6250 7500 2000 8300 "" "2" ",onsolder"]
Pin[-30000 -10000 7500 2000 8300 5500 "" "3" ""]
Pad[-30000 -13750 -30000 -6250 7500 2000 8300 "" "3" ""]
Pad[-30000 -13750 -30000 -6250 7500 2000 8300 "" "3" ",onsolder"]
Pin[-10000 -10000 7500 2000 8300 5500 "" "4" ""]
Pad[-10000 -13750 -10000 -6250 7500 2000 8300 "" "4" ""]
Pad[-10000 -13750 -10000 -6250 7500 2000 8300 "" "4" ",onsolder"]
Pin[10000 -10000 7500 2000 8300 5500 "" "5" ""]
Pad[10000 -13750 10000 -6250 7500 2000 8300 "" "5" ""]
Pad[10000 -13750 10000 -6250 7500 2000 8300 "" "5" ",onsolder"]
Pin[30000 -10000 7500 2000 8300 5500 "" "6" ""]
Pad[30000 -13750 30000 -6250 7500 2000 8300 "" "6" ""]
Pad[30000 -13750 30000 -6250 7500 2000 8300 "" "6" ",onsolder"]
Pin[50000 -10000 7500 2000 8300 5500 "" "7" ""]
Pad[50000 -13750 50000 -6250 7500 2000 8300 "" "7" ""]
Pad[50000 -13750 50000 -6250 7500 2000 8300 "" "7" ",onsolder"]
Pin[70000 -10000 7500 2000 8300 5500 "" "8" ""]
Pad[70000 -13750 70000 -6250 7500 2000 8300 "" "8" ""]
Pad[70000 -13750 70000 -6250 7500 2000 8300 "" "8" ",onsolder"]
)
