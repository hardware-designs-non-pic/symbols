# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-MSTBA7
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-MSTBA7
# Text descriptor count: 1
# Draw segment object count: 56
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 7
#
Element["" ">NAME" "" "" 0 0 -56490 -35500 0 100 ""]
(
ElementLine[-74000 -18000 -74000 17500 500]
ElementLine[-74000 -18000 -62500 -18000 500]
ElementLine[-62500 -18000 -57500 -18000 500]
ElementLine[-57500 -18000 -42500 -18000 500]
ElementLine[-42500 -18000 -37500 -18000 500]
ElementLine[62500 -18000 74000 -18000 500]
ElementLine[-74000 17500 74000 17500 500]
ElementLine[74000 -18000 74000 17500 500]
ElementLine[-74000 29500 -63500 29500 500]
ElementLine[-74000 17500 -74000 29500 500]
ElementLine[74000 17500 74000 29500 500]
ElementLine[-56490 29500 -58000 24500 500]
ElementLine[-56490 29500 -43500 29500 500]
ElementLine[-58000 24500 -62000 24500 500]
ElementLine[-63500 29500 -62000 24500 500]
ElementLine[-63500 29500 -56490 29500 500]
ElementLine[-36500 29500 -38000 24500 500]
ElementLine[-36500 29500 -23500 29500 500]
ElementLine[-23500 29500 -16500 29500 500]
ElementLine[-16500 29500 -3500 29500 500]
ElementLine[-3500 29500 3500 29500 500]
ElementLine[3500 29500 16500 29500 500]
ElementLine[16500 29500 23500 29500 500]
ElementLine[23500 29500 36500 29500 500]
ElementLine[36500 29500 43500 29500 500]
ElementLine[43500 29500 56490 29500 500]
ElementLine[56490 29500 63500 29500 500]
ElementLine[63500 29500 74000 29500 500]
ElementLine[-43500 29500 -42000 24500 500]
ElementLine[-43500 29500 -36500 29500 500]
ElementLine[-42000 24500 -38000 24500 500]
ElementLine[-22000 24500 -18000 24500 500]
ElementLine[-16500 29500 -18000 24500 500]
ElementLine[-23500 29500 -22000 24500 500]
ElementLine[-22500 -18000 -17500 -18000 500]
ElementLine[-37500 -18000 -22500 -18000 500]
ElementLine[-2500 -18000 2500 -18000 500]
ElementLine[-17500 -18000 -2500 -18000 500]
ElementLine[-2000 24500 2000 24500 500]
ElementLine[3500 29500 2000 24500 500]
ElementLine[-3500 29500 -2000 24500 500]
ElementLine[17500 -18000 22500 -18000 500]
ElementLine[2500 -18000 17500 -18000 500]
ElementLine[18000 24500 22000 24500 500]
ElementLine[23500 29500 22000 24500 500]
ElementLine[16500 29500 18000 24500 500]
ElementLine[38000 24500 42000 24500 500]
ElementLine[36500 29500 38000 24500 500]
ElementLine[43500 29500 42000 24500 500]
ElementLine[37500 -18000 42500 -18000 500]
ElementLine[22500 -18000 37500 -18000 500]
ElementLine[58000 24500 62000 24500 500]
ElementLine[63500 29500 62000 24500 500]
ElementLine[56490 29500 58000 24500 500]
ElementLine[42500 -18000 57500 -18000 500]
ElementLine[57500 -18000 62500 -18000 500]
Pin[-60000 -10000 7500 2000 8300 5500 "" "1" ""]
Pad[-60000 -13750 -60000 -6250 7500 2000 8300 "" "1" ""]
Pad[-60000 -13750 -60000 -6250 7500 2000 8300 "" "1" ",onsolder"]
Pin[-40000 -10000 7500 2000 8300 5500 "" "2" ""]
Pad[-40000 -13750 -40000 -6250 7500 2000 8300 "" "2" ""]
Pad[-40000 -13750 -40000 -6250 7500 2000 8300 "" "2" ",onsolder"]
Pin[-20000 -10000 7500 2000 8300 5500 "" "3" ""]
Pad[-20000 -13750 -20000 -6250 7500 2000 8300 "" "3" ""]
Pad[-20000 -13750 -20000 -6250 7500 2000 8300 "" "3" ",onsolder"]
Pin[0 -10000 7500 2000 8300 5500 "" "4" ""]
Pad[0 -13750 0 -6250 7500 2000 8300 "" "4" ""]
Pad[0 -13750 0 -6250 7500 2000 8300 "" "4" ",onsolder"]
Pin[20000 -10000 7500 2000 8300 5500 "" "5" ""]
Pad[20000 -13750 20000 -6250 7500 2000 8300 "" "5" ""]
Pad[20000 -13750 20000 -6250 7500 2000 8300 "" "5" ",onsolder"]
Pin[40000 -10000 7500 2000 8300 5500 "" "6" ""]
Pad[40000 -13750 40000 -6250 7500 2000 8300 "" "6" ""]
Pad[40000 -13750 40000 -6250 7500 2000 8300 "" "6" ",onsolder"]
Pin[60000 -10000 7500 2000 8300 5500 "" "7" ""]
Pad[60000 -13750 60000 -6250 7500 2000 8300 "" "7" ""]
Pad[60000 -13750 60000 -6250 7500 2000 8300 "" "7" ",onsolder"]
)
