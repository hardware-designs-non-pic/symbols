# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-EURO
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-EURO
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 3
# Draw arc object count: 8
# Pad count: 5
#
Element["" "1" "" "" 0 0 11500 -34000 0 100 ""]
(
ElementLine[-46700 -67400 5400 -62100 1000]
ElementLine[67500 47800 61900 -7300 1000]
ElementLine[47400 67400 -2500 62400 1000]
ElementLine[-62100 6800 -66900 -47300 1000]
ElementLine[-46700 -67400 5400 -62100 1000]
ElementLine[67500 47800 61900 -7300 1000]
ElementLine[47400 67400 -2500 62400 1000]
ElementLine[-62100 6800 -66900 -47300 1000]
ElementLine[42700 49000 55300 49000 500]
ElementLine[49000 55300 49000 42700 500]
ElementLine[-54800 -49000 -42200 -49000 500]
ElementLine[-48500 -42700 -48500 -55300 500]
ElementArc[0 0 37052 37052 0 360 500]
ElementArc[49000 49000 4455 4455 0 360 500]
ElementArc[-48500 -49000 4455 4455 0 360 500]
ElementArc[-48500 -49000 18519 18519 0 360 1000]
ElementArc[49100 49000 18453 18453 0 360 1000]
ElementArc[100 -100 62226 62226 0 360 1000]
ElementArc[-200 0 62317 62317 0 360 1000]
ElementArc[-48500 -49000 18519 18519 0 360 1000]
ElementArc[49100 49000 18453 18453 0 360 1000]
ElementArc[100 -100 62226 62226 0 360 1000]
ElementArc[-200 0 62317 62317 0 360 1000]
Pin[-5900 -33400 20500 2000 21300 16530 "" "1" ""]
Pin[31500 0 20500 2000 21300 16530 "" "2" ""]
Pin[-5900 33400 20500 2000 21300 16530 "" "3" ""]
Pin[-31500 0 20500 2000 21300 16530 "" "4" ""]
Pin[0 0 20500 2000 21300 16530 "" "5" ""]
)
