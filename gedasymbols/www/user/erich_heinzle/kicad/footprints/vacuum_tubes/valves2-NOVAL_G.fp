# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-NOVAL_G
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-NOVAL_G
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 3
# Draw arc object count: 4
# Pad count: 9
#
Element["" "1" "" "" 0 0 26600 -31300 0 100 ""]
(
ElementLine[57500 -7500 57500 7500 0]
ElementLine[50000 0 65000 0 0]
ElementLine[-57500 -7500 -57500 7500 0]
ElementLine[-65000 0 -50000 0 0]
ElementLine[64200 -7400 27100 -35600 800]
ElementLine[-63500 -7900 -28000 -35000 800]
ElementLine[-63500 8000 -26800 36000 800]
ElementLine[64100 7500 27500 35600 800]
ElementArc[0 0 24749 24749 0 360 600]
ElementArc[57500 0 3536 3536 0 360 400]
ElementArc[-57500 0 3536 3536 0 360 400]
ElementArc[57500 0 9982 9982 0 360 800]
ElementArc[-57600 100 9910 9910 0 360 800]
ElementArc[-100 200 44996 44996 0 360 800]
ElementArc[0 200 44895 44895 0 360 800]
Pin[30000 -40000 7000 2000 7800 3930 "" "1" "blah"]
Pad[30000 -43500 30000 -36500 7000 2000 7800 "" "1" "square"]
Pad[30000 -43500 30000 -36500 7000 2000 7800 "" "1" "square,onsolder"]
Pin[0 -50000 7000 2000 7800 3930 "" "2" ""]
Pad[0 -53500 0 -46500 7000 2000 7800 "" "2" ""]
Pad[0 -53500 0 -46500 7000 2000 7800 "" "2" ",onsolder"]
Pin[-30000 -40000 7000 2000 7800 3930 "" "3" ""]
Pad[-30000 -43500 -30000 -36500 7000 2000 7800 "" "3" ""]
Pad[-30000 -43500 -30000 -36500 7000 2000 7800 "" "3" ",onsolder"]
Pin[-50000 -25000 7000 2000 7800 3930 "" "4" ""]
Pad[-50000 -28500 -50000 -21500 7000 2000 7800 "" "4" ""]
Pad[-50000 -28500 -50000 -21500 7000 2000 7800 "" "4" ",onsolder"]
Pin[-50000 25000 7000 2000 7800 3930 "" "5" ""]
Pad[-50000 21500 -50000 28500 7000 2000 7800 "" "5" ""]
Pad[-50000 21500 -50000 28500 7000 2000 7800 "" "5" ",onsolder"]
Pin[-30000 40000 7000 2000 7800 3930 "" "6" ""]
Pad[-30000 36500 -30000 43500 7000 2000 7800 "" "6" ""]
Pad[-30000 36500 -30000 43500 7000 2000 7800 "" "6" ",onsolder"]
Pin[0 50000 7000 2000 7800 3930 "" "7" ""]
Pad[0 46500 0 53500 7000 2000 7800 "" "7" ""]
Pad[0 46500 0 53500 7000 2000 7800 "" "7" ",onsolder"]
Pin[30000 40000 7000 2000 7800 3930 "" "8" ""]
Pad[30000 36500 30000 43500 7000 2000 7800 "" "8" ""]
Pad[30000 36500 30000 43500 7000 2000 7800 "" "8" ",onsolder"]
Pin[50000 25000 7000 2000 7800 3930 "" "9" ""]
Pad[50000 21500 50000 28500 7000 2000 7800 "" "9" ""]
Pad[50000 21500 50000 28500 7000 2000 7800 "" "9" ",onsolder"]
)
