# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-MINI_G
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-MINI_G
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 6
# Draw arc object count: 4
# Pad count: 7
#
Element["" "1" "" "" 0 0 18700 -30300 0 100 ""]
(
ElementLine[45000 -7500 45000 7500 0]
ElementLine[37500 0 52500 0 0]
ElementLine[-52500 0 -37500 0 0]
ElementLine[-45000 -7500 -45000 7500 0]
ElementLine[27500 -28700 51100 -5000 800]
ElementLine[-50900 -5200 -27500 -28900 800]
ElementLine[50800 5400 27500 29100 800]
ElementLine[-50900 5300 -28800 27700 800]
ElementLine[27500 -28700 51100 -5000 800]
ElementLine[-50900 -5200 -27500 -28900 800]
ElementLine[27500 -28700 51100 -5000 800]
ElementArc[-45000 0 3536 3536 0 360 400]
ElementArc[45000 0 3536 3536 0 360 400]
ElementArc[-100 0 21213 21213 0 360 600]
ElementArc[100 0 21213 21213 0 360 400]
ElementArc[100 0 21213 21213 0 360 400]
ElementArc[100 0 21213 21213 0 360 400]
ElementArc[100 0 39956 39956 0 360 800]
ElementArc[-100 200 39970 39970 0 360 800]
ElementArc[45000 0 7887 7887 0 360 800]
ElementArc[-45000 0 7856 7856 0 360 800]
Pin[20000 -40000 7000 2000 7800 3930 "" "1" "blah"]
Pad[20000 -43500 20000 -36500 7000 2000 7800 "" "1" "square"]
Pad[20000 -43500 20000 -36500 7000 2000 7800 "" "1" "square,onsolder"]
Pin[-20000 -40000 7000 2000 7800 3930 "" "2" ""]
Pad[-20000 -43500 -20000 -36500 7000 2000 7800 "" "2" ""]
Pad[-20000 -43500 -20000 -36500 7000 2000 7800 "" "2" ",onsolder"]
Pin[-45000 -20000 7000 2000 7800 3930 "" "3" ""]
Pad[-45000 -23500 -45000 -16500 7000 2000 7800 "" "3" ""]
Pad[-45000 -23500 -45000 -16500 7000 2000 7800 "" "3" ",onsolder"]
Pin[-45000 20000 7000 2000 7800 3930 "" "4" ""]
Pad[-45000 16500 -45000 23500 7000 2000 7800 "" "4" ""]
Pad[-45000 16500 -45000 23500 7000 2000 7800 "" "4" ",onsolder"]
Pin[-20000 40000 7000 2000 7800 3930 "" "5" ""]
Pad[-20000 36500 -20000 43500 7000 2000 7800 "" "5" ""]
Pad[-20000 36500 -20000 43500 7000 2000 7800 "" "5" ",onsolder"]
Pin[20000 40000 7000 2000 7800 3930 "" "6" ""]
Pad[20000 36500 20000 43500 7000 2000 7800 "" "6" ""]
Pad[20000 36500 20000 43500 7000 2000 7800 "" "6" ",onsolder"]
Pin[45000 20000 7000 2000 7800 3930 "" "7" ""]
Pad[45000 16500 45000 23500 7000 2000 7800 "" "7" ""]
Pad[45000 16500 45000 23500 7000 2000 7800 "" "7" ",onsolder"]
)
