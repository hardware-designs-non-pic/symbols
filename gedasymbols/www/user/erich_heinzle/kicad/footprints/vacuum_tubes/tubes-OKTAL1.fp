# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-OKTAL1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-OKTAL1
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 18
# Draw arc object count: 2
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 -2500 68990 0 100 ""]
(
ElementLine[-19680 -7870 -41330 -17710 2400]
ElementLine[-7870 -19680 -17710 -41330 2400]
ElementLine[7870 -19680 15740 -39370 2400]
ElementLine[19680 -7870 41330 -15740 2400]
ElementLine[19680 7870 41330 17710 2400]
ElementLine[7870 19680 17710 41330 2400]
ElementLine[-7870 19680 -17710 41330 2400]
ElementLine[-19680 7870 -41330 15740 2400]
ElementArc[0 0 41847 41847 0 360 800]
ElementArc[0 0 39061 39061 0 360 500]
ElementArc[-35430 0 5558 5558 0 360 500]
ElementArc[35430 0 5558 5558 0 360 500]
ElementArc[0 35430 5558 5558 0 360 500]
ElementArc[0 -35430 5558 5558 0 360 500]
ElementArc[-23620 23620 5558 5558 0 360 500]
ElementArc[-23620 -23620 5558 5558 0 360 500]
ElementArc[23620 -23620 5558 5558 0 360 500]
ElementArc[23620 23620 5558 5558 0 360 500]
ElementArc[-23620 -23620 4398 4398 0 360 250]
ElementArc[0 -35430 4398 4398 0 360 250]
ElementArc[23620 -23620 4398 4398 0 360 250]
ElementArc[35430 0 4398 4398 0 360 250]
ElementArc[23620 23620 4398 4398 0 360 250]
ElementArc[0 35430 4398 4398 0 360 250]
ElementArc[-23620 23620 4398 4398 0 360 250]
ElementArc[-35430 0 4398 4398 0 360 250]
ElementArc[-590 0 15751 15751 0 360 1000]
ElementArc[4920 13770 5296 5296 0 360 1000]
Pin[35430 35430 10000 2000 10800 5000 "" "1" ""]
Pin[51180 0 10000 2000 10800 5000 "" "2" ""]
Pin[35430 -35430 10000 2000 10800 5000 "" "3" ""]
Pin[0 -51180 10000 2000 10800 5000 "" "4" ""]
Pin[-35430 -35430 10000 2000 10800 5000 "" "5" ""]
Pin[-51180 0 10000 2000 10800 5000 "" "6" ""]
Pin[-35430 35430 10000 2000 10800 5000 "" "7" ""]
Pin[0 51180 10000 2000 10800 5000 "" "8" ""]
)
