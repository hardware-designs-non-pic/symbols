# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Karsten Malcher 
# No warranties express or implied
# Footprint converted from Kicad Module socket_gzc9-b
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: socket_gzc9-b
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 11
# Draw arc object count: 8
# Pad count: 10
#
Element["" "T***" "" "" 0 0 0 -50000 0 100 ""]
(
ElementLine[-21650 39760 -21360 38580 1490]
ElementLine[-5910 44880 -5510 44090 1490]
ElementLine[21750 -39670 21460 -38980 1490]
ElementLine[5810 -44880 5510 -44090 1490]
ElementArc[-22440 -7290 5900 5900 0 360 1490]
ElementArc[-13870 -19190 5900 5900 0 360 1490]
ElementArc[-10 -23520 5900 5900 0 360 1490]
ElementArc[13890 -19090 5900 5900 0 360 1490]
ElementArc[22440 -7280 5900 5900 0 360 1490]
ElementArc[22450 7280 5900 5900 0 360 1490]
ElementArc[13880 19100 5900 5900 0 360 1490]
ElementArc[-13870 19100 5900 5900 0 360 1490]
ElementArc[-22440 7280 5900 5900 0 360 1490]
ElementArc[0 0 7090 7090 0 360 1490]
ElementArc[0 0 35420 35420 0 360 1490]
ElementArc[0 0 45267 45267 180 -90 1490]
ElementArc[0 0 45289 45289 225 -90 1490]
ElementArc[0 0 45255 45255 0 -90 1490]
ElementArc[0 0 45272 45272 45 -90 1490]
ElementArc[-13480 41540 8412 8412 0 -90 1490]
ElementArc[-13480 41540 8378 8378 252 -90 1490]
ElementArc[13480 -41540 8381 8381 180 -90 1490]
ElementArc[13480 -41540 8351 8351 72 -90 1490]
Pin[-23140 31850 9840 2000 10640 5900 "" "9" ""]
Pin[-37440 12150 9840 2000 10640 5900 "" "8" ""]
Pin[-37440 -12150 9840 2000 10640 5900 "" "7" ""]
Pin[-23140 -31850 9840 2000 10640 5900 "" "6" ""]
Pin[0 -39370 9840 2000 10640 5900 "" "5" ""]
Pin[23140 -31850 9840 2000 10640 5900 "" "4" ""]
Pin[37440 -12150 9840 2000 10640 5900 "" "3" ""]
Pin[37440 12150 9840 2000 10640 5900 "" "2" ""]
Pin[23140 31850 9840 2000 10640 5900 "" "1" ""]
Pin[0 0 13770 2000 14570 13770 "" "" "hole"]
)
