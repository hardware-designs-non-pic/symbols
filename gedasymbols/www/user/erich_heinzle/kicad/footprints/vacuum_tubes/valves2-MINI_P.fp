# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module valves2-MINI_P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: valves2-MINI_P
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 7
#
Element["" ">NAME" "" "" 0 0 38400 -30900 0 100 ""]
(
ElementArc[0 0 24749 24749 0 360 600]
Pin[25000 10000 10000 2000 20800 3930 "" "1" "blah"]
Pad[20000 10000 30000 10000 10000 2000 10800 "" "1" "square"]
Pad[20000 10000 30000 10000 10000 2000 10800 "" "1" "square,onsolder"]
Pin[25000 -10000 10000 2000 20800 3930 "" "2" ""]
Pad[20000 -10000 30000 -10000 10000 2000 10800 "" "2" ""]
Pad[20000 -10000 30000 -10000 10000 2000 10800 "" "2" ",onsolder"]
Pin[10000 -25000 10000 2000 10800 3930 "" "3" ""]
Pad[10000 -30000 10000 -20000 10000 2000 10800 "" "3" ""]
Pad[10000 -30000 10000 -20000 10000 2000 10800 "" "3" ",onsolder"]
Pin[-10000 -25000 10000 2000 10800 3930 "" "4" ""]
Pad[-10000 -30000 -10000 -20000 10000 2000 10800 "" "4" ""]
Pad[-10000 -30000 -10000 -20000 10000 2000 10800 "" "4" ",onsolder"]
Pin[-25000 -10000 10000 2000 20800 3930 "" "5" ""]
Pad[-30000 -10000 -20000 -10000 10000 2000 10800 "" "5" ""]
Pad[-30000 -10000 -20000 -10000 10000 2000 10800 "" "5" ",onsolder"]
Pin[-25000 10000 10000 2000 20800 3930 "" "6" ""]
Pad[-30000 10000 -20000 10000 10000 2000 10800 "" "6" ""]
Pad[-30000 10000 -20000 10000 10000 2000 10800 "" "6" ",onsolder"]
Pin[-10000 25000 10000 2000 10800 3930 "" "7" ""]
Pad[-10000 20000 -10000 30000 10000 2000 10800 "" "7" ""]
Pad[-10000 20000 -10000 30000 10000 2000 10800 "" "7" ",onsolder"]
)
