# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-NOVAL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-NOVAL
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 22
# Draw arc object count: 0
# Pad count: 9
#
Element["" ">NAME" "" "" 0 0 1500 54500 0 100 ""]
(
ElementArc[0 0 8344 8344 0 360 500]
ElementArc[-25590 7870 4172 4172 0 360 600]
ElementArc[-15740 21650 4172 4172 0 360 600]
ElementArc[15740 21650 4172 4172 0 360 600]
ElementArc[25590 7870 4398 4398 0 360 600]
ElementArc[-25590 -8850 4228 4228 0 360 600]
ElementArc[-15740 -21650 4045 4045 0 360 600]
ElementArc[0 -26570 4172 4172 0 360 600]
ElementArc[15740 -21650 4455 4455 0 360 600]
ElementArc[25590 -8850 4398 4398 0 360 600]
ElementArc[-15800 -21600 2701 2701 0 360 250]
ElementArc[0 -26600 2701 2701 0 360 250]
ElementArc[15900 -21600 2701 2701 0 360 250]
ElementArc[25800 -8800 2701 2701 0 360 250]
ElementArc[25800 7900 2701 2701 0 360 250]
ElementArc[15800 21800 2701 2701 0 360 250]
ElementArc[-15800 21800 2701 2701 0 360 250]
ElementArc[-25700 7900 2701 2701 0 360 250]
ElementArc[-25600 -8900 2701 2701 0 360 250]
ElementArc[0 0 31820 31820 0 360 1200]
ElementArc[0 0 7071 7071 0 360 800]
ElementArc[0 0 26573 26573 0 360 800]
Pin[20500 27000 10000 2000 10800 5600 "" "1" ""]
Pin[32490 9770 10000 2000 10800 5600 "" "2" ""]
Pin[31510 -11380 10000 2000 10800 5600 "" "3" ""]
Pin[20700 -26650 10000 2000 10800 5600 "" "4" ""]
Pin[0 -33550 10000 2000 10800 5600 "" "5" ""]
Pin[-19700 -27650 10000 2000 10800 5600 "" "6" ""]
Pin[-31510 -11880 10000 2000 10800 5600 "" "7" ""]
Pin[-31990 10270 10000 2000 10800 5600 "" "8" ""]
Pin[-20500 26500 10000 2000 10800 5600 "" "9" ""]
)
