# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Karsten Malcher 
# No warranties express or implied
# Footprint converted from Kicad Module tube_gzc7-y
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tube_gzc7-y
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 9
# Draw arc object count: 8
# Pad count: 7
#
Element["" "T***" "" "" 0 0 0 -41340 0 100 ""]
(
ElementArc[0 0 31500 31500 0 360 1490]
ElementArc[-13190 13190 5900 5900 0 360 1490]
ElementArc[-13270 -13280 5900 5900 0 360 1490]
ElementArc[-10 -18800 5900 5900 0 360 1490]
ElementArc[13100 -13190 5900 5900 0 360 1490]
ElementArc[18700 10 5900 5900 0 360 1490]
ElementArc[-18690 0 5900 5900 0 360 1490]
ElementArc[13300 13190 5900 5900 0 360 1490]
ElementArc[0 0 7090 7090 0 360 1490]
ElementArc[0 0 37486 37486 180 -90 1490]
ElementArc[0 0 37404 37404 0 -90 1490]
ElementArc[0 0 37505 37505 135 -90 1490]
ElementArc[0 0 37407 37407 -45 -90 1490]
ElementArc[34450 -14170 5852 5852 117 -90 1490]
ElementArc[34450 -14170 5820 5820 0 -90 1490]
ElementArc[-34450 14170 5852 5852 -63 -90 1490]
ElementArc[-34450 14170 5820 5820 180 -90 1490]
Pin[31490 0 11800 2000 12600 7870 "" "2" ""]
Pin[22270 -22270 11800 2000 12600 7870 "" "3" ""]
Pin[0 -31490 11800 2000 12600 7870 "" "4" ""]
Pin[-22270 -22270 11800 2000 12600 7870 "" "5" ""]
Pin[-31490 0 11800 2000 12600 7870 "" "6" ""]
Pin[-22270 22270 11800 2000 12600 7870 "" "7" ""]
Pin[22270 22270 11800 2000 12600 7870 "" "1" ""]
)
