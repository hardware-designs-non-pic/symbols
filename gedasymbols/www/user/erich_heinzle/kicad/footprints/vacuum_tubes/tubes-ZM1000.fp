# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-ZM1000
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-ZM1000
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 18
# Draw arc object count: 1
# Pad count: 16
#
Element["" "ZM-1000" "" "" 0 0 1680 43620 0 100 ""]
(
ElementArc[0 0 25046 25046 0 360 1200]
ElementArc[-5000 -20000 2489 2489 0 360 250]
ElementArc[5000 -20000 2489 2489 0 360 250]
ElementArc[15000 -10000 2489 2489 0 360 250]
ElementArc[5000 -10000 2489 2489 0 360 250]
ElementArc[-5000 -10000 2489 2489 0 360 250]
ElementArc[-15000 -10000 2489 2489 0 360 250]
ElementArc[-15000 0 2489 2489 0 360 250]
ElementArc[-15000 10000 2489 2489 0 360 250]
ElementArc[-5000 0 2489 2489 0 360 250]
ElementArc[5000 0 2489 2489 0 360 250]
ElementArc[15000 0 2489 2489 0 360 250]
ElementArc[5000 10000 2489 2489 0 360 250]
ElementArc[-5000 10000 2489 2489 0 360 250]
ElementArc[-5000 20000 2489 2489 0 360 250]
ElementArc[5000 20000 2489 2489 0 360 250]
ElementArc[15000 10000 2489 2489 0 360 250]
ElementArc[0 0 23504 23504 0 360 250]
ElementArc[0 280 31491 31491 0 360 500]
Pin[15000 -10000 5500 2000 6300 3200 "" "IV" ""]
Pin[-15000 10000 5500 2000 6300 3200 "" "A1" ""]
Pin[15000 10000 5500 2000 6300 3200 "" "A2" ""]
Pin[-15000 -10000 5500 2000 6300 3200 "" "IV" ""]
Pin[-5000 0 5500 2000 6300 3200 "" "K0" ""]
Pin[5000 -20000 5500 2000 6300 3200 "" "K1" ""]
Pin[5000 -10000 5500 2000 6300 3200 "" "K2" ""]
Pin[5000 0 5500 2000 6300 3200 "" "K3" ""]
Pin[5000 10000 5500 2000 6300 3200 "" "K4" ""]
Pin[5000 20000 5500 2000 6300 3200 "" "K5" ""]
Pin[-5000 20000 5500 2000 6300 3200 "" "K6" ""]
Pin[-5000 10000 5500 2000 6300 3200 "" "K7" ""]
Pin[-5000 -20000 5500 2000 6300 3200 "" "K8" ""]
Pin[-5000 -10000 5500 2000 6300 3200 "" "K9" ""]
Pin[-15000 0 5500 2000 6300 3200 "" "KDP" ""]
Pin[15000 0 5500 2000 6300 3200 "" "V" ""]
)
