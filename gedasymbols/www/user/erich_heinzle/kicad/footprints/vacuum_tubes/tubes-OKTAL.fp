# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module tubes-OKTAL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: tubes-OKTAL
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 18
# Draw arc object count: 3
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 -1000 62000 0 100 ""]
(
ElementLine[-45000 0 -25000 0 2400]
ElementLine[0 -46000 0 -26000 2400]
ElementLine[26000 0 45000 0 2400]
ElementLine[0 24000 0 44000 2400]
ElementLine[18000 18000 31000 31000 2400]
ElementLine[18000 -18000 32000 -32000 2400]
ElementLine[-18000 -18000 -32000 -32000 2400]
ElementLine[-17000 17000 -31000 31000 2400]
ElementArc[-35000 15000 4412 4412 0 360 250]
ElementArc[-35000 -15000 4412 4412 0 360 250]
ElementArc[-15000 -35000 4412 4412 0 360 250]
ElementArc[15000 -35000 4412 4412 0 360 250]
ElementArc[35000 -15000 4412 4412 0 360 250]
ElementArc[35000 15000 4412 4412 0 360 250]
ElementArc[15000 35000 4412 4412 0 360 250]
ElementArc[-15000 35000 4412 4412 0 360 250]
ElementArc[0 0 37703 37703 0 360 500]
ElementArc[-35000 15000 7269 7269 0 360 800]
ElementArc[-15000 35000 7099 7099 0 360 800]
ElementArc[15000 35000 7099 7099 0 360 800]
ElementArc[35000 15000 6958 6958 0 360 800]
ElementArc[35000 -15000 7198 7198 0 360 800]
ElementArc[15000 -35000 7198 7198 0 360 800]
ElementArc[-15000 -35000 7269 7269 0 360 800]
ElementArc[-35000 -15000 7269 7269 0 360 800]
ElementArc[0 0 39527 39527 0 360 1200]
ElementArc[0 15000 5000 5000 0 360 500]
ElementArc[0 0 15811 15811 0 360 500]
ElementArc[0 0 17088 17088 0 360 500]
Pin[15000 35000 10000 2000 10800 5000 "" "1" ""]
Pin[35000 15000 10000 2000 10800 5000 "" "2" ""]
Pin[35000 -15000 10000 2000 10800 5000 "" "3" ""]
Pin[15000 -35000 10000 2000 10800 5000 "" "4" ""]
Pin[-15000 -35000 10000 2000 10800 5000 "" "5" ""]
Pin[-35000 -15000 10000 2000 10800 5000 "" "6" ""]
Pin[-35000 15000 10000 2000 10800 5000 "" "7" ""]
Pin[-15000 35000 10000 2000 10800 5000 "" "8" ""]
)
