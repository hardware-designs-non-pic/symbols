# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module tfbga48
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: tfbga48
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 48
#
Element["" " U    " "" "" 0 0 15748 0 1 100 ""]
(
ElementLine[-11811 -15354 -11417 -15748 500]
ElementLine[-11023 -15748 -11811 -14960 500]
ElementLine[-11811 -14173 -10236 -15748 500]
ElementLine[-10629 -15748 -11811 -14566 500]
ElementLine[-11811 -15748 11811 -15748 500]
ElementLine[11811 -15748 11811 15748 500]
ElementLine[11811 15748 -11811 15748 500]
ElementLine[-11811 15748 -11811 -15748 500]
ElementLine[-12598 -11811 -12598 -16535 500]
ElementLine[-12598 -16535 -7874 -16535 500]
ElementArc[-8267 -12204 1969 1969 0 360 500]
ElementLine[-11023 -15748 -11811 -13779 500]
Pad[-7381 10334 -7381 10334 1377 2000 2177 "0" "_H1_" "blah"]
Pad[-4429 10334 -4429 10334 1377 2000 2177 "0" "_H2_" "blah"]
Pad[-1476 10334 -1476 10334 1377 2000 2177 "0" "_H3_" "blah"]
Pad[1476 10334 1476 10334 1377 2000 2177 "0" "_H4_" "blah"]
Pad[4429 10334 4429 10334 1377 2000 2177 "0" "_H5_" "blah"]
Pad[7381 10334 7381 10334 1377 2000 2177 "0" "_H6_" "blah"]
Pad[-7381 7381 -7381 7381 1377 2000 2177 "0" "_G1_" "blah"]
Pad[-4429 7381 -4429 7381 1377 2000 2177 "0" "_G2_" "blah"]
Pad[-1476 7381 -1476 7381 1377 2000 2177 "0" "_G3_" "blah"]
Pad[1476 7381 1476 7381 1377 2000 2177 "0" "_G4_" "blah"]
Pad[4429 7381 4429 7381 1377 2000 2177 "0" "_G5_" "blah"]
Pad[7381 7381 7381 7381 1377 2000 2177 "0" "_G6_" "blah"]
Pad[-7381 4429 -7381 4429 1377 2000 2177 "0" "_F1_" "blah"]
Pad[-7381 1476 -7381 1476 1377 2000 2177 "0" "_E1_" "blah"]
Pad[-7381 -1476 -7381 -1476 1377 2000 2177 "0" "_D1_" "blah"]
Pad[-7381 -4429 -7381 -4429 1377 2000 2177 "0" "_C1_" "blah"]
Pad[-7381 -7381 -7381 -7381 1377 2000 2177 "0" "_B1_" "blah"]
Pad[-7381 -10334 -7381 -10334 1377 2000 2177 "0" "_A1_" "blah"]
Pad[-4429 4429 -4429 4429 1377 2000 2177 "0" "_F2_" "blah"]
Pad[-1476 4429 -1476 4429 1377 2000 2177 "0" "_F3_" "blah"]
Pad[1476 4429 1476 4429 1377 2000 2177 "0" "_F4_" "blah"]
Pad[4429 4429 4429 4429 1377 2000 2177 "0" "_F5_" "blah"]
Pad[7381 4429 7381 4429 1377 2000 2177 "0" "_F6_" "blah"]
Pad[-4429 1476 -4429 1476 1377 2000 2177 "0" "_E2_" "blah"]
Pad[-1476 1476 -1476 1476 1377 2000 2177 "0" "_E3_" "blah"]
Pad[1476 1476 1476 1476 1377 2000 2177 "0" "_E4_" "blah"]
Pad[4429 1476 4429 1476 1377 2000 2177 "0" "_E5_" "blah"]
Pad[7381 1476 7381 1476 1377 2000 2177 "0" "_E6_" "blah"]
Pad[-4429 -1476 -4429 -1476 1377 2000 2177 "0" "_D2_" "blah"]
Pad[-1476 -1476 -1476 -1476 1377 2000 2177 "0" "_D3_" "blah"]
Pad[1476 -1476 1476 -1476 1377 2000 2177 "0" "_D4_" "blah"]
Pad[4429 -1476 4429 -1476 1377 2000 2177 "0" "_D5_" "blah"]
Pad[7381 -1476 7381 -1476 1377 2000 2177 "0" "_D6_" "blah"]
Pad[-4429 -4429 -4429 -4429 1377 2000 2177 "0" "_C2_" "blah"]
Pad[-1476 -4429 -1476 -4429 1377 2000 2177 "0" "_C3_" "blah"]
Pad[1476 -4429 1476 -4429 1377 2000 2177 "0" "_C4_" "blah"]
Pad[4429 -4429 4429 -4429 1377 2000 2177 "0" "_C5_" "blah"]
Pad[7381 -4429 7381 -4429 1377 2000 2177 "0" "_C6_" "blah"]
Pad[-4429 -7381 -4429 -7381 1377 2000 2177 "0" "_B2_" "blah"]
Pad[-1476 -7381 -1476 -7381 1377 2000 2177 "0" "_B3_" "blah"]
Pad[1476 -7381 1476 -7381 1377 2000 2177 "0" "_B4_" "blah"]
Pad[4429 -7381 4429 -7381 1377 2000 2177 "0" "_B5_" "blah"]
Pad[7381 -7381 7381 -7381 1377 2000 2177 "0" "_B6_" "blah"]
Pad[-4429 -10334 -4429 -10334 1377 2000 2177 "0" "_A2_" "blah"]
Pad[-1476 -10334 -1476 -10334 1377 2000 2177 "0" "_A3_" "blah"]
Pad[1476 -10334 1476 -10334 1377 2000 2177 "0" "_A4_" "blah"]
Pad[4429 -10334 4429 -10334 1377 2000 2177 "0" "_A5_" "blah"]
Pad[7381 -10334 7381 -10334 1377 2000 2177 "0" "_A6_" "blah"]
)
