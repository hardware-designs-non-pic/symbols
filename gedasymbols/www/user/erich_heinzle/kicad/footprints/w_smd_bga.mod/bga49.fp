# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module bga49
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: bga49
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 49
#
Element["" " U    " "" "" 0 0 0 -15748 0 100 ""]
(
ElementLine[-13385 13779 -13779 13385 500]
ElementLine[-12992 13779 -13779 12992 500]
ElementLine[-12598 13779 -13779 12598 500]
ElementLine[-12204 13779 -13779 12204 500]
ElementLine[-14566 9842 -14566 14566 500]
ElementLine[-14566 14566 -9842 14566 500]
ElementLine[-13779 10629 -10629 13779 500]
ElementLine[-11023 13779 -13779 11023 500]
ElementLine[-13779 11417 -11417 13779 500]
ElementLine[-13779 13779 -13779 -13779 500]
ElementLine[13779 13779 -13779 13779 500]
ElementLine[13779 -13779 13779 13779 500]
ElementLine[-13779 -13779 13779 -13779 500]
ElementLine[-11811 13779 -13779 11811 500]
Pad[9448 -3149 9448 -3149 1850 2000 2650 "0" "_G5_" "blah"]
Pad[6299 -3149 6299 -3149 1850 2000 2650 "0" "_F5_" "blah"]
Pad[3149 -3149 3149 -3149 1850 2000 2650 "0" "_E5_" "blah"]
Pad[0 -3149 0 -3149 1850 2000 2650 "0" "_D5_" "blah"]
Pad[-3149 -3149 -3149 -3149 1850 2000 2650 "0" "_C5_" "blah"]
Pad[-6299 -3149 -6299 -3149 1850 2000 2650 "0" "_B5_" "blah"]
Pad[-9448 -3149 -9448 -3149 1850 2000 2650 "0" "_A5_" "blah"]
Pad[-9448 -6299 -9448 -6299 1850 2000 2650 "0" "_A6_" "blah"]
Pad[-6299 -6299 -6299 -6299 1850 2000 2650 "0" "_B6_" "blah"]
Pad[-3149 -6299 -3149 -6299 1850 2000 2650 "0" "_C6_" "blah"]
Pad[0 -6299 0 -6299 1850 2000 2650 "0" "_D6_" "blah"]
Pad[3149 -6299 3149 -6299 1850 2000 2650 "0" "_E6_" "blah"]
Pad[6299 -6299 6299 -6299 1850 2000 2650 "0" "_F6_" "blah"]
Pad[9448 -6299 9448 -6299 1850 2000 2650 "0" "_G6_" "blah"]
Pad[-9448 -9448 -9448 -9448 1850 2000 2650 "0" "_A7_" "blah"]
Pad[-6299 -9448 -6299 -9448 1850 2000 2650 "0" "_B7_" "blah"]
Pad[-3149 -9448 -3149 -9448 1850 2000 2650 "0" "_C7_" "blah"]
Pad[0 -9448 0 -9448 1850 2000 2650 "0" "_D7_" "blah"]
Pad[3149 -9448 3149 -9448 1850 2000 2650 "0" "_E7_" "blah"]
Pad[6299 -9448 6299 -9448 1850 2000 2650 "0" "_F7_" "blah"]
Pad[9448 -9448 9448 -9448 1850 2000 2650 "0" "_G7_" "blah"]
Pad[9448 3149 9448 3149 1850 2000 2650 "0" "_G3_" "blah"]
Pad[6299 3149 6299 3149 1850 2000 2650 "0" "_F3_" "blah"]
Pad[3149 3149 3149 3149 1850 2000 2650 "0" "_E3_" "blah"]
Pad[0 3149 0 3149 1850 2000 2650 "0" "_D3_" "blah"]
Pad[-3149 3149 -3149 3149 1850 2000 2650 "0" "_C3_" "blah"]
Pad[-6299 3149 -6299 3149 1850 2000 2650 "0" "_B3_" "blah"]
Pad[-9448 3149 -9448 3149 1850 2000 2650 "0" "_A3_" "blah"]
Pad[-9448 0 -9448 0 1850 2000 2650 "0" "_A4_" "blah"]
Pad[-6299 0 -6299 0 1850 2000 2650 "0" "_B4_" "blah"]
Pad[-3149 0 -3149 0 1850 2000 2650 "0" "_C4_" "blah"]
Pad[0 0 0 0 1850 2000 2650 "0" "_D4_" "blah"]
Pad[3149 0 3149 0 1850 2000 2650 "0" "_E4_" "blah"]
Pad[6299 0 6299 0 1850 2000 2650 "0" "_F4_" "blah"]
Pad[9448 0 9448 0 1850 2000 2650 "0" "_G4_" "blah"]
Pad[9448 6299 9448 6299 1850 2000 2650 "0" "_G2_" "blah"]
Pad[6299 6299 6299 6299 1850 2000 2650 "0" "_F2_" "blah"]
Pad[3149 6299 3149 6299 1850 2000 2650 "0" "_E2_" "blah"]
Pad[0 6299 0 6299 1850 2000 2650 "0" "_D2_" "blah"]
Pad[-3149 6299 -3149 6299 1850 2000 2650 "0" "_C2_" "blah"]
Pad[-6299 6299 -6299 6299 1850 2000 2650 "0" "_B2_" "blah"]
Pad[-9448 6299 -9448 6299 1850 2000 2650 "0" "_A2_" "blah"]
Pad[-9448 9448 -9448 9448 1850 2000 2650 "0" "_A1_" "blah"]
Pad[-6299 9448 -6299 9448 1850 2000 2650 "0" "_B1_" "blah"]
Pad[-3149 9448 -3149 9448 1850 2000 2650 "0" "_C1_" "blah"]
Pad[0 9448 0 9448 1850 2000 2650 "0" "_D1_" "blah"]
Pad[3149 9448 3149 9448 1850 2000 2650 "0" "_E1_" "blah"]
Pad[6299 9448 6299 9448 1850 2000 2650 "0" "_F1_" "blah"]
Pad[9448 9448 9448 9448 1850 2000 2650 "0" "_G1_" "blah"]
)
