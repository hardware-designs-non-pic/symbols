# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module r-xbga-n8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: r-xbga-n8
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" " U    " "" "" 0 0 0 -3937 0 100 ""]
(
ElementLine[-3779 -1811 -3779 1811 500]
ElementLine[-3779 1811 3779 1811 500]
ElementLine[3779 -1811 3779 1811 500]
ElementLine[-3779 -1811 3779 -1811 500]
ElementLine[-4724 -393 -4724 2755 500]
ElementLine[-4724 2755 -1574 2755 500]
ElementLine[-3149 1574 -3543 1181 500]
ElementLine[-3543 393 -2362 1574 500]
ElementLine[-2755 1574 -3543 787 500]
Pad[984 -984 984 -984 1023 2000 1823 "0" "_C2_" "blah"]
Pad[984 984 984 984 1023 2000 1823 "0" "_C1_" "blah"]
Pad[2952 984 2952 984 1023 2000 1823 "0" "_D1_" "blah"]
Pad[2952 -984 2952 -984 1023 2000 1823 "0" "_D2_" "blah"]
Pad[-984 -984 -984 -984 1023 2000 1823 "0" "_B2_" "blah"]
Pad[-984 984 -984 984 1023 2000 1823 "0" "_B1_" "blah"]
Pad[-2952 984 -2952 984 1023 2000 1823 "0" "_A1_" "blah"]
Pad[-2952 -984 -2952 -984 1023 2000 1823 "0" "_A2_" "blah"]
)
