# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 8p8c
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 8p8c
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" "J***" "" "" 0 0 0 -45280 0 100 ""]
(
ElementLine[-29920 -38980 29920 -38980 1500]
ElementLine[-29920 42520 29920 42520 1500]
ElementLine[15750 1180 15750 42520 1500]
ElementLine[-15750 42520 -15750 1180 1500]
ElementLine[7870 42520 7870 1180 1500]
ElementLine[-19690 1180 -19690 42520 1500]
ElementLine[-19690 1180 19690 1180 1500]
ElementLine[19690 1180 19690 42520 1500]
ElementLine[-7870 42520 -7870 1180 1500]
ElementLine[-29920 42520 -29920 -38980 1500]
ElementLine[29920 -38980 29920 42520 1500]
Pin[-22500 0 12600 2000 13400 12600 "" "" "hole"]
Pin[22500 0 12600 2000 13400 12600 "" "" "hole"]
Pin[17500 -35000 5910 2000 6710 3540 "" "8" ""]
Pin[12500 -25000 5910 2000 6710 3540 "" "7" ""]
Pin[7500 -35000 5910 2000 6710 3540 "" "6" ""]
Pin[2500 -25000 5910 2000 6710 3540 "" "5" ""]
Pin[-2500 -35000 5910 2000 6710 3540 "" "4" ""]
Pin[-7500 -25000 5910 2000 6710 3540 "" "3" ""]
Pin[-12500 -35000 5910 2000 6710 3540 "" "2" ""]
Pin[-17500 -25000 5910 2000 6710 3540 "" "1" ""]
)
