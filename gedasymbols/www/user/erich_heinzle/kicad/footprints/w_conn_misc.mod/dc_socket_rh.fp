# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module dc_socket_rh
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: dc_socket_rh
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 18
#
Element["" "dc_socket_rh" "" "" 0 0 0 34250 0 100 ""]
(
ElementLine[-17720 11020 17720 11020 1500]
ElementLine[-17716 29527 17716 29527 1500]
ElementLine[17720 28740 17720 -28740 1500]
ElementLine[17720 -28740 -17720 -28740 1500]
ElementLine[-17720 -28740 -17720 28740 1500]
Pin[19685 -17322 7874 2000 8674 3937 "" "1" ""]
Pin[19685 -14370 7874 2000 8674 3937 "" "1" ""]
Pin[19685 -11614 7874 2000 8674 3937 "" "1" ""]
Pad[-4330 -24803 4330 -24803 7874 2000 8674 "" "3" "blah"]
Pin[1377 -24803 7874 2000 8674 3937 "" "3" ""]
Pin[-4330 -24803 7874 2000 8674 3937 "" "3" ""]
Pin[4330 -24803 7874 2000 8674 3937 "" "3" ""]
Pin[-1377 -24803 7874 2000 8674 3937 "" "3" ""]
Pad[-4330 -24803 4330 -24803 7874 2000 8674 "" "3" "blah"]
Pad[19685 -17322 19685 -8661 7874 2000 8674 "" "1" "blah"]
Pad[-4330 0 4330 0 7874 2000 8674 "" "2" "blah"]
Pin[-1377 0 7874 2000 8674 3937 "" "2" ""]
Pin[4330 0 7874 2000 8674 3937 "" "2" ""]
Pin[-4330 0 7874 2000 8674 3937 "" "2" ""]
Pin[1377 0 7874 2000 8674 3937 "" "2" ""]
Pad[-4330 0 4330 0 7874 2000 8674 "" "2" "blah"]
Pad[19685 -17322 19685 -8661 7874 2000 8674 "" "1" "blah"]
Pin[19685 -8661 7874 2000 8674 3937 "" "1" ""]
)
