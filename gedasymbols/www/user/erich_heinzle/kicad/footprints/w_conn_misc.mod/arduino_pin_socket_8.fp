# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module arduino_pin_socket_8
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: arduino_pin_socket_8
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "ARDUINO_PIN_SOCKET_8" "" "" 0 0 0 -8500 0 100 ""]
(
ElementLine[-30000 -5000 -30000 5000 1200]
ElementLine[-40000 -5000 40000 -5000 1200]
ElementLine[40000 -5000 40000 5000 1200]
ElementLine[40000 5000 -40000 5000 1200]
ElementLine[-40000 5000 -40000 -5000 1200]
Pin[-35000 0 6000 2000 6800 3939 "" "1" "square"]
Pin[-25000 0 6000 2000 6800 3939 "" "2" ""]
Pin[-15000 0 6000 2000 6800 3939 "" "3" ""]
Pin[-5000 0 6000 2000 6800 3939 "" "4" ""]
Pin[5000 0 6000 2000 6800 3939 "" "5" ""]
Pin[15000 0 6000 2000 6800 3939 "" "6" ""]
Pin[25000 0 6000 2000 6800 3939 "" "7" ""]
Pin[35000 0 6000 2000 6800 3939 "" "8" ""]
)
