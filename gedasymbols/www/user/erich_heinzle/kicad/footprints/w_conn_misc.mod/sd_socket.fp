# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sd_socket
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sd_socket
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 17
#
Element["" "sd_socket" "" "" 0 0 0 66930 0 100 ""]
(
ElementLine[-38980 60630 -56300 60630 1500]
ElementLine[56300 60630 38980 60630 1500]
ElementLine[25200 -13390 -24410 -13390 1500]
ElementLine[32680 -7480 39370 60630 1500]
ElementLine[-32680 -6690 -39370 60630 1500]
ElementLine[-56300 60630 -56300 -52760 1500]
ElementLine[-56300 -52760 56300 -52760 1500]
ElementLine[56300 -52760 56300 60630 1500]
ElementArc[0 60630 38980 38980 270 -90 1500]
ElementArc[0 60630 38980 38980 0 -90 1500]
ElementArc[25980 -6690 6745 6745 270 -90 1500]
ElementArc[-25200 -5910 7521 7521 0 -90 1500]
Pad[36500 -58265 36500 -56255 4130 2000 4930 "" "9" "square"]
Pad[26660 -58265 26660 -56255 4130 2000 4930 "" "1" "square"]
Pad[16810 -58265 16810 -56255 4130 2000 4930 "" "2" "square"]
Pad[6970 -58265 6970 -56255 4130 2000 4930 "" "3" "square"]
Pad[-2870 -58265 -2870 -56255 4130 2000 4930 "" "4" "square"]
Pad[-12710 -58265 -12710 -56255 4130 2000 4930 "" "5" "square"]
Pad[-22560 -58265 -22560 -56255 4130 2000 4930 "" "6" "square"]
Pad[-32080 -58265 -32080 -56255 4130 2000 4930 "" "7" "square"]
Pad[-38780 -58265 -38780 -56255 4130 2000 4930 "" "8" "square"]
Pad[-48240 -56360 -47260 -56360 4330 2000 5130 "" "CD" "square"]
Pad[58070 -37700 58070 -37700 7480 2000 8280 "" "" "square"]
Pad[58070 23130 58070 23130 7480 2000 8280 "" "" "square"]
Pad[-58540 -37700 -58540 -37700 7480 2000 8280 "" "" "square"]
Pad[-58265 -16439 -56295 -16439 4330 2000 5130 "" "COM" "square"]
Pad[-57480 21630 -57480 21630 6300 2000 7100 "" "WP" "square"]
Pin[47640 38680 5120 2000 5920 5120 "" "" ""]
Pin[-47640 38680 7090 2000 7890 7090 "" "" ""]
)
