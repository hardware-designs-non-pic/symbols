# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module 6p6c
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: 6p6c
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "J***" "" "" 0 0 0 -47240 0 100 ""]
(
ElementLine[15750 0 15750 41340 1500]
ElementLine[-15750 41340 -15750 0 1500]
ElementLine[7870 41340 7870 0 1500]
ElementLine[-19690 0 -19690 41340 1500]
ElementLine[-19690 0 19690 0 1500]
ElementLine[19690 0 19690 41340 1500]
ElementLine[-7870 41340 -7870 0 1500]
ElementLine[30310 41340 -30310 41340 1500]
ElementLine[-30310 41340 -30310 -40160 1500]
ElementLine[-30310 -40160 30310 -40160 1500]
ElementLine[30310 -40160 30310 41340 1500]
Pin[-20000 0 12600 2000 13400 12600 "" "" "hole"]
Pin[20000 0 12600 2000 13400 12600 "" "" "hole"]
Pin[12500 -35000 5910 2000 6710 3540 "" "6" ""]
Pin[7500 -25000 5910 2000 6710 3540 "" "5" ""]
Pin[2500 -35000 5910 2000 6710 3540 "" "4" ""]
Pin[-2500 -25000 5910 2000 6710 3540 "" "3" ""]
Pin[-7500 -35000 5910 2000 6710 3540 "" "2" ""]
Pin[-12500 -25000 5910 2000 6710 3540 "" "1" ""]
)
