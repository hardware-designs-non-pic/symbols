# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Karsten Malcher 
# No warranties express or implied
# Footprint converted from Kicad Module switch-CK101X12
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: switch-CK101X12
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 3
# Draw arc object count: 21
# Pad count: 13
#
Element["" "switch-CK101X12" "" "" 0 0 7500 -56490 0 100 ""]
(
ElementLine[-15500 -26800 15500 -26800 600]
ElementLine[31000 0 15500 26800 600]
ElementLine[15500 26800 -15500 26800 600]
ElementLine[-15500 26800 -31000 0 600]
ElementLine[-31000 0 -15500 -26800 600]
ElementLine[-5000 0 5500 0 600]
ElementLine[0 5500 0 -5500 600]
ElementLine[15500 -26800 31000 0 600]
ElementLine[-2500 37500 2500 37500 600]
ElementLine[0 35000 0 40000 600]
ElementArc[0 37500 4243 4243 0 360 300]
ElementArc[0 0 36416 36416 0 360 300]
ElementArc[0 0 18385 18385 0 360 300]
ElementArc[24490 42500 4999 4999 0 360 600]
ElementArc[-24500 42500 5007 5007 0 360 600]
ElementArc[-24490 -42500 4999 4999 0 360 600]
ElementArc[0 0 34012 34012 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[24500 -42500 5007 5007 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
ElementArc[0 0 12044 12044 0 360 600]
ElementArc[0 0 19517 19517 0 360 600]
Pin[-11200 41800 7800 2000 8600 5200 "" "1" ""]
Pin[-30600 30600 7800 2000 8600 5200 "" "2" ""]
Pin[-41800 11200 7800 2000 8600 5200 "" "3" ""]
Pin[-41800 -11200 7800 2000 8600 5200 "" "4" ""]
Pin[-30600 -30600 7800 2000 8600 5200 "" "5" ""]
Pin[-11200 -41800 7800 2000 8600 5200 "" "6" ""]
Pin[11200 -41800 7800 2000 8600 5200 "" "7" ""]
Pin[30600 -30600 7800 2000 8600 5200 "" "8" ""]
Pin[41800 -11200 7800 2000 8600 5200 "" "9" ""]
Pin[41800 11200 7800 2000 8600 5200 "" "10" ""]
Pin[30600 30600 7800 2000 8600 5200 "" "11" ""]
Pin[11200 41800 7800 2000 8600 5200 "" "12" ""]
Pin[-11300 11300 7800 2000 8600 5200 "" "A" ""]
)
