# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-10-27
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-10-27
# Text descriptor count: 1
# Draw segment object count: 30
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 680 -10370 0 100 ""]
(
ElementLine[62990 0 19680 0 2400]
ElementLine[0 -19680 0 -62990 2400]
ElementLine[0 19680 0 62990 2400]
ElementLine[-19680 -3930 -61020 -11810 2400]
ElementLine[-19680 3930 -61020 11810 2400]
ElementLine[-17710 -7870 -57080 -23620 2400]
ElementLine[-17710 7870 -59050 23620 2400]
ElementLine[-15740 11810 -51180 35430 2400]
ElementLine[-15740 -11810 -51180 -35430 2400]
ElementLine[-11810 -15740 -41330 -47240 2400]
ElementLine[-41330 47240 -11810 15740 2400]
ElementLine[-3930 19680 -11810 61020 2400]
ElementLine[-3930 -19680 -11810 -61020 2400]
ElementLine[-7870 -17710 -27550 -57080 2400]
ElementLine[-27550 57080 -7870 17710 2400]
ElementLine[3930 -19680 11810 -61020 2400]
ElementLine[11810 61020 3930 19680 2400]
ElementLine[7870 -17710 27550 -57080 2400]
ElementLine[41330 -47240 11810 -15740 2400]
ElementLine[11810 15740 41330 47240 2400]
ElementLine[7870 17710 27550 57080 2400]
ElementLine[15740 11810 51180 35430 2400]
ElementLine[51180 -35430 15740 -11810 2400]
ElementLine[19680 -3930 61020 -11810 2400]
ElementLine[61020 11810 19680 3930 2400]
ElementLine[57080 -23620 17710 -7870 2400]
ElementLine[17710 7870 57080 23620 2400]
ElementLine[-55110 0 -27550 0 2400]
ElementLine[-62990 0 -55110 0 2400]
ElementLine[-27550 0 -19680 0 2400]
ElementArc[0 0 38962 38962 0 360 250]
ElementArc[0 0 19474 19474 0 360 250]
Pin[-62990 0 6000 2000 6800 4000 "" "1" ""]
Pad[-62990 -3000 -62990 3000 6000 2000 6800 "" "1" ""]
Pad[-62990 -3000 -62990 3000 6000 2000 6800 "" "1" ",onsolder"]
Pin[-19680 0 6000 2000 6800 4000 "" "2" ""]
Pad[-19680 -3000 -19680 3000 6000 2000 6800 "" "2" ""]
Pad[-19680 -3000 -19680 3000 6000 2000 6800 "" "2" ",onsolder"]
)
