# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-8230
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-8230
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 0 -11250 0 100 ""]
(
ElementLine[-18430 930 -15000 930 260]
ElementLine[-15000 930 -15000 -930 260]
ElementLine[-18430 -930 -15000 -930 260]
ElementLine[-18430 930 -18430 -930 260]
ElementLine[15000 930 18430 930 260]
ElementLine[18430 930 18430 -930 260]
ElementLine[15000 -930 18430 -930 260]
ElementLine[15000 930 15000 -930 260]
ElementLine[-10000 -6250 10000 -6250 700]
ElementLine[10000 6250 -10000 6250 700]
ElementLine[-22500 0 -18750 0 1900]
ElementLine[22500 0 18750 0 1900]
ElementArc[-9370 -620 5664 5664 0 360 700]
ElementArc[-9370 620 5665 5665 0 360 700]
ElementArc[9370 620 5664 5664 0 360 700]
ElementArc[9370 -620 5665 5665 0 360 700]
Pin[-22500 0 6600 2000 7400 3200 "" "1" ""]
Pad[-22500 -3300 -22500 3300 6600 2000 7400 "" "1" ""]
Pad[-22500 -3300 -22500 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[22500 0 6600 2000 7400 3200 "" "2" ""]
Pad[22500 -3300 22500 3300 6600 2000 7400 "" "2" ""]
Pad[22500 -3300 22500 3300 6600 2000 7400 "" "2" ",onsolder"]
)
