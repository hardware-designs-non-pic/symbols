# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module HFSRA_C
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: HFSRA_C
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "HFSRA_C" "" "" 0 0 0 30000 0 100 ""]
(
ElementArc[0 0 20616 20616 0 360 1500]
Pin[-10000 0 8820 2000 9620 3200 "" "1" "square"]
Pin[10000 0 8820 2000 9620 3200 "" "2" ""]
)
