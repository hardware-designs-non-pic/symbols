# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module Pot_ferrite_P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Pot_ferrite_P
# Text descriptor count: 1
# Draw segment object count: 41
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" "Pot_P" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-10000 -35000 -10000 -37000 1500]
ElementLine[-10000 -37000 -13000 -39000 1500]
ElementLine[10000 -35000 10000 -37000 1500]
ElementLine[10000 -37000 13000 -39000 1500]
ElementLine[-15000 -20000 -10000 -15000 1500]
ElementLine[-10000 -15000 9000 -15000 1500]
ElementLine[9000 -15000 10000 -15000 1500]
ElementLine[10000 -15000 15000 -20000 1500]
ElementLine[15000 -20000 15000 -27000 1500]
ElementLine[-15000 18000 -12000 15000 1500]
ElementLine[-12000 15000 12000 15000 1500]
ElementLine[12000 15000 15000 18000 1500]
ElementLine[-10000 36000 -10000 37000 1500]
ElementLine[-10000 36000 -10000 32000 1500]
ElementLine[-10000 37000 -12000 39000 1500]
ElementLine[10000 30000 10000 37000 1500]
ElementLine[10000 37000 12000 39000 1500]
ElementLine[-15000 -20000 -15000 -27500 1500]
ElementLine[-15000 -27500 -10000 -30000 1500]
ElementLine[-10000 -30000 -10000 -35000 1500]
ElementLine[15000 -25000 15000 -27500 1500]
ElementLine[15000 -27500 10000 -30000 1500]
ElementLine[10000 -30000 10000 -35000 1500]
ElementLine[15000 17500 15000 25000 1500]
ElementLine[15000 25000 15000 27500 1500]
ElementLine[15000 27500 10000 30000 1500]
ElementLine[-10000 32500 -10000 30000 1500]
ElementLine[-10000 30000 -15000 27500 1500]
ElementLine[-15000 27500 -15000 17500 1500]
ElementLine[40000 10000 40000 -10000 1500]
ElementLine[25000 -7500 25000 -10000 1500]
ElementLine[25000 -10000 42500 -10000 1500]
ElementLine[42500 -10000 42500 10000 1500]
ElementLine[42500 10000 25000 10000 1500]
ElementLine[25000 10000 25000 -7500 1500]
ElementLine[-40000 -10000 -40000 7500 1500]
ElementLine[-40000 -10000 -25000 -10000 1500]
ElementLine[-25000 -10000 -25000 10000 1500]
ElementLine[-25000 10000 -42500 10000 1500]
ElementLine[-42500 10000 -42500 -10000 1500]
ElementLine[-42500 -10000 -40000 -10000 1500]
ElementArc[0 0 31623 31623 0 360 1500]
ElementArc[0 0 41231 41231 0 360 1500]
Pin[-10000 -22500 7200 2000 8000 3200 "" "1" "square"]
Pin[-10000 22500 7200 2000 8000 3200 "" "2" ""]
Pin[10000 22500 7200 2000 8000 3200 "" "3" ""]
Pin[10000 -22500 7200 2000 8000 3200 "" "4" ""]
Pin[40000 0 6000 2000 6800 3200 "" "5" ""]
Pin[-40000 0 6000 2000 6800 3200 "" "6" ""]
)
