# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B30
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B30
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -25880 -6430 0 100 ""]
(
ElementLine[-53140 -27550 53140 -27550 500]
ElementLine[53140 -27550 53140 27550 500]
ElementLine[53140 27550 -53140 27550 500]
ElementLine[-53140 27550 -53140 -27550 500]
ElementLine[-57080 0 -41330 0 500]
ElementLine[-49210 7870 -49210 -7870 500]
ElementLine[41330 0 57080 0 500]
ElementLine[49210 7870 49210 -7870 500]
ElementArc[-49210 0 5558 5558 0 360 500]
ElementArc[49210 0 5558 5558 0 360 500]
Pin[29520 -14760 15000 2000 15800 5000 "" "1" ""]
Pad[29520 -22260 29520 -7260 15000 2000 15800 "" "1" ""]
Pad[29520 -22260 29520 -7260 15000 2000 15800 "" "1" ",onsolder"]
Pin[29520 14760 15000 2000 15800 5000 "" "2" ""]
Pad[29520 7260 29520 22260 15000 2000 15800 "" "2" ""]
Pad[29520 7260 29520 22260 15000 2000 15800 "" "2" ",onsolder"]
Pin[-29520 -14760 15000 2000 15800 5000 "" "3" ""]
Pad[-29520 -22260 -29520 -7260 15000 2000 15800 "" "3" ""]
Pad[-29520 -22260 -29520 -7260 15000 2000 15800 "" "3" ",onsolder"]
Pin[-29520 14760 15000 2000 15800 5000 "" "4" ""]
Pad[-29520 7260 -29520 22260 15000 2000 15800 "" "4" ""]
Pad[-29520 7260 -29520 22260 15000 2000 15800 "" "4" ",onsolder"]
Pin[0 -14760 15000 2000 15800 5000 "" "5" ""]
Pad[0 -22260 0 -7260 15000 2000 15800 "" "5" ""]
Pad[0 -22260 0 -7260 15000 2000 15800 "" "5" ",onsolder"]
Pin[0 14760 15000 2000 15800 5000 "" "6" ""]
Pad[0 7260 0 22260 15000 2000 15800 "" "6" ""]
Pad[0 7260 0 22260 15000 2000 15800 "" "6" ",onsolder"]
)
