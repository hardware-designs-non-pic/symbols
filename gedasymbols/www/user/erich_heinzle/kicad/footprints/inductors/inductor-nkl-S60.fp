# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-S60
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-S60
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 6
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">value" "" "" 0 0 54370 13240 0 100 ""]
(
ElementLine[-100390 -59050 100390 -59050 500]
ElementLine[100390 -59050 100390 59050 500]
ElementLine[100390 59050 -100390 59050 500]
ElementLine[-100390 59050 -100390 -59050 500]
ElementLine[73740 -44290 83740 -44290 500]
ElementLine[78740 -39290 78740 -49290 500]
ElementLine[73740 44290 83740 44290 500]
ElementLine[78740 49290 78740 39290 500]
ElementLine[-83740 44290 -73740 44290 500]
ElementLine[-78740 49290 -78740 39290 500]
ElementLine[-83740 -44290 -73740 -44290 500]
ElementLine[-78740 -39290 -78740 -49290 500]
ElementLine[-109250 0 -87590 0 500]
ElementLine[-98420 10820 -98420 -10820 500]
ElementLine[87590 0 109250 0 500]
ElementLine[98420 10820 98420 -10820 500]
ElementArc[78740 -44290 3536 3536 0 360 500]
ElementArc[78740 44290 3536 3536 0 360 500]
ElementArc[-78740 44290 3536 3536 0 360 500]
ElementArc[-78740 -44290 3536 3536 0 360 500]
ElementArc[-98420 0 7651 7651 0 360 500]
ElementArc[98420 0 7651 7651 0 360 500]
Pin[59050 -44290 19680 2000 20480 5600 "" "1" ""]
Pad[59050 -54135 59050 -34445 19680 2000 20480 "" "1" ""]
Pad[59050 -54135 59050 -34445 19680 2000 20480 "" "1" ",onsolder"]
Pin[59050 44290 19680 2000 20480 5600 "" "2" ""]
Pad[59050 34445 59050 54135 19680 2000 20480 "" "2" ""]
Pad[59050 34445 59050 54135 19680 2000 20480 "" "2" ",onsolder"]
Pin[-59050 -44290 19680 2000 20480 5600 "" "3" ""]
Pad[-59050 -54135 -59050 -34445 19680 2000 20480 "" "3" ""]
Pad[-59050 -54135 -59050 -34445 19680 2000 20480 "" "3" ",onsolder"]
Pin[-59050 44290 19680 2000 20480 5600 "" "4" ""]
Pad[-59050 34445 -59050 54135 19680 2000 20480 "" "4" ""]
Pad[-59050 34445 -59050 54135 19680 2000 20480 "" "4" ",onsolder"]
Pin[0 -44290 19680 2000 20480 5600 "" "5" ""]
Pad[0 -54135 0 -34445 19680 2000 20480 "" "5" ""]
Pad[0 -54135 0 -34445 19680 2000 20480 "" "5" ",onsolder"]
Pin[0 44290 19680 2000 20480 5600 "" "6" ""]
Pad[0 34445 0 54135 19680 2000 20480 "" "6" ""]
Pad[0 34445 0 54135 19680 2000 20480 "" "6" ",onsolder"]
)
