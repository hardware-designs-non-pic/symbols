# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module L_HF12_3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: L_HF12_3
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L_HF_12_3" "" "" 0 0 10000 -20000 0 100 ""]
(
ElementLine[27500 12500 30000 -12500 1500]
ElementLine[30000 -12500 30000 0 1500]
ElementLine[0 0 0 12500 1500]
ElementLine[0 12500 2500 -12500 1500]
ElementLine[2500 -12500 2500 12500 1500]
ElementLine[2500 12500 5000 -12500 1500]
ElementLine[5000 -12500 5000 12500 1500]
ElementLine[5000 12500 7500 -12500 1500]
ElementLine[7500 -12500 7500 12500 1500]
ElementLine[7500 12500 10000 -12500 1500]
ElementLine[10000 -12500 10000 12500 1500]
ElementLine[10000 12500 12500 -12500 1500]
ElementLine[12500 -12500 12500 12500 1500]
ElementLine[12500 12500 15000 -12500 1500]
ElementLine[15000 -12500 15000 12500 1500]
ElementLine[15000 12500 17500 -12500 1500]
ElementLine[17500 -12500 17500 12500 1500]
ElementLine[17500 12500 20000 -12500 1500]
ElementLine[20000 -12500 20000 12500 1500]
ElementLine[20000 12500 22500 -12500 1500]
ElementLine[22500 -12500 22500 12500 1500]
ElementLine[22500 12500 25000 -12500 1500]
ElementLine[25000 -12500 25000 12500 1500]
ElementLine[25000 12500 27500 -12500 1500]
ElementLine[27500 -12500 27500 12500 1500]
Pin[0 0 7870 2000 8670 3200 "" "1" "square"]
Pin[30000 0 7870 2000 8670 3200 "" "2" ""]
)
