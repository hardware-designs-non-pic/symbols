# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-19
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-19
# Text descriptor count: 1
# Draw segment object count: 40
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 2500 -32500 0 100 ""]
(
ElementLine[-25590 1960 25590 1960 260]
ElementLine[25590 1960 25590 -1960 260]
ElementLine[-25590 -1960 25590 -1960 260]
ElementLine[-25590 1960 -25590 -1960 260]
ElementLine[-23620 -19680 -23620 -23620 500]
ElementLine[-23620 -23620 -23620 -31490 500]
ElementLine[-23620 -31490 -23620 -39370 500]
ElementLine[-23620 -39370 -23620 -47240 500]
ElementLine[23620 -47240 23620 -39370 500]
ElementLine[23620 -39370 23620 -31490 500]
ElementLine[23620 -31490 23620 -23620 500]
ElementLine[23620 -23620 23620 -19680 500]
ElementLine[23620 19680 23620 23620 500]
ElementLine[23620 23620 23620 31490 500]
ElementLine[23620 31490 23620 39370 500]
ElementLine[23620 39370 23620 47240 500]
ElementLine[-23620 47240 -23620 39370 500]
ElementLine[-23620 39370 -23620 31490 500]
ElementLine[-23620 31490 -23620 23620 500]
ElementLine[-23620 23620 -23620 19680 500]
ElementLine[-23620 11810 -23620 7870 500]
ElementLine[-23620 7870 -23620 -7870 500]
ElementLine[-23620 -7870 -23620 -11810 500]
ElementLine[23620 -11810 23620 -7870 500]
ElementLine[23620 -7870 23620 7870 500]
ElementLine[23620 7870 23620 11810 500]
ElementLine[-23620 -7870 23620 -7870 2400]
ElementLine[-23620 7870 23620 7870 2400]
ElementLine[23620 31490 -23620 31490 2400]
ElementLine[-23620 39370 23620 39370 2400]
ElementLine[23620 47240 -23620 47240 2400]
ElementLine[-23620 -23620 23620 -23620 2400]
ElementLine[-23620 -39370 23620 -39370 2400]
ElementLine[-23620 -47240 23620 -47240 2400]
ElementLine[-23620 -15740 -15740 -15740 2400]
ElementLine[15740 -15740 23620 -15740 2400]
ElementLine[23620 15740 15740 15740 2400]
ElementLine[-23620 15740 -15740 15740 2400]
ElementLine[-17710 -15740 17710 -15740 2400]
ElementLine[-17710 15740 17710 15740 2400]
Pin[-23620 15740 5200 2000 6000 3200 "" "1" ""]
Pad[-23620 13140 -23620 18340 5200 2000 6000 "" "1" ""]
Pad[-23620 13140 -23620 18340 5200 2000 6000 "" "1" ",onsolder"]
Pin[23620 15740 5200 2000 6000 3200 "" "2" ""]
Pad[23620 13140 23620 18340 5200 2000 6000 "" "2" ""]
Pad[23620 13140 23620 18340 5200 2000 6000 "" "2" ",onsolder"]
Pin[-23620 -15740 5200 2000 6000 3200 "" "3" ""]
Pad[-23620 -18340 -23620 -13140 5200 2000 6000 "" "3" ""]
Pad[-23620 -18340 -23620 -13140 5200 2000 6000 "" "3" ",onsolder"]
Pin[23620 -15740 5200 2000 6000 3200 "" "4" ""]
Pad[23620 -18340 23620 -13140 5200 2000 6000 "" "4" ""]
Pad[23620 -18340 23620 -13140 5200 2000 6000 "" "4" ",onsolder"]
)
