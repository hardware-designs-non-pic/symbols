# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-M1332
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-M1332
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 1250 -9500 0 100 ""]
(
ElementLine[-10620 2500 -9370 2500 260]
ElementLine[-9370 2500 -9370 -2500 260]
ElementLine[-10620 -2500 -9370 -2500 260]
ElementLine[-10620 2500 -10620 -2500 260]
ElementLine[9370 2500 10620 2500 260]
ElementLine[10620 2500 10620 -2500 260]
ElementLine[9370 -2500 10620 -2500 260]
ElementLine[9370 2500 9370 -2500 260]
ElementLine[-9440 -6290 9440 -6290 700]
ElementLine[9440 -6290 9440 -4330 700]
ElementLine[9440 -4330 9440 4330 700]
ElementLine[9440 4330 9440 6290 700]
ElementLine[9440 6290 -9440 6290 700]
ElementLine[-9440 6290 -9440 4330 700]
ElementLine[-9440 4330 -9440 -4330 700]
ElementLine[-9440 -4330 -9440 -6290 700]
Pad[-7865 0 -5895 0 7870 2000 8670 "" "1" "square"]
Pad[5895 0 7865 0 7870 2000 8670 "" "2" "square"]
)
