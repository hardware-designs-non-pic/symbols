# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS1003
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS1003
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 1500 -23230 0 100 ""]
(
ElementLine[-11810 -19680 -11810 19680 500]
ElementLine[-11810 19680 11810 19680 500]
ElementLine[11810 19680 11810 -19680 500]
ElementLine[11810 -19680 -11810 -19680 500]
ElementLine[-11810 -25590 -11810 19680 500]
ElementLine[-11810 19680 -11810 25590 500]
ElementLine[-11810 25590 11810 25590 500]
ElementLine[11810 25590 11810 19680 500]
ElementLine[11810 19680 11810 -25590 500]
ElementLine[11810 -25590 -11810 -25590 500]
Pin[-7870 14760 5600 2000 6400 1600 "" "1" ""]
Pad[-7870 11960 -7870 17560 5600 2000 6400 "" "1" ""]
Pad[-7870 11960 -7870 17560 5600 2000 6400 "" "1" ",onsolder"]
Pin[7870 14760 5600 2000 6400 1600 "" "2" ""]
Pad[7870 11960 7870 17560 5600 2000 6400 "" "2" ""]
Pad[7870 11960 7870 17560 5600 2000 6400 "" "2" ",onsolder"]
Pin[-7870 -14760 5600 2000 6400 1600 "" "3" ""]
Pad[-7870 -17560 -7870 -11960 5600 2000 6400 "" "3" ""]
Pad[-7870 -17560 -7870 -11960 5600 2000 6400 "" "3" ",onsolder"]
Pin[7870 -14760 5600 2000 6400 1600 "" "4" ""]
Pad[7870 -17560 7870 -11960 5600 2000 6400 "" "4" ""]
Pad[7870 -17560 7870 -11960 5600 2000 6400 "" "4" ",onsolder"]
Pin[-7870 -4920 5600 2000 6400 1600 "" "5" ""]
Pad[-7870 -7720 -7870 -2120 5600 2000 6400 "" "5" ""]
Pad[-7870 -7720 -7870 -2120 5600 2000 6400 "" "5" ",onsolder"]
Pin[7870 -4920 5600 2000 6400 1600 "" "6" ""]
Pad[7870 -7720 7870 -2120 5600 2000 6400 "" "6" ""]
Pad[7870 -7720 7870 -2120 5600 2000 6400 "" "6" ",onsolder"]
)
