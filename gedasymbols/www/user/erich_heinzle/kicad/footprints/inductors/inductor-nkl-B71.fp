# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B71
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B71
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 8
#
Element["" ">value" "" "" 0 0 5000 17500 0 100 ""]
(
ElementLine[25590 23620 25590 31490 500]
ElementLine[-25590 -23620 -25590 -31490 500]
ElementLine[25590 -23620 25590 -31490 500]
ElementLine[-25590 31490 -25590 23620 500]
ElementArc[0 -3930 43697 43697 0 360 500]
ElementArc[0 3930 43697 43697 0 360 500]
ElementArc[0 0 34825 34825 0 360 500]
ElementArc[0 0 34825 34825 0 360 500]
Pin[-19680 -29520 7000 2000 7800 5000 "" "1" ""]
Pad[-19680 -33020 -19680 -26020 7000 2000 7800 "" "1" ""]
Pad[-19680 -33020 -19680 -26020 7000 2000 7800 "" "1" ",onsolder"]
Pin[-19680 29520 7000 2000 7800 5000 "" "2" ""]
Pad[-19680 26020 -19680 33020 7000 2000 7800 "" "2" ""]
Pad[-19680 26020 -19680 33020 7000 2000 7800 "" "2" ",onsolder"]
Pin[19680 -29520 7000 2000 7800 5000 "" "3" ""]
Pad[19680 -33020 19680 -26020 7000 2000 7800 "" "3" ""]
Pad[19680 -33020 19680 -26020 7000 2000 7800 "" "3" ",onsolder"]
Pin[19680 29520 7000 2000 7800 5000 "" "4" ""]
Pad[19680 26020 19680 33020 7000 2000 7800 "" "4" ""]
Pad[19680 26020 19680 33020 7000 2000 7800 "" "4" ",onsolder"]
Pin[-9840 -34440 7000 2000 7800 5000 "" "5" ""]
Pad[-9840 -37940 -9840 -30940 7000 2000 7800 "" "5" ""]
Pad[-9840 -37940 -9840 -30940 7000 2000 7800 "" "5" ",onsolder"]
Pin[-9840 34440 7000 2000 7800 5000 "" "6" ""]
Pad[-9840 30940 -9840 37940 7000 2000 7800 "" "6" ""]
Pad[-9840 30940 -9840 37940 7000 2000 7800 "" "6" ",onsolder"]
Pin[9840 -34440 7000 2000 7800 5000 "" "7" ""]
Pad[9840 -37940 9840 -30940 7000 2000 7800 "" "7" ""]
Pad[9840 -37940 9840 -30940 7000 2000 7800 "" "7" ",onsolder"]
Pin[9840 34440 7000 2000 7800 5000 "" "8" ""]
Pad[9840 30940 9840 37940 7000 2000 7800 "" "8" ""]
Pad[9840 30940 9840 37940 7000 2000 7800 "" "8" ",onsolder"]
)
