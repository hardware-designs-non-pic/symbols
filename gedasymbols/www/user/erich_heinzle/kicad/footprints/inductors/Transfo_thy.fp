# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module Transfo_thy
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Transfo_thy
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "Transfo_Thy" "" "" 0 0 15000 20000 0 100 ""]
(
ElementLine[-40000 30000 -40000 40000 1500]
ElementLine[-40000 40000 30000 40000 1500]
ElementLine[30000 40000 30000 30000 1500]
ElementLine[-40000 -20000 -40000 -30000 1500]
ElementLine[-40000 -30000 30000 -30000 1500]
ElementLine[30000 -30000 30000 -20000 1500]
ElementLine[30000 30000 30000 -20000 1500]
ElementLine[-40000 30000 -40000 -20000 1500]
ElementArc[-27500 -20000 3536 3536 0 360 1500]
Pin[-20000 -15000 7200 2000 8000 3200 "" "1" "square"]
Pin[-20000 15000 7200 2000 8000 3200 "" "2" ""]
Pin[15000 15000 7200 2000 8000 3200 "" "3" ""]
Pin[20000 -15000 7200 2000 8000 3200 "" "4" ""]
)
