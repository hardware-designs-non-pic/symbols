# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS1908
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS1908
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 680 -15680 0 100 ""]
(
ElementLine[-26570 39370 -26570 -39370 500]
ElementLine[-26570 -39370 26570 -39370 500]
ElementLine[26570 -39370 26570 39370 500]
ElementLine[26570 39370 -26570 39370 500]
ElementLine[-31490 51180 -31490 -51180 500]
ElementLine[-31490 -51180 31490 -51180 500]
ElementLine[31490 -51180 31490 51180 500]
ElementLine[31490 51180 -31490 51180 500]
Pin[-19680 24600 10000 2000 10800 4000 "" "1" ""]
Pad[-19680 19600 -19680 29600 10000 2000 10800 "" "1" ""]
Pad[-19680 19600 -19680 29600 10000 2000 10800 "" "1" ",onsolder"]
Pin[19680 24600 10000 2000 10800 4000 "" "2" ""]
Pad[19680 19600 19680 29600 10000 2000 10800 "" "2" ""]
Pad[19680 19600 19680 29600 10000 2000 10800 "" "2" ",onsolder"]
Pin[-19680 -24600 10000 2000 10800 4000 "" "3" ""]
Pad[-19680 -29600 -19680 -19600 10000 2000 10800 "" "3" ""]
Pad[-19680 -29600 -19680 -19600 10000 2000 10800 "" "3" ",onsolder"]
Pin[19680 -24600 10000 2000 10800 4000 "" "4" ""]
Pad[19680 -29600 19680 -19600 10000 2000 10800 "" "4" ""]
Pad[19680 -29600 19680 -19600 10000 2000 10800 "" "4" ",onsolder"]
)
