# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-32
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-32
# Text descriptor count: 1
# Draw segment object count: 27
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 680 -41870 0 100 ""]
(
ElementLine[-27550 -62990 27550 -62990 500]
ElementLine[27550 -62990 27550 62990 500]
ElementLine[27550 62990 -27550 62990 500]
ElementLine[-27550 62990 -27550 -62990 500]
ElementLine[-35430 -66920 35430 -66920 2400]
ElementLine[-35430 -62990 35430 -62990 2400]
ElementLine[-35430 -59050 35430 -59050 2400]
ElementLine[-35430 -55110 35430 -55110 2400]
ElementLine[-35430 -51180 35430 -51180 2400]
ElementLine[-35430 -47240 35430 -47240 2400]
ElementLine[-35430 -35430 35430 -35430 2400]
ElementLine[-35430 -31490 35430 -31490 2400]
ElementLine[-35430 -23620 35430 -23620 2400]
ElementLine[-35430 -15740 35430 -15740 2400]
ElementLine[-35430 -7870 35430 -7870 2400]
ElementLine[-35430 0 35430 0 2400]
ElementLine[-35430 7870 35430 7870 2400]
ElementLine[-35430 15740 35430 15740 2400]
ElementLine[-35430 23620 35430 23620 2400]
ElementLine[-35430 31490 35430 31490 2400]
ElementLine[-35430 35430 35430 35430 2400]
ElementLine[-35430 47240 35430 47240 2400]
ElementLine[-35430 51180 35430 51180 2400]
ElementLine[-35430 55110 35430 55110 2400]
ElementLine[-35430 59050 35430 59050 2400]
ElementLine[-35430 62990 35430 62990 2400]
ElementLine[-35430 66920 35430 66920 2400]
Pin[-35430 -19680 5200 2000 6000 3200 "" "1" ""]
Pad[-35430 -22280 -35430 -17080 5200 2000 6000 "" "1" ""]
Pad[-35430 -22280 -35430 -17080 5200 2000 6000 "" "1" ",onsolder"]
Pin[35430 -19680 5200 2000 6000 3200 "" "2" ""]
Pad[35430 -22280 35430 -17080 5200 2000 6000 "" "2" ""]
Pad[35430 -22280 35430 -17080 5200 2000 6000 "" "2" ",onsolder"]
Pin[-35430 19680 5200 2000 6000 3200 "" "3" ""]
Pad[-35430 17080 -35430 22280 5200 2000 6000 "" "3" ""]
Pad[-35430 17080 -35430 22280 5200 2000 6000 "" "3" ",onsolder"]
Pin[35430 19680 5200 2000 6000 3200 "" "4" ""]
Pad[35430 17080 35430 22280 5200 2000 6000 "" "4" ""]
Pad[35430 17080 35430 22280 5200 2000 6000 "" "4" ",onsolder"]
)
