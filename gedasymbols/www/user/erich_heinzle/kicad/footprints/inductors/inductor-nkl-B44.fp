# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B44
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B44
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" ">name" "" "" 0 0 9680 -35790 0 100 ""]
(
ElementArc[0 0 61235 61235 0 360 250]
Pin[-28740 0 15000 2000 15800 5000 "" "1" ""]
Pad[-28740 -7500 -28740 7500 15000 2000 15800 "" "1" ""]
Pad[-28740 -7500 -28740 7500 15000 2000 15800 "" "1" ",onsolder"]
Pin[28740 0 15000 2000 15800 5000 "" "2" ""]
Pad[28740 -7500 28740 7500 15000 2000 15800 "" "2" ""]
Pad[28740 -7500 28740 7500 15000 2000 15800 "" "2" ",onsolder"]
Pin[68110 0 15000 2000 15800 5000 "" "3" ""]
Pad[68110 -7500 68110 7500 15000 2000 15800 "" "3" ""]
Pad[68110 -7500 68110 7500 15000 2000 15800 "" "3" ",onsolder"]
)
