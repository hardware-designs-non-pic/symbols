# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-79F
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-79F
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 7500 -11250 0 100 ""]
(
ElementLine[-10930 930 -7500 930 260]
ElementLine[-7500 930 -7500 -930 260]
ElementLine[-10930 -930 -7500 -930 260]
ElementLine[-10930 930 -10930 -930 260]
ElementLine[7500 930 10930 930 260]
ElementLine[10930 930 10930 -930 260]
ElementLine[7500 -930 10930 -930 260]
ElementLine[7500 930 7500 -930 260]
ElementLine[-3750 -5000 3750 -5000 700]
ElementLine[3750 5000 -3750 5000 700]
ElementLine[-15000 0 -11250 0 1900]
ElementLine[15000 0 11250 0 1900]
ElementLine[-7500 -1250 -7500 1250 700]
ElementLine[7500 -1250 7500 1250 700]
ElementArc[-3750 -1250 3750 3750 0 360 700]
ElementArc[3750 -1250 3750 3750 0 360 700]
ElementArc[3750 1250 3750 3750 0 360 700]
ElementArc[-3750 1250 3750 3750 0 360 700]
Pin[-15000 0 6600 2000 7400 3200 "" "1" ""]
Pad[-15000 -3300 -15000 3300 6600 2000 7400 "" "1" ""]
Pad[-15000 -3300 -15000 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[15000 0 6600 2000 7400 3200 "" "2" ""]
Pad[15000 -3300 15000 3300 6600 2000 7400 "" "2" ""]
Pad[15000 -3300 15000 3300 6600 2000 7400 "" "2" ",onsolder"]
)
