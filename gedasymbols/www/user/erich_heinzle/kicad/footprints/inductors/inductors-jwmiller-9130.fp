# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-9130
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-9130
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 0 -11250 0 100 ""]
(
ElementLine[-15930 930 -12500 930 260]
ElementLine[-12500 930 -12500 -930 260]
ElementLine[-15930 -930 -12500 -930 260]
ElementLine[-15930 930 -15930 -930 260]
ElementLine[12500 930 15930 930 260]
ElementLine[15930 930 15930 -930 260]
ElementLine[12500 -930 15930 -930 260]
ElementLine[12500 930 12500 -930 260]
ElementLine[-20000 0 -16250 0 1900]
ElementLine[20000 0 16250 0 1900]
ElementLine[-12500 -4370 12500 -4370 700]
ElementLine[12500 -4370 12500 4370 700]
ElementLine[12500 4370 -12500 4370 700]
ElementLine[-12500 4370 -12500 -4370 700]
Pin[-20000 0 6600 2000 7400 3200 "" "1" ""]
Pad[-20000 -3300 -20000 3300 6600 2000 7400 "" "1" ""]
Pad[-20000 -3300 -20000 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[20000 0 6600 2000 7400 3200 "" "2" ""]
Pad[20000 -3300 20000 3300 6600 2000 7400 "" "2" ""]
Pad[20000 -3300 20000 3300 6600 2000 7400 "" "2" ",onsolder"]
)
