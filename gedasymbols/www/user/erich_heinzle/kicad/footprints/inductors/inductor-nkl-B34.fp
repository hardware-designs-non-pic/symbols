# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B34
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B34
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">name" "" "" 0 0 5750 -35790 0 100 ""]
(
ElementLine[-7500 0 7500 0 500]
ElementLine[0 7500 0 -7500 500]
ElementArc[0 0 47320 47320 0 360 250]
ElementArc[0 0 5303 5303 0 360 500]
Pin[-44290 -29520 15000 2000 15800 5000 "" "1" ""]
Pad[-44290 -37020 -44290 -22020 15000 2000 15800 "" "1" ""]
Pad[-44290 -37020 -44290 -22020 15000 2000 15800 "" "1" ",onsolder"]
Pin[44290 -29520 15000 2000 15800 5000 "" "2" ""]
Pad[44290 -37020 44290 -22020 15000 2000 15800 "" "2" ""]
Pad[44290 -37020 44290 -22020 15000 2000 15800 "" "2" ",onsolder"]
Pin[-44290 29520 15000 2000 15800 5000 "" "3" ""]
Pad[-44290 22020 -44290 37020 15000 2000 15800 "" "3" ""]
Pad[-44290 22020 -44290 37020 15000 2000 15800 "" "3" ",onsolder"]
Pin[44290 29520 15000 2000 15800 5000 "" "4" ""]
Pad[44290 22020 44290 37020 15000 2000 15800 "" "4" ""]
Pad[44290 22020 44290 37020 15000 2000 15800 "" "4" ",onsolder"]
Pin[-24600 0 15000 2000 15800 5000 "" "5" ""]
Pad[-24600 -7500 -24600 7500 15000 2000 15800 "" "5" ""]
Pad[-24600 -7500 -24600 7500 15000 2000 15800 "" "5" ",onsolder"]
Pin[-49210 0 15000 2000 15800 5000 "" "6" ""]
Pad[-49210 -7500 -49210 7500 15000 2000 15800 "" "6" ""]
Pad[-49210 -7500 -49210 7500 15000 2000 15800 "" "6" ",onsolder"]
Pin[24600 0 15000 2000 15800 5000 "" "7" ""]
Pad[24600 -7500 24600 7500 15000 2000 15800 "" "7" ""]
Pad[24600 -7500 24600 7500 15000 2000 15800 "" "7" ",onsolder"]
Pin[49210 0 15000 2000 15800 5000 "" "8" ""]
Pad[49210 -7500 49210 7500 15000 2000 15800 "" "8" ""]
Pad[49210 -7500 49210 7500 15000 2000 15800 "" "8" ",onsolder"]
)
