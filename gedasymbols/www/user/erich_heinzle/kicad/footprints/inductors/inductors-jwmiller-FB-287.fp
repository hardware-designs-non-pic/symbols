# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-FB-287
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-FB-287
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -2500 -20000 0 100 ""]
(
ElementLine[-14760 -14560 14760 -14560 700]
ElementLine[14760 -14560 14760 14170 700]
ElementLine[14760 14170 -14760 14170 700]
ElementLine[-14760 14170 -14760 -14560 700]
ElementLine[-14560 -4330 14560 -4330 0]
ElementLine[-14560 4330 14560 4330 0]
ElementLine[-25000 0 25000 0 3200]
Pin[-25000 0 10000 2000 10800 5000 "" "1" ""]
Pad[-25000 -5000 -25000 5000 10000 2000 10800 "" "1" ""]
Pad[-25000 -5000 -25000 5000 10000 2000 10800 "" "1" ",onsolder"]
Pin[25000 0 10000 2000 10800 5000 "" "2" ""]
Pad[25000 -5000 25000 5000 10000 2000 10800 "" "2" ""]
Pad[25000 -5000 25000 5000 10000 2000 10800 "" "2" ",onsolder"]
)
