# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B42
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B42
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 8
#
Element["" "name" "" "" 0 0 -10350 -35790 0 100 ""]
(
ElementArc[0 0 58449 58449 0 360 250]
ElementArc[0 0 5558 5558 0 360 250]
Pin[59050 -39370 15000 2000 15800 3200 "" "P$1" ""]
Pad[59050 -46870 59050 -31870 15000 2000 15800 "" "P$1" ""]
Pad[59050 -46870 59050 -31870 15000 2000 15800 "" "P$1" ",onsolder"]
Pin[-59050 -39370 15000 2000 15800 3200 "" "P$2" ""]
Pad[-59050 -46870 -59050 -31870 15000 2000 15800 "" "P$2" ""]
Pad[-59050 -46870 -59050 -31870 15000 2000 15800 "" "P$2" ",onsolder"]
Pin[59050 39370 15000 2000 15800 3200 "" "P$3" ""]
Pad[59050 31870 59050 46870 15000 2000 15800 "" "P$3" ""]
Pad[59050 31870 59050 46870 15000 2000 15800 "" "P$3" ",onsolder"]
Pin[-59050 39370 15000 2000 15800 3200 "" "P$4" ""]
Pad[-59050 31870 -59050 46870 15000 2000 15800 "" "P$4" ""]
Pad[-59050 31870 -59050 46870 15000 2000 15800 "" "P$4" ",onsolder"]
Pin[29520 0 15000 2000 15800 3200 "" "P$5" ""]
Pad[29520 -7500 29520 7500 15000 2000 15800 "" "P$5" ""]
Pad[29520 -7500 29520 7500 15000 2000 15800 "" "P$5" ",onsolder"]
Pin[68890 0 15000 2000 15800 3200 "" "P$6" ""]
Pad[68890 -7500 68890 7500 15000 2000 15800 "" "P$6" ""]
Pad[68890 -7500 68890 7500 15000 2000 15800 "" "P$6" ",onsolder"]
Pin[-29520 0 15000 2000 15800 3200 "" "P$7" ""]
Pad[-29520 -7500 -29520 7500 15000 2000 15800 "" "P$7" ""]
Pad[-29520 -7500 -29520 7500 15000 2000 15800 "" "P$7" ",onsolder"]
Pin[-68890 0 15000 2000 15800 3200 "" "P$8" ""]
Pad[-68890 -7500 -68890 7500 15000 2000 15800 "" "P$8" ""]
Pad[-68890 -7500 -68890 7500 15000 2000 15800 "" "P$8" ",onsolder"]
)
