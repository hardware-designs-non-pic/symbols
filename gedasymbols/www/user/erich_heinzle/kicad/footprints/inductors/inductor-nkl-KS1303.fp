# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS1303
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS1303
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -2500 -26810 2 100 ""]
(
ElementLine[13770 21650 13770 -21650 500]
ElementLine[13770 -21650 -13770 -21650 500]
ElementLine[-13770 -21650 -13770 21650 500]
ElementLine[-13770 21650 13770 21650 500]
ElementLine[-15740 -31490 -15740 31490 500]
ElementLine[-15740 31490 15740 31490 500]
ElementLine[15740 31490 15740 -31490 500]
ElementLine[15740 -31490 -15740 -31490 500]
Pin[-9840 14760 6600 2000 7400 2400 "" "1" ""]
Pad[-9840 11460 -9840 18060 6600 2000 7400 "" "1" ""]
Pad[-9840 11460 -9840 18060 6600 2000 7400 "" "1" ",onsolder"]
Pin[9840 14760 6600 2000 7400 2400 "" "2" ""]
Pad[9840 11460 9840 18060 6600 2000 7400 "" "2" ""]
Pad[9840 11460 9840 18060 6600 2000 7400 "" "2" ",onsolder"]
Pin[-9840 -14760 6600 2000 7400 2400 "" "3" ""]
Pad[-9840 -18060 -9840 -11460 6600 2000 7400 "" "3" ""]
Pad[-9840 -18060 -9840 -11460 6600 2000 7400 "" "3" ",onsolder"]
Pin[9840 -14760 6600 2000 7400 2400 "" "4" ""]
Pad[9840 -18060 9840 -11460 6600 2000 7400 "" "4" ""]
Pad[9840 -18060 9840 -11460 6600 2000 7400 "" "4" ",onsolder"]
Pin[-9840 -4920 6600 2000 7400 2400 "" "5" ""]
Pad[-9840 -8220 -9840 -1620 6600 2000 7400 "" "5" ""]
Pad[-9840 -8220 -9840 -1620 6600 2000 7400 "" "5" ",onsolder"]
Pin[9840 -4920 6600 2000 7400 2400 "" "6" ""]
Pad[9840 -8220 9840 -1620 6600 2000 7400 "" "6" ""]
Pad[9840 -8220 9840 -1620 6600 2000 7400 "" "6" ",onsolder"]
)
