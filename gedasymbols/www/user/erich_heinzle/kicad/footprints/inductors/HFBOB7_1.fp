# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module HFBOB7_1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: HFBOB7_1
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 7
#
Element["" "HFBOB7_1" "" "" 0 0 5000 25000 0 100 ""]
(
ElementLine[-2500 2500 2500 -2500 1500]
ElementLine[-15000 -15000 15000 -15000 1500]
ElementLine[15000 -15000 15000 15000 1500]
ElementLine[15000 15000 -15000 15000 1500]
ElementLine[-15000 15000 -15000 -15000 1500]
ElementArc[0 0 5000 5000 0 360 1500]
Pin[-10000 -10000 7200 2000 8000 3200 "" "1" "square"]
Pin[-10000 10000 7200 2000 8000 3200 "" "2" ""]
Pin[10000 10000 7200 2000 8000 3200 "" "3" ""]
Pin[10000 0 7200 2000 8000 3200 "" "4" ""]
Pin[10000 -10000 7200 2000 8000 3200 "" "5" ""]
Pin[0 -15000 7200 2000 8000 3200 "" "6" ""]
Pin[0 15000 7200 2000 8000 3200 "" "7" ""]
)
