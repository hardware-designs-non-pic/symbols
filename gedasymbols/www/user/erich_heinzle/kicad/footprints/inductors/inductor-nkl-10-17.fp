# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-10-17
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-10-17
# Text descriptor count: 1
# Draw segment object count: 22
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">value" "" "" 0 0 5000 7500 0 100 ""]
(
ElementLine[0 -15740 0 -39370 2400]
ElementLine[15740 0 39370 0 2400]
ElementLine[0 15740 0 39370 2400]
ElementLine[5900 15740 11810 37400 2400]
ElementLine[5900 -15740 11810 -37400 2400]
ElementLine[9840 -13770 23620 -31490 2400]
ElementLine[9840 13770 23620 31490 2400]
ElementLine[13770 -9840 31490 -23620 2400]
ElementLine[13770 9840 31490 23620 2400]
ElementLine[15740 3930 37400 13770 2400]
ElementLine[15740 -3930 37400 -13770 2400]
ElementLine[-39370 0 -15740 0 2400]
ElementLine[-15740 -3930 -37400 -13770 2400]
ElementLine[-15740 3930 -37400 13770 2400]
ElementLine[-13770 -9840 -31490 -23620 2400]
ElementLine[-13770 9840 -31490 23620 2400]
ElementLine[-5900 -15740 -11810 -37400 2400]
ElementLine[-5900 15740 -11810 37400 2400]
ElementLine[-9840 -13770 -23620 -31490 2400]
ElementLine[-9840 13770 -23620 31490 2400]
ElementLine[-43300 0 -39370 0 2400]
ElementLine[-11810 0 -17710 -1960 2400]
ElementArc[0 0 25046 25046 0 360 250]
ElementArc[0 0 13916 13916 0 360 250]
Pin[-43300 0 5200 2000 6000 3200 "" "1" ""]
Pad[-43300 -2600 -43300 2600 5200 2000 6000 "" "1" ""]
Pad[-43300 -2600 -43300 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[-11810 0 5200 2000 6000 3200 "" "2" ""]
Pad[-11810 -2600 -11810 2600 5200 2000 6000 "" "2" ""]
Pad[-11810 -2600 -11810 2600 5200 2000 6000 "" "2" ",onsolder"]
)
