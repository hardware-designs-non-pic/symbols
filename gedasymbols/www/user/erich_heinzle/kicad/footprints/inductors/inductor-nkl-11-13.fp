# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-13
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-13
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 2500 -12500 0 100 ""]
(
ElementLine[-11810 -27550 -11810 0 500]
ElementLine[-11810 0 -11810 27550 500]
ElementLine[-11810 27550 11810 27550 500]
ElementLine[11810 27550 11810 0 500]
ElementLine[11810 0 11810 -27550 500]
ElementLine[11810 -27550 -11810 -27550 500]
ElementLine[-11810 0 11810 0 1600]
ElementLine[-15740 -5900 15740 -5900 1600]
ElementLine[-15740 -17710 15740 -17710 1600]
ElementLine[-15740 -23620 15740 -23620 1600]
ElementLine[-15740 -27550 15740 -27550 1600]
ElementLine[-15740 5900 15740 5900 1600]
ElementLine[-15740 17710 15740 17710 1600]
ElementLine[-15740 23620 15740 23620 1600]
ElementLine[-15740 27550 15740 27550 1600]
ElementLine[-15740 0 -11810 0 1600]
ElementLine[11810 0 15740 0 1600]
Pin[-15740 0 5200 2000 6000 3200 "" "1" ""]
Pad[-15740 -2600 -15740 2600 5200 2000 6000 "" "1" ""]
Pad[-15740 -2600 -15740 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[15740 0 5200 2000 6000 3200 "" "2" ""]
Pad[15740 -2600 15740 2600 5200 2000 6000 "" "2" ""]
Pad[15740 -2600 15740 2600 5200 2000 6000 "" "2" ",onsolder"]
)
