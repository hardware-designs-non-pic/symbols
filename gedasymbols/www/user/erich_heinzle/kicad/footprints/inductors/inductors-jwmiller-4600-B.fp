# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-4600-B
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-4600-B
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -31250 -18750 0 100 ""]
(
ElementLine[-57500 1250 -43750 1250 260]
ElementLine[-43750 1250 -43750 -1250 260]
ElementLine[-57500 -1250 -43750 -1250 260]
ElementLine[-57500 1250 -57500 -1250 260]
ElementLine[43750 1250 57500 1250 260]
ElementLine[57500 1250 57500 -1250 260]
ElementLine[43750 -1250 57500 -1250 260]
ElementLine[43750 1250 43750 -1250 260]
ElementLine[-43750 -11250 -43750 11250 1000]
ElementLine[-43750 11250 -37500 11250 1000]
ElementLine[-37500 11250 -37500 15000 1000]
ElementLine[-37500 15000 37500 15000 1000]
ElementLine[37500 15000 37500 11250 1000]
ElementLine[37500 11250 43750 11250 1000]
ElementLine[43750 11250 43750 -11250 1000]
ElementLine[43750 -11250 37500 -11250 1000]
ElementLine[37500 -11250 37500 -15000 1000]
ElementLine[37500 -15000 -37500 -15000 1000]
ElementLine[-37500 -15000 -37500 -11250 1000]
ElementLine[-37500 -11250 -43750 -11250 1000]
ElementLine[-63750 0 -56250 0 2500]
ElementLine[56250 0 63750 0 2500]
ElementLine[-37500 -11250 -37500 11250 700]
ElementLine[37500 -11250 37500 11250 700]
Pin[-63750 0 10000 2000 10800 4000 "" "1" ""]
Pad[-63750 -5000 -63750 5000 10000 2000 10800 "" "1" ""]
Pad[-63750 -5000 -63750 5000 10000 2000 10800 "" "1" ",onsolder"]
Pin[63750 0 10000 2000 10800 4000 "" "2" ""]
Pad[63750 -5000 63750 5000 10000 2000 10800 "" "2" ""]
Pad[63750 -5000 63750 5000 10000 2000 10800 "" "2" ",onsolder"]
)
