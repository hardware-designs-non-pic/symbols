# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-S40
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-S40
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -290 -12340 0 100 ""]
(
ElementLine[-64960 -35430 64960 -35430 500]
ElementLine[64960 -35430 64960 35430 500]
ElementLine[64960 35430 -64960 35430 500]
ElementLine[-64960 35430 -64960 -35430 500]
ElementLine[-70860 0 -55110 0 500]
ElementLine[-62990 7870 -62990 -7870 500]
ElementLine[55110 0 70860 0 500]
ElementLine[62990 7870 62990 -7870 500]
ElementArc[-62990 0 5558 5558 0 360 500]
ElementArc[62990 0 5558 5558 0 360 500]
Pin[39370 -24600 15000 2000 15800 4000 "" "1" ""]
Pad[39370 -32100 39370 -17100 15000 2000 15800 "" "1" ""]
Pad[39370 -32100 39370 -17100 15000 2000 15800 "" "1" ",onsolder"]
Pin[39370 24600 15000 2000 15800 4000 "" "2" ""]
Pad[39370 17100 39370 32100 15000 2000 15800 "" "2" ""]
Pad[39370 17100 39370 32100 15000 2000 15800 "" "2" ",onsolder"]
Pin[-39370 -24600 15000 2000 15800 4000 "" "3" ""]
Pad[-39370 -32100 -39370 -17100 15000 2000 15800 "" "3" ""]
Pad[-39370 -32100 -39370 -17100 15000 2000 15800 "" "3" ",onsolder"]
Pin[-39370 24600 15000 2000 15800 4000 "" "4" ""]
Pad[-39370 17100 -39370 32100 15000 2000 15800 "" "4" ""]
Pad[-39370 17100 -39370 32100 15000 2000 15800 "" "4" ",onsolder"]
Pin[0 -24600 15000 2000 15800 4000 "" "5" ""]
Pad[0 -32100 0 -17100 15000 2000 15800 "" "5" ""]
Pad[0 -32100 0 -17100 15000 2000 15800 "" "5" ",onsolder"]
Pin[0 24600 15000 2000 15800 4000 "" "6" ""]
Pad[0 17100 0 32100 15000 2000 15800 "" "6" ""]
Pad[0 17100 0 32100 15000 2000 15800 "" "6" ",onsolder"]
)
