# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-toko-D10F
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-toko-D10F
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -8400 -14150 2 100 ""]
(
ElementLine[-23620 19680 -16920 19680 260]
ElementLine[-16920 19680 -16920 6690 260]
ElementLine[-23620 6690 -16920 6690 260]
ElementLine[-23620 19680 -23620 6690 260]
ElementLine[16920 -6690 23620 -6690 260]
ElementLine[23620 -6690 23620 -19680 260]
ElementLine[16920 -19680 23620 -19680 260]
ElementLine[16920 -6690 16920 -19680 260]
ElementLine[-16920 19680 16920 19680 260]
ElementLine[16920 19680 16920 -19680 260]
ElementLine[-16920 -19680 16920 -19680 260]
ElementLine[-16920 19680 -16920 -19680 260]
ElementLine[-23620 -19680 -23620 19680 800]
ElementLine[23930 19680 23930 -19680 800]
ElementLine[23930 -19680 -23620 -19680 800]
ElementLine[23930 19680 -23620 19680 800]
Pad[-20270 -5705 -20270 3345 6690 2000 7490 "" "1" "square"]
Pad[20270 -3345 20270 5705 6690 2000 7490 "" "2" "square"]
)
