# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module SM0805I
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: SM0805I
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "SM0805I" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-2000 3000 -6000 3000 500]
ElementLine[-6000 3000 -6000 -3000 500]
ElementLine[-6000 -3000 -2000 -3000 500]
ElementLine[2000 -3000 6000 -3000 500]
ElementLine[6000 -3000 6000 3000 500]
ElementLine[6000 3000 2000 3000 500]
ElementArc[-6500 3000 500 500 0 360 500]
Pad[-3750 -1000 -3750 1000 3500 2000 4300 "" "1" "square"]
Pad[3750 -1000 3750 1000 3500 2000 4300 "" "2" "square"]
)
