# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-17
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-17
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 370 -12180 0 100 ""]
(
ElementLine[-11810 -35430 11810 -35430 500]
ElementLine[11810 -35430 11810 0 500]
ElementLine[11810 0 11810 35430 500]
ElementLine[11810 35430 -11810 35430 500]
ElementLine[-11810 35430 -11810 0 500]
ElementLine[-11810 0 -11810 -35430 500]
ElementLine[-11810 0 11810 0 1600]
ElementLine[-15740 -3930 15740 -3930 1600]
ElementLine[-15740 3930 15740 3930 1600]
ElementLine[-15740 7870 15740 7870 1600]
ElementLine[-15740 -7870 15740 -7870 1600]
ElementLine[-15740 -15740 15740 -15740 1600]
ElementLine[-15740 -19680 15740 -19680 1600]
ElementLine[-15740 -23620 15740 -23620 1600]
ElementLine[-15740 -27550 15740 -27550 1600]
ElementLine[-15740 -31490 15740 -31490 1600]
ElementLine[-15740 -35430 15740 -35430 1600]
ElementLine[-15740 15740 15740 15740 1600]
ElementLine[-15740 19680 15740 19680 1600]
ElementLine[-15740 23620 15740 23620 1600]
ElementLine[-15740 27550 15740 27550 1600]
ElementLine[-15740 31490 15740 31490 1600]
ElementLine[-15740 35430 15740 35430 1600]
ElementLine[-15740 0 -11810 0 1600]
ElementLine[11810 0 15740 0 1600]
Pin[-15740 0 5200 2000 6000 3200 "" "1" ""]
Pad[-15740 -2600 -15740 2600 5200 2000 6000 "" "1" ""]
Pad[-15740 -2600 -15740 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[15740 0 5200 2000 6000 3200 "" "2" ""]
Pad[15740 -2600 15740 2600 5200 2000 6000 "" "2" ""]
Pad[15740 -2600 15740 2600 5200 2000 6000 "" "2" ",onsolder"]
)
