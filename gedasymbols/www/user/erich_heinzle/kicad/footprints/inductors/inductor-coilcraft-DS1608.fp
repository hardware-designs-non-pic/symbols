# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-coilcraft-DS1608
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-coilcraft-DS1608
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">Name" "" "" 0 0 1000 -3600 0 100 ""]
(
ElementLine[10500 0 10500 2500 0]
ElementLine[10500 -2500 10500 2500 1000]
ElementLine[10500 2500 6800 8800 1000]
ElementLine[6800 8800 -6800 8800 1000]
ElementLine[-6800 8800 -10500 2500 1000]
ElementLine[-10500 2500 -10500 -2500 1000]
ElementLine[-10500 -2500 -6800 -8800 1000]
ElementLine[-6800 -8800 6800 -8800 1000]
ElementLine[6800 -8800 10500 -2500 1000]
ElementLine[-8500 5800 -8500 -5800 1000]
ElementLine[8500 -5800 8500 5800 1000]
ElementArc[0 0 5657 5657 0 360 500]
Pad[-10800 -4250 -10800 4250 5500 2000 6300 "" "P$1" "square"]
Pad[10800 -4250 10800 4250 5500 2000 6300 "" "P$2" "square"]
)
