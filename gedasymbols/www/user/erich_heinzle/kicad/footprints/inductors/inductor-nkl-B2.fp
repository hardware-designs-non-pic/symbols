# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B2
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 -4770 -7890 0 100 ""]
(
ElementLine[-43300 -19680 -31490 -19680 500]
ElementLine[-31490 -19680 -27550 -24600 500]
ElementLine[-27550 -24600 -21650 -24600 500]
ElementLine[-21650 -24600 -17710 -19680 500]
ElementLine[-17710 -19680 17710 -19680 500]
ElementLine[17710 -19680 21650 -24600 500]
ElementLine[21650 -24600 27550 -24600 500]
ElementLine[27550 -24600 31490 -19680 500]
ElementLine[31490 -19680 43300 -19680 500]
ElementLine[43300 -19680 43300 19680 500]
ElementLine[43300 19680 31490 19680 500]
ElementLine[31490 19680 27550 24600 500]
ElementLine[27550 24600 21650 24600 500]
ElementLine[21650 24600 17710 19680 500]
ElementLine[17710 19680 -17710 19680 500]
ElementLine[-17710 19680 -21650 24600 500]
ElementLine[-21650 24600 -27550 24600 500]
ElementLine[-27550 24600 -31490 19680 500]
ElementLine[-31490 19680 -43300 19680 500]
ElementLine[-43300 19680 -43300 -19680 500]
Pin[-24600 -19680 15000 2000 15800 5000 "" "1" ""]
Pad[-24600 -27180 -24600 -12180 15000 2000 15800 "" "1" ""]
Pad[-24600 -27180 -24600 -12180 15000 2000 15800 "" "1" ",onsolder"]
Pin[-24600 19680 15000 2000 15800 5000 "" "2" ""]
Pad[-24600 12180 -24600 27180 15000 2000 15800 "" "2" ""]
Pad[-24600 12180 -24600 27180 15000 2000 15800 "" "2" ",onsolder"]
Pin[24600 -19680 15000 2000 15800 5000 "" "3" ""]
Pad[24600 -27180 24600 -12180 15000 2000 15800 "" "3" ""]
Pad[24600 -27180 24600 -12180 15000 2000 15800 "" "3" ",onsolder"]
Pin[24600 19680 15000 2000 15800 5000 "" "4" ""]
Pad[24600 12180 24600 27180 15000 2000 15800 "" "4" ""]
Pad[24600 12180 24600 27180 15000 2000 15800 "" "4" ",onsolder"]
)
