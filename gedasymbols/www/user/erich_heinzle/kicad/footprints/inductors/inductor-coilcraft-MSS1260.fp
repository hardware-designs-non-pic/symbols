# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-coilcraft-MSS1260
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-coilcraft-MSS1260
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 1
# Draw arc object count: 12
# Pad count: 2
#
Element["" ">Value" "" "" 0 0 3180 5370 0 100 ""]
(
ElementLine[-16330 -24210 16330 -24210 0]
ElementLine[24210 -16330 24210 16330 800]
ElementLine[16330 24210 -16330 24210 0]
ElementLine[-24210 16330 -24210 -16330 0]
ElementLine[-16330 -24010 16330 -24010 800]
ElementLine[24010 -16330 24010 16330 500]
ElementLine[16330 24010 -16330 24010 800]
ElementLine[-24010 16330 -24010 -16330 800]
ElementLine[-25590 -25590 25590 -25590 1000]
ElementLine[25590 -25590 25590 25590 1000]
ElementLine[25590 25590 -25590 25590 1000]
ElementLine[-25590 25590 -25590 -25590 1000]
ElementArc[0 0 12516 12516 0 360 400]
ElementArc[0 90 20975 20975 0 360 800]
ElementArc[0 90 20949 20949 0 360 800]
ElementArc[-90 0 20949 20949 0 360 800]
ElementArc[0 -90 20949 20949 0 360 800]
ElementArc[-16330 -16330 5060 5060 0 360 800]
ElementArc[-16330 16330 5060 5060 0 360 800]
ElementArc[16330 16330 5060 5060 0 360 800]
ElementArc[16330 -16370 5050 5050 0 360 800]
ElementArc[-16330 -16330 7680 7680 0 360 800]
ElementArc[16330 -16330 7680 7680 0 360 800]
ElementArc[16330 16330 7680 7680 0 360 800]
ElementArc[-16330 16330 7680 7680 0 360 800]
Pad[-2955 -16730 2955 -16730 15740 2000 16540 "" "P$1" "square"]
Pad[-2955 16730 2955 16730 15740 2000 16540 "" "P$2" "square"]
)
