# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module ALTNC_S
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ALTNC_S
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "ALTNC_S" "" "" 0 0 0 25000 0 100 ""]
(
ElementLine[20000 20000 25000 0 1500]
ElementLine[10000 20000 20000 -20000 1500]
ElementLine[0 20000 10000 -20000 1500]
ElementLine[-10000 20000 0 -20000 1500]
ElementLine[-20000 20000 -10000 -20000 1500]
ElementLine[-30000 20000 -20000 -20000 1500]
ElementLine[25000 20000 25000 -20000 1500]
ElementLine[25000 -20000 -30000 -20000 1500]
ElementLine[-30000 -20000 -30000 20000 1500]
ElementLine[-30000 20000 25000 20000 1500]
Pin[-10000 -10000 12760 2000 13560 3200 "" "1" "square"]
Pin[10000 10000 12760 2000 13560 3200 "" "2" ""]
)
