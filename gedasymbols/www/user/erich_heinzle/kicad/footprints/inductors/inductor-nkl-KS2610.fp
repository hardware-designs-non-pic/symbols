# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS2610
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS2610
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 2180 -22480 0 100 ""]
(
ElementLine[-33460 43300 -33460 -43300 500]
ElementLine[-33460 -43300 33460 -43300 500]
ElementLine[33460 -43300 33460 43300 500]
ElementLine[33460 43300 -33460 43300 500]
ElementLine[-37400 66920 -37400 -66920 500]
ElementLine[-37400 -66920 37400 -66920 500]
ElementLine[37400 -66920 37400 66920 500]
ElementLine[37400 66920 -37400 66920 500]
Pin[-24600 29520 15000 2000 15800 5000 "" "1" ""]
Pad[-24600 22020 -24600 37020 15000 2000 15800 "" "1" ""]
Pad[-24600 22020 -24600 37020 15000 2000 15800 "" "1" ",onsolder"]
Pin[24600 29520 15000 2000 15800 5000 "" "2" ""]
Pad[24600 22020 24600 37020 15000 2000 15800 "" "2" ""]
Pad[24600 22020 24600 37020 15000 2000 15800 "" "2" ",onsolder"]
Pin[-24600 -29520 15000 2000 15800 5000 "" "3" ""]
Pad[-24600 -37020 -24600 -22020 15000 2000 15800 "" "3" ""]
Pad[-24600 -37020 -24600 -22020 15000 2000 15800 "" "3" ",onsolder"]
Pin[24600 -29520 15000 2000 15800 5000 "" "4" ""]
Pad[24600 -37020 24600 -22020 15000 2000 15800 "" "4" ""]
Pad[24600 -37020 24600 -22020 15000 2000 15800 "" "4" ",onsolder"]
)
