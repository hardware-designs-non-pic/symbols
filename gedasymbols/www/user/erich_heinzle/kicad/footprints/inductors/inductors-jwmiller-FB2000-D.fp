# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-FB2000-D
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-FB2000-D
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 7
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -17500 0 100 ""]
(
ElementLine[-5000 5000 0 -7500 1000]
ElementLine[0 7500 5000 -5000 1000]
ElementArc[0 0 8839 8839 0 360 350]
ElementArc[0 -7500 877 877 0 360 500]
ElementArc[-5000 -5000 877 877 0 360 500]
ElementArc[5000 -5000 877 877 0 360 500]
ElementArc[0 7500 877 877 0 360 500]
ElementArc[5000 5000 877 877 0 360 500]
ElementArc[-5000 5000 877 877 0 360 500]
Pin[-5000 -5000 6600 2000 7400 3200 "" "1" ""]
Pad[-5000 -8300 -5000 -1700 6600 2000 7400 "" "1" ""]
Pad[-5000 -8300 -5000 -1700 6600 2000 7400 "" "1" ",onsolder"]
Pin[5000 5000 6600 2000 7400 3200 "" "2" ""]
Pad[5000 1700 5000 8300 6600 2000 7400 "" "2" ""]
Pad[5000 1700 5000 8300 6600 2000 7400 "" "2" ",onsolder"]
)
