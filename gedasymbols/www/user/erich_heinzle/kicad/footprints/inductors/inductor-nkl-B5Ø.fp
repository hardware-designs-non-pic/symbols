# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B5Ø
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B5Ø
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 12
#
Element["" ">name" "" "" 0 0 7720 -59410 0 100 ""]
(
ElementArc[0 0 69593 69593 0 360 250]
Pin[59050 -59050 15000 2000 15800 5600 "" "1" ""]
Pad[59050 -66550 59050 -51550 15000 2000 15800 "" "1" ""]
Pad[59050 -66550 59050 -51550 15000 2000 15800 "" "1" ",onsolder"]
Pin[-59050 -59050 15000 2000 15800 5600 "" "2" ""]
Pad[-59050 -66550 -59050 -51550 15000 2000 15800 "" "2" ""]
Pad[-59050 -66550 -59050 -51550 15000 2000 15800 "" "2" ",onsolder"]
Pin[59050 59050 15000 2000 15800 5600 "" "3" ""]
Pad[59050 51550 59050 66550 15000 2000 15800 "" "3" ""]
Pad[59050 51550 59050 66550 15000 2000 15800 "" "3" ",onsolder"]
Pin[-59050 59050 15000 2000 15800 5000 "" "4" ""]
Pad[-59050 51550 -59050 66550 15000 2000 15800 "" "4" ""]
Pad[-59050 51550 -59050 66550 15000 2000 15800 "" "4" ",onsolder"]
Pin[19680 -78740 15000 2000 15800 5600 "" "5" ""]
Pad[19680 -86240 19680 -71240 15000 2000 15800 "" "5" ""]
Pad[19680 -86240 19680 -71240 15000 2000 15800 "" "5" ",onsolder"]
Pin[-19680 -78740 15000 2000 15800 5600 "" "6" ""]
Pad[-19680 -86240 -19680 -71240 15000 2000 15800 "" "6" ""]
Pad[-19680 -86240 -19680 -71240 15000 2000 15800 "" "6" ",onsolder"]
Pin[19680 78740 15000 2000 15800 5600 "" "7" ""]
Pad[19680 71240 19680 86240 15000 2000 15800 "" "7" ""]
Pad[19680 71240 19680 86240 15000 2000 15800 "" "7" ",onsolder"]
Pin[-19680 78740 15000 2000 15800 5600 "" "8" ""]
Pad[-19680 71240 -19680 86240 15000 2000 15800 "" "8" ""]
Pad[-19680 71240 -19680 86240 15000 2000 15800 "" "8" ",onsolder"]
Pin[19680 -39370 15000 2000 15800 5600 "" "9" ""]
Pad[19680 -46870 19680 -31870 15000 2000 15800 "" "9" ""]
Pad[19680 -46870 19680 -31870 15000 2000 15800 "" "9" ",onsolder"]
Pin[-19680 -39370 15000 2000 15800 5600 "" "10" ""]
Pad[-19680 -46870 -19680 -31870 15000 2000 15800 "" "10" ""]
Pad[-19680 -46870 -19680 -31870 15000 2000 15800 "" "10" ",onsolder"]
Pin[19680 19680 15000 2000 15800 5600 "" "11" ""]
Pad[19680 12180 19680 27180 15000 2000 15800 "" "11" ""]
Pad[19680 12180 19680 27180 15000 2000 15800 "" "11" ",onsolder"]
Pin[-19680 19680 15000 2000 15800 5000 "" "12" ""]
Pad[-19680 12180 -19680 27180 15000 2000 15800 "" "12" ""]
Pad[-19680 12180 -19680 27180 15000 2000 15800 "" "12" ",onsolder"]
)
