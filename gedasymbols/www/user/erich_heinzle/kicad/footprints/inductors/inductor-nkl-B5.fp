# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B5
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">value" "" "" 0 0 -900 7500 0 100 ""]
(
ElementLine[-25590 -11810 25590 -11810 500]
ElementLine[25590 -11810 25590 11810 500]
ElementLine[25590 11810 -25590 11810 500]
ElementLine[-25590 11810 -25590 -11810 500]
Pin[-19680 -5900 7500 2000 8300 5000 "" "1" ""]
Pad[-19680 -9650 -19680 -2150 7500 2000 8300 "" "1" ""]
Pad[-19680 -9650 -19680 -2150 7500 2000 8300 "" "1" ",onsolder"]
Pin[-19680 5900 7500 2000 8300 5000 "" "2" ""]
Pad[-19680 2150 -19680 9650 7500 2000 8300 "" "2" ""]
Pad[-19680 2150 -19680 9650 7500 2000 8300 "" "2" ",onsolder"]
Pin[19680 -5900 7500 2000 8300 5000 "" "3" ""]
Pad[19680 -9650 19680 -2150 7500 2000 8300 "" "3" ""]
Pad[19680 -9650 19680 -2150 7500 2000 8300 "" "3" ",onsolder"]
Pin[19680 5900 7500 2000 8300 5000 "" "4" ""]
Pad[19680 2150 19680 9650 7500 2000 8300 "" "4" ""]
Pad[19680 2150 19680 9650 7500 2000 8300 "" "4" ",onsolder"]
)
