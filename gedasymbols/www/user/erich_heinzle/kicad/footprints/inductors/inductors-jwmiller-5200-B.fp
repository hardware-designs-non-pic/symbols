# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-5200-B
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-5200-B
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -32500 -26250 0 100 ""]
(
ElementLine[-58750 1250 -45000 1250 260]
ElementLine[-45000 1250 -45000 -1250 260]
ElementLine[-58750 -1250 -45000 -1250 260]
ElementLine[-58750 1250 -58750 -1250 260]
ElementLine[45000 1250 58750 1250 260]
ElementLine[58750 1250 58750 -1250 260]
ElementLine[45000 -1250 58750 -1250 260]
ElementLine[45000 1250 45000 -1250 260]
ElementLine[-45000 -17500 -45000 17500 1000]
ElementLine[-45000 17500 -32500 17500 1000]
ElementLine[-32500 17500 -32500 20000 1000]
ElementLine[-32500 20000 32500 20000 1000]
ElementLine[32500 20000 32500 17500 1000]
ElementLine[32500 17500 45000 17500 1000]
ElementLine[45000 17500 45000 -17500 1000]
ElementLine[45000 -17500 32500 -17500 1000]
ElementLine[32500 -17500 32500 -20000 1000]
ElementLine[32500 -20000 -32500 -20000 1000]
ElementLine[-32500 -20000 -32500 -17500 1000]
ElementLine[-32500 -17500 -45000 -17500 1000]
ElementLine[-65000 0 -57500 0 2500]
ElementLine[57500 0 65000 0 2500]
ElementLine[-32500 -17500 -32500 17500 700]
ElementLine[32500 -17500 32500 17500 700]
Pin[-65000 0 10000 2000 10800 4000 "" "1" ""]
Pad[-65000 -5000 -65000 5000 10000 2000 10800 "" "1" ""]
Pad[-65000 -5000 -65000 5000 10000 2000 10800 "" "1" ",onsolder"]
Pin[65000 0 10000 2000 10800 4000 "" "2" ""]
Pad[65000 -5000 65000 5000 10000 2000 10800 "" "2" ""]
Pad[65000 -5000 65000 5000 10000 2000 10800 "" "2" ",onsolder"]
)
