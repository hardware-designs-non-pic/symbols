# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module IND_CMS
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: IND_CMS
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "IND_CMS" "" "" 0 0 -2940 0 1 100 ""]
(
ElementLine[6000 8000 12500 8000 500]
ElementLine[12500 8000 12500 -8000 500]
ElementLine[12500 -8000 6000 -8000 500]
ElementLine[-6000 -8000 -12500 -8000 500]
ElementLine[-12500 -8000 -12500 8000 500]
ElementLine[-12500 8000 -6000 8000 500]
Pad[-9000 -5750 -9000 5750 8500 2000 9300 "" "1" "square"]
Pad[9000 -5750 9000 5750 8500 2000 9300 "" "2" "square"]
)
