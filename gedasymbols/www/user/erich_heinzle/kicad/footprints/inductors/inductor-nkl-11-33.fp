# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-33
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-33
# Text descriptor count: 1
# Draw segment object count: 28
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 680 -37930 0 100 ""]
(
ElementLine[-23620 -66920 23620 -66920 500]
ElementLine[23620 -66920 23620 62990 500]
ElementLine[23620 62990 23620 66920 500]
ElementLine[23620 66920 -23620 66920 500]
ElementLine[-23620 66920 -23620 -66920 500]
ElementLine[-27550 0 27550 0 2400]
ElementLine[-27550 -7870 27550 -7870 2400]
ElementLine[-27550 -15740 27550 -15740 2400]
ElementLine[-27550 -23620 27550 -23620 2400]
ElementLine[-27550 -31490 27550 -31490 2400]
ElementLine[-27550 -43300 27550 -43300 2400]
ElementLine[-27550 -51180 27550 -51180 2400]
ElementLine[-27550 -55110 27550 -55110 2400]
ElementLine[-27550 -59050 27550 -59050 2400]
ElementLine[-27550 -62990 27550 -62990 2400]
ElementLine[-27550 -66920 27550 -66920 2400]
ElementLine[-27550 -70860 27550 -70860 2400]
ElementLine[-27550 7870 27550 7870 2400]
ElementLine[-27550 15740 27550 15740 2400]
ElementLine[-27550 23620 27550 23620 2400]
ElementLine[-27550 31490 27550 31490 2400]
ElementLine[-27550 43300 27550 43300 2400]
ElementLine[-27550 51180 27550 51180 2400]
ElementLine[-27550 55110 27550 55110 2400]
ElementLine[-27550 59050 27550 59050 2400]
ElementLine[-27550 66920 27550 66920 2400]
ElementLine[-27550 70860 27550 70860 2400]
ElementLine[-27550 62990 27550 62990 2400]
Pin[-35430 0 6000 2000 6800 4000 "" "1" ""]
Pad[-35430 -3000 -35430 3000 6000 2000 6800 "" "1" ""]
Pad[-35430 -3000 -35430 3000 6000 2000 6800 "" "1" ",onsolder"]
Pin[35430 0 6000 2000 6800 4000 "" "2" ""]
Pad[35430 -3000 35430 3000 6000 2000 6800 "" "2" ""]
Pad[35430 -3000 35430 3000 6000 2000 6800 "" "2" ",onsolder"]
)
