# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-FB-085
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-FB-085
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 7500 -7500 0 100 ""]
(
ElementLine[-4330 -2750 4330 -2750 700]
ElementLine[4330 -2750 4330 2750 700]
ElementLine[4330 2750 -4330 2750 700]
ElementLine[-4330 2750 -4330 -2750 700]
ElementLine[4330 -1180 -4330 -1180 0]
ElementLine[4330 1180 -4330 1180 0]
ElementLine[-8750 0 8750 0 1600]
Pin[-8750 0 6000 2000 6800 3200 "" "1" ""]
Pad[-8750 -3000 -8750 3000 6000 2000 6800 "" "1" ""]
Pad[-8750 -3000 -8750 3000 6000 2000 6800 "" "1" ",onsolder"]
Pin[8750 0 6000 2000 6800 3200 "" "2" ""]
Pad[8750 -3000 8750 3000 6000 2000 6800 "" "2" ""]
Pad[8750 -3000 8750 3000 6000 2000 6800 "" "2" ",onsolder"]
)
