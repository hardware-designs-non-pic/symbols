# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-M1331
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-M1331
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -8250 0 100 ""]
(
ElementLine[-8750 2500 -7500 2500 260]
ElementLine[-7500 2500 -7500 -2500 260]
ElementLine[-8750 -2500 -7500 -2500 260]
ElementLine[-8750 2500 -8750 -2500 260]
ElementLine[7500 2500 8750 2500 260]
ElementLine[8750 2500 8750 -2500 260]
ElementLine[7500 -2500 8750 -2500 260]
ElementLine[7500 2500 7500 -2500 260]
ElementLine[-7480 -5110 7480 -5110 700]
ElementLine[7480 -5110 7480 -4330 700]
ElementLine[7480 -4330 7480 4330 700]
ElementLine[7480 4330 7480 5110 700]
ElementLine[7480 5110 -7480 5110 700]
ElementLine[-7480 5110 -7480 4330 700]
ElementLine[-7480 4330 -7480 -4330 700]
ElementLine[-7480 -4330 -7480 -5110 700]
Pad[-6295 0 -5505 0 7870 2000 8670 "" "1" "square"]
Pad[5505 0 6295 0 7870 2000 8670 "" "2" "square"]
)
