# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-coilcraft-DO1606
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-coilcraft-DO1606
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 1
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">Name" "" "" 0 0 -2500 -12500 0 100 ""]
(
ElementLine[-12000 -4000 -12500 -4000 800]
ElementLine[-12500 -4000 -12500 4000 800]
ElementLine[-12500 4000 -12000 4000 800]
ElementLine[12000 -4000 12500 -4000 800]
ElementLine[12500 -4000 12500 4000 800]
ElementLine[12500 4000 12000 4000 800]
ElementArc[0 0 7425 7425 0 360 400]
ElementArc[-11960 -8000 3996 3996 0 360 800]
ElementArc[11820 -7750 3754 3754 0 360 800]
ElementArc[12000 7750 3750 3750 0 360 800]
ElementArc[-12000 7750 3750 3750 0 360 800]
Pad[-11500 -2500 -11500 2500 4000 2000 4800 "" "P$1" "square"]
Pad[11500 -2500 11500 2500 4000 2000 4800 "" "P$2" "square"]
)
