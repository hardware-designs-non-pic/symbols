# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B2814
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B2814
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -3240 -26120 0 100 ""]
(
ElementLine[-5900 0 5900 0 500]
ElementLine[0 5900 0 -5900 500]
ElementArc[0 0 38962 38962 0 360 250]
ElementArc[0 0 4172 4172 0 360 500]
Pin[-29520 39370 15000 2000 15800 3200 "" "P$1" ""]
Pad[-29520 31870 -29520 46870 15000 2000 15800 "" "P$1" ""]
Pad[-29520 31870 -29520 46870 15000 2000 15800 "" "P$1" ",onsolder"]
Pin[-29520 -39370 15000 2000 15800 3200 "" "P$2" ""]
Pad[-29520 -46870 -29520 -31870 15000 2000 15800 "" "P$2" ""]
Pad[-29520 -46870 -29520 -31870 15000 2000 15800 "" "P$2" ",onsolder"]
Pin[29520 39370 15000 2000 15800 3200 "" "P$3" ""]
Pad[29520 31870 29520 46870 15000 2000 15800 "" "P$3" ""]
Pad[29520 31870 29520 46870 15000 2000 15800 "" "P$3" ",onsolder"]
Pin[29520 -39370 15000 2000 15800 3200 "" "P$4" ""]
Pad[29520 -46870 29520 -31870 15000 2000 15800 "" "P$4" ""]
Pad[29520 -46870 29520 -31870 15000 2000 15800 "" "P$4" ",onsolder"]
Pin[0 49210 15000 2000 15800 3200 "" "P$5" ""]
Pad[0 41710 0 56710 15000 2000 15800 "" "P$5" ""]
Pad[0 41710 0 56710 15000 2000 15800 "" "P$5" ",onsolder"]
Pin[0 -49210 15000 2000 15800 3200 "" "P$6" ""]
Pad[0 -56710 0 -41710 15000 2000 15800 "" "P$6" ""]
Pad[0 -56710 0 -41710 15000 2000 15800 "" "P$6" ",onsolder"]
)
