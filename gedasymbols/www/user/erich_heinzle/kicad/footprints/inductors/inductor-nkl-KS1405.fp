# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS1405
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS1405
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 1430 -28010 0 100 ""]
(
ElementLine[13770 -19680 13770 19680 500]
ElementLine[13770 19680 -13770 19680 500]
ElementLine[-13770 19680 -13770 -19680 500]
ElementLine[-13770 -19680 13770 -19680 500]
ElementLine[-15740 -35430 15740 -35430 500]
ElementLine[15740 -35430 15740 35430 500]
ElementLine[15740 35430 -15740 35430 500]
ElementLine[-15740 35430 -15740 -35430 500]
Pin[-9840 14760 7600 2000 8400 3200 "" "1" ""]
Pad[-9840 10960 -9840 18560 7600 2000 8400 "" "1" ""]
Pad[-9840 10960 -9840 18560 7600 2000 8400 "" "1" ",onsolder"]
Pin[9840 14760 7600 2000 8400 3200 "" "2" ""]
Pad[9840 10960 9840 18560 7600 2000 8400 "" "2" ""]
Pad[9840 10960 9840 18560 7600 2000 8400 "" "2" ",onsolder"]
Pin[-9840 -14760 7600 2000 8400 3200 "" "3" ""]
Pad[-9840 -18560 -9840 -10960 7600 2000 8400 "" "3" ""]
Pad[-9840 -18560 -9840 -10960 7600 2000 8400 "" "3" ",onsolder"]
Pin[9840 -14760 7600 2000 8400 3200 "" "4" ""]
Pad[9840 -18560 9840 -10960 7600 2000 8400 "" "4" ""]
Pad[9840 -18560 9840 -10960 7600 2000 8400 "" "4" ",onsolder"]
Pin[-9840 -4920 7600 2000 8400 3200 "" "5" ""]
Pad[-9840 -8720 -9840 -1120 7600 2000 8400 "" "5" ""]
Pad[-9840 -8720 -9840 -1120 7600 2000 8400 "" "5" ",onsolder"]
Pin[9840 -4920 7600 2000 8400 3200 "" "6" ""]
Pad[9840 -8720 9840 -1120 7600 2000 8400 "" "6" ""]
Pad[9840 -8720 9840 -1120 7600 2000 8400 "" "6" ",onsolder"]
)
