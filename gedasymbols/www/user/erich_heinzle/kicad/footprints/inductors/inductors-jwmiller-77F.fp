# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-77F
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-77F
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -15000 0 100 ""]
(
ElementLine[-23430 930 -20000 930 260]
ElementLine[-20000 930 -20000 -930 260]
ElementLine[-23430 -930 -20000 -930 260]
ElementLine[-23430 930 -23430 -930 260]
ElementLine[20000 930 23430 930 260]
ElementLine[23430 930 23430 -930 260]
ElementLine[20000 -930 23430 -930 260]
ElementLine[20000 930 20000 -930 260]
ElementLine[-13750 -8750 13750 -8750 700]
ElementLine[13750 8750 -13750 8750 700]
ElementLine[-27500 0 -23750 0 1900]
ElementLine[27500 0 23750 0 1900]
ElementArc[-12500 -1250 7603 7603 0 360 700]
ElementArc[-12500 1250 7603 7603 0 360 700]
ElementArc[12500 1250 7603 7603 0 360 700]
ElementArc[12500 -1250 7603 7603 0 360 700]
Pin[-27500 0 7600 2000 8400 3600 "" "1" ""]
Pad[-27500 -3800 -27500 3800 7600 2000 8400 "" "1" ""]
Pad[-27500 -3800 -27500 3800 7600 2000 8400 "" "1" ",onsolder"]
Pin[27500 0 7600 2000 8400 3600 "" "2" ""]
Pad[27500 -3800 27500 3800 7600 2000 8400 "" "2" ""]
Pad[27500 -3800 27500 3800 7600 2000 8400 "" "2" ",onsolder"]
)
