# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-S30
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-S30
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -29820 -6430 0 100 ""]
(
ElementLine[-51180 -25590 51180 -25590 500]
ElementLine[51180 -25590 51180 25590 500]
ElementLine[51180 25590 -51180 25590 500]
ElementLine[-51180 25590 -51180 -25590 500]
ElementLine[-57080 0 -41330 0 500]
ElementLine[-49210 7870 -49210 -7870 500]
ElementLine[41330 0 57080 0 500]
ElementLine[49210 7870 49210 -7870 500]
ElementArc[-49210 0 5558 5558 0 360 500]
ElementArc[49210 0 5558 5558 0 360 500]
Pin[29520 -14760 10000 2000 10800 3200 "" "1" ""]
Pad[29520 -19760 29520 -9760 10000 2000 10800 "" "1" ""]
Pad[29520 -19760 29520 -9760 10000 2000 10800 "" "1" ",onsolder"]
Pin[29520 14760 10000 2000 10800 3200 "" "2" ""]
Pad[29520 9760 29520 19760 10000 2000 10800 "" "2" ""]
Pad[29520 9760 29520 19760 10000 2000 10800 "" "2" ",onsolder"]
Pin[-29520 -14760 10000 2000 10800 3200 "" "3" ""]
Pad[-29520 -19760 -29520 -9760 10000 2000 10800 "" "3" ""]
Pad[-29520 -19760 -29520 -9760 10000 2000 10800 "" "3" ",onsolder"]
Pin[-29520 14760 10000 2000 10800 3200 "" "4" ""]
Pad[-29520 9760 -29520 19760 10000 2000 10800 "" "4" ""]
Pad[-29520 9760 -29520 19760 10000 2000 10800 "" "4" ",onsolder"]
Pin[0 -14760 10000 2000 10800 3200 "" "5" ""]
Pad[0 -19760 0 -9760 10000 2000 10800 "" "5" ""]
Pad[0 -19760 0 -9760 10000 2000 10800 "" "5" ",onsolder"]
Pin[0 14760 10000 2000 10800 3200 "" "6" ""]
Pad[0 9760 0 19760 10000 2000 10800 "" "6" ""]
Pad[0 9760 0 19760 10000 2000 10800 "" "6" ",onsolder"]
)
