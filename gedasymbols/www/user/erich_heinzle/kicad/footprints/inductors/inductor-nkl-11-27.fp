# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-27
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-27
# Text descriptor count: 1
# Draw segment object count: 27
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 680 -30050 0 100 ""]
(
ElementLine[-23620 -55110 23620 -55110 500]
ElementLine[23620 -55110 23620 0 500]
ElementLine[23620 0 23620 55110 500]
ElementLine[23620 55110 -23620 55110 500]
ElementLine[-23620 55110 -23620 0 500]
ElementLine[-23620 0 -23620 -55110 500]
ElementLine[-31490 -7870 31490 -7870 2400]
ElementLine[-31490 -15740 31490 -15740 2400]
ElementLine[-31490 -35430 31490 -35430 2400]
ElementLine[-31490 -43300 31490 -43300 2400]
ElementLine[-31490 -47240 31490 -47240 2400]
ElementLine[-31490 -51180 31490 -51180 2400]
ElementLine[-31490 -55110 31490 -55110 2400]
ElementLine[-31490 -23620 31490 -23620 2400]
ElementLine[-31490 7870 31490 7870 2400]
ElementLine[-31490 15740 31490 15740 2400]
ElementLine[-31490 23620 31490 23620 2400]
ElementLine[-31490 35430 31490 35430 2400]
ElementLine[-31490 43300 31490 43300 2400]
ElementLine[-31490 47240 31490 47240 2400]
ElementLine[-31490 51180 31490 51180 2400]
ElementLine[-31490 55110 31490 55110 2400]
ElementLine[-23620 0 23620 0 2400]
ElementLine[-31490 0 -23620 0 2400]
ElementLine[23620 0 31490 0 2400]
ElementLine[-31490 -59050 31490 -59050 2400]
ElementLine[-31490 59050 31490 59050 2400]
Pin[-31490 0 5200 2000 6000 3200 "" "1" ""]
Pad[-31490 -2600 -31490 2600 5200 2000 6000 "" "1" ""]
Pad[-31490 -2600 -31490 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[31490 0 5200 2000 6000 3200 "" "2" ""]
Pad[31490 -2600 31490 2600 5200 2000 6000 "" "2" ""]
Pad[31490 -2600 31490 2600 5200 2000 6000 "" "2" ",onsolder"]
)
