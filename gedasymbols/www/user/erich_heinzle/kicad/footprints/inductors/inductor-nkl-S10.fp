# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-S10
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-S10
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 -11120 -3480 0 100 ""]
(
ElementLine[-33460 -17710 33460 -17710 500]
ElementLine[33460 -17710 33460 17710 500]
ElementLine[33460 17710 -33460 17710 500]
ElementLine[-33460 17710 -33460 -17710 500]
ElementLine[-37400 0 -25590 0 500]
ElementLine[-31490 5900 -31490 -5900 500]
ElementLine[25590 0 37400 0 500]
ElementLine[31490 5900 31490 -5900 500]
ElementArc[-31490 0 4172 4172 0 360 500]
ElementArc[31490 0 4172 4172 0 360 500]
Pin[19680 -9840 8590 2000 9390 3200 "" "1" ""]
Pad[19680 -14140 19680 -5540 8590 2000 9390 "" "1" ""]
Pad[19680 -14140 19680 -5540 8590 2000 9390 "" "1" ",onsolder"]
Pin[19680 9840 8590 2000 9390 3200 "" "2" ""]
Pad[19680 5540 19680 14140 8590 2000 9390 "" "2" ""]
Pad[19680 5540 19680 14140 8590 2000 9390 "" "2" ",onsolder"]
Pin[-19680 -9840 8590 2000 9390 3200 "" "3" ""]
Pad[-19680 -14140 -19680 -5540 8590 2000 9390 "" "3" ""]
Pad[-19680 -14140 -19680 -5540 8590 2000 9390 "" "3" ",onsolder"]
Pin[-19680 9840 8590 2000 9390 3200 "" "4" ""]
Pad[-19680 5540 -19680 14140 8590 2000 9390 "" "4" ""]
Pad[-19680 5540 -19680 14140 8590 2000 9390 "" "4" ",onsolder"]
Pin[0 -9840 8590 2000 9390 3200 "" "5" ""]
Pad[0 -14140 0 -5540 8590 2000 9390 "" "5" ""]
Pad[0 -14140 0 -5540 8590 2000 9390 "" "5" ",onsolder"]
Pin[0 9840 8590 2000 9390 3200 "" "6" ""]
Pad[0 5540 0 14140 8590 2000 9390 "" "6" ""]
Pad[0 5540 0 14140 8590 2000 9390 "" "6" ",onsolder"]
)
