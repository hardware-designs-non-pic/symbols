# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-26
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-26
# Text descriptor count: 1
# Draw segment object count: 26
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 680 -14310 0 100 ""]
(
ElementLine[-19680 -51180 19680 -51180 500]
ElementLine[19680 -51180 19680 51180 500]
ElementLine[19680 51180 -19680 51180 500]
ElementLine[-19680 51180 -19680 -51180 500]
ElementLine[-27550 -7870 27550 -7870 2400]
ElementLine[-27550 0 27550 0 2400]
ElementLine[-27550 7870 27550 7870 2400]
ElementLine[-27550 23620 27550 23620 2400]
ElementLine[-27550 31490 27550 31490 2400]
ElementLine[-27550 39370 27550 39370 2400]
ElementLine[-27550 43300 27550 43300 2400]
ElementLine[-27550 47240 27550 47240 2400]
ElementLine[-27550 51180 27550 51180 2400]
ElementLine[-27550 -7870 27550 -7870 2400]
ElementLine[-27550 -23620 27550 -23620 2400]
ElementLine[-27550 -23620 27550 -23620 2400]
ElementLine[-27550 -31490 27550 -31490 2400]
ElementLine[-27550 -39370 27550 -39370 2400]
ElementLine[-27550 -43300 27550 -43300 2400]
ElementLine[-27550 -47240 27550 -47240 2400]
ElementLine[-27550 -51180 27550 -51180 2400]
ElementLine[-27550 -15740 -15740 -15740 2400]
ElementLine[11810 -15740 27550 -15740 2400]
ElementLine[11810 -15740 27550 -15740 2400]
ElementLine[11810 15740 27550 15740 2400]
ElementLine[-27550 15740 -15740 15740 2400]
Pin[-27550 -15740 5200 2000 6000 3200 "" "1" ""]
Pad[-27550 -18340 -27550 -13140 5200 2000 6000 "" "1" ""]
Pad[-27550 -18340 -27550 -13140 5200 2000 6000 "" "1" ",onsolder"]
Pin[27550 -15740 5200 2000 6000 3200 "" "2" ""]
Pad[27550 -18340 27550 -13140 5200 2000 6000 "" "2" ""]
Pad[27550 -18340 27550 -13140 5200 2000 6000 "" "2" ",onsolder"]
Pin[-27550 15740 5200 2000 6000 3200 "" "3" ""]
Pad[-27550 13140 -27550 18340 5200 2000 6000 "" "3" ""]
Pad[-27550 13140 -27550 18340 5200 2000 6000 "" "3" ",onsolder"]
Pin[27550 15740 5200 2000 6000 3200 "" "4" ""]
Pad[27550 13140 27550 18340 5200 2000 6000 "" "4" ""]
Pad[27550 13140 27550 18340 5200 2000 6000 "" "4" ",onsolder"]
)
