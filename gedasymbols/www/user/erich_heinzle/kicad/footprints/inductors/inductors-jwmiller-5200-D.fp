# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-5200-D
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-5200-D
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -50000 -25000 0 100 ""]
(
ElementLine[-76250 1250 -62500 1250 260]
ElementLine[-62500 1250 -62500 -1250 260]
ElementLine[-76250 -1250 -62500 -1250 260]
ElementLine[-76250 1250 -76250 -1250 260]
ElementLine[62500 1250 76250 1250 260]
ElementLine[76250 1250 76250 -1250 260]
ElementLine[62500 -1250 76250 -1250 260]
ElementLine[62500 1250 62500 -1250 260]
ElementLine[-62500 -16250 -62500 16250 1000]
ElementLine[-62500 16250 -50000 16250 1000]
ElementLine[-50000 16250 -50000 20000 1000]
ElementLine[-50000 20000 50000 20000 1000]
ElementLine[50000 20000 50000 16250 1000]
ElementLine[50000 16250 62500 16250 1000]
ElementLine[62500 16250 62500 -16250 1000]
ElementLine[62500 -16250 50000 -16250 1000]
ElementLine[50000 -16250 50000 -20000 1000]
ElementLine[50000 -20000 -50000 -20000 1000]
ElementLine[-50000 -20000 -50000 -16250 1000]
ElementLine[-50000 -16250 -62500 -16250 1000]
ElementLine[-82500 0 -75000 0 2500]
ElementLine[75000 0 82500 0 2500]
ElementLine[-50000 -16250 -50000 16250 700]
ElementLine[50000 -16250 50000 16250 700]
Pin[-82500 0 10000 2000 10800 4000 "" "1" ""]
Pad[-82500 -5000 -82500 5000 10000 2000 10800 "" "1" ""]
Pad[-82500 -5000 -82500 5000 10000 2000 10800 "" "1" ",onsolder"]
Pin[82500 0 10000 2000 10800 4000 "" "2" ""]
Pad[82500 -5000 82500 5000 10000 2000 10800 "" "2" ""]
Pad[82500 -5000 82500 5000 10000 2000 10800 "" "2" ",onsolder"]
)
