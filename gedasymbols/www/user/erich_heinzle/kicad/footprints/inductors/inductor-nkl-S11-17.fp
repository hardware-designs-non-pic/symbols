# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-S11-17
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-S11-17
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "type" "" "" 0 0 -8480 -12640 0 100 ""]
(
ElementLine[-41330 -21650 41330 -21650 500]
ElementLine[41330 -21650 41330 21650 500]
ElementLine[41330 21650 -41330 21650 500]
ElementLine[-41330 21650 -41330 -21650 500]
Pin[-24600 0 11810 2000 12610 4330 "" "P$1" ""]
Pad[-24600 -5905 -24600 5905 11810 2000 12610 "" "P$1" ""]
Pad[-24600 -5905 -24600 5905 11810 2000 12610 "" "P$1" ",onsolder"]
Pin[24600 0 11810 2000 12610 4330 "" "P$2" ""]
Pad[24600 -5905 24600 5905 11810 2000 12610 "" "P$2" ""]
Pad[24600 -5905 24600 5905 11810 2000 12610 "" "P$2" ",onsolder"]
)
