# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-FB2000-AB
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-FB2000-AB
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -7500 -17500 0 100 ""]
(
ElementLine[-20000 -12500 20000 -12500 700]
ElementLine[20000 12500 -20000 12500 700]
ElementLine[-20000 -12500 -20000 12500 700]
ElementLine[20000 -12500 20000 12500 700]
Pin[-35000 0 6600 2000 7400 3200 "" "1" ""]
Pad[-35000 -3300 -35000 3300 6600 2000 7400 "" "1" ""]
Pad[-35000 -3300 -35000 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[35000 0 6600 2000 7400 3200 "" "2" ""]
Pad[35000 -3300 35000 3300 6600 2000 7400 "" "2" ""]
Pad[35000 -3300 35000 3300 6600 2000 7400 "" "2" ",onsolder"]
)
