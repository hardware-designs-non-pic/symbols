# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module SM1008I
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: SM1008I
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "SM1008I" "" "" 0 0 0 -2000 0 100 ""]
(
ElementLine[-3000 -5500 -9000 -5500 500]
ElementLine[-9000 -5500 -9000 5500 500]
ElementLine[-9000 5500 -3000 5500 500]
ElementLine[3000 5500 9000 5500 500]
ElementLine[9000 5500 9000 -5500 500]
ElementLine[9000 -5500 3000 -5500 500]
Pad[-6000 -1500 -6000 1500 7000 2000 7800 "" "1" "square"]
Pad[6000 -1500 6000 1500 7000 2000 7800 "" "2" "square"]
)
