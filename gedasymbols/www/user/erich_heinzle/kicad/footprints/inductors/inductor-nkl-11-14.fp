# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-14
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-14
# Text descriptor count: 1
# Draw segment object count: 28
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">value" "" "" 0 0 5000 22500 0 100 ""]
(
ElementLine[-17710 1960 17710 1960 260]
ElementLine[17710 1960 17710 -1960 260]
ElementLine[-17710 -1960 17710 -1960 260]
ElementLine[-17710 1960 -17710 -1960 260]
ElementLine[-11810 13770 -11810 27550 500]
ElementLine[-11810 27550 11810 27550 500]
ElementLine[11810 27550 11810 13770 500]
ElementLine[-11810 -13770 -11810 -27550 500]
ElementLine[-11810 -27550 11810 -27550 500]
ElementLine[11810 -27550 11810 -13770 500]
ElementLine[11810 -9840 11810 9840 500]
ElementLine[-11810 9840 -11810 -9840 500]
ElementLine[-15740 -3930 15740 -3930 1600]
ElementLine[-15740 3930 15740 3930 1600]
ElementLine[-9840 11810 9840 11810 1600]
ElementLine[-15740 7870 15740 7870 1600]
ElementLine[-15740 15740 15740 15740 1600]
ElementLine[-15740 18620 15740 18620 1600]
ElementLine[-15740 27550 15740 27550 1600]
ElementLine[-15740 -7870 15740 -7870 1600]
ElementLine[-15740 -15740 15740 -15740 1600]
ElementLine[-15740 -18620 15740 -18620 1600]
ElementLine[-15740 -27550 15740 -27550 1600]
ElementLine[-9840 -11810 9840 -11810 1600]
ElementLine[-15740 -11810 -9840 -11810 1600]
ElementLine[9840 -11810 15740 -11810 1600]
ElementLine[9840 11810 15740 11810 1600]
ElementLine[-15740 11810 -7870 11810 1600]
Pin[-15740 11810 5200 2000 6000 3200 "" "1" ""]
Pad[-15740 9210 -15740 14410 5200 2000 6000 "" "1" ""]
Pad[-15740 9210 -15740 14410 5200 2000 6000 "" "1" ",onsolder"]
Pin[15740 11810 5200 2000 6000 3200 "" "2" ""]
Pad[15740 9210 15740 14410 5200 2000 6000 "" "2" ""]
Pad[15740 9210 15740 14410 5200 2000 6000 "" "2" ",onsolder"]
Pin[-15740 -11810 5200 2000 6000 3200 "" "3" ""]
Pad[-15740 -14410 -15740 -9210 5200 2000 6000 "" "3" ""]
Pad[-15740 -14410 -15740 -9210 5200 2000 6000 "" "3" ",onsolder"]
Pin[15740 -11810 5200 2000 6000 3200 "" "4" ""]
Pad[15740 -14410 15740 -9210 5200 2000 6000 "" "4" ""]
Pad[15740 -14410 15740 -9210 5200 2000 6000 "" "4" ",onsolder"]
)
