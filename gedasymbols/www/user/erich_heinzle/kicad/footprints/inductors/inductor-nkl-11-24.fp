# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-11-24
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-11-24
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 680 -26120 0 100 ""]
(
ElementLine[-15740 -47240 15740 -47240 500]
ElementLine[15740 -47240 15740 47240 500]
ElementLine[15740 47240 -15740 47240 500]
ElementLine[-15740 47240 -15740 -47240 500]
ElementLine[-19680 -3930 19680 -3930 2400]
ElementLine[-19680 3930 19680 3930 2400]
ElementLine[-19680 7870 19680 7870 2400]
ElementLine[-19680 11810 19680 11810 2400]
ElementLine[-19680 15740 19680 15740 2400]
ElementLine[-19680 19680 19680 19680 2400]
ElementLine[-19680 31490 19680 31490 2400]
ElementLine[-19680 35430 19680 35430 2400]
ElementLine[-19680 39370 19680 39370 2400]
ElementLine[-19680 43300 19680 43300 2400]
ElementLine[-19680 47240 19680 47240 2400]
ElementLine[-19680 -7870 19680 -7870 2400]
ElementLine[-19680 -11810 19680 -11810 2400]
ElementLine[-19680 -15740 19680 -15740 2400]
ElementLine[-19680 -19680 19680 -19680 2400]
ElementLine[-19680 -31490 19680 -31490 2400]
ElementLine[-19680 -35430 19680 -35430 2400]
ElementLine[-19680 -39370 19680 -39370 2400]
ElementLine[-19680 -43300 19680 -43300 2400]
ElementLine[-19680 -47240 19680 -47240 2400]
ElementLine[-19680 -27550 -15740 -27550 2400]
ElementLine[-19680 -23620 -15740 -23620 2400]
ElementLine[15740 -27550 19680 -27550 2400]
ElementLine[15740 -23620 19680 -23620 2400]
ElementLine[15740 23620 19680 23620 2400]
ElementLine[15740 27550 19680 27550 2400]
ElementLine[-19680 27550 -15740 27550 2400]
ElementLine[-19680 23620 -15740 23620 2400]
Pin[-19680 0 5200 2000 6000 3200 "" "P$1" ""]
Pad[-19680 -2600 -19680 2600 5200 2000 6000 "" "P$1" ""]
Pad[-19680 -2600 -19680 2600 5200 2000 6000 "" "P$1" ",onsolder"]
Pin[19680 0 5200 2000 6000 3200 "" "P$2" ""]
Pad[19680 -2600 19680 2600 5200 2000 6000 "" "P$2" ""]
Pad[19680 -2600 19680 2600 5200 2000 6000 "" "P$2" ",onsolder"]
)
