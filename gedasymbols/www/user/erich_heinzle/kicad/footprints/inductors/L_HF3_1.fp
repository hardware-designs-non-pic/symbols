# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module L_HF3_1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: L_HF3_1
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L_HF_3_1" "" "" 0 0 10000 -20000 0 100 ""]
(
ElementLine[5000 12500 2500 -12500 1500]
ElementLine[2500 -12500 2500 12500 1500]
ElementLine[2500 12500 0 0 1500]
ElementLine[7500 -12500 10000 0 1500]
ElementLine[5000 12500 5000 -12500 1500]
ElementLine[5000 -12500 7500 12500 1500]
ElementLine[7500 12500 7500 -12500 1500]
Pin[0 0 7870 2000 8670 3200 "" "1" "square"]
Pin[10000 0 7870 2000 8670 3200 "" "2" ""]
)
