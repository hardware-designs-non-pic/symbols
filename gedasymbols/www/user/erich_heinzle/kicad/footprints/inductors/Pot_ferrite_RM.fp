# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module Pot_ferrite_RM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Pot_ferrite_RM
# Text descriptor count: 1
# Draw segment object count: 22
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" "Pot_RM" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[12500 12500 27500 27500 1500]
ElementLine[27500 27500 30000 27500 1500]
ElementLine[40000 10000 40000 -10000 1500]
ElementLine[25000 -7500 25000 -10000 1500]
ElementLine[25000 -10000 42500 -10000 1500]
ElementLine[42500 -10000 42500 10000 1500]
ElementLine[42500 10000 25000 10000 1500]
ElementLine[25000 10000 25000 -7500 1500]
ElementLine[-40000 -10000 -40000 7500 1500]
ElementLine[-40000 -10000 -25000 -10000 1500]
ElementLine[-25000 -10000 -25000 10000 1500]
ElementLine[-25000 10000 -42500 10000 1500]
ElementLine[-42500 10000 -42500 -10000 1500]
ElementLine[-42500 -10000 -40000 -10000 1500]
ElementLine[-30000 27500 -27500 27500 1500]
ElementLine[-27500 27500 -12500 12500 1500]
ElementLine[-12500 12500 12500 12500 1500]
ElementLine[-30000 -27500 -27500 -27500 1500]
ElementLine[-27500 -27500 -10000 -12500 1500]
ElementLine[-10000 -12500 12500 -12500 1500]
ElementLine[12500 -12500 27500 -27500 1500]
ElementLine[27500 -27500 30000 -27500 1500]
ElementArc[0 0 31623 31623 0 360 1500]
ElementArc[0 0 41231 41231 0 360 1500]
Pin[-20000 -20000 7200 2000 8000 3200 "" "1" "square"]
Pin[-20000 20000 7200 2000 8000 3200 "" "2" ""]
Pin[20000 20000 7200 2000 8000 3200 "" "3" ""]
Pin[20000 -20000 7200 2000 8000 3200 "" "4" ""]
Pin[40000 0 6000 2000 6800 3200 "" "5" ""]
Pin[-40000 0 6000 2000 6800 3200 "" "6" ""]
)
