# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B23
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B23
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 10
# Pad count: 10
#
Element["" ">value" "" "" 0 0 25800 7510 0 100 ""]
(
ElementLine[-43300 -23620 43300 -23620 500]
ElementLine[43300 -23620 43300 23620 500]
ElementLine[43300 23620 -43300 23620 500]
ElementLine[-43300 23620 -43300 -23620 500]
ElementLine[-39370 -29520 39370 -29520 500]
ElementLine[-39370 29520 39370 29520 500]
ElementArc[0 -23620 5900 5900 0 360 500]
ElementArc[-19680 -23620 5910 5910 0 360 500]
ElementArc[19680 -23620 5910 5910 0 360 500]
ElementArc[-19680 23620 5910 5910 0 360 500]
ElementArc[0 23620 5900 5900 0 360 500]
ElementArc[19680 23620 5910 5910 0 360 500]
ElementArc[-39170 23620 5710 5710 0 360 500]
ElementArc[39170 -23620 5710 5710 0 360 500]
ElementArc[-39170 -23620 5708 5708 0 360 500]
ElementArc[39170 23620 5708 5708 0 360 500]
Pin[-39370 24600 15000 2000 15800 5000 "" "1" ""]
Pad[-39370 17100 -39370 32100 15000 2000 15800 "" "1" ""]
Pad[-39370 17100 -39370 32100 15000 2000 15800 "" "1" ",onsolder"]
Pin[-19680 24600 15000 2000 15800 5000 "" "2" ""]
Pad[-19680 17100 -19680 32100 15000 2000 15800 "" "2" ""]
Pad[-19680 17100 -19680 32100 15000 2000 15800 "" "2" ",onsolder"]
Pin[0 24600 15000 2000 15800 5000 "" "3" ""]
Pad[0 17100 0 32100 15000 2000 15800 "" "3" ""]
Pad[0 17100 0 32100 15000 2000 15800 "" "3" ",onsolder"]
Pin[19680 24600 15000 2000 15800 5000 "" "4" ""]
Pad[19680 17100 19680 32100 15000 2000 15800 "" "4" ""]
Pad[19680 17100 19680 32100 15000 2000 15800 "" "4" ",onsolder"]
Pin[39370 24600 15000 2000 15800 5000 "" "5" ""]
Pad[39370 17100 39370 32100 15000 2000 15800 "" "5" ""]
Pad[39370 17100 39370 32100 15000 2000 15800 "" "5" ",onsolder"]
Pin[39370 -24600 15000 2000 15800 5000 "" "6" ""]
Pad[39370 -32100 39370 -17100 15000 2000 15800 "" "6" ""]
Pad[39370 -32100 39370 -17100 15000 2000 15800 "" "6" ",onsolder"]
Pin[19680 -24600 15000 2000 15800 5000 "" "7" ""]
Pad[19680 -32100 19680 -17100 15000 2000 15800 "" "7" ""]
Pad[19680 -32100 19680 -17100 15000 2000 15800 "" "7" ",onsolder"]
Pin[0 -24600 15000 2000 15800 5000 "" "8" ""]
Pad[0 -32100 0 -17100 15000 2000 15800 "" "8" ""]
Pad[0 -32100 0 -17100 15000 2000 15800 "" "8" ",onsolder"]
Pin[-19680 -24600 15000 2000 15800 5000 "" "9" ""]
Pad[-19680 -32100 -19680 -17100 15000 2000 15800 "" "9" ""]
Pad[-19680 -32100 -19680 -17100 15000 2000 15800 "" "9" ",onsolder"]
Pin[-39370 -24600 15000 2000 15800 5000 "" "10" ""]
Pad[-39370 -32100 -39370 -17100 15000 2000 15800 "" "10" ""]
Pad[-39370 -32100 -39370 -17100 15000 2000 15800 "" "10" ",onsolder"]
)
