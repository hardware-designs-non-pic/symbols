# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-PM1812
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-PM1812
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 1250 -10750 0 100 ""]
(
ElementLine[-10000 2500 -8750 2500 260]
ElementLine[-8750 2500 -8750 -2500 260]
ElementLine[-10000 -2500 -8750 -2500 260]
ElementLine[-10000 2500 -10000 -2500 260]
ElementLine[8750 2500 10000 2500 260]
ElementLine[10000 2500 10000 -2500 260]
ElementLine[8750 -2500 10000 -2500 260]
ElementLine[8750 2500 8750 -2500 260]
ElementLine[-8850 6880 8850 6880 700]
ElementLine[8850 6880 8850 -6880 700]
ElementLine[8850 -6880 -8850 -6880 700]
ElementLine[-8850 -6880 -8850 6880 700]
Pad[-8460 0 -7280 0 6690 2000 7490 "" "1" "square"]
Pad[7280 0 8460 0 6690 2000 7490 "" "2" "square"]
)
