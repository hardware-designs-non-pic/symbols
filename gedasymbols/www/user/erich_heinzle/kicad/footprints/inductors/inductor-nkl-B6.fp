# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B6
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B6
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">value" "" "" 0 0 5000 9130 0 100 ""]
(
ElementArc[0 0 22260 22260 0 360 250]
Pin[-17220 19680 6600 2000 7400 5000 "" "1" ""]
Pad[-17220 16380 -17220 22980 6600 2000 7400 "" "1" ""]
Pad[-17220 16380 -17220 22980 6600 2000 7400 "" "1" ",onsolder"]
Pin[-17220 -19680 6600 2000 7400 5000 "" "2" ""]
Pad[-17220 -22980 -17220 -16380 6600 2000 7400 "" "2" ""]
Pad[-17220 -22980 -17220 -16380 6600 2000 7400 "" "2" ",onsolder"]
Pin[17220 19680 6600 2000 7400 5000 "" "3" ""]
Pad[17220 16380 17220 22980 6600 2000 7400 "" "3" ""]
Pad[17220 16380 17220 22980 6600 2000 7400 "" "3" ",onsolder"]
Pin[17220 -19680 6600 2000 7400 5000 "" "4" ""]
Pad[17220 -22980 17220 -16380 6600 2000 7400 "" "4" ""]
Pad[17220 -22980 17220 -16380 6600 2000 7400 "" "4" ",onsolder"]
Pin[-9840 24600 6600 2000 7400 5000 "" "5" ""]
Pad[-9840 21300 -9840 27900 6600 2000 7400 "" "5" ""]
Pad[-9840 21300 -9840 27900 6600 2000 7400 "" "5" ",onsolder"]
Pin[-9840 -24600 6600 2000 7400 5000 "" "6" ""]
Pad[-9840 -27900 -9840 -21300 6600 2000 7400 "" "6" ""]
Pad[-9840 -27900 -9840 -21300 6600 2000 7400 "" "6" ",onsolder"]
Pin[9840 24600 6600 2000 7400 5000 "" "7" ""]
Pad[9840 21300 9840 27900 6600 2000 7400 "" "7" ""]
Pad[9840 21300 9840 27900 6600 2000 7400 "" "7" ",onsolder"]
Pin[9840 -24600 6600 2000 7400 5000 "" "8" ""]
Pad[9840 -27900 9840 -21300 6600 2000 7400 "" "8" ""]
Pad[9840 -27900 9840 -21300 6600 2000 7400 "" "8" ",onsolder"]
)
