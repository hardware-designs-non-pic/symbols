# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B3
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">Name" "" "" 0 0 -5600 -11670 0 100 ""]
(
ElementLine[-53140 -27550 -39370 -27550 500]
ElementLine[-39370 -27550 -31490 -35430 500]
ElementLine[-31490 -35430 -27550 -35430 500]
ElementLine[-27550 -35430 -19680 -27550 500]
ElementLine[-19680 -27550 19680 -27550 500]
ElementLine[19680 -27550 27550 -35430 500]
ElementLine[27550 -35430 31490 -35430 500]
ElementLine[31490 -35430 39370 -27550 500]
ElementLine[39370 -27550 53140 -27550 500]
ElementLine[53140 -27550 53140 27550 500]
ElementLine[53140 27550 39370 27550 500]
ElementLine[39370 27550 31490 35430 500]
ElementLine[31490 35430 27550 35430 500]
ElementLine[27550 35430 19680 27550 500]
ElementLine[19680 27550 -19680 27550 500]
ElementLine[-19680 27550 -27550 35430 500]
ElementLine[-27550 35430 -32480 35430 500]
ElementLine[-32480 35430 -39370 27550 500]
ElementLine[-39370 27550 -53140 27550 500]
ElementLine[-53140 27550 -53140 -27550 500]
Pin[-29520 -24600 15000 2000 15800 5000 "" "1" ""]
Pad[-29520 -32100 -29520 -17100 15000 2000 15800 "" "1" ""]
Pad[-29520 -32100 -29520 -17100 15000 2000 15800 "" "1" ",onsolder"]
Pin[-29520 24600 15000 2000 15800 5000 "" "2" ""]
Pad[-29520 17100 -29520 32100 15000 2000 15800 "" "2" ""]
Pad[-29520 17100 -29520 32100 15000 2000 15800 "" "2" ",onsolder"]
Pin[29520 -24600 15000 2000 15800 5000 "" "3" ""]
Pad[29520 -32100 29520 -17100 15000 2000 15800 "" "3" ""]
Pad[29520 -32100 29520 -17100 15000 2000 15800 "" "3" ",onsolder"]
Pin[29520 24600 15000 2000 15800 5000 "" "4" ""]
Pad[29520 17100 29520 32100 15000 2000 15800 "" "4" ""]
Pad[29520 17100 29520 32100 15000 2000 15800 "" "4" ",onsolder"]
)
