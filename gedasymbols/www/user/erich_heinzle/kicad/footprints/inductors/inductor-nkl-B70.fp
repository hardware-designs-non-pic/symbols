# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B70
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B70
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 -590 -34520 0 100 ""]
(
ElementArc[0 0 97425 97425 0 360 250]
Pin[-49210 0 25400 2000 26200 5000 "" "P$1" ""]
Pad[-49210 -12700 -49210 12700 25400 2000 26200 "" "P$1" ""]
Pad[-49210 -12700 -49210 12700 25400 2000 26200 "" "P$1" ",onsolder"]
Pin[-118110 0 25400 2000 26200 5000 "" "P$2" ""]
Pad[-118110 -12700 -118110 12700 25400 2000 26200 "" "P$2" ""]
Pad[-118110 -12700 -118110 12700 25400 2000 26200 "" "P$2" ",onsolder"]
Pin[49210 0 25400 2000 26200 5000 "" "P$3" ""]
Pad[49210 -12700 49210 12700 25400 2000 26200 "" "P$3" ""]
Pad[49210 -12700 49210 12700 25400 2000 26200 "" "P$3" ",onsolder"]
Pin[118110 0 25400 2000 26200 5000 "" "P$4" ""]
Pad[118110 -12700 118110 12700 25400 2000 26200 "" "P$4" ""]
Pad[118110 -12700 118110 12700 25400 2000 26200 "" "P$4" ",onsolder"]
)
