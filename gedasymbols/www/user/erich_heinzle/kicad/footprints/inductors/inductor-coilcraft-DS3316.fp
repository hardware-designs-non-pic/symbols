# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-coilcraft-DS3316
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-coilcraft-DS3316
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">Name" "" "" 0 0 530 -22570 0 100 ""]
(
ElementLine[-25510 -5980 -9520 -17710 1000]
ElementLine[-9520 -17710 9330 -17710 1000]
ElementLine[9330 -17710 25510 -5980 1000]
ElementLine[25510 -5980 25510 5980 1000]
ElementLine[25510 5980 9520 17710 1000]
ElementLine[9520 17710 -9520 17710 1000]
ElementLine[-9520 17710 -25510 5980 1000]
ElementLine[-25510 5980 -25510 -5980 1000]
ElementLine[-27550 7790 -27550 -7870 1000]
ElementLine[-27550 -7870 -9960 -20390 1000]
ElementLine[10110 -20390 -9960 -20390 1000]
ElementLine[27550 7870 27550 -7870 1000]
ElementLine[27550 -7870 10110 -20390 1000]
ElementLine[27550 7870 11810 20620 1000]
ElementLine[11810 20620 -9600 20620 1000]
ElementLine[-9600 20620 -27550 7790 1000]
ElementArc[0 0 11653 11653 0 360 250]
ElementArc[0 0 10295 10295 0 360 2000]
ElementArc[0 0 6732 6732 0 360 0]
Pad[-22025 0 -21515 0 10980 2000 11780 "" "P$1" "square"]
Pad[21515 0 22025 0 10980 2000 11780 "" "P$2" "square"]
)
