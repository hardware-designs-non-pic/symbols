# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-coilcraft-DO3316P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-coilcraft-DO3316P
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -12500 -22500 0 100 ""]
(
ElementLine[-25000 -7500 -7500 -18000 1000]
ElementLine[25000 -7500 7500 -18000 1000]
ElementLine[25000 7500 7500 18000 1000]
ElementLine[-25000 7500 -7500 18000 1000]
ElementLine[-7500 -18000 7500 -18000 1000]
ElementLine[-7500 18000 7500 18000 1000]
ElementLine[-25000 -7500 -25000 7500 1000]
ElementLine[25000 -7500 25000 7500 1000]
ElementArc[0 0 16553 16553 0 360 1000]
ElementArc[0 0 16553 16553 0 360 1000]
ElementArc[0 0 16553 16553 0 360 1000]
ElementArc[0 0 16553 16553 0 360 1000]
Pad[-21035 0 -20525 0 10980 2000 11780 "" "1" "square"]
Pad[20525 0 21035 0 10980 2000 11780 "" "2" "square"]
)
