# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-4600-D
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-4600-D
# Text descriptor count: 1
# Draw segment object count: 26
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 57500 -12500 0 100 ""]
(
ElementLine[-32500 30000 -17500 30000 260]
ElementLine[-17500 30000 -17500 -30000 260]
ElementLine[-32500 -30000 -17500 -30000 260]
ElementLine[-32500 30000 -32500 -30000 260]
ElementLine[-7500 30000 7500 30000 260]
ElementLine[7500 30000 7500 -30000 260]
ElementLine[-7500 -30000 7500 -30000 260]
ElementLine[-7500 30000 -7500 -30000 260]
ElementLine[17500 30000 32500 30000 260]
ElementLine[32500 30000 32500 -30000 260]
ElementLine[17500 -30000 32500 -30000 260]
ElementLine[17500 30000 17500 -30000 260]
ElementLine[-50000 1250 -38750 1250 260]
ElementLine[-38750 1250 -38750 -1250 260]
ElementLine[-50000 -1250 -38750 -1250 260]
ElementLine[-50000 1250 -50000 -1250 260]
ElementLine[38750 1250 50000 1250 260]
ElementLine[50000 1250 50000 -1250 260]
ElementLine[38750 -1250 50000 -1250 260]
ElementLine[38750 1250 38750 -1250 260]
ElementLine[-38750 -11250 -38750 11250 1000]
ElementLine[-38750 11250 38750 11250 1000]
ElementLine[38750 11250 38750 -11250 1000]
ElementLine[38750 -11250 -38750 -11250 1000]
ElementLine[-56250 0 -48750 0 2500]
ElementLine[48750 0 56250 0 2500]
Pin[-56250 0 10000 2000 10800 4000 "" "1" ""]
Pad[-56250 -5000 -56250 5000 10000 2000 10800 "" "1" ""]
Pad[-56250 -5000 -56250 5000 10000 2000 10800 "" "1" ",onsolder"]
Pin[56250 0 10000 2000 10800 4000 "" "2" ""]
Pad[56250 -5000 56250 5000 10000 2000 10800 "" "2" ""]
Pad[56250 -5000 56250 5000 10000 2000 10800 "" "2" ",onsolder"]
)
