# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module ALTNC
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ALTNC
# Text descriptor count: 1
# Draw segment object count: 41
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "ALTNC" "" "" 0 0 0 37500 0 100 ""]
(
ElementLine[20000 42500 25000 42500 1500]
ElementLine[-25000 47500 20000 42500 1500]
ElementLine[20000 -55000 25000 -55000 1500]
ElementLine[-25000 -50000 20000 -55000 1500]
ElementLine[-25000 -20000 20000 -25000 1500]
ElementLine[-25000 -30000 20000 -35000 1500]
ElementLine[-25000 -40000 20000 -45000 1500]
ElementLine[20000 -25000 25000 -25000 1500]
ElementLine[20000 -35000 25000 -35000 1500]
ElementLine[20000 -45000 25000 -45000 1500]
ElementLine[-25000 40000 20000 35000 1500]
ElementLine[-25000 30000 20000 25000 1500]
ElementLine[-25000 20000 20000 15000 1500]
ElementLine[20000 35000 25000 35000 1500]
ElementLine[20000 25000 25000 25000 1500]
ElementLine[20000 15000 25000 15000 1500]
ElementLine[25000 -57500 27500 -57500 1500]
ElementLine[27500 -57500 27500 47500 1500]
ElementLine[27500 47500 -27500 47500 1500]
ElementLine[-27500 47500 -27500 -57500 1500]
ElementLine[-27500 -57500 25000 -57500 1500]
ElementLine[-32500 -57500 -30000 -60000 1500]
ElementLine[-30000 -60000 -27500 -62500 1500]
ElementLine[-27500 -62500 27500 -62500 1500]
ElementLine[27500 -62500 32500 -57500 1500]
ElementLine[32500 -57500 32500 47500 1500]
ElementLine[32500 47500 30000 50000 1500]
ElementLine[30000 50000 27500 52500 1500]
ElementLine[27500 52500 -27500 52500 1500]
ElementLine[-27500 52500 -32500 47500 1500]
ElementLine[-32500 47500 -32500 -57500 1500]
ElementLine[-30000 50000 -30000 -60000 1500]
ElementLine[-30000 -60000 30000 -60000 1500]
ElementLine[30000 -60000 30000 50000 1500]
ElementLine[30000 50000 -30000 50000 1500]
ElementLine[20000 -15000 25000 -15000 1500]
ElementLine[20000 -5000 25000 -5000 1500]
ElementLine[20000 5000 25000 5000 1500]
ElementLine[-25000 -10000 20000 -15000 1500]
ElementLine[-25000 0 20000 -5000 1500]
ElementLine[-25000 10000 20000 5000 1500]
Pin[-15000 -15000 12760 2000 13560 3200 "" "1" "square"]
Pin[20000 15000 12760 2000 13560 3200 "" "2" ""]
)
