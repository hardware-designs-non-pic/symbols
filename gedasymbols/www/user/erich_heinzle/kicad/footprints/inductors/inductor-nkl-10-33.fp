# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-10-33
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-10-33
# Text descriptor count: 1
# Draw segment object count: 38
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 2500 -12500 0 100 ""]
(
ElementLine[-70860 0 -35430 0 2400]
ElementLine[31490 0 74800 0 2400]
ElementLine[0 31490 0 74800 2400]
ElementLine[0 -31490 0 -74800 2400]
ElementLine[31490 -3930 72830 -11810 2400]
ElementLine[31490 3930 72830 11810 2400]
ElementLine[27550 15740 64960 35430 2400]
ElementLine[27550 -15740 64960 -35430 2400]
ElementLine[-27550 -15740 -64960 -35430 2400]
ElementLine[-27550 15740 -64960 35430 2400]
ElementLine[29520 -9840 70860 -23620 2400]
ElementLine[29520 7870 70860 23620 2400]
ElementLine[-29520 -9840 -70860 -23620 2400]
ElementLine[-31490 -3930 -72830 -11810 2400]
ElementLine[-72830 11810 -29520 7870 2400]
ElementLine[-29520 11810 -70860 23620 2400]
ElementLine[3930 -31490 11810 -72830 2400]
ElementLine[-3930 -31490 -11810 -72830 2400]
ElementLine[3930 31490 11810 72830 2400]
ElementLine[-3930 31490 -11810 72830 2400]
ElementLine[15740 -27550 35430 -64960 2400]
ElementLine[-35430 -64960 -15740 -27550 2400]
ElementLine[-35430 64960 -15740 27550 2400]
ElementLine[35430 64960 15740 27550 2400]
ElementLine[-23620 70860 -9840 29520 2400]
ElementLine[23620 70860 9840 29520 2400]
ElementLine[23620 -70860 9840 -29520 2400]
ElementLine[-25590 -70860 -9840 -29520 2400]
ElementLine[-25590 19680 -59050 45270 2400]
ElementLine[-21650 23620 -47240 57080 2400]
ElementLine[21650 23620 47240 57080 2400]
ElementLine[25590 19680 57080 47240 2400]
ElementLine[21650 -23620 47240 -57080 2400]
ElementLine[25590 -19680 57080 -47240 2400]
ElementLine[-21650 -23620 -47240 -57080 2400]
ElementLine[-25590 -19680 -57080 -47240 2400]
ElementLine[-78740 0 -70860 0 2400]
ElementLine[-35430 0 -27550 0 2400]
ElementArc[0 0 47320 47320 0 360 250]
ElementArc[0 0 27832 27832 0 360 250]
Pin[-78740 0 6000 2000 6800 4000 "" "1" ""]
Pad[-78740 -3000 -78740 3000 6000 2000 6800 "" "1" ""]
Pad[-78740 -3000 -78740 3000 6000 2000 6800 "" "1" ",onsolder"]
Pin[-27550 0 6000 2000 6800 4000 "" "2" ""]
Pad[-27550 -3000 -27550 3000 6000 2000 6800 "" "2" ""]
Pad[-27550 -3000 -27550 3000 6000 2000 6800 "" "2" ",onsolder"]
)
