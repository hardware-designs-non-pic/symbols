# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-10-24
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-10-24
# Text descriptor count: 1
# Draw segment object count: 26
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 2500 -7500 0 100 ""]
(
ElementLine[-47240 0 -23620 0 2400]
ElementLine[15740 0 55110 0 2400]
ElementLine[0 -15740 0 -55110 2400]
ElementLine[0 15740 0 55110 2400]
ElementLine[-15740 -3930 -53140 -9840 2400]
ElementLine[-15740 3930 -53140 9840 2400]
ElementLine[-13770 -7870 -49210 -21650 2400]
ElementLine[-13770 7870 -49210 21650 2400]
ElementLine[-11810 -11810 -39370 -35430 2400]
ElementLine[-11810 11810 -39370 35430 2400]
ElementLine[-3930 -15740 -13770 -53140 2400]
ElementLine[-3930 15740 -13770 53140 2400]
ElementLine[-7870 13770 -27550 45270 2400]
ElementLine[-7870 -13770 -27550 -45270 2400]
ElementLine[3930 15740 11810 53140 2400]
ElementLine[3930 -15740 11810 -53140 2400]
ElementLine[15740 -3930 53140 -11810 2400]
ElementLine[15740 3930 53140 11810 2400]
ElementLine[13770 -7870 49210 -21650 2400]
ElementLine[13770 7870 49210 21650 2400]
ElementLine[11810 -11810 39370 -35430 2400]
ElementLine[7870 -13770 27550 -45270 2400]
ElementLine[7870 13770 27550 47240 2400]
ElementLine[11810 11810 39370 35430 2400]
ElementLine[-53140 0 -47240 0 2400]
ElementLine[-23620 0 -17710 0 2400]
ElementArc[0 0 33404 33404 0 360 250]
ElementArc[0 0 16702 16702 0 360 250]
Pin[-53140 0 6000 2000 6800 4000 "" "1" ""]
Pad[-53140 -3000 -53140 3000 6000 2000 6800 "" "1" ""]
Pad[-53140 -3000 -53140 3000 6000 2000 6800 "" "1" ",onsolder"]
Pin[-17710 0 6000 2000 6800 4000 "" "2" ""]
Pad[-17710 -3000 -17710 3000 6000 2000 6800 "" "2" ""]
Pad[-17710 -3000 -17710 3000 6000 2000 6800 "" "2" ",onsolder"]
)
