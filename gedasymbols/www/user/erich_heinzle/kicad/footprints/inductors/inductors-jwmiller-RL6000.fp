# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-RL6000
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-RL6000
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 33240 -12340 0 100 ""]
(
ElementArc[0 0 13195 13195 0 360 350]
Pin[-9840 0 7600 2000 8400 3200 "" "1" ""]
Pad[-9840 -3800 -9840 3800 7600 2000 8400 "" "1" ""]
Pad[-9840 -3800 -9840 3800 7600 2000 8400 "" "1" ",onsolder"]
Pin[9840 0 7600 2000 8400 3200 "" "2" ""]
Pad[9840 -3800 9840 3800 7600 2000 8400 "" "2" ""]
Pad[9840 -3800 9840 3800 7600 2000 8400 "" "2" ",onsolder"]
)
