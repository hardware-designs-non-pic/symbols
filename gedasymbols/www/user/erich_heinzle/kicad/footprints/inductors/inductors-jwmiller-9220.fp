# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-9220
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-9220
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -10000 -15000 0 100 ""]
(
ElementLine[-28430 980 -22630 980 260]
ElementLine[-22630 980 -22630 -980 260]
ElementLine[-28430 -980 -22630 -980 260]
ElementLine[-28430 980 -28430 -980 260]
ElementLine[22630 980 28430 980 260]
ElementLine[28430 980 28430 -980 260]
ElementLine[22630 -980 28430 -980 260]
ElementLine[22630 980 22630 -980 260]
ElementLine[-32500 0 -28750 0 1900]
ElementLine[32500 0 28750 0 1900]
ElementLine[-22500 -10000 22500 -10000 700]
ElementLine[22500 -10000 22500 10000 700]
ElementLine[22500 10000 -22500 10000 700]
ElementLine[-22500 10000 -22500 -10000 700]
Pin[-32500 0 6600 2000 7400 3200 "" "1" ""]
Pad[-32500 -3300 -32500 3300 6600 2000 7400 "" "1" ""]
Pad[-32500 -3300 -32500 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[32500 0 6600 2000 7400 3200 "" "2" ""]
Pad[32500 -3300 32500 3300 6600 2000 7400 "" "2" ""]
Pad[32500 -3300 32500 3300 6600 2000 7400 "" "2" ",onsolder"]
)
