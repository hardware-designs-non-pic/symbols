# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B2818
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B2818
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">name" "" "" 0 0 680 -26120 0 100 ""]
(
ElementLine[-55110 -55110 55110 -55110 500]
ElementLine[55110 -55110 55110 55110 500]
ElementLine[55110 55110 -55110 55110 500]
ElementLine[-55110 55110 -55110 -55110 500]
Pin[-29520 -49210 15000 2000 15800 3200 "" "P$1" ""]
Pad[-29520 -56710 -29520 -41710 15000 2000 15800 "" "P$1" ""]
Pad[-29520 -56710 -29520 -41710 15000 2000 15800 "" "P$1" ",onsolder"]
Pin[29520 -49210 15000 2000 15800 3200 "" "P$2" ""]
Pad[29520 -56710 29520 -41710 15000 2000 15800 "" "P$2" ""]
Pad[29520 -56710 29520 -41710 15000 2000 15800 "" "P$2" ",onsolder"]
Pin[-9840 49210 15000 2000 15800 3200 "" "P$3" ""]
Pad[-9840 41710 -9840 56710 15000 2000 15800 "" "P$3" ""]
Pad[-9840 41710 -9840 56710 15000 2000 15800 "" "P$3" ",onsolder"]
Pin[9840 49210 15000 2000 15800 3200 "" "P$4" ""]
Pad[9840 41710 9840 56710 15000 2000 15800 "" "P$4" ""]
Pad[9840 41710 9840 56710 15000 2000 15800 "" "P$4" ",onsolder"]
Pin[29520 49210 15000 2000 15800 3200 "" "P$5" ""]
Pad[29520 41710 29520 56710 15000 2000 15800 "" "P$5" ""]
Pad[29520 41710 29520 56710 15000 2000 15800 "" "P$5" ",onsolder"]
Pin[-29520 49210 15000 2000 15800 3200 "" "P$6" ""]
Pad[-29520 41710 -29520 56710 15000 2000 15800 "" "P$6" ""]
Pad[-29520 41710 -29520 56710 15000 2000 15800 "" "P$6" ",onsolder"]
)
