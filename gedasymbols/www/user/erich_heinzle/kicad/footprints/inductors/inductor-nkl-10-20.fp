# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-10-20
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-10-20
# Text descriptor count: 1
# Draw segment object count: 30
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">name" "" "" 0 0 2500 -12500 0 100 ""]
(
ElementLine[19680 0 43300 0 2400]
ElementLine[-19680 0 -43300 0 2400]
ElementLine[0 -19680 0 -43300 2400]
ElementLine[0 19680 0 43300 2400]
ElementLine[-15740 11810 -35430 23620 2400]
ElementLine[15740 11810 35430 25590 2400]
ElementLine[15740 -11810 35430 -27550 2400]
ElementLine[-15740 -11810 -35430 -27550 2400]
ElementLine[3930 19680 7870 43300 2400]
ElementLine[-7870 43300 -3930 19680 2400]
ElementLine[-19680 3930 -43300 7870 2400]
ElementLine[-19680 -3930 -43300 -7870 2400]
ElementLine[-3930 -19680 -7870 -43300 2400]
ElementLine[3930 -19680 7870 -43300 2400]
ElementLine[19680 -3930 43300 -7870 2400]
ElementLine[19680 3930 43300 7870 2400]
ElementLine[7870 -17710 15740 -41330 2400]
ElementLine[-7870 -17710 -15740 -41330 2400]
ElementLine[-7870 17710 -15740 41330 2400]
ElementLine[7870 17710 15740 41330 2400]
ElementLine[-11810 15740 -25590 35430 2400]
ElementLine[11810 15740 25590 35430 2400]
ElementLine[-17710 7870 -41330 15740 2400]
ElementLine[-17710 -7870 -41330 -15740 2400]
ElementLine[17710 -7870 41330 -15740 2400]
ElementLine[17710 7870 41330 15740 2400]
ElementLine[11810 -15740 23620 -37400 2400]
ElementLine[-11810 -15740 -23620 -37400 2400]
ElementLine[-47240 0 -43300 0 2400]
ElementLine[-21650 -1960 -11810 0 2400]
ElementArc[0 0 27832 27832 0 360 250]
ElementArc[0 0 16702 16702 0 360 250]
Pin[-47240 0 5200 2000 6000 3200 "" "1" ""]
Pad[-47240 -2600 -47240 2600 5200 2000 6000 "" "1" ""]
Pad[-47240 -2600 -47240 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[-11810 0 5200 2000 6000 3200 "" "2" ""]
Pad[-11810 -2600 -11810 2600 5200 2000 6000 "" "2" ""]
Pad[-11810 -2600 -11810 2600 5200 2000 6000 "" "2" ",onsolder"]
)
