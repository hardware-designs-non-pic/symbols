# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-KS3615
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-KS3615
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">name" "" "" 0 0 -680 -23480 0 100 ""]
(
ElementLine[-47240 49210 -47240 -49210 500]
ElementLine[-47240 -49210 47240 -49210 500]
ElementLine[47240 -49210 47240 49210 500]
ElementLine[47240 49210 -47240 49210 500]
ElementLine[-51180 90550 -51180 -90550 500]
ElementLine[-51180 -90550 51180 -90550 500]
ElementLine[51180 -90550 51180 90550 500]
ElementLine[51180 90550 -51180 90550 500]
Pin[-39370 34440 15000 2000 15800 5000 "" "1" ""]
Pad[-39370 26940 -39370 41940 15000 2000 15800 "" "1" ""]
Pad[-39370 26940 -39370 41940 15000 2000 15800 "" "1" ",onsolder"]
Pin[39370 34440 15000 2000 15800 5000 "" "2" ""]
Pad[39370 26940 39370 41940 15000 2000 15800 "" "2" ""]
Pad[39370 26940 39370 41940 15000 2000 15800 "" "2" ",onsolder"]
Pin[-39370 -34440 15000 2000 15800 5000 "" "3" ""]
Pad[-39370 -41940 -39370 -26940 15000 2000 15800 "" "3" ""]
Pad[-39370 -41940 -39370 -26940 15000 2000 15800 "" "3" ",onsolder"]
Pin[39370 -34440 15000 2000 15800 5000 "" "4" ""]
Pad[39370 -41940 39370 -26940 15000 2000 15800 "" "4" ""]
Pad[39370 -41940 39370 -26940 15000 2000 15800 "" "4" ",onsolder"]
)
