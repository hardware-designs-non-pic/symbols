# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-5200-A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-5200-A
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -50000 -37500 0 100 ""]
(
ElementLine[-75000 3750 -62500 3750 260]
ElementLine[-62500 3750 -62500 -3750 260]
ElementLine[-75000 -3750 -62500 -3750 260]
ElementLine[-75000 3750 -75000 -3750 260]
ElementLine[62160 3750 75000 3750 260]
ElementLine[75000 3750 75000 -3750 260]
ElementLine[62160 -3750 75000 -3750 260]
ElementLine[62160 3750 62160 -3750 260]
ElementLine[-62500 -20000 -62500 20000 1000]
ElementLine[-62500 20000 -50000 20000 1000]
ElementLine[-50000 20000 -50000 30000 1000]
ElementLine[-50000 30000 50000 30000 1000]
ElementLine[50000 30000 50000 20000 1000]
ElementLine[50000 20000 62500 20000 1000]
ElementLine[62500 20000 62500 -20000 1000]
ElementLine[62500 -20000 50000 -20000 1000]
ElementLine[50000 -20000 50000 -30000 1000]
ElementLine[50000 -30000 -50000 -30000 1000]
ElementLine[-50000 -30000 -50000 -20000 1000]
ElementLine[-50000 -20000 -62500 -20000 1000]
ElementLine[75170 0 89560 0 7500]
ElementLine[-89560 0 -75170 0 7500]
ElementLine[-50000 -20000 -50000 20000 700]
ElementLine[50000 -20000 50000 20000 700]
Pin[-90000 0 25400 2000 26200 8590 "" "1" ""]
Pad[-90000 -12700 -90000 12700 25400 2000 26200 "" "1" ""]
Pad[-90000 -12700 -90000 12700 25400 2000 26200 "" "1" ",onsolder"]
Pin[90000 0 25400 2000 26200 8590 "" "2" ""]
Pad[90000 -12700 90000 12700 25400 2000 26200 "" "2" ""]
Pad[90000 -12700 90000 12700 25400 2000 26200 "" "2" ",onsolder"]
)
