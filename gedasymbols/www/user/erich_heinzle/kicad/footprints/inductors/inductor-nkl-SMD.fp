# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-SMD
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-SMD
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">name" "" "" 0 0 -1670 -13520 0 100 ""]
(
ElementLine[-25590 -33460 25590 -33460 500]
ElementLine[25590 -33460 25590 33460 500]
ElementLine[25590 33460 -25590 33460 500]
ElementLine[-25590 33460 -25590 -33460 500]
Pad[17710 -29130 17710 -25190 7080 2000 7880 "" "P$1" "square"]
Pad[5900 -29130 5900 -25190 7080 2000 7880 "" "P$2" "square"]
Pad[-5900 -29130 -5900 -25190 7080 2000 7880 "" "P$3" "square"]
Pad[-17710 -29130 -17710 -25190 7080 2000 7880 "" "P$4" "square"]
Pad[-17710 25190 -17710 29130 7080 2000 7880 "" "P$5" "square"]
Pad[-5900 25190 -5900 29130 7080 2000 7880 "" "P$6" "square"]
Pad[5900 25190 5900 29130 7080 2000 7880 "" "P$7" "square"]
Pad[17710 25190 17710 29130 7080 2000 7880 "" "P$8" "square"]
)
