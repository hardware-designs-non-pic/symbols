# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B19
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B19
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 8
#
Element["" ">name" "" "" 0 0 -12490 -7730 0 100 ""]
(
ElementLine[35430 -19680 35430 19680 500]
ElementLine[35430 19680 -35430 19680 500]
ElementLine[-35430 19680 -35430 -19680 500]
ElementLine[35430 -19680 -35430 -19680 500]
ElementArc[9840 -19680 5910 5910 0 360 500]
ElementArc[-9840 -19680 5900 5900 0 360 500]
ElementArc[-9840 19680 5910 5910 0 360 500]
ElementArc[9840 19680 5900 5900 0 360 500]
ElementArc[29520 19680 5910 5910 0 360 500]
ElementArc[-29520 19680 5900 5900 0 360 500]
ElementArc[-29520 -19680 5910 5910 0 360 500]
ElementArc[29520 -19680 5900 5900 0 360 500]
Pin[-29520 19680 15000 2000 15800 5000 "" "1" ""]
Pad[-29520 12180 -29520 27180 15000 2000 15800 "" "1" ""]
Pad[-29520 12180 -29520 27180 15000 2000 15800 "" "1" ",onsolder"]
Pin[-9840 19680 15000 2000 15800 5000 "" "2" ""]
Pad[-9840 12180 -9840 27180 15000 2000 15800 "" "2" ""]
Pad[-9840 12180 -9840 27180 15000 2000 15800 "" "2" ",onsolder"]
Pin[9840 19680 15000 2000 15800 5000 "" "3" ""]
Pad[9840 12180 9840 27180 15000 2000 15800 "" "3" ""]
Pad[9840 12180 9840 27180 15000 2000 15800 "" "3" ",onsolder"]
Pin[29520 19680 15000 2000 15800 5000 "" "4" ""]
Pad[29520 12180 29520 27180 15000 2000 15800 "" "4" ""]
Pad[29520 12180 29520 27180 15000 2000 15800 "" "4" ",onsolder"]
Pin[29520 -19680 15000 2000 15800 5000 "" "5" ""]
Pad[29520 -27180 29520 -12180 15000 2000 15800 "" "5" ""]
Pad[29520 -27180 29520 -12180 15000 2000 15800 "" "5" ",onsolder"]
Pin[9840 -19680 15000 2000 15800 5000 "" "6" ""]
Pad[9840 -27180 9840 -12180 15000 2000 15800 "" "6" ""]
Pad[9840 -27180 9840 -12180 15000 2000 15800 "" "6" ",onsolder"]
Pin[-9840 -19680 15000 2000 15800 5000 "" "7" ""]
Pad[-9840 -27180 -9840 -12180 15000 2000 15800 "" "7" ""]
Pad[-9840 -27180 -9840 -12180 15000 2000 15800 "" "7" ",onsolder"]
Pin[-29520 -19680 15000 2000 15800 5000 "" "8" ""]
Pad[-29520 -27180 -29520 -12180 15000 2000 15800 "" "8" ""]
Pad[-29520 -27180 -29520 -12180 15000 2000 15800 "" "8" ",onsolder"]
)
