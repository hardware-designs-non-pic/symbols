# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-FB-110
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-FB-110
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 6250 -11250 0 100 ""]
(
ElementLine[-6490 -6880 6490 -6880 700]
ElementLine[6490 -6880 6490 6880 700]
ElementLine[6490 6880 -6490 6880 700]
ElementLine[-6490 6880 -6490 -6880 700]
ElementLine[6290 -2360 -6290 -2360 0]
ElementLine[-6290 2360 6290 2360 0]
ElementLine[-12500 0 12500 0 1600]
Pin[-12500 0 7000 2000 7800 3600 "" "1" ""]
Pad[-12500 -3500 -12500 3500 7000 2000 7800 "" "1" ""]
Pad[-12500 -3500 -12500 3500 7000 2000 7800 "" "1" ",onsolder"]
Pin[12500 0 7000 2000 7800 3600 "" "2" ""]
Pad[12500 -3500 12500 3500 7000 2000 7800 "" "2" ""]
Pad[12500 -3500 12500 3500 7000 2000 7800 "" "2" ",onsolder"]
)
