# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B1-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B1-2
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">Name" "" "" 0 0 -680 -8720 0 100 ""]
(
ElementLine[-36410 -19680 -26570 -19680 500]
ElementLine[-26570 -19680 -23620 -24600 500]
ElementLine[-23620 -24600 -15740 -24600 500]
ElementLine[-15740 -24600 -12790 -19680 500]
ElementLine[-12790 -19680 12790 -19680 500]
ElementLine[12790 -19680 15740 -24600 500]
ElementLine[15740 -24600 23620 -24600 500]
ElementLine[23620 -24600 26570 -19680 500]
ElementLine[26570 -19680 36410 -19680 500]
ElementLine[36410 -19680 36410 19680 500]
ElementLine[36410 19680 26570 19680 500]
ElementLine[26570 19680 23620 24600 500]
ElementLine[23620 24600 15740 24600 500]
ElementLine[15740 24600 12790 19680 500]
ElementLine[12790 19680 -12790 19680 500]
ElementLine[-12790 19680 -15740 24600 500]
ElementLine[-15740 24600 -23620 24600 500]
ElementLine[-23620 24600 -26570 19680 500]
ElementLine[-26570 19680 -36410 19680 500]
ElementLine[-36410 19680 -36410 -19680 500]
Pin[-19680 -19680 15000 2000 15800 5000 "" "1" ""]
Pad[-19680 -27180 -19680 -12180 15000 2000 15800 "" "1" ""]
Pad[-19680 -27180 -19680 -12180 15000 2000 15800 "" "1" ",onsolder"]
Pin[-19680 19680 15000 2000 15800 5000 "" "2" ""]
Pad[-19680 12180 -19680 27180 15000 2000 15800 "" "2" ""]
Pad[-19680 12180 -19680 27180 15000 2000 15800 "" "2" ",onsolder"]
Pin[19680 -19680 15000 2000 15800 5000 "" "3" ""]
Pad[19680 -27180 19680 -12180 15000 2000 15800 "" "3" ""]
Pad[19680 -27180 19680 -12180 15000 2000 15800 "" "3" ",onsolder"]
Pin[19680 19680 15000 2000 15800 5000 "" "4" ""]
Pad[19680 12180 19680 27180 15000 2000 15800 "" "4" ""]
Pad[19680 12180 19680 27180 15000 2000 15800 "" "4" ",onsolder"]
)
