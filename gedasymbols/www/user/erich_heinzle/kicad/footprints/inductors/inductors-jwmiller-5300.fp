# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductors-jwmiller-5300
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductors-jwmiller-5300
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 -11250 -15000 0 100 ""]
(
ElementLine[-28430 930 -25000 930 260]
ElementLine[-25000 930 -25000 -930 260]
ElementLine[-28430 -930 -25000 -930 260]
ElementLine[-28430 930 -28430 -930 260]
ElementLine[25000 930 28430 930 260]
ElementLine[28430 930 28430 -930 260]
ElementLine[25000 -930 28430 -930 260]
ElementLine[25000 930 25000 -930 260]
ElementLine[-32500 0 -28750 0 1900]
ElementLine[32500 0 28750 0 1900]
ElementArc[0 -107840 100353 100353 0 360 700]
ElementArc[0 109090 101595 101595 0 360 700]
ElementArc[-15620 0 9221 9221 0 360 700]
ElementArc[15620 0 9221 9221 0 360 700]
Pin[-32500 0 7600 2000 8400 3600 "" "1" ""]
Pad[-32500 -3800 -32500 3800 7600 2000 8400 "" "1" ""]
Pad[-32500 -3800 -32500 3800 7600 2000 8400 "" "1" ",onsolder"]
Pin[32500 0 7600 2000 8400 3600 "" "2" ""]
Pad[32500 -3800 32500 3800 7600 2000 8400 "" "2" ""]
Pad[32500 -3800 32500 3800 7600 2000 8400 "" "2" ",onsolder"]
)
