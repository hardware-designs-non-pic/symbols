# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module inductor-nkl-B9
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: inductor-nkl-B9
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">value" "" "" 0 0 3030 9130 0 100 ""]
(
ElementArc[0 0 26446 26446 0 360 250]
Pin[-19680 24600 7000 2000 7800 5000 "" "1" ""]
Pad[-19680 21100 -19680 28100 7000 2000 7800 "" "1" ""]
Pad[-19680 21100 -19680 28100 7000 2000 7800 "" "1" ",onsolder"]
Pin[-19680 -24600 7000 2000 7800 5000 "" "2" ""]
Pad[-19680 -28100 -19680 -21100 7000 2000 7800 "" "2" ""]
Pad[-19680 -28100 -19680 -21100 7000 2000 7800 "" "2" ",onsolder"]
Pin[19680 24600 7000 2000 7800 5000 "" "3" ""]
Pad[19680 21100 19680 28100 7000 2000 7800 "" "3" ""]
Pad[19680 21100 19680 28100 7000 2000 7800 "" "3" ",onsolder"]
Pin[19680 -24600 7000 2000 7800 5000 "" "4" ""]
Pad[19680 -28100 19680 -21100 7000 2000 7800 "" "4" ""]
Pad[19680 -28100 19680 -21100 7000 2000 7800 "" "4" ",onsolder"]
Pin[0 31490 7000 2000 7800 5000 "" "5" ""]
Pad[0 27990 0 34990 7000 2000 7800 "" "5" ""]
Pad[0 27990 0 34990 7000 2000 7800 "" "5" ",onsolder"]
Pin[0 -31490 7000 2000 7800 5000 "" "6" ""]
Pad[0 -34990 0 -27990 7000 2000 7800 "" "6" ""]
Pad[0 -34990 0 -27990 7000 2000 7800 "" "6" ",onsolder"]
)
