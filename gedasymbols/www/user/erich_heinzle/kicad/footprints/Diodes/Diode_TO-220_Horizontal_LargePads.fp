# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_TO-220_Horizontal_LargePads
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_TO-220_Horizontal_LargePads
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "D" "" "" 0 0 1000 -36000 0 100 ""]
(
ElementLine[-2000 -25000 -10000 -25000 1500]
ElementLine[-10000 -25000 -10000 -14500 1500]
ElementLine[3500 -25000 10000 -25000 1500]
ElementLine[10000 -25000 10000 -14500 1500]
ElementLine[-2000 -25000 3500 -29000 1500]
ElementLine[3500 -29000 3500 -21000 1500]
ElementLine[3500 -21000 -2000 -25000 1500]
ElementLine[-2000 -29000 -2000 -21000 1500]
ElementLine[-10000 -14500 -10000 -9000 1500]
ElementLine[10000 -14500 10000 -9000 1500]
ElementLine[21000 -48000 21000 -79500 1500]
ElementLine[21000 -79500 -21000 -79500 1500]
ElementLine[-21000 -79500 -21000 -48000 1500]
ElementLine[21000 -14500 21000 -48000 1500]
ElementLine[21000 -48000 -21000 -48000 1500]
ElementLine[-21000 -48000 -21000 -14500 1500]
ElementLine[0 -14500 -21000 -14500 1500]
ElementLine[0 -14500 21000 -14500 1500]
ElementArc[0 -66000 9899 9899 0 360 1500]
Pin[-10000 0 6690 2000 7490 3940 "" "2" "blah"]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "2" "square"]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "2" "square,onsolder"]
Pin[10000 0 6690 2000 7490 3940 "" "1" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "1" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "1" ",onsolder"]
Pin[0 -66000 14960 2000 15760 14960 "" "2" ""]
)
