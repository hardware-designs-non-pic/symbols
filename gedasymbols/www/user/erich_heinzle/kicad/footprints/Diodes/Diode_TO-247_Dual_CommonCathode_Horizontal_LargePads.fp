# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_TO-247_Dual_CommonCathode_Horizontal_LargePads
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_TO-247_Dual_CommonCathode_Horizontal_LargePads
# Text descriptor count: 1
# Draw segment object count: 21
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "D" "" "" 0 0 0 -58660 0 100 ""]
(
ElementLine[-11810 -39760 -22050 -39760 1500]
ElementLine[-22050 -39760 -22050 -22440 1500]
ElementLine[11420 -39760 22050 -39760 1500]
ElementLine[22050 -39760 22050 -22440 1500]
ElementLine[0 -39760 0 -22440 1500]
ElementLine[-6690 -39760 -11810 -44880 1500]
ElementLine[-11810 -44880 -11810 -34650 1500]
ElementLine[-11810 -34650 -6690 -39760 1500]
ElementLine[6300 -39760 11420 -44880 1500]
ElementLine[11420 -44880 11420 -34650 1500]
ElementLine[11420 -34650 6300 -39760 1500]
ElementLine[-6690 -39760 6300 -39760 1500]
ElementLine[-6690 -44880 -6690 -34650 1500]
ElementLine[6300 -44880 6300 -34650 1500]
ElementLine[-22000 -13000 -22000 -20000 1500]
ElementLine[0 -13000 0 -20000 1500]
ElementLine[22000 -13000 22000 -20000 1500]
ElementLine[32000 -101000 32000 -20000 1500]
ElementLine[-32000 -101000 32000 -101000 1500]
ElementLine[-32000 -20000 -32000 -101000 1500]
ElementLine[32000 -20000 -32000 -20000 1500]
ElementArc[0 -77000 7000 7000 0 360 1500]
Pin[0 0 13780 2000 14580 5910 "" "2" "blah"]
Pad[0 -3940 0 3940 13780 2000 14580 "" "2" "square"]
Pad[0 -3940 0 3940 13780 2000 14580 "" "2" "square,onsolder"]
Pin[-22000 0 13780 2000 14580 5910 "" "1" ""]
Pad[-22000 -3940 -22000 3940 13780 2000 14580 "" "1" ""]
Pad[-22000 -3940 -22000 3940 13780 2000 14580 "" "1" ",onsolder"]
Pin[22000 0 13780 2000 14580 5910 "" "3" ""]
Pad[22000 -3940 22000 3940 13780 2000 14580 "" "3" ""]
Pad[22000 -3940 22000 3940 13780 2000 14580 "" "3" ",onsolder"]
Pin[0 -77000 15750 2000 16550 15750 "" "9" ""]
)
