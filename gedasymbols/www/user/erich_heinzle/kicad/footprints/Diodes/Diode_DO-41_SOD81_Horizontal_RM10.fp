# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_DO-41_SOD81_Horizontal_RM10
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_DO-41_SOD81_Horizontal_RM10
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 10000 0 100 ""]
(
ElementLine[-10000 0 -14000 0 1500]
ElementLine[9000 0 14000 0 1500]
ElementLine[8000 -5000 8000 5000 1000]
ElementLine[7000 -5000 7000 5000 1000]
ElementLine[6000 -5000 6000 5000 1000]
ElementLine[9000 -5000 9000 5000 1000]
ElementLine[5000 -5000 10000 5000 1000]
ElementLine[10000 -5000 5000 5000 1000]
ElementLine[5000 -5000 5000 5000 1000]
ElementLine[7500 -5000 7500 5000 1000]
ElementLine[10000 5000 10000 -5000 1000]
ElementLine[10000 -5000 -10000 -5000 1000]
ElementLine[-10000 -5000 -10000 5000 1000]
ElementLine[-10000 5000 10000 5000 1000]
Pin[-20000 0 7870 2000 8670 5000 "" "1" ""]
Pin[20000 0 7870 2000 8670 3940 "" "2" "square"]
)
