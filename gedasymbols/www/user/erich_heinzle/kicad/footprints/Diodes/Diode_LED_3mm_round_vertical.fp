# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_LED_3mm_round_vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_LED_3mm_round_vertical
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 25000 0 100 ""]
(
ElementLine[-2500 5500 -1000 6000 1500]
ElementLine[-1000 6000 500 6000 1500]
ElementLine[500 6000 2500 5500 1500]
ElementLine[-2500 -5500 -1000 -6000 1500]
ElementLine[-1000 -6000 500 -6000 1500]
ElementLine[500 -6000 2500 -5500 1500]
Pin[-5000 0 7870 2000 8670 3940 "" "1" ""]
Pin[5000 0 6690 2000 7490 3940 "" "2" "square"]
)
