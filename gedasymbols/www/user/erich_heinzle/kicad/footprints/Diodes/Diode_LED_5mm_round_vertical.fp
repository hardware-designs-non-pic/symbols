# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_LED_5mm_round_vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_LED_5mm_round_vertical
# Text descriptor count: 1
# Draw segment object count: 47
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 25000 0 100 ""]
(
ElementLine[-10000 7500 -10000 3000 1500]
ElementLine[-10000 -8500 -10000 -3000 1500]
ElementLine[-8500 5000 -7500 6500 1500]
ElementLine[-7500 6500 -6000 8000 1500]
ElementLine[-6000 8000 -4000 9000 1500]
ElementLine[-4000 9000 -1000 10000 1500]
ElementLine[-1000 10000 500 10000 1500]
ElementLine[500 10000 3000 9500 1500]
ElementLine[3000 9500 6000 8000 1500]
ElementLine[6000 8000 7000 7000 1500]
ElementLine[7000 7000 8500 5500 1500]
ElementLine[5500 -8500 7000 -7000 1500]
ElementLine[7000 -7000 8000 -6000 1500]
ElementLine[8000 -6000 8500 -5500 1500]
ElementLine[-8500 -5000 -8000 -6000 1500]
ElementLine[-8000 -6000 -6500 -7500 1500]
ElementLine[-6500 -7500 -4500 -9000 1500]
ElementLine[-4500 -9000 -3000 -9500 1500]
ElementLine[-3000 -9500 -1000 -10000 1500]
ElementLine[-1000 -10000 500 -10000 1500]
ElementLine[500 -10000 3000 -9500 1500]
ElementLine[3000 -9500 5500 -8500 1500]
ElementLine[-10000 7500 -8500 9000 1500]
ElementLine[-9500 -8500 -8000 -9500 1500]
ElementLine[-8000 -9500 -6000 -11000 1500]
ElementLine[-6000 -11000 -3500 -12000 1500]
ElementLine[-3500 -12000 -1000 -12500 1500]
ElementLine[-1000 -12500 1000 -12500 1500]
ElementLine[1000 -12500 3500 -12000 1500]
ElementLine[3500 -12000 6000 -11000 1500]
ElementLine[6000 -11000 8000 -9500 1500]
ElementLine[8000 -9500 10000 -7500 1500]
ElementLine[10000 -7500 11500 -5000 1500]
ElementLine[11500 -5000 12000 -3500 1500]
ElementLine[12000 -3500 12500 -1000 1500]
ElementLine[12500 -1000 12500 1500 1500]
ElementLine[12500 1500 12000 3500 1500]
ElementLine[12000 3500 11000 6000 1500]
ElementLine[11000 6000 9500 8000 1500]
ElementLine[9500 8000 7500 10000 1500]
ElementLine[7500 10000 6000 11000 1500]
ElementLine[6000 11000 3500 12000 1500]
ElementLine[3500 12000 1000 12500 1500]
ElementLine[1000 12500 -1500 12500 1500]
ElementLine[-1500 12500 -4000 12000 1500]
ElementLine[-4000 12000 -7000 10500 1500]
ElementLine[-7000 10500 -8500 9000 1500]
Pin[-5000 0 7870 2000 8670 3940 "" "1" ""]
Pin[5000 0 6690 2000 7490 3940 "" "2" "square"]
)
