# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_P600_Vertical_AnodeUp
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_P600_Vertical_AnodeUp
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 27500 0 100 ""]
(
ElementLine[9000 0 21000 0 1500]
ElementLine[12500 0 17500 0 1000]
ElementLine[15000 -2500 15000 2500 1000]
ElementLine[12500 0 17500 5000 1000]
ElementLine[12500 0 17500 -5000 1000]
ElementLine[12500 -5000 12500 5000 1000]
ElementLine[17500 -5000 17500 5000 1000]
ElementArc[0 0 20000 20000 0 360 1000]
Pin[30000 0 13940 2000 14740 6300 "" "1" ""]
Pin[0 0 13940 2000 14740 6300 "" "2" "square"]
)
