# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_TO-220_Dual_CommonCathode_Horizontal
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_TO-220_Dual_CommonCathode_Horizontal
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "D" "" "" 0 0 1000 -36000 0 100 ""]
(
ElementLine[120 -25830 120 -16930 1500]
ElementLine[-8190 -25630 -12600 -25750 1500]
ElementLine[-12600 -25750 -12600 -17050 1500]
ElementLine[9210 -25750 12910 -25830 1500]
ElementLine[12910 -25830 12910 -17050 1500]
ElementLine[-4880 -25630 5710 -25830 1500]
ElementLine[-4800 -25830 -8390 -28820 1500]
ElementLine[-8390 -28820 -8390 -22320 1500]
ElementLine[-8390 -22320 -4800 -25750 1500]
ElementLine[5710 -26340 9090 -29020 1500]
ElementLine[9090 -29020 9090 -22830 1500]
ElementLine[9090 -22830 5910 -25630 1500]
ElementLine[-4720 -28980 -4720 -22600 1500]
ElementLine[5670 -28980 5670 -22910 1500]
ElementLine[-10000 -14500 -10000 -7500 1500]
ElementLine[0 -14500 0 -7500 1500]
ElementLine[10000 -14500 10000 -7500 1500]
ElementLine[21000 -48000 21000 -79500 1500]
ElementLine[21000 -79500 -21000 -79500 1500]
ElementLine[-21000 -79500 -21000 -48000 1500]
ElementLine[21000 -14500 21000 -48000 1500]
ElementLine[21000 -48000 -21000 -48000 1500]
ElementLine[-21000 -48000 -21000 -14500 1500]
ElementLine[0 -14500 -21000 -14500 1500]
ElementLine[0 -14500 21000 -14500 1500]
ElementArc[0 -66000 9899 9899 0 360 1500]
Pin[0 0 5910 2000 6710 3940 "" "2" "blah"]
Pad[0 -1965 0 1965 5910 2000 6710 "" "2" "square"]
Pad[0 -1965 0 1965 5910 2000 6710 "" "2" "square,onsolder"]
Pin[-10000 0 5910 2000 6710 3940 "" "1" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "1" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "1" ",onsolder"]
Pin[10000 0 5910 2000 6710 3940 "" "3" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "3" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "3" ",onsolder"]
Pin[0 -66000 14960 2000 15760 14960 "" "9" ""]
)
