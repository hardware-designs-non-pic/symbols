# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_TO-247_Vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_TO-247_Vertical
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-4000 0 -14000 0 1500]
ElementLine[4000 0 14000 0 1500]
ElementLine[-4000 0 4000 -6000 1500]
ElementLine[4000 -6000 4000 6000 1500]
ElementLine[4000 6000 -4000 0 1500]
ElementLine[-4000 -6000 -4000 6000 1500]
ElementLine[32000 11000 32000 -11000 1500]
ElementLine[32000 -11000 -32000 -11000 1500]
ElementLine[-32000 -11000 -32000 11000 1500]
ElementLine[-32000 11000 32000 11000 1500]
Pin[-22000 0 9840 2000 10640 5910 "" "2" "blah"]
Pad[-22000 -3940 -22000 3940 9840 2000 10640 "" "2" "square"]
Pad[-22000 -3940 -22000 3940 9840 2000 10640 "" "2" "square,onsolder"]
Pin[22000 0 9840 2000 10640 5910 "" "1" ""]
Pad[22000 -3940 22000 3940 9840 2000 10640 "" "1" ""]
Pad[22000 -3940 22000 3940 9840 2000 10640 "" "1" ",onsolder"]
)
