# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_TO-247_Horizontal_MountFromLS
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_TO-247_Horizontal_MountFromLS
# Text descriptor count: 1
# Draw segment object count: 51
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "D" "" "" 0 0 -4000 -108000 0 100 ""]
(
ElementLine[-4000 -34000 -22000 -34000 1500]
ElementLine[-22000 -34000 -22000 -20000 1500]
ElementLine[4000 -34000 22000 -34000 1500]
ElementLine[22000 -34000 22000 -20000 1500]
ElementLine[4000 -34000 -4000 -40000 1500]
ElementLine[-4000 -40000 -4000 -28000 1500]
ElementLine[-4000 -28000 4000 -34000 1500]
ElementLine[4000 -40000 4000 -28000 1500]
ElementLine[-32000 -23000 -32000 -20000 1500]
ElementLine[-32000 -31000 -32000 -27000 1500]
ElementLine[-32000 -27000 -32000 -28000 1500]
ElementLine[-32000 -41000 -32000 -36000 1500]
ElementLine[-32000 -36000 -32000 -35000 1500]
ElementLine[-32000 -51000 -32000 -46000 1500]
ElementLine[-32000 -61000 -32000 -56000 1500]
ElementLine[-32000 -71000 -32000 -66000 1500]
ElementLine[-32000 -81000 -32000 -76000 1500]
ElementLine[-32000 -91000 -32000 -86000 1500]
ElementLine[-32000 -101000 -32000 -96000 1500]
ElementLine[32000 -25000 32000 -21000 1500]
ElementLine[32000 -35000 32000 -30000 1500]
ElementLine[32000 -30000 32000 -29000 1500]
ElementLine[32000 -45000 32000 -40000 1500]
ElementLine[32000 -57000 32000 -51000 1500]
ElementLine[32000 -69000 32000 -63000 1500]
ElementLine[32000 -82000 32000 -75000 1500]
ElementLine[32000 -92000 32000 -87000 1500]
ElementLine[32000 -101000 32000 -97000 1500]
ElementLine[32000 -97000 32000 -96000 1500]
ElementLine[29000 -101000 32000 -101000 1500]
ElementLine[23000 -101000 26000 -101000 1500]
ElementLine[13000 -101000 18000 -101000 1500]
ElementLine[3000 -101000 9000 -101000 1500]
ElementLine[-6000 -101000 -1000 -101000 1500]
ElementLine[-15000 -101000 -10000 -101000 1500]
ElementLine[-24000 -101000 -19000 -101000 1500]
ElementLine[-32000 -101000 -28000 -101000 1500]
ElementLine[24000 -20000 18000 -20000 1500]
ElementLine[18000 -20000 19000 -20000 1500]
ElementLine[32000 -20000 29000 -20000 1500]
ElementLine[29000 -20000 28000 -20000 1500]
ElementLine[7000 -20000 12000 -20000 1500]
ElementLine[-3000 -20000 2000 -20000 1500]
ElementLine[-13000 -20000 -7000 -20000 1500]
ElementLine[-24000 -20000 -19000 -20000 1500]
ElementLine[-19000 -20000 -18000 -20000 1500]
ElementLine[-18000 -20000 -17000 -20000 1500]
ElementLine[-32000 -20000 -29000 -20000 1500]
ElementLine[-29000 -20000 -28000 -20000 1500]
ElementLine[-22000 -11000 -22000 -20000 1500]
ElementLine[22000 -11000 22000 -20000 1500]
ElementArc[0 -77000 7000 7000 0 360 1500]
Pin[-22000 0 9840 2000 10640 5910 "" "1" ""]
Pad[-22000 -3940 -22000 3940 9840 2000 10640 "" "1" ""]
Pad[-22000 -3940 -22000 3940 9840 2000 10640 "" "1" ",onsolder"]
Pin[22000 0 9840 2000 10640 5910 "" "2" "blah"]
Pad[22000 -3940 22000 3940 9840 2000 10640 "" "2" "square"]
Pad[22000 -3940 22000 3940 9840 2000 10640 "" "2" "square,onsolder"]
Pin[0 -77250 15750 2000 16550 15750 "" "9" ""]
)
