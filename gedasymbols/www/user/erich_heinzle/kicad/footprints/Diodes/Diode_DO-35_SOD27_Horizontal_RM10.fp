# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Diode_DO-35_SOD27_Horizontal_RM10
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Diode_DO-35_SOD27_Horizontal_RM10
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "D" "" "" 0 0 0 10000 0 100 ""]
(
ElementLine[-9000 0 -14500 0 1500]
ElementLine[8500 0 14500 0 1500]
ElementLine[7000 -3000 7000 3000 1000]
ElementLine[8000 -3000 8000 3000 1000]
ElementLine[9000 0 9000 3000 1000]
ElementLine[9000 3000 -9000 3000 1000]
ElementLine[-9000 3000 -9000 -3000 1000]
ElementLine[-9000 -3000 9000 -3000 1000]
ElementLine[9000 -3000 9000 0 1000]
Pin[-20000 0 6690 2000 7490 2760 "" "1" ""]
Pin[20000 0 6690 2000 7490 2760 "" "2" "square"]
)
