# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module MagneticBuzzer_Kingstate_KCG0601_RevA_25Oct2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: MagneticBuzzer_Kingstate_KCG0601_RevA_25Oct2010
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "W" "" "" 0 0 0 -19690 0 100 ""]
(
ElementLine[4920 6890 8860 6890 1500]
ElementLine[6890 4920 6890 8860 1500]
ElementLine[2760 2760 2360 3150 1500]
ElementLine[2360 3150 1570 3540 1500]
ElementLine[1570 3540 390 3940 1500]
ElementLine[390 3940 -390 3940 1500]
ElementLine[-390 3940 -1570 3540 1500]
ElementLine[-1570 3540 -2360 3150 1500]
ElementLine[2360 -3150 1570 -3540 1500]
ElementLine[1570 -3540 390 -3940 1500]
ElementLine[390 -3940 -390 -3940 1500]
ElementLine[-390 -3940 -1570 -3540 1500]
ElementLine[-1570 -3540 -2360 -3150 1500]
ElementArc[0 0 12990 12990 0 360 1500]
Pin[-6300 0 6300 2000 7100 3940 "" "1" ""]
Pin[6300 0 6300 2000 7100 3940 "" "2" ""]
)
