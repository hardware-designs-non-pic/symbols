# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module MagneticBuzzer_StarMicronics_HMB-06_HMB-12_RevA_25Oct2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: MagneticBuzzer_StarMicronics_HMB-06_HMB-12_RevA_25Oct2010
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 2
#
Element["" "W" "" "" 0 0 0 -39370 0 100 ""]
(
ElementLine[23620 -3940 23620 3940 1500]
ElementLine[19690 0 27560 0 1500]
ElementArc[0 0 6690 6690 0 360 1500]
ElementArc[0 0 27560 27560 0 360 1500]
ElementArc[0 0 31500 31500 0 360 1500]
Pin[-14960 0 6300 2000 7100 3940 "" "1" ""]
Pin[14960 0 6300 2000 7100 3940 "" "2" ""]
)
