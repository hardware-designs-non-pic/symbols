# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module MagneticBuzzer_ProjectsUnlimited_AI-4228-TWT-R_RevA_25Oct2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: MagneticBuzzer_ProjectsUnlimited_AI-4228-TWT-R_RevA_25Oct2010
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 9
# Draw arc object count: 0
# Pad count: 2
#
Element["" "W" "" "" 0 0 -3940 -94490 0 100 ""]
(
ElementLine[72830 0 72830 5910 1500]
ElementLine[72830 0 72830 -5910 1500]
ElementLine[72830 0 66930 0 1500]
ElementLine[66930 0 78740 0 1500]
ElementArc[0 0 10599 10599 0 360 1500]
ElementArc[0 0 12450 12450 0 360 1500]
ElementArc[0 0 23702 23702 0 360 1500]
ElementArc[0 0 25892 25892 0 360 1500]
ElementArc[0 0 35430 35430 0 360 1500]
ElementArc[0 0 37400 37400 0 360 1500]
ElementArc[0 0 61147 61147 0 360 1500]
ElementArc[0 0 62990 62990 0 360 1500]
ElementArc[0 0 82680 82680 0 360 1500]
Pin[-50000 0 11810 2000 12610 5910 "" "1" ""]
Pin[50000 0 11810 2000 12610 5910 "" "2" ""]
)
