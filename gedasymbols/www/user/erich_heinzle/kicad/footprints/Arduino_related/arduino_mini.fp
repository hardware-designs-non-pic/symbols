# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yann Jautard
# No warranties express or implied
# Footprint converted from Kicad Module arduino_mini
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: arduino_mini
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 30
#
Element["" "U***" "" "" 0 0 -55000 -15000 0 100 ""]
(
ElementLine[-90000 -25000 80000 -25000 1500]
ElementLine[80000 -25000 80000 25000 1500]
ElementLine[80000 25000 -90000 25000 1500]
ElementLine[-90000 25000 -90000 -25000 1500]
ElementLine[-90000 5000 -85000 5000 1500]
ElementLine[-85000 5000 -85000 -5000 1500]
ElementLine[-85000 -5000 -90000 -5000 1500]
Pin[-75000 30000 6200 2000 7000 3200 "" "1" "square"]
Pin[-65000 30000 6200 2000 7000 3200 "" "2" ""]
Pin[-55000 30000 6200 2000 7000 3200 "" "3" ""]
Pin[-45000 30000 6200 2000 7000 3200 "" "4" ""]
Pin[-35000 30000 6200 2000 7000 3200 "" "5" ""]
Pin[-25000 30000 6200 2000 7000 3200 "" "6" ""]
Pin[-15000 30000 6200 2000 7000 3200 "" "7" ""]
Pin[-5000 30000 6200 2000 7000 3200 "" "8" ""]
Pin[5000 30000 6200 2000 7000 3200 "" "9" ""]
Pin[15000 30000 6200 2000 7000 3200 "" "10" ""]
Pin[25000 30000 6200 2000 7000 3200 "" "11" ""]
Pin[35000 30000 6200 2000 7000 3200 "" "12" ""]
Pin[45000 30000 6200 2000 7000 3200 "" "13" ""]
Pin[55000 30000 6200 2000 7000 3200 "" "14" ""]
Pin[65000 30000 6200 2000 7000 3200 "" "15" ""]
Pin[65000 -30000 6200 2000 7000 3200 "" "16" ""]
Pin[55000 -30000 6200 2000 7000 3200 "" "17" ""]
Pin[45000 -30000 6200 2000 7000 3200 "" "18" ""]
Pin[35000 -30000 6200 2000 7000 3200 "" "19" ""]
Pin[25000 -30000 6200 2000 7000 3200 "" "20" ""]
Pin[15000 -30000 6200 2000 7000 3200 "" "21" ""]
Pin[5000 -30000 6200 2000 7000 3200 "" "22" ""]
Pin[-5000 -30000 6200 2000 7000 3200 "" "23" ""]
Pin[-15000 -30000 6200 2000 7000 3200 "" "24" ""]
Pin[-25000 -30000 6200 2000 7000 3200 "" "25" ""]
Pin[-35000 -30000 6200 2000 7000 3200 "" "26" ""]
Pin[-45000 -30000 6200 2000 7000 3200 "" "27" ""]
Pin[-55000 -30000 6200 2000 7000 3200 "" "28" ""]
Pin[-65000 -30000 6200 2000 7000 3200 "" "29" ""]
Pin[-75000 -30000 6200 2000 7000 3200 "" "30" ""]
)
