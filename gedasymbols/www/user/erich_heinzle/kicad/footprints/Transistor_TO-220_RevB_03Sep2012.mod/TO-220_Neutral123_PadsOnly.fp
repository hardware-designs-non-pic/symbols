# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module TO-220_Neutral123_PadsOnly
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TO-220_Neutral123_PadsOnly
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "IC" "" "" 0 0 200 -13390 0 100 ""]
(
Pin[0 0 5910 2000 6710 3940 "" "2" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "2" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "2" ",onsolder"]
Pin[-10000 0 5910 2000 6710 3940 "" "1" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "1" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "1" ",onsolder"]
Pin[10000 0 5910 2000 6710 3940 "" "3" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "3" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "3" ",onsolder"]
)
