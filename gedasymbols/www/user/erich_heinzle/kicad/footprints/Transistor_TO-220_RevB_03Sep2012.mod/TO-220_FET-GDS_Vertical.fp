# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module TO-220_FET-GDS_Vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TO-220_FET-GDS_Vertical
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "T" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-6000 -12000 -6000 -7500 1500]
ElementLine[6000 -12000 6000 -7500 1500]
ElementLine[21000 -7500 21000 7000 1500]
ElementLine[21000 7000 -21000 7000 1500]
ElementLine[-21000 7000 -21000 -7500 1500]
ElementLine[21000 -12000 21000 -7500 1500]
ElementLine[21000 -7500 -21000 -7500 1500]
ElementLine[-21000 -7500 -21000 -12000 1500]
ElementLine[0 -12000 -21000 -12000 1500]
ElementLine[0 -12000 21000 -12000 1500]
Pin[0 0 5910 2000 6710 3940 "" "D" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "D" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "D" ",onsolder"]
Pin[-10000 0 5910 2000 6710 3940 "" "G" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "G" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "G" ",onsolder"]
Pin[10000 0 5910 2000 6710 3940 "" "S" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "S" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "S" ",onsolder"]
)
