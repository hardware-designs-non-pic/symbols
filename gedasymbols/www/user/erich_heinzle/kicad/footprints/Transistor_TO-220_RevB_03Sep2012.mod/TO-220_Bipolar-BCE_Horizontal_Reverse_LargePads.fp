# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module TO-220_Bipolar-BCE_Horizontal_Reverse_LargePads
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TO-220_Bipolar-BCE_Horizontal_Reverse_LargePads
# Text descriptor count: 1
# Draw segment object count: 15
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "T" "" "" 0 0 790 -86610 0 100 ""]
(
ElementLine[-10000 -14500 -10000 -9500 1500]
ElementLine[0 -14500 0 -9500 1500]
ElementLine[10000 -14500 10000 -9500 1500]
ElementLine[16400 -48100 21100 -48100 1500]
ElementLine[-16500 -48000 -21000 -48000 1500]
ElementLine[-16500 -21300 -16500 -47900 1500]
ElementLine[16400 -48000 16400 -21300 1500]
ElementLine[16400 -21300 -16400 -21300 1500]
ElementLine[21000 -48000 21000 -79500 1500]
ElementLine[21000 -79500 -21000 -79500 1500]
ElementLine[-21000 -79500 -21000 -48000 1500]
ElementLine[21000 -14500 21000 -48000 1500]
ElementLine[-21000 -48000 -21000 -14500 1500]
ElementLine[0 -14500 -21000 -14500 1500]
ElementLine[0 -14500 21000 -14500 1500]
ElementArc[0 -66000 9899 9899 0 360 1500]
Pin[0 0 6690 2000 7490 3940 "" "C" ""]
Pad[0 -3545 0 3545 6690 2000 7490 "" "C" ""]
Pad[0 -3545 0 3545 6690 2000 7490 "" "C" ",onsolder"]
Pin[-10000 0 6690 2000 7490 3940 "" "E" ""]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "E" ""]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "E" ",onsolder"]
Pin[10000 0 6690 2000 7490 3940 "" "B" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "B" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "B" ",onsolder"]
Pin[0 -66000 14960 2000 15760 14960 "" "" "hole"]
)
