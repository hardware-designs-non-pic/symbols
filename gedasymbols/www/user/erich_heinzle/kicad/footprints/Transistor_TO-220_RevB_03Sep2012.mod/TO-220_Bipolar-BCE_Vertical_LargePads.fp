# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module TO-220_Bipolar-BCE_Vertical_LargePads
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TO-220_Bipolar-BCE_Vertical_LargePads
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "T" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[21000 -7500 13500 -7500 1500]
ElementLine[3500 -7500 6500 -7500 1500]
ElementLine[-6000 -7500 -6500 -7500 1500]
ElementLine[-6000 -7500 -3500 -7500 1500]
ElementLine[-21000 -7500 -14000 -7500 1500]
ElementLine[-21000 7000 -14500 7000 1500]
ElementLine[-4000 7500 -6500 7500 1500]
ElementLine[6000 7500 3500 7500 1500]
ElementLine[21000 7000 14500 7000 1500]
ElementLine[-6000 -12000 -6000 -7500 1500]
ElementLine[6000 -12000 6000 -7500 1500]
ElementLine[21000 -7500 21000 7000 1500]
ElementLine[-21000 7000 -21000 -7500 1500]
ElementLine[21000 -12000 21000 -7500 1500]
ElementLine[-21000 -7500 -21000 -12000 1500]
ElementLine[0 -12000 -21000 -12000 1500]
ElementLine[0 -12000 21000 -12000 1500]
Pin[0 0 6690 2000 7490 3940 "" "C" ""]
Pad[0 -3545 0 3545 6690 2000 7490 "" "C" ""]
Pad[0 -3545 0 3545 6690 2000 7490 "" "C" ",onsolder"]
Pin[-10000 0 6690 2000 7490 3940 "" "B" ""]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "B" ""]
Pad[-10000 -3545 -10000 3545 6690 2000 7490 "" "B" ",onsolder"]
Pin[10000 0 6690 2000 7490 3940 "" "E" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "E" ""]
Pad[10000 -3545 10000 3545 6690 2000 7490 "" "E" ",onsolder"]
)
