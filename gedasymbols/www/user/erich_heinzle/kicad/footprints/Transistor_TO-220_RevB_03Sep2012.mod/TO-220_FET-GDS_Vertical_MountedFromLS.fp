# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module TO-220_FET-GDS_Vertical_MountedFromLS
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TO-220_FET-GDS_Vertical_MountedFromLS
# Text descriptor count: 1
# Draw segment object count: 43
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "T" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-6000 -12000 -6000 -10000 1500]
ElementLine[6000 -12000 6000 -10000 1500]
ElementLine[18500 -7500 21000 -7500 1500]
ElementLine[14000 -7500 16000 -7500 1500]
ElementLine[9500 -7500 11500 -7500 1500]
ElementLine[5000 -7500 7000 -7500 1500]
ElementLine[500 -7500 2500 -7500 1500]
ElementLine[-4500 -7500 -2000 -7500 1500]
ElementLine[-18500 -7500 -21000 -7500 1500]
ElementLine[-15000 -7500 -16500 -7500 1500]
ElementLine[-11000 -7500 -13000 -7500 1500]
ElementLine[-6000 -7500 -9000 -7500 1500]
ElementLine[18500 -12000 21000 -12000 1500]
ElementLine[14000 -12000 16000 -12000 1500]
ElementLine[9500 -12000 11500 -12000 1500]
ElementLine[5000 -12000 7000 -12000 1500]
ElementLine[500 -12000 2500 -12000 1500]
ElementLine[-4500 -12000 -2000 -12000 1500]
ElementLine[-9000 -12000 -6500 -12000 1500]
ElementLine[-13000 -12000 -11000 -12000 1500]
ElementLine[-16500 -12000 -15000 -12000 1500]
ElementLine[-21000 -12000 -18500 -12000 1500]
ElementLine[21000 5000 21000 7000 1500]
ElementLine[21000 1000 21000 3000 1500]
ElementLine[21000 -3000 21000 -1000 1500]
ElementLine[21000 -7500 21000 -5500 1500]
ElementLine[21000 -12000 21000 -10000 1500]
ElementLine[-21000 -10000 -21000 -12000 1500]
ElementLine[-21000 -5500 -21000 -7500 1500]
ElementLine[-21000 -1000 -21000 -3000 1500]
ElementLine[-21000 3000 -21000 1000 1500]
ElementLine[-21000 7000 -21000 5000 1500]
ElementLine[18500 7000 21000 7000 1500]
ElementLine[14000 7000 16000 7000 1500]
ElementLine[9500 7000 11500 7000 1500]
ElementLine[5000 7000 7000 7000 1500]
ElementLine[500 7000 2500 7000 1500]
ElementLine[-4500 7000 -2000 7000 1500]
ElementLine[-9000 7000 -7000 7000 1500]
ElementLine[-13000 7000 -11000 7000 1500]
ElementLine[-16500 7000 -15000 7000 1500]
ElementLine[-21000 7000 -19500 7000 1500]
ElementLine[-19500 7000 -18500 7000 1500]
Pin[0 0 5910 2000 6710 3940 "" "D" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "D" ""]
Pad[0 -1965 0 1965 5910 2000 6710 "" "D" ",onsolder"]
Pin[-10000 0 5910 2000 6710 3940 "" "S" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "S" ""]
Pad[-10000 -1965 -10000 1965 5910 2000 6710 "" "S" ",onsolder"]
Pin[10000 0 5910 2000 6710 3940 "" "G" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "G" ""]
Pad[10000 -1965 10000 1965 5910 2000 6710 "" "G" ",onsolder"]
)
