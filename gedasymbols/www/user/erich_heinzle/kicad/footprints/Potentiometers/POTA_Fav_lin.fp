# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POTA_Fav_lin
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POTA_Fav_lin
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POTA_Fav_Lin" "" "" 0 0 20000 -30000 0 100 ""]
(
ElementLine[-30000 -20000 -20000 20000 1500]
ElementLine[-20000 20000 -30000 20000 1500]
ElementLine[-30000 20000 -20000 -20000 1500]
ElementLine[-30000 -10000 -30000 10000 1500]
ElementLine[-30000 10000 -20000 10000 1500]
ElementLine[-20000 10000 -20000 -10000 1500]
ElementLine[-20000 -10000 -20000 -20000 1500]
ElementLine[-20000 -20000 -30000 -20000 1500]
ElementLine[-30000 -20000 -30000 20000 1500]
ElementLine[-30000 20000 -20000 20000 1500]
ElementLine[-20000 20000 -20000 10000 1500]
ElementLine[-100000 -10000 120000 -10000 1500]
ElementLine[120000 -10000 120000 10000 1500]
ElementLine[120000 10000 -100000 10000 1500]
ElementLine[-100000 10000 -100000 -10000 1500]
ElementLine[-130000 -20000 140000 -20000 1500]
ElementLine[140000 -20000 140000 20000 1500]
ElementLine[140000 20000 -130000 20000 1500]
ElementLine[-130000 20000 -130000 -20000 1500]
Pin[90000 0 9430 2000 10230 3200 "" "2" ""]
Pin[-90000 0 8820 2000 9620 3200 "" "1" "square"]
Pin[110000 0 8820 2000 9620 3200 "" "3" ""]
)
