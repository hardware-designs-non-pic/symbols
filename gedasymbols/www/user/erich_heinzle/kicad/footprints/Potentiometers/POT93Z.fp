# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POT93Z
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POT93Z
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POT93Z" "" "" 0 0 0 -30000 0 100 ""]
(
ElementLine[22500 7500 20000 7500 1500]
ElementLine[22500 7500 25000 7500 1500]
ElementLine[25000 7500 25000 15000 1500]
ElementLine[25000 15000 20000 15000 1500]
ElementLine[-15000 -20000 20000 -20000 1500]
ElementLine[20000 -20000 20000 20000 1500]
ElementLine[20000 20000 -20000 20000 1500]
ElementLine[-20000 20000 -20000 -20000 1500]
ElementLine[-20000 -20000 -15000 -20000 1500]
Pin[5000 0 8030 2000 8830 3200 "" "2" ""]
Pin[-5000 -10000 8030 2000 8830 3200 "" "1" "square"]
Pin[-5000 10000 8030 2000 8830 3200 "" "3" ""]
)
