# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POAOV
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POAOV
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POAOV" "" "" 0 0 -2500 -22500 0 100 ""]
(
ElementLine[-5000 0 7500 0 1500]
ElementLine[7500 -2500 10000 -2500 1500]
ElementLine[10000 -2500 10000 2500 1500]
ElementLine[10000 2500 7500 2500 1500]
ElementLine[5000 -5000 7500 -5000 1500]
ElementLine[7500 -5000 7500 5000 1500]
ElementLine[7500 5000 5000 5000 1500]
ElementLine[-5000 -17500 5000 -17500 1500]
ElementLine[5000 -17500 5000 17500 1500]
ElementLine[5000 17500 -5000 17500 1500]
ElementLine[-5000 15000 -5000 17500 1500]
ElementLine[-5000 -10000 -5000 15000 1500]
ElementLine[-5000 -10000 -5000 -17500 1500]
Pin[10000 0 8820 2000 9620 3200 "" "2" ""]
Pin[0 -10000 8820 2000 9620 3200 "" "1" "square"]
Pin[0 10000 8820 2000 9620 3200 "" "3" ""]
)
