# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POT63YB
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POT63YB
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POT63YB" "" "" 0 0 7500 -32500 0 100 ""]
(
ElementLine[12500 20000 15000 20000 1500]
ElementLine[15000 20000 15000 -20000 1500]
ElementLine[-7500 -20000 -7500 20000 1500]
ElementLine[-7500 20000 12500 20000 1500]
ElementLine[0 -20000 0 -25000 1500]
ElementLine[0 -25000 7500 -25000 1500]
ElementLine[7500 -25000 7500 -20000 1500]
ElementLine[15000 -20000 -7500 -20000 1500]
Pin[10000 0 8820 2000 9620 3200 "" "2" ""]
Pin[0 -10000 8820 2000 9620 3200 "" "1" "square"]
Pin[0 10000 8820 2000 9620 3200 "" "3" ""]
)
