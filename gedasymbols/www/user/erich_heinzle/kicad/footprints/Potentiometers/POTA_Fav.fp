# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POTA_Fav
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POTA_Fav
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POTA_Fav" "" "" 0 0 20000 -50000 0 100 ""]
(
ElementLine[-40000 -10000 -110000 -10000 1500]
ElementLine[-110000 -10000 -110000 10000 1500]
ElementLine[-110000 10000 -40000 10000 1500]
ElementLine[-10000 -20000 -40000 -20000 1500]
ElementLine[-40000 -20000 -40000 20000 1500]
ElementLine[-40000 20000 -10000 20000 1500]
ElementLine[30000 -40000 30000 40000 1500]
ElementLine[30000 40000 0 40000 1500]
ElementLine[0 40000 -10000 40000 1500]
ElementLine[-10000 40000 -10000 -40000 1500]
ElementLine[-10000 -40000 30000 -40000 1500]
Pin[20000 0 9430 2000 10230 3200 "" "2" ""]
Pin[20000 -20000 8820 2000 9620 3200 "" "1" "square"]
Pin[20000 20000 8820 2000 9620 3200 "" "3" ""]
)
