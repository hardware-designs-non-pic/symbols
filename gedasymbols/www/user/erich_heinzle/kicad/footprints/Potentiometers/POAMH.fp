# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POAMH
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POAMH
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POAMH" "" "" 0 0 10000 -50000 0 100 ""]
(
ElementLine[0 -35000 0 -40000 1500]
ElementLine[0 -40000 7500 -40000 1500]
ElementLine[7500 -40000 7500 -35000 1500]
ElementLine[-7500 -27500 -7500 45000 1500]
ElementLine[-7500 45000 15000 45000 1500]
ElementLine[15000 45000 15000 -27500 1500]
ElementLine[15000 -27500 15000 -35000 1500]
ElementLine[15000 -35000 -7500 -35000 1500]
ElementLine[-7500 -35000 -7500 -27500 1500]
Pin[10000 0 8820 2000 9620 3200 "" "2" ""]
Pin[0 -20000 8820 2000 9620 3200 "" "1" "square"]
Pin[0 30000 8820 2000 9620 3200 "" "3" ""]
)
