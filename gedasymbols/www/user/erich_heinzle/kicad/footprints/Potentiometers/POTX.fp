# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POTX
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POTX
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POTX" "" "" 0 0 0 -30000 0 100 ""]
(
ElementLine[5000 20000 -15000 20000 1500]
ElementLine[-10000 -20000 5000 -20000 1500]
ElementLine[5000 -20000 5000 20000 1500]
ElementLine[-15000 20000 -15000 -20000 1500]
ElementLine[-15000 -20000 -10000 -20000 1500]
ElementLine[5000 -5000 2500 0 1500]
ElementLine[2500 0 0 -2500 1500]
ElementLine[0 -2500 5000 -5000 1500]
ElementLine[-5000 5000 5000 -5000 1500]
Pin[-10000 0 8030 2000 8830 3200 "" "2" ""]
Pin[0 -10000 8030 2000 8830 3200 "" "1" "square"]
Pin[0 10000 8030 2000 8830 3200 "" "3" ""]
)
