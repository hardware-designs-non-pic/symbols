# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module POT93YA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: POT93YA
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "POT93YA" "" "" 0 0 5000 -27500 0 100 ""]
(
ElementLine[5000 -10000 10000 -15000 1500]
ElementLine[12500 20000 15000 20000 1500]
ElementLine[15000 20000 15000 -20000 1500]
ElementLine[-7500 -20000 -7500 20000 1500]
ElementLine[-7500 20000 12500 20000 1500]
ElementLine[15000 -20000 -7500 -20000 1500]
ElementArc[7500 -12500 5000 5000 0 360 1500]
Pin[0 0 8030 2000 8830 3200 "" "2" ""]
Pin[0 -10000 8820 2000 9620 3200 "" "1" "square"]
Pin[0 10000 8820 2000 9620 3200 "" "3" ""]
)
