# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module REL_DIP
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: REL_DIP
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "U***" "" "" 0 0 -20000 -5000 0 100 ""]
(
ElementLine[-40000 -10000 40000 -10000 1500]
ElementLine[40000 10000 -40000 10000 1500]
ElementLine[-40000 10000 -40000 -10000 1500]
ElementLine[-40000 -5000 -35000 -5000 1500]
ElementLine[-35000 -5000 -35000 5000 1500]
ElementLine[-35000 5000 -40000 5000 1500]
ElementLine[40000 -10000 40000 10000 1500]
Pin[-30000 15000 5500 2000 6300 3150 "" "1" "square"]
Pin[-20000 15000 5500 2000 6300 3150 "" "2" ""]
Pin[20000 15000 5500 2000 6300 3150 "" "6" ""]
Pin[30000 15000 5500 2000 6300 3150 "" "7" ""]
Pin[30000 -15000 5500 2000 6300 3150 "" "8" ""]
Pin[20000 -15000 5500 2000 6300 3150 "" "9" ""]
Pin[-20000 -15000 5500 2000 6300 3150 "" "13" ""]
Pin[-30000 -15000 5500 2000 6300 3150 "" "14" ""]
)
