# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Gianni Roveredo
# No warranties express or implied
# Footprint converted from Kicad Module RELAY_G5V-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAY_G5V-2
# Text descriptor count: 1
# Draw segment object count: 21
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "K2" "" "" 0 0 30000 0 1 100 ""]
(
ElementLine[7500 5000 2500 5000 1500]
ElementLine[2500 5000 2500 -25000 1500]
ElementLine[2500 -25000 7500 -20000 1500]
ElementLine[7500 -15000 5000 -15000 1500]
ElementLine[5000 -15000 5000 -20000 1500]
ElementLine[7500 -35000 5000 -35000 1500]
ElementLine[5000 -35000 5000 -30000 1500]
ElementLine[-7500 5000 -2500 5000 1500]
ElementLine[-2500 5000 -2500 -25000 1500]
ElementLine[-2500 -25000 -7500 -20000 1500]
ElementLine[-7500 -15000 -5000 -15000 1500]
ElementLine[-5000 -15000 -5000 -20000 1500]
ElementLine[-7500 -35000 -5000 -35000 1500]
ElementLine[-5000 -35000 -5000 -30000 1500]
ElementLine[-5000 40000 -5000 35000 1500]
ElementLine[-5000 35000 5000 35000 1500]
ElementLine[5000 35000 5000 40000 1500]
ElementLine[-20000 -40000 20000 -40000 1500]
ElementLine[20000 -40000 20000 40000 1500]
ElementLine[20000 40000 -20000 40000 1500]
ElementLine[-20000 40000 -20000 -40000 1500]
Pin[-15000 -35000 11810 2000 12610 3150 "" "9" ""]
Pin[15000 -35000 11810 2000 12610 3150 "N-000046" "8" ""]
Pin[-15000 5000 11810 2000 12610 3150 "N-000047" "13" ""]
Pin[15000 5000 11810 2000 12610 3150 "N-000047" "4" ""]
Pin[-15000 35000 11810 2000 12610 3150 "N-000111" "16" ""]
Pin[15000 35000 11810 2000 12610 3150 "N-000050" "1" ""]
Pin[-15000 -15000 11810 2000 12610 3150 "" "11" ""]
Pin[15000 -15000 11810 2000 12610 3150 "" "6" ""]
)
