# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module RELAIS-FINDER-85-02-2RT
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAIS-FINDER-85-02-2RT
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 9
#
Element["" "RELAIS-FINDER-85/02-2RT" "" "" 0 0 0 72500 0 100 ""]
(
ElementLine[35000 50000 42500 50000 1500]
ElementLine[22500 50000 30000 50000 1500]
ElementLine[5000 50000 15000 50000 1500]
ElementLine[-15000 50000 -7500 50000 1500]
ElementLine[-30000 50000 -25000 50000 1500]
ElementLine[-42500 50000 -35000 50000 1500]
ElementLine[-40000 50000 -42500 50000 1500]
ElementLine[35000 -50000 42500 -50000 1500]
ElementLine[20000 -50000 25000 -50000 1500]
ElementLine[5000 -50000 10000 -50000 1500]
ElementLine[-7500 -50000 -2500 -50000 1500]
ElementLine[-25000 -50000 -17500 -50000 1500]
ElementLine[-42500 -50000 -35000 -50000 1500]
ElementLine[-42500 -57500 0 -57500 1500]
ElementLine[42500 57500 -42500 57500 1500]
ElementLine[-42500 57500 -42500 -57500 1500]
ElementLine[0 -57500 42500 -57500 1500]
ElementLine[42500 -57500 42500 57500 1500]
Pin[25000 -37500 9840 2000 10640 5910 "" "1" ""]
Pin[-25000 -37500 9840 2000 10640 5910 "" "2" ""]
Pin[-25000 -37500 9840 2000 10640 5910 "" "3" ""]
Pin[-25000 -22500 9840 2000 10640 5910 "" "6" ""]
Pin[25000 -22500 9840 2000 10640 5910 "" "4" ""]
Pin[25000 2500 9840 2000 10640 5910 "" "7" ""]
Pin[-25000 2500 9840 2000 10640 5910 "" "9" ""]
Pin[25000 27500 9840 2000 10640 5910 "" "A" ""]
Pin[-25000 27500 9840 2000 10640 5910 "" "B" ""]
)
