# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Gianni Roveredo
# No warranties express or implied
# Footprint converted from Kicad Module RELAY_G6B-2214P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAY_G6B-2214P
# Text descriptor count: 1
# Draw segment object count: 27
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "K4" "" "" 0 0 0 -72500 0 100 ""]
(
ElementLine[5000 -5000 5000 -15000 1500]
ElementLine[5000 -15000 2500 -25000 1500]
ElementLine[-5000 -15000 -2500 -25000 1500]
ElementLine[5000 -5000 10000 -5000 1500]
ElementLine[10000 -35000 5000 -35000 1500]
ElementLine[5000 -35000 5000 -25000 1500]
ElementLine[-10000 -35000 -5000 -35000 1500]
ElementLine[-5000 -35000 -5000 -25000 1500]
ElementLine[-10000 -5000 -5000 -5000 1500]
ElementLine[-5000 -5000 -5000 -15000 1500]
ElementLine[-10000 25000 -15000 25000 1500]
ElementLine[10000 25000 15000 25000 1500]
ElementLine[-5000 25000 -5000 30000 1500]
ElementLine[-5000 30000 5000 30000 1500]
ElementLine[5000 30000 5000 25000 1500]
ElementLine[-10000 25000 -5000 25000 1500]
ElementLine[-5000 25000 -5000 20000 1500]
ElementLine[-5000 20000 5000 20000 1500]
ElementLine[5000 20000 5000 25000 1500]
ElementLine[5000 25000 10000 25000 1500]
ElementLine[-5000 40000 -5000 35000 1500]
ElementLine[-5000 35000 5000 35000 1500]
ElementLine[5000 35000 5000 40000 1500]
ElementLine[-20000 -40000 20000 -40000 1500]
ElementLine[20000 -40000 20000 40000 1500]
ElementLine[20000 40000 -20000 40000 1500]
ElementLine[-20000 40000 -20000 -40000 1500]
Pin[-15000 -35000 15750 2000 16550 3150 "N-000005" "5" ""]
Pin[15000 -35000 15750 2000 16550 3150 "N-000004" "4" ""]
Pin[-15000 -5000 15750 2000 16550 3150 "N-000006" "6" ""]
Pin[15000 -5000 15750 2000 16550 3150 "N-000053" "3" ""]
Pin[-15000 35000 11810 2000 12610 3150 "N-000021" "8" ""]
Pin[15000 35000 11810 2000 12610 3150 "N-000048" "1" ""]
)
