# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module RELAIS-T36-1RT
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAIS-T36-1RT
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "Relais-T36-1RT" "" "" 0 0 0 40000 0 100 ""]
(
ElementLine[0 -30000 37500 -30000 1500]
ElementLine[37500 -30000 37500 30000 1500]
ElementLine[37500 30000 -37500 30000 1500]
ElementLine[-37500 30000 -37500 -30000 1500]
ElementLine[-37500 -30000 0 -30000 1500]
Pin[-30000 0 9940 2000 10740 5120 "" "11" ""]
Pin[-22500 -25000 9840 2000 10640 5120 "" "A1" ""]
Pin[-22500 25000 9840 2000 10640 5120 "" "A2" ""]
Pin[27500 -25000 9840 2000 10640 5120 "" "14" ""]
Pin[27500 25000 9840 2000 10640 5120 "" "12" ""]
)
