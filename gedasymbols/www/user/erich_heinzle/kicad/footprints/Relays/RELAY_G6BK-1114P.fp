# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Gianni Roveredo
# No warranties express or implied
# Footprint converted from Kicad Module RELAY_G6BK-1114P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAY_G6BK-1114P
# Text descriptor count: 1
# Draw segment object count: 31
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "K1" "" "" 0 0 -9000 -19000 0 100 ""]
(
ElementLine[-5000 42000 -5000 40000 1500]
ElementLine[-5000 40000 5000 40000 1500]
ElementLine[5000 40000 5000 42000 1500]
ElementLine[-5000 35000 -5000 33000 1500]
ElementLine[-5000 33000 5000 33000 1500]
ElementLine[5000 33000 5000 36000 1500]
ElementLine[5000 36000 5000 37000 1500]
ElementLine[5000 37000 -5000 37000 1500]
ElementLine[-5000 37000 -5000 35000 1500]
ElementLine[-5000 25000 -5000 23000 1500]
ElementLine[-5000 23000 5000 23000 1500]
ElementLine[5000 23000 5000 27000 1500]
ElementLine[5000 27000 -5000 27000 1500]
ElementLine[-5000 27000 -5000 25000 1500]
ElementLine[-20000 42000 -20000 40000 1500]
ElementLine[-20000 42000 20000 42000 1500]
ElementLine[20000 42000 20000 39000 1500]
ElementLine[15000 35000 5000 35000 1500]
ElementLine[-15000 35000 -5000 35000 1500]
ElementLine[-500 -15000 -3000 -25000 1500]
ElementLine[4500 -35000 -500 -35000 1500]
ElementLine[-500 -35000 -500 -25000 1500]
ElementLine[4500 -5000 -500 -5000 1500]
ElementLine[-500 -5000 -500 -15000 1500]
ElementLine[-10000 25000 -15000 25000 1500]
ElementLine[10000 25000 15000 25000 1500]
ElementLine[-10000 25000 -5000 25000 1500]
ElementLine[5000 25000 10000 25000 1500]
ElementLine[-20000 -40000 20000 -40000 1500]
ElementLine[20000 -40000 20000 40000 1500]
ElementLine[-20000 40000 -20000 -40000 1500]
Pin[15000 -35000 12630 2000 13430 3150 "N-000014" "4" ""]
Pin[15000 -5000 12630 2000 13430 3150 "N-000043" "3" ""]
Pin[-15000 35000 8000 2000 8800 3150 "N-000042" "6" ""]
Pin[15000 35000 8000 2000 8800 3150 "N-000041" "1" ""]
Pin[-15000 25000 8000 2000 8800 3150 "N-000016" "7" ""]
Pin[15000 25000 8000 2000 8800 3150 "N-000041" "2" ""]
)
