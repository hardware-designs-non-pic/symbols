# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module RELAIS-1RT-1A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAIS-1RT-1A
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "RELAIS-1RT/1A" "" "" 0 0 0 24000 0 100 ""]
(
ElementLine[-25000 -14000 -25000 -15000 1500]
ElementLine[-25000 12000 -25000 15000 1500]
ElementLine[-24000 -4000 -23000 -4000 1500]
ElementLine[-23000 -4000 -23000 4000 1500]
ElementLine[-23000 4000 -24000 4000 1500]
ElementLine[-24000 4000 -24000 -3000 1500]
ElementLine[-25000 15000 -25000 14000 790]
ElementLine[-25000 15000 25000 15000 1500]
ElementLine[25000 15000 25000 14000 1500]
ElementLine[-25000 -15000 -25000 -14000 1180]
ElementLine[-25000 -15000 25000 -15000 1500]
ElementLine[25000 -15000 25000 -14000 1500]
ElementLine[-25000 14000 -25000 -14000 1500]
ElementLine[25000 14000 25000 -14000 1500]
Pin[20000 -10000 7090 2000 7890 3200 "" "6" ""]
Pin[10000 -10000 7090 2000 7890 3200 "" "7" ""]
Pin[-20000 -10000 7090 2000 7890 3200 "" "10" ""]
Pin[-20000 10000 7090 2000 7890 3200 "" "1" ""]
Pin[10000 10000 7090 2000 7890 3200 "" "4" ""]
Pin[20000 10000 7090 2000 7890 3200 "" "5" ""]
)
