# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module RELAIS-1RT-2A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: RELAIS-1RT-2A
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "RELAIS-1RT/2A" "" "" 0 0 0 31000 0 100 ""]
(
ElementLine[26000 14000 26000 20000 1500]
ElementLine[26000 20000 24000 20000 1500]
ElementLine[-36000 15000 -36000 20000 1500]
ElementLine[-36000 20000 -25000 20000 1500]
ElementLine[26000 -14000 26000 -20000 1500]
ElementLine[26000 -20000 24000 -20000 1500]
ElementLine[-36000 -13000 -36000 -20000 1500]
ElementLine[-36000 -20000 -25000 -20000 1500]
ElementLine[-25000 20000 25000 20000 1500]
ElementLine[-25000 -20000 25000 -20000 1500]
ElementLine[-36000 15000 -36000 -13000 1500]
ElementLine[26000 14000 26000 -14000 1500]
Pin[20000 -15000 7870 2000 8670 3940 "" "4" ""]
Pin[-20000 -15000 7870 2000 8670 3940 "" "5" ""]
Pin[-30000 -15000 7870 2000 8670 3940 "" "6" ""]
Pin[-30000 15000 7870 2000 8670 3940 "" "1" ""]
Pin[-20000 15000 7870 2000 8670 3940 "" "2" ""]
Pin[20000 15000 7870 2000 8670 3940 "" "3" ""]
)
