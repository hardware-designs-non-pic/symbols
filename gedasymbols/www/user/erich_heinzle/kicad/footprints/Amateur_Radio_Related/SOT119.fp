# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOT119
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOT119
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 3
# Draw arc object count: 4
# Pad count: 6
#
Element["" "SOT119" "" "" 0 0 0 67500 0 100 ""]
(
ElementLine[-37500 12500 -22500 12500 1500]
ElementLine[-37500 -12500 -22500 -12500 1500]
ElementLine[37500 12500 22500 12500 1500]
ElementLine[37500 -12500 22500 -12500 1500]
ElementArc[37500 0 5000 5000 0 360 1500]
ElementArc[-37500 0 5000 5000 0 360 1500]
ElementArc[0 0 26101 26101 0 360 1500]
ElementArc[-37500 0 12500 12500 90 -90 1500]
ElementArc[-37500 0 12500 12500 0 -90 1500]
ElementArc[37500 0 12500 12500 270 -90 1500]
ElementArc[37500 0 12500 12500 180 -90 1500]
Pad[0 -34470 0 -30530 15750 2000 16550 "" "3" "square"]
Pad[-25000 18510 -25000 31490 22440 2000 23240 "" "1" "square"]
Pad[25000 18510 25000 31490 22440 2000 23240 "" "1" "square"]
Pad[-390 32500 390 32500 19690 2000 20490 "" "2" "square"]
Pad[-25000 -31490 -25000 -18510 22440 2000 23240 "" "1" "square"]
Pad[25000 -31490 25000 -18510 22440 2000 23240 "" "1" "square"]
)
