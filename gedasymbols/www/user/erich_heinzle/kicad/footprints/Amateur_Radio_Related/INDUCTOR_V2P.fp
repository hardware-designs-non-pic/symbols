# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module INDUCTOR_V2P
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: INDUCTOR_V2P
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 5
#
Element["" "L**" "" "" 0 0 2000 8000 0 100 ""]
(
ElementArc[0 0 20000 20000 0 360 1500]
ElementArc[0 0 12500 12500 0 360 1500]
Pin[22500 12500 9840 2000 10640 3200 "" "1" "square"]
Pin[0 -25000 9840 2000 10640 3200 "" "3" ""]
Pin[20000 -15000 9840 2000 10640 3200 "" "4" ""]
Pin[0 25000 9840 2000 10640 3200 "" "2" ""]
Pin[0 0 6000 2000 6800 3200 "" "5" ""]
)
