# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TRANSFO_MOULE_10VA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TRANSFO_MOULE_10VA
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 12
#
Element["" "Transfo moulé/10VA" "" "" 0 0 0 100000 0 100 ""]
(
ElementLine[-55000 -60000 55000 -60000 1500]
ElementLine[55000 -60000 55000 60000 1500]
ElementLine[55000 60000 -55000 60000 1500]
ElementLine[-55000 60000 -55000 -60000 1500]
ElementLine[100000 -85000 100000 85000 1500]
ElementLine[-100000 85000 100000 85000 1500]
ElementLine[-100000 -85000 100000 -85000 1500]
ElementLine[-100000 -85000 -100000 85000 1500]
Pin[-50000 -55000 7870 2000 8670 3200 "" "1" ""]
Pin[-30000 -55000 7870 2000 8670 3200 "" "2" ""]
Pin[-10000 -55000 7870 2000 8670 3200 "" "3" ""]
Pin[50000 -55000 7870 2000 8670 3200 "" "6" ""]
Pin[10000 -55000 7870 2000 8670 3200 "" "4" ""]
Pin[30000 -55000 7870 2000 8670 3200 "" "5" ""]
Pin[10000 55000 7870 2000 8670 3200 "" "9" ""]
Pin[30000 55000 7870 2000 8670 3200 "" "8" ""]
Pin[50000 55000 7870 2000 8670 3200 "" "7" ""]
Pin[-10000 55000 7870 2000 8670 3200 "" "10" ""]
Pin[-30000 55000 7870 2000 8670 3200 "" "11" ""]
Pin[-50000 55000 7870 2000 8670 3200 "" "12" ""]
)
