# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOT123
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOT123
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 3
# Draw arc object count: 4
# Pad count: 4
#
Element["" "SOT123" "" "" 0 0 0 62500 0 100 ""]
(
ElementLine[-20000 37500 -2500 20000 1500]
ElementLine[-35000 17500 -20000 2500 1500]
ElementLine[5000 -20000 20000 -35000 1500]
ElementLine[37500 -17500 20000 0 1500]
ElementArc[0 0 20156 20156 0 360 1500]
ElementArc[-27500 27500 7071 7071 0 360 1500]
ElementArc[27500 -25000 7071 7071 0 360 1500]
ElementArc[-27500 27500 12500 12500 180 -90 1500]
ElementArc[-27500 27500 12500 12500 45 -90 1500]
ElementArc[27500 -25000 12500 12500 225 -90 1500]
ElementArc[27500 -25000 12500 12500 315 -90 1500]
Pad[0 -27890 0 -27110 22050 2000 22850 "" "3" "square"]
Pad[27500 -390 27500 390 22050 2000 22850 "" "1" "square"]
Pad[-27500 -390 -27500 390 22050 2000 22850 "" "1" "square"]
Pad[0 27110 0 27890 22050 2000 22850 "" "2" "square"]
)
