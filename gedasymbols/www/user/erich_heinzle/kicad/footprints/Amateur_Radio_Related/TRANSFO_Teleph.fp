# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TRANSFO_Teleph.
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TRANSFO_Teleph.
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "TRANSFO_Télèph. 600/600" "" "" 0 0 0 50000 0 100 ""]
(
ElementLine[-30000 -35000 35000 -35000 1500]
ElementLine[35000 -35000 35000 -30000 1500]
ElementLine[-35000 35000 -35000 -30000 1500]
ElementLine[-35000 -30000 -30000 -35000 1500]
ElementLine[35000 -30000 35000 35000 1500]
ElementLine[0 35000 -35000 35000 1500]
ElementLine[0 35000 35000 35000 1500]
Pin[15000 -25000 6000 2000 6800 2360 "" "4" ""]
Pin[5000 -25000 6000 2000 6800 2360 "" "3" ""]
Pin[-5000 -25000 6000 2000 6800 2360 "" "2" ""]
Pin[-15000 -25000 6000 2000 6800 2360 "" "1" ""]
Pin[-15000 25000 6000 2000 6800 2360 "" "8" ""]
Pin[-5000 25000 6000 2000 6800 2360 "" "7" ""]
Pin[5000 25000 6000 2000 6800 2360 "" "6" ""]
Pin[15000 25000 6000 2000 6800 2360 "" "5" ""]
)
