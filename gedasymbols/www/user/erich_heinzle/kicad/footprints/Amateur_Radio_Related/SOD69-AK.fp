# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOD69-AK
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOD69-AK
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "VAL**" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-5000 10000 10000 -5000 1200]
ElementLine[10000 -5000 10000 -10000 1200]
ElementLine[10000 -10000 5000 -15000 1200]
ElementLine[5000 -15000 -5000 -15000 1200]
ElementLine[-5000 -15000 -15000 -5000 1200]
ElementLine[-15000 -5000 -15000 5000 1200]
ElementLine[-15000 5000 -10000 10000 1200]
ElementLine[-10000 10000 -5000 10000 1200]
Pin[5000 -5000 5500 2000 6300 3200 "N-000088" "2" "square"]
Pin[-5000 5000 5500 2000 6300 3200 "N-000075" "1" ""]
)
