# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOT121
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOT121
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 3
# Draw arc object count: 4
# Pad count: 4
#
Element["" "SOT121" "" "" 0 0 0 62500 0 100 ""]
(
ElementLine[25000 -7500 37500 -17500 1500]
ElementLine[-35000 17500 -25000 7500 1500]
ElementLine[20000 -35000 10000 -25000 1500]
ElementLine[-20000 37500 -7500 25000 1500]
ElementArc[-27500 27500 7071 7071 0 360 1500]
ElementArc[27500 -25000 7071 7071 0 360 1500]
ElementArc[0 0 26101 26101 0 360 1500]
ElementArc[-27500 27500 12500 12500 180 -90 1500]
ElementArc[-27500 27500 12500 12500 45 -90 1500]
ElementArc[27500 -25000 12500 12500 225 -90 1500]
ElementArc[27500 -25000 12500 12500 315 -90 1500]
Pad[-2760 -37500 2760 -37500 22830 2000 23630 "" "3" "square"]
Pad[34740 0 40260 0 22830 2000 23630 "" "1" "square"]
Pad[-40260 0 -34740 0 22830 2000 23630 "" "1" "square"]
Pad[-2760 37500 2760 37500 22830 2000 23630 "" "2" "square"]
)
