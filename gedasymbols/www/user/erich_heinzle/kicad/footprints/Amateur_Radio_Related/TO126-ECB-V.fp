# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TO126-ECB-V
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TO126-ECB-V
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "Q2" "" "" 0 0 15000 -15000 1 100 ""]
(
ElementLine[-5000 15000 -5000 20000 1500]
ElementLine[-5000 2500 -5000 5000 1500]
ElementLine[-5000 -7500 -5000 -5000 1500]
ElementLine[-5000 -15000 -5000 -12500 1500]
ElementLine[-5000 -20000 -5000 -17500 1500]
ElementLine[-7500 -20000 -7500 20000 1200]
ElementLine[-7500 -20000 7500 -20000 1200]
ElementLine[7500 -20000 7500 20000 1200]
ElementLine[7500 20000 -7500 20000 1200]
Pin[0 -10000 7480 2000 8280 3940 "N-000026" "3" ""]
Pin[0 0 7480 2000 8280 3940 "N-000014" "2" ""]
Pin[0 10000 7480 2000 8280 3940 "N-000015" "1" "square"]
)
