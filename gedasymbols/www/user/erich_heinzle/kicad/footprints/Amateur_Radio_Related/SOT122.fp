# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOT122
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOT122
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "SOT122" "" "" 0 0 0 67500 0 100 ""]
(
ElementArc[0 0 20156 20156 0 360 1500]
Pad[0 -42480 0 -27520 22440 2000 23240 "" "3" "square"]
Pad[35000 -7480 35000 7480 22440 2000 23240 "" "1" "square"]
Pad[0 27520 0 42480 22440 2000 23240 "" "2" "square"]
Pad[-35000 -7480 -35000 7480 22440 2000 23240 "" "1" "square"]
)
