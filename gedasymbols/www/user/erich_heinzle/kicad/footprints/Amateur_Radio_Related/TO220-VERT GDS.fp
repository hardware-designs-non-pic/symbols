# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TO220-VERT GDS
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TO220-VERT GDS
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "TO220-VERT GDS" "" "" 0 0 -12500 0 1 100 ""]
(
ElementLine[7500 -20000 10000 -20000 1500]
ElementLine[10000 -20000 10000 20000 1500]
ElementLine[10000 20000 7500 20000 1500]
ElementLine[-7500 -20000 7500 -20000 1500]
ElementLine[7500 -20000 7500 20000 1500]
ElementLine[7500 20000 -7500 20000 1500]
ElementLine[-7500 20000 -7500 -20000 1500]
Pin[0 -10000 7000 2000 7800 4000 "" "1" ""]
Pin[0 0 7000 2000 7800 4000 "" "2" ""]
Pin[0 10000 7000 2000 7800 4000 "" "3" "square"]
)
