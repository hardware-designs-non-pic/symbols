# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module PotaP11-1W_254
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: PotaP11-1W_254
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "PotaP11/1W_254" "" "" 0 0 0 112500 0 100 ""]
(
ElementLine[-25000 -15000 25000 -15000 1500]
ElementLine[12500 65000 12500 100000 1500]
ElementLine[12500 100000 -12500 100000 1500]
ElementLine[-12500 100000 -12500 65000 1500]
ElementLine[0 65000 -20000 65000 1500]
ElementLine[-20000 65000 -20000 27500 1500]
ElementLine[0 65000 20000 65000 1500]
ElementLine[20000 65000 20000 27500 1500]
ElementLine[-25000 -17500 -25000 -7500 1500]
ElementLine[-25000 -17500 25000 -17500 1500]
ElementLine[25000 -17500 25000 -7500 1500]
ElementLine[-25000 27500 -25000 7500 1500]
ElementLine[-25000 27500 25000 27500 1500]
ElementLine[25000 27500 25000 7500 1500]
ElementLine[25000 -7500 25000 7500 1500]
ElementLine[-25000 -7500 -25000 7500 1500]
Pin[0 0 7090 2000 7890 4720 "" "2" ""]
Pin[-10000 0 11810 2000 12610 4720 "" "1" ""]
Pin[10000 0 7090 2000 7890 4720 "" "3" ""]
)
