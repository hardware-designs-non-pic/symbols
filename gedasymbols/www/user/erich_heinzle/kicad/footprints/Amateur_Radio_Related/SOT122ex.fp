# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SOT122ex
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SOT122ex
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "SOT122" "" "" 0 0 0 67500 0 100 ""]
(
ElementLine[-12050 16100 -13650 14800 1500]
ElementLine[-13650 14800 -15350 13050 1500]
ElementLine[-15350 13050 -16200 11950 1500]
ElementLine[16050 12200 14550 13900 1500]
ElementLine[14550 13900 13150 15250 1500]
ElementLine[13150 15250 12050 16150 1500]
ElementLine[11900 -16300 13700 -14800 1500]
ElementLine[13700 -14800 15050 -13400 1500]
ElementLine[15050 -13400 16200 -12000 1500]
ElementLine[-16150 -11950 -14700 -13650 1500]
ElementLine[-14700 -13650 -13100 -15250 1500]
ElementLine[-13100 -15250 -12000 -16100 1500]
Pad[-3350 -24000 3350 -24000 15750 2000 16550 "" "" "square"]
Pad[35000 -7480 35000 7480 22440 2000 23240 "" "1" "square"]
Pad[0 27520 0 42480 22440 2000 23240 "" "2" "square"]
Pad[-35000 -7480 -35000 7480 22440 2000 23240 "" "1" "square"]
Pad[-5300 -32900 -5300 -32900 11810 2000 12610 "" "" "blah"]
Pad[5350 -41810 5350 -29990 11810 2000 12610 "" "3" "blah"]
)
