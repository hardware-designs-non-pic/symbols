# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module INDUCTOR_V-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: INDUCTOR_V-1
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L**" "" "" 0 0 2000 8000 0 100 ""]
(
ElementArc[0 0 15000 15000 0 360 1500]
Pin[-15000 0 7500 2000 8300 3200 "N-000007" "1" "square"]
Pin[15000 0 7500 2000 8300 3200 "VCC" "2" ""]
)
