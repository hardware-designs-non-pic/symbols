# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module R25W
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: R25W
# Text descriptor count: 1
# Draw segment object count: 13
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "R25W" "" "" 0 0 0 65000 0 100 ""]
(
ElementLine[-15000 55000 -15000 27500 1500]
ElementLine[-55000 55000 -55000 27500 1500]
ElementLine[55000 -55000 55000 -27500 1500]
ElementLine[15000 -55000 15000 -27500 1500]
ElementLine[-55000 55000 -15000 55000 1500]
ElementLine[15000 -55000 55000 -55000 1500]
ElementLine[-97500 0 -55000 0 1500]
ElementLine[55000 0 97500 0 1500]
ElementLine[-55000 -27500 55000 -27500 1500]
ElementLine[55000 27500 -55000 27500 1500]
ElementLine[-55000 27500 -55000 -27500 1500]
ElementLine[55000 0 55000 27500 1500]
ElementLine[55000 0 55000 -27500 1500]
ElementArc[-35000 40000 7500 7500 0 360 1500]
ElementArc[35000 -40000 7906 7906 0 360 1500]
Pin[97500 0 19690 2000 20490 9840 "" "1" ""]
Pin[-97500 0 19690 2000 20490 9840 "" "2" ""]
)
