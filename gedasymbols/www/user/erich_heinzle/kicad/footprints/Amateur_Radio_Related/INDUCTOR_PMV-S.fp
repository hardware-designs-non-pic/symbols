# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module INDUCTOR_PMV-S
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: INDUCTOR_PMV-S
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "L**" "" "" 0 0 0 20000 0 100 ""]
(
ElementArc[0 0 15000 15000 0 360 1500]
Pin[-15000 0 9840 2000 10640 3200 "N-000013" "1" "square"]
Pin[15000 0 11180 2000 11980 3200 "GND" "2" ""]
Pin[0 -15000 11180 2000 11980 3200 "N-000003" "3" ""]
)
