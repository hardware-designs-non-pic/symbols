# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TRANSFO_RF
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TRANSFO_RF
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "TRANSFO_RF" "" "" 0 0 0 40000 0 100 ""]
(
ElementLine[26000 -30000 26000 30000 1500]
ElementLine[26000 30000 -26000 30000 1500]
ElementLine[-26000 30000 -26000 -30000 1500]
ElementLine[-26000 -30000 1000 -30000 1500]
ElementLine[0 -30000 26000 -30000 1500]
Pin[-19000 -23000 9840 2000 10640 3200 "" "1" ""]
Pin[19000 -23000 9840 2000 10640 3200 "" "4" ""]
Pin[-19000 23000 9840 2000 10640 3200 "" "2" ""]
Pin[19000 23000 9840 2000 10640 3200 "" "3" ""]
)
