# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module bornier12
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: bornier12
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 12
#
Element["" "bornier12" "" "" 0 0 0 42500 0 100 ""]
(
ElementLine[-115000 15000 115000 15000 1500]
ElementLine[-115000 -15000 115000 -15000 1500]
ElementLine[-115000 -20000 -115000 30000 1500]
ElementLine[-115000 30000 115000 30000 1500]
ElementLine[115000 30000 115000 -20000 1500]
ElementLine[-115000 -20000 115000 -20000 1500]
Pin[-110000 0 9840 2000 10640 4720 "" "1" ""]
Pin[-90000 0 9840 2000 10640 4720 "" "2" ""]
Pin[-70000 0 9840 2000 10640 4720 "" "3" ""]
Pin[-50000 0 9840 2000 10640 4720 "" "4" ""]
Pin[-30000 0 9840 2000 10640 4720 "" "5" ""]
Pin[-10000 0 9840 2000 10640 4720 "" "6" ""]
Pin[10000 0 9840 2000 10640 4720 "" "7" ""]
Pin[30000 0 9840 2000 10640 4720 "" "8" ""]
Pin[50000 0 9840 2000 10640 4720 "" "9" ""]
Pin[70000 0 9840 2000 10640 4720 "" "10" ""]
Pin[90000 0 9840 2000 10640 4720 "" "11" ""]
Pin[110000 0 9840 2000 10640 4720 "" "12" ""]
)
