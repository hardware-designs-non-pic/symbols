# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TO39_LM117H
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TO39_LM117H
# Text descriptor count: 1
# Draw segment object count: 3
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "TO39_LM117H" "" "" 0 0 -1000 22000 0 100 ""]
(
ElementLine[-15000 -10500 -20000 -15500 1200]
ElementLine[-10500 -15000 -15500 -20000 1200]
ElementLine[-15500 -20000 -20000 -15500 1200]
ElementArc[0 0 18398 18398 0 360 1200]
Pin[10000 0 6500 2000 7300 3100 "" "3" ""]
Pin[0 10000 6500 2000 7300 3100 "" "2" ""]
Pin[-10000 0 6500 2000 7300 3100 "" "1" "square"]
)
