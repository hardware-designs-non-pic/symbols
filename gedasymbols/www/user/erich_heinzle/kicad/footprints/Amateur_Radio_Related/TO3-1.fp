# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TO3-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TO3-1
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 1
# Draw arc object count: 3
# Pad count: 4
#
Element["" "TO3/1" "" "" 0 0 -54000 -44000 0 100 ""]
(
ElementLine[-32500 -37500 -72500 -12500 1500]
ElementLine[37500 -32500 72500 -12500 1500]
ElementLine[-72500 12500 -37500 32500 1500]
ElementLine[72500 12500 37500 32500 1500]
ElementArc[0 0 40078 40078 0 360 1200]
ElementArc[0 0 49497 49497 225 -360 1500]
ElementArc[-60000 0 17678 17678 135 -360 1500]
ElementArc[60000 0 17678 17678 315 -360 1500]
Pin[-60000 0 25000 2000 25800 15500 "" "3" ""]
Pin[60000 0 25000 2000 25800 15500 "" "3" ""]
Pin[-10000 -20000 10000 2000 10800 5200 "" "2" ""]
Pin[-10000 20000 10000 2000 10800 5200 "" "1" ""]
)
