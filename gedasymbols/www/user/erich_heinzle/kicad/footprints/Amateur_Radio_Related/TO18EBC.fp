# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TO18EBC
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TO18EBC
# Text descriptor count: 1
# Draw segment object count: 3
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "Q4" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[-10500 -6000 -13500 -8000 1200]
ElementLine[-13500 -8000 -12000 -10000 1200]
ElementLine[-12000 -10000 -9000 -8000 1200]
ElementArc[0 0 12000 12000 0 360 800]
Pin[0 7500 6000 2000 6800 3100 "N-000058" "2" ""]
Pin[7500 -2500 6000 2000 6800 3100 "+5VDC" "3" ""]
Pin[-7500 -2500 6000 2000 6800 3100 "N-000054" "1" ""]
)
