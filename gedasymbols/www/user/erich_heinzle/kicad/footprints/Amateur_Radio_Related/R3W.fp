# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module R3W
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: R3W
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "R3W" "" "" 0 0 0 5000 0 100 ""]
(
ElementLine[-25000 0 -40000 0 790]
ElementLine[25000 0 40000 0 790]
ElementLine[-25000 -10000 25000 -10000 1500]
ElementLine[25000 -10000 25000 10000 1500]
ElementLine[25000 10000 -25000 10000 1500]
ElementLine[-25000 10000 -25000 -10000 1500]
Pin[-40000 0 9840 2000 10640 3200 "" "1" ""]
Pin[40000 0 9840 2000 10640 3200 "" "2" ""]
)
