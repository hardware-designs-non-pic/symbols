# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module PONT_REDRESSEUR_1A
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: PONT_REDRESSEUR_1A
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 4
#
Element["" "PONT_REDRESSEUR_1A" "" "" 0 0 0 30000 0 100 ""]
(
ElementLine[-15000 -12500 -15000 12500 1500]
ElementArc[0 0 20000 20000 0 360 1500]
Pin[-10000 -10000 7870 2000 8670 3200 "" "1" ""]
Pin[10000 -10000 7870 2000 8670 3200 "" "2" ""]
Pin[-10000 10000 7870 2000 8670 3200 "" "3" ""]
Pin[10000 10000 7870 2000 8670 3200 "" "4" ""]
)
