# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module CRYSTAL_FILTER
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: CRYSTAL_FILTER
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 4
#
Element["" "CRYSTAL FILTER KVG-XF9" "" "" 0 0 -17500 67500 0 100 ""]
(
ElementLine[-87500 40000 -87500 47500 1500]
ElementLine[57500 40000 57500 47500 1500]
ElementLine[-77500 -55000 47500 -55000 1500]
ElementLine[-80000 55000 50000 55000 1500]
ElementLine[-87500 40000 -87500 -45000 1500]
ElementLine[57500 40000 57500 -45000 1500]
ElementArc[-80000 47500 7500 7500 0 90 1500]
ElementArc[50000 47500 7500 7500 90 90 1500]
ElementArc[-77500 -45000 10000 10000 270 90 1500]
ElementArc[47500 -45000 10000 10000 180 90 1500]
Pin[0 30000 19690 2000 20490 12200 "" "4" ""]
Pin[30000 30000 11810 2000 12610 4330 "" "2" ""]
Pin[-30000 -30000 19690 2000 20490 12200 "" "3" ""]
Pin[-60000 -30000 11810 2000 12610 4330 "" "1" ""]
)
