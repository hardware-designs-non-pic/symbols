# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module TRANSFO_MOULE_2,4VA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: TRANSFO_MOULE_2,4VA
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" "Transfo moulé/2,4VA" "" "" 0 0 0 62500 0 100 ""]
(
ElementLine[-65000 50000 -65000 55000 1500]
ElementLine[-65000 -50000 -65000 -55000 1500]
ElementLine[65000 52500 65000 55000 1500]
ElementLine[65000 -50000 65000 -55000 1500]
ElementLine[-65000 55000 65000 55000 1500]
ElementLine[-65000 -55000 65000 -55000 1500]
ElementLine[-65000 -52500 -65000 52500 1500]
ElementLine[65000 -52500 65000 52500 1500]
ElementLine[-45000 -45000 -45000 45000 1500]
ElementLine[45000 -45000 45000 45000 1500]
ElementLine[-45000 45000 45000 45000 1500]
ElementLine[-45000 -45000 45000 -45000 1500]
Pin[-40000 -40000 7870 2000 8670 3200 "" "1" ""]
Pin[-20000 -40000 7870 2000 8670 3200 "" "2" ""]
Pin[0 -40000 7870 2000 8670 3200 "" "3" ""]
Pin[20000 -40000 7870 2000 8670 3200 "" "4" ""]
Pin[40000 -40000 7870 2000 8670 3200 "" "5" ""]
Pin[20000 40000 7870 2000 8670 3200 "" "7" ""]
Pin[40000 40000 7870 2000 8670 3200 "" "6" ""]
Pin[0 40000 7870 2000 8670 3200 "" "8" ""]
Pin[-20000 40000 7870 2000 8670 3200 "" "9" ""]
Pin[-40000 40000 7870 2000 8670 3200 "" "10" ""]
)
