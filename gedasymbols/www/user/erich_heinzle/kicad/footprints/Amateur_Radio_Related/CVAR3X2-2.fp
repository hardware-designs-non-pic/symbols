# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module CVAR3X2-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: CVAR3X2-2
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 7
#
Element["" "CV3x2-2" "" "" 0 0 0 27500 0 100 ""]
(
ElementArc[0 0 20100 20100 0 360 1500]
Pin[0 -20000 10000 2000 10800 5200 "" "1" ""]
Pin[10000 10000 10000 2000 10800 5200 "" "2" ""]
Pin[-10000 10000 10000 2000 10800 5200 "" "3" ""]
Pin[-20000 0 10000 2000 10800 5200 "" "3" ""]
Pin[20000 0 10000 2000 10800 5200 "" "2" ""]
Pin[-15000 5000 7870 2000 8670 0 "" "3" "square"]
Pin[15000 5000 9060 2000 9860 0 "" "2" "square"]
)
