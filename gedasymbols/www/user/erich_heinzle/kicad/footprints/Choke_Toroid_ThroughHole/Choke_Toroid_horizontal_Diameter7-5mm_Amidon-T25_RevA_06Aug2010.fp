# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_horizontal_Diameter7-5mm_Amidon-T25_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_horizontal_Diameter7-5mm_Amidon-T25_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-12600 0 -4330 0 1500]
ElementLine[-4330 0 -9840 7090 1500]
ElementLine[-9840 7090 -1570 3540 1500]
ElementLine[-1570 3540 -790 12600 1500]
ElementLine[-790 12600 2760 2760 1500]
ElementLine[2760 2760 9450 8270 1500]
ElementLine[9450 8270 4330 0 1500]
ElementLine[4330 0 12600 0 1500]
ElementArc[0 0 3940 3940 0 360 1500]
ElementArc[0 0 12990 12990 0 360 1500]
Pin[-19690 0 9840 2000 10640 3940 "" "1" ""]
Pin[19690 0 9840 2000 10640 3940 "" "2" ""]
)
