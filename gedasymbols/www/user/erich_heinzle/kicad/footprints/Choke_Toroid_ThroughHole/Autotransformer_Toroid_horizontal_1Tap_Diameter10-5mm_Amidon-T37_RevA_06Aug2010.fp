# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Autotransformer_Toroid_horizontal_1Tap_Diameter10-5mm_Amidon-T37_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Autotransformer_Toroid_horizontal_1Tap_Diameter10-5mm_Amidon-T37_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 3
#
Element["" "L" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-21260 0 -8660 0 1500]
ElementLine[-8660 0 -19690 6300 1500]
ElementLine[-19690 6300 -7480 3940 1500]
ElementLine[-7480 3940 -15750 13390 1500]
ElementLine[-15750 13390 -5510 6690 1500]
ElementLine[-5510 6690 -8270 19290 1500]
ElementLine[-8270 19290 -1180 8270 1500]
ElementLine[-1180 8270 1570 20470 1500]
ElementLine[1570 20470 3540 7480 1500]
ElementLine[3540 7480 9060 18500 1500]
ElementLine[9060 18500 7090 4720 1500]
ElementLine[7090 4720 18900 8270 1500]
ElementLine[18900 8270 8270 0 1500]
ElementLine[8270 0 20870 0 1500]
ElementArc[0 0 8270 8270 0 360 1500]
ElementArc[0 0 20870 20870 0 360 1500]
Pin[-29530 0 11810 2000 12610 4720 "" "1" ""]
Pin[29530 0 11810 2000 12610 4720 "" "3" ""]
Pin[0 29530 11810 2000 12610 4720 "" "2" ""]
)
