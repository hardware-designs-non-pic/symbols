# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_8x16mm_Vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_8x16mm_Vertical
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 65000 -40000 0 100 ""]
(
ElementLine[63000 0 36000 0 1500]
ElementLine[0 0 24000 0 1500]
ElementLine[24000 -31500 0 -31500 1500]
ElementLine[63000 -31500 36000 -31500 1500]
ElementLine[63000 0 63000 -31500 1500]
ElementLine[0 -31500 0 0 1500]
Pin[30000 0 7870 2000 8670 3940 "" "1" ""]
Pin[30000 -30000 7870 2000 8670 3940 "" "2" ""]
)
