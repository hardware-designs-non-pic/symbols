# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Transformer_Toroid_Tapped_horizontal_Diameter10-5mm_Amidon-T37_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Transformer_Toroid_Tapped_horizontal_Diameter10-5mm_Amidon-T37_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" "L" "" "" 0 0 0 -45000 0 100 ""]
(
ElementLine[-18900 8270 -8270 3940 1500]
ElementLine[-8270 3940 -13780 14570 1500]
ElementLine[-13780 14570 -5120 7090 1500]
ElementLine[-5120 7090 -7090 18500 1500]
ElementLine[-7090 18500 -390 8660 1500]
ElementLine[-390 8660 390 20470 1500]
ElementLine[390 20470 4330 7090 1500]
ElementLine[4330 7090 11810 16540 1500]
ElementLine[11810 16540 7480 3940 1500]
ElementLine[7480 3940 18500 8270 1500]
ElementLine[-19290 -8270 -8270 -3150 1500]
ElementLine[-8270 -3150 -13780 -14960 1500]
ElementLine[-13780 -14960 -3540 -7480 1500]
ElementLine[-3540 -7480 -4720 -20080 1500]
ElementLine[-4720 -20080 1180 -8270 1500]
ElementLine[1180 -8270 6300 -18900 1500]
ElementLine[6300 -18900 5510 -6690 1500]
ElementLine[5510 -6690 13780 -14570 1500]
ElementLine[13780 -14570 7870 -3940 1500]
ElementLine[7870 -3940 18500 -8270 1500]
ElementArc[0 0 8270 8270 0 360 1500]
ElementArc[0 0 20870 20870 0 360 1500]
Pin[-25590 11810 11810 2000 12610 4720 "" "1" ""]
Pin[25590 11810 11810 2000 12610 4720 "" "3" ""]
Pin[-25590 -11810 11810 2000 12610 4720 "" "4" ""]
Pin[25590 -11810 11810 2000 12610 4720 "" "6" ""]
Pin[0 28740 11810 2000 12610 4720 "" "2" ""]
Pin[0 -28740 11810 2000 12610 4720 "" "5" ""]
)
