# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_horizontal_Diameter4-5mm_Amidon-T16_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_horizontal_Diameter4-5mm_Amidon-T16_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[3940 0 8660 0 1500]
ElementLine[3940 1180 8270 2360 1500]
ElementLine[2760 2760 7090 4720 1500]
ElementLine[1180 3540 4720 7090 1500]
ElementLine[-390 3540 790 8660 1500]
ElementLine[-1970 3150 -2360 8270 1500]
ElementLine[-2760 2760 -5120 7090 1500]
ElementLine[-3540 1570 -7090 5120 1500]
ElementLine[-9060 0 -4330 0 1500]
ElementLine[-4330 0 -8270 3150 1500]
ElementArc[0 0 3940 3940 0 360 1500]
ElementArc[0 0 9060 9060 0 360 1500]
Pin[-15750 0 9840 2000 10640 3940 "" "1" ""]
Pin[15750 0 9840 2000 10640 3940 "" "2" ""]
)
