# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_horizontal_Diameter6mm_Amidon-T20_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_horizontal_Diameter6mm_Amidon-T20_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[-2760 0 -9060 4720 1500]
ElementLine[-9060 4720 -1570 1970 1500]
ElementLine[-1570 1970 -6690 7870 1500]
ElementLine[-6690 7870 -390 2760 1500]
ElementLine[-390 2760 -1180 10240 1500]
ElementLine[-1180 10240 1180 2760 1500]
ElementLine[1180 2760 4720 9060 1500]
ElementLine[4720 9060 1970 1970 1500]
ElementLine[1970 1970 9060 5120 1500]
ElementLine[9060 5120 2360 0 1500]
ElementLine[2360 0 10240 0 1500]
ElementLine[-10630 0 -2360 0 1500]
ElementArc[0 0 10630 10630 0 360 1500]
ElementArc[0 0 2360 2360 0 360 1500]
Pin[-17720 0 9840 2000 10640 3940 "" "1" ""]
Pin[17720 0 9840 2000 10640 3940 "" "2" ""]
)
