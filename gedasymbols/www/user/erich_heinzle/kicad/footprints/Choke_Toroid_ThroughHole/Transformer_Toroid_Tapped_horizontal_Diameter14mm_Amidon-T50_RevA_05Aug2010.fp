# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Transformer_Toroid_Tapped_Horizontal_Diameter14mm_Amidon-T50_RevA_05Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Transformer_Toroid_Tapped_Horizontal_Diameter14mm_Amidon-T50_RevA_05Aug2010
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" "Tr" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-25200 15350 -11810 7480 1500]
ElementLine[-11810 7480 -20080 21260 1500]
ElementLine[-20080 21260 -6690 12200 1500]
ElementLine[-6690 12200 -7870 28350 1500]
ElementLine[-7870 28350 1180 13390 1500]
ElementLine[1180 13390 8660 27560 1500]
ElementLine[8660 27560 8660 10630 1500]
ElementLine[8660 10630 23230 15750 1500]
ElementLine[23230 15750 24410 16140 1500]
ElementLine[-25200 -15350 -11020 -7870 1500]
ElementLine[-11020 -7870 -17720 -23620 1500]
ElementLine[-17720 -23620 -4330 -12600 1500]
ElementLine[-4330 -12600 -6300 -28740 1500]
ElementLine[-6300 -28740 2360 -13780 1500]
ElementLine[2360 -13780 7090 -27950 1500]
ElementLine[7090 -27950 9060 -10240 1500]
ElementLine[9060 -10240 18900 -22050 1500]
ElementLine[18900 -22050 11810 -6690 1500]
ElementLine[11810 -6690 24800 -14570 1500]
ElementArc[0 0 13390 13390 0 360 1500]
ElementArc[0 0 29530 29530 0 360 1500]
Pin[-31500 19690 11810 2000 12610 4720 "" "1" ""]
Pin[31500 19690 11810 2000 12610 4720 "" "3" ""]
Pin[-31500 -19690 11810 2000 12610 4720 "" "4" ""]
Pin[31500 -19690 11810 2000 12610 4720 "" "6" ""]
Pin[0 -37400 11810 2000 12610 4720 "" "5" ""]
Pin[0 37400 11810 2000 12610 4720 "" "2" ""]
)
