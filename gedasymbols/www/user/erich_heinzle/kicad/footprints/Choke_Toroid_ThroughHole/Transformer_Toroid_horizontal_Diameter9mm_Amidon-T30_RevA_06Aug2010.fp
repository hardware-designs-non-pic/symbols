# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Transformer_Toroid_horizontal_Diameter9mm_Amidon-T30_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Transformer_Toroid_horizontal_Diameter9mm_Amidon-T30_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 4
#
Element["" "L" "" "" 0 0 0 -25000 0 100 ""]
(
ElementLine[-15350 9060 -4720 3150 1500]
ElementLine[-4720 3150 -11420 12600 1500]
ElementLine[-11420 12600 -3150 5120 1500]
ElementLine[-3150 5120 -5120 16540 1500]
ElementLine[-5120 16540 -390 6300 1500]
ElementLine[-390 6300 1570 16930 1500]
ElementLine[1570 16930 2360 5510 1500]
ElementLine[2360 5510 8270 14960 1500]
ElementLine[8270 14960 4720 3940 1500]
ElementLine[4720 3940 12200 12200 1500]
ElementLine[12200 12200 5910 1970 1500]
ElementLine[5910 1970 15350 7480 1500]
ElementLine[-15750 -8270 -5510 -2760 1500]
ElementLine[-5510 -2760 -10630 -13780 1500]
ElementLine[-10630 -13780 -3150 -5120 1500]
ElementLine[-3150 -5120 -3940 -17320 1500]
ElementLine[-3940 -17320 -790 -6300 1500]
ElementLine[-790 -6300 1570 -17320 1500]
ElementLine[1570 -17320 1970 -5910 1500]
ElementLine[1970 -5910 6690 -15750 1500]
ElementLine[6690 -15750 3940 -4720 1500]
ElementLine[3940 -4720 11810 -12200 1500]
ElementLine[11810 -12200 5910 -1970 1500]
ElementLine[5910 -1970 15350 -7090 1500]
ElementArc[0 0 5910 5910 0 360 1500]
ElementArc[0 0 17720 17720 0 360 1500]
Pin[-21650 11810 7870 2000 8670 3940 "" "1" ""]
Pin[21650 11810 7870 2000 8670 3940 "" "2" ""]
Pin[-21650 -11810 7870 2000 8670 3940 "" "3" ""]
Pin[21650 -11810 7870 2000 8670 3940 "" "4" ""]
)
