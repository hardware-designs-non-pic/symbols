# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_5x10mm_Vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_5x10mm_Vertical
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 45000 -30000 0 100 ""]
(
ElementLine[0 0 14000 0 1500]
ElementLine[0 -20000 14000 -20000 1500]
ElementLine[30000 0 26000 0 1500]
ElementLine[30000 -20000 26000 -20000 1500]
ElementLine[40000 0 30000 0 1500]
ElementLine[40000 -20000 30000 -20000 1500]
ElementLine[40000 0 40000 -20000 1500]
ElementLine[0 -20000 0 0 1500]
Pin[20000 0 7870 2000 8670 3940 "" "1" ""]
Pin[20000 -20000 7870 2000 8670 3940 "" "2" ""]
)
