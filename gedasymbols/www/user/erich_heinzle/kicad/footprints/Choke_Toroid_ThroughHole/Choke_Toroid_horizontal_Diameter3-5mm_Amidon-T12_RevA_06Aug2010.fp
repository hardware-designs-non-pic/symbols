# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_horizontal_Diameter3-5mm_Amidon-T12_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_horizontal_Diameter3-5mm_Amidon-T12_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[-7090 0 -3150 0 1500]
ElementLine[-3150 0 -5910 3150 1500]
ElementLine[-5910 3150 -2360 1970 1500]
ElementLine[-2360 1970 -3150 5910 1500]
ElementLine[-3150 5910 -390 2760 1500]
ElementLine[-390 2760 -390 6690 1500]
ElementLine[-390 6690 1180 2360 1500]
ElementLine[1180 2360 3940 5510 1500]
ElementLine[3940 5510 2360 1570 1500]
ElementLine[2360 1570 5910 2760 1500]
ElementLine[5910 2760 3150 0 1500]
ElementLine[3150 0 6690 0 1500]
ElementArc[0 0 2760 2760 0 360 1500]
ElementArc[0 0 7090 7090 0 360 1500]
Pin[-12990 0 7870 2000 8670 3150 "" "1" ""]
Pin[12990 0 7870 2000 8670 3150 "" "2" ""]
)
