# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Autotransformer_Toroid_Horizontal_1Tap_Diameter14mm_Amidon-T50_RevA_05Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Autotransformer_Toroid_Horizontal_1Tap_Diameter14mm_Amidon-T50_RevA_05Aug2010
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 3
#
Element["" "Tr" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-29530 0 -13780 0 1500]
ElementLine[-13780 0 -27560 8660 1500]
ElementLine[-27560 8660 -12600 5120 1500]
ElementLine[-12600 5120 -22830 18110 1500]
ElementLine[-22830 18110 -8270 10240 1500]
ElementLine[-8270 10240 -12600 25980 1500]
ElementLine[-12600 25980 -3150 12990 1500]
ElementLine[-3150 12990 -390 29130 1500]
ElementLine[-390 29130 5510 12200 1500]
ElementLine[5510 12200 14570 25200 1500]
ElementLine[14570 25200 10630 7870 1500]
ElementLine[10630 7870 25590 14570 1500]
ElementLine[25590 14570 12990 3150 1500]
ElementLine[12990 3150 29130 4330 1500]
ElementLine[29130 4330 13390 -390 1500]
ElementLine[13390 -390 29530 0 1500]
ElementArc[0 0 13390 13390 0 360 1500]
ElementArc[0 0 29530 29530 0 360 1500]
Pin[-39760 0 11810 2000 12610 4720 "" "1" ""]
Pin[0 39370 11810 2000 12610 4720 "" "2" ""]
Pin[39370 0 11810 2000 12610 4720 "" "3" ""]
)
