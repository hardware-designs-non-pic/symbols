# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_6,5x13mm_Vertical
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_6,5x13mm_Vertical
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 55000 -35000 0 100 ""]
(
ElementLine[0 -26000 20500 -26000 1500]
ElementLine[51500 -26000 32000 -26000 1500]
ElementLine[0 0 20000 0 1500]
ElementLine[51500 0 31500 0 1500]
ElementLine[51500 0 51500 -26000 1500]
ElementLine[0 -26000 0 0 1500]
Pin[26000 -2000 7870 2000 8670 3940 "" "1" ""]
Pin[26000 -24000 7870 2000 8670 3940 "" "2" ""]
)
