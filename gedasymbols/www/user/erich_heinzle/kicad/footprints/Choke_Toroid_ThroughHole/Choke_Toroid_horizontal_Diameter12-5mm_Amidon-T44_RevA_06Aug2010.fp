# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Choke_Toroid_horizontal_Diameter12-5mm_Amidon-T44_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Choke_Toroid_horizontal_Diameter12-5mm_Amidon-T44_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 2
#
Element["" "L" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-24410 0 -15350 0 1500]
ElementLine[-15350 0 -23230 7870 1500]
ElementLine[-23230 7870 -13780 7090 1500]
ElementLine[-13780 7090 -17720 16930 1500]
ElementLine[-17720 16930 -9450 12200 1500]
ElementLine[-9450 12200 -10240 22440 1500]
ElementLine[-10240 22440 -3540 14960 1500]
ElementLine[-3540 14960 -790 24410 1500]
ElementLine[-790 24410 3150 14960 1500]
ElementLine[3150 14960 8660 23230 1500]
ElementLine[8660 23230 9450 12200 1500]
ElementLine[9450 12200 16930 17320 1500]
ElementLine[16930 17320 13390 7480 1500]
ElementLine[13390 7480 22830 8270 1500]
ElementLine[22830 8270 15350 390 1500]
ElementLine[15350 390 24800 390 1500]
ElementArc[0 0 15350 15350 0 360 1500]
ElementArc[0 0 24800 24800 0 360 1500]
Pin[-33460 0 11810 2000 12610 4720 "" "1" ""]
Pin[33460 0 11810 2000 12610 4720 "" "2" ""]
)
