# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Transformer_Toroid_Tapped_horizontal_Diameter12-5mm_Amidon-T44_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Transformer_Toroid_Tapped_horizontal_Diameter12-5mm_Amidon-T44_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" "L" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[14570 -5510 22050 -9060 1500]
ElementLine[-22440 -10630 -13780 -7090 1500]
ElementLine[-13780 -7090 -18900 -15350 1500]
ElementLine[-18900 -15350 -10240 -11810 1500]
ElementLine[-10240 -11810 -14570 -19690 1500]
ElementLine[-14570 -19690 -6300 -14170 1500]
ElementLine[-6300 -14170 -5910 -23620 1500]
ElementLine[-5910 -23620 -1570 -15750 1500]
ElementLine[-1570 -15750 1970 -24020 1500]
ElementLine[1970 -24020 3940 -15350 1500]
ElementLine[3940 -15350 9060 -22050 1500]
ElementLine[9060 -22050 8270 -12990 1500]
ElementLine[8270 -12990 14960 -18500 1500]
ElementLine[14960 -18500 11810 -9840 1500]
ElementLine[11810 -9840 19690 -13780 1500]
ElementLine[19690 -13780 14570 -5910 1500]
ElementLine[-22050 11020 -12990 8660 1500]
ElementLine[-12990 8660 -16540 17720 1500]
ElementLine[-16540 17720 -10240 11810 1500]
ElementLine[-10240 11810 -10240 21650 1500]
ElementLine[-10240 21650 -5120 14960 1500]
ElementLine[-5120 14960 -4720 23620 1500]
ElementLine[-4720 23620 -790 15350 1500]
ElementLine[-790 15350 1180 24410 1500]
ElementLine[1180 24410 3940 14960 1500]
ElementLine[3940 14960 8660 22440 1500]
ElementLine[8660 22440 6690 14170 1500]
ElementLine[6690 14170 14570 18900 1500]
ElementLine[14570 18900 10630 11020 1500]
ElementLine[10630 11020 19290 14960 1500]
ElementLine[19290 14960 13780 6690 1500]
ElementLine[13780 6690 22050 10240 1500]
ElementArc[0 0 15350 15350 0 360 1500]
ElementArc[0 0 24800 24800 0 360 1500]
Pin[-29530 13780 11810 2000 12610 4720 "" "1" ""]
Pin[29530 13780 11810 2000 12610 4720 "" "3" ""]
Pin[-29530 -13780 11810 2000 12610 4720 "" "4" ""]
Pin[29530 -13780 11810 2000 12610 4720 "" "6" ""]
Pin[0 32280 11810 2000 12610 4720 "" "2" ""]
Pin[0 -32680 11810 2000 12610 4720 "" "5" ""]
)
