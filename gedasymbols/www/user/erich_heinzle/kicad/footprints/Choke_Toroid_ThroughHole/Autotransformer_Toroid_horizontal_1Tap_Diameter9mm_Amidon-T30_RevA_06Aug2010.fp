# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Autotransformer_Toroid_horizontal_1Tap_Diameter9mm_Amidon-T30_RevA_06Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Autotransformer_Toroid_horizontal_1Tap_Diameter9mm_Amidon-T30_RevA_06Aug2010
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 3
#
Element["" "L" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-16930 0 -6300 0 1500]
ElementLine[-6300 0 -14960 7870 1500]
ElementLine[-14960 7870 -4720 4330 1500]
ElementLine[-4720 4330 -9060 14570 1500]
ElementLine[-9060 14570 -1180 5910 1500]
ElementLine[-1180 5910 790 16930 1500]
ElementLine[790 16930 1970 5510 1500]
ElementLine[1970 5510 9060 14170 1500]
ElementLine[9060 14170 5120 3540 1500]
ElementLine[5120 3540 14960 7870 1500]
ElementLine[14960 7870 5910 -390 1500]
ElementLine[5910 -390 17320 -390 1500]
ElementArc[0 0 5910 5910 0 360 1500]
ElementArc[0 0 17720 17720 0 360 1500]
Pin[-25590 0 11810 2000 12610 4720 "" "1" ""]
Pin[25590 0 11810 2000 12610 4720 "" "3" ""]
Pin[0 25590 11810 2000 12610 4720 "" "2" ""]
)
