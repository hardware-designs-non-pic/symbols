# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_SMD_0603_2Pads_obsolete_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_SMD_0603_2Pads_obsolete_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "XT" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[12200 7280 -12200 7280 1500]
ElementLine[-12200 -7280 12200 -7280 1500]
Pad[-8860 -1575 -8860 1575 6300 2000 7100 "" "1" "square"]
Pad[8860 -1575 8860 1575 6300 2000 7100 "" "2" "square"]
)
