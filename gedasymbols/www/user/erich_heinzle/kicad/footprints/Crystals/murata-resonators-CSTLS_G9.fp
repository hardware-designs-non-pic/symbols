# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module murata-resonators-CSTLS_G9
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: murata-resonators-CSTLS_G9
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -5210 -10370 0 100 ""]
(
ElementLine[-4920 -7480 -4920 7480 500]
ElementArc[-640 35430 43305 43305 269 -20 500]
ElementArc[-640 -35430 43305 43305 91 20 500]
ElementArc[640 35430 43305 43305 -89 20 500]
ElementArc[640 -35430 43305 43305 89 -20 500]
ElementArc[-11810 -100 5901 5901 0 56 500]
ElementArc[11810 -100 5901 5901 180 -56 500]
ElementArc[11810 100 5901 5901 180 56 500]
ElementArc[-11810 100 5901 5901 0 -56 500]
Pin[-9840 0 5200 2000 6000 3200 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 0 5200 2000 6000 3200 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ",onsolder"]
Pin[9840 0 5200 2000 6000 3200 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ",onsolder"]
)
