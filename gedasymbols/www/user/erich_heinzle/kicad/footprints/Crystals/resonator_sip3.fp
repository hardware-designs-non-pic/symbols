# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module resonator_sip3
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: resonator_sip3
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 3
#
Element["" "Resonator_SIP3" "" "" 0 0 0 14170 0 100 ""]
(
ElementLine[-19690 3939 -19690 -3939 1490]
ElementLine[13780 9840 -13780 9840 1490]
ElementLine[19690 -3939 19690 3939 1490]
ElementLine[-13780 -9840 13780 -9840 1490]
ElementArc[-13780 3939 5900 5900 90 -90 1490]
ElementArc[13780 -3939 5900 5900 270 -90 1490]
ElementArc[13780 3939 5910 5910 180 -90 1490]
ElementArc[-13780 -3939 5910 5910 0 -90 1490]
Pin[-9840 0 5900 2000 6700 3150 "" "1" ""]
Pad[-9840 -985 -9840 985 5900 2000 6700 "" "1" ""]
Pad[-9840 -985 -9840 985 5900 2000 6700 "" "1" ",onsolder"]
Pin[0 0 5900 2000 6700 3150 "" "2" ""]
Pad[0 -985 0 985 5900 2000 6700 "" "2" ""]
Pad[0 -985 0 985 5900 2000 6700 "" "2" ",onsolder"]
Pin[9840 0 5900 2000 6700 3150 "" "3" ""]
Pad[9840 -985 9840 985 5900 2000 6700 "" "3" ""]
Pad[9840 -985 9840 985 5900 2000 6700 "" "3" ",onsolder"]
)
