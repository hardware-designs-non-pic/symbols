# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_fa128
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_fa128
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "X***" "" "" 0 0 0 -6690 0 100 ""]
(
ElementLine[-5510 -4720 5510 -4720 590]
ElementLine[5510 -4720 5510 4720 590]
ElementLine[5510 4720 -5510 4720 590]
ElementLine[-5510 4720 -5510 -4720 590]
ElementLine[-3939 -3150 -3939 3150 590]
ElementLine[-3939 3150 3939 3150 590]
ElementLine[3939 3150 3939 -3150 590]
ElementLine[3939 -3150 -3939 -3150 590]
Pad[-3050 2250 -2650 2250 3340 2000 4140 "" "1" "square"]
Pad[2650 -2250 3050 -2250 3340 2000 4140 "" "2" "square"]
Pad[2650 2250 3050 2250 3340 2000 4140 "" "3" "square"]
Pad[-3050 -2250 -2650 -2250 3340 2000 4140 "" "4" "square"]
)
