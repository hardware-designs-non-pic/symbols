# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_smd_5x3.2mm
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_smd_5x3.2mm
# Text descriptor count: 1
# Draw segment object count: 12
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" "X***" "" "" 0 0 0 -7874 0 100 ""]
(
ElementLine[-8661 -6299 8661 -6299 590]
ElementLine[-9842 -1968 -9842 -5118 590]
ElementLine[9842 -1968 9842 -5118 590]
ElementLine[9055 1574 9055 -1574 590]
ElementLine[9055 1574 9842 1968 590]
ElementLine[9055 -1574 9842 -1968 590]
ElementLine[9842 5118 9842 1968 590]
ElementLine[-8661 6299 8661 6299 590]
ElementLine[-9842 5118 -9842 1968 590]
ElementLine[-9842 1968 -9055 1574 590]
ElementLine[-9842 -1968 -9055 -1574 590]
ElementLine[-9055 1574 -9055 -1574 590]
ElementArc[9842 -6299 1181 1181 90 -90 590]
ElementArc[-9842 -6299 1181 1181 180 -90 590]
ElementArc[9842 6299 1181 1181 0 -90 590]
ElementArc[-9842 6299 1181 1181 270 -90 590]
Pad[-7283 -1377 -7283 1377 6692 2000 7492 "" "1" "square"]
Pad[7283 -1377 7283 1377 6692 2000 7492 "" "2" "square"]
)
