# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_smd_ma-406
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_smd_ma-406
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 4
#
Element["" "X***" "" "" 0 0 0 -10236 0 100 ""]
(
ElementLine[-23228 -2755 -20078 -2755 1000]
ElementLine[-20078 -2755 -20078 2755 1000]
ElementLine[-20078 2755 -23228 2755 1000]
ElementLine[-23228 -7874 23228 -7874 1000]
ElementLine[23228 -7874 23228 7874 1000]
ElementLine[-23228 7874 23228 7874 1000]
ElementLine[-23228 -7874 -23228 7874 1000]
ElementArc[14173 0 2998 2998 0 360 1000]
Pad[-18897 6889 -18897 7283 7086 2000 7886 "" "1" "square"]
Pad[-18897 -7283 -18897 -6889 7086 2000 7886 "" "2" "square"]
Pad[18897 6889 18897 7283 7086 2000 7886 "" "3" "square"]
Pad[18897 -7283 18897 -6889 7086 2000 7886 "" "3" "square"]
)
