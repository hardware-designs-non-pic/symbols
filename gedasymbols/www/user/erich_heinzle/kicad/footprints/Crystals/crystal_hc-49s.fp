# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_hc-49s
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_hc-49s
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 2
#
Element["" "HC-49S" "" "" 0 0 0 -13000 0 100 ""]
(
ElementLine[-13000 7000 13000 7000 1000]
ElementLine[13000 -7000 -13000 -7000 1000]
ElementLine[13000 9000 -13000 9000 1000]
ElementLine[-13000 -9000 13000 -9000 1000]
ElementArc[13000 0 9000 9000 270 -90 1000]
ElementArc[13000 0 7000 7000 180 -90 1000]
ElementArc[13000 0 7000 7000 270 -90 1000]
ElementArc[-13000 0 7000 7000 90 -90 1000]
ElementArc[-13000 0 7000 7000 0 -90 1000]
ElementArc[13000 0 9000 9000 180 -90 1000]
ElementArc[-13000 0 9000 9000 90 -90 1000]
ElementArc[-13000 0 9000 9000 0 -90 1000]
Pin[-10000 0 7870 2000 8670 3150 "" "1" ""]
Pin[10000 0 7870 2000 8670 3150 "" "2" ""]
)
