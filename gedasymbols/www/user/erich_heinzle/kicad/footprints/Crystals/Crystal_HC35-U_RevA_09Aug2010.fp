# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_HC35-U_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_HC35-U_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 3
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 3
#
Element["" "XT" "" "" 0 0 0 -5000 0 100 ""]
(
ElementLine[11420 -13780 13390 -16140 1500]
ElementLine[13390 -16140 15750 -13390 1500]
ElementLine[15750 -13390 13780 -11810 1500]
ElementArc[0 0 18110 18110 0 360 1500]
Pin[-9840 0 4720 2000 5520 2360 "" "1" ""]
Pin[9840 0 4720 2000 5520 2360 "" "2" ""]
Pin[0 9840 4720 2000 5520 2360 "" "3" ""]
)
