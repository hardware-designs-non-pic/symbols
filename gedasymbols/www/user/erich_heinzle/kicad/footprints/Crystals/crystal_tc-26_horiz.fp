# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_tc-26_horiz
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_tc-26_horiz
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "TC-26_Horiz" "" "" 0 0 0 -16929 0 100 ""]
(
ElementLine[-3937 -10629 -3937 12992 1000]
ElementLine[-3937 -10629 3937 -10629 1000]
ElementLine[3937 -10629 3937 12992 1000]
ElementLine[-3937 12992 3937 12992 1000]
Pin[-1377 -12480 1968 2000 2768 984 "" "1" ""]
Pin[1377 -12480 1968 2000 2768 984 "" "2" ""]
Pad[0 12992 0 12992 7874 2000 8674 "" "" "square"]
)
