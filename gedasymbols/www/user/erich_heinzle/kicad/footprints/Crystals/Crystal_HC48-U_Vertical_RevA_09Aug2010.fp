# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_HC48-U_Vertical_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_HC48-U_Vertical_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 61
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "XT" "" "" 0 0 0 0 0 100 ""]
(
ElementLine[-20280 -14960 19880 -15160 1500]
ElementLine[19880 -15160 22240 -14760 1500]
ElementLine[22240 -14760 25590 -13780 1500]
ElementLine[25590 -13780 28350 -12200 1500]
ElementLine[28350 -12200 31690 -8860 1500]
ElementLine[31690 -8860 33460 -5510 1500]
ElementLine[33460 -5510 34250 -2170 1500]
ElementLine[34250 -2170 34450 390 1500]
ElementLine[34450 390 33460 5310 1500]
ElementLine[33460 5310 31690 8660 1500]
ElementLine[31690 8660 29330 11220 1500]
ElementLine[29330 11220 26380 13190 1500]
ElementLine[26380 13190 22240 14570 1500]
ElementLine[22240 14570 18900 14960 1500]
ElementLine[18900 14960 -19690 14960 1500]
ElementLine[-19690 14960 -23430 14370 1500]
ElementLine[-23430 14370 -26380 13190 1500]
ElementLine[-26380 13190 -28940 11610 1500]
ElementLine[-28940 11610 -31100 9250 1500]
ElementLine[-31100 9250 -33270 5710 1500]
ElementLine[-33270 5710 -34060 3540 1500]
ElementLine[-34060 3540 -34450 -790 1500]
ElementLine[-34450 -790 -33660 -4920 1500]
ElementLine[-33660 -4920 -31890 -8460 1500]
ElementLine[-31890 -8460 -28940 -11610 1500]
ElementLine[-28940 -11610 -25790 -13580 1500]
ElementLine[-25790 -13580 -22640 -14760 1500]
ElementLine[-22640 -14760 -20080 -14960 1500]
ElementLine[20080 17910 -19090 17910 1500]
ElementLine[-19090 17910 -21850 17720 1500]
ElementLine[-21850 17720 -25590 16930 1500]
ElementLine[-25590 16930 -28540 15350 1500]
ElementLine[-28540 15350 -31500 13190 1500]
ElementLine[-31500 13190 -34250 10240 1500]
ElementLine[-34250 10240 -36220 6100 1500]
ElementLine[-36220 6100 -37200 2560 1500]
ElementLine[-37200 2560 -37600 0 1500]
ElementLine[-37600 0 -37200 -3150 1500]
ElementLine[-37200 -3150 -35830 -7280 1500]
ElementLine[-35830 -7280 -33860 -10630 1500]
ElementLine[-33860 -10630 -32090 -12800 1500]
ElementLine[-32090 -12800 -29330 -14960 1500]
ElementLine[-29330 -14960 -25980 -16730 1500]
ElementLine[-25980 -16730 -22830 -17520 1500]
ElementLine[-22830 -17520 -19880 -17910 1500]
ElementLine[-19880 -17910 20080 -17910 1500]
ElementLine[20080 -17910 23820 -17520 1500]
ElementLine[23820 -17520 27170 -16140 1500]
ElementLine[27170 -16140 30710 -13980 1500]
ElementLine[30710 -13980 33070 -11610 1500]
ElementLine[33070 -11610 36220 -6500 1500]
ElementLine[36220 -6500 37200 -2560 1500]
ElementLine[37200 -2560 37400 980 1500]
ElementLine[37400 980 36810 4530 1500]
ElementLine[36810 4530 35830 7280 1500]
ElementLine[35830 7280 34060 10240 1500]
ElementLine[34060 10240 31500 13190 1500]
ElementLine[31500 13190 29720 14760 1500]
ElementLine[29720 14760 26570 16340 1500]
ElementLine[26570 16340 23030 17520 1500]
ElementLine[23030 17520 20080 17910 1500]
Pin[-24210 0 9840 2000 10640 5910 "" "1" ""]
Pin[24210 0 9840 2000 10640 5910 "" "2" ""]
)
