# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_HC52-U_Vertical_3Pin_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_HC52-U_Vertical_3Pin_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 46
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "XT" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[5750 -4690 -5910 -4690 1500]
ElementLine[8740 6540 -9490 6540 1500]
ElementLine[-6690 4800 5710 4800 1500]
ElementLine[9410 -4530 11060 -4210 1500]
ElementLine[11060 -4210 12170 -3500 1500]
ElementLine[12170 -3500 13190 -2170 1500]
ElementLine[13190 -2170 13700 -870 1500]
ElementLine[13700 -870 13820 80 1500]
ElementLine[13820 80 13460 1610 1500]
ElementLine[13460 1610 12830 2760 1500]
ElementLine[12830 2760 11770 3780 1500]
ElementLine[11770 3780 10390 4450 1500]
ElementLine[10390 4450 8460 4570 1500]
ElementLine[-9490 4530 -11060 4130 1500]
ElementLine[-11060 4130 -12360 3310 1500]
ElementLine[-12360 3310 -13230 2130 1500]
ElementLine[-13230 2130 -13740 830 1500]
ElementLine[-13740 830 -13740 -670 1500]
ElementLine[-13740 -670 -13230 -2130 1500]
ElementLine[-13230 -2130 -12320 -3350 1500]
ElementLine[-12320 -3350 -11300 -4130 1500]
ElementLine[-11300 -4130 -10000 -4450 1500]
ElementLine[-10000 -4450 -9170 -4530 1500]
ElementLine[-9410 -6500 9170 -6500 1500]
ElementLine[9170 -6500 10510 -6340 1500]
ElementLine[10510 -6340 11970 -5870 1500]
ElementLine[11970 -5870 13700 -4690 1500]
ElementLine[13700 -4690 15200 -2600 1500]
ElementLine[15200 -2600 15710 -430 1500]
ElementLine[15710 -430 15590 1140 1500]
ElementLine[15590 1140 14840 3270 1500]
ElementLine[14840 3270 12950 5310 1500]
ElementLine[12950 5310 10940 6300 1500]
ElementLine[10940 6300 8820 6500 1500]
ElementLine[-9530 6460 -11420 6060 1500]
ElementLine[-11420 6060 -12760 5510 1500]
ElementLine[-12760 5510 -13860 4530 1500]
ElementLine[-13860 4530 -14720 3430 1500]
ElementLine[-14720 3430 -15430 1810 1500]
ElementLine[-15430 1810 -15710 280 1500]
ElementLine[-15710 280 -15630 -1650 1500]
ElementLine[-15630 -1650 -14880 -3230 1500]
ElementLine[-14880 -3230 -13660 -4690 1500]
ElementLine[-13660 -4690 -12280 -5710 1500]
ElementLine[-12280 -5710 -10940 -6220 1500]
ElementLine[-10940 -6220 -9370 -6500 1500]
Pin[-7480 0 5510 2000 6310 3150 "" "1" ""]
Pin[7480 0 5510 2000 6310 3150 "" "2" ""]
Pin[0 0 5510 2000 6310 3150 "" "3" ""]
)
