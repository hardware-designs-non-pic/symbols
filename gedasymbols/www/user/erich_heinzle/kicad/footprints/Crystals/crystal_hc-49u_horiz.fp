# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_hc-49u_horiz
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_hc-49u_horiz
# Text descriptor count: 1
# Draw segment object count: 11
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "HC-49U_Horiz" "" "" 0 0 0 -34250 0 100 ""]
(
ElementLine[10000 31000 10000 23000 1500]
ElementLine[-10000 31000 -10000 23000 1500]
ElementLine[21000 21000 22000 21000 1500]
ElementLine[22000 21000 22000 23000 1500]
ElementLine[22000 23000 -22000 23000 1500]
ElementLine[-22000 23000 -22000 21000 1500]
ElementLine[-22000 21000 -21000 21000 1500]
ElementLine[-21000 -30000 21000 -30000 1500]
ElementLine[21000 -30000 21000 21000 1500]
ElementLine[21000 21000 -21000 21000 1500]
ElementLine[-21000 21000 -21000 -30000 1500]
Pin[-10000 30880 7870 2000 8670 3150 "" "1" ""]
Pin[10000 30880 7870 2000 8670 3150 "" "2" ""]
)
