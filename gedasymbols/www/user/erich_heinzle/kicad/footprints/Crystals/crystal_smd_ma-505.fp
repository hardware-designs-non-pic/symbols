# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_smd_ma-505
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_smd_ma-505
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "X***" "" "" 0 0 0 -12598 0 100 ""]
(
ElementLine[-16929 9842 -16929 -9842 1000]
ElementLine[-17716 -9842 -17716 9842 1000]
ElementLine[-18503 9842 -18503 -9842 1000]
ElementLine[-18897 9842 -18897 -9842 1000]
ElementLine[-25000 -10000 25000 -10000 1000]
ElementLine[25000 -10000 25000 10000 1000]
ElementLine[-25000 10000 25000 10000 1000]
ElementLine[-25000 -10000 -25000 10000 1000]
Pad[-21850 -2952 -21850 2952 16141 2000 16941 "" "1" "square"]
Pad[21850 -2952 21850 2952 16141 2000 16941 "" "2" "square"]
)
