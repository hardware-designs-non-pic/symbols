# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module murata-resonators-CSTLA_T
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: murata-resonators-CSTLA_T
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -7500 -12500 0 100 ""]
(
ElementLine[-4920 -9440 -4920 9440 500]
ElementArc[1620 16530 26420 26420 -86 -42 500]
ElementArc[-1620 16530 26420 26420 266 42 500]
ElementArc[1620 -16530 26420 26420 86 42 500]
ElementArc[-1620 -16530 26420 26420 94 -42 500]
ElementArc[-13770 0 5910 5910 0 -45 500]
ElementArc[-13770 0 5910 5910 0 45 500]
ElementArc[13770 0 5910 5910 180 -45 500]
ElementArc[13770 0 5910 5910 180 45 500]
Pin[-9840 0 5200 2000 6000 3200 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 0 5200 2000 6000 3200 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ",onsolder"]
Pin[9840 0 5200 2000 6000 3200 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ",onsolder"]
)
