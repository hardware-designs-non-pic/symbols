# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_tc-38
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_tc-38
# Text descriptor count: 1
# Draw segment object count: 0
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "TC-38" "" "" 0 0 0 -10236 0 100 ""]
(
ElementArc[0 0 6299 6299 0 360 1000]
Pin[-2165 0 2755 2000 3555 1377 "" "1" ""]
Pin[2165 0 2755 2000 3555 1377 "" "2" ""]
)
