# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module resonator_awscr
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: resonator_awscr
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "X***" "" "" 0 0 0 -6690 0 100 ""]
(
ElementLine[-4921 -3937 -4921 3937 590]
ElementLine[-4921 3937 4921 3937 590]
ElementLine[4921 3937 4921 -3937 590]
ElementLine[4921 -3937 -4921 -3937 590]
Pad[-3937 2952 -3937 4133 1968 2000 2768 "" "1" "square"]
Pad[3937 2952 3937 4133 1968 2000 2768 "" "3" "square"]
Pad[0 2952 0 4133 1968 2000 2768 "" "2" "square"]
Pad[3937 -4133 3937 -2952 1968 2000 2768 "" "3" "square"]
Pad[0 -4133 0 -2952 1968 2000 2768 "" "2" "square"]
Pad[-3937 -4133 -3937 -2952 1968 2000 2768 "" "1" "square"]
)
