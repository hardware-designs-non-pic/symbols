# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module murata-resonators-CSTLS_X
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: murata-resonators-CSTLS_X
# Text descriptor count: 1
# Draw segment object count: 1
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -3240 -10370 0 100 ""]
(
ElementLine[-4920 -6290 -4920 6290 500]
ElementArc[90 9250 16330 16330 -90 35 500]
ElementArc[-90 9250 16330 16330 270 -35 500]
ElementArc[90 -9250 16330 16330 90 35 500]
ElementArc[-90 -9250 16330 16330 90 -35 500]
ElementArc[-5110 -100 5911 5911 0 -42 500]
ElementArc[5110 -100 5911 5911 180 -42 500]
ElementArc[-5110 100 5911 5911 0 42 500]
ElementArc[5110 100 5911 5911 180 42 500]
Pin[-9840 0 5200 2000 6000 3200 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ""]
Pad[-9840 -2600 -9840 2600 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 0 5200 2000 6000 3200 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ""]
Pad[0 -2600 0 2600 5200 2000 6000 "" "2" ",onsolder"]
Pin[9840 0 5200 2000 6000 3200 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ""]
Pad[9840 -2600 9840 2600 5200 2000 6000 "" "3" ",onsolder"]
)
