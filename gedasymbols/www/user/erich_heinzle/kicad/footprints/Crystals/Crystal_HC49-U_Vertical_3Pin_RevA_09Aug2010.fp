# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_HC49-U_Vertical_3Pin_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_HC49-U_Vertical_3Pin_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 51
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "XT" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[18500 -3940 19290 -2360 1500]
ElementLine[19290 -2360 19690 0 1500]
ElementLine[19690 0 19290 1970 1500]
ElementLine[19290 1970 17720 4720 1500]
ElementLine[17720 4720 15350 6300 1500]
ElementLine[15350 6300 12990 7090 1500]
ElementLine[12990 7090 -12990 7090 1500]
ElementLine[-12990 7090 -15750 6300 1500]
ElementLine[-15750 6300 -17320 5120 1500]
ElementLine[-17320 5120 -18900 3150 1500]
ElementLine[-18900 3150 -19690 790 1500]
ElementLine[-19690 790 -19690 -1180 1500]
ElementLine[-19690 -1180 -18900 -3150 1500]
ElementLine[-18900 -3150 -16930 -5510 1500]
ElementLine[-16930 -5510 -14960 -6690 1500]
ElementLine[-14960 -6690 -12990 -7090 1500]
ElementLine[-12600 -7090 13390 -7090 1500]
ElementLine[13390 -7090 14960 -6690 1500]
ElementLine[14960 -6690 16930 -5510 1500]
ElementLine[16930 -5510 18900 -3540 1500]
ElementLine[-12560 -9170 -14370 -8980 1500]
ElementLine[-14370 -8980 -15940 -8540 1500]
ElementLine[-15940 -8540 -17640 -7680 1500]
ElementLine[-17640 -7680 -18780 -6770 1500]
ElementLine[-18780 -6770 -20080 -5390 1500]
ElementLine[-20080 -5390 -21220 -3270 1500]
ElementLine[-21220 -3270 -21730 -910 1500]
ElementLine[-21730 -910 -21730 1100 1500]
ElementLine[-21730 1100 -21060 3860 1500]
ElementLine[-21060 3860 -19490 6180 1500]
ElementLine[-19490 6180 -17680 7640 1500]
ElementLine[-17680 7640 -16020 8460 1500]
ElementLine[-16020 8460 -14250 9090 1500]
ElementLine[-14250 9090 -12520 9210 1500]
ElementLine[16380 8350 17870 7480 1500]
ElementLine[17870 7480 19130 6380 1500]
ElementLine[19130 6380 20120 5080 1500]
ElementLine[20120 5080 21300 2910 1500]
ElementLine[21300 2910 21730 1060 1500]
ElementLine[21730 1060 21810 -750 1500]
ElementLine[21810 -750 21460 -2560 1500]
ElementLine[21460 -2560 20710 -4330 1500]
ElementLine[20710 -4330 19290 -6180 1500]
ElementLine[19290 -6180 17910 -7440 1500]
ElementLine[17910 -7440 16380 -8350 1500]
ElementLine[16380 -8350 14690 -8900 1500]
ElementLine[14690 -8900 12950 -9170 1500]
ElementLine[-12600 9170 12800 9170 1500]
ElementLine[12800 9170 14450 9020 1500]
ElementLine[14450 9020 16380 8350 1500]
ElementLine[-12600 -9170 12800 -9170 1500]
Pin[-9610 0 5910 2000 6710 3150 "" "1" ""]
Pin[9610 0 5910 2000 6710 3150 "" "2" ""]
Pin[0 0 5910 2000 6710 3150 "" "3" ""]
)
