# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_HC49-U_Horizontal_Typ2_RevA_09Aug2010
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_HC49-U_Horizontal_Typ2_RevA_09Aug2010
# Text descriptor count: 1
# Draw segment object count: 6
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" "XT" "" "" 0 0 0 -75000 0 100 ""]
(
ElementLine[9450 -5510 9450 -9840 1500]
ElementLine[-9840 -5510 -9840 -9840 1500]
ElementLine[-19690 -9840 -19690 -63390 1500]
ElementLine[-19690 -63390 19690 -63390 1500]
ElementLine[19690 -63390 19690 -9840 1500]
ElementLine[-21650 -9840 21650 -9840 1500]
Pin[-9610 0 5910 2000 6710 3150 "" "1" ""]
Pin[9610 0 5910 2000 6710 3150 "" "2" ""]
Pad[0 -43900 0 -30120 39370 2000 40170 "" "3" "square"]
Pin[24020 -37400 4720 2000 5520 3150 "" "3" "square"]
Pin[-24020 -37400 4720 2000 5520 3150 "" "3" "square"]
Pin[0 -68110 4720 2000 5520 3150 "" "3" "square"]
)
