# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Crystal_Round_Horizontal_3mm_BigPad_RevA_25Apr2012
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: Crystal_Round_Horizontal_3mm_BigPad_RevA_25Apr2012
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "XT" "" "" 0 0 980 -46650 0 100 ""]
(
ElementLine[6300 -4920 6300 -38780 1500]
ElementLine[6300 -38780 -6300 -38780 1500]
ElementLine[-6300 -38780 -6300 -4920 1500]
ElementLine[-3350 -4920 -6300 -4920 1500]
ElementLine[3540 -4920 6300 -4920 1500]
ElementLine[-1180 -4920 -1570 -3740 1500]
ElementLine[1180 -4920 1570 -3740 1500]
ElementLine[3540 -4920 -3540 -4920 1500]
Pin[-2950 0 3940 2000 4740 2360 "" "1" ""]
Pad[-2950 -985 -2950 985 3940 2000 4740 "" "1" ""]
Pad[-2950 -985 -2950 985 3940 2000 4740 "" "1" ",onsolder"]
Pin[2950 0 3940 2000 4740 2360 "" "2" ""]
Pad[2950 -985 2950 985 3940 2000 4740 "" "2" ""]
Pad[2950 -985 2950 985 3940 2000 4740 "" "2" ",onsolder"]
)
