# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_xrcgb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_xrcgb
# Text descriptor count: 1
# Draw segment object count: 16
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 4
#
Element["" "X***" "" "" 0 0 0 -6690 0 100 ""]
(
ElementLine[-2755 5905 -3937 4724 590]
ElementLine[-6692 1968 -5511 3149 590]
ElementLine[-2755 5905 -6692 5905 590]
ElementLine[-6692 5905 -6692 1968 590]
ElementLine[-6299 2362 -6299 5511 590]
ElementLine[-6299 5511 -3149 5511 590]
ElementLine[-5905 5118 -5905 2755 590]
ElementLine[-5905 5118 -3543 5118 590]
ElementLine[-5511 -4724 5511 -4724 590]
ElementLine[5511 -4724 5511 4724 590]
ElementLine[5511 4724 -5511 4724 590]
ElementLine[-5511 4724 -5511 -4724 590]
ElementLine[-3937 -2362 -3937 2362 590]
ElementLine[-3149 3149 3149 3149 590]
ElementLine[3937 2362 3937 -2362 590]
ElementLine[3149 -3149 -3149 -3149 590]
ElementArc[-3937 -3149 787 787 180 -90 590]
ElementArc[-3937 3149 787 787 270 -90 590]
ElementArc[3937 3149 787 787 0 -90 590]
ElementArc[3937 -3149 787 787 90 -90 590]
Pad[-2755 1968 -2559 1968 2755 2000 3555 "" "1" "square"]
Pad[2559 -1968 2755 -1968 2755 2000 3555 "" "3" "square"]
Pad[2559 1968 2755 1968 2755 2000 3555 "" "2" "square"]
Pad[-2755 -1968 -2559 -1968 2755 2000 3555 "" "2" "square"]
)
