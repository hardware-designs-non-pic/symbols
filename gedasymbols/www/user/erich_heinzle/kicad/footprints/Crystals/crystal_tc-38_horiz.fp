# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module crystal_tc-38_horiz
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: crystal_tc-38_horiz
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "TC-38_Horiz" "" "" 0 0 0 -22834 0 100 ""]
(
ElementLine[-6299 -14960 -6299 18110 1000]
ElementLine[-6299 -14960 6299 -14960 1000]
ElementLine[6299 -14960 6299 18110 1000]
ElementLine[-6299 18110 6299 18110 1000]
Pin[-2165 -17322 2755 2000 3555 1377 "" "1" ""]
Pin[2165 -17322 2755 2000 3555 1377 "" "2" ""]
Pad[0 18110 0 18110 11811 2000 12611 "" "" "square"]
)
