# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Heatsink_SheetType_50x7mm_2Fixations
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Heatsink_SheetType_50x7mm_2Fixations
# Text descriptor count: 1
# Draw segment object count: 33
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "HS" "" "" 0 0 0 -40000 0 100 ""]
(
ElementLine[-39500 0 -77000 0 1500]
ElementLine[-8000 0 -23500 0 1500]
ElementLine[23500 0 8000 0 1500]
ElementLine[55000 0 39000 0 1500]
ElementLine[55000 0 76500 0 1500]
ElementLine[-23500 -28000 -39500 -28000 1500]
ElementLine[8000 -28000 -8000 -28000 1500]
ElementLine[-43500 -14000 -43500 0 1500]
ElementLine[-55500 -14000 -55500 0 1500]
ElementLine[-39500 -28000 -39500 0 1500]
ElementLine[-23500 -28000 -23500 0 1500]
ElementLine[-8000 -28000 -8000 0 1500]
ElementLine[39000 -28000 23500 -28000 1500]
ElementLine[39000 -28000 39000 0 1500]
ElementLine[23500 -28000 23500 0 1500]
ElementLine[8000 -27500 8000 0 1500]
ElementLine[43500 -14000 43500 0 1500]
ElementLine[-49500 2000 -49500 6500 1500]
ElementLine[-49500 -6000 -49500 -1500 1500]
ElementLine[-49500 -15500 -49500 -10000 1500]
ElementLine[-49500 -21500 -49500 -17500 1500]
ElementLine[-49500 -30500 -49500 -25500 1500]
ElementLine[-49500 -35500 -49500 -33000 1500]
ElementLine[49500 6500 49500 10500 1500]
ElementLine[49500 -3000 49500 3500 1500]
ElementLine[49500 -10500 49500 -6500 1500]
ElementLine[49500 -19500 49500 -15000 1500]
ElementLine[49500 -28000 49500 -24000 1500]
ElementLine[49500 -35000 49500 -31500 1500]
ElementLine[55000 -14000 55000 0 1500]
ElementLine[100500 0 100500 -14000 1500]
ElementLine[100500 -14000 -100500 -14000 1500]
ElementLine[-100500 -14000 -100500 0 1500]
Pin[89000 0 19690 2000 20490 9840 "" "1" ""]
Pin[-89000 0 19690 2000 20490 9840 "" "1" ""]
)
