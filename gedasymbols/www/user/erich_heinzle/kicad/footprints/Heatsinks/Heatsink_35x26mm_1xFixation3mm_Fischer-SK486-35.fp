# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Heatsink_35x26mm_1xFixation3mm_Fischer-SK486-35
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Heatsink_35x26mm_1xFixation3mm_Fischer-SK486-35
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 1
#
Element["" "HS" "" "" 0 0 62500 -92000 0 100 ""]
(
ElementLine[0 0 138000 0 1500]
ElementLine[138000 0 138000 -102500 1500]
ElementLine[138000 -102500 0 -102500 1500]
ElementLine[0 -102500 0 0 1500]
Pin[102500 -51500 25580 2000 26380 12200 "" "1" ""]
)
