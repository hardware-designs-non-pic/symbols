# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Heatsink_38x38mm_SpringFixation
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Heatsink_38x38mm_SpringFixation
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "HS" "" "" 0 0 80000 -122000 0 100 ""]
(
ElementLine[150000 -80000 172000 -80000 1500]
ElementLine[172000 -80000 172000 26000 1500]
ElementLine[164000 66000 150000 66000 1500]
ElementLine[150000 66000 150000 0 1500]
ElementLine[0 -70000 -22000 -70000 1500]
ElementLine[-22000 -70000 -22000 -176000 1500]
ElementLine[-14000 -216000 0 -216000 1500]
ElementLine[0 -216000 0 -150000 1500]
ElementLine[-30000 -176000 -14000 -176000 1500]
ElementLine[-14000 -176000 -14000 -216000 1500]
ElementLine[-14000 -216000 -30000 -216000 1500]
ElementLine[-30000 -216000 -30000 -176000 1500]
ElementLine[164000 26000 164000 66000 1500]
ElementLine[180000 26000 180000 66000 1500]
ElementLine[180000 66000 164000 66000 1500]
ElementLine[180000 26000 164000 26000 1500]
ElementLine[0 0 150000 0 1500]
ElementLine[150000 0 150000 -150000 1500]
ElementLine[150000 -150000 0 -150000 1500]
ElementLine[0 -150000 0 0 1500]
Pin[172000 36000 11810 2000 12610 4330 "" "1" ""]
Pin[-22000 -186000 11810 2000 12610 4330 "" "1" ""]
Pin[-22000 -208000 11810 2000 12610 4330 "" "1" ""]
Pin[172000 58000 11810 2000 12610 4330 "" "1" ""]
)
