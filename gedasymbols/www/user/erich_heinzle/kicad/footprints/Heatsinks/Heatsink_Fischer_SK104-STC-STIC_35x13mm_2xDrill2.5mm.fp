# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Heatsink_Fischer_SK104-STC-STIC_35x13mm_2xDrill2.5mm
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Heatsink_Fischer_SK104-STC-STIC_35x13mm_2xDrill2.5mm
# Text descriptor count: 1
# Draw segment object count: 42
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "HS" "" "" 0 0 5000 -90000 0 100 ""]
(
ElementLine[-7000 -3000 -7000 3000 1500]
ElementLine[7000 -3000 7000 3000 1500]
ElementLine[-33500 25000 -31500 25000 1500]
ElementLine[69000 -25000 47500 -14000 1500]
ElementLine[47500 -14000 50000 -25000 1500]
ElementLine[-39500 -10500 -50000 -25000 1500]
ElementLine[-50000 -25000 -48000 -14000 1500]
ElementLine[-48000 -14000 -69000 -25000 1500]
ElementLine[-40000 10500 -50000 25000 1500]
ElementLine[-50000 25000 -48000 14500 1500]
ElementLine[-48000 14500 -69000 25000 1500]
ElementLine[-60000 10000 -69000 25000 1500]
ElementLine[-60000 -10000 -69000 -25000 1500]
ElementLine[31500 -25000 33000 -25000 1500]
ElementLine[31500 25000 33500 25000 1500]
ElementLine[39500 10500 50000 25000 1500]
ElementLine[50000 25000 47500 14000 1500]
ElementLine[47500 14000 69000 25000 1500]
ElementLine[60000 9500 69000 13000 1500]
ElementLine[69000 13000 63000 6000 1500]
ElementLine[69000 25000 60000 9500 1500]
ElementLine[60000 -10000 68500 -13000 1500]
ElementLine[68500 -13000 62500 -7000 1500]
ElementLine[69000 -25000 60000 -10000 1500]
ElementLine[40000 -10500 50000 -25000 1500]
ElementLine[33500 25000 39500 10500 1500]
ElementLine[33500 -25000 40000 -10500 1500]
ElementLine[-33500 25000 -40000 10000 1500]
ElementLine[-31500 -25000 -33500 -25000 1500]
ElementLine[-33500 -25000 -39500 -10500 1500]
ElementLine[-69000 13000 -62000 7500 1500]
ElementLine[-69500 -13000 -62500 -7000 1500]
ElementLine[-69000 13000 -60000 10000 1500]
ElementLine[-69000 -13000 -60000 -10000 1500]
ElementLine[0 3000 -31500 3000 1500]
ElementLine[-31500 3000 -31500 25000 1500]
ElementLine[0 3000 31500 3000 1500]
ElementLine[31500 3000 31500 25000 1500]
ElementLine[0 -3000 -31500 -3000 1500]
ElementLine[-31500 -3000 -31500 -25000 1500]
ElementLine[0 -3000 31500 -3000 1500]
ElementLine[31500 -3000 31500 -25000 1500]
Pin[50000 0 23620 2000 24420 9840 "" "1" ""]
Pin[-50000 0 23620 2000 24420 9840 "" "1" ""]
)
