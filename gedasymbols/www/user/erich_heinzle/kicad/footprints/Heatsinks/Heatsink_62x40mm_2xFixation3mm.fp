# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Heatsink_62x40mm_2xFixation3mm
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Heatsink_62x40mm_2xFixation3mm
# Text descriptor count: 1
# Draw segment object count: 44
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" "HS" "" "" 0 0 109000 -135500 0 100 ""]
(
ElementLine[264000 -48500 266000 -48500 1500]
ElementLine[266000 -48500 268000 -48000 1500]
ElementLine[268000 -48000 271500 -46500 1500]
ElementLine[271500 -46500 273000 -45500 1500]
ElementLine[273000 -45500 276000 -42500 1500]
ElementLine[276000 -42500 277500 -40000 1500]
ElementLine[277500 -40000 278000 -39000 1500]
ElementLine[278000 -39000 278500 -37000 1500]
ElementLine[278500 -37000 279000 -34500 1500]
ElementLine[279000 -34500 279000 -32000 1500]
ElementLine[279000 -32000 278000 -28000 1500]
ElementLine[278000 -28000 277500 -27000 1500]
ElementLine[277500 -27000 277000 -26000 1500]
ElementLine[277000 -26000 276000 -24500 1500]
ElementLine[276000 -24500 273500 -22000 1500]
ElementLine[273500 -22000 271500 -20500 1500]
ElementLine[271500 -20500 269500 -19500 1500]
ElementLine[269500 -19500 267500 -19000 1500]
ElementLine[267500 -19000 265500 -18500 1500]
ElementLine[265500 -18500 264000 -18500 1500]
ElementLine[264000 -48500 244500 -48500 1500]
ElementLine[264000 -18500 244500 -18500 1500]
ElementLine[-19500 -139000 -23500 -138500 1500]
ElementLine[-23500 -138500 -27000 -137000 1500]
ElementLine[-27000 -137000 -29000 -135500 1500]
ElementLine[-29000 -135500 -31000 -133500 1500]
ElementLine[-31000 -133500 -32500 -131500 1500]
ElementLine[-32500 -131500 -33500 -129500 1500]
ElementLine[-33500 -129500 -34000 -128000 1500]
ElementLine[-34000 -128000 -34500 -125000 1500]
ElementLine[-34500 -125000 -34500 -123000 1500]
ElementLine[-34500 -123000 -34000 -120500 1500]
ElementLine[-34000 -120500 -33000 -117500 1500]
ElementLine[-33000 -117500 -31500 -115000 1500]
ElementLine[-31500 -115000 -29500 -113000 1500]
ElementLine[-29500 -113000 -27000 -111000 1500]
ElementLine[-27000 -111000 -23500 -109500 1500]
ElementLine[-23500 -109500 -19500 -109000 1500]
ElementLine[0 -109000 -19500 -109000 1500]
ElementLine[-19500 -139000 0 -139000 1500]
ElementLine[0 0 244500 0 1500]
ElementLine[244500 0 244500 -157500 1500]
ElementLine[244500 -157500 0 -157500 1500]
ElementLine[0 -157500 0 0 1500]
Pin[264000 -33500 25580 2000 26380 12200 "" "1" ""]
Pin[-19500 -124000 25580 2000 26380 12200 "" "1" ""]
)
