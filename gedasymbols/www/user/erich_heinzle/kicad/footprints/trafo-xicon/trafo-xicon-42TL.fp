# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module trafo-xicon-42TL
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: trafo-xicon-42TL
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 2
# Draw arc object count: 4
# Pad count: 6
#
Element["" "6" "" "" 0 0 -16560 -23770 0 100 ""]
(
ElementLine[-29500 11550 29600 11550 500]
ElementLine[29600 -11480 29600 11550 500]
ElementLine[-22220 -11640 -22220 -22170 500]
ElementLine[-14930 29550 14940 29550 500]
ElementLine[22220 22270 22220 11850 500]
ElementLine[-22210 22270 -22210 11850 500]
ElementLine[22220 -22170 22220 -11640 500]
ElementLine[14940 -29450 -14940 -29450 500]
ElementLine[29500 -11550 -29600 -11550 500]
ElementLine[-29600 11540 -29600 -11550 500]
ElementLine[-32690 0 -25200 0 500]
ElementLine[-28950 3740 -28950 -3740 500]
ElementLine[25200 0 32690 0 500]
ElementLine[28950 3740 28950 -3740 500]
ElementArc[-28950 0 2645 2645 0 360 500]
ElementArc[28950 0 2645 2645 0 360 500]
ElementArc[-14940 -22170 7280 7280 0 -90 500]
ElementArc[14940 -22170 7280 7280 270 -90 500]
ElementArc[-14930 22270 7280 7280 0 90 500]
ElementArc[14940 22270 7280 7280 180 -90 500]
Pin[-10000 24000 5110 2000 5910 2750 "" "1" ""]
Pin[0 23890 5110 2000 5910 2750 "" "2" ""]
Pin[10000 24000 5110 2000 5910 2750 "" "3" ""]
Pin[10000 -24000 5110 2000 5910 2750 "" "4" ""]
Pin[0 -24000 5110 2000 5910 2750 "" "5" ""]
Pin[-10000 -24000 5110 2000 5910 2750 "" "6" ""]
)
