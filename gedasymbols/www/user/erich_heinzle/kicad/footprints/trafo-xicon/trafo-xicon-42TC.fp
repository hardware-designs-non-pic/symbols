# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module trafo-xicon-42TC
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: trafo-xicon-42TC
# Text descriptor count: 1
# Draw segment object count: 14
# Draw circle object count: 2
# Draw arc object count: 4
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -5000 -5000 0 100 ""]
(
ElementLine[16530 9640 16530 21450 500]
ElementLine[16530 -9640 16530 -21450 500]
ElementLine[12590 -25390 -12590 -25390 500]
ElementLine[-16530 -21450 -16530 -9640 500]
ElementLine[-16530 9640 -16530 21450 500]
ElementLine[-12590 25390 12590 25390 500]
ElementLine[-24380 9580 24480 9580 500]
ElementLine[24380 -9580 -24480 -9580 500]
ElementLine[-24480 9570 -24480 -9580 500]
ElementLine[24480 -9570 24480 9580 500]
ElementLine[-28340 0 -20850 0 500]
ElementLine[-24600 3740 -24600 -3740 500]
ElementLine[20850 0 28340 0 500]
ElementLine[24600 3740 24600 -3740 500]
ElementArc[-24600 0 2645 2645 0 360 500]
ElementArc[24600 0 2645 2645 0 360 500]
ElementArc[12590 21450 3940 3940 180 -90 500]
ElementArc[12590 -21450 3940 3940 270 -90 500]
ElementArc[-12590 -21450 3940 3940 0 -90 500]
ElementArc[-12590 21450 3940 3940 0 90 500]
Pin[-11000 17100 5110 2000 5910 2750 "" "1" ""]
Pin[0 17100 5110 2000 5910 2750 "" "2" ""]
Pin[11000 17100 5110 2000 5910 2750 "" "3" ""]
Pin[11000 -17100 5110 2000 5910 2750 "" "4" ""]
Pin[0 -17100 5110 2000 5910 2750 "" "5" ""]
Pin[-11000 -17100 5110 2000 5910 2750 "" "6" ""]
)
