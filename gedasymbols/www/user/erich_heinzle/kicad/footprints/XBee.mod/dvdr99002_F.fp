# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module dvdr99002_F
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: dvdr99002_F
# Text descriptor count: 1
# Draw segment object count: 57
# Draw circle object count: 9
# Draw arc object count: 0
# Pad count: 5
#
Element["" "dvdr99002" "" "" 0 0 0 70000 0 100 ""]
(
ElementLine[-30000 -82500 -27500 -80000 1500]
ElementLine[-27500 -80000 27500 -80000 1500]
ElementLine[27500 -80000 30000 -82500 1500]
ElementLine[-30000 -90000 -67500 -90000 1500]
ElementLine[-55000 -52500 -55000 60000 1500]
ElementLine[45000 -52500 45000 60000 1500]
ElementLine[-30000 -90000 -30000 -82500 1500]
ElementLine[30000 -82500 30000 -90000 1500]
ElementLine[-67500 -90000 -67500 115000 1500]
ElementLine[67500 115000 67500 -90000 1500]
ElementLine[15000 80000 15000 85000 1500]
ElementLine[-15000 80000 -15000 85000 1500]
ElementLine[-67500 115000 20000 115000 1500]
ElementLine[20000 115000 67500 115000 1500]
ElementLine[-30000 -90000 67500 -90000 1500]
ElementLine[-65000 20000 -65000 5000 1500]
ElementLine[-57500 20000 -65000 20000 1500]
ElementLine[-57500 5000 -57500 20000 1500]
ElementLine[-65000 5000 -57500 5000 1500]
ElementLine[-65000 30000 -57500 30000 1500]
ElementLine[-57500 30000 -57500 45000 1500]
ElementLine[-57500 45000 -65000 45000 1500]
ElementLine[-65000 45000 -65000 30000 1500]
ElementLine[-65000 -7500 -65000 -22500 1500]
ElementLine[-57500 -7500 -65000 -7500 1500]
ElementLine[-57500 -22500 -57500 -7500 1500]
ElementLine[-65000 -22500 -57500 -22500 1500]
ElementLine[-65000 -47500 -57500 -47500 1500]
ElementLine[-57500 -47500 -57500 -32500 1500]
ElementLine[-57500 -32500 -65000 -32500 1500]
ElementLine[-65000 -32500 -65000 -47500 1500]
ElementLine[45000 -52500 20000 -77500 1500]
ElementLine[45000 60000 -55000 60000 1500]
ElementLine[-55000 -52500 -30000 -77500 1500]
ElementLine[-30000 -77500 20000 -77500 1500]
ElementLine[-15000 5000 10000 5000 1500]
ElementLine[10000 5000 15000 5000 1500]
ElementLine[15000 5000 15000 50000 1500]
ElementLine[15000 50000 -15000 50000 1500]
ElementLine[-15000 50000 -15000 5000 1500]
ElementLine[-15000 105000 -15000 115000 1500]
ElementLine[-15000 105000 -15000 85000 1500]
ElementLine[-15000 80000 15000 80000 1500]
ElementLine[15000 85000 15000 115000 1500]
ElementLine[-30000 -90000 -30000 -100000 1500]
ElementLine[-30000 -100000 -30000 -130000 1500]
ElementLine[-30000 -130000 30000 -130000 1500]
ElementLine[30000 -130000 30000 -90000 1500]
ElementLine[30000 -90000 -30000 -90000 1500]
ElementLine[40000 -50000 40000 40000 1500]
ElementLine[30000 -50000 40000 -50000 1500]
ElementLine[30000 40000 30000 -50000 1500]
ElementLine[40000 40000 30000 40000 1500]
ElementLine[-40000 40000 -50000 40000 1500]
ElementLine[-50000 40000 -50000 -50000 1500]
ElementLine[-50000 -50000 -40000 -50000 1500]
ElementLine[-40000 -50000 -40000 40000 1500]
ElementArc[10000 -82500 2500 2500 0 360 1500]
ElementArc[-10000 -82500 2500 2500 0 360 1500]
ElementArc[20000 -82500 2500 2500 0 360 1500]
ElementArc[0 -82500 2500 2500 0 360 1500]
ElementArc[-20000 -82500 2500 2500 0 360 1500]
ElementArc[-57500 -80000 5000 5000 0 360 1500]
ElementArc[57500 -80000 5000 5000 0 360 1500]
ElementArc[57500 105000 5000 5000 0 360 1500]
ElementArc[-57500 105000 5000 5000 0 360 1500]
Pin[-20000 -82500 8820 2000 9620 3200 "" "F1" "square"]
Pin[-10000 -82500 8820 2000 9620 3200 "" "F2" ""]
Pin[0 -82500 8820 2000 9620 3200 "" "F3" ""]
Pin[10000 -82500 8820 2000 9620 3200 "" "F4" ""]
Pin[20000 -82500 8820 2000 9620 3200 "" "F5" ""]
)
