# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module XBee_patch
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: XBee_patch
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 20
#
Element["" "XBee_patch" "" "" 0 0 -25000 30000 1 100 ""]
(
ElementLine[-12500 -7500 7500 -17500 1500]
ElementLine[7500 -17500 7500 2500 1500]
ElementLine[7500 2500 -12500 -7500 1500]
ElementLine[15000 7500 15000 5000 1500]
ElementLine[-15000 7500 -15000 5000 1500]
ElementLine[-15000 -22500 15000 -22500 1500]
ElementLine[15000 -22500 15000 2500 1500]
ElementLine[15000 2500 15000 5000 1500]
ElementLine[15000 7500 -15000 7500 1500]
ElementLine[-15000 5000 -15000 -22500 1500]
ElementLine[-49000 77000 -49000 81000 1500]
ElementLine[49000 77000 49000 81000 1500]
ElementLine[-49000 77000 -49000 -1000 1500]
ElementLine[49000 0 49000 -1000 1500]
ElementLine[49000 77000 49000 0 1500]
ElementLine[-49000 81000 49000 81000 1500]
ElementLine[49000 -1000 20000 -26000 1500]
ElementLine[-49000 -1000 -20000 -26000 1500]
ElementLine[-20000 -26000 20000 -26000 1500]
ElementArc[0 -7500 12500 12500 0 360 1500]
ElementArc[27000 0 3162 3162 0 360 1500]
ElementArc[27000 0 5831 5831 0 360 1500]
Pin[43310 0 7320 2000 8120 3200 "" "20" ""]
Pin[43310 31500 7320 2000 8120 3200 "" "16" ""]
Pin[43310 47240 7320 2000 8120 3200 "" "14" ""]
Pin[43310 39370 7320 2000 8120 3200 "" "15" ""]
Pin[43310 15750 7320 2000 8120 3200 "" "18" ""]
Pin[43310 23620 7320 2000 8120 3200 "" "17" ""]
Pin[43310 7870 7320 2000 8120 3200 "" "19" ""]
Pin[43310 62990 7320 2000 8120 3200 "" "12" ""]
Pin[43310 70870 7320 2000 8120 3200 "" "11" ""]
Pin[43310 55120 7320 2000 8120 3200 "" "13" ""]
Pin[-43310 55120 7320 2000 8120 3200 "" "8" ""]
Pin[-43310 70870 7320 2000 8120 3200 "" "10" ""]
Pin[-43310 62990 7320 2000 8120 3200 "" "9" ""]
Pin[-43310 7870 7320 2000 8120 3200 "" "2" ""]
Pin[-43310 23620 7320 2000 8120 3200 "" "4" ""]
Pin[-43310 15750 7320 2000 8120 3200 "" "3" ""]
Pin[-43310 39370 7320 2000 8120 3200 "" "6" ""]
Pin[-43310 47240 7320 2000 8120 3200 "" "7" ""]
Pin[-43310 31500 7320 2000 8120 3200 "" "5" ""]
Pin[-43310 0 7320 2000 8120 3200 "" "1" ""]
)
