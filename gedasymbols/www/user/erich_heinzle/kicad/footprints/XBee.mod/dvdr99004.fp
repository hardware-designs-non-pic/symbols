# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Yvon Tollens
# No warranties express or implied
# Footprint converted from Kicad Module dvdr99004
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: dvdr99004
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 5
# Draw arc object count: 0
# Pad count: 18
#
Element["" "dvdr99004" "" "" 0 0 0 60000 0 100 ""]
(
ElementLine[-25000 -70000 25000 -70000 1500]
ElementLine[-37500 -50000 -25000 -50000 1500]
ElementLine[37500 -50000 25000 -50000 1500]
ElementLine[-25000 -40000 25000 -40000 1500]
ElementLine[-15000 10000 15000 10000 1500]
ElementLine[-15000 50000 -15000 15000 1500]
ElementLine[-15000 15000 15000 15000 1500]
ElementLine[15000 15000 15000 50000 1500]
ElementLine[37500 50000 -37500 50000 1500]
ElementLine[25000 -40000 25000 -70000 1500]
ElementLine[-25000 -70000 -25000 -40000 1500]
ElementLine[37500 -50000 37500 50000 1500]
ElementLine[-37500 50000 -37500 -50000 1500]
ElementLine[-15000 -35000 10000 -35000 1500]
ElementLine[10000 -35000 15000 -35000 1500]
ElementLine[15000 -35000 15000 10000 1500]
ElementLine[-15000 10000 -15000 -35000 1500]
ElementArc[20000 -40000 2500 2500 0 360 1500]
ElementArc[10000 -40000 2500 2500 0 360 1500]
ElementArc[0 -40000 2500 2500 0 360 1500]
ElementArc[-10000 -40000 2500 2500 0 360 1500]
ElementArc[-20000 -40000 2500 2500 0 360 1500]
Pin[-30000 30000 8820 2000 9620 3200 "" "17" ""]
Pin[-30000 40000 8820 2000 9620 3200 "" "18" ""]
Pin[-30000 20000 8820 2000 9620 3200 "" "16" ""]
Pin[-30000 10000 8820 2000 9620 3200 "" "15" ""]
Pin[-30000 0 8820 2000 9620 3200 "" "14" ""]
Pin[30000 40000 8820 2000 9620 3200 "" "1" "square"]
Pin[30000 30000 8820 2000 9620 3200 "" "2" ""]
Pin[30000 20000 8820 2000 9620 3200 "" "3" ""]
Pin[30000 10000 8820 2000 9620 3200 "" "4" ""]
Pin[30000 0 8820 2000 9620 3200 "" "5" ""]
Pin[30000 -10000 8820 2000 9620 3200 "" "6" ""]
Pin[30000 -20000 8820 2000 9620 3200 "" "7" ""]
Pin[30000 -30000 8820 2000 9620 3200 "" "8" ""]
Pin[30000 -40000 8820 2000 9620 3200 "" "9" ""]
Pin[-30000 -40000 8820 2000 9620 3200 "" "10" ""]
Pin[-30000 -30000 8820 2000 9620 3200 "" "11" ""]
Pin[-30000 -20000 8820 2000 9620 3200 "" "12" ""]
Pin[-30000 -10000 8820 2000 9620 3200 "" "13" ""]
)
