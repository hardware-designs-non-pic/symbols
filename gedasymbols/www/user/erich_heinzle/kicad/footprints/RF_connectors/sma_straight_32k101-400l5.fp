# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sma_straight_32k101-400l5
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sma_straight_32k101-400l5
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 3
# Draw arc object count: 0
# Pad count: 5
#
Element["" "sma_straight_32k101-400l5" "" "" 0 0 0 18897 0 100 ""]
(
ElementLine[-12598 -12598 -12598 12598 1500]
ElementLine[-12598 -12598 12598 -12598 1500]
ElementLine[12598 -12598 12598 12598 1500]
ElementLine[12598 12598 -12598 12598 1500]
ElementArc[0 0 2756 2756 0 360 1500]
ElementArc[0 0 10236 10236 0 360 1500]
ElementArc[0 0 12205 12205 0 360 1500]
Pin[0 0 9055 2000 9855 5314 "" "1" ""]
Pin[10000 -10000 9842 2000 10642 5708 "" "2" ""]
Pin[-10000 -10000 9842 2000 10642 5708 "" "2" ""]
Pin[-10000 10000 9842 2000 10642 5708 "" "2" ""]
Pin[10000 10000 9842 2000 10642 5708 "" "2" ""]
)
