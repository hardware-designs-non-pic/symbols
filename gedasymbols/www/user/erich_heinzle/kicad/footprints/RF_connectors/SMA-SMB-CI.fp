# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Francois Lambert
# No warranties express or implied
# Footprint converted from Kicad Module SMA-SMB-CI
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SMA-SMB-CI
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "J***" "" "" 0 0 0 -35000 0 100 ""]
(
ElementLine[-17500 -17500 17500 -17500 1500]
ElementLine[17500 -17500 17500 17500 1500]
ElementLine[17500 17500 -17500 17500 1500]
ElementLine[-17500 17500 -17500 -17500 1500]
Pin[-10000 -10000 11810 2000 12610 6300 "" "2" ""]
Pin[0 0 10000 2000 10800 3940 "" "1" ""]
Pin[10000 -10000 11810 2000 12610 6300 "" "2" ""]
Pin[-10000 10000 11810 2000 12610 6300 "" "2" ""]
Pin[10000 10000 11810 2000 12610 6300 "" "2" ""]
)
