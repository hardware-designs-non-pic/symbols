# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module lemo_epb00250ntn
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: lemo_epb00250ntn
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 5
#
Element["" "lemo_epb00250ntn" "" "" 0 0 0 20080 0 100 ""]
(
ElementLine[-14170 -14170 14170 -14170 1500]
ElementLine[14170 -14170 14170 14170 1500]
ElementLine[14170 14170 -14170 14170 1500]
ElementLine[-14170 14170 -14170 -14170 1500]
ElementArc[0 0 5120 5120 0 360 1500]
ElementArc[0 0 13780 13780 0 360 1500]
Pin[0 0 7080 2000 7880 3540 "" "1" ""]
Pin[10000 -10000 7070 2000 7870 3540 "" "2" ""]
Pin[-10000 -10000 7070 2000 7870 3540 "" "2" ""]
Pin[-10000 10000 7070 2000 7870 3540 "" "2" ""]
Pin[10000 10000 7070 2000 7870 3540 "" "2" ""]
)
