# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module coaxial_u.fl-r-smt-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: coaxial_u.fl-r-smt-1
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 2
# Draw arc object count: 4
# Pad count: 3
#
Element["" "coaxial_u.fl-r-smt-1" "" "" 0 0 0 -8267 0 100 ""]
(
ElementLine[-5118 3937 -5118 -3937 590]
ElementLine[3937 5118 -3937 5118 590]
ElementLine[5118 -3937 5118 3937 590]
ElementLine[-3937 -5118 3937 -5118 590]
ElementArc[0 0 394 394 0 360 590]
ElementArc[0 0 3937 3937 0 360 590]
ElementArc[-3937 3937 1181 1181 90 -90 590]
ElementArc[3937 3937 1181 1181 180 -90 590]
ElementArc[3937 -3937 1181 1181 270 -90 590]
ElementArc[-3937 -3937 1181 1181 0 -90 590]
Pad[-5807 -2263 -5807 2263 4133 2000 4933 "" "2" "square"]
Pad[5807 -2263 5807 2263 4133 2000 4933 "" "2" "square"]
Pad[0 5905 0 6102 3937 2000 4737 "" "1" "square"]
)
