# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module sma_90_r300.124.403
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: sma_90_r300.124.403
# Text descriptor count: 1
# Draw segment object count: 23
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "sma_90_r300.124.403" "" "" 0 0 0 35039 0 100 ""]
(
ElementLine[10629 29527 10629 27165 1500]
ElementLine[-10629 29527 -10629 27165 1500]
ElementLine[-10629 8267 -10629 -3543 1500]
ElementLine[10629 -3543 10629 8267 1500]
ElementLine[-12598 26771 -10629 27165 1500]
ElementLine[-10629 27165 10629 27165 1500]
ElementLine[10629 27165 12598 26771 1500]
ElementLine[-10629 8267 10629 8267 1500]
ElementLine[10629 8267 12598 8661 1500]
ElementLine[-12598 8661 -10629 8267 1500]
ElementLine[-12598 11417 12598 8661 1500]
ElementLine[-12598 17322 12598 14566 1500]
ElementLine[-12598 14173 12598 11417 1500]
ElementLine[-12598 20472 12598 17716 1500]
ElementLine[-12598 23622 12598 20866 1500]
ElementLine[-12598 8661 -12598 26771 1500]
ElementLine[-12598 26771 12598 24015 1500]
ElementLine[12598 8661 12598 26771 1500]
ElementLine[-10629 29527 10629 29527 1500]
ElementLine[-12992 -3543 -12992 -29527 1500]
ElementLine[-12992 -29527 12992 -29534 1500]
ElementLine[12992 -29527 12992 -3543 1500]
ElementLine[12992 -3543 -12992 -3543 1500]
Pin[0 -16700 9055 2000 9855 5314 "" "1" ""]
Pin[10000 -26700 9055 2000 9855 5314 "" "2" ""]
Pin[-10000 -26700 9055 2000 9855 5314 "" "2" ""]
Pin[-10000 -6700 9055 2000 9855 5314 "" "2" ""]
Pin[10000 -6700 9055 2000 9855 5314 "" "2" ""]
)
