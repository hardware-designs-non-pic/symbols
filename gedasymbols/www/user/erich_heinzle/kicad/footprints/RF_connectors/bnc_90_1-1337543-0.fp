# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module bnc_90_1-1337543-0
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: bnc_90_1-1337543-0
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "bnc_90_1-1337543-0" "" "" 0 0 0 -58661 0 100 ""]
(
ElementLine[-25196 4724 25196 1968 1181]
ElementLine[-25196 20472 25196 17716 1181]
ElementLine[-25196 16535 25196 13779 1181]
ElementLine[-25196 8661 25196 5905 1181]
ElementLine[-25196 12598 25196 9842 1181]
ElementLine[-25196 28346 25196 25590 1181]
ElementLine[-25196 24409 25196 21653 1181]
ElementLine[-25196 32283 25196 29527 1181]
ElementLine[-25196 36220 25196 33464 1181]
ElementLine[25196 36220 25196 0 1181]
ElementLine[-25196 36220 -25196 0 1181]
ElementLine[18897 83464 18897 36220 1181]
ElementLine[-18897 83464 -18897 36220 1181]
ElementLine[-18897 83464 18897 83464 1181]
ElementLine[-25196 36220 25196 36220 1181]
ElementLine[29133 0 29133 -53937 1181]
ElementLine[-29133 -53937 -29133 0 1181]
ElementLine[-29133 -53937 29133 -53937 1181]
ElementLine[-29133 0 29133 0 1181]
Pin[0 -48740 5905 2000 6705 3543 "" "1" ""]
Pin[10000 -48740 5905 2000 6705 3543 "" "2" ""]
Pin[20000 -28740 8267 2000 9067 8267 "" "" "hole"]
Pin[-20000 -28740 8267 2000 9067 8267 "" "" "hole"]
)
