# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Frans Schreduer
# No warranties express or implied
# Footprint converted from Kicad Module SMA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: SMA
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" "SMA" "" "" 0 0 0 20000 0 100 ""]
(
ElementLine[10470 -45890 10470 -49610 1500]
ElementLine[-10470 -45890 -10470 -49610 1500]
ElementLine[-10470 -12400 -10470 -27280 1500]
ElementLine[10470 -12400 10470 -27280 1500]
ElementLine[-12570 -44650 1050 -45890 1500]
ElementLine[12570 -29760 -10470 -27280 1500]
ElementLine[-12570 -29760 12570 -32240 1500]
ElementLine[-12570 -32240 12570 -34720 1500]
ElementLine[-12570 -34720 12570 -37200 1500]
ElementLine[-12570 -37200 12570 -39690 1500]
ElementLine[-12570 -39690 12570 -42170 1500]
ElementLine[-12570 -42170 12570 -44650 1500]
ElementLine[-12570 -44650 -10470 -45890 1500]
ElementLine[-10470 -45890 10470 -45890 1500]
ElementLine[10470 -45890 12570 -44650 1500]
ElementLine[12570 -44650 12570 -28520 1500]
ElementLine[12570 -28520 10470 -27280 1500]
ElementLine[10470 -27280 -10470 -27280 1500]
ElementLine[-10470 -27280 -12570 -28520 1500]
ElementLine[-12570 -28520 -12570 -44650 1500]
ElementLine[10470 -49610 -10470 -49610 1500]
ElementLine[-12400 -12400 12400 -12400 1500]
ElementLine[12400 -12400 12400 12400 1500]
ElementLine[12400 12400 -12400 12400 1500]
ElementLine[-12400 12400 -12400 -12400 1500]
Pin[0 0 7870 2000 8670 3940 "" "1" ""]
Pin[10630 -10630 7870 2000 8670 3940 "" "3" ""]
Pin[-10630 -10630 7870 2000 8670 3940 "" "2" ""]
Pin[-10630 10630 7870 2000 8670 3940 "" "5" ""]
Pin[10630 10630 7870 2000 8670 3940 "" "4" ""]
)
