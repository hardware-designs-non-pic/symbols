# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module bnc_straight_1-1337541-0
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: mm
# Footprint = module name: bnc_straight_1-1337541-0
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 4
# Draw arc object count: 6
# Pad count: 4
#
Element["" "bnc_straight_1-1337541-0" "" "" 0 0 0 -33858 0 100 ""]
(
ElementLine[-3937 21653 -3937 18503 1181]
ElementLine[-3937 21653 3937 21653 1181]
ElementLine[3937 21653 3937 18503 1181]
ElementLine[3937 -21653 3937 -18503 1181]
ElementLine[-3937 -21653 -3937 -18503 1181]
ElementLine[-12598 -21653 12598 -21653 1181]
ElementLine[21259 -29527 -21259 -29527 1181]
ElementLine[29133 29527 29133 -21653 1181]
ElementLine[-29133 -21653 -29133 29527 1181]
ElementLine[-29133 29527 29133 29527 1181]
ElementArc[0 0 3543 3543 0 360 1181]
ElementArc[0 0 9055 9055 0 360 1181]
ElementArc[0 0 18110 18110 0 360 1181]
ElementArc[0 0 18898 18898 0 360 1181]
ElementArc[0 0 25205 25205 0 360 1181]
ElementArc[21259 -21653 7874 7874 270 -90 1181]
ElementArc[-21259 -21653 7874 7874 0 -90 1181]
Pin[0 0 5905 2000 6705 3543 "" "1" ""]
Pin[10000 0 5905 2000 6705 3543 "" "2" ""]
Pin[20000 -20000 8267 2000 9067 8267 "" "" "hole"]
Pin[-20000 20000 8267 2000 9067 8267 "" "" "hole"]
)
