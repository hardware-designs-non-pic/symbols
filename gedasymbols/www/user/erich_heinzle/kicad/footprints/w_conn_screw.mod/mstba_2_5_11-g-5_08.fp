# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mstba_2_5_11-g-5_08
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mstba_2_5_11-g-5_08
# Text descriptor count: 1
# Draw segment object count: 5
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 11
#
Element["" "2 5 11-G-5 08 " "" "" 0 0 0 14760 0 100 ""]
(
ElementLine[-113780 -36220 113780 -36220 1000]
ElementLine[-113780 7870 -113780 -39370 1000]
ElementLine[-113780 -39370 113780 -39370 1000]
ElementLine[113780 -39370 113780 7870 1000]
ElementLine[113780 7870 -113780 7870 1000]
Pin[-100000 0 11810 2000 12610 5510 "0" "_1_" "square"]
Pin[-80000 0 11810 2000 12610 5510 "0" "_2_" ""]
Pin[-40000 0 11810 2000 12610 5510 "0" "_4_" ""]
Pin[-60000 0 11810 2000 12610 5510 "0" "_3_" ""]
Pin[-20000 0 11810 2000 12610 5510 "0" "_5_" ""]
Pin[0 0 11810 2000 12610 5510 "0" "_6_" ""]
Pin[20000 0 11810 2000 12610 5510 "0" "_7_" ""]
Pin[40000 0 11810 2000 12610 5510 "0" "_8_" ""]
Pin[60000 0 11810 2000 12610 5510 "0" "_9_" ""]
Pin[80000 0 11810 2000 12610 5510 "0" "_10_" ""]
Pin[100000 0 11810 2000 12610 5510 "0" "_11_" ""]
)
