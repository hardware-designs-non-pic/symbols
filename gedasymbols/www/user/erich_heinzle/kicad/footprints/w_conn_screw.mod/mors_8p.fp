# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mors_8p
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mors_8p
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" "N MORS 8P " "" "" 0 0 0 -23000 0 100 ""]
(
ElementLine[80000 18000 78000 18000 1000]
ElementLine[78000 14000 80000 14000 1000]
ElementLine[78000 -18000 80000 -18000 1000]
ElementLine[-80000 -18000 78000 -18000 1000]
ElementLine[78000 18000 -80000 18000 1000]
ElementLine[-80000 14000 78000 14000 1000]
ElementLine[80000 -18000 80000 18000 1000]
ElementLine[-80000 18000 -80000 14000 1000]
ElementLine[-80000 -18000 -80000 -15000 1000]
ElementLine[-80000 15000 -80000 -15000 1000]
Pin[-70000 0 11810 2000 12610 4920 "0" "_1_" "square"]
Pin[-50000 0 11810 2000 12610 4920 "0" "_2_" ""]
Pin[-30000 0 11810 2000 12610 4920 "0" "_3_" ""]
Pin[-10000 0 11810 2000 12610 4920 "0" "_4_" ""]
Pin[10000 0 11810 2000 12610 4920 "0" "_5_" ""]
Pin[30000 0 11810 2000 12610 4920 "0" "_6_" ""]
Pin[50000 0 11810 2000 12610 4920 "0" "_7_" ""]
Pin[70000 0 11810 2000 12610 4920 "0" "_8_" ""]
)
