# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mstbva_2_5-7-g-5_08
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mstbva_2_5-7-g-5_08
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 7
# Pad count: 7
#
Element["" "2 5 7-G-5 08 " "" "" 0 0 0 -23000 0 100 ""]
(
ElementLine[74000 20000 -74000 20000 1200]
ElementLine[-74000 16000 74000 16000 1200]
ElementLine[74000 -15000 -74000 -15000 1200]
ElementLine[-74000 -15000 -74000 20000 1200]
ElementLine[74000 20000 74000 -15000 1200]
ElementArc[-60000 0 14142 14142 -45 -90 1200]
ElementArc[-40000 0 14142 14142 -45 -90 1200]
ElementLine[70000 -10000 70000 16000 1200]
ElementLine[-70000 -10000 -70000 16000 1200]
ElementArc[-20000 0 14142 14142 -45 -90 1200]
ElementArc[0 0 14142 14142 -45 -90 1200]
ElementArc[20000 0 14142 14142 -45 -90 1200]
ElementArc[40000 0 14142 14142 -45 -90 1200]
ElementArc[60000 0 14142 14142 -45 -90 1200]
Pin[-60000 0 11810 2000 12610 5510 "0" "_1_" "square"]
Pin[0 0 11810 2000 12610 5510 "0" "_4_" ""]
Pin[20000 0 11810 2000 12610 5510 "0" "_5_" ""]
Pin[40000 0 11810 2000 12610 5510 "0" "_6_" ""]
Pin[60000 0 11810 2000 12610 5510 "0" "_7_" ""]
Pin[-20000 0 11810 2000 12610 5510 "0" "_3_" ""]
Pin[-40000 0 11810 2000 12610 5510 "0" "_2_" ""]
)
