# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mors_3p
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mors_3p
# Text descriptor count: 1
# Draw segment object count: 9
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" "N MORS 3P " "" "" 0 0 0 -23000 0 100 ""]
(
ElementLine[-30000 18000 -30000 14000 1000]
ElementLine[-30000 14000 30000 14000 1000]
ElementLine[30000 14000 30000 18000 1000]
ElementLine[30000 18000 -30000 18000 1000]
ElementLine[-30000 -18000 -30000 -15000 1000]
ElementLine[-30000 -18000 30000 -18000 1000]
ElementLine[30000 -18000 30000 -15000 1000]
ElementLine[-30000 15000 -30000 -15000 1000]
ElementLine[30000 15000 30000 -15000 1000]
Pin[-20000 0 11810 2000 12610 4920 "0" "_1_" "square"]
Pin[0 0 11810 2000 12610 4920 "0" "_2_" ""]
Pin[20000 0 11810 2000 12610 4920 "0" "_3_" ""]
)
