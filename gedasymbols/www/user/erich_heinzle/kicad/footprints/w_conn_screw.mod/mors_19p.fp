# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mors_19p
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mors_19p
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 19
#
Element["" "N MORS 19P " "" "" 0 0 0 -23000 0 100 ""]
(
ElementLine[190000 -18000 -189000 -18000 1000]
ElementLine[-189000 18000 190000 18000 1000]
ElementLine[190000 14000 -189000 14000 1000]
ElementLine[190000 -18000 190000 18000 1000]
ElementLine[-189000 18000 -189000 14000 1000]
ElementLine[-189000 -18000 -189000 -15000 1000]
ElementLine[-189000 15000 -189000 -15000 1000]
Pin[-180000 0 11810 2000 12610 4920 "0" "_1_" "square"]
Pin[-160000 0 11810 2000 12610 4920 "0" "_2_" ""]
Pin[-140000 0 11810 2000 12610 4920 "0" "_3_" ""]
Pin[-120000 0 11810 2000 12610 4920 "0" "_4_" ""]
Pin[-100000 0 11810 2000 12610 4920 "0" "_5_" ""]
Pin[-80000 0 11810 2000 12610 4920 "0" "_6_" ""]
Pin[-60000 0 11810 2000 12610 4920 "0" "_7_" ""]
Pin[-40000 0 11810 2000 12610 4920 "0" "_8_" ""]
Pin[-20000 0 11810 2000 12610 4920 "0" "_9_" ""]
Pin[0 0 11810 2000 12610 4920 "0" "_10_" ""]
Pin[20000 0 11810 2000 12610 4920 "0" "_11_" ""]
Pin[40000 0 11810 2000 12610 4920 "0" "_12_" ""]
Pin[60000 0 11810 2000 12610 4920 "0" "_13_" ""]
Pin[80000 0 11810 2000 12610 4920 "0" "_14_" ""]
Pin[100000 0 11810 2000 12610 4920 "0" "_15_" ""]
Pin[120000 0 11810 2000 12610 4920 "0" "_16_" ""]
Pin[140000 0 11810 2000 12610 4920 "0" "_17_" ""]
Pin[160000 0 11810 2000 12610 4920 "0" "_18_" ""]
Pin[180000 0 11810 2000 12610 4920 "0" "_19_" ""]
)
