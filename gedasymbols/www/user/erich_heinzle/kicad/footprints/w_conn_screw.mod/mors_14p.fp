# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module mors_14p
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: mors_14p
# Text descriptor count: 1
# Draw segment object count: 7
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 14
#
Element["" "N MORS 14P " "" "" 0 0 0 -23000 0 100 ""]
(
ElementLine[140000 -18000 -139000 -18000 1000]
ElementLine[-139000 18000 140000 18000 1000]
ElementLine[140000 14000 -139000 14000 1000]
ElementLine[140000 -18000 140000 18000 1000]
ElementLine[-139000 18000 -139000 14000 1000]
ElementLine[-139000 -18000 -139000 -15000 1000]
ElementLine[-139000 15000 -139000 -15000 1000]
Pin[-130000 0 11810 2000 12610 4920 "0" "_1_" "square"]
Pin[-110000 0 11810 2000 12610 4920 "0" "_2_" ""]
Pin[-90000 0 11810 2000 12610 4920 "0" "_3_" ""]
Pin[-70000 0 11810 2000 12610 4920 "0" "_4_" ""]
Pin[-50000 0 11810 2000 12610 4920 "0" "_5_" ""]
Pin[-30000 0 11810 2000 12610 4920 "0" "_6_" ""]
Pin[-10000 0 11810 2000 12610 4920 "0" "_7_" ""]
Pin[10000 0 11810 2000 12610 4920 "0" "_8_" ""]
Pin[30000 0 11810 2000 12610 4920 "0" "_9_" ""]
Pin[50000 0 11810 2000 12610 4920 "0" "_10_" ""]
Pin[70000 0 11810 2000 12610 4920 "0" "_11_" ""]
Pin[90000 0 11810 2000 12610 4920 "0" "_12_" ""]
Pin[110000 0 11810 2000 12610 4920 "0" "_13_" ""]
Pin[130000 0 11810 2000 12610 4920 "0" "_14_" ""]
)
