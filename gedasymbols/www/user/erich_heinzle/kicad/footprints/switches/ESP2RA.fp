# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module ESP2RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ESP2RA
# Text descriptor count: 1
# Draw segment object count: 84
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -22170 -7810 1 100 ""]
(
ElementLine[-16610 -21370 -11490 -21370 1000]
ElementLine[-11490 -21370 -8740 -21370 1000]
ElementLine[-8740 -21370 -1650 -21370 1000]
ElementLine[-1650 -21370 1490 -21370 1000]
ElementLine[1490 -21370 8580 -21370 1000]
ElementLine[8580 -21370 11330 -21370 1000]
ElementLine[11330 -21370 16450 -21370 1000]
ElementLine[16450 -21370 16450 -6810 1000]
ElementLine[16450 -6810 15660 -6810 1000]
ElementLine[15660 -6810 15660 8140 1000]
ElementLine[15660 8140 9370 8140 1000]
ElementLine[9370 8140 3460 8140 1000]
ElementLine[3460 8140 -2830 8140 1000]
ElementLine[-2830 8140 -9520 8140 1000]
ElementLine[-9520 8140 -15820 8140 1000]
ElementLine[-15820 8140 -15820 -6810 1000]
ElementLine[-15820 -6810 -16610 -6810 1000]
ElementLine[-16610 -6810 -16610 -21370 1000]
ElementLine[9370 8140 10150 8140 500]
ElementLine[10150 8140 10150 16810 500]
ElementLine[10150 16810 10150 17200 500]
ElementLine[10150 17200 9370 17990 500]
ElementLine[-9520 8140 -9520 16810 500]
ElementLine[-9520 16810 -9130 17200 500]
ElementLine[-9130 17200 -8740 17990 500]
ElementLine[-8740 17990 9370 17990 500]
ElementLine[-9520 16810 -2830 16810 500]
ElementLine[-2830 16810 3460 16810 500]
ElementLine[3460 16810 10150 16810 500]
ElementLine[-2830 8140 -2830 16810 500]
ElementLine[3460 8140 3460 16810 500]
ElementLine[-15820 -6810 15660 -6810 1000]
ElementLine[-11490 -35940 -11100 -35940 500]
ElementLine[-11100 -35940 -8740 -35940 500]
ElementLine[-8740 -35940 -8740 -21370 500]
ElementLine[-11490 -35940 -11490 -21370 500]
ElementLine[-1650 -35820 -1250 -35820 500]
ElementLine[-1250 -35820 1490 -35820 500]
ElementLine[1490 -35820 1490 -21370 500]
ElementLine[-1650 -35820 -1650 -21370 500]
ElementLine[8580 -35940 11330 -35940 500]
ElementLine[11330 -35940 11330 -21370 500]
ElementLine[8580 -35940 8580 -21370 500]
ElementLine[-11100 -35940 -11100 -21770 500]
ElementLine[-10700 -21770 -10700 -35550 500]
ElementLine[-10700 -35550 -9130 -35550 500]
ElementLine[-9130 -35550 -9130 -21770 500]
ElementLine[-9130 -21770 -10310 -21770 500]
ElementLine[-10310 -21770 -10310 -35150 500]
ElementLine[-10310 -35150 -9520 -35150 500]
ElementLine[-9520 -35150 -9520 -22160 500]
ElementLine[-9520 -22160 -9920 -22160 500]
ElementLine[-9920 -22160 -9920 -34760 500]
ElementLine[-1250 -35820 -1250 -21770 500]
ElementLine[-1250 -21770 1100 -21770 500]
ElementLine[1100 -21770 1100 -35430 500]
ElementLine[1100 -35430 -860 -35430 500]
ElementLine[-860 -35430 -860 -22160 500]
ElementLine[-860 -22160 700 -22160 500]
ElementLine[700 -22160 700 -35030 500]
ElementLine[700 -35030 -470 -35030 500]
ElementLine[-470 -35030 -470 -22550 500]
ElementLine[-470 -22550 -70 -22550 500]
ElementLine[-70 -22550 310 -22550 500]
ElementLine[310 -22550 310 -34640 500]
ElementLine[310 -34640 -70 -34640 500]
ElementLine[-70 -34640 -70 -22550 500]
ElementLine[8970 -35550 8970 -21770 500]
ElementLine[8970 -21770 9370 -21770 500]
ElementLine[9370 -21770 10940 -21770 500]
ElementLine[10940 -21770 10940 -35550 500]
ElementLine[10940 -35550 9370 -35550 500]
ElementLine[9370 -35550 9370 -21770 500]
ElementLine[9370 -21770 9760 -22160 500]
ElementLine[9760 -22160 10550 -22160 500]
ElementLine[10550 -22160 10550 -35150 500]
ElementLine[10550 -35150 10150 -35150 500]
ElementLine[10150 -35150 9760 -35150 500]
ElementLine[9760 -35150 9760 -22550 500]
ElementLine[9760 -22550 10150 -22550 500]
ElementLine[10150 -22550 10150 -35150 500]
ElementLine[-4720 18110 -4720 31100 500]
ElementLine[-4720 31100 5510 31100 500]
ElementLine[5510 31100 5510 18110 500]
Pin[-10000 -35000 6600 2000 7400 4000 "" "P$1" ""]
Pin[0 -25000 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -35000 6600 2000 7400 4000 "" "P$3" ""]
Pin[10000 -25000 6600 2000 7400 4000 "" "P$4" ""]
Pin[0 -35000 6600 2000 7400 4000 "" "P$5" ""]
Pin[-10000 -25000 6600 2000 7400 4000 "" "P$6" ""]
)
