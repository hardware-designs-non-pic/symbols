# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module MSP113PC
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: MSP113PC
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 4
# Draw arc object count: 0
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 1470 -23360 0 100 ""]
(
ElementLine[-5110 -12590 5900 -12590 1000]
ElementLine[5900 -12590 12200 -6290 1000]
ElementLine[12200 -6290 12200 5900 1000]
ElementLine[12200 5900 6290 11810 1000]
ElementLine[6290 11810 -6690 11810 1000]
ElementLine[-6690 11810 -12200 6290 1000]
ElementLine[-12200 6290 -12200 -5900 1000]
ElementLine[-12200 -5900 -5510 -12590 1000]
ElementArc[0 0 13916 13916 0 360 500]
ElementArc[0 0 12827 12827 0 360 250]
ElementArc[0 0 5558 5558 0 360 250]
ElementArc[0 0 6958 6958 0 360 500]
Pin[0 10000 10000 2000 10800 6600 "" "P$1" ""]
Pin[0 -10000 10000 2000 10800 6600 "" "P$2" ""]
Pin[-10000 0 10000 2000 10800 6600 "" "P$3" ""]
Pin[10000 0 10000 2000 10800 6600 "" "P$4" ""]
)
