# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-RF19
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-RF19
# Text descriptor count: 1
# Draw segment object count: 28
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 3
#
Element["" ">VALUE" "" "" 0 0 -10000 15000 0 100 ""]
(
ElementLine[-37500 37500 37500 37500 600]
ElementLine[37500 37500 37500 -37500 600]
ElementLine[37500 -37500 -37500 -37500 600]
ElementLine[-37500 -37500 -37500 37500 600]
ElementLine[-25000 30000 25000 30000 600]
ElementLine[30000 25000 30000 -25000 600]
ElementLine[25000 -30000 -25000 -30000 600]
ElementLine[-30000 -25000 -30000 25000 600]
ElementLine[-2500 32500 -2500 35000 600]
ElementLine[-2500 35000 2500 35000 600]
ElementLine[2500 35000 2500 32500 600]
ElementLine[2500 32500 25000 32500 600]
ElementLine[-2500 32500 -25000 32500 600]
ElementLine[32500 25000 32500 2500 600]
ElementLine[32500 2500 35000 2500 600]
ElementLine[35000 2500 35000 -2500 600]
ElementLine[35000 -2500 32500 -2500 600]
ElementLine[32500 -2500 32500 -25000 600]
ElementLine[25000 -32500 2500 -32500 600]
ElementLine[2500 -32500 2500 -35000 600]
ElementLine[-2500 -35000 -2500 -32500 600]
ElementLine[-2500 -32500 -25000 -32500 600]
ElementLine[-32500 -25000 -32500 -2500 600]
ElementLine[-32500 -2500 -35000 -2500 600]
ElementLine[-35000 -2500 -35000 2500 600]
ElementLine[-35000 2500 -32500 2500 600]
ElementLine[-32500 2500 -32500 25000 600]
ElementLine[-2500 -35000 2500 -35000 600]
ElementArc[25000 25000 5000 5000 0 360 600]
ElementArc[25000 -25000 5000 5000 0 360 600]
ElementArc[-25000 -25000 5000 5000 0 360 600]
ElementArc[-25000 25000 5000 5000 0 360 600]
ElementArc[25000 25000 7500 7500 0 360 600]
ElementArc[25000 -25000 7500 7500 0 360 600]
ElementArc[-25000 -25000 7500 7500 0 360 600]
ElementArc[-25000 25000 7500 7500 0 360 600]
Pin[0 0 6600 2000 7400 4400 "" "1" ""]
Pad[0 -3300 0 3300 6600 2000 7400 "" "1" ""]
Pad[0 -3300 0 3300 6600 2000 7400 "" "1" ",onsolder"]
Pin[-7500 -25000 6600 2000 7400 4400 "" "2" ""]
Pad[-7500 -28300 -7500 -21700 6600 2000 7400 "" "2" ""]
Pad[-7500 -28300 -7500 -21700 6600 2000 7400 "" "2" ",onsolder"]
Pin[7500 25000 6600 2000 7400 4400 "" "3" ""]
Pad[7500 21700 7500 28300 6600 2000 7400 "" "3" ""]
Pad[7500 21700 7500 28300 6600 2000 7400 "" "3" ",onsolder"]
)
