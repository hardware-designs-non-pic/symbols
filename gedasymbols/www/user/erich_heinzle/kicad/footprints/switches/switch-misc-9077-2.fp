# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-9077-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-9077-2
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 16
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -5000 -22500 0 100 ""]
(
ElementLine[17500 17500 17500 12500 600]
ElementLine[-17500 -17500 -12500 -17500 600]
ElementLine[-17500 -17500 -17500 -12500 600]
ElementLine[17500 17500 12500 17500 600]
ElementLine[16000 16000 16000 12500 600]
ElementLine[-16000 -15500 -12500 -15500 600]
ElementLine[-16000 -15500 -16000 -12500 600]
ElementLine[16000 16000 12500 16000 600]
ElementLine[16000 -12500 17500 -12500 600]
ElementLine[16000 -12500 16000 -15500 600]
ElementLine[17500 -12500 17500 -17500 600]
ElementLine[12500 -15500 12500 -17500 600]
ElementLine[12500 -15500 16000 -15500 600]
ElementLine[12500 -17500 17500 -17500 600]
ElementLine[-12500 -15500 -12500 -17500 600]
ElementLine[-12500 -15500 12500 -15500 600]
ElementLine[-12500 -17500 12500 -17500 600]
ElementLine[-12500 17500 -12500 16000 600]
ElementLine[-12500 17500 -17500 17500 600]
ElementLine[-12500 16000 -16000 16000 600]
ElementLine[12500 17500 12500 16000 600]
ElementLine[12500 17500 -12500 17500 600]
ElementLine[12500 16000 -12500 16000 600]
ElementLine[16000 12500 17500 12500 600]
ElementLine[16000 12500 16000 -12500 600]
ElementLine[17500 12500 17500 -12500 600]
ElementLine[-17500 12500 -16000 12500 600]
ElementLine[-17500 12500 -17500 17500 600]
ElementLine[-16000 12500 -16000 16000 600]
ElementLine[-17500 -12500 -16000 -12500 600]
ElementLine[-17500 -12500 -17500 12500 600]
ElementLine[-16000 -12500 -16000 12500 600]
ElementArc[0 0 9994 9994 0 360 600]
ElementArc[0 0 11995 11995 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 9997 9997 0 360 600]
ElementArc[0 0 11988 11988 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 9995 9995 0 360 600]
ElementArc[0 0 9992 9992 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 11995 11995 0 360 600]
ElementArc[0 0 11992 11992 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
Pin[-10000 -10000 5200 2000 6000 3200 "" "1" ""]
Pad[-10000 -12600 -10000 -7400 5200 2000 6000 "" "1" ""]
Pad[-10000 -12600 -10000 -7400 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 -10000 5200 2000 6000 3200 "" "2" ""]
Pad[0 -12600 0 -7400 5200 2000 6000 "" "2" ""]
Pad[0 -12600 0 -7400 5200 2000 6000 "" "2" ",onsolder"]
Pin[10000 -10000 5200 2000 6000 3200 "" "3" ""]
Pad[10000 -12600 10000 -7400 5200 2000 6000 "" "3" ""]
Pad[10000 -12600 10000 -7400 5200 2000 6000 "" "3" ",onsolder"]
Pin[-10000 10000 5200 2000 6000 3200 "" "4" ""]
Pad[-10000 7400 -10000 12600 5200 2000 6000 "" "4" ""]
Pad[-10000 7400 -10000 12600 5200 2000 6000 "" "4" ",onsolder"]
Pin[0 10000 5200 2000 6000 3200 "" "5" ""]
Pad[0 7400 0 12600 5200 2000 6000 "" "5" ""]
Pad[0 7400 0 12600 5200 2000 6000 "" "5" ",onsolder"]
Pin[10000 10000 5200 2000 6000 3200 "" "6" ""]
Pad[10000 7400 10000 12600 5200 2000 6000 "" "6" ""]
Pad[10000 7400 10000 12600 5200 2000 6000 "" "6" ",onsolder"]
)
