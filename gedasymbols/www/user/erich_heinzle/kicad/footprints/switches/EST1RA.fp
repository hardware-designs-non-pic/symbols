# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module EST1RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: EST1RA
# Text descriptor count: 1
# Draw segment object count: 85
# Draw circle object count: 0
# Draw arc object count: 1
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -22170 -2810 1 100 ""]
(
ElementLine[-16610 -16370 -11490 -16370 1000]
ElementLine[-11490 -16370 -8740 -16370 1000]
ElementLine[-8740 -16370 -1650 -16370 1000]
ElementLine[-1650 -16370 1490 -16370 1000]
ElementLine[1490 -16370 8580 -16370 1000]
ElementLine[8580 -16370 11330 -16370 1000]
ElementLine[11330 -16370 16450 -16370 1000]
ElementLine[16450 -16370 16450 -1810 1000]
ElementLine[16450 -1810 15660 -1810 1000]
ElementLine[15660 -1810 15660 13140 1000]
ElementLine[15660 13140 9370 13140 1000]
ElementLine[9370 13140 3460 13140 1000]
ElementLine[3460 13140 -3220 13140 1000]
ElementLine[-3220 13140 -9520 13140 1000]
ElementLine[-9520 13140 -15820 13140 1000]
ElementLine[-15820 13140 -15820 -1810 1000]
ElementLine[-15820 -1810 -16610 -1810 1000]
ElementLine[-16610 -1810 -16610 -16370 1000]
ElementLine[9370 13140 10150 13140 500]
ElementLine[10150 13140 10150 21810 500]
ElementLine[10150 21810 10150 22200 500]
ElementLine[10150 22200 9370 22990 500]
ElementLine[-9520 13140 -9520 21810 500]
ElementLine[-9520 21810 -9520 22200 500]
ElementLine[-9520 22200 -8740 22990 500]
ElementLine[-8740 22990 -4010 22990 500]
ElementLine[-4010 22990 3460 22990 500]
ElementLine[3460 22990 9370 22990 500]
ElementLine[-9520 21810 -3220 21810 500]
ElementLine[-3220 21810 3460 21810 500]
ElementLine[3460 21810 10150 21810 500]
ElementLine[-3220 13140 -3220 21810 500]
ElementLine[3460 13140 3460 21810 500]
ElementLine[-15820 -1810 15660 -1810 1000]
ElementLine[-4010 22990 -9130 46220 500]
ElementLine[3460 22990 -1650 47000 500]
ElementLine[-11490 -30940 -11100 -30940 500]
ElementLine[-11100 -30940 -8740 -30940 500]
ElementLine[-8740 -30940 -8740 -16370 500]
ElementLine[-11490 -30940 -11490 -16370 500]
ElementLine[-1650 -20700 -1250 -20700 500]
ElementLine[-1250 -20700 1490 -20700 500]
ElementLine[1490 -20700 1490 -16370 500]
ElementLine[-1650 -20700 -1650 -16370 500]
ElementLine[8580 -30940 11330 -30940 500]
ElementLine[11330 -30940 11330 -16370 500]
ElementLine[8580 -30940 8580 -16370 500]
ElementLine[-11100 -30940 -11100 -16770 500]
ElementLine[-10700 -16770 -10700 -30550 500]
ElementLine[-10700 -30550 -9130 -30550 500]
ElementLine[-9130 -30550 -9130 -16770 500]
ElementLine[-9130 -16770 -10310 -16770 500]
ElementLine[-10310 -16770 -10310 -30150 500]
ElementLine[-10310 -30150 -9520 -30150 500]
ElementLine[-9520 -30150 -9520 -17160 500]
ElementLine[-9520 -17160 -9920 -17160 500]
ElementLine[-9920 -17160 -9920 -29760 500]
ElementLine[-1250 -20700 -1250 -16770 500]
ElementLine[-1250 -16770 1100 -16770 500]
ElementLine[1100 -16770 1100 -20310 500]
ElementLine[1100 -20310 -860 -20310 500]
ElementLine[-860 -20310 -860 -17160 500]
ElementLine[-860 -17160 700 -17160 500]
ElementLine[700 -17160 700 -19920 500]
ElementLine[700 -19920 -470 -19920 500]
ElementLine[-470 -19920 -470 -17550 500]
ElementLine[-470 -17550 -70 -17550 500]
ElementLine[-70 -17550 310 -17550 500]
ElementLine[310 -17550 310 -19520 500]
ElementLine[310 -19520 -70 -19520 500]
ElementLine[-70 -19520 -70 -17550 500]
ElementLine[8970 -30550 8970 -16770 500]
ElementLine[8970 -16770 9370 -16770 500]
ElementLine[9370 -16770 10940 -16770 500]
ElementLine[10940 -16770 10940 -30550 500]
ElementLine[10940 -30550 9370 -30550 500]
ElementLine[9370 -30550 9370 -16770 500]
ElementLine[9370 -16770 9760 -17160 500]
ElementLine[9760 -17160 10550 -17160 500]
ElementLine[10550 -17160 10550 -30150 500]
ElementLine[10550 -30150 10150 -30150 500]
ElementLine[10150 -30150 9760 -30150 500]
ElementLine[9760 -30150 9760 -17550 500]
ElementLine[9760 -17550 10150 -17550 500]
ElementLine[10150 -17550 10150 -30150 500]
ElementArc[-5410 46810 3765 3765 0 360 500]
Pin[-10000 -30000 6600 2000 7400 4000 "" "P$1" ""]
Pin[0 -20000 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -30000 6600 2000 7400 4000 "" "P$3" ""]
Pin[10000 -20000 6600 2000 7400 4000 "" "P$4" ""]
Pin[-10000 -20000 6600 2000 7400 4000 "" "P$6" ""]
)
