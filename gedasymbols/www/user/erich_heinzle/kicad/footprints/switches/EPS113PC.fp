# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module EPS113PC
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: EPS113PC
# Text descriptor count: 1
# Draw segment object count: 26
# Draw circle object count: 2
# Draw arc object count: 1
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 5500 -39500 0 100 ""]
(
ElementLine[-10510 -25510 -13010 -23010 500]
ElementLine[-10510 -25510 10510 -25510 500]
ElementLine[13010 -23400 10510 -25510 500]
ElementLine[10510 25110 13010 23010 500]
ElementLine[10510 25110 -10510 25110 500]
ElementLine[-13010 23010 -10510 25110 500]
ElementLine[17000 -9000 17000 9000 500]
ElementLine[-17000 -9000 -17000 9000 500]
ElementLine[7500 12500 10000 10000 500]
ElementLine[-10000 10000 -7500 12500 500]
ElementLine[13010 23010 13010 11690 500]
ElementLine[13010 -23400 13010 -11690 500]
ElementLine[-13010 -11690 -13010 -23010 500]
ElementLine[-13010 23010 -13010 11490 500]
ElementLine[-17000 9000 -13010 11490 500]
ElementLine[-13800 11100 0 20000 500]
ElementLine[17000 9000 13010 11690 500]
ElementLine[13800 11300 0 20000 500]
ElementLine[17000 -9000 13010 -11690 500]
ElementLine[13800 -11300 0 -20000 500]
ElementLine[-17000 -9000 -13010 -11690 500]
ElementLine[-16940 -8930 0 -20000 500]
ElementLine[-1000 -14000 -1500 -14000 500]
ElementLine[-1500 -14000 -1500 -11500 500]
ElementLine[2000 -14000 2000 -11000 500]
ElementLine[-7480 12590 7480 12590 500]
ElementArc[0 0 4172 4172 0 360 500]
ElementArc[0 0 7863 7863 0 360 500]
ElementArc[0 0 14142 14142 0 360 500]
Pin[0 -18550 10000 2000 20800 6600 "" "1" ""]
Pad[-5000 -18550 5000 -18550 10000 2000 10800 "" "1" ""]
Pad[-5000 -18550 5000 -18550 10000 2000 10800 "" "1" ",onsolder"]
Pin[0 0 10000 2000 20800 6600 "" "2" ""]
Pad[-5000 0 5000 0 10000 2000 10800 "" "2" ""]
Pad[-5000 0 5000 0 10000 2000 10800 "" "2" ",onsolder"]
Pin[0 18160 10000 2000 20800 6600 "" "3" ""]
Pad[-5000 18160 5000 18160 10000 2000 10800 "" "3" ""]
Pad[-5000 18160 5000 18160 10000 2000 10800 "" "3" ",onsolder"]
)
