# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-TMAV433BAA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-TMAV433BAA
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -17500 0 100 ""]
(
ElementLine[-2500 8120 2500 8120 500]
ElementLine[2500 8120 2500 6250 500]
ElementLine[2500 6250 -2500 6250 500]
ElementLine[-2500 6250 -2500 8120 500]
ElementLine[-2500 6250 -3750 6250 500]
ElementLine[-2500 -8120 -2500 -6250 500]
ElementLine[-2500 -6250 2500 -6250 500]
ElementLine[2500 -6250 2500 -8120 500]
ElementLine[2500 -8120 -2500 -8120 500]
ElementLine[-4370 5620 -4370 -5620 500]
ElementLine[-3750 -6250 -2500 -6250 500]
ElementLine[-1870 4370 1870 4370 500]
ElementLine[1870 4370 1870 -4370 500]
ElementLine[1870 -4370 -1870 -4370 500]
ElementLine[-1870 -4370 -1870 4370 500]
ElementLine[2500 -6250 3750 -6250 500]
ElementLine[4370 -5620 4370 5620 500]
ElementLine[3750 6250 2500 6250 500]
ElementArc[-3750 5620 630 630 0 360 500]
ElementArc[-3750 -5620 620 620 0 360 500]
ElementArc[3750 -5620 630 630 0 360 500]
ElementArc[3750 5620 620 620 0 360 500]
Pad[-200 -10620 200 -10620 5600 2000 6400 "" "P1" "square"]
Pad[-200 10620 200 10620 5600 2000 6400 "" "P2" "square"]
)
