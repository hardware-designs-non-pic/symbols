# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module TR1-15,1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TR1-15,1
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 290 -3280 0 100 ""]
(
ElementLine[-24600 -29600 24600 -29600 500]
ElementLine[24600 -29600 24600 19600 500]
ElementLine[24600 19600 -24600 19600 500]
ElementLine[-24600 19600 -24600 -29600 500]
ElementLine[-29520 -34520 29520 -34520 1000]
ElementLine[29520 -34520 29520 24520 1000]
ElementLine[29520 24520 -29520 24520 1000]
ElementLine[-29520 24520 -29520 -34520 1000]
Pin[0 -10070 5600 2000 6400 3200 "" "P$1" ""]
Pin[-10000 -10070 5600 2000 6400 3200 "" "P$2" ""]
Pin[10000 -10070 5600 2000 6400 3200 "" "P$3" ""]
Pin[-10000 9920 5600 2000 6400 3200 "" "P$4" ""]
Pin[10000 9920 5600 2000 6400 3200 "" "P$5" ""]
)
