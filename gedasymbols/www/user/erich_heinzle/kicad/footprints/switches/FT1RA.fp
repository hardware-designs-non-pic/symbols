# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module FT1RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: FT1RA
# Text descriptor count: 1
# Draw segment object count: 74
# Draw circle object count: 0
# Draw arc object count: 1
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -17460 720 1 100 ""]
(
ElementLine[-13770 -8030 13770 -8030 500]
ElementLine[13770 -8030 13770 7710 500]
ElementLine[13770 7710 13770 13220 500]
ElementLine[13770 13220 11810 13220 500]
ElementLine[11810 13220 7870 13220 500]
ElementLine[7870 13220 -7870 13220 500]
ElementLine[-7870 13220 -11810 13220 500]
ElementLine[-11810 13220 -13770 13220 500]
ElementLine[-7870 13220 -7870 18740 500]
ElementLine[-7870 18740 -7080 19520 500]
ElementLine[-7080 19520 -3540 19520 500]
ElementLine[-3540 19520 3540 19520 500]
ElementLine[3540 19520 7080 19520 500]
ElementLine[7080 19520 7870 18740 500]
ElementLine[7870 18740 7870 13220 500]
ElementLine[-7870 18740 7870 18740 500]
ElementLine[-3540 19520 -6290 30940 500]
ElementLine[3540 19520 780 31730 500]
ElementLine[-13770 13220 -13770 7710 500]
ElementLine[-13770 7710 -13770 -8030 500]
ElementLine[-11810 13220 -11810 8110 500]
ElementLine[-11810 8110 -11810 7710 500]
ElementLine[-11810 7710 -13770 7710 500]
ElementLine[13770 7710 11810 7710 500]
ElementLine[11810 7710 11810 8110 500]
ElementLine[11810 8110 11810 13220 500]
ElementLine[-11810 8110 11810 8110 500]
ElementLine[-11410 -10620 -8660 -10620 500]
ElementLine[-8660 -10620 -8660 -8260 500]
ElementLine[-8660 -8260 -11410 -8260 500]
ElementLine[-11410 -8260 -11410 -10620 500]
ElementLine[-11410 -10620 -11020 -10620 500]
ElementLine[-11020 -10620 -11020 -8660 500]
ElementLine[-11020 -8660 -10620 -8660 500]
ElementLine[-10620 -8660 -9050 -8660 500]
ElementLine[-9050 -8660 -9050 -10230 500]
ElementLine[-9050 -10230 -10620 -10230 500]
ElementLine[-10620 -10230 -10620 -8660 500]
ElementLine[-10620 -8660 -9440 -8660 500]
ElementLine[-9440 -8660 -9440 -9840 500]
ElementLine[-9440 -9840 -10230 -9840 500]
ElementLine[-10230 -9840 -10230 -9050 500]
ElementLine[-10230 -9050 -9840 -9050 500]
ElementLine[-9840 -9050 -9840 -9440 500]
ElementLine[-1570 -10620 -1570 -8260 500]
ElementLine[-1570 -8260 1570 -8260 500]
ElementLine[1570 -8260 1570 -10620 500]
ElementLine[1570 -10620 -1570 -10620 500]
ElementLine[-1570 -10620 -1180 -10620 500]
ElementLine[-1180 -10620 -1180 -8660 500]
ElementLine[-1180 -8660 1180 -8660 500]
ElementLine[1180 -8660 1180 -10230 500]
ElementLine[1180 -10230 -780 -10230 500]
ElementLine[-780 -10230 -780 -9050 500]
ElementLine[-780 -9050 780 -9050 500]
ElementLine[780 -9050 780 -9840 500]
ElementLine[780 -9840 -390 -9840 500]
ElementLine[-390 -9840 -390 -9440 500]
ElementLine[-390 -9440 390 -9440 500]
ElementLine[8660 -10620 8660 -8260 500]
ElementLine[8660 -8260 11410 -8260 500]
ElementLine[11410 -8260 11410 -10620 500]
ElementLine[11410 -10620 9050 -10620 500]
ElementLine[8660 -10620 9050 -10620 500]
ElementLine[9050 -10620 9050 -8660 500]
ElementLine[9050 -8660 11020 -8660 500]
ElementLine[11020 -8660 11020 -10230 500]
ElementLine[11020 -10230 9440 -10230 500]
ElementLine[9440 -10230 9440 -9050 500]
ElementLine[9440 -9050 10620 -9050 500]
ElementLine[10620 -9050 10620 -9840 500]
ElementLine[10620 -9840 9840 -9840 500]
ElementLine[9840 -9840 9840 -9440 500]
ElementLine[9840 -9440 10230 -9440 500]
ElementArc[-2750 31330 3553 3553 0 360 500]
Pin[0 -10000 6600 2000 7400 4000 "" "P$1" ""]
Pin[10000 -10000 6600 2000 7400 4000 "" "P$2" ""]
Pin[-10000 -10000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 10000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 10000 6600 2000 7400 4000 "" "P$5" ""]
)
