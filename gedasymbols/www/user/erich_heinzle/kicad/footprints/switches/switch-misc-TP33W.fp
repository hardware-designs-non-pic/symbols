# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-TP33W
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-TP33W
# Text descriptor count: 1
# Draw segment object count: 53
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -21000 7500 1 100 ""]
(
ElementLine[-16500 20000 -16500 -14000 600]
ElementLine[16500 -14000 16500 20000 600]
ElementLine[-3500 -52000 3500 -52000 600]
ElementLine[-9000 -36500 -9000 -21500 600]
ElementLine[9000 -36500 9000 -21500 600]
ElementLine[5000 -50500 5000 -38000 600]
ElementLine[-5000 -50500 -5000 -38000 600]
ElementLine[-5000 -50500 5000 -50500 600]
ElementLine[-3500 -52000 -5000 -50500 600]
ElementLine[3500 -52000 5000 -50500 600]
ElementLine[-9000 -36500 -7500 -38000 600]
ElementLine[-7500 -38000 -5000 -38000 600]
ElementLine[-5000 -38000 5000 -38000 600]
ElementLine[5000 -38000 7500 -38000 600]
ElementLine[7500 -38000 9000 -36500 600]
ElementLine[9000 -36500 -9000 -36500 600]
ElementLine[-16500 -14000 -11000 -14000 600]
ElementLine[-11000 -14000 11000 -14000 600]
ElementLine[11000 -14000 16500 -14000 600]
ElementLine[-10000 -20500 -9000 -21500 600]
ElementLine[-9000 -21500 9000 -21500 600]
ElementLine[9000 -21500 10000 -20500 600]
ElementLine[10000 -20500 10000 -15000 600]
ElementLine[10000 -15000 11000 -14000 600]
ElementLine[-10000 -20500 -10000 -15000 600]
ElementLine[-10000 -15000 -11000 -14000 600]
ElementLine[10000 -15000 -10000 -15000 600]
ElementLine[-10000 -20500 10000 -20500 600]
ElementLine[16500 20000 15000 20000 600]
ElementLine[15000 20000 12000 17000 600]
ElementLine[12000 17000 10000 15000 600]
ElementLine[10000 15000 8000 15000 600]
ElementLine[8000 15000 2000 15000 600]
ElementLine[2000 15000 -2000 15000 600]
ElementLine[-2000 15000 -8000 15000 600]
ElementLine[-8000 15000 -10000 15000 600]
ElementLine[-10000 15000 -12000 17000 600]
ElementLine[-12000 17000 -15000 20000 600]
ElementLine[-15000 20000 -16500 20000 600]
ElementLine[-12000 17000 -12000 20500 600]
ElementLine[-12000 20500 -8000 20500 600]
ElementLine[-8000 20500 -8000 16000 600]
ElementLine[-8000 16000 -8000 15000 600]
ElementLine[-2000 20500 -2000 16000 600]
ElementLine[2000 20500 2000 16000 600]
ElementLine[-2000 16000 -2000 15000 600]
ElementLine[2000 16000 2000 15000 600]
ElementLine[8000 16000 8000 15000 600]
ElementLine[8000 20500 8000 16000 600]
ElementLine[12000 17000 12000 20500 600]
ElementLine[-2000 20500 2000 20500 600]
ElementLine[8000 20500 12000 20500 600]
ElementLine[0 -30000 0 -35500 600]
Pin[10000 20000 6600 2000 7400 4400 "" "1" ""]
Pad[10000 16700 10000 23300 6600 2000 7400 "" "1" ""]
Pad[10000 16700 10000 23300 6600 2000 7400 "" "1" ",onsolder"]
Pin[0 20000 6600 2000 7400 4400 "" "2" ""]
Pad[0 16700 0 23300 6600 2000 7400 "" "2" ""]
Pad[0 16700 0 23300 6600 2000 7400 "" "2" ",onsolder"]
Pin[-10000 20000 6600 2000 7400 4400 "" "3" ""]
Pad[-10000 16700 -10000 23300 6600 2000 7400 "" "3" ""]
Pad[-10000 16700 -10000 23300 6600 2000 7400 "" "3" ",onsolder"]
Pin[10000 0 6600 2000 7400 4400 "" "M" ""]
Pad[10000 -3300 10000 3300 6600 2000 7400 "" "M" ""]
Pad[10000 -3300 10000 3300 6600 2000 7400 "" "M" ",onsolder"]
Pin[-10000 0 6600 2000 7400 4400 "" "M1" ""]
Pad[-10000 -3300 -10000 3300 6600 2000 7400 "" "M1" ""]
Pad[-10000 -3300 -10000 3300 6600 2000 7400 "" "M1" ",onsolder"]
)
