# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module APE1RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: APE1RA
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -22800 -4000 1 100 ""]
(
ElementLine[-16730 -20000 16730 -20000 1000]
ElementLine[18700 -6220 18700 11490 1000]
ElementLine[-18700 11490 -18700 -6220 1000]
ElementLine[16730 -20000 16730 -6220 1000]
ElementLine[16730 -6220 18700 -6220 1000]
ElementLine[-16730 -20000 -16730 -6220 1000]
ElementLine[-16730 -6220 -18700 -6220 1000]
ElementLine[-7870 12480 -7870 26250 1000]
ElementLine[7870 12480 7870 26250 1000]
ElementLine[-6880 27240 6880 27240 1000]
ElementLine[-7870 12480 7870 12480 500]
ElementLine[-18700 -12120 -18700 -6220 500]
ElementLine[18700 -12120 18700 -6220 500]
ElementLine[-18700 11490 18700 11490 1000]
ElementLine[-4720 27160 -4720 42910 500]
ElementLine[-4720 42910 4720 42910 500]
ElementLine[4720 42910 4720 27160 500]
ElementLine[-16530 -6290 16920 -6290 500]
ElementArc[-9590 13460 1986 1986 0 360 500]
ElementArc[9590 13460 1980 1980 0 360 500]
ElementArc[-6880 26250 990 990 0 360 1000]
ElementArc[6880 26250 990 990 0 360 1000]
Pin[0 -20000 6600 2000 7400 4000 "" "P$1" ""]
Pin[-10000 -20000 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -20000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 10000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 10000 6600 2000 7400 4000 "" "P$5" ""]
)
