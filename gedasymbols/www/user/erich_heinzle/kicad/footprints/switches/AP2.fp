# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module AP2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: AP2
# Text descriptor count: 1
# Draw segment object count: 30
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -19720 1740 1 100 ""]
(
ElementLine[-14960 -18700 -14960 18700 1000]
ElementLine[-14960 18700 -10620 18700 1000]
ElementLine[-10620 18700 -5900 18700 1000]
ElementLine[-5900 18700 5510 18700 1000]
ElementLine[5510 18700 10230 18700 1000]
ElementLine[10230 18700 14960 18700 1000]
ElementLine[14960 18700 14960 -18700 1000]
ElementLine[14960 -18700 10620 -18700 1000]
ElementLine[10620 -18700 5900 -18700 1000]
ElementLine[-5900 -18700 -10620 -18700 1000]
ElementLine[-10620 -18700 -14960 -18700 1000]
ElementLine[-14960 -18700 -14960 -20660 500]
ElementLine[-14960 -20660 -10620 -20660 500]
ElementLine[-10620 -20660 -10620 -18700 500]
ElementLine[14960 -18700 14960 -20660 500]
ElementLine[14960 -20660 10620 -20660 500]
ElementLine[10620 -20660 10620 -18700 500]
ElementLine[-14960 18700 -14960 20660 500]
ElementLine[-14960 20660 -10620 20660 500]
ElementLine[-10620 20660 -10620 18700 500]
ElementLine[14960 18700 14960 20660 500]
ElementLine[14960 20660 10230 20660 500]
ElementLine[10230 20660 10230 18700 500]
ElementLine[-5900 -18700 -5900 -20660 500]
ElementLine[-5900 -20660 5900 -20660 500]
ElementLine[5900 -20660 5900 -18700 500]
ElementLine[-5900 18700 -5900 20660 500]
ElementLine[-5900 20660 5510 20660 500]
ElementLine[5510 20660 5510 18700 500]
ElementLine[-5900 -18700 5900 -18700 1000]
ElementArc[0 0 5558 5558 0 360 250]
ElementArc[0 0 3606 3606 0 360 250]
Pin[-10000 0 6600 2000 7400 4000 "" "P$1" ""]
Pin[10000 0 6600 2000 7400 4000 "" "P$2" ""]
Pin[-10000 -10000 6600 2000 7400 4000 "" "P$3" ""]
Pin[10000 -10000 6600 2000 7400 4000 "" "P$4" ""]
Pin[-10000 10000 6600 2000 7400 4000 "" "P$5" ""]
Pin[10000 10000 6600 2000 7400 4000 "" "P$6" ""]
)
