# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-SRKL
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-SRKL
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 4
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -12000 -38500 0 100 ""]
(
ElementLine[-24500 34490 24500 34490 600]
ElementLine[24500 -34490 -24500 -34490 600]
ElementLine[24500 33500 -24500 33500 600]
ElementLine[24500 -34490 24500 33500 600]
ElementLine[24500 -34490 -24500 -34490 600]
ElementLine[24500 -34490 24500 34490 600]
ElementLine[-24500 34490 -24500 -34490 600]
ElementLine[-24500 -34490 -24500 33500 600]
ElementLine[-20000 29000 20000 29000 600]
ElementLine[20000 -2000 20000 29000 600]
ElementLine[20000 -2000 -20000 -2000 600]
ElementLine[-20000 29000 -20000 -2000 600]
ElementLine[-23000 -7000 23000 -7000 600]
ElementLine[23000 -33000 23000 -7000 600]
ElementLine[23000 -33000 -23000 -33000 600]
ElementLine[-23000 -7000 -23000 -33000 600]
ElementLine[-23000 -7000 -20000 -2000 600]
ElementLine[23000 -7000 20000 -2000 600]
ElementLine[20000 29000 24500 33500 600]
ElementLine[-20000 29000 -24500 33500 600]
ElementLine[12800 -30000 17190 -30000 500]
ElementLine[15000 -27800 15000 -32200 500]
ElementLine[-17190 30000 -12800 30000 500]
ElementLine[-15000 32200 -15000 27800 500]
ElementArc[0 -20000 3182 3182 0 360 300]
ElementArc[0 -20000 4243 4243 0 360 300]
ElementArc[15000 -30000 1556 1556 0 360 500]
ElementArc[-15000 30000 1556 1556 0 360 500]
Pin[-5000 -20000 5200 2000 6000 3200 "" "+" ""]
Pad[-5000 -22600 -5000 -17400 5200 2000 6000 "" "+" ""]
Pad[-5000 -22600 -5000 -17400 5200 2000 6000 "" "+" ",onsolder"]
Pin[5000 -20000 5200 2000 6000 3200 "" "-" ""]
Pad[5000 -22600 5000 -17400 5200 2000 6000 "" "-" ""]
Pad[5000 -22600 5000 -17400 5200 2000 6000 "" "-" ",onsolder"]
Pin[-15000 20000 5200 2000 6000 3200 "" "1" ""]
Pad[-15000 17400 -15000 22600 5200 2000 6000 "" "1" ""]
Pad[-15000 17400 -15000 22600 5200 2000 6000 "" "1" ",onsolder"]
Pin[-15000 0 5200 2000 6000 3200 "" "2" ""]
Pad[-15000 -2600 -15000 2600 5200 2000 6000 "" "2" ""]
Pad[-15000 -2600 -15000 2600 5200 2000 6000 "" "2" ",onsolder"]
Pin[15000 0 5200 2000 6000 3200 "" "3" ""]
Pad[15000 -2600 15000 2600 5200 2000 6000 "" "3" ""]
Pad[15000 -2600 15000 2600 5200 2000 6000 "" "3" ",onsolder"]
Pin[15000 20000 5200 2000 6000 3200 "" "4" ""]
Pad[15000 17400 15000 22600 5200 2000 6000 "" "4" ""]
Pad[15000 17400 15000 22600 5200 2000 6000 "" "4" ",onsolder"]
)
