# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-FSMRA4J
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-FSMRA4J
# Text descriptor count: 1
# Draw segment object count: 25
# Draw circle object count: 6
# Draw arc object count: 4
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 2500 -22500 0 100 ""]
(
ElementLine[-11020 -11810 -11020 -13770 500]
ElementLine[-11020 -13770 -6690 -13770 500]
ElementLine[-6690 -13770 -6690 -11810 500]
ElementLine[6690 -11810 6690 -13770 500]
ElementLine[6690 -13770 11020 -13770 500]
ElementLine[11020 -13770 11020 -11810 500]
ElementLine[11020 11810 11020 13770 500]
ElementLine[11020 13770 6690 13770 500]
ElementLine[6690 13770 6690 11810 500]
ElementLine[-11020 11810 -11020 13770 500]
ElementLine[-11020 13770 -6690 13770 500]
ElementLine[-6690 13770 -6690 11810 500]
ElementLine[-11810 -11410 -11810 11410 500]
ElementLine[-11410 11810 -6690 11810 500]
ElementLine[-6690 11810 6690 11810 500]
ElementLine[6690 11810 11410 11810 500]
ElementLine[11810 11410 11810 -11410 500]
ElementLine[11410 -11810 6690 -11810 500]
ElementLine[6690 -11810 -6690 -11810 500]
ElementLine[-6690 -11810 -11410 -11810 500]
ElementLine[-11800 -31400 15800 -31400 500]
ElementLine[2000 -31400 2000 -21600 500]
ElementLine[-6800 -21600 2000 -21600 500]
ElementLine[2000 -21600 10800 -21600 500]
ElementLine[-16400 -36200 -16400 -12700 1200]
ElementArc[0 0 4172 4172 0 360 250]
ElementArc[-9840 -9840 552 552 0 360 250]
ElementArc[9840 -9840 552 552 0 360 250]
ElementArc[9840 9840 552 552 0 360 250]
ElementArc[-9840 9840 552 552 0 360 250]
ElementArc[0 0 5006 5006 0 360 250]
ElementArc[-11410 -11410 400 400 0 360 500]
ElementArc[-11410 11410 400 400 0 360 500]
ElementArc[11410 11410 400 400 0 360 500]
ElementArc[11410 -11410 400 400 0 360 500]
Pin[-8850 -12790 7500 2000 8300 5000 "" "1" ""]
Pin[-8850 12590 7500 2000 8300 5000 "" "2" ""]
Pin[8850 -12990 7500 2000 8300 5000 "" "3" ""]
Pin[8850 12400 7500 2000 8300 5000 "" "4" ""]
Pin[15770 -31460 7950 2000 8750 5300 "" "5" ""]
Pin[-11820 -31460 7950 2000 8750 5300 "" "6" ""]
Pin[10800 -21640 6150 2000 6950 4100 "" "7" ""]
Pin[-6790 -21640 6150 2000 6950 4100 "" "8" ""]
)
