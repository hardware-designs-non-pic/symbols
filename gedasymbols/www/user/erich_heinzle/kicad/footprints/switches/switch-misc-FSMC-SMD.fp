# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-FSMC-SMD
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-FSMC-SMD
# Text descriptor count: 1
# Draw segment object count: 17
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 2
#
Element["" ">NAME" "" "" 0 0 2500 -27500 0 100 ""]
(
ElementLine[-2750 -11810 -2750 -13770 500]
ElementLine[-2750 -13770 1960 -13770 500]
ElementLine[1960 -13770 1960 -11810 500]
ElementLine[-2750 11810 -2750 13770 500]
ElementLine[-2750 13770 1960 13770 500]
ElementLine[1960 13770 1960 11810 500]
ElementLine[-7080 -11410 -7080 11410 500]
ElementLine[6690 11410 6690 -11410 500]
ElementLine[-6490 -11810 6290 -11810 500]
ElementLine[6290 -11810 6490 -11810 500]
ElementLine[-6490 11810 1960 11810 500]
ElementLine[1960 11810 6290 11810 500]
ElementLine[6290 11810 6490 11810 500]
ElementLine[-3140 -7870 -3140 7870 500]
ElementLine[-3140 -7870 2750 -7870 500]
ElementLine[2750 -7870 2750 7870 500]
ElementLine[-3140 7870 2750 7870 500]
ElementArc[-6690 -11410 390 390 0 360 500]
ElementArc[-6690 11410 400 400 0 360 500]
ElementArc[6290 11410 400 400 0 360 500]
ElementArc[6290 -11410 400 400 0 360 500]
Pad[-370 -19615 -370 -16465 5110 2000 5910 "" "P$1" "square"]
Pad[-370 16535 -370 19685 5110 2000 5910 "" "P$2" "square"]
)
