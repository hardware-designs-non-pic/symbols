# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-9077-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-9077-1
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 0
# Draw arc object count: 10
# Pad count: 3
#
Element["" "T" "" "" 0 0 -12500 -8500 0 100 ""]
(
ElementLine[-17500 -13500 -17500 -8000 600]
ElementLine[17500 13500 12500 13500 600]
ElementLine[17500 13500 17500 8000 600]
ElementLine[-17500 -13500 -12500 -13500 600]
ElementLine[-16000 -12000 -16000 -8000 600]
ElementLine[16000 12000 12500 12000 600]
ElementLine[16000 12000 16000 8000 600]
ElementLine[-16000 -12000 -12500 -12000 600]
ElementLine[-16000 8000 -17500 8000 600]
ElementLine[-16000 8000 -16000 12000 600]
ElementLine[-17500 8000 -17500 13500 600]
ElementLine[-12500 12000 -12500 13500 600]
ElementLine[-12500 12000 -16000 12000 600]
ElementLine[-12500 13500 -17500 13500 600]
ElementLine[12500 12000 12500 13500 600]
ElementLine[12500 12000 -12500 12000 600]
ElementLine[12500 13500 -12500 13500 600]
ElementLine[12500 -13500 12500 -12000 600]
ElementLine[12500 -13500 17500 -13500 600]
ElementLine[12500 -12000 16000 -12000 600]
ElementLine[-12500 -13500 -12500 -12000 600]
ElementLine[-12500 -13500 12500 -13500 600]
ElementLine[-12500 -12000 12500 -12000 600]
ElementLine[-16000 -8000 -17500 -8000 600]
ElementLine[-16000 -8000 -16000 8000 600]
ElementLine[-17500 -8000 -17500 8000 600]
ElementLine[17500 -8000 16000 -8000 600]
ElementLine[17500 -8000 17500 -13500 600]
ElementLine[16000 -8000 16000 -12000 600]
ElementLine[17500 8000 16000 8000 600]
ElementLine[17500 8000 17500 -8000 600]
ElementLine[16000 8000 16000 -8000 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 11992 11992 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 9989 9989 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 12000 12000 0 360 600]
ElementArc[0 0 10000 10000 0 360 600]
ElementArc[0 0 9998 9998 0 360 600]
ElementArc[0 0 11997 11997 0 360 600]
Pin[-10000 4000 5200 2000 6000 3200 "" "1" ""]
Pad[-10000 1400 -10000 6600 5200 2000 6000 "" "1" ""]
Pad[-10000 1400 -10000 6600 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 4000 5200 2000 6000 3200 "" "2" ""]
Pad[0 1400 0 6600 5200 2000 6000 "" "2" ""]
Pad[0 1400 0 6600 5200 2000 6000 "" "2" ",onsolder"]
Pin[10000 4000 5200 2000 6000 3200 "" "3" ""]
Pad[10000 1400 10000 6600 5200 2000 6000 "" "3" ""]
Pad[10000 1400 10000 6600 5200 2000 6000 "" "3" ",onsolder"]
)
