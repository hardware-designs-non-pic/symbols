# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-DTE6
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-DTE6
# Text descriptor count: 1
# Draw segment object count: 33
# Draw circle object count: 0
# Draw arc object count: 1
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 2500 -28000 0 100 ""]
(
ElementLine[4000 -19000 4000 -22000 600]
ElementLine[4000 -19000 -4000 -19000 600]
ElementLine[-4000 -19000 -4000 -22000 600]
ElementLine[4000 23500 4000 22000 600]
ElementLine[-4000 23500 -4000 22000 600]
ElementLine[4000 19000 -4000 19000 600]
ElementLine[-4000 22000 4000 22000 600]
ElementLine[-4000 22000 -4000 19000 600]
ElementLine[4000 22000 4000 19000 600]
ElementLine[-4000 -22000 4000 -22000 600]
ElementLine[-4000 -22000 -4000 -23500 600]
ElementLine[4000 -22000 4000 -23500 600]
ElementLine[-5000 -10000 5000 -10000 600]
ElementLine[-10000 -5000 -10000 -2500 600]
ElementLine[-8500 -2500 -10000 2500 600]
ElementLine[-10000 2500 -10000 5000 600]
ElementLine[-9000 0 -7500 0 600]
ElementLine[-5000 10000 5000 10000 600]
ElementLine[-6000 0 -2000 0 600]
ElementLine[-500 0 3500 0 600]
ElementLine[10000 -5000 10000 -2500 600]
ElementLine[10000 2500 10000 5000 600]
ElementLine[11500 -2500 10000 2500 600]
ElementLine[5000 0 9000 0 600]
ElementLine[-15000 13500 -15000 -13500 600]
ElementLine[-15000 13500 -13500 15000 600]
ElementLine[15000 13500 13500 15000 600]
ElementLine[13500 15000 -13500 15000 600]
ElementLine[13500 -15000 15000 -13500 600]
ElementLine[15000 -13500 15000 13500 600]
ElementLine[-15000 -13500 -13500 -15000 600]
ElementLine[-13500 -15000 13500 -15000 600]
ElementLine[-21000 11610 -21000 -11610 600]
ElementArc[0 0 23996 23996 0 360 600]
Pin[-10000 -10000 6600 2000 7400 4400 "" "1" ""]
Pad[-10000 -13300 -10000 -6700 6600 2000 7400 "" "1" ""]
Pad[-10000 -13300 -10000 -6700 6600 2000 7400 "" "1" ",onsolder"]
Pin[10000 -10000 6600 2000 7400 4400 "" "2" ""]
Pad[10000 -13300 10000 -6700 6600 2000 7400 "" "2" ""]
Pad[10000 -13300 10000 -6700 6600 2000 7400 "" "2" ",onsolder"]
Pin[10000 10000 6600 2000 7400 4400 "" "3" ""]
Pad[10000 6700 10000 13300 6600 2000 7400 "" "3" ""]
Pad[10000 6700 10000 13300 6600 2000 7400 "" "3" ",onsolder"]
Pin[-10000 10000 6600 2000 7400 4400 "" "4" ""]
Pad[-10000 6700 -10000 13300 6600 2000 7400 "" "4" ""]
Pad[-10000 6700 -10000 13300 6600 2000 7400 "" "4" ",onsolder"]
)
