# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module TML1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TML1
# Text descriptor count: 1
# Draw segment object count: 10
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">VALUE" "" "" 0 0 3180 3400 0 100 ""]
(
ElementLine[-23620 -23620 23620 -23620 1000]
ElementLine[23620 -23620 23620 23620 1000]
ElementLine[23620 23620 -23620 23620 1000]
ElementLine[-23620 23620 -23620 -23620 1000]
ElementLine[-19680 -19680 19680 -19680 500]
ElementLine[19680 -19680 19680 19680 500]
ElementLine[19680 19680 -19680 19680 500]
ElementLine[-19680 19680 -19680 -19680 500]
ElementLine[-21250 0 -14170 0 500]
ElementLine[-17710 3540 -17710 -3540 500]
ElementArc[-17710 0 2503 2503 0 360 500]
Pin[-9840 -24600 10000 2000 10800 5000 "" "P$1" ""]
Pin[9840 -24600 10000 2000 10800 5000 "" "P$2" ""]
Pin[-9840 24600 10000 2000 10800 5000 "" "P$3" ""]
Pin[9840 24600 10000 2000 10800 5000 "" "P$4" ""]
Pin[-20000 -15000 5600 2000 6400 3200 "" "P$5" ""]
Pin[20000 15000 5600 2000 6400 3200 "" "P$6" ""]
)
