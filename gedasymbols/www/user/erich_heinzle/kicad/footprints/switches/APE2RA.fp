# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module APE2RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: APE2RA
# Text descriptor count: 1
# Draw segment object count: 68
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 -22800 -4000 1 100 ""]
(
ElementLine[-16730 -20000 16730 -20000 1000]
ElementLine[18700 -6220 18700 11490 1000]
ElementLine[-18700 11490 -18700 -6220 1000]
ElementLine[16730 -20000 16730 -6220 1000]
ElementLine[16730 -6220 18700 -6220 1000]
ElementLine[-16730 -20000 -16730 -6220 1000]
ElementLine[-16730 -6220 -18700 -6220 1000]
ElementLine[-7870 12480 -7870 26250 1000]
ElementLine[7870 12480 7870 26250 1000]
ElementLine[-6880 27240 6880 27240 1000]
ElementLine[-7870 12480 7870 12480 500]
ElementLine[-18700 -12120 -18700 -6220 500]
ElementLine[18700 -12120 18700 -6220 500]
ElementLine[-18700 11490 18700 11490 1000]
ElementLine[-11410 -30980 -11410 -19960 500]
ElementLine[-11410 -19960 -8660 -19960 500]
ElementLine[-8660 -19960 -8660 -30980 500]
ElementLine[-8660 -30980 -11020 -30980 500]
ElementLine[-11410 -30980 -11020 -30980 500]
ElementLine[-11020 -30980 -11020 -20350 500]
ElementLine[-11020 -20350 -9050 -20350 500]
ElementLine[-9050 -20350 -9050 -30590 500]
ElementLine[-9050 -30590 -10620 -30590 500]
ElementLine[-10620 -30590 -10620 -20740 500]
ElementLine[-10620 -20740 -9440 -20740 500]
ElementLine[-9440 -20740 -9440 -30190 500]
ElementLine[-9440 -30190 -10230 -30190 500]
ElementLine[-10230 -30190 -10230 -21140 500]
ElementLine[-10230 -21140 -9840 -21140 500]
ElementLine[-9840 -21140 -9840 -29800 500]
ElementLine[-1570 -30980 -1570 -20350 500]
ElementLine[-1570 -20350 1570 -20350 500]
ElementLine[1570 -20350 1570 -30980 500]
ElementLine[1570 -30980 -1180 -30980 500]
ElementLine[-1570 -30980 -1180 -30980 500]
ElementLine[-1180 -30980 -1180 -20740 500]
ElementLine[-1180 -20740 1180 -20740 500]
ElementLine[1180 -20740 1180 -30590 500]
ElementLine[1180 -30590 -780 -30590 500]
ElementLine[-780 -30590 -780 -21140 500]
ElementLine[-780 -21140 780 -21140 500]
ElementLine[780 -21140 780 -30190 500]
ElementLine[780 -30190 -390 -30190 500]
ElementLine[-390 -30190 -390 -21530 500]
ElementLine[-390 -21530 390 -21530 500]
ElementLine[390 -21530 390 -29800 500]
ElementLine[390 -29800 0 -29800 500]
ElementLine[0 -29800 0 -21920 500]
ElementLine[8660 -30980 8660 -19960 500]
ElementLine[8660 -19960 11410 -19960 500]
ElementLine[11410 -19960 11410 -30980 500]
ElementLine[11410 -30980 9050 -30980 500]
ElementLine[8660 -30980 9050 -30980 500]
ElementLine[9050 -30980 9050 -20740 500]
ElementLine[9050 -20740 11020 -20740 500]
ElementLine[11020 -20740 11020 -30590 500]
ElementLine[11020 -30590 9440 -30590 500]
ElementLine[9440 -30590 9440 -21140 500]
ElementLine[9440 -21140 10620 -21140 500]
ElementLine[10620 -21140 10620 -30190 500]
ElementLine[10620 -30190 9840 -30190 500]
ElementLine[9840 -30190 9840 -21530 500]
ElementLine[9840 -21530 10230 -21530 500]
ElementLine[10230 -21530 10230 -29800 500]
ElementLine[-4720 27160 -4720 42910 500]
ElementLine[-4720 42910 4720 42910 500]
ElementLine[4720 42910 4720 27160 500]
ElementLine[-16530 -6290 16920 -6290 500]
ElementArc[-9590 13460 1986 1986 0 360 500]
ElementArc[9590 13460 1980 1980 0 360 500]
ElementArc[-6880 26250 990 990 0 360 1000]
ElementArc[6880 26250 990 990 0 360 1000]
Pin[0 -20000 6600 2000 7400 4000 "" "P$1" ""]
Pin[-10000 -20000 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -20000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 10000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 10000 6600 2000 7400 4000 "" "P$5" ""]
Pin[-10000 -30000 6600 2000 7400 4000 "" "P$6" ""]
Pin[0 -30000 6600 2000 7400 4000 "" "P$7" ""]
Pin[10000 -30000 6600 2000 7400 4000 "" "P$8" ""]
)
