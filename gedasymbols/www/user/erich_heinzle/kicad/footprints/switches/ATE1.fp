# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module ATE1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ATE1
# Text descriptor count: 1
# Draw segment object count: 32
# Draw circle object count: 1
# Draw arc object count: 3
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -14610 1740 1 100 ""]
(
ElementLine[-9840 -18700 -9840 18700 1000]
ElementLine[-9840 18700 -7870 18700 1000]
ElementLine[-7870 18700 -1960 18700 1000]
ElementLine[-1960 18700 1960 18700 1000]
ElementLine[1960 18700 7870 18700 1000]
ElementLine[7870 18700 9840 18700 1000]
ElementLine[9840 18700 9840 -18700 1000]
ElementLine[9840 -18700 7870 -18700 1000]
ElementLine[7870 -18700 1960 -18700 1000]
ElementLine[1960 -18700 -1960 -18700 1000]
ElementLine[-1960 -18700 -7870 -18700 1000]
ElementLine[-7870 -18700 -9840 -18700 1000]
ElementLine[-3930 -9840 -2950 980 500]
ElementLine[3930 -9840 2950 980 500]
ElementLine[-9840 -18700 -9840 -20660 500]
ElementLine[-9840 -20660 -7870 -20660 500]
ElementLine[-7870 -20660 -7870 -18700 500]
ElementLine[9840 -18700 9840 -20660 500]
ElementLine[9840 -20660 7870 -20660 500]
ElementLine[7870 -20660 7870 -18700 500]
ElementLine[-9840 18700 -9840 20660 500]
ElementLine[-9840 20660 -7870 20660 500]
ElementLine[-7870 20660 -7870 18700 500]
ElementLine[9840 18700 9840 20660 500]
ElementLine[9840 20660 7870 20660 500]
ElementLine[7870 20660 7870 18700 500]
ElementLine[-1960 -18700 -1960 -20660 500]
ElementLine[-1960 -20660 1960 -20660 500]
ElementLine[1960 -20660 1960 -18700 500]
ElementLine[-1960 18700 -1960 20660 500]
ElementLine[-1960 20660 1960 20660 500]
ElementLine[1960 20660 1960 18700 500]
ElementArc[0 0 5558 5558 0 360 250]
ElementArc[0 980 2950 2950 0 360 500]
ElementArc[0 -9840 3930 3930 0 360 500]
ElementArc[0 0 6297 6297 0 360 500]
Pin[0 0 6600 2000 7400 4000 "" "P$1" ""]
Pin[0 -10000 6600 2000 7400 4000 "" "P$2" ""]
Pin[0 10000 6600 2000 7400 4000 "" "P$3" ""]
)
