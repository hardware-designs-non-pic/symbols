# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module APE1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: APE1
# Text descriptor count: 1
# Draw segment object count: 30
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -14610 1740 1 100 ""]
(
ElementLine[-9840 -18700 -9840 18700 1000]
ElementLine[-9840 18700 -7870 18700 1000]
ElementLine[-7870 18700 -1960 18700 1000]
ElementLine[-1960 18700 1960 18700 1000]
ElementLine[1960 18700 7870 18700 1000]
ElementLine[7870 18700 9840 18700 1000]
ElementLine[9840 18700 9840 -18700 1000]
ElementLine[9840 -18700 7870 -18700 1000]
ElementLine[7870 -18700 1960 -18700 1000]
ElementLine[1960 -18700 -1960 -18700 1000]
ElementLine[-1960 -18700 -7870 -18700 1000]
ElementLine[-7870 -18700 -9840 -18700 1000]
ElementLine[-9840 -18700 -9840 -20660 500]
ElementLine[-9840 -20660 -7870 -20660 500]
ElementLine[-7870 -20660 -7870 -18700 500]
ElementLine[9840 -18700 9840 -20660 500]
ElementLine[9840 -20660 7870 -20660 500]
ElementLine[7870 -20660 7870 -18700 500]
ElementLine[-9840 18700 -9840 20660 500]
ElementLine[-9840 20660 -7870 20660 500]
ElementLine[-7870 20660 -7870 18700 500]
ElementLine[9840 18700 9840 20660 500]
ElementLine[9840 20660 7870 20660 500]
ElementLine[7870 20660 7870 18700 500]
ElementLine[-1960 -18700 -1960 -20660 500]
ElementLine[-1960 -20660 1960 -20660 500]
ElementLine[1960 -20660 1960 -18700 500]
ElementLine[-1960 18700 -1960 20660 500]
ElementLine[-1960 20660 1960 20660 500]
ElementLine[1960 20660 1960 18700 500]
ElementArc[0 0 5558 5558 0 360 250]
ElementArc[0 0 3606 3606 0 360 250]
Pin[0 0 6600 2000 7400 4000 "" "P$1" ""]
Pin[0 -10000 6600 2000 7400 4000 "" "P$2" ""]
Pin[0 10000 6600 2000 7400 4000 "" "P$3" ""]
)
