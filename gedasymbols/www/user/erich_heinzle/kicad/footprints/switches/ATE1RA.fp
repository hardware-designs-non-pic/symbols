# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module ATE1RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: ATE1RA
# Text descriptor count: 1
# Draw segment object count: 21
# Draw circle object count: 0
# Draw arc object count: 5
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -22800 1000 1 100 ""]
(
ElementLine[-16730 -15000 16730 -15000 1000]
ElementLine[18700 -1220 18700 16490 1000]
ElementLine[-18700 16490 -18700 -1220 1000]
ElementLine[16730 -15000 16730 -1220 1000]
ElementLine[16730 -1220 18700 -1220 1000]
ElementLine[-16730 -15000 -16730 -1220 1000]
ElementLine[-16730 -1220 -18700 -1220 1000]
ElementLine[-7870 17480 -7870 31250 1000]
ElementLine[7870 17480 7870 31250 1000]
ElementLine[-6880 32240 -4920 32240 1000]
ElementLine[-4920 32240 1960 32240 1000]
ElementLine[1960 32240 6880 32240 1000]
ElementLine[-7870 17480 7870 17480 500]
ElementLine[-4920 33220 -8850 48970 500]
ElementLine[1960 33220 0 49960 500]
ElementLine[-4920 33220 -4920 32240 500]
ElementLine[1960 33220 1960 32240 500]
ElementLine[-18700 -7120 -18700 -1220 500]
ElementLine[18700 -7120 18700 -1220 500]
ElementLine[-18700 16490 18700 16490 1000]
ElementLine[-16530 -1180 16920 -1180 500]
ElementArc[-9590 18460 1986 1986 0 360 500]
ElementArc[9590 18460 1980 1980 0 360 500]
ElementArc[-6880 31250 990 990 0 360 1000]
ElementArc[6880 31250 990 990 0 360 1000]
ElementArc[-4420 49460 4448 4448 0 360 500]
Pin[0 -15000 6600 2000 7400 4000 "" "P$1" ""]
Pin[-10000 -15000 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -15000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 15000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 15000 6600 2000 7400 4000 "" "P$5" ""]
)
