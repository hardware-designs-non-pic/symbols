# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-PVA2R
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-PVA2R
# Text descriptor count: 1
# Draw segment object count: 37
# Draw circle object count: 2
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 37500 0 0 100 ""]
(
ElementLine[20000 -17500 20000 17500 600]
ElementLine[-20000 17500 -20000 -17500 600]
ElementLine[20000 -17500 10000 -17500 600]
ElementLine[10000 -20000 10000 -17500 600]
ElementLine[-20000 -17500 -10000 -17500 600]
ElementLine[-10000 -20000 -10000 -17500 600]
ElementLine[-10000 -20000 -2500 -20000 600]
ElementLine[-20000 17500 -10000 17500 600]
ElementLine[-10000 20000 -10000 17500 600]
ElementLine[20000 17500 10000 17500 600]
ElementLine[10000 20000 10000 17500 600]
ElementLine[10000 20000 2500 20000 600]
ElementLine[-2500 20000 -2500 17500 600]
ElementLine[-2500 20000 -10000 20000 600]
ElementLine[-2500 17500 2500 17500 600]
ElementLine[2500 17500 2500 20000 600]
ElementLine[2500 20000 -2500 20000 600]
ElementLine[-2500 -20000 -2500 -17500 600]
ElementLine[-2500 -20000 2500 -20000 600]
ElementLine[-2500 -17500 2500 -17500 600]
ElementLine[2500 -17500 2500 -20000 600]
ElementLine[2500 -20000 10000 -20000 600]
ElementLine[-12500 -15000 12500 -15000 600]
ElementLine[-12500 15000 12500 15000 600]
ElementLine[-12500 13500 -12500 15000 600]
ElementLine[-7500 -10000 -7500 7500 600]
ElementLine[-7500 7500 -7500 10000 600]
ElementLine[-7500 10000 7500 10000 600]
ElementLine[7500 10000 7500 7500 600]
ElementLine[7500 7500 7500 -10000 600]
ElementLine[7500 -10000 -7500 -10000 600]
ElementLine[-12500 -15000 -12500 -13500 600]
ElementLine[12500 13500 12500 15000 600]
ElementLine[12500 -15000 12500 -13500 600]
ElementLine[-5000 0 5000 0 600]
ElementLine[0 5000 0 -5000 600]
ElementLine[-7500 7500 7500 7500 600]
ElementArc[0 0 10324 10324 0 360 300]
ElementArc[0 0 8839 8839 0 360 300]
Pin[-11800 -9800 5200 2000 6000 3200 "" "1" ""]
Pad[-11800 -11400 -11800 -8200 5200 2000 6000 "" "1" ""]
Pad[-11800 -11400 -11800 -8200 5200 2000 6000 "" "1" ",onsolder"]
Pin[-11800 0 5200 2000 6000 3200 "" "2" ""]
Pad[-11800 -1600 -11800 1600 5200 2000 6000 "" "2" ""]
Pad[-11800 -1600 -11800 1600 5200 2000 6000 "" "2" ",onsolder"]
Pin[-11800 9800 5200 2000 6000 3200 "" "3" ""]
Pad[-11800 8200 -11800 11400 5200 2000 6000 "" "3" ""]
Pad[-11800 8200 -11800 11400 5200 2000 6000 "" "3" ",onsolder"]
Pin[11800 -9800 5200 2000 6000 3200 "" "4" ""]
Pad[11800 -11400 11800 -8200 5200 2000 6000 "" "4" ""]
Pad[11800 -11400 11800 -8200 5200 2000 6000 "" "4" ",onsolder"]
Pin[11800 0 5200 2000 6000 3200 "" "5" ""]
Pad[11800 -1600 11800 1600 5200 2000 6000 "" "5" ""]
Pad[11800 -1600 11800 1600 5200 2000 6000 "" "5" ",onsolder"]
Pin[11800 9800 5200 2000 6000 3200 "" "6" ""]
Pad[11800 8200 11800 11400 5200 2000 6000 "" "6" ""]
Pad[11800 8200 11800 11400 5200 2000 6000 "" "6" ",onsolder"]
)
