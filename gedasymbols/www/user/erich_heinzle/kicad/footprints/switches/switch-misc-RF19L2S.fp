# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-RF19L2S
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-RF19L2S
# Text descriptor count: 1
# Draw segment object count: 58
# Draw circle object count: 0
# Draw arc object count: 8
# Pad count: 7
#
Element["" ">NAME" "" "" 0 0 -25000 -42500 0 100 ""]
(
ElementLine[-37500 37500 37500 37500 600]
ElementLine[37500 37500 37500 -37500 600]
ElementLine[37500 -37500 -37500 -37500 600]
ElementLine[-37500 -37500 -37500 37500 600]
ElementLine[-25000 30000 25000 30000 600]
ElementLine[30000 25000 30000 -25000 600]
ElementLine[25000 -30000 -25000 -30000 600]
ElementLine[-30000 -25000 -30000 25000 600]
ElementLine[-2500 32500 -2500 35000 600]
ElementLine[-2500 35000 2500 35000 600]
ElementLine[2500 35000 2500 32500 600]
ElementLine[2500 32500 25000 32500 600]
ElementLine[-2500 32500 -25000 32500 600]
ElementLine[32500 25000 32500 2500 600]
ElementLine[32500 2500 35000 2500 600]
ElementLine[35000 2500 35000 -2500 600]
ElementLine[35000 -2500 32500 -2500 600]
ElementLine[32500 -2500 32500 -25000 600]
ElementLine[25000 -32500 2500 -32500 600]
ElementLine[2500 -32500 2500 -35000 600]
ElementLine[2500 -35000 -2500 -35000 600]
ElementLine[-2500 -35000 -2500 -32500 600]
ElementLine[-2500 -32500 -25000 -32500 600]
ElementLine[-32500 -25000 -32500 -2500 600]
ElementLine[-32500 -2500 -35000 -2500 600]
ElementLine[-35000 -2500 -35000 2500 600]
ElementLine[-35000 2500 -32500 2500 600]
ElementLine[-32500 2500 -32500 25000 600]
ElementLine[20000 -20000 20000 -12500 600]
ElementLine[22500 -17500 17500 -17500 600]
ElementLine[17500 -17500 20000 -12500 600]
ElementLine[20000 -12500 20000 -10000 600]
ElementLine[20000 -12500 22500 -17500 600]
ElementLine[17500 -12500 20000 -12500 600]
ElementLine[20000 -12500 22500 -12500 600]
ElementLine[23500 -17000 27000 -20500 600]
ElementLine[27000 -20500 25000 -19500 600]
ElementLine[25000 -19500 26000 -18500 600]
ElementLine[26000 -18500 27000 -20500 600]
ElementLine[23500 -13500 27000 -17000 600]
ElementLine[27000 -17000 25000 -16000 600]
ElementLine[25000 -16000 26000 -15000 600]
ElementLine[26000 -15000 27000 -17000 600]
ElementLine[-20000 -20000 -20000 -12500 600]
ElementLine[-17500 -17500 -22500 -17500 600]
ElementLine[-22500 -17500 -20000 -12500 600]
ElementLine[-20000 -12500 -20000 -10000 600]
ElementLine[-20000 -12500 -17500 -17500 600]
ElementLine[-22500 -12500 -20000 -12500 600]
ElementLine[-20000 -12500 -17500 -12500 600]
ElementLine[-16500 -17000 -13000 -20500 600]
ElementLine[-13000 -20500 -15000 -19500 600]
ElementLine[-15000 -19500 -14000 -18500 600]
ElementLine[-14000 -18500 -13000 -20500 600]
ElementLine[-16500 -13500 -13000 -17000 600]
ElementLine[-13000 -17000 -15000 -16000 600]
ElementLine[-15000 -16000 -14000 -15000 600]
ElementLine[-14000 -15000 -13000 -17000 600]
ElementArc[25000 25000 5000 5000 0 360 600]
ElementArc[25000 -25000 5000 5000 0 360 600]
ElementArc[-25000 -25000 5000 5000 0 360 600]
ElementArc[-25000 25000 5000 5000 0 360 600]
ElementArc[25000 25000 7500 7500 0 360 600]
ElementArc[25000 -25000 7500 7500 0 360 600]
ElementArc[-25000 -25000 7500 7500 0 360 600]
ElementArc[-25000 25000 7500 7500 0 360 600]
Pin[2500 -10000 6600 2000 7400 4400 "" "1" ""]
Pin[10000 -2500 6600 2000 7400 4400 "" "2" ""]
Pin[7000 25000 6600 2000 7400 4400 "" "3" ""]
Pad[7000 21700 7000 28300 6600 2000 7400 "" "3" ""]
Pad[7000 21700 7000 28300 6600 2000 7400 "" "3" ",onsolder"]
Pin[0 0 6600 2000 7400 4400 "" "4" ""]
Pin[-2500 10000 6600 2000 7400 4400 "" "5" ""]
Pin[-10000 2500 6600 2000 7400 4400 "" "6" ""]
Pin[-7000 -25000 6600 2000 7400 4400 "" "7" ""]
Pad[-7000 -28300 -7000 -21700 6600 2000 7400 "" "7" ""]
Pad[-7000 -28300 -7000 -21700 6600 2000 7400 "" "7" ",onsolder"]
)
