# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module EST2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: EST2
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 5
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -16570 -220 1 100 ""]
(
ElementLine[-11100 -16290 10940 -16290 1000]
ElementLine[10940 -16290 10940 -4480 1000]
ElementLine[10940 -4480 10940 4960 1000]
ElementLine[10940 4960 10940 16770 1000]
ElementLine[10940 16770 -11100 16770 1000]
ElementLine[-11100 16770 -11100 4960 1000]
ElementLine[-11100 4960 -11100 -4480 1000]
ElementLine[-11100 -4480 -11100 -16290 1000]
ElementLine[-11100 -4480 -9920 -4480 500]
ElementLine[-11100 4960 -9920 4960 500]
ElementLine[10940 4960 9760 4960 500]
ElementLine[9760 -4480 10940 -4480 500]
ElementLine[-4800 -6060 -4010 -550 500]
ElementLine[5030 -5660 4250 -550 500]
ElementLine[-8740 -6450 -8740 -13930 500]
ElementLine[-8740 -13930 8580 -13930 500]
ElementLine[8580 -13930 8580 -6450 500]
ElementLine[8580 6920 8580 14010 500]
ElementLine[8580 14010 -8740 14010 500]
ElementLine[-8740 14010 -8740 6920 500]
ElementArc[-70 430 10824 10824 0 360 500]
ElementArc[-70 30 10833 10833 0 360 500]
ElementArc[110 -550 4140 4140 0 360 500]
ElementArc[110 -5860 4914 4914 0 360 500]
ElementArc[110 230 7986 7986 0 360 500]
Pin[5000 -10000 6600 2000 7400 4000 "" "P$1" ""]
Pin[5000 0 6600 2000 7400 4000 "" "P$2" ""]
Pin[5000 10000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-5000 -10000 6600 2000 7400 4000 "" "P$4" ""]
Pin[-5000 10000 6600 2000 7400 4000 "" "P$5" ""]
Pin[-5000 0 6600 2000 7400 4000 "" "P$6" ""]
)
