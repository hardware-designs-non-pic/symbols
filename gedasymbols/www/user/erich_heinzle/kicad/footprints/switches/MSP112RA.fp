# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module MSP112RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: MSP112RA
# Text descriptor count: 1
# Draw segment object count: 52
# Draw circle object count: 0
# Draw arc object count: 5
# Pad count: 4
#
Element["" ">NAME" "" "" 0 0 -16670 -5050 1 100 ""]
(
ElementLine[-12990 -12630 -11810 -12630 1000]
ElementLine[-11810 -12630 -8260 -12630 1000]
ElementLine[-8260 -12630 8260 -12630 1000]
ElementLine[8260 -12630 11810 -12630 1000]
ElementLine[11810 -12630 12990 -12630 1000]
ElementLine[12990 -12630 12990 7040 1000]
ElementLine[-12990 7040 -12990 -12630 1000]
ElementLine[-11810 8220 -11020 8220 1000]
ElementLine[-11020 8220 11020 8220 1000]
ElementLine[11020 8220 11810 8220 1000]
ElementLine[-11020 8220 -11020 14920 1000]
ElementLine[-11020 14920 -10230 15700 1000]
ElementLine[-10230 15700 10230 15700 1000]
ElementLine[10230 15700 11020 14920 1000]
ElementLine[11020 14920 11020 8220 1000]
ElementLine[-390 24760 390 24760 500]
ElementLine[-1180 21220 -780 21220 500]
ElementLine[-780 21220 780 21220 500]
ElementLine[780 21220 1180 21220 500]
ElementLine[-2360 17280 -2360 19640 500]
ElementLine[-2360 19640 -2360 20030 500]
ElementLine[-2360 20030 -1180 21220 500]
ElementLine[2360 17280 2360 19640 500]
ElementLine[2360 19640 2360 20030 500]
ElementLine[2360 20030 1180 21220 500]
ElementLine[-2360 19640 2360 19640 500]
ElementLine[-11810 -12630 -11810 -16960 500]
ElementLine[-11810 -16960 -11020 -16960 500]
ElementLine[-11020 -16960 -8260 -16960 500]
ElementLine[-8260 -16960 8260 -16960 500]
ElementLine[8260 -16960 11810 -16960 500]
ElementLine[11810 -16960 11810 -12630 500]
ElementLine[-8260 -16960 -8260 -12630 500]
ElementLine[8260 -16960 8260 -12630 500]
ElementLine[-11020 -16960 -11020 -18540 500]
ElementLine[-11020 -18540 -4720 -18540 500]
ElementLine[-4720 -18540 4720 -18540 500]
ElementLine[4720 -18540 11020 -18540 500]
ElementLine[11020 -18540 11020 -17360 500]
ElementLine[-4720 -18540 -4720 -30740 500]
ElementLine[-4720 -30740 4720 -30740 500]
ElementLine[4720 -30740 4720 -18540 500]
ElementLine[-780 21220 -780 35390 500]
ElementLine[-780 35390 780 35390 500]
ElementLine[780 35390 780 21220 500]
ElementLine[780 21220 -390 21220 500]
ElementLine[-390 21220 -390 35000 500]
ElementLine[-390 35000 0 35000 500]
ElementLine[0 35000 390 35000 500]
ElementLine[390 35000 390 21610 500]
ElementLine[390 21610 0 21610 500]
ElementLine[0 21610 0 35000 500]
ElementArc[-11810 7040 1180 1180 0 360 1000]
ElementArc[12000 7240 1010 1010 0 360 1000]
ElementArc[-3930 17280 1580 1580 0 360 500]
ElementArc[3930 17280 1570 1570 0 360 500]
ElementArc[0 -24640 7713 7713 0 360 500]
Pin[-10000 -15000 7600 2000 8400 5000 "" "P$1" ""]
Pin[10000 -15000 7600 2000 8400 5000 "" "P$2" ""]
Pin[0 25000 7600 2000 8400 5000 "" "P$3" ""]
Pin[0 35000 7600 2000 8400 5000 "" "P$4" ""]
)
