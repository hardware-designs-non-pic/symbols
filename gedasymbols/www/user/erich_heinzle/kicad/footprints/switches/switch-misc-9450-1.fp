# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-9450-1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-9450-1
# Text descriptor count: 1
# Draw segment object count: 18
# Draw circle object count: 2
# Draw arc object count: 15
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -500 -13500 0 100 ""]
(
ElementLine[-14000 9000 14000 9000 600]
ElementLine[14000 -9000 14000 9000 600]
ElementLine[14000 -9000 -14000 -9000 600]
ElementLine[-14000 9000 -14000 -9000 600]
ElementLine[-3000 7500 3000 7500 600]
ElementLine[12500 -7500 12500 1500 600]
ElementLine[12500 -7500 -12500 -7500 600]
ElementLine[-12500 1500 -12500 -7500 600]
ElementLine[3000 7500 7000 7500 600]
ElementLine[-3000 7500 -7000 7500 600]
ElementLine[-7000 7500 -12500 7500 600]
ElementLine[-12500 7500 -12500 1500 600]
ElementLine[7000 7500 12500 7500 600]
ElementLine[12500 7500 12500 1500 600]
ElementLine[8400 -5000 11600 -5000 500]
ElementLine[10000 -3400 10000 -6600 500]
ElementLine[-11600 -5000 -8400 -5000 500]
ElementLine[-10000 -3400 -10000 -6600 500]
ElementArc[10000 -5000 1131 1131 0 360 500]
ElementArc[-10000 -5000 1131 1131 0 360 500]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
Pin[-10000 5000 5200 2000 6000 3200 "" "1" ""]
Pad[-10000 2400 -10000 7600 5200 2000 6000 "" "1" ""]
Pad[-10000 2400 -10000 7600 5200 2000 6000 "" "1" ",onsolder"]
Pin[0 5000 5200 2000 6000 3200 "" "2" ""]
Pad[0 2400 0 7600 5200 2000 6000 "" "2" ""]
Pad[0 2400 0 7600 5200 2000 6000 "" "2" ",onsolder"]
Pin[10000 5000 5200 2000 6000 3200 "" "3" ""]
Pad[10000 2400 10000 7600 5200 2000 6000 "" "3" ""]
Pad[10000 2400 10000 7600 5200 2000 6000 "" "3" ",onsolder"]
)
