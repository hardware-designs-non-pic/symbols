# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module TME1
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TME1
# Text descriptor count: 1
# Draw segment object count: 53
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -490 -19030 0 100 ""]
(
ElementLine[-12590 -14560 -9840 -14560 1000]
ElementLine[-9840 -14560 9440 -14560 1000]
ElementLine[9440 -14560 12590 -14560 1000]
ElementLine[12590 -14560 12590 -11020 1000]
ElementLine[12590 -11020 12590 -9050 1000]
ElementLine[12590 -9050 12590 -7870 1000]
ElementLine[12590 -7870 12590 -780 1000]
ElementLine[12590 -780 12590 9050 1000]
ElementLine[12590 9050 12590 11020 1000]
ElementLine[12590 11020 12590 14560 1000]
ElementLine[12590 14560 9840 14560 1000]
ElementLine[9840 14560 -9840 14560 1000]
ElementLine[-9840 14560 -12590 14560 1000]
ElementLine[-12590 14560 -12590 11020 1000]
ElementLine[-12590 11020 -12590 9050 1000]
ElementLine[-12590 9050 -12590 7870 1000]
ElementLine[-12590 7870 -12590 780 1000]
ElementLine[-12590 780 -12590 -780 1000]
ElementLine[-12590 -780 -12590 -9050 1000]
ElementLine[-12590 -9050 -12590 -11020 1000]
ElementLine[-12590 -11020 -12590 -14560 1000]
ElementLine[9840 14560 9840 11020 500]
ElementLine[9840 11020 12590 11020 500]
ElementLine[-12590 11020 -9840 11020 500]
ElementLine[-9840 11020 -9840 14560 500]
ElementLine[-12590 -11020 -9840 -11020 500]
ElementLine[-9840 -11020 -9840 -14560 500]
ElementLine[12590 -11020 9440 -11020 500]
ElementLine[9440 -11020 9440 -14560 500]
ElementLine[-12590 -11020 -14960 -11020 500]
ElementLine[-14960 -11020 -14960 -9050 500]
ElementLine[-14960 -9050 -12590 -9050 500]
ElementLine[-12590 11020 -14960 11020 500]
ElementLine[-14960 11020 -14960 9050 500]
ElementLine[-14960 9050 -12590 9050 500]
ElementLine[-14960 -780 -14960 780 500]
ElementLine[-14960 780 -12590 780 500]
ElementLine[-14960 -780 -12590 -780 500]
ElementLine[12590 -11020 14960 -11020 500]
ElementLine[14960 -11020 14960 -9050 500]
ElementLine[14960 -9050 12590 -9050 500]
ElementLine[12590 11020 14960 11020 500]
ElementLine[14960 11020 14960 9050 500]
ElementLine[14960 9050 12590 9050 500]
ElementLine[12590 -780 14960 -780 500]
ElementLine[14960 -780 14960 780 500]
ElementLine[14960 780 12990 780 500]
ElementLine[12590 -9050 11810 -9050 500]
ElementLine[11810 -9050 11810 -7870 500]
ElementLine[11810 -7870 12590 -7870 500]
ElementLine[-12590 9050 -11810 9050 500]
ElementLine[-11810 9050 -11810 7870 500]
ElementLine[-11810 7870 -12590 7870 500]
ElementArc[0 0 4172 4172 0 360 250]
Pin[-15000 -10000 6600 2000 7400 3200 "" "P$1" ""]
Pin[-15000 0 6600 2000 7400 3200 "" "P$2" ""]
Pin[-15000 10000 6600 2000 7400 3200 "" "P$3" ""]
Pin[15000 -10000 6600 2000 7400 3200 "" "P$4" ""]
Pin[15000 0 6600 2000 7400 3200 "" "P$5" ""]
Pin[15000 10000 6600 2000 7400 3200 "" "P$6" ""]
)
