# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module FT2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: FT2
# Text descriptor count: 1
# Draw segment object count: 22
# Draw circle object count: 2
# Draw arc object count: 3
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 -1270 -13120 0 100 ""]
(
ElementLine[-13770 -9050 -10620 -9050 1000]
ElementLine[-10620 -9050 10230 -9050 1000]
ElementLine[10230 -9050 13770 -9050 1000]
ElementLine[13770 -9050 13770 -3930 1000]
ElementLine[13770 -3930 13770 3930 1000]
ElementLine[13770 3930 13770 9050 1000]
ElementLine[13770 9050 10230 9050 1000]
ElementLine[10230 9050 -10620 9050 1000]
ElementLine[-10620 9050 -13770 9050 1000]
ElementLine[-13770 9050 -13770 3930 1000]
ElementLine[-13770 3930 -13770 -3930 1000]
ElementLine[-13770 -3930 -13770 -9050 1000]
ElementLine[1960 -3540 -5900 -3540 500]
ElementLine[1960 3540 -5900 3540 500]
ElementLine[-10620 -9050 -10620 9050 500]
ElementLine[10230 -9050 10230 9050 500]
ElementLine[-13770 -3930 -11410 -3930 500]
ElementLine[-11410 -3930 -11410 3930 500]
ElementLine[-11410 3930 -13770 3930 500]
ElementLine[13770 -3930 11020 -3930 500]
ElementLine[11020 -3930 11020 3930 500]
ElementLine[11020 3930 13770 3930 500]
ElementArc[-5900 0 2503 2503 0 360 250]
ElementArc[-5900 0 1103 1103 0 360 250]
ElementArc[0 0 7916 7916 0 360 500]
ElementArc[1770 0 3545 3545 0 360 500]
ElementArc[0 0 3561 3561 0 360 500]
Pin[0 -5110 6600 2000 7400 4000 "" "P$1" ""]
Pin[-10000 -5110 6600 2000 7400 4000 "" "P$2" ""]
Pin[10000 -5110 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 4880 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 4880 6600 2000 7400 4000 "" "P$5" ""]
Pin[0 5000 6600 2000 7400 4000 "" "P$6" ""]
)
