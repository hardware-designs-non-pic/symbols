# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module TR2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: TR2
# Text descriptor count: 1
# Draw segment object count: 8
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 7
#
Element["" ">NAME" "" "" 0 0 -7180 -23750 0 100 ""]
(
ElementLine[-14760 -14760 14760 -14760 500]
ElementLine[14760 -14760 14760 14760 500]
ElementLine[14760 14760 -14760 14760 500]
ElementLine[-14760 14760 -14760 -14760 500]
ElementLine[-19680 -19680 19680 -19680 1000]
ElementLine[19680 -19680 19680 19680 1000]
ElementLine[19680 19680 -19680 19680 1000]
ElementLine[-19680 19680 -19680 -19680 1000]
ElementArc[0 0 4172 4172 0 360 250]
Pin[0 -10000 5600 2000 6400 3200 "" "P$1" ""]
Pin[-10000 -10000 5600 2000 6400 3200 "" "P$2" ""]
Pin[10000 -10000 5600 2000 6400 3200 "" "P$3" ""]
Pin[-10000 10000 5600 2000 6400 3200 "" "P$4" ""]
Pin[10000 10000 5600 2000 6400 3200 "" "P$5" ""]
Pin[-10000 0 5600 2000 6400 3200 "" "P$6" ""]
Pin[10000 0 5600 2000 6400 3200 "" "P$7" ""]
)
