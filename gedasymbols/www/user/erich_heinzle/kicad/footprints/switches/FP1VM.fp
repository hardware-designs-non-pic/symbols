# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module FP1VM
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: FP1VM
# Text descriptor count: 1
# Draw segment object count: 80
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -14310 -3830 1 100 ""]
(
ElementLine[-8930 -12790 -8930 3740 1000]
ElementLine[-8930 3740 -8930 6100 1000]
ElementLine[-8930 6100 -8930 8460 1000]
ElementLine[-8930 8460 -7750 8460 1000]
ElementLine[-7750 8460 -4210 8460 1000]
ElementLine[-4210 8460 4050 8460 1000]
ElementLine[4050 8460 7990 8460 1000]
ElementLine[7990 8460 9170 8460 1000]
ElementLine[9170 8460 9170 6100 1000]
ElementLine[9170 6100 9170 3740 1000]
ElementLine[9170 3740 9170 -12790 1000]
ElementLine[9170 -12790 -6570 -12790 1000]
ElementLine[-6570 -12790 -8930 -12790 1000]
ElementLine[-7750 8460 -7360 8460 500]
ElementLine[-7360 8460 -7360 13970 500]
ElementLine[-7360 13970 -6570 14760 500]
ElementLine[-6570 14760 7200 14760 500]
ElementLine[7200 14760 7990 13970 500]
ElementLine[7990 13970 7990 8460 500]
ElementLine[-7360 13970 7990 13970 500]
ElementLine[-6570 -35620 -6570 -12790 500]
ElementLine[-6570 -12790 -3420 -12790 1000]
ElementLine[-3420 -12790 -3420 -35620 500]
ElementLine[-3420 -35620 -6180 -35620 500]
ElementLine[-6570 -35620 -6180 -35620 500]
ElementLine[-6180 -35620 -6180 -13180 500]
ElementLine[-6180 -13180 -3810 -13180 500]
ElementLine[-3810 -13180 -3810 -35230 500]
ElementLine[-3810 -35230 -5780 -35230 500]
ElementLine[-5780 -35230 -5780 -13580 500]
ElementLine[-5780 -13580 -4210 -13580 500]
ElementLine[-4210 -13580 -4210 -34840 500]
ElementLine[-4210 -34840 -5390 -34840 500]
ElementLine[-5390 -34840 -5390 -13970 500]
ElementLine[-5390 -13970 -4600 -13970 500]
ElementLine[-4600 -13970 -4600 -34440 500]
ElementLine[-4600 -34440 -5000 -34440 500]
ElementLine[-5000 -34440 -5000 -14370 500]
ElementLine[-8930 6100 -4210 6100 500]
ElementLine[-4210 6100 4050 6100 500]
ElementLine[4050 6100 9170 6100 500]
ElementLine[-8930 3740 -6570 3740 500]
ElementLine[-6570 3740 3460 3740 500]
ElementLine[3460 3740 6610 3740 500]
ElementLine[6610 3740 9170 3740 500]
ElementLine[-6570 3740 -6570 5700 500]
ElementLine[-6570 5700 -3420 5700 500]
ElementLine[-3420 5700 -3420 4130 500]
ElementLine[-3420 4130 -6180 4130 500]
ElementLine[-6180 4130 -6180 5310 500]
ElementLine[-6180 5310 -3810 5310 500]
ElementLine[-3810 5310 -3810 4520 500]
ElementLine[-3810 4520 -5780 4520 500]
ElementLine[-5780 4520 -5780 4920 500]
ElementLine[-5780 4920 -4210 4920 500]
ElementLine[3460 3740 3460 5900 500]
ElementLine[3460 5900 6610 5900 500]
ElementLine[6610 5900 6610 3740 500]
ElementLine[6610 3740 3850 3740 500]
ElementLine[3850 3740 3850 5700 500]
ElementLine[3850 5700 6220 5700 500]
ElementLine[6220 5700 6220 4130 500]
ElementLine[6220 4130 4250 4130 500]
ElementLine[4250 4130 4250 5310 500]
ElementLine[4250 5310 5820 5310 500]
ElementLine[5820 5310 5820 4520 500]
ElementLine[5820 4520 4640 4520 500]
ElementLine[4640 4520 4640 4920 500]
ElementLine[4640 4920 5430 4920 500]
ElementLine[-4210 6100 -4210 8460 500]
ElementLine[4050 6100 4050 8460 500]
ElementLine[-9440 3540 -11020 3540 500]
ElementLine[-11020 3540 -11020 6290 500]
ElementLine[-11020 6290 -8660 6290 500]
ElementLine[9050 3540 11020 3540 500]
ElementLine[11020 3540 11020 6290 500]
ElementLine[11020 6290 9440 6290 500]
ElementLine[-3540 14960 -3540 27950 500]
ElementLine[-3540 27950 4330 27950 500]
ElementLine[4330 27950 4330 14960 500]
Pin[-5000 -25000 6600 2000 7400 4000 "" "P$1" ""]
Pin[-5000 -35000 6600 2000 7400 4000 "" "P$2" ""]
Pin[-5000 -15000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-5000 5000 6600 2000 7400 4000 "" "P$4" ""]
Pin[5000 5000 6600 2000 7400 4000 "" "P$5" ""]
)
