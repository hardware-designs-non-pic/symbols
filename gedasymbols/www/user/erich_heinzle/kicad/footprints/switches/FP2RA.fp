# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module FP2RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: FP2RA
# Text descriptor count: 1
# Draw segment object count: 75
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" ">NAME" "" "" 0 0 -17460 -4270 1 100 ""]
(
ElementLine[-13770 -13030 13770 -13030 500]
ElementLine[13770 -13030 13770 2710 500]
ElementLine[13770 2710 13770 8220 500]
ElementLine[13770 8220 11810 8220 500]
ElementLine[11810 8220 7870 8220 500]
ElementLine[7870 8220 -7870 8220 500]
ElementLine[-7870 8220 -11810 8220 500]
ElementLine[-11810 8220 -13770 8220 500]
ElementLine[-7870 8220 -7870 13740 500]
ElementLine[-7870 13740 -7080 14520 500]
ElementLine[-7080 14520 7080 14520 500]
ElementLine[7080 14520 7870 13740 500]
ElementLine[7870 13740 7870 8220 500]
ElementLine[-7870 13740 7870 13740 500]
ElementLine[-13770 8220 -13770 2710 500]
ElementLine[-13770 2710 -13770 -13030 500]
ElementLine[-11810 8220 -11810 3110 500]
ElementLine[-11810 3110 -11810 2710 500]
ElementLine[-11810 2710 -13770 2710 500]
ElementLine[13770 2710 11810 2710 500]
ElementLine[11810 2710 11810 3110 500]
ElementLine[11810 3110 11810 8220 500]
ElementLine[-11810 3110 11810 3110 500]
ElementLine[-11410 -25860 -11410 -13260 500]
ElementLine[-11410 -25860 -8660 -25860 500]
ElementLine[-8660 -25860 -8660 -13260 500]
ElementLine[-8660 -13260 -11020 -13260 500]
ElementLine[-11020 -13260 -11020 -25470 500]
ElementLine[-11020 -25470 -9050 -25470 500]
ElementLine[-9050 -25470 -9050 -13660 500]
ElementLine[-9050 -13660 -10620 -13660 500]
ElementLine[-10620 -13660 -10620 -25070 500]
ElementLine[-10620 -25070 -9440 -25070 500]
ElementLine[-9440 -25070 -9440 -14050 500]
ElementLine[-9440 -14050 -10230 -14050 500]
ElementLine[-10230 -14050 -10230 -24680 500]
ElementLine[-10230 -24680 -9840 -24680 500]
ElementLine[-9840 -24680 -9840 -14440 500]
ElementLine[-1570 -25860 -1570 -13260 500]
ElementLine[-1570 -13260 1570 -13260 500]
ElementLine[1570 -13260 1570 -25860 500]
ElementLine[1570 -25860 -1180 -25860 500]
ElementLine[-1570 -25860 -1180 -25860 500]
ElementLine[-1180 -25860 -1180 -13660 500]
ElementLine[-1180 -13660 1180 -13660 500]
ElementLine[1180 -13660 1180 -25470 500]
ElementLine[1180 -25470 -780 -25470 500]
ElementLine[-780 -25470 -780 -14050 500]
ElementLine[-780 -14050 780 -14050 500]
ElementLine[780 -14050 780 -25070 500]
ElementLine[780 -25070 -390 -25070 500]
ElementLine[-390 -25070 -390 -14440 500]
ElementLine[-390 -14440 390 -14440 500]
ElementLine[390 -14440 390 -24680 500]
ElementLine[390 -24680 0 -24680 500]
ElementLine[0 -24680 0 -14840 500]
ElementLine[8660 -25860 8660 -13260 500]
ElementLine[8660 -13260 11410 -13260 500]
ElementLine[11410 -13260 11410 -25860 500]
ElementLine[11410 -25860 8660 -25860 500]
ElementLine[8660 -25860 9050 -25860 500]
ElementLine[9050 -25860 9050 -13660 500]
ElementLine[9050 -13660 11020 -13660 500]
ElementLine[11020 -13660 11020 -25470 500]
ElementLine[11020 -25470 9440 -25470 500]
ElementLine[9440 -25470 9440 -14050 500]
ElementLine[9440 -14050 10620 -14050 500]
ElementLine[10620 -14050 10620 -25070 500]
ElementLine[10620 -25070 9840 -25070 500]
ElementLine[9840 -25070 9840 -14440 500]
ElementLine[9840 -14440 10230 -14440 500]
ElementLine[10230 -14440 10230 -24680 500]
ElementLine[-3930 14560 -3930 27550 500]
ElementLine[-3930 27550 3930 27550 500]
ElementLine[3930 27550 3930 14560 500]
Pin[0 -15000 6600 2000 7400 4000 "" "P$1" ""]
Pin[10000 -15000 6600 2000 7400 4000 "" "P$2" ""]
Pin[-10000 -15000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 5000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 5000 6600 2000 7400 4000 "" "P$5" ""]
Pin[-10000 -25000 6600 2000 7400 4000 "" "P$6" ""]
Pin[0 -25000 6600 2000 7400 4000 "" "P$7" ""]
Pin[10000 -25000 6600 2000 7400 4000 "" "P$8" ""]
)
