# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-DB1C-D2LC
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-DB1C-D2LC
# Text descriptor count: 1
# Draw segment object count: 34
# Draw circle object count: 3
# Draw arc object count: 2
# Pad count: 3
#
Element["" ">NAME" "" "" 0 0 -47500 -27500 1 100 ""]
(
ElementLine[28340 -3930 30700 -3930 260]
ElementLine[30700 -3930 30700 -10620 260]
ElementLine[28340 -10620 30700 -10620 260]
ElementLine[28340 -3930 28340 -10620 260]
ElementLine[-1180 -3930 1180 -3930 260]
ElementLine[1180 -3930 1180 -10620 260]
ElementLine[-1180 -10620 1180 -10620 260]
ElementLine[-1180 -3930 -1180 -10620 260]
ElementLine[-30700 -3930 -28340 -3930 260]
ElementLine[-28340 -3930 -28340 -10620 260]
ElementLine[-30700 -10620 -28340 -10620 260]
ElementLine[-30700 -3930 -30700 -10620 260]
ElementLine[-18770 -10740 -39250 -10740 500]
ElementLine[39250 -10740 -18850 -10740 500]
ElementLine[39250 -10740 39250 -46650 500]
ElementLine[-39250 -10740 -39250 -45660 500]
ElementLine[-38220 -46180 -550 -46180 500]
ElementLine[38740 -47160 20470 -47160 500]
ElementLine[19400 -48740 17710 -48740 500]
ElementLine[17710 -48740 12990 -48740 500]
ElementLine[12990 -48740 550 -48740 500]
ElementLine[19400 -48740 20470 -47160 500]
ElementLine[550 -48740 -550 -46180 500]
ElementLine[38740 -47160 39250 -46650 500]
ElementLine[-38220 -46180 -39250 -45660 500]
ElementLine[-46450 -63770 35070 -51810 1200]
ElementLine[35390 -51810 34090 -43140 500]
ElementLine[26370 -53030 25070 -44400 500]
ElementLine[17710 -51410 17710 -48740 500]
ElementLine[12990 -51410 12990 -48740 500]
ElementLine[15430 -20010 22120 -20010 500]
ElementLine[18770 -16660 18770 -23360 500]
ElementLine[-22120 -20010 -15430 -20010 500]
ElementLine[-18770 -16660 -18770 -23360 500]
ElementArc[29600 -43850 2333 2333 0 360 250]
ElementArc[18770 -20010 2362 2362 0 360 500]
ElementArc[-18770 -20010 2362 2362 0 360 500]
ElementArc[15350 -51410 2360 2360 0 360 500]
ElementArc[29580 -43750 4555 4555 0 360 500]
Pin[29520 0 7670 2000 8470 5110 "" "C" ""]
Pin[-29520 0 7670 2000 8470 5110 "" "NC" ""]
Pin[0 0 7670 2000 8470 5110 "" "NO" ""]
)
