# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-RF15L2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-RF15L2
# Text descriptor count: 1
# Draw segment object count: 62
# Draw circle object count: 0
# Draw arc object count: 4
# Pad count: 7
#
Element["" ">NAME" "" "" 0 0 -17500 -35000 0 100 ""]
(
ElementLine[-30000 30000 30000 30000 600]
ElementLine[30000 30000 30000 -30000 600]
ElementLine[30000 -30000 -30000 -30000 600]
ElementLine[-30000 -30000 -30000 30000 600]
ElementLine[22500 17500 22500 -17500 600]
ElementLine[-22500 -17500 -22500 17500 600]
ElementLine[-22500 25000 -25000 27500 600]
ElementLine[-25000 27500 -27500 25000 600]
ElementLine[-27500 25000 -25000 22500 600]
ElementLine[-25000 22500 -25000 -22500 600]
ElementLine[-25000 -22500 -27500 -25000 600]
ElementLine[-27500 -25000 -25000 -27500 600]
ElementLine[-25000 -27500 -22500 -25000 600]
ElementLine[22500 -25000 25000 -27500 600]
ElementLine[25000 -27500 27500 -25000 600]
ElementLine[27500 -25000 25000 -22500 600]
ElementLine[25000 -22500 25000 22500 600]
ElementLine[25000 22500 27500 25000 600]
ElementLine[27500 25000 25000 27500 600]
ElementLine[25000 27500 22500 25000 600]
ElementLine[17500 22500 12500 22500 600]
ElementLine[12500 22500 2500 22500 600]
ElementLine[2500 22500 -17500 22500 600]
ElementLine[22500 25000 12500 25000 600]
ElementLine[12500 25000 2500 25000 600]
ElementLine[2500 25000 -22500 25000 600]
ElementLine[-2500 -25000 22500 -25000 600]
ElementLine[-2500 -25000 -12500 -25000 600]
ElementLine[-12500 -25000 -22500 -25000 600]
ElementLine[-2500 -22500 17500 -22500 600]
ElementLine[-17500 -22500 -12500 -22500 600]
ElementLine[-12500 -22500 -2500 -22500 600]
ElementLine[15000 -17500 15000 -10000 600]
ElementLine[17500 -15000 12500 -15000 600]
ElementLine[12500 -15000 15000 -10000 600]
ElementLine[15000 -10000 15000 -7500 600]
ElementLine[15000 -10000 17500 -15000 600]
ElementLine[12500 -10000 15000 -10000 600]
ElementLine[15000 -10000 17500 -10000 600]
ElementLine[18500 -14500 22000 -18000 600]
ElementLine[22000 -18000 20000 -17000 600]
ElementLine[20000 -17000 21000 -16000 600]
ElementLine[21000 -16000 22000 -18000 600]
ElementLine[18500 -11000 22000 -14500 600]
ElementLine[22000 -14500 20000 -13500 600]
ElementLine[20000 -13500 21000 -12500 600]
ElementLine[21000 -12500 22000 -14500 600]
ElementLine[-17500 -17500 -17500 -10000 600]
ElementLine[-15000 -15000 -20000 -15000 600]
ElementLine[-20000 -15000 -17500 -10000 600]
ElementLine[-17500 -10000 -17500 -7500 600]
ElementLine[-17500 -10000 -15000 -15000 600]
ElementLine[-20000 -10000 -17500 -10000 600]
ElementLine[-17500 -10000 -15000 -10000 600]
ElementLine[-14000 -14500 -10500 -18000 600]
ElementLine[-10500 -18000 -12500 -17000 600]
ElementLine[-12500 -17000 -11500 -16000 600]
ElementLine[-11500 -16000 -10500 -18000 600]
ElementLine[-14000 -11000 -10500 -14500 600]
ElementLine[-10500 -14500 -12500 -13500 600]
ElementLine[-12500 -13500 -11500 -12500 600]
ElementLine[-11500 -12500 -10500 -14500 600]
ElementArc[17500 -17500 5000 5000 0 360 600]
ElementArc[17500 17500 5000 5000 0 360 600]
ElementArc[-17500 17500 5000 5000 0 360 600]
ElementArc[-17500 -17500 5000 5000 0 360 600]
Pin[0 0 6000 2000 6800 4000 "" "1" ""]
Pin[-7000 -25000 6000 2000 6800 4000 "" "2" ""]
Pad[-7000 -28000 -7000 -22000 6000 2000 6800 "" "2" ""]
Pad[-7000 -28000 -7000 -22000 6000 2000 6800 "" "2" ",onsolder"]
Pin[7000 25000 6000 2000 6800 4000 "" "3" ""]
Pad[7000 22000 7000 28000 6000 2000 6800 "" "3" ""]
Pad[7000 22000 7000 28000 6000 2000 6800 "" "3" ",onsolder"]
Pin[2500 -10000 6000 2000 6800 4000 "" "D1+" ""]
Pin[10000 -2500 6000 2000 6800 4000 "" "D1-" ""]
Pin[-2500 10000 6000 2000 6800 4000 "" "D2+" ""]
Pin[-10000 2500 6000 2000 6800 4000 "" "D2-" ""]
)
