# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module switch-misc-9450-2
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: switch-misc-9450-2
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 16
# Pad count: 6
#
Element["" ">NAME" "" "" 0 0 0 -13500 0 100 ""]
(
ElementLine[-14000 9000 14000 9000 600]
ElementLine[14000 -9000 14000 9000 600]
ElementLine[14000 -9000 -14000 -9000 600]
ElementLine[-14000 9000 -14000 -9000 600]
ElementLine[7000 7500 12500 7500 600]
ElementLine[12500 -2000 12500 2000 600]
ElementLine[12500 -7500 7000 -7500 600]
ElementLine[-12500 2000 -12500 -2000 600]
ElementLine[12500 7500 12500 2000 600]
ElementLine[12500 -2000 12500 -7500 600]
ElementLine[7000 -7500 3500 -7500 600]
ElementLine[7000 7500 3500 7500 600]
ElementLine[-3500 7500 -7000 7500 600]
ElementLine[-3500 -7500 -7000 -7500 600]
ElementLine[3500 7500 -3500 7500 600]
ElementLine[-7000 7500 -12500 7500 600]
ElementLine[-12500 7500 -12500 2000 600]
ElementLine[-12500 -2000 -12500 -7500 600]
ElementLine[-12500 -7500 -7000 -7500 600]
ElementLine[3500 -7500 -3500 -7500 600]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 6491 6491 0 360 600]
ElementArc[0 0 7998 7998 0 360 600]
ElementArc[0 0 7992 7992 0 360 600]
ElementArc[0 0 7999 7999 0 360 600]
ElementArc[0 0 7998 7998 0 360 600]
Pin[-10000 5000 5200 2000 6000 3200 "" "1" "blah"]
Pad[-10000 3100 -10000 6900 5200 2000 6000 "" "1" "square"]
Pad[-10000 3100 -10000 6900 5200 2000 6000 "" "1" "square,onsolder"]
Pin[0 5000 5200 2000 6000 3200 "" "2" ""]
Pad[0 3350 0 6650 5200 2000 6000 "" "2" ""]
Pad[0 3350 0 6650 5200 2000 6000 "" "2" ",onsolder"]
Pin[10000 5000 5200 2000 6000 3200 "" "3" ""]
Pad[10000 3350 10000 6650 5200 2000 6000 "" "3" ""]
Pad[10000 3350 10000 6650 5200 2000 6000 "" "3" ",onsolder"]
Pin[-10000 -5000 5200 2000 6000 3200 "" "4" ""]
Pad[-10000 -6650 -10000 -3350 5200 2000 6000 "" "4" ""]
Pad[-10000 -6650 -10000 -3350 5200 2000 6000 "" "4" ",onsolder"]
Pin[0 -5000 5200 2000 6000 3200 "" "5" ""]
Pad[0 -6650 0 -3350 5200 2000 6000 "" "5" ""]
Pad[0 -6650 0 -3350 5200 2000 6000 "" "5" ",onsolder"]
Pin[10000 -5000 5200 2000 6000 3200 "" "6" ""]
Pad[10000 -6650 10000 -3350 5200 2000 6000 "" "6" ""]
Pad[10000 -6650 10000 -3350 5200 2000 6000 "" "6" ",onsolder"]
)
