# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module converted from Eagle library by oshec.org
# All rights reserved by original authors
# No warranties express or implied
# Footprint converted from Kicad Module FP1RA
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: FP1RA
# Text descriptor count: 1
# Draw segment object count: 73
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" ">NAME" "" "" 0 0 -17460 -4270 1 100 ""]
(
ElementLine[-13770 -13030 13770 -13030 500]
ElementLine[13770 -13030 13770 2710 500]
ElementLine[13770 2710 13770 8220 500]
ElementLine[13770 8220 11810 8220 500]
ElementLine[11810 8220 7870 8220 500]
ElementLine[7870 8220 -7870 8220 500]
ElementLine[-7870 8220 -11810 8220 500]
ElementLine[-11810 8220 -13770 8220 500]
ElementLine[-7870 8220 -7870 13740 500]
ElementLine[-7870 13740 -7080 14520 500]
ElementLine[-7080 14520 7080 14520 500]
ElementLine[7080 14520 7870 13740 500]
ElementLine[7870 13740 7870 8220 500]
ElementLine[-7870 13740 7870 13740 500]
ElementLine[-13770 8220 -13770 2710 500]
ElementLine[-13770 2710 -13770 -13030 500]
ElementLine[-11810 8220 -11810 3110 500]
ElementLine[-11810 3110 -11810 2710 500]
ElementLine[-11810 2710 -13770 2710 500]
ElementLine[13770 2710 11810 2710 500]
ElementLine[11810 2710 11810 3110 500]
ElementLine[11810 3110 11810 8220 500]
ElementLine[-11810 3110 11810 3110 500]
ElementLine[-11410 -15620 -8660 -15620 500]
ElementLine[-8660 -15620 -8660 -13260 500]
ElementLine[-8660 -13260 -11410 -13260 500]
ElementLine[-11410 -13260 -11410 -15620 500]
ElementLine[-11410 -15620 -11020 -15620 500]
ElementLine[-11020 -15620 -11020 -13660 500]
ElementLine[-11020 -13660 -10620 -13660 500]
ElementLine[-10620 -13660 -9050 -13660 500]
ElementLine[-9050 -13660 -9050 -15230 500]
ElementLine[-9050 -15230 -10620 -15230 500]
ElementLine[-10620 -15230 -10620 -13660 500]
ElementLine[-10620 -13660 -9440 -13660 500]
ElementLine[-9440 -13660 -9440 -14840 500]
ElementLine[-9440 -14840 -10230 -14840 500]
ElementLine[-10230 -14840 -10230 -14050 500]
ElementLine[-10230 -14050 -9840 -14050 500]
ElementLine[-9840 -14050 -9840 -14440 500]
ElementLine[-1570 -15620 -1570 -13260 500]
ElementLine[-1570 -13260 1570 -13260 500]
ElementLine[1570 -13260 1570 -15620 500]
ElementLine[1570 -15620 -1570 -15620 500]
ElementLine[-1570 -15620 -1180 -15620 500]
ElementLine[-1180 -15620 -1180 -13660 500]
ElementLine[-1180 -13660 1180 -13660 500]
ElementLine[1180 -13660 1180 -15230 500]
ElementLine[1180 -15230 -780 -15230 500]
ElementLine[-780 -15230 -780 -14050 500]
ElementLine[-780 -14050 780 -14050 500]
ElementLine[780 -14050 780 -14840 500]
ElementLine[780 -14840 -390 -14840 500]
ElementLine[-390 -14840 -390 -14440 500]
ElementLine[-390 -14440 390 -14440 500]
ElementLine[8660 -15620 8660 -13260 500]
ElementLine[8660 -13260 11410 -13260 500]
ElementLine[11410 -13260 11410 -15620 500]
ElementLine[11410 -15620 9050 -15620 500]
ElementLine[8660 -15620 9050 -15620 500]
ElementLine[9050 -15620 9050 -13660 500]
ElementLine[9050 -13660 11020 -13660 500]
ElementLine[11020 -13660 11020 -15230 500]
ElementLine[11020 -15230 9440 -15230 500]
ElementLine[9440 -15230 9440 -14050 500]
ElementLine[9440 -14050 10620 -14050 500]
ElementLine[10620 -14050 10620 -14840 500]
ElementLine[10620 -14840 9840 -14840 500]
ElementLine[9840 -14840 9840 -14440 500]
ElementLine[9840 -14440 10230 -14440 500]
ElementLine[-3930 14560 -3930 27550 500]
ElementLine[-3930 27550 3930 27550 500]
ElementLine[3930 27550 3930 14560 500]
Pin[0 -15000 6600 2000 7400 4000 "" "P$1" ""]
Pin[10000 -15000 6600 2000 7400 4000 "" "P$2" ""]
Pin[-10000 -15000 6600 2000 7400 4000 "" "P$3" ""]
Pin[-10000 5000 6600 2000 7400 4000 "" "P$4" ""]
Pin[10000 5000 6600 2000 7400 4000 "" "P$5" ""]
)
