# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Elko_vert_DM10_RM5to7-5_CopperClear
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Elko_vert_DM10_RM5to7-5_CopperClear
# Text descriptor count: 1
# Draw segment object count: 40
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" "C" "" "" 0 0 0 -25000 0 100 ""]
(
ElementLine[-19090 -5710 -19490 -4330 1500]
ElementLine[-19490 -4330 -19880 -2360 1500]
ElementLine[-19880 -2360 -20080 390 1500]
ElementLine[-20080 390 -19690 2760 1500]
ElementLine[-19690 2760 -19290 5120 1500]
ElementLine[-19290 5120 -18310 7870 1500]
ElementLine[-18310 7870 -17130 10430 1500]
ElementLine[-17130 10430 -15350 12800 1500]
ElementLine[-15350 12800 -12600 15550 1500]
ElementLine[-12600 15550 -9060 17910 1500]
ElementLine[-9060 17910 -4720 19490 1500]
ElementLine[-4720 19490 -1770 19880 1500]
ElementLine[-1770 19880 1770 19880 1500]
ElementLine[1770 19880 4530 19490 1500]
ElementLine[4530 19490 8270 18310 1500]
ElementLine[8270 18310 11020 16730 1500]
ElementLine[11020 16730 13780 14370 1500]
ElementLine[13780 14370 16140 12010 1500]
ElementLine[19490 -4530 18700 -7280 1500]
ElementLine[18700 -7280 17520 -9840 1500]
ElementLine[17520 -9840 15750 -12400 1500]
ElementLine[15750 -12400 14170 -14170 1500]
ElementLine[14170 -14170 12010 -15940 1500]
ElementLine[12010 -15940 10240 -17130 1500]
ElementLine[10240 -17130 7680 -18500 1500]
ElementLine[7680 -18500 5310 -19290 1500]
ElementLine[5310 -19290 2760 -19880 1500]
ElementLine[2760 -19880 -200 -20080 1500]
ElementLine[-200 -20080 -2760 -19880 1500]
ElementLine[-2760 -19880 -5910 -19090 1500]
ElementLine[-5910 -19090 -9250 -17720 1500]
ElementLine[-9250 -17720 -11810 -16140 1500]
ElementLine[-11810 -16140 -14170 -13980 1500]
ElementLine[-14170 -13980 -15940 -12200 1500]
ElementLine[19290 5510 19690 3740 1500]
ElementLine[19690 3740 20080 980 1500]
ElementLine[20080 980 19880 -1970 1500]
ElementLine[19880 -1970 19490 -4530 1500]
ElementLine[-8000 -14000 -8000 -10000 1500]
ElementLine[-10000 -12000 -6000 -12000 1500]
Pin[13390 6690 7870 2000 8670 3940 "" "2" ""]
Pin[-13390 -6690 7870 2000 8670 3940 "" "1" ""]
Pin[10000 0 7870 2000 8670 3940 "" "2" ""]
Pin[-10000 0 7870 2000 8670 3940 "" "1" ""]
)
