# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Elko_vert_DM5_RM2_CopperClear
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Elko_vert_DM5_RM2_CopperClear
# Text descriptor count: 1
# Draw segment object count: 4
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "C" "" "" 0 0 0 -15000 0 100 ""]
(
ElementLine[-3150 -6300 -3150 -5120 1500]
ElementLine[-3150 -6300 -1970 -6300 1500]
ElementLine[-4330 -6300 -3150 -6300 1500]
ElementLine[-3150 -6300 -3150 -7480 1500]
ElementArc[0 0 9840 9840 0 360 1500]
Pin[-3940 0 5830 2000 6630 3150 "" "1" ""]
Pin[3940 0 5830 2000 6630 3150 "" "2" ""]
)
