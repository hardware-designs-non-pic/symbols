# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Elko_vert_DM14_RM5_CopperClear
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Elko_vert_DM14_RM5_CopperClear
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "C" "" "" 0 0 0 -35000 0 100 ""]
(
ElementLine[-16000 -12000 -8000 -12000 1500]
ElementLine[-12000 -16000 -12000 -8000 1500]
ElementArc[0 0 28000 28000 0 360 1500]
Pin[10000 0 11810 2000 12610 4720 "" "2" ""]
Pin[-10000 0 11810 2000 12610 4720 "" "1" ""]
)
