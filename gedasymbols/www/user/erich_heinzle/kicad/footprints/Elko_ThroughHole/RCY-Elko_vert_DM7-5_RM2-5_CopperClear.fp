# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Bernd Wiebus
# No warranties express or implied
# Footprint converted from Kicad Module Elko_vert_DM7-5_RM2-5_CopperClear
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Footprint = module name: Elko_vert_DM7-5_RM2-5_CopperClear
# Text descriptor count: 1
# Draw segment object count: 2
# Draw circle object count: 1
# Draw arc object count: 0
# Pad count: 2
#
Element["" "C" "" "" 0 0 0 -20000 0 100 ""]
(
ElementLine[-7000 -10000 -7000 -5000 1500]
ElementLine[-9500 -7500 -5000 -7500 1500]
ElementArc[0 0 15000 15000 0 360 1500]
Pin[5000 0 5910 2000 6710 3150 "" "2" ""]
Pin[-5000 0 5910 2000 6710 3150 "" "1" ""]
)
