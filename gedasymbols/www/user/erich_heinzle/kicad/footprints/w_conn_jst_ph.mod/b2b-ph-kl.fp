# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b2b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b2b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 21
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-390 9060 -390 7090 1500]
ElementLine[-390 7090 390 7090 1500]
ElementLine[390 7090 390 9060 1500]
ElementLine[11810 -1970 9840 -1970 1500]
ElementLine[9840 3150 11810 3150 1500]
ElementLine[-9840 3150 -11810 3150 1500]
ElementLine[-9840 -1970 -11810 -1970 1500]
ElementLine[-1970 -6690 -1970 -4720 1500]
ElementLine[-1970 -4720 -9840 -4720 1500]
ElementLine[-9840 -4720 -9840 9060 1500]
ElementLine[-9840 9060 9840 9060 1500]
ElementLine[9840 9060 9840 -4720 1500]
ElementLine[9840 -4720 1970 -4720 1500]
ElementLine[1970 -4720 1970 -6690 1500]
ElementLine[-11810 -6690 -11810 11020 1500]
ElementLine[11810 -6690 11810 11020 1500]
ElementLine[-5120 -6690 -5120 -7480 1500]
ElementLine[-5120 -7480 -6300 -7480 1500]
ElementLine[-6300 -7480 -6300 -6690 1500]
ElementLine[11810 -6690 -11810 -6690 1500]
ElementLine[-11810 11020 11810 11020 1500]
Pin[-3940 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_2_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_2_" ",onsolder"]
)
