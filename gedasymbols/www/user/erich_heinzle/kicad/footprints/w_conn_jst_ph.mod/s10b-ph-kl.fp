# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s10b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s10b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-43310 24800 43310 24800 1500]
ElementLine[33460 7870 -33460 7870 1500]
ElementLine[-43310 1570 43310 1570 1500]
ElementLine[-36610 16140 -36610 24800 1500]
ElementLine[-36610 24800 -37790 24800 1500]
ElementLine[-37790 24800 -37790 16140 1500]
ElementLine[33460 7870 33460 24800 1500]
ElementLine[-33460 7870 -33460 24800 1500]
ElementLine[36600 16140 36600 9840 1500]
ElementLine[40540 16140 36600 16140 1500]
ElementLine[40540 9840 40540 16140 1500]
ElementLine[36600 9840 40540 9840 1500]
ElementLine[-40550 9840 -36610 9840 1500]
ElementLine[-36610 9840 -36610 16140 1500]
ElementLine[-36610 16140 -40550 16140 1500]
ElementLine[-40550 16140 -40550 9840 1500]
ElementLine[42510 -5510 42510 1570 1500]
ElementLine[-42500 -5510 -42500 1570 1500]
ElementLine[43300 24800 43300 -5510 1500]
ElementLine[43300 -5510 41730 -5510 1500]
ElementLine[41730 -5510 41730 1570 1500]
ElementLine[-41720 1570 -41720 -5510 1500]
ElementLine[-41720 -5510 -43290 -5510 1500]
ElementLine[-43290 -5510 -43290 24800 1500]
Pin[-35430 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-27570 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-27570 -985 -27570 985 4720 2000 5520 "0" "_2_" ""]
Pad[-27570 -985 -27570 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-19690 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_3_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-11820 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_4_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_5_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[3930 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[3930 -985 3930 985 4720 2000 5520 "0" "_6_" ""]
Pad[3930 -985 3930 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_7_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[19680 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_8_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[27560 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_9_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[35430 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_10_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_10_" ",onsolder"]
)
