# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s8b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s8b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-25590 7870 25590 7870 1500]
ElementLine[35430 24800 -35430 24800 1500]
ElementLine[-35430 1570 35430 1570 1500]
ElementLine[-28740 16140 -28740 24800 1500]
ElementLine[-28740 24800 -29920 24800 1500]
ElementLine[-29920 24800 -29920 16140 1500]
ElementLine[25590 7870 25590 24800 1500]
ElementLine[-25590 7870 -25590 24800 1500]
ElementLine[28730 16140 28730 9840 1500]
ElementLine[32670 16140 28730 16140 1500]
ElementLine[32670 9840 32670 16140 1500]
ElementLine[28730 9840 32670 9840 1500]
ElementLine[-32680 9840 -28740 9840 1500]
ElementLine[-28740 9840 -28740 16140 1500]
ElementLine[-28740 16140 -32680 16140 1500]
ElementLine[-32680 16140 -32680 9840 1500]
ElementLine[34640 -5510 34640 1570 1500]
ElementLine[-34630 -5510 -34630 1570 1500]
ElementLine[35430 24800 35430 -5510 1500]
ElementLine[35430 -5510 33860 -5510 1500]
ElementLine[33860 -5510 33860 1570 1500]
ElementLine[-33850 1570 -33850 -5510 1500]
ElementLine[-33850 -5510 -35420 -5510 1500]
ElementLine[-35420 -5510 -35420 24800 1500]
Pin[-27560 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-19690 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_2_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-11810 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_3_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_4_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_5_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_6_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[19690 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_7_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[27560 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_8_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_8_" ",onsolder"]
)
