# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b12b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b12b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 51
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 12
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[390 7090 390 9060 1500]
ElementLine[-390 7090 390 7090 1500]
ElementLine[-390 9060 -390 7090 1500]
ElementLine[-51180 11020 51180 11020 1500]
ElementLine[49210 9060 -49210 9060 1500]
ElementLine[-51180 -6690 51180 -6690 1500]
ElementLine[7480 9060 7480 7090 1500]
ElementLine[7480 7090 8260 7090 1500]
ElementLine[8260 7090 8260 9060 1500]
ElementLine[-8260 9060 -8260 7090 1500]
ElementLine[-8260 7090 -7480 7090 1500]
ElementLine[-7480 7090 -7480 9060 1500]
ElementLine[16140 7090 16140 9060 1500]
ElementLine[15360 7090 16140 7090 1500]
ElementLine[15360 9060 15360 7090 1500]
ElementLine[23230 9060 23230 7090 1500]
ElementLine[23230 7090 24010 7090 1500]
ElementLine[24010 7090 24010 9060 1500]
ElementLine[38970 9060 38970 7090 1500]
ElementLine[38970 7090 39750 7090 1500]
ElementLine[39750 7090 39750 9060 1500]
ElementLine[-16170 9060 -16170 7090 1500]
ElementLine[-16170 7090 -15390 7090 1500]
ElementLine[-15390 7090 -15390 9060 1500]
ElementLine[31880 7090 31880 9060 1500]
ElementLine[31100 7090 31880 7090 1500]
ElementLine[31100 9060 31100 7090 1500]
ElementLine[-31130 7090 -31130 9060 1500]
ElementLine[-31910 7090 -31130 7090 1500]
ElementLine[-31910 9060 -31910 7090 1500]
ElementLine[-39010 7090 -39010 9060 1500]
ElementLine[-39790 7090 -39010 7090 1500]
ElementLine[-39790 9060 -39790 7090 1500]
ElementLine[-24040 9060 -24040 7090 1500]
ElementLine[-24040 7090 -23260 7090 1500]
ElementLine[-23260 7090 -23260 9060 1500]
ElementLine[51180 -1970 49210 -1970 1500]
ElementLine[49210 3150 51180 3150 1500]
ElementLine[-49240 3150 -51210 3150 1500]
ElementLine[-49240 -1970 -51210 -1970 1500]
ElementLine[-41370 -6690 -41370 -4720 1500]
ElementLine[-41370 -4720 -49240 -4720 1500]
ElementLine[-49240 -4720 -49240 9060 1500]
ElementLine[49210 9060 49210 -4720 1500]
ElementLine[49210 -4720 41340 -4720 1500]
ElementLine[41340 -4720 41340 -6690 1500]
ElementLine[-51210 -6690 -51210 11020 1500]
ElementLine[51180 -6690 51180 11020 1500]
ElementLine[-44520 -6690 -44520 -7480 1500]
ElementLine[-44520 -7480 -45700 -7480 1500]
ElementLine[-45700 -7480 -45700 -6690 1500]
Pin[3930 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[3930 -985 3930 985 4720 2000 5520 "0" "_7_" ""]
Pad[3930 -985 3930 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_6_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-11820 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_5_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-43340 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-43340 -985 -43340 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-43340 -985 -43340 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-27580 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_3_" ""]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-35460 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-35460 -985 -35460 985 4720 2000 5520 "0" "_2_" ""]
Pad[-35460 -985 -35460 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-19710 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-19710 -985 -19710 985 4720 2000 5520 "0" "_4_" ""]
Pad[-19710 -985 -19710 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[11800 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[11800 -985 11800 985 4720 2000 5520 "0" "_8_" ""]
Pad[11800 -985 11800 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[19680 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_9_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[27550 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[27550 -985 27550 985 4720 2000 5520 "0" "_10_" ""]
Pad[27550 -985 27550 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[35430 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_11_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[43310 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_12_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_12_" ",onsolder"]
)
