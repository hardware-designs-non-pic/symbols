# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s10b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s10b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 12
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[33460 23620 -33460 23620 1500]
ElementLine[-47240 27560 47240 27560 1500]
ElementLine[47240 3940 -47240 3940 1500]
ElementLine[-47270 27560 -47270 -1970 1500]
ElementLine[47250 -1970 47250 27560 1500]
ElementLine[47250 -1970 45670 -1970 1500]
ElementLine[46460 3940 46460 -1970 1500]
ElementLine[45670 -1970 45670 3940 1500]
ElementLine[-46480 -1970 -46480 3940 1500]
ElementLine[-45690 3940 -45690 -1970 1500]
ElementLine[-45690 -1970 -47270 -1970 1500]
ElementLine[-33460 27560 -33460 23620 1500]
ElementLine[33470 23620 33470 27550 1500]
ElementLine[-39380 3940 -39380 21650 1500]
ElementLine[-39380 21650 -42140 21650 1500]
ElementLine[-42140 21650 -42140 3940 1500]
ElementLine[39370 3940 39370 21650 1500]
ElementLine[39370 21650 42130 21650 1500]
ElementLine[42130 21650 42130 3940 1500]
Pad[-35440 -4925 -35440 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-27560 -4925 -27560 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-44680 18885 -44680 26355 5900 2000 6700 "0" "__" "square"]
Pad[44680 18885 44680 26355 5900 2000 6700 "0" "__" "square"]
Pad[-19690 -4925 -19690 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-11810 -4925 -11810 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-3950 -4925 -3950 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[3940 -4925 3940 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[11800 -4925 11800 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[19680 -4925 19680 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[27550 -4925 27550 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[35430 -4925 35430 4925 3920 2000 4720 "0" "_10_" "square"]
)
