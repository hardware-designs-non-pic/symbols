# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s14b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s14b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 16
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[49210 23620 -49210 23620 1500]
ElementLine[-62990 27560 62990 27560 1500]
ElementLine[62990 3940 -62990 3940 1500]
ElementLine[-63010 27560 -63010 -1970 1500]
ElementLine[63000 -1970 63000 27560 1500]
ElementLine[63000 -1970 61420 -1970 1500]
ElementLine[62210 3940 62210 -1970 1500]
ElementLine[61420 -1970 61420 3940 1500]
ElementLine[-62220 -1970 -62220 3940 1500]
ElementLine[-61430 3940 -61430 -1970 1500]
ElementLine[-61430 -1970 -63010 -1970 1500]
ElementLine[-49210 27560 -49210 23620 1500]
ElementLine[49220 23620 49220 27550 1500]
ElementLine[-55130 3940 -55130 21650 1500]
ElementLine[-55130 21650 -57890 21650 1500]
ElementLine[-57890 21650 -57890 3940 1500]
ElementLine[55120 3940 55120 21650 1500]
ElementLine[55120 21650 57880 21650 1500]
ElementLine[57880 21650 57880 3940 1500]
Pad[-51200 -4925 -51200 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-43320 -4925 -43320 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-60430 18885 -60430 26355 5900 2000 6700 "0" "__" "square"]
Pad[60430 18885 60430 26355 5900 2000 6700 "0" "__" "square"]
Pad[-35450 -4925 -35450 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-27570 -4925 -27570 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-19710 -4925 -19710 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[-11820 -4925 -11820 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[-3960 -4925 -3960 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[3920 -4925 3920 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[11790 -4925 11790 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[19670 -4925 19670 4925 3920 2000 4720 "0" "_10_" "square"]
Pad[27540 -4925 27540 4925 3920 2000 4720 "0" "_11_" "square"]
Pad[35430 -4925 35430 4925 3920 2000 4720 "0" "_12_" "square"]
Pad[43300 -4925 43300 4925 3920 2000 4720 "0" "_13_" "square"]
Pad[51180 -4925 51180 4925 3920 2000 4720 "0" "_14_" "square"]
)
