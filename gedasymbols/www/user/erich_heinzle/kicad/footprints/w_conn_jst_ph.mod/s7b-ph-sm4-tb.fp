# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s7b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s7b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 9
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[21650 23620 -21650 23620 1500]
ElementLine[-35430 27560 35430 27560 1500]
ElementLine[35430 3940 -35430 3940 1500]
ElementLine[-35450 27560 -35450 -1970 1500]
ElementLine[35440 -1970 35440 27560 1500]
ElementLine[35440 -1970 33860 -1970 1500]
ElementLine[34650 3940 34650 -1970 1500]
ElementLine[33860 -1970 33860 3940 1500]
ElementLine[-34660 -1970 -34660 3940 1500]
ElementLine[-33870 3940 -33870 -1970 1500]
ElementLine[-33870 -1970 -35450 -1970 1500]
ElementLine[-21650 27560 -21650 23620 1500]
ElementLine[21660 23620 21660 27550 1500]
ElementLine[-27570 3940 -27570 21650 1500]
ElementLine[-27570 21650 -30330 21650 1500]
ElementLine[-30330 21650 -30330 3940 1500]
ElementLine[27560 3940 27560 21650 1500]
ElementLine[27560 21650 30320 21650 1500]
ElementLine[30320 21650 30320 3940 1500]
Pad[-23630 -4925 -23630 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-15750 -4925 -15750 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-32870 18885 -32870 26355 5900 2000 6700 "0" "__" "square"]
Pad[32870 18885 32870 26355 5900 2000 6700 "0" "__" "square"]
Pad[-7880 -4925 -7880 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[0 -4925 0 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[7860 -4925 7860 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[15750 -4925 15750 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[23620 -4925 23620 4925 3920 2000 4720 "0" "_7_" "square"]
)
