# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s11b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s11b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 11
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-37400 7870 37400 7870 1500]
ElementLine[47240 1570 -47240 1570 1500]
ElementLine[-47240 24800 47240 24800 1500]
ElementLine[-40540 16140 -40540 24800 1500]
ElementLine[-40540 24800 -41720 24800 1500]
ElementLine[-41720 24800 -41720 16140 1500]
ElementLine[37390 7870 37390 24800 1500]
ElementLine[-37390 7870 -37390 24800 1500]
ElementLine[40530 16140 40530 9840 1500]
ElementLine[44470 16140 40530 16140 1500]
ElementLine[44470 9840 44470 16140 1500]
ElementLine[40530 9840 44470 9840 1500]
ElementLine[-44480 9840 -40540 9840 1500]
ElementLine[-40540 9840 -40540 16140 1500]
ElementLine[-40540 16140 -44480 16140 1500]
ElementLine[-44480 16140 -44480 9840 1500]
ElementLine[46440 -5510 46440 1570 1500]
ElementLine[-46430 -5510 -46430 1570 1500]
ElementLine[47230 24800 47230 -5510 1500]
ElementLine[47230 -5510 45660 -5510 1500]
ElementLine[45660 -5510 45660 1570 1500]
ElementLine[-45650 1570 -45650 -5510 1500]
ElementLine[-45650 -5510 -47220 -5510 1500]
ElementLine[-47220 -5510 -47220 24800 1500]
Pin[-39360 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-39360 -985 -39360 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-39360 -985 -39360 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-31510 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-31510 -985 -31510 985 4720 2000 5520 "0" "_2_" ""]
Pad[-31510 -985 -31510 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-23630 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_3_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-15760 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_4_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_5_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-10 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-10 -985 -10 985 4720 2000 5520 "0" "_6_" ""]
Pad[-10 -985 -10 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_7_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[15740 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[15740 -985 15740 985 4720 2000 5520 "0" "_8_" ""]
Pad[15740 -985 15740 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_9_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[31490 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[31490 -985 31490 985 4720 2000 5520 "0" "_10_" ""]
Pad[31490 -985 31490 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[39370 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_11_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_11_" ",onsolder"]
)
