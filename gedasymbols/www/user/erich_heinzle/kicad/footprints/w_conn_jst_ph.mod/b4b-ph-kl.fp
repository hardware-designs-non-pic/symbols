# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b4b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b4b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 27
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[390 7090 390 9060 1500]
ElementLine[-390 7090 390 7090 1500]
ElementLine[-390 9060 -390 7090 1500]
ElementLine[-19690 11020 19690 11020 1500]
ElementLine[17720 9060 -17720 9060 1500]
ElementLine[-19690 -6690 19690 -6690 1500]
ElementLine[-7490 7090 -7490 9060 1500]
ElementLine[-8270 7090 -7490 7090 1500]
ElementLine[-8270 9060 -8270 7090 1500]
ElementLine[7480 9060 7480 7090 1500]
ElementLine[7480 7090 8260 7090 1500]
ElementLine[8260 7090 8260 9060 1500]
ElementLine[19690 -1970 17720 -1970 1500]
ElementLine[17720 3150 19690 3150 1500]
ElementLine[-17720 3150 -19690 3150 1500]
ElementLine[-17720 -1970 -19690 -1970 1500]
ElementLine[-9850 -6690 -9850 -4720 1500]
ElementLine[-9850 -4720 -17720 -4720 1500]
ElementLine[-17720 -4720 -17720 9060 1500]
ElementLine[17720 9060 17720 -4720 1500]
ElementLine[17720 -4720 9850 -4720 1500]
ElementLine[9850 -4720 9850 -6690 1500]
ElementLine[-19690 -6690 -19690 11020 1500]
ElementLine[19690 -6690 19690 11020 1500]
ElementLine[-13000 -6690 -13000 -7480 1500]
ElementLine[-13000 -7480 -14180 -7480 1500]
ElementLine[-14180 -7480 -14180 -6690 1500]
Pin[-11820 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_3_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_2_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_4_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_4_" ",onsolder"]
)
