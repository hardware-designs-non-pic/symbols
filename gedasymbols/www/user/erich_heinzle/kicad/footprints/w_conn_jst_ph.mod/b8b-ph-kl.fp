# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b8b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b8b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 39
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-35430 11020 35430 11020 1500]
ElementLine[33460 9060 -33460 9060 1500]
ElementLine[-35430 -6690 35430 -6690 1500]
ElementLine[7480 9060 7480 7090 1500]
ElementLine[7480 7090 8260 7090 1500]
ElementLine[8260 7090 8260 9060 1500]
ElementLine[23220 9060 23220 7090 1500]
ElementLine[23220 7090 24000 7090 1500]
ElementLine[24000 7090 24000 9060 1500]
ElementLine[-410 9060 -410 7090 1500]
ElementLine[-410 7090 370 7090 1500]
ElementLine[370 7090 370 9060 1500]
ElementLine[16130 7090 16130 9060 1500]
ElementLine[15350 7090 16130 7090 1500]
ElementLine[15350 9060 15350 7090 1500]
ElementLine[-15370 7090 -15370 9060 1500]
ElementLine[-16150 7090 -15370 7090 1500]
ElementLine[-16150 9060 -16150 7090 1500]
ElementLine[-23250 7090 -23250 9060 1500]
ElementLine[-24030 7090 -23250 7090 1500]
ElementLine[-24030 9060 -24030 7090 1500]
ElementLine[-8280 9060 -8280 7090 1500]
ElementLine[-8280 7090 -7500 7090 1500]
ElementLine[-7500 7090 -7500 9060 1500]
ElementLine[35430 -1970 33460 -1970 1500]
ElementLine[33460 3150 35430 3150 1500]
ElementLine[-33480 3150 -35450 3150 1500]
ElementLine[-33480 -1970 -35450 -1970 1500]
ElementLine[-25610 -6690 -25610 -4720 1500]
ElementLine[-25610 -4720 -33480 -4720 1500]
ElementLine[-33480 -4720 -33480 9060 1500]
ElementLine[33460 9060 33460 -4720 1500]
ElementLine[33460 -4720 25590 -4720 1500]
ElementLine[25590 -4720 25590 -6690 1500]
ElementLine[-35450 -6690 -35450 11020 1500]
ElementLine[35430 -6690 35430 11020 1500]
ElementLine[-28760 -6690 -28760 -7480 1500]
ElementLine[-28760 -7480 -29940 -7480 1500]
ElementLine[-29940 -7480 -29940 -6690 1500]
Pin[19690 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_7_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[11820 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_6_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_5_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-27580 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-11820 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_3_" ""]
Pad[-11820 -985 -11820 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-19700 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-19700 -985 -19700 985 4720 2000 5520 "0" "_2_" ""]
Pad[-19700 -985 -19700 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-3950 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-3950 -985 -3950 985 4720 2000 5520 "0" "_4_" ""]
Pad[-3950 -985 -3950 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[27560 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_8_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_8_" ",onsolder"]
)
