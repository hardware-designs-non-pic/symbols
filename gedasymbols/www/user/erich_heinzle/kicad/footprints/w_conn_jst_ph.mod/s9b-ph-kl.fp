# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s9b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s9b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 9
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-29530 7870 29530 7870 1500]
ElementLine[39370 24800 -39370 24800 1500]
ElementLine[-39370 1570 39370 1570 1500]
ElementLine[-32680 16140 -32680 24800 1500]
ElementLine[-32680 24800 -33860 24800 1500]
ElementLine[-33860 24800 -33860 16140 1500]
ElementLine[29520 7870 29520 24800 1500]
ElementLine[-29530 7870 -29530 24800 1500]
ElementLine[32660 16140 32660 9840 1500]
ElementLine[36600 16140 32660 16140 1500]
ElementLine[36600 9840 36600 16140 1500]
ElementLine[32660 9840 36600 9840 1500]
ElementLine[-36620 9840 -32680 9840 1500]
ElementLine[-32680 9840 -32680 16140 1500]
ElementLine[-32680 16140 -36620 16140 1500]
ElementLine[-36620 16140 -36620 9840 1500]
ElementLine[38570 -5510 38570 1570 1500]
ElementLine[-38570 -5510 -38570 1570 1500]
ElementLine[39360 24800 39360 -5510 1500]
ElementLine[39360 -5510 37790 -5510 1500]
ElementLine[37790 -5510 37790 1570 1500]
ElementLine[-37790 1570 -37790 -5510 1500]
ElementLine[-37790 -5510 -39360 -5510 1500]
ElementLine[-39360 -5510 -39360 24800 1500]
Pin[-31500 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-23630 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_2_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-15750 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_3_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_4_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_5_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_6_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_7_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_8_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[31500 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_9_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_9_" ",onsolder"]
)
