# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s5b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s5b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 7
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[13780 23620 -13780 23620 1500]
ElementLine[-27560 27560 27560 27560 1500]
ElementLine[27560 3940 -27560 3940 1500]
ElementLine[-27570 27560 -27570 -1970 1500]
ElementLine[27560 -1970 27560 27560 1500]
ElementLine[27560 -1970 25980 -1970 1500]
ElementLine[26770 3940 26770 -1970 1500]
ElementLine[25980 -1970 25980 3940 1500]
ElementLine[-26780 -1970 -26780 3940 1500]
ElementLine[-25990 3940 -25990 -1970 1500]
ElementLine[-25990 -1970 -27570 -1970 1500]
ElementLine[-13780 27560 -13780 23620 1500]
ElementLine[13780 23620 13780 27550 1500]
ElementLine[-19690 3940 -19690 21650 1500]
ElementLine[-19690 21650 -22450 21650 1500]
ElementLine[-22450 21650 -22450 3940 1500]
ElementLine[19680 3940 19680 21650 1500]
ElementLine[19680 21650 22440 21650 1500]
ElementLine[22440 21650 22440 3940 1500]
Pad[-15750 -4925 -15750 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-7870 -4925 -7870 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-24980 18885 -24980 26355 5900 2000 6700 "0" "__" "square"]
Pad[24980 18885 24980 26355 5900 2000 6700 "0" "__" "square"]
Pad[0 -4925 0 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[7870 -4925 7870 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[15750 -4925 15750 4925 3920 2000 4720 "0" "_5_" "square"]
)
