# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s2b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s2b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 20
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-15750 27560 -15750 -1970 1500]
ElementLine[15750 -1970 15750 27560 1500]
ElementLine[15750 -1970 14170 -1970 1500]
ElementLine[14960 3940 14960 -1970 1500]
ElementLine[14170 -1970 14170 3940 1500]
ElementLine[-14960 -1970 -14960 3940 1500]
ElementLine[-14170 3940 -14170 -1970 1500]
ElementLine[-14170 -1970 -15750 -1970 1500]
ElementLine[-1970 27560 -1970 23620 1500]
ElementLine[-1970 23620 1970 23620 1500]
ElementLine[1970 23620 1970 27560 1500]
ElementLine[-7870 3940 -7870 21650 1500]
ElementLine[-7870 21650 -10630 21650 1500]
ElementLine[-10630 21650 -10630 3940 1500]
ElementLine[7870 3940 7870 21650 1500]
ElementLine[7870 21650 10630 21650 1500]
ElementLine[10630 21650 10630 3940 1500]
ElementLine[-15750 27560 15750 27560 1500]
ElementLine[15750 3940 -15750 3940 1500]
ElementLine[-5120 27560 -6300 27560 1500]
Pad[-3940 -4925 -3940 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[3940 -4920 3940 4920 3930 2000 4730 "0" "_2_" "square"]
Pad[-13170 18890 -13170 26370 5900 2000 6700 "0" "__" "square"]
Pad[13180 18895 13180 26365 5900 2000 6700 "0" "__" "square"]
)
