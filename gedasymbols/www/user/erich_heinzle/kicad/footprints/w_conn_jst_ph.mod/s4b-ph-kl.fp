# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s4b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s4b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 4
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-12990 16140 -12990 24800 1500]
ElementLine[-12990 24800 -14170 24800 1500]
ElementLine[-14170 24800 -14170 16140 1500]
ElementLine[9840 7870 -9840 7870 1500]
ElementLine[-19690 24800 19690 24800 1500]
ElementLine[-19690 1570 19690 1570 1500]
ElementLine[9840 7870 9840 24800 1500]
ElementLine[-9850 7870 -9850 24800 1500]
ElementLine[12980 16140 12980 9840 1500]
ElementLine[16920 16140 12980 16140 1500]
ElementLine[16920 9840 16920 16140 1500]
ElementLine[12980 9840 16920 9840 1500]
ElementLine[-16940 9840 -13000 9840 1500]
ElementLine[-13000 9840 -13000 16140 1500]
ElementLine[-13000 16140 -16940 16140 1500]
ElementLine[-16940 16140 -16940 9840 1500]
ElementLine[18890 -5510 18890 1570 1500]
ElementLine[-18890 -5510 -18890 1570 1500]
ElementLine[19680 24800 19680 -5510 1500]
ElementLine[19680 -5510 18110 -5510 1500]
ElementLine[18110 -5510 18110 1570 1500]
ElementLine[-18110 1570 -18110 -5510 1500]
ElementLine[-18110 -5510 -19680 -5510 1500]
ElementLine[-19680 -5510 -19680 24800 1500]
Pin[-11810 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_2_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_3_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_4_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_4_" ",onsolder"]
)
