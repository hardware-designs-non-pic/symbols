# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s7b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s7b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 7
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-24800 16140 -24800 24800 1500]
ElementLine[-24800 24800 -25980 24800 1500]
ElementLine[-25980 24800 -25980 16140 1500]
ElementLine[-21650 7870 21650 7870 1500]
ElementLine[31500 24800 -31500 24800 1500]
ElementLine[-31500 1570 31500 1570 1500]
ElementLine[21650 7870 21650 24800 1500]
ElementLine[-21650 7870 -21650 24800 1500]
ElementLine[24790 16140 24790 9840 1500]
ElementLine[28730 16140 24790 16140 1500]
ElementLine[28730 9840 28730 16140 1500]
ElementLine[24790 9840 28730 9840 1500]
ElementLine[-28740 9840 -24800 9840 1500]
ElementLine[-24800 9840 -24800 16140 1500]
ElementLine[-24800 16140 -28740 16140 1500]
ElementLine[-28740 16140 -28740 9840 1500]
ElementLine[30700 -5510 30700 1570 1500]
ElementLine[-30690 -5510 -30690 1570 1500]
ElementLine[31490 24800 31490 -5510 1500]
ElementLine[31490 -5510 29920 -5510 1500]
ElementLine[29920 -5510 29920 1570 1500]
ElementLine[-29910 1570 -29910 -5510 1500]
ElementLine[-29910 -5510 -31480 -5510 1500]
ElementLine[-31480 -5510 -31480 24800 1500]
Pin[-23620 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-23620 -985 -23620 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-23620 -985 -23620 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-15750 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_2_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-7870 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_3_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_4_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_5_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_6_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_7_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_7_" ",onsolder"]
)
