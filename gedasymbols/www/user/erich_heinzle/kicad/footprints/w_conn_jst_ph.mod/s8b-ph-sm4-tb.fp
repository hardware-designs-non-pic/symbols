# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s8b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s8b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[25590 23620 -25590 23620 1500]
ElementLine[-39370 27560 39370 27560 1500]
ElementLine[39370 3940 -39370 3940 1500]
ElementLine[-39390 27560 -39390 -1970 1500]
ElementLine[39370 -1970 39370 27560 1500]
ElementLine[39370 -1970 37790 -1970 1500]
ElementLine[38580 3940 38580 -1970 1500]
ElementLine[37790 -1970 37790 3940 1500]
ElementLine[-38600 -1970 -38600 3940 1500]
ElementLine[-37810 3940 -37810 -1970 1500]
ElementLine[-37810 -1970 -39390 -1970 1500]
ElementLine[-25600 27560 -25600 23620 1500]
ElementLine[25590 23620 25590 27550 1500]
ElementLine[-31500 3940 -31500 21650 1500]
ElementLine[-31500 21650 -34260 21650 1500]
ElementLine[-34260 21650 -34260 3940 1500]
ElementLine[31490 3940 31490 21650 1500]
ElementLine[31490 21650 34250 21650 1500]
ElementLine[34250 21650 34250 3940 1500]
Pad[-27570 -4925 -27570 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-19690 -4925 -19690 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-36810 18885 -36810 26355 5900 2000 6700 "0" "__" "square"]
Pad[36810 18885 36810 26355 5900 2000 6700 "0" "__" "square"]
Pad[-11820 -4925 -11820 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-3940 -4925 -3940 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[3920 -4925 3920 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[11810 -4925 11810 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[19670 -4925 19670 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[27560 -4925 27560 4925 3920 2000 4720 "0" "_8_" "square"]
)
