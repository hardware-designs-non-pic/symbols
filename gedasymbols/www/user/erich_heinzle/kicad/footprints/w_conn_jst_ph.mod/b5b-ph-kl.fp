# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b5b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b5b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 30
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[23620 -6690 -23620 -6690 1500]
ElementLine[-21650 9060 21650 9060 1500]
ElementLine[23620 11020 -23620 11020 1500]
ElementLine[12190 7090 12190 9060 1500]
ElementLine[11410 7090 12190 7090 1500]
ElementLine[11410 9060 11410 7090 1500]
ElementLine[-3550 7090 -3550 9060 1500]
ElementLine[-4330 7090 -3550 7090 1500]
ElementLine[-4330 9060 -4330 7090 1500]
ElementLine[-11430 7090 -11430 9060 1500]
ElementLine[-12210 7090 -11430 7090 1500]
ElementLine[-12210 9060 -12210 7090 1500]
ElementLine[3540 9060 3540 7090 1500]
ElementLine[3540 7090 4320 7090 1500]
ElementLine[4320 7090 4320 9060 1500]
ElementLine[23620 -1970 21650 -1970 1500]
ElementLine[21650 3150 23620 3150 1500]
ElementLine[-21660 3150 -23630 3150 1500]
ElementLine[-21660 -1970 -23630 -1970 1500]
ElementLine[-13790 -6690 -13790 -4720 1500]
ElementLine[-13790 -4720 -21660 -4720 1500]
ElementLine[-21660 -4720 -21660 9060 1500]
ElementLine[21650 9060 21650 -4720 1500]
ElementLine[21650 -4720 13780 -4720 1500]
ElementLine[13780 -4720 13780 -6690 1500]
ElementLine[-23630 -6690 -23630 11020 1500]
ElementLine[23620 -6690 23620 11020 1500]
ElementLine[-16940 -6690 -16940 -7480 1500]
ElementLine[-16940 -7480 -18120 -7480 1500]
ElementLine[-18120 -7480 -18120 -6690 1500]
Pin[-15760 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_3_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_2_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_4_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_5_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_5_" ",onsolder"]
)
