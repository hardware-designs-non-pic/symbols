# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s9b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s9b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 11
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[29530 23620 -29530 23620 1500]
ElementLine[-43310 27560 43310 27560 1500]
ElementLine[43310 3940 -43310 3940 1500]
ElementLine[-43330 27560 -43330 -1970 1500]
ElementLine[43310 -1970 43310 27560 1500]
ElementLine[43310 -1970 41730 -1970 1500]
ElementLine[42520 3940 42520 -1970 1500]
ElementLine[41730 -1970 41730 3940 1500]
ElementLine[-42540 -1970 -42540 3940 1500]
ElementLine[-41750 3940 -41750 -1970 1500]
ElementLine[-41750 -1970 -43330 -1970 1500]
ElementLine[-29530 27560 -29530 23620 1500]
ElementLine[29530 23620 29530 27550 1500]
ElementLine[-35440 3940 -35440 21650 1500]
ElementLine[-35440 21650 -38200 21650 1500]
ElementLine[-38200 21650 -38200 3940 1500]
ElementLine[35430 3940 35430 21650 1500]
ElementLine[35430 21650 38190 21650 1500]
ElementLine[38190 21650 38190 3940 1500]
Pad[-31500 -4925 -31500 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-23620 -4925 -23620 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-40740 18885 -40740 26355 5900 2000 6700 "0" "__" "square"]
Pad[40740 18885 40740 26355 5900 2000 6700 "0" "__" "square"]
Pad[-15750 -4925 -15750 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-7870 -4925 -7870 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-10 -4925 -10 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[7880 -4925 7880 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[15740 -4925 15740 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[23620 -4925 23620 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[31500 -4925 31500 4925 3920 2000 4720 "0" "_9_" "square"]
)
