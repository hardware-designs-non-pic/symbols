# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s3b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s3b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[5910 23620 -5910 23620 1500]
ElementLine[-19690 3940 19690 3940 1500]
ElementLine[-19690 27560 19690 27560 1500]
ElementLine[-19690 27560 -19690 -1970 1500]
ElementLine[19690 -1970 19690 27560 1500]
ElementLine[19690 -1970 18110 -1970 1500]
ElementLine[18900 3940 18900 -1970 1500]
ElementLine[18110 -1970 18110 3940 1500]
ElementLine[-18900 -1970 -18900 3940 1500]
ElementLine[-18110 3940 -18110 -1970 1500]
ElementLine[-18110 -1970 -19690 -1970 1500]
ElementLine[-5900 27560 -5900 23620 1500]
ElementLine[5910 23620 5910 27550 1500]
ElementLine[-11810 3940 -11810 21650 1500]
ElementLine[-11810 21650 -14570 21650 1500]
ElementLine[-14570 21650 -14570 3940 1500]
ElementLine[11810 3940 11810 21650 1500]
ElementLine[11810 21650 14570 21650 1500]
ElementLine[14570 21650 14570 3940 1500]
Pad[-7870 -4925 -7870 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[0 -4925 0 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-17100 18885 -17100 26355 5900 2000 6700 "0" "__" "square"]
Pad[17100 18885 17100 26355 5900 2000 6700 "0" "__" "square"]
Pad[7870 -4925 7870 4925 3920 2000 4720 "0" "_3_" "square"]
)
