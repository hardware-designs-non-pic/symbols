# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s5b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s5b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 5
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-16930 16140 -16930 24800 1500]
ElementLine[-16930 24800 -18110 24800 1500]
ElementLine[-18110 24800 -18110 16140 1500]
ElementLine[-23620 24800 23620 24800 1500]
ElementLine[-13780 7870 13780 7870 1500]
ElementLine[-23620 1570 23620 1570 1500]
ElementLine[13770 7870 13770 24800 1500]
ElementLine[-13780 7870 -13780 24800 1500]
ElementLine[16910 16140 16910 9840 1500]
ElementLine[20850 16140 16910 16140 1500]
ElementLine[20850 9840 20850 16140 1500]
ElementLine[16910 9840 20850 9840 1500]
ElementLine[-20870 9840 -16930 9840 1500]
ElementLine[-16930 9840 -16930 16140 1500]
ElementLine[-16930 16140 -20870 16140 1500]
ElementLine[-20870 16140 -20870 9840 1500]
ElementLine[22820 -5510 22820 1570 1500]
ElementLine[-22820 -5510 -22820 1570 1500]
ElementLine[23610 24800 23610 -5510 1500]
ElementLine[23610 -5510 22040 -5510 1500]
ElementLine[22040 -5510 22040 1570 1500]
ElementLine[-22040 1570 -22040 -5510 1500]
ElementLine[-22040 -5510 -23610 -5510 1500]
ElementLine[-23610 -5510 -23610 24800 1500]
Pin[-15750 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-7870 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_2_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_3_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_4_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_5_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_5_" ",onsolder"]
)
