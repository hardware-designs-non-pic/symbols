# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s13b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s13b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 15
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-45280 23620 45280 23620 1500]
ElementLine[59060 27560 -59060 27560 1500]
ElementLine[-59060 3940 59060 3940 1500]
ElementLine[-59070 27560 -59070 -1970 1500]
ElementLine[59060 -1970 59060 27560 1500]
ElementLine[59060 -1970 57480 -1970 1500]
ElementLine[58270 3940 58270 -1970 1500]
ElementLine[57480 -1970 57480 3940 1500]
ElementLine[-58280 -1970 -58280 3940 1500]
ElementLine[-57490 3940 -57490 -1970 1500]
ElementLine[-57490 -1970 -59070 -1970 1500]
ElementLine[-45280 27560 -45280 23620 1500]
ElementLine[45290 23620 45290 27550 1500]
ElementLine[-51190 3940 -51190 21650 1500]
ElementLine[-51190 21650 -53950 21650 1500]
ElementLine[-53950 21650 -53950 3940 1500]
ElementLine[51180 3940 51180 21650 1500]
ElementLine[51180 21650 53940 21650 1500]
ElementLine[53940 21650 53940 3940 1500]
Pad[-47260 -4925 -47260 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-39380 -4925 -39380 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-56490 18885 -56490 26355 5900 2000 6700 "0" "__" "square"]
Pad[56490 18885 56490 26355 5900 2000 6700 "0" "__" "square"]
Pad[-31510 -4925 -31510 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-23630 -4925 -23630 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-15770 -4925 -15770 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[-7880 -4925 -7880 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[-20 -4925 -20 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[7860 -4925 7860 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[15730 -4925 15730 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[23610 -4925 23610 4925 3920 2000 4720 "0" "_10_" "square"]
Pad[31480 -4925 31480 4925 3920 2000 4720 "0" "_11_" "square"]
Pad[39370 -4925 39370 4925 3920 2000 4720 "0" "_12_" "square"]
Pad[47240 -4925 47240 4925 3920 2000 4720 "0" "_13_" "square"]
)
