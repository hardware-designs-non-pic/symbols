# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b7b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b7b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 36
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 7
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-31500 -6690 31500 -6690 1500]
ElementLine[29530 9060 -29530 9060 1500]
ElementLine[-31500 11020 31500 11020 1500]
ElementLine[19280 9060 19280 7090 1500]
ElementLine[19280 7090 20060 7090 1500]
ElementLine[20060 7090 20060 9060 1500]
ElementLine[3530 9060 3530 7090 1500]
ElementLine[3530 7090 4310 7090 1500]
ElementLine[4310 7090 4310 9060 1500]
ElementLine[12190 7090 12190 9060 1500]
ElementLine[11410 7090 12190 7090 1500]
ElementLine[11410 9060 11410 7090 1500]
ElementLine[-11430 7090 -11430 9060 1500]
ElementLine[-12210 7090 -11430 7090 1500]
ElementLine[-12210 9060 -12210 7090 1500]
ElementLine[-19310 7090 -19310 9060 1500]
ElementLine[-20090 7090 -19310 7090 1500]
ElementLine[-20090 9060 -20090 7090 1500]
ElementLine[-4340 9060 -4340 7090 1500]
ElementLine[-4340 7090 -3560 7090 1500]
ElementLine[-3560 7090 -3560 9060 1500]
ElementLine[31490 -1970 29520 -1970 1500]
ElementLine[29520 3150 31490 3150 1500]
ElementLine[-29540 3150 -31510 3150 1500]
ElementLine[-29540 -1970 -31510 -1970 1500]
ElementLine[-21670 -6690 -21670 -4720 1500]
ElementLine[-21670 -4720 -29540 -4720 1500]
ElementLine[-29540 -4720 -29540 9060 1500]
ElementLine[29520 9060 29520 -4720 1500]
ElementLine[29520 -4720 21650 -4720 1500]
ElementLine[21650 -4720 21650 -6690 1500]
ElementLine[-31510 -6690 -31510 11020 1500]
ElementLine[31490 -6690 31490 11020 1500]
ElementLine[-24820 -6690 -24820 -7480 1500]
ElementLine[-24820 -7480 -26000 -7480 1500]
ElementLine[-26000 -7480 -26000 -6690 1500]
Pin[-23640 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_3_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-15760 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_2_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-10 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-10 -985 -10 985 4720 2000 5520 "0" "_4_" ""]
Pad[-10 -985 -10 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_5_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_6_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_7_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_7_" ",onsolder"]
)
