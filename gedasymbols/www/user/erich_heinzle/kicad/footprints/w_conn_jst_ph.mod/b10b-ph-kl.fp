# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b10b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b10b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 45
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 10
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-43310 11020 43310 11020 1500]
ElementLine[41340 9060 -41340 9060 1500]
ElementLine[-43310 -6690 43310 -6690 1500]
ElementLine[-380 9060 -380 7090 1500]
ElementLine[-380 7090 400 7090 1500]
ElementLine[400 7090 400 9060 1500]
ElementLine[8270 7090 8270 9060 1500]
ElementLine[7490 7090 8270 7090 1500]
ElementLine[7490 9060 7490 7090 1500]
ElementLine[15360 9060 15360 7090 1500]
ElementLine[15360 7090 16140 7090 1500]
ElementLine[16140 7090 16140 9060 1500]
ElementLine[31100 9060 31100 7090 1500]
ElementLine[31100 7090 31880 7090 1500]
ElementLine[31880 7090 31880 9060 1500]
ElementLine[-8290 9060 -8290 7090 1500]
ElementLine[-8290 7090 -7510 7090 1500]
ElementLine[-7510 7090 -7510 9060 1500]
ElementLine[24010 7090 24010 9060 1500]
ElementLine[23230 7090 24010 7090 1500]
ElementLine[23230 9060 23230 7090 1500]
ElementLine[-23250 7090 -23250 9060 1500]
ElementLine[-24030 7090 -23250 7090 1500]
ElementLine[-24030 9060 -24030 7090 1500]
ElementLine[-31130 7090 -31130 9060 1500]
ElementLine[-31910 7090 -31130 7090 1500]
ElementLine[-31910 9060 -31910 7090 1500]
ElementLine[-16160 9060 -16160 7090 1500]
ElementLine[-16160 7090 -15380 7090 1500]
ElementLine[-15380 7090 -15380 9060 1500]
ElementLine[43310 -1970 41340 -1970 1500]
ElementLine[41340 3150 43310 3150 1500]
ElementLine[-41360 3150 -43330 3150 1500]
ElementLine[-41360 -1970 -43330 -1970 1500]
ElementLine[-33490 -6690 -33490 -4720 1500]
ElementLine[-33490 -4720 -41360 -4720 1500]
ElementLine[-41360 -4720 -41360 9060 1500]
ElementLine[41340 9060 41340 -4720 1500]
ElementLine[41340 -4720 33470 -4720 1500]
ElementLine[33470 -4720 33470 -6690 1500]
ElementLine[-43330 -6690 -43330 11020 1500]
ElementLine[43310 -6690 43310 11020 1500]
ElementLine[-36640 -6690 -36640 -7480 1500]
ElementLine[-36640 -7480 -37820 -7480 1500]
ElementLine[-37820 -7480 -37820 -6690 1500]
Pin[11810 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_7_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_6_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_5_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-35460 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-35460 -985 -35460 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-35460 -985 -35460 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-19700 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-19700 -985 -19700 985 4720 2000 5520 "0" "_3_" ""]
Pad[-19700 -985 -19700 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-27580 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_2_" ""]
Pad[-27580 -985 -27580 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-11830 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-11830 -985 -11830 985 4720 2000 5520 "0" "_4_" ""]
Pad[-11830 -985 -11830 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[19680 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_8_" ""]
Pad[19680 -985 19680 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[27560 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_9_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[35430 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_10_" ""]
Pad[35430 -985 35430 985 4720 2000 5520 "0" "_10_" ",onsolder"]
)
