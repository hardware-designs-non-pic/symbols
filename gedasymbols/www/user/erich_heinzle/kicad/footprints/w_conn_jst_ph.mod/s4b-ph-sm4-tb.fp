# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s4b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s4b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[9840 23620 -9840 23620 1500]
ElementLine[23620 27560 -23620 27560 1500]
ElementLine[-23620 3940 23620 3940 1500]
ElementLine[-23630 27560 -23630 -1970 1500]
ElementLine[23630 -1970 23630 27560 1500]
ElementLine[23630 -1970 22050 -1970 1500]
ElementLine[22840 3940 22840 -1970 1500]
ElementLine[22050 -1970 22050 3940 1500]
ElementLine[-22840 -1970 -22840 3940 1500]
ElementLine[-22050 3940 -22050 -1970 1500]
ElementLine[-22050 -1970 -23630 -1970 1500]
ElementLine[-9840 27560 -9840 23620 1500]
ElementLine[9840 23620 9840 27550 1500]
ElementLine[-15750 3940 -15750 21650 1500]
ElementLine[-15750 21650 -18510 21650 1500]
ElementLine[-18510 21650 -18510 3940 1500]
ElementLine[15750 3940 15750 21650 1500]
ElementLine[15750 21650 18510 21650 1500]
ElementLine[18510 21650 18510 3940 1500]
Pad[-11810 -4925 -11810 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-3930 -4925 -3930 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-21040 18885 -21040 26355 5900 2000 6700 "0" "__" "square"]
Pad[21040 18885 21040 26355 5900 2000 6700 "0" "__" "square"]
Pad[3930 -4925 3930 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[11810 -4925 11810 4925 3920 2000 4720 "0" "_4_" "square"]
)
