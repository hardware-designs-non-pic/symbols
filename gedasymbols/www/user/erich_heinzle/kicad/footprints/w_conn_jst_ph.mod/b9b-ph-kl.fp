# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b9b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b9b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 42
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 9
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[39370 11020 -39370 11020 1500]
ElementLine[-37400 9060 37400 9060 1500]
ElementLine[39370 -6690 -39370 -6690 1500]
ElementLine[4330 7090 4330 9060 1500]
ElementLine[3550 7090 4330 7090 1500]
ElementLine[3550 9060 3550 7090 1500]
ElementLine[11420 9060 11420 7090 1500]
ElementLine[11420 7090 12200 7090 1500]
ElementLine[12200 7090 12200 9060 1500]
ElementLine[27160 9060 27160 7090 1500]
ElementLine[27160 7090 27940 7090 1500]
ElementLine[27940 7090 27940 9060 1500]
ElementLine[-4350 9060 -4350 7090 1500]
ElementLine[-4350 7090 -3570 7090 1500]
ElementLine[-3570 7090 -3570 9060 1500]
ElementLine[20070 7090 20070 9060 1500]
ElementLine[19290 7090 20070 7090 1500]
ElementLine[19290 9060 19290 7090 1500]
ElementLine[-19310 7090 -19310 9060 1500]
ElementLine[-20090 7090 -19310 7090 1500]
ElementLine[-20090 9060 -20090 7090 1500]
ElementLine[-27190 7090 -27190 9060 1500]
ElementLine[-27970 7090 -27190 7090 1500]
ElementLine[-27970 9060 -27970 7090 1500]
ElementLine[-12220 9060 -12220 7090 1500]
ElementLine[-12220 7090 -11440 7090 1500]
ElementLine[-11440 7090 -11440 9060 1500]
ElementLine[39370 -1970 37400 -1970 1500]
ElementLine[37400 3150 39370 3150 1500]
ElementLine[-37420 3150 -39390 3150 1500]
ElementLine[-37420 -1970 -39390 -1970 1500]
ElementLine[-29550 -6690 -29550 -4720 1500]
ElementLine[-29550 -4720 -37420 -4720 1500]
ElementLine[-37420 -4720 -37420 9060 1500]
ElementLine[37400 9060 37400 -4720 1500]
ElementLine[37400 -4720 29530 -4720 1500]
ElementLine[29530 -4720 29530 -6690 1500]
ElementLine[-39390 -6690 -39390 11020 1500]
ElementLine[39370 -6690 39370 11020 1500]
ElementLine[-32700 -6690 -32700 -7480 1500]
ElementLine[-32700 -7480 -33880 -7480 1500]
ElementLine[-33880 -7480 -33880 -6690 1500]
Pin[15750 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_7_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[7880 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_6_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_5_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-31520 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-31520 -985 -31520 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-31520 -985 -31520 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-15760 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_3_" ""]
Pad[-15760 -985 -15760 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-23640 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_2_" ""]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-7890 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-7890 -985 -7890 985 4720 2000 5520 "0" "_4_" ""]
Pad[-7890 -985 -7890 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_8_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[31500 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_9_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_9_" ",onsolder"]
)
