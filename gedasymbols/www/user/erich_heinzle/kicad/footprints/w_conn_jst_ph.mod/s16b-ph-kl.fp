# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s16b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s16b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 16
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[66930 24800 -66930 24800 1500]
ElementLine[-57090 7870 57090 7870 1500]
ElementLine[66930 1570 -66930 1570 1500]
ElementLine[-60230 16140 -60230 24800 1500]
ElementLine[-60230 24800 -61410 24800 1500]
ElementLine[-61410 24800 -61410 16140 1500]
ElementLine[57080 7870 57080 24800 1500]
ElementLine[-57080 7870 -57080 24800 1500]
ElementLine[60220 16140 60220 9840 1500]
ElementLine[64160 16140 60220 16140 1500]
ElementLine[64160 9840 64160 16140 1500]
ElementLine[60220 9840 64160 9840 1500]
ElementLine[-64170 9840 -60230 9840 1500]
ElementLine[-60230 9840 -60230 16140 1500]
ElementLine[-60230 16140 -64170 16140 1500]
ElementLine[-64170 16140 -64170 9840 1500]
ElementLine[66130 -5510 66130 1570 1500]
ElementLine[-66120 -5510 -66120 1570 1500]
ElementLine[66920 24800 66920 -5510 1500]
ElementLine[66920 -5510 65350 -5510 1500]
ElementLine[65350 -5510 65350 1570 1500]
ElementLine[-65340 1570 -65340 -5510 1500]
ElementLine[-65340 -5510 -66910 -5510 1500]
ElementLine[-66910 -5510 -66910 24800 1500]
Pin[-59050 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-59050 -985 -59050 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-59050 -985 -59050 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-51180 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-51180 -985 -51180 985 4720 2000 5520 "0" "_2_" ""]
Pad[-51180 -985 -51180 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-43300 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-43300 -985 -43300 985 4720 2000 5520 "0" "_3_" ""]
Pad[-43300 -985 -43300 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-35430 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_4_" ""]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-27550 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-27550 -985 -27550 985 4720 2000 5520 "0" "_5_" ""]
Pad[-27550 -985 -27550 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-19680 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-19680 -985 -19680 985 4720 2000 5520 "0" "_6_" ""]
Pad[-19680 -985 -19680 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-11800 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[-11800 -985 -11800 985 4720 2000 5520 "0" "_7_" ""]
Pad[-11800 -985 -11800 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[-3930 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[-3930 -985 -3930 985 4720 2000 5520 "0" "_8_" ""]
Pad[-3930 -985 -3930 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[3950 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[3950 -985 3950 985 4720 2000 5520 "0" "_9_" ""]
Pad[3950 -985 3950 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[11820 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_10_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[19700 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[19700 -985 19700 985 4720 2000 5520 "0" "_11_" ""]
Pad[19700 -985 19700 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[27570 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[27570 -985 27570 985 4720 2000 5520 "0" "_12_" ""]
Pad[27570 -985 27570 985 4720 2000 5520 "0" "_12_" ",onsolder"]
Pin[35440 0 4720 2000 5520 2760 "0" "_13_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_13_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_13_" ",onsolder"]
Pin[43310 0 4720 2000 5520 2760 "0" "_14_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_14_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_14_" ",onsolder"]
Pin[51190 0 4720 2000 5520 2760 "0" "_15_" ""]
Pad[51190 -985 51190 985 4720 2000 5520 "0" "_15_" ""]
Pad[51190 -985 51190 985 4720 2000 5520 "0" "_15_" ",onsolder"]
Pin[59060 0 4720 2000 5520 2760 "0" "_16_" ""]
Pad[59060 -985 59060 985 4720 2000 5520 "0" "_16_" ""]
Pad[59060 -985 59060 985 4720 2000 5520 "0" "_16_" ",onsolder"]
)
