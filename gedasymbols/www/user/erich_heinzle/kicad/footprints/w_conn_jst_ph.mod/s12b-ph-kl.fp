# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s12b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s12b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 12
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-51180 1570 51180 1570 1500]
ElementLine[41340 7870 -41340 7870 1500]
ElementLine[-51180 24800 51180 24800 1500]
ElementLine[-44470 16140 -44470 24800 1500]
ElementLine[-44470 24800 -45650 24800 1500]
ElementLine[-45650 24800 -45650 16140 1500]
ElementLine[41330 7870 41330 24800 1500]
ElementLine[-41320 7870 -41320 24800 1500]
ElementLine[44470 16140 44470 9840 1500]
ElementLine[48410 16140 44470 16140 1500]
ElementLine[48410 9840 48410 16140 1500]
ElementLine[44470 9840 48410 9840 1500]
ElementLine[-48410 9840 -44470 9840 1500]
ElementLine[-44470 9840 -44470 16140 1500]
ElementLine[-44470 16140 -48410 16140 1500]
ElementLine[-48410 16140 -48410 9840 1500]
ElementLine[50380 -5510 50380 1570 1500]
ElementLine[-50360 -5510 -50360 1570 1500]
ElementLine[51170 24800 51170 -5510 1500]
ElementLine[51170 -5510 49600 -5510 1500]
ElementLine[49600 -5510 49600 1570 1500]
ElementLine[-49580 1570 -49580 -5510 1500]
ElementLine[-49580 -5510 -51150 -5510 1500]
ElementLine[-51150 -5510 -51150 24800 1500]
Pin[-43290 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-43290 -985 -43290 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-43290 -985 -43290 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-35440 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-35440 -985 -35440 985 4720 2000 5520 "0" "_2_" ""]
Pad[-35440 -985 -35440 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-27560 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_3_" ""]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-19690 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_4_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-11810 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_5_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_6_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_7_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_8_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[19690 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_9_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[27560 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_10_" ""]
Pad[27560 -985 27560 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[35440 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_11_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[43310 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_12_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_12_" ",onsolder"]
)
