# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s15b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s15b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 17
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[53150 23620 -53150 23620 1500]
ElementLine[-66930 27560 66930 27560 1500]
ElementLine[66930 3940 -66930 3940 1500]
ElementLine[-66950 27560 -66950 -1970 1500]
ElementLine[66940 -1970 66940 27560 1500]
ElementLine[66940 -1970 65360 -1970 1500]
ElementLine[66150 3940 66150 -1970 1500]
ElementLine[65360 -1970 65360 3940 1500]
ElementLine[-66160 -1970 -66160 3940 1500]
ElementLine[-65370 3940 -65370 -1970 1500]
ElementLine[-65370 -1970 -66950 -1970 1500]
ElementLine[-53150 27560 -53150 23620 1500]
ElementLine[53160 23620 53160 27550 1500]
ElementLine[-59060 3940 -59060 21650 1500]
ElementLine[-59060 21650 -61820 21650 1500]
ElementLine[-61820 21650 -61820 3940 1500]
ElementLine[59060 3940 59060 21650 1500]
ElementLine[59060 21650 61820 21650 1500]
ElementLine[61820 21650 61820 3940 1500]
Pad[-55140 -4925 -55140 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-47260 -4925 -47260 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-64370 18885 -64370 26355 5900 2000 6700 "0" "__" "square"]
Pad[64370 18885 64370 26355 5900 2000 6700 "0" "__" "square"]
Pad[-39390 -4925 -39390 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-31510 -4925 -31510 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-23650 -4925 -23650 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[-15760 -4925 -15760 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[-7900 -4925 -7900 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[-20 -4925 -20 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[7850 -4925 7850 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[15730 -4925 15730 4925 3920 2000 4720 "0" "_10_" "square"]
Pad[23600 -4925 23600 4925 3920 2000 4720 "0" "_11_" "square"]
Pad[31490 -4925 31490 4925 3920 2000 4720 "0" "_12_" "square"]
Pad[39360 -4925 39360 4925 3920 2000 4720 "0" "_13_" "square"]
Pad[47240 -4925 47240 4925 3920 2000 4720 "0" "_14_" "square"]
Pad[55120 -4925 55120 4925 3920 2000 4720 "0" "_15_" "square"]
)
