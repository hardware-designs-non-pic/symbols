# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s6b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s6b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 6
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-20870 16140 -20870 24800 1500]
ElementLine[-20870 24800 -22050 24800 1500]
ElementLine[-22050 24800 -22050 16140 1500]
ElementLine[27560 1570 -27560 1570 1500]
ElementLine[-17720 7870 17720 7870 1500]
ElementLine[-27560 24800 27560 24800 1500]
ElementLine[17710 7870 17710 24800 1500]
ElementLine[-17720 7870 -17720 24800 1500]
ElementLine[20850 16140 20850 9840 1500]
ElementLine[24790 16140 20850 16140 1500]
ElementLine[24790 9840 24790 16140 1500]
ElementLine[20850 9840 24790 9840 1500]
ElementLine[-24810 9840 -20870 9840 1500]
ElementLine[-20870 9840 -20870 16140 1500]
ElementLine[-20870 16140 -24810 16140 1500]
ElementLine[-24810 16140 -24810 9840 1500]
ElementLine[26760 -5510 26760 1570 1500]
ElementLine[-26760 -5510 -26760 1570 1500]
ElementLine[27550 24800 27550 -5510 1500]
ElementLine[27550 -5510 25980 -5510 1500]
ElementLine[25980 -5510 25980 1570 1500]
ElementLine[-25980 1570 -25980 -5510 1500]
ElementLine[-25980 -5510 -27550 -5510 1500]
ElementLine[-27550 -5510 -27550 24800 1500]
Pin[-19690 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-19690 -985 -19690 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-11810 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_2_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-3940 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_3_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_4_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[11810 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_5_" ""]
Pad[11810 -985 11810 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[19690 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_6_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_6_" ",onsolder"]
)
