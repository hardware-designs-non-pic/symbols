# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s13b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s13b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 13
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[55120 24800 -55120 24800 1500]
ElementLine[-45280 7870 45280 7870 1500]
ElementLine[55120 1570 -55120 1570 1500]
ElementLine[-48410 16140 -48410 24800 1500]
ElementLine[-48410 24800 -49590 24800 1500]
ElementLine[-49590 24800 -49590 16140 1500]
ElementLine[45270 7870 45270 24800 1500]
ElementLine[-45260 7870 -45260 24800 1500]
ElementLine[48410 16140 48410 9840 1500]
ElementLine[52350 16140 48410 16140 1500]
ElementLine[52350 9840 52350 16140 1500]
ElementLine[48410 9840 52350 9840 1500]
ElementLine[-52350 9840 -48410 9840 1500]
ElementLine[-48410 9840 -48410 16140 1500]
ElementLine[-48410 16140 -52350 16140 1500]
ElementLine[-52350 16140 -52350 9840 1500]
ElementLine[54320 -5510 54320 1570 1500]
ElementLine[-54300 -5510 -54300 1570 1500]
ElementLine[55110 24800 55110 -5510 1500]
ElementLine[55110 -5510 53540 -5510 1500]
ElementLine[53540 -5510 53540 1570 1500]
ElementLine[-53520 1570 -53520 -5510 1500]
ElementLine[-53520 -5510 -55090 -5510 1500]
ElementLine[-55090 -5510 -55090 24800 1500]
Pin[-47230 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-47230 -985 -47230 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-47230 -985 -47230 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-39380 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-39380 -985 -39380 985 4720 2000 5520 "0" "_2_" ""]
Pad[-39380 -985 -39380 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-31500 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_3_" ""]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-23630 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_4_" ""]
Pad[-23630 -985 -23630 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-15750 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_5_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_6_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_7_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_8_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_9_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_10_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[31500 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_11_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[39370 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_12_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_12_" ",onsolder"]
Pin[47240 0 4720 2000 5520 2760 "0" "_13_" ""]
Pad[47240 -985 47240 985 4720 2000 5520 "0" "_13_" ""]
Pad[47240 -985 47240 985 4720 2000 5520 "0" "_13_" ",onsolder"]
)
