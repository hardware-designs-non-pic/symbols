# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s6b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s6b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 8
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[17720 23620 -17720 23620 1500]
ElementLine[-31500 27560 31500 27560 1500]
ElementLine[31500 3940 -31500 3940 1500]
ElementLine[-31510 27560 -31510 -1970 1500]
ElementLine[31500 -1970 31500 27560 1500]
ElementLine[31500 -1970 29920 -1970 1500]
ElementLine[30710 3940 30710 -1970 1500]
ElementLine[29920 -1970 29920 3940 1500]
ElementLine[-30720 -1970 -30720 3940 1500]
ElementLine[-29930 3940 -29930 -1970 1500]
ElementLine[-29930 -1970 -31510 -1970 1500]
ElementLine[-17720 27560 -17720 23620 1500]
ElementLine[17720 23620 17720 27550 1500]
ElementLine[-23630 3940 -23630 21650 1500]
ElementLine[-23630 21650 -26390 21650 1500]
ElementLine[-26390 21650 -26390 3940 1500]
ElementLine[23620 3940 23620 21650 1500]
ElementLine[23620 21650 26380 21650 1500]
ElementLine[26380 21650 26380 3940 1500]
Pad[-19690 -4925 -19690 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-11810 -4925 -11810 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-28930 18885 -28930 26355 5900 2000 6700 "0" "__" "square"]
Pad[28930 18885 28930 26355 5900 2000 6700 "0" "__" "square"]
Pad[-3940 -4925 -3940 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[3940 -4925 3940 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[11800 -4925 11800 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[19690 -4925 19690 4925 3920 2000 4720 "0" "_6_" "square"]
)
