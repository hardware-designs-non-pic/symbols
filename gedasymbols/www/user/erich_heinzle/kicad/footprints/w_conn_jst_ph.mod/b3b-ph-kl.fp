# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b3b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b3b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[-15750 -6690 15750 -6690 1500]
ElementLine[13780 9060 -13780 9060 1500]
ElementLine[-15750 11020 15750 11020 1500]
ElementLine[-3550 7090 -3550 9060 1500]
ElementLine[-4330 7090 -3550 7090 1500]
ElementLine[-4330 9060 -4330 7090 1500]
ElementLine[3540 9060 3540 7090 1500]
ElementLine[3540 7090 4320 7090 1500]
ElementLine[4320 7090 4320 9060 1500]
ElementLine[15750 -1970 13780 -1970 1500]
ElementLine[13780 3150 15750 3150 1500]
ElementLine[-13780 3150 -15750 3150 1500]
ElementLine[-13780 -1970 -15750 -1970 1500]
ElementLine[-5910 -6690 -5910 -4720 1500]
ElementLine[-5910 -4720 -13780 -4720 1500]
ElementLine[-13780 -4720 -13780 9060 1500]
ElementLine[13780 9060 13780 -4720 1500]
ElementLine[13780 -4720 5910 -4720 1500]
ElementLine[5910 -4720 5910 -6690 1500]
ElementLine[-15750 -6690 -15750 11020 1500]
ElementLine[15750 -6690 15750 11020 1500]
ElementLine[-9060 -6690 -9060 -7480 1500]
ElementLine[-9060 -7480 -10240 -7480 1500]
ElementLine[-10240 -7480 -10240 -6690 1500]
Pin[-7880 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[7880 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_3_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_2_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_2_" ",onsolder"]
)
