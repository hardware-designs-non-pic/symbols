# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s3b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s3b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 3
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-9060 16140 -9060 24800 1500]
ElementLine[-9060 24800 -10240 24800 1500]
ElementLine[-10240 24800 -10240 16140 1500]
ElementLine[-15750 1570 15750 1570 1500]
ElementLine[-5910 7870 5910 7870 1500]
ElementLine[-15750 24800 15750 24800 1500]
ElementLine[5900 7870 5900 24800 1500]
ElementLine[-5910 7870 -5910 24800 1500]
ElementLine[9040 16140 9040 9840 1500]
ElementLine[12980 16140 9040 16140 1500]
ElementLine[12980 9840 12980 16140 1500]
ElementLine[9040 9840 12980 9840 1500]
ElementLine[-13000 9840 -9060 9840 1500]
ElementLine[-9060 9840 -9060 16140 1500]
ElementLine[-9060 16140 -13000 16140 1500]
ElementLine[-13000 16140 -13000 9840 1500]
ElementLine[14960 -5510 14960 1570 1500]
ElementLine[-14960 -5510 -14960 1570 1500]
ElementLine[15750 24800 15750 -5510 1500]
ElementLine[15750 -5510 14180 -5510 1500]
ElementLine[14180 -5510 14180 1570 1500]
ElementLine[-14180 1570 -14180 -5510 1500]
ElementLine[-14180 -5510 -15750 -5510 1500]
ElementLine[-15750 -5510 -15750 24800 1500]
Pin[-7870 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_2_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[7870 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_3_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_3_" ",onsolder"]
)
