# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module b11b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: b11b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 48
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 11
#
Element["" " JP    " "" "" 0 0 0 -11810 0 100 ""]
(
ElementLine[47240 11020 -47240 11020 1500]
ElementLine[-45280 9060 45280 9060 1500]
ElementLine[47240 -6690 -47240 -6690 1500]
ElementLine[3540 9060 3540 7090 1500]
ElementLine[3540 7090 4320 7090 1500]
ElementLine[4320 7090 4320 9060 1500]
ElementLine[-4320 9060 -4320 7090 1500]
ElementLine[-4320 7090 -3540 7090 1500]
ElementLine[-3540 7090 -3540 9060 1500]
ElementLine[12200 7090 12200 9060 1500]
ElementLine[11420 7090 12200 7090 1500]
ElementLine[11420 9060 11420 7090 1500]
ElementLine[19290 9060 19290 7090 1500]
ElementLine[19290 7090 20070 7090 1500]
ElementLine[20070 7090 20070 9060 1500]
ElementLine[35030 9060 35030 7090 1500]
ElementLine[35030 7090 35810 7090 1500]
ElementLine[35810 7090 35810 9060 1500]
ElementLine[-12230 9060 -12230 7090 1500]
ElementLine[-12230 7090 -11450 7090 1500]
ElementLine[-11450 7090 -11450 9060 1500]
ElementLine[27940 7090 27940 9060 1500]
ElementLine[27160 7090 27940 7090 1500]
ElementLine[27160 9060 27160 7090 1500]
ElementLine[-27190 7090 -27190 9060 1500]
ElementLine[-27970 7090 -27190 7090 1500]
ElementLine[-27970 9060 -27970 7090 1500]
ElementLine[-35070 7090 -35070 9060 1500]
ElementLine[-35850 7090 -35070 7090 1500]
ElementLine[-35850 9060 -35850 7090 1500]
ElementLine[-20100 9060 -20100 7090 1500]
ElementLine[-20100 7090 -19320 7090 1500]
ElementLine[-19320 7090 -19320 9060 1500]
ElementLine[47240 -1970 45270 -1970 1500]
ElementLine[45270 3150 47240 3150 1500]
ElementLine[-45300 3150 -47270 3150 1500]
ElementLine[-45300 -1970 -47270 -1970 1500]
ElementLine[-37430 -6690 -37430 -4720 1500]
ElementLine[-37430 -4720 -45300 -4720 1500]
ElementLine[-45300 -4720 -45300 9060 1500]
ElementLine[45270 9060 45270 -4720 1500]
ElementLine[45270 -4720 37400 -4720 1500]
ElementLine[37400 -4720 37400 -6690 1500]
ElementLine[-47270 -6690 -47270 11020 1500]
ElementLine[47240 -6690 47240 11020 1500]
ElementLine[-40580 -6690 -40580 -7480 1500]
ElementLine[-40580 -7480 -41760 -7480 1500]
ElementLine[-41760 -7480 -41760 -6690 1500]
Pin[7870 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_7_" ""]
Pad[7870 -985 7870 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_6_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-7880 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_5_" ""]
Pad[-7880 -985 -7880 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-39400 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-39400 -985 -39400 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-39400 -985 -39400 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-23640 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_3_" ""]
Pad[-23640 -985 -23640 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-31520 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-31520 -985 -31520 985 4720 2000 5520 "0" "_2_" ""]
Pad[-31520 -985 -31520 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-15770 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-15770 -985 -15770 985 4720 2000 5520 "0" "_4_" ""]
Pad[-15770 -985 -15770 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[15740 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[15740 -985 15740 985 4720 2000 5520 "0" "_8_" ""]
Pad[15740 -985 15740 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[23620 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_9_" ""]
Pad[23620 -985 23620 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[31490 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[31490 -985 31490 985 4720 2000 5520 "0" "_10_" ""]
Pad[31490 -985 31490 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[39370 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_11_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_11_" ",onsolder"]
)
