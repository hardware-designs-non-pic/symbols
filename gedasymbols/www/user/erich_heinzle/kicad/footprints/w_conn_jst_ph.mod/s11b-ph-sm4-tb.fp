# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s11b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s11b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 13
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-51180 3940 51180 3940 1500]
ElementLine[37400 23620 -37400 23620 1500]
ElementLine[-51180 27560 51180 27560 1500]
ElementLine[-51200 27560 -51200 -1970 1500]
ElementLine[51180 -1970 51180 27560 1500]
ElementLine[51180 -1970 49600 -1970 1500]
ElementLine[50390 3940 50390 -1970 1500]
ElementLine[49600 -1970 49600 3940 1500]
ElementLine[-50410 -1970 -50410 3940 1500]
ElementLine[-49620 3940 -49620 -1970 1500]
ElementLine[-49620 -1970 -51200 -1970 1500]
ElementLine[-37400 27560 -37400 23620 1500]
ElementLine[37410 23620 37410 27550 1500]
ElementLine[-43310 3940 -43310 21650 1500]
ElementLine[-43310 21650 -46070 21650 1500]
ElementLine[-46070 21650 -46070 3940 1500]
ElementLine[43300 3940 43300 21650 1500]
ElementLine[43300 21650 46060 21650 1500]
ElementLine[46060 21650 46060 3940 1500]
Pad[-39380 -4925 -39380 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-31500 -4925 -31500 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-48620 18885 -48620 26355 5900 2000 6700 "0" "__" "square"]
Pad[48620 18885 48620 26355 5900 2000 6700 "0" "__" "square"]
Pad[-23630 -4925 -23630 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-15750 -4925 -15750 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-7890 -4925 -7890 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[0 -4925 0 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[7860 -4925 7860 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[15740 -4925 15740 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[23610 -4925 23610 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[31490 -4925 31490 4925 3920 2000 4720 "0" "_10_" "square"]
Pad[39370 -4925 39370 4925 3920 2000 4720 "0" "_11_" "square"]
)
