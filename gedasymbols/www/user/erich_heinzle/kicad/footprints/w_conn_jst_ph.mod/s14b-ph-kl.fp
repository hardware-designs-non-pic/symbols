# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s14b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s14b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 14
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[59060 24800 -59060 24800 1500]
ElementLine[-49210 7870 49210 7870 1500]
ElementLine[59060 1570 -59060 1570 1500]
ElementLine[-52350 16140 -52350 24800 1500]
ElementLine[-52350 24800 -53530 24800 1500]
ElementLine[-53530 24800 -53530 16140 1500]
ElementLine[49210 7870 49210 24800 1500]
ElementLine[-49200 7870 -49200 24800 1500]
ElementLine[52350 16140 52350 9840 1500]
ElementLine[56290 16140 52350 16140 1500]
ElementLine[56290 9840 56290 16140 1500]
ElementLine[52350 9840 56290 9840 1500]
ElementLine[-56290 9840 -52350 9840 1500]
ElementLine[-52350 9840 -52350 16140 1500]
ElementLine[-52350 16140 -56290 16140 1500]
ElementLine[-56290 16140 -56290 9840 1500]
ElementLine[58260 -5510 58260 1570 1500]
ElementLine[-58240 -5510 -58240 1570 1500]
ElementLine[59050 24800 59050 -5510 1500]
ElementLine[59050 -5510 57480 -5510 1500]
ElementLine[57480 -5510 57480 1570 1500]
ElementLine[-57460 1570 -57460 -5510 1500]
ElementLine[-57460 -5510 -59030 -5510 1500]
ElementLine[-59030 -5510 -59030 24800 1500]
Pin[-51170 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-51170 -985 -51170 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-51170 -985 -51170 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-43310 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-43310 -985 -43310 985 4720 2000 5520 "0" "_2_" ""]
Pad[-43310 -985 -43310 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-35430 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_3_" ""]
Pad[-35430 -985 -35430 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-27560 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_4_" ""]
Pad[-27560 -985 -27560 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-19680 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-19680 -985 -19680 985 4720 2000 5520 "0" "_5_" ""]
Pad[-19680 -985 -19680 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-11810 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_6_" ""]
Pad[-11810 -985 -11810 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-3930 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[-3930 -985 -3930 985 4720 2000 5520 "0" "_7_" ""]
Pad[-3930 -985 -3930 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_8_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[11820 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_9_" ""]
Pad[11820 -985 11820 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[19690 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_10_" ""]
Pad[19690 -985 19690 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[27570 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[27570 -985 27570 985 4720 2000 5520 "0" "_11_" ""]
Pad[27570 -985 27570 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[35440 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_12_" ""]
Pad[35440 -985 35440 985 4720 2000 5520 "0" "_12_" ",onsolder"]
Pin[43310 0 4720 2000 5520 2760 "0" "_13_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_13_" ""]
Pad[43310 -985 43310 985 4720 2000 5520 "0" "_13_" ",onsolder"]
Pin[51180 0 4720 2000 5520 2760 "0" "_14_" ""]
Pad[51180 -985 51180 985 4720 2000 5520 "0" "_14_" ""]
Pad[51180 -985 51180 985 4720 2000 5520 "0" "_14_" ",onsolder"]
)
