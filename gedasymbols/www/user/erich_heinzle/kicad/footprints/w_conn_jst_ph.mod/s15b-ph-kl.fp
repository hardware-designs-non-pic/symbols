# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s15b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s15b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 15
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[62990 24800 -62990 24800 1500]
ElementLine[-53150 7870 53150 7870 1500]
ElementLine[62990 1570 -62990 1570 1500]
ElementLine[-56290 16140 -56290 24800 1500]
ElementLine[-56290 24800 -57470 24800 1500]
ElementLine[-57470 24800 -57470 16140 1500]
ElementLine[53140 7870 53140 24800 1500]
ElementLine[-53140 7870 -53140 24800 1500]
ElementLine[56280 16140 56280 9840 1500]
ElementLine[60220 16140 56280 16140 1500]
ElementLine[60220 9840 60220 16140 1500]
ElementLine[56280 9840 60220 9840 1500]
ElementLine[-60230 9840 -56290 9840 1500]
ElementLine[-56290 9840 -56290 16140 1500]
ElementLine[-56290 16140 -60230 16140 1500]
ElementLine[-60230 16140 -60230 9840 1500]
ElementLine[62190 -5510 62190 1570 1500]
ElementLine[-62180 -5510 -62180 1570 1500]
ElementLine[62980 24800 62980 -5510 1500]
ElementLine[62980 -5510 61410 -5510 1500]
ElementLine[61410 -5510 61410 1570 1500]
ElementLine[-61400 1570 -61400 -5510 1500]
ElementLine[-61400 -5510 -62970 -5510 1500]
ElementLine[-62970 -5510 -62970 24800 1500]
Pin[-55110 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-55110 -985 -55110 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-55110 -985 -55110 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[-47250 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[-47250 -985 -47250 985 4720 2000 5520 "0" "_2_" ""]
Pad[-47250 -985 -47250 985 4720 2000 5520 "0" "_2_" ",onsolder"]
Pin[-39370 0 4720 2000 5520 2760 "0" "_3_" ""]
Pad[-39370 -985 -39370 985 4720 2000 5520 "0" "_3_" ""]
Pad[-39370 -985 -39370 985 4720 2000 5520 "0" "_3_" ",onsolder"]
Pin[-31500 0 4720 2000 5520 2760 "0" "_4_" ""]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_4_" ""]
Pad[-31500 -985 -31500 985 4720 2000 5520 "0" "_4_" ",onsolder"]
Pin[-23620 0 4720 2000 5520 2760 "0" "_5_" ""]
Pad[-23620 -985 -23620 985 4720 2000 5520 "0" "_5_" ""]
Pad[-23620 -985 -23620 985 4720 2000 5520 "0" "_5_" ",onsolder"]
Pin[-15750 0 4720 2000 5520 2760 "0" "_6_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_6_" ""]
Pad[-15750 -985 -15750 985 4720 2000 5520 "0" "_6_" ",onsolder"]
Pin[-7870 0 4720 2000 5520 2760 "0" "_7_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_7_" ""]
Pad[-7870 -985 -7870 985 4720 2000 5520 "0" "_7_" ",onsolder"]
Pin[0 0 4720 2000 5520 2760 "0" "_8_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_8_" ""]
Pad[0 -985 0 985 4720 2000 5520 "0" "_8_" ",onsolder"]
Pin[7880 0 4720 2000 5520 2760 "0" "_9_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_9_" ""]
Pad[7880 -985 7880 985 4720 2000 5520 "0" "_9_" ",onsolder"]
Pin[15750 0 4720 2000 5520 2760 "0" "_10_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_10_" ""]
Pad[15750 -985 15750 985 4720 2000 5520 "0" "_10_" ",onsolder"]
Pin[23630 0 4720 2000 5520 2760 "0" "_11_" ""]
Pad[23630 -985 23630 985 4720 2000 5520 "0" "_11_" ""]
Pad[23630 -985 23630 985 4720 2000 5520 "0" "_11_" ",onsolder"]
Pin[31500 0 4720 2000 5520 2760 "0" "_12_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_12_" ""]
Pad[31500 -985 31500 985 4720 2000 5520 "0" "_12_" ",onsolder"]
Pin[39370 0 4720 2000 5520 2760 "0" "_13_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_13_" ""]
Pad[39370 -985 39370 985 4720 2000 5520 "0" "_13_" ",onsolder"]
Pin[47240 0 4720 2000 5520 2760 "0" "_14_" ""]
Pad[47240 -985 47240 985 4720 2000 5520 "0" "_14_" ""]
Pad[47240 -985 47240 985 4720 2000 5520 "0" "_14_" ",onsolder"]
Pin[55120 0 4720 2000 5520 2760 "0" "_15_" ""]
Pad[55120 -985 55120 985 4720 2000 5520 "0" "_15_" ""]
Pad[55120 -985 55120 985 4720 2000 5520 "0" "_15_" ",onsolder"]
)
