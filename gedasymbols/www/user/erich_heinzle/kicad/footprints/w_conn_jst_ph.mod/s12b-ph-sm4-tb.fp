# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s12b-ph-sm4-tb
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s12b-ph-sm4-tb
# Text descriptor count: 1
# Draw segment object count: 19
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 14
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-55120 3940 55120 3940 1500]
ElementLine[41340 23620 -41340 23620 1500]
ElementLine[-55120 27560 55120 27560 1500]
ElementLine[-55140 27560 -55140 -1970 1500]
ElementLine[55120 -1970 55120 27560 1500]
ElementLine[55120 -1970 53540 -1970 1500]
ElementLine[54330 3940 54330 -1970 1500]
ElementLine[53540 -1970 53540 3940 1500]
ElementLine[-54350 -1970 -54350 3940 1500]
ElementLine[-53560 3940 -53560 -1970 1500]
ElementLine[-53560 -1970 -55140 -1970 1500]
ElementLine[-41340 27560 -41340 23620 1500]
ElementLine[41350 23620 41350 27550 1500]
ElementLine[-47250 3940 -47250 21650 1500]
ElementLine[-47250 21650 -50010 21650 1500]
ElementLine[-50010 21650 -50010 3940 1500]
ElementLine[47240 3940 47240 21650 1500]
ElementLine[47240 21650 50000 21650 1500]
ElementLine[50000 21650 50000 3940 1500]
Pad[-43320 -4925 -43320 4925 3920 2000 4720 "0" "_1_" "square"]
Pad[-35440 -4925 -35440 4925 3920 2000 4720 "0" "_2_" "square"]
Pad[-52550 18885 -52550 26355 5900 2000 6700 "0" "__" "square"]
Pad[52550 18885 52550 26355 5900 2000 6700 "0" "__" "square"]
Pad[-27570 -4925 -27570 4925 3920 2000 4720 "0" "_3_" "square"]
Pad[-19690 -4925 -19690 4925 3920 2000 4720 "0" "_4_" "square"]
Pad[-11830 -4925 -11830 4925 3920 2000 4720 "0" "_5_" "square"]
Pad[-3940 -4925 -3940 4925 3920 2000 4720 "0" "_6_" "square"]
Pad[3920 -4925 3920 4925 3920 2000 4720 "0" "_7_" "square"]
Pad[11800 -4925 11800 4925 3920 2000 4720 "0" "_8_" "square"]
Pad[19670 -4925 19670 4925 3920 2000 4720 "0" "_9_" "square"]
Pad[27550 -4925 27550 4925 3920 2000 4720 "0" "_10_" "square"]
Pad[35420 -4925 35420 4925 3920 2000 4720 "0" "_11_" "square"]
Pad[43310 -4925 43310 4925 3920 2000 4720 "0" "_12_" "square"]
)
