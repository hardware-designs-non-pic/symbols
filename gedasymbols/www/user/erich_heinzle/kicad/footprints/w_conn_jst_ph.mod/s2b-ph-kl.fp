# Kicad module converted to gEDA PCB footprint by: Erich Heinzle
# Kicad module author: Walter Lain
# No warranties express or implied
# Footprint converted from Kicad Module s2b-ph-kl
# dist-license: GPL
# use-license: unlimited
# Users of the foot print must ensure that the solder mask reliefs and clearances
# are compatible with the PCB manufacturer's process tolerances
# Pins and SMD pads have been converted from Kicad foot prints which
# do not have solder mask relief or clearances specified.
# Fairly sane default values have been used for solder mask relief and clearances.
# Kicad module units: 0.1 mil
# Footprint = module name: s2b-ph-kl
# Text descriptor count: 1
# Draw segment object count: 24
# Draw circle object count: 0
# Draw arc object count: 0
# Pad count: 2
#
Element["" " JP    " "" "" 0 0 0 -11020 0 100 ""]
(
ElementLine[-5120 16140 -5120 24800 1500]
ElementLine[-5120 24800 -6300 24800 1500]
ElementLine[-6300 24800 -6300 16140 1500]
ElementLine[1970 7870 1970 24800 1500]
ElementLine[-1970 7870 -1970 24800 1500]
ElementLine[1970 7870 -1970 7870 1500]
ElementLine[5110 16140 5110 9840 1500]
ElementLine[9050 16140 5110 16140 1500]
ElementLine[9050 9840 9050 16140 1500]
ElementLine[5110 9840 9050 9840 1500]
ElementLine[-9060 9840 -5120 9840 1500]
ElementLine[-5120 9840 -5120 16140 1500]
ElementLine[-5120 16140 -9060 16140 1500]
ElementLine[-9060 16140 -9060 9840 1500]
ElementLine[11810 1570 -11810 1570 1500]
ElementLine[11020 -5510 11020 1570 1500]
ElementLine[-11020 -5510 -11020 1570 1500]
ElementLine[-11810 24800 11810 24800 1500]
ElementLine[11810 24800 11810 -5510 1500]
ElementLine[11810 -5510 10240 -5510 1500]
ElementLine[10240 -5510 10240 1570 1500]
ElementLine[-10240 1570 -10240 -5510 1500]
ElementLine[-10240 -5510 -11810 -5510 1500]
ElementLine[-11810 -5510 -11810 24800 1500]
Pin[-3940 0 4720 2000 5520 2760 "0" "_1_" ""]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_1_" "square"]
Pad[-3940 -985 -3940 985 4720 2000 5520 "0" "_1_" "square,onsolder"]
Pin[3940 0 4720 2000 5520 2760 "0" "_2_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_2_" ""]
Pad[3940 -985 3940 985 4720 2000 5520 "0" "_2_" ",onsolder"]
)
