Element["" "DIP 14 DIP172_Relay (300 mils wide)" "" "" 2.1402mm 2.1402mm 10.00mil 10.00mil 0 100 ""]
(
    Attribute("description" "DIP 14 DIP172_Relay (300 mils wide)")
	Attribute("author" "Lawrence Hallman")
	Attribute("dist-license" "GPL")
	Attribute("use-license" "Unlimited")
	Pin[0.0000 0.0000 80.00mil 88.00mil 80.00mil 25.00mil "Pin_1" "1" "square"]
	Pin[0.0000 100.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_2" "2" ""]
	Pin[0.0000 500.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_6" "6" ""]
	Pin[0.0000 600.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_7" "7" ""]
	Pin[300.00mil 600.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_8" "8" ""]
	Pin[300.00mil 500.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_9" "9" ""]
	Pin[300.00mil 100.00mil 80.00mil 88.00mil 80.00mil 25.00mil "Pin_13" "13" ""]
	Pin[300.00mil 0.0000 80.00mil 88.00mil 80.00mil 25.00mil "Pin_14" "14" ""]
	ElementLine [-50.00mil -50.00mil 100.00mil -50.00mil 10.00mil]
	ElementLine [-50.00mil -50.00mil -50.00mil 650.00mil 10.00mil]
	ElementLine [-50.00mil 650.00mil 350.00mil 650.00mil 10.00mil]
	ElementLine [350.00mil 650.00mil 350.00mil -50.00mil 10.00mil]
	ElementLine [350.00mil -50.00mil 200.00mil -50.00mil 10.00mil]
	ElementArc [150.00mil -50.00mil 50.00mil 50.00mil 0 180 10.00mil]

	)
