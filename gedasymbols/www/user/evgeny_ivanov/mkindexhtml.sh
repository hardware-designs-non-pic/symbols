#!/usr/bin/env sh

add_links()
{
    for i in `ls $1/*.$2 | sort`; do
        j=${i/$1\//}
        echo "<li><a href=\"$i\">$j</a></li>" >> index.html
    done
}

echo '<!--#set var="title" value="Evgeny Ivanov" -->
<!--#include virtual="/header.html" -->

Evgeny Ivanov<br>
email: <a href="mailto:iev@land.ru">iev@land.ru</a><br>
Files below are mostly untested. Please, check before use.

<h2>Symbols</h2>
<ul>' > index.html

add_links symbols sym
add_links symbols/connector sym

echo '</ul>

<h2>Footprints</h2>
<ul>' >> index.html

add_links footprints fp

echo '</ul>
' >> index.html

echo "Last update: `date +%F`
"  >> index.html

echo '<!--#include virtual="/trailer.html" -->
' >> index.html

