Element["" "SOT23-GDS" "SOT23-GDS" "SOT23-GDS" 88583 99409 0 0 0 100 ""]
(
	Pad[-5466 -54 -3300 -54 2362 2000 4362 "" "D" "square,edge2"]
	Pad[4180 3686 6346 3686 2362 2000 4362 "" "G" "square,edge2"]
	Pad[4180 -3794 6346 -3794 2362 2000 4362 "" "S" "square,edge2"]
	ElementLine [3590 6442 -2906 6442 1000]
	ElementLine [-2906 -6550 3590 -6550 1000]
	ElementLine [-2891 -6491 3609 -6491 800]
	Attribute("author" "John Griessen")
	Attribute("copyright" "2016 John Griessen")
	Attribute("use-license" "Unlimited")
	Attribute("dist-license" "GPL")
	)
