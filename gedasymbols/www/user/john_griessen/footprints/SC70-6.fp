Element["square,edge2" "SC70-6" "U1" "SC70-6" 30118 149606 0 0 0 100 "square,edge2"]
(
	Pad[6692 2559 8070 2559 1574 1000 2574 "" "5" "square"]
	Pad[-590 0 787 0 1574 1000 2574 "" "1" "square,edge2"]
	Pad[-590 2559 787 2559 1574 1000 2574 "" "2" "square,edge2"]
	Pad[-590 5118 787 5118 1574 1000 2574 "" "3" "square,edge2"]
	Pad[6692 5118 8070 5118 1574 1000 2574 "" "4" "square,edge2"]
	Pad[6692 0 8070 0 1574 1000 2574 "" "6" "square,edge2"]
	ElementLine [1771 6693 5707 6693 1000]
	ElementLine [1771 -1575 5708 -1575 1000]
	Attribute("author" "John Griessen")
	Attribute("copyright" "2016 John Griessen")
	Attribute("use-license" "Unlimited")
	Attribute("dist-license" "GPL")
	)
