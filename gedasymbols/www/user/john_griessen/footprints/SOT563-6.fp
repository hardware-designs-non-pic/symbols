Element["" "SOT563-6" "SOT563-6" "" 31.1000mm 21.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-0.0750mm 1.0000mm 0.0750mm 1.0000mm 0.3000mm 40.00mil 1.3160mm "" "3" "square"]
	Pad[-0.0750mm 0.5000mm 0.0750mm 0.5000mm 0.3000mm 40.00mil 1.3160mm "" "2" "square"]
	Pad[-0.0750mm 0.0000 0.0750mm 0.0000 0.3000mm 40.00mil 1.3160mm "" "1" "square"]
	Pad[1.2750mm 0.0000 1.4250mm 0.0000 0.3000mm 40.00mil 1.3160mm "" "6" "square"]
	Pad[1.2750mm 0.5000mm 1.4250mm 0.5000mm 0.3000mm 40.00mil 1.3160mm "" "5" "square"]
	Pad[1.2750mm 1.0000mm 1.4250mm 1.0000mm 0.3000mm 40.00mil 1.3160mm "" "4" "square"]
	ElementLine [0.1250mm -0.3250mm 1.2250mm -0.3250mm 10.00mil]
	ElementLine [0.1250mm 1.3250mm 1.2250mm 1.3250mm 10.00mil]
	Attribute("author" "John Griessen")
	Attribute("copyright" "2016 John Griessen")
	Attribute("use-license" "Unlimited")
	Attribute("dist-license" "GPL")
	)
