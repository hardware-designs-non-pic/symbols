Element["" "dio_0603" "dio_0603" "dio_0603" 28.0000mm 65.0000mm -2.0000mm -2.5000mm 0 100 ""]
(
	Pad[-0.7463mm 0.05mil -0.7463mm 0.05mil 39.37mil 55.12mil 47.24mil "cathode" "1" "square"]
	Pad[0.7536mm 0.05mil 0.7536mm 0.05mil 39.37mil 55.12mil 47.24mil "anode" "2" "square,edge2"]
	ElementLine [-1.5000mm -0.7500mm -1.4963mm 29.59mil 7.87mil]
	ElementLine [1.5036mm -29.47mil -1.8000mm -0.7500mm 7.87mil]
	ElementLine [1.5036mm 29.59mil 1.5036mm -29.47mil 7.87mil]
	ElementLine [-1.8000mm 0.7500mm 1.5036mm 29.59mil 7.87mil]
	ElementLine [-1.6500mm -0.7500mm -1.6500mm 29.53mil 7.87mil]
	ElementLine [-1.8000mm -0.7500mm -1.8000mm 29.53mil 7.87mil]

	)
