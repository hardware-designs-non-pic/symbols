Element["" "dio_1206" "dio_1206" "dio_1206" 28.0000mm 78.0000mm -3.5000mm -3.0000mm 0 100 ""]
(
	Pad[-1.7501mm 0.0000 -1.7501mm 0.0000 80.00mil 59.06mil 87.87mil "cathode" "1" "square"]
	Pad[1.7501mm 0.0000 1.7501mm 0.0000 80.00mil 59.06mil 87.87mil "anode" "2" "square,edge2"]
	ElementLine [-0.5001mm 0.0000 0.5500mm 0.0000 8.00mil]
	ElementLine [0.2499mm 0.5001mm -0.5001mm 0.0000 8.00mil]
	ElementLine [0.2499mm -0.4999mm 0.2499mm 0.5001mm 8.00mil]
	ElementLine [-0.5001mm 0.0000 0.2499mm -0.4999mm 8.00mil]
	ElementLine [-0.5001mm -0.5999mm -0.5001mm 0.6002mm 8.00mil]
	ElementLine [-0.1001mm -0.1999mm -0.1001mm 0.2002mm 8.00mil]
	ElementLine [0.1001mm -0.3000mm 0.1001mm 0.2002mm 8.00mil]
	ElementLine [0.0000 0.2502mm 0.2499mm 0.2502mm 8.00mil]
	ElementLine [0.0000 -0.2499mm 0.2499mm -0.2499mm 8.00mil]
	ElementLine [0.0000 -0.2499mm 0.0000 0.2502mm 8.00mil]
	ElementLine [-3.0000mm -1.2500mm -3.0000mm 1.2498mm 8.00mil]
	ElementLine [3.0000mm -1.2499mm -3.3000mm -1.2499mm 8.00mil]
	ElementLine [3.0000mm 1.2498mm 3.0000mm -1.2499mm 8.00mil]
	ElementLine [-3.3000mm 1.2500mm 3.0000mm 1.2498mm 8.00mil]
	ElementLine [-3.1500mm -1.2500mm -3.1500mm 1.2498mm 8.00mil]
	ElementLine [-3.3000mm -1.2500mm -3.3000mm 1.2498mm 8.00mil]

	)
