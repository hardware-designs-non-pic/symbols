Element["" "dio_300mil" "dio_300mil" "dio_300mil" 1200.00mil 700.00mil -100.00mil -150.00mil 0 100 ""]
(
	Pin[-150.00mil 0.0000 90.00mil 55.12mil 97.87mil 40.00mil "cathode" "1" "square,edge2"]
	Pin[150.00mil 0.0000 90.00mil 55.12mil 97.87mil 40.00mil "anode" "2" "edge2"]
	ElementLine [-100.00mil 0.0000 100.00mil 0.0000 25.00mil]
	ElementLine [20.00mil -40.00mil 50.00mil -40.00mil 25.00mil]
	ElementLine [50.00mil -20.00mil -10.00mil -20.00mil 25.00mil]
	ElementLine [50.00mil 40.00mil 20.00mil 40.00mil 25.00mil]
	ElementLine [50.00mil -60.00mil 50.00mil 60.00mil 25.00mil]
	ElementLine [-60.00mil -60.00mil -60.00mil 60.00mil 25.00mil]
	ElementLine [-50.00mil 0.0000 50.00mil -60.00mil 25.00mil]
	ElementLine [-50.00mil 0.0000 50.00mil 60.00mil 25.00mil]
	ElementLine [50.00mil 20.00mil -10.00mil 20.00mil 25.00mil]

	)
