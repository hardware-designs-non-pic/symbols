#!/usr/bin/python
#
# Generates a catalog of footprints in HTML format
#
# This program generates a catalog of footprints for the gedasymbols.org
# website. The program reads footprint files, extracts metadata from the
# footprint files and generates an HTML catalog.
#
# The PCB footprint must contain the following attribute:
#
#     Attribute("description" "category, description")
#

import glob
import os
import re

#
# a list of globs to locate all the footprint files
#
footprint_folders = [
    "./footprints/*.fp"
    ]


#
# the output filename for the footprint catalog
#
output_filename = "footprints.html"


#
# Regex used to find the attributes inside a PCB footprint file
#
description_regex = re.compile(
    "(attribute\s*\(\s*\"(.*?)\".*\"(.*?)\"\s*\))",
    re.IGNORECASE
    )


#
# Regex used to extract the category from the description
#
category_regex = re.compile("([^,]*)")


#
# get a list of all the filenames in the footprint folders
#
def get_filenames(footprint_folders):
    filenames = []
    for folder in footprint_folders:
        filenames.extend(glob.glob(folder))
    return filenames


#
# get a dictionary of all the attributes inside a footprint file
#
# @param filename the filename of the footprint
# @return a dictionary of attributes
#
def get_attributes(filename):
    contents = open(filename).read()
    matches = description_regex.findall(contents)
    return dict((k, v) for a, k, v in matches)


#
# A class to store metadata belonging to a footprint
#
class Footprint:

    #
    # initialize a new footprint instance
    #
    # @param filename the filename of the footprint
    #
    def __init__(self, filename):
        self.filename = filename
        attributes = get_attributes(filename)
        self.description = attributes["description"]
        self.category = category_regex.match(self.description).group(1)


if __name__ == "__main__":
    filenames = get_filenames(footprint_folders)
    footprints = [Footprint(f) for f in filenames]
    categories = sorted(set(f.category for f in footprints))

    output = open(output_filename, "w")
    for category in categories:
        output.write("<h3>%s</h3>" % category)
        output.write("<ul>")
        f2 = sorted(footprints, key=lambda k: k.filename)
        for footprint in f2:
            if category == footprint.category:
                output.write('<li><a href="%s">%s</a> - %s</li>' % (
                    footprint.filename,
                    os.path.basename(footprint.filename),
                    footprint.description
                    ))
        output.write("</ul>")
