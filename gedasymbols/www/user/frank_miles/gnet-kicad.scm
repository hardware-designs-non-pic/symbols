;;; gEDA - GPL Electronic Design Automation
;;; gnetlist - gEDA Netlist
;;; KiCad backend
;;; Copyright (C) 2003, 2005-2010 Dan McMahill
;;; Copyright (C) 2015 Frank Miles
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;;; MA 02111-1301 USA.

;;; This has been adapted from Dan McMahill's futurenet2 and Jaume Masip's
;;;		systemc backends.
;;;			Thanks Dan and Jaume!
;;; There is more than one KiCad netlist format that can be generated
;;;		from the KiCad schematic package.  This code is intended to
;;;		match the simplest "legacy" format.  I haven't found a solid
;;;		KiCad document that defines this format - however working
;;;		backwards from the kicad source code - and confirmed by exporting
;;;		netlists from kicad:eeschem it appears essentially identical
;;;		to an old format produced by the DOS OrCAD schematic package
;;;		for use by the OrCAD PCB.  Only the header appears to be
;;;		different.
;;; Being somewhat short on time, I have kept the following restrictions
;;;		from the FutureNet2 netlist generation:
;;;  - Output the netnames in all caps.
;;;  - Netname length is 8 characters max.  +,-, and _ are allowed
;;; One significant deviation is that I have replaced the 32-bit hex code
;;;		for the date for each component with a hash of each component's
;;;		contents.  This provides a single-value comparison to see if
;;;		a component or its connections have changed (which is presumably
;;;		the reason for the date code) in comparing netlists.  This is
;;;		buggy in that you cannot tell from the value which is the newer
;;;		(and hopefully better) component.  So look at the file dates!
;;;		It has the advantage that regenerating the netlist will preserve
;;;		all unchanged components, minimizing apparent changes while not
;;;		requiring access to previously generated netlists.

;; This procedure takes a net name as determined by gnetlist and
;; modifies it to be a valid FutureNet2 net name.
;;
(define kicad:map-net-names
  (lambda (net-name)
    (let ((rx (make-regexp "^unnamed_net"))
	  (net-alias net-name)
          )
      ;; XXX we should use a dynamic regexp based on the current value
      ;; for the unnamed net base string.

      ;; Remove "unnamed_net" and replace with "UN"
      (if (regexp-exec rx net-name) 
	    (set! net-alias (string-append "UN" (substring net-name 11)))
	    )
      ;; Truncate to 8 characters
      (if (> (string-length net-alias) 8)
	  (set! net-alias (substring net-alias 0 8))
	  )
      ;; Convert to all upper case
      (string-upcase net-alias)
      )
    )
  )

;; This procedure takes a refdes as determined by gnetlist and
;; modifies it to be a valid FutureNet2 refdes.  This has been
;; left in for no particular reason for KiCad.
(define kicad:map-refdes
  (lambda (refdes)
    (let ((refdes-alias refdes)
          )
      ;; XXX do we need to truncate to 8 characters like with net names?
;;      (if (> (string-length refdes-alias) 8)
;;	  (set! refdes-alias (substring refdes-alias 0 8))
;;	  )
      ;; Convert to all upper case
      (string-upcase refdes-alias)
      )
    )
 )

; Determine if a 'word' represents an integer (no signs or decimal points)
; RETURN #t (string represents integer) else #f
(define kicad:isWordNumeric
  (lambda (xstr)
	(define cl '())
	(define retval #t)
	(if (or (null? xstr) (not (string? xstr)) (= 0 (string-length xstr)))	; (not string) or (0 length)
	  (set! retval #f)
	  (begin
	    (set! cl (string->list xstr))
		(while (and retval (< 0 (length cl)))
		  (if (not (char-numeric? (car cl)))
	  		(set! retval #f)
			(set! cl (cdr cl))
		  )
		)
	  )
    )
	retval
   )
 )
;UT (kicad:isWordNumeric "0")	;#t
;UT (kicad:isWordNumeric "2648") ;#t
;UT (kicad:isWordNumeric "0x3")	;#f
;UT (kicad:isWordNumeric '())	;#f
;UT (kicad:isWordNumeric 123)	;#f

; Convert a 'word' to a number if that's what it represents
; otherwise returns the argument
(define kicad:cnvtWord2Number
  (lambda (xstr)
	(if (kicad:isWordNumeric xstr)
	  (string->number xstr)
	  xstr
	)
   )
 )
;UT (kicad:cnvtWord2Number "0x3")	; "0x3"
;UT (kicad:cnvtWord2Number "2648")	; 2648
;UT (kicad:cnvtWord2Number '())		; '()

; Compare pinnumbers (for netlist readability)
(define kicad:lessThanPN
  (lambda (x y)
	(define result #f)
	(define x2 (kicad:cnvtWord2Number x))
	(define y2 (kicad:cnvtWord2Number y))
	(if (and (string? x2) (string? y2))
		(set! result (string<? x2 y2))
		(if (and (number? x2) (number? y2))
			(set! result (< x2 y2))
			(set! result (string? x2))
 		)
    )
	result
  )
 )
;UT (kicad:lessThanPN "123" "321")	; #t
;UT (kicad:lessThanPN "123" "0x321") ; #f
;UT (kicad:lessThanPN "321" "123")	; #f
;UT (kicad:lessThanPN "0x321" "123") ; #t

; Recursively join a list of lists of strings into a single string.
; The top-level call should have <final> ="" (empty string)
; Allowed list members include integers; final result preserves ordering).
; RETURN: a single string incorporating the whole of <strmob>.
(define kicad:joinIntoString
  (lambda (strmob final)
	(if (list? strmob)
	  (begin
		(if (< 1 (length strmob))
		  (set! final (kicad:joinIntoString (cdr strmob) (kicad:joinIntoString (car strmob) final))) ;list recurs
		  (if (= 1 (length strmob))
			(set! final (kicad:joinIntoString (car strmob) final))	; process list with single element
			;#f			; empty list (do nothing)
		  )
		)
	)
	(if (string? strmob)
	  (set! final (string-append final " " strmob))	;next element is a string
	  (if (number? strmob)
	    (set! final (string-append final " " (number->string strmob)))	;next element is a number
		(if (not (eq? #f strmob))
		  (begin
			(newline)
			(display "What do I do now???")
			(display strmob)
			(newline)
			(throw 'unhandled-value-type)
		  )
		  ; else we ignore #f (deliberately eliminated item)
		)
	  )
	)
   )
   final
  )
 )
;UT (kicad:joinIntoString (list "abc" "def") "")	; " abc def"
;UT (kicad:joinIntoString (list "one" (list 2 "three" (list "four" 5)) "six") "") ; " one 2 three four 5 six"

(define kicad:showItemNL
  (lambda (itemlist tabcnt)
	(define pretab (make-string tabcnt #\tab))
	(display pretab)
	(display itemlist)
	(newline)
	)
  )
; Exclude any "unconnected" pin-netname entries.  This is especially important for slotted
;	components where some power pins are left open.
; ARGUMENTS: (source-list destination-list) where the
;	initial call should have an empty list for the latter.
; RETURN: elided list
(define u_n "unconnected_pin-")
(define u_n_l (string-length u_n))
(define kicad:exclUncon
  (lambda (src_l dest_l)
	(define elem '())
	(define lastelem '())
	;	(display "\n PROCESS: ")
	;	(display src_l)
	;	(newline)
	(if (and (not (null? src_l)) (list? src_l) (< 0 (length src_l)))
	  (begin
		(set! elem (car src_l))
	;	(display "  E: ")
	;	(display elem)
	;	(newline)
	;	;(set! lastelem (cdr (car (cdr elem))))
		(set! lastelem (cdr elem))
	;	(display "  L: ")
	;	(display lastelem)
	;	(display "  after this:")
	;	(display (cdr src_l))
	;	(newline)
		(if (or (> u_n_l (string-length lastelem)) (not (string=? u_n (substring lastelem 0 u_n_l))))
		  (set! dest_l (cons (list (car elem) lastelem) (kicad:exclUncon (cdr src_l) dest_l)))
	   		(set! dest_l (kicad:exclUncon (cdr src_l) dest_l))
		  ;(display "   skipping!\n")
		 )
	   )
	 )
	dest_l
   )
 )
; get all of the pin-net sets for a given part
; RETURN list of (pin# netname) in sorted order
(define kicad:getPinNetSets
  (lambda (package)
	(define gpnl (gnetlist:get-pins-nets package))
	;(display "\n  GPNL - pre-filter")
	;(display gpnl)
	;(newline)
	(set! gpnl (kicad:exclUncon gpnl '()))
	;(display "\n  GPNL - presort")
	;(display gpnl)
	;(newline)
	(set! gpnl (sort gpnl (lambda (x y) (kicad:lessThanPN (car x) (car y)))))
	;(display "\n  GPNL - post-sort")
	;(display gpnl)
	;(newline)
	gpnl
  )
 )
; Recursively develop the complete pin-list (with net connections) for a component
; The top-level call should have level=1 and ecp_pinlist=()
; RETURN: list of (pinnumber net-name)
(define kicad:eval_component_pins
  (lambda (port package pins level ecp_pinlist)	; dumpflag)
	(define abpn '())
	(define anet '())
    (if (and (not (null? package)) (not (null? pins)))
	(begin
	  (let
		((pin (car pins)))
	    ;;(set! abpn (gnetlist:get-attribute-by-pinnumber package pin "pinnumber"))
		;;(if (and (= 1 level) (string=? "U25" refdes))
		;;(if (= 1 level)
		;(if dumpflag
		;  (kicad:showItemNL pin 2))
		;(if dumpflag
		;  (kicad:showItemNL (gnetlist:get-nets package pin) 1)
		;  ;;(display (gnetlist:get-pins package))
		;  ;;(display (gnetlist:get-nets package pin))
		;  ;;(display (gnetlist:get-nets package pin))
		;  )
		(set! anet (gnetlist:alias-net (car (gnetlist:get-nets package pin))))
		;;(set! ecp_pinlist (cons (list pin abpn anet) ecp_pinlist))
		(set! ecp_pinlist (cons (list pin anet) ecp_pinlist))
	  )
	  (set! ecp_pinlist (kicad:eval_component_pins port package (cdr pins) (+ level 1) ecp_pinlist)) ; dumpflag))
	)
   )
   (if (= 1 level)
		(sort ecp_pinlist (lambda (x y) (kicad:lessThanPN (car x) (car y))))
		ecp_pinlist
   )
  )
 )

; Generate hash from a list of 'stuff' describing a component and its pins/nets.
; This version truncates the hash to the lowest 32bits and adds a '/' prefix.
(define kicad:generateCompHash
  (lambda (mashup)
	(define mash_string (kicad:joinIntoString mashup ""))
	(define longhash (format #f "~:@(~x~)" (string-hash mash_string)))
	(string-append "/" (substring longhash (- (string-length longhash) 8)))
  )
 )
; Write-out all pin-nets.
(define kicad:showPinNets
  (lambda (xpnlst port)
	(define current '())
	(if (< 0 (length xpnlst))
	  (begin
	  	(set! current (car xpnlst))
		(display (string-append "  " "  ( " (car current) "\t" (car (cdr current)) " )\n") port)
		(kicad:showPinNets (cdr xpnlst) port)
	  )
	)
  )
 )

;; Recursively write out the components.
(define kicad:components
   (lambda (port packages symcnt)
	  (define allhash '())
	  (define footpr '())
	  (define ecp_pinlist '())
	  (define pinnet '())
	  (define refdes '())
	  (define val '())
	  ;(define dumpflag (string=? "a" "Z"))
      (if (not (null? packages))
         (begin
            (let ((pattern (gnetlist:get-package-attribute (car packages) "pattern"))
                  (package (car packages)))
	    	;; The above pattern should stay as "pattern" and not "footprint"
	        ;; get the footprint; reference designator; and part-value
		    (set! footpr (gnetlist:get-package-attribute package "footprint"))
		    (set! refdes (gnetlist:alias-refdes package))
	        (set! val (gnetlist:get-package-attribute package "value"))
		    (if (string=? val "unknown") 
			  (set! val (gnetlist:get-package-attribute package "device") ))

		    ;; get the connections: pins and net-names
		    (set! pinnet (kicad:getPinNetSets package))
		    (set! allhash (kicad:generateCompHash
							(kicad:joinIntoString (list footpr refdes val pinnet) "")))
			;; debug printout
			;(display "Convert: ")
			;(display (kicad:joinIntoString (list footpr refdes val pinnet) ""))
			;(display " into ")
			;(display allhash)
			;(newline)

			;; output!
			(display "  ( " port)
			(display allhash port)
			(display "  " port)
			(display footpr port)
			(display "  " port)
			(display refdes port)
			(display "  " port)
			(display val port)
			(newline port)
			(kicad:showPinNets pinnet port)
			;; close the part
			(display "  )\n" port)
	      )
          (kicad:components port (cdr packages) (+ symcnt 1))
	    )
	  )
     )
   )

;; The top level netlister for kicad
(define kicad 
   (lambda (filename)
     (newline)
     (display "---------------------------------\n")
     (display "gEDA/gnetlist KiCad Backend\n")
     (display "This backend is EXPERIMENTAL\n")
     (display "Use at your own risk!\n")
     (display "\n")
     (display "You may need to run the output netlist\n")
     (display "through unix2dos before importing to\n")
     (display "windows based layout tools\n")
     (display "---------------------------------\n\n")

      (let ((port (open-output-file filename))
	    (all-nets (gnetlist:get-all-unique-nets "dummy"))
	    )
	
	;; initialize the net-name aliasing
        (gnetlist:build-net-aliases kicad:map-net-names all-unique-nets)

	;; initialize the refdes aliasing
        (gnetlist:build-refdes-aliases kicad:map-refdes packages)

	;; write the header
	(display "# EESchema Netlist Version 1.1\tCreated " port)
	(display (strftime "%c" (localtime (current-time))) port)
	(display "\n(\n" port)
	
	;; write the components and their nets
	(kicad:components port packages 1)
	;;(display ")\n" port)
	
	;; terminating ")"
	(display ")\n\n" port)
	
	;; close netlist
	(close-output-port port)
	)
  )
 )

