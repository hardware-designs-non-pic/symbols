;;; gEDA - GPL Electronic Design Automation
;;; censor-fix.scm - plug-in to fix the attribute censorship bug
;;; Copyright (C) 2016 John P. Doty
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;; Load this file with the -m flag to gnetlist.

;; Uppercases all device= attributes

(define devupper:get-package-attribute gnetlist:get-package-attribute)

(define (gnetlist:get-package-attribute refdes attribute)
	(let 
		(
			(v (devupper:get-package-attribute refdes attribute))
		)
		(if (equal? attribute "device")
			(string-upcase v)
			v
		)
	)
)