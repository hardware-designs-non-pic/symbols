#!/bin/sh

# get the query string from the web server
qs="$QUERY_STRING"

echo Content-type: text/plain
echo

### Implementation ###
list()
{
	cd $DOCUMENT_ROOT
	find . -name '*.fp' -printf '%p|%s|%T@\n'  | sed "s@^\./@@g;s/0*$/0/;"
}

case "$qs"
in
	md5) list | md5sum ;;
	gzip) list | gzip ;;
	*) list
esac
