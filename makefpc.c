#include <stdio.h>

#define SPACING 157.48
#define WIDTH 90
#define LENGTH 1970

void main( void )
{
    long x1, y1, x2, y2;
    int pinno;

    printf("# 33 pin flat cable connector, 0.40mm spacing\n#\n");
    printf("    Element[0x00000000 \"FPC connector 0.4mm 33 pin\" \"J1\" \"FPC4033\" 0 0 -250 -40 0 100 0x00000000]\n");
    printf("(\n");
    for ( pinno=-16; pinno<17; pinno++ )
	{
	    x1 = (long)((float)pinno*SPACING);
	    x2 = x1 + WIDTH;
	    y1 = 0;
	    y2 = y1+LENGTH;
	    printf("    Pad[ %6d %6d %6d %6d %d 20 \"%d\" \"%d\" 0x00000100]\n",
		   x1,y1,x2,y2,WIDTH,17+pinno,17+pinno);
	}
    printf("    ElementLine[-30000 -5000 30000 -5000 1000]\n");
    printf("    ElementLine[-30000  5000 30000  5000 1000]\n");
    printf(")\n");
}

	    



